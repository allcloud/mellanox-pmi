trigger FM_discussion_to_RM on FM_Discussion__c (after insert, after update) {

set<FM_discussion__c> FM_Move_to_RM = new set<FM_discussion__c>(); 
integer RMCase1 = 0; 
integer RMCase2 = 0;
integer RMCase3 = 0; 
integer RMCase4 = 0; 
integer RMCase5 = 0; 

 

for(FM_discussion__c FM:trigger.new)
{
  if( (trigger.isinsert && FM.RM_case1__c!=NULL && FM.from_RM__c ==false)   
     ||(trigger.isupdate && FM.RM_case1__c!=NULL && trigger.oldmap.get(FM.id).RM_case1__c==NULL))                 
  {FM_Move_to_RM.add(FM);      
   RMCase1 =1; }

                    
  if( (trigger.isinsert && FM.RM_case2__c!=NULL && FM.from_RM__c ==false)    
      ||(trigger.isupdate && FM.RM_case2__c!=NULL && trigger.oldmap.get(FM.id).RM_case2__c==NULL))                  
  {FM_Move_to_RM.add(FM);      
   RMCase2 =1; }         
        
 if( (trigger.isinsert && FM.RM_case3__c!=NULL && FM.from_RM__c ==false) ||     
      (trigger.isupdate && FM.RM_case3__c!=NULL && trigger.oldmap.get(FM.id).RM_case3__c==NULL))                  
  {FM_Move_to_RM.add(FM);      
   RMCase3 =1; }
   
  if( (trigger.isinsert && FM.RM_case4__c!=NULL && FM.from_RM__c ==false) ||     
      (trigger.isupdate && FM.RM_case4__c!=NULL && trigger.oldmap.get(FM.id).RM_case4__c==NULL))                  
  {FM_Move_to_RM.add(FM);      
   RMCase4 =1; } 
   
   if( (trigger.isinsert && FM.RM_case5__c!=NULL && FM.from_RM__c ==false) ||     
      (trigger.isupdate && FM.RM_case5__c!=NULL && trigger.oldmap.get(FM.id).RM_case5__c==NULL))                  
  {FM_Move_to_RM.add(FM);      
   RMCase5 =1; } 
   


}  
  
system.debug('RM to insert' +  FM_Move_to_RM);
system.debug('RM to insert- Length: ' +  FM_Move_to_RM.size());
system.debug('RMcase1' +  RMcase1);
system.debug('RMcase2' +  RMcase2);


 for(FM_discussion__c FMd:FM_Move_to_RM)
{
 if( RMcase1>0)            
 ENV.Insert_RM_discussion(FMd,FMd.RM_case1__c);          
 if( RMcase2>0)            
 ENV.Insert_RM_discussion(FMd,FMd.RM_case2__c);
 if( RMcase3>0)            
 ENV.Insert_RM_discussion(FMd,FMd.RM_case3__c);
 if( RMcase4>0)            
 ENV.Insert_RM_discussion(FMd,FMd.RM_case4__c);
 if( RMcase5>0)            
 ENV.Insert_RM_discussion(FMd,FMd.RM_case5__c);
}
}