trigger Project_Member_after_delete on Project_Member__c (after delete) {


List<id> CustomerProjectIds =  new list<id>();
List<id> MellanoxProjectIds =  new list<id>();
List<id> FollowObjects =  new list<id>();
List<EntitySubscription> FollowersToDelete = new List<EntitySubscription>();
map<id,set<id>> MemToProj =  new map<id,set<id>>();
set<id> Projlist = new set<id>();
List<Milestone1_Milestone__Share> MsSharesToDelete = new List<Milestone1_Milestone__Share>();
List<Milestone1_Project__Share> PmSharesToDelete = new List<Milestone1_Project__Share>();
map<id,id> MilestoneToProj = new map<id,id>();
map<id,id> TaskToMilestone = new map<id,id>();


for (Project_Member__c  contact : Trigger.old) 
{
// System.debug('CommunityID' + ConnectApi.Communities.getCommunities());

 if(contact.Project_Member__c != NULL)
  { 
    
    if(contact.Member_UserType__c != 'Standard')
    {
     CustomerProjectIds.add(contact.Project__c);
    }
     else
     {
      FollowObjects.add(contact.Project__c);
      MellanoxProjectIds.add(contact.Project__c);
     }
    
   // FollowObjects.add(contact.Project__c);
   // PMtoUserType.put(contact.Project_Member__c, contact.Member_UserType__c);
      
    
    if(!MemToProj.containsKey(contact.Project__c))
     {
      Projlist = new set<id>();
      Projlist.add(contact.Project__c);
      MemToProj.put(contact.Project_Member__c,Projlist);
     }else
     {
      Projlist = MemToProj.get(contact.Project__c);
      Projlist.add(contact.Project__c);
      MemToProj.put(contact.Project_Member__c,Projlist); 
     } // else
   } //if

}


// Remove Mellanox user viewer
if(!MellanoxProjectIds.isempty())
{
for( Milestone1_Milestone__c ms:[Select id,Project__c, ispublic__c from Milestone1_Milestone__c where Project__c in:MellanoxProjectIds] )
  {
   FollowObjects.add(ms.id);
   MilestoneToProj.put(ms.id,ms.Project__c);
 }
 for( task ts:[Select id,WhatId  from task where WhatId in:MilestoneToProj.keySet()] )
  {
   FollowObjects.add(ts.id);
   TaskToMilestone.put(ts.id,ts.WhatId);
  }
  
  
for(EntitySubscription es:[select SubscriberId, ParentId  from EntitySubscription where ParentId in: FollowObjects LIMIT 1000 ])
{

  if(MemToProj.containsKey(es.SubscriberId) && MemToProj.get(es.SubscriberId).contains( es.ParentId))  // The follower is member of relevant project
  {FollowersToDelete.add(es);}
 
  if(MemToProj.containsKey(es.SubscriberId) && MilestoneToProj.containsKey(es.ParentId)&& MemToProj.get(es.SubscriberId).contains(MilestoneToProj.get(es.ParentId)))
  {FollowersToDelete.add(es);}
 
  if(MemToProj.containsKey(es.SubscriberId) && TaskToMilestone.containsKey(es.ParentId)&& MemToProj.get(es.SubscriberId).contains(MilestoneToProj.get(TaskToMilestone.get(es.ParentId))))
  {FollowersToDelete.add(es);}
} //for

delete(FollowersToDelete);

}

// Remove Portal user shaing rules

if(!CustomerProjectIds.isempty())
{
 for( Milestone1_Milestone__c ms:[Select id,Project__c, ispublic__c from Milestone1_Milestone__c where Project__c in:CustomerProjectIds] )
  {
   MilestoneToProj.put(ms.id,ms.Project__c);
  }
  
 for(Milestone1_Milestone__Share MMS: [SELECT Id, ParentId, UserOrGroupId,AccessLevel,RowCause FROM Milestone1_Milestone__Share where ParentId in: MilestoneToProj.keySet() AND RowCause ='Manual' ])
 {
  if(MemToProj.containsKey(MMS.UserOrGroupId) && MemToProj.get(MMS.UserOrGroupId).contains(MilestoneToProj.get(MMS.ParentId)))
   { MsSharesToDelete.add(MMS);}
 }

 for(Milestone1_Project__Share MPS: [SELECT Id, ParentId, UserOrGroupId,AccessLevel,RowCause FROM Milestone1_Project__Share where ParentId in:CustomerProjectIds AND RowCause ='Manual' ])
 {
  if(MemToProj.containsKey(MPS.UserOrGroupId) && MemToProj.get(MPS.UserOrGroupId).contains(MPS.ParentId))
  { PmSharesToDelete.add(MPS);}
 } 

 delete(PmSharesToDelete);
 delete(MSSharesToDelete);
 
}
}