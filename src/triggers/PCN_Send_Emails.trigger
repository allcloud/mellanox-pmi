trigger PCN_Send_Emails on PCN__c (after update) {
    List<PCN__c> pcns_updates = new List<PCN__c>();
    Set<ID> set_ids = new Set<ID>();
    Set<ID> ContactIDs_approversbyOPN = new Set<ID>();
    
    for (PCN__c p : Trigger.New){
        if( (Trigger.oldmap.get(p.id).Send_Emails__c==false && p.Send_Emails__c==true) 
                || (Trigger.oldmap.get(p.id).Send_Update_Revision_Email__c==false && p.Send_Update_Revision_Email__c==true)){
            set_ids.add(p.id);
            //only pull out PCN_Contacts which Contact !+ null and PCN_Notification = true
            List<PCN_Contacts__c> pcn_contacts_updates = new List<PCN_Contacts__c>([select id,Contact__c,PCN__c,Contact__r.PCN_Approver__c,
            																				is_Approver_by_OPN__c  
            																			from PCN_Contacts__c 
                                                                                        where Contact__c!=null 
                                                                                        and Contact_PCN_Notification__c=true 
                                                                                        and PCN__c=:p.ID]);
            
            Set<ID> contact_IDs = new Set<ID>();
            //List<ID> lst_contact_IDs = new List<ID>();
            for (PCN_Contacts__c pcontact : pcn_contacts_updates){
                if(!contact_IDs.contains(pcontact.Contact__c)){
                    contact_IDs.add(pcontact.Contact__c);
                }  
                if(pcontact.is_Approver_by_OPN__c == true)
                	ContactIDs_approversbyOPN.add(pcontact.Contact__c);	             
            }   
            
            if(p.Additional_Contact_1__c != null)
                contact_IDs.add(p.Additional_Contact_1__c);
            if(p.Additional_Contact_2__c != null)
                contact_IDs.add(p.Additional_Contact_2__c);
            if(p.Additional_Contact_3__c != null)
                contact_IDs.add(p.Additional_Contact_3__c);
            if(p.Additional_Contact_4__c != null)
                contact_IDs.add(p.Additional_Contact_4__c);
            if(p.Additional_Contact_5__c != null)
                contact_IDs.add(p.Additional_Contact_5__c);
            if(p.Additional_Contact_6__c != null)
                contact_IDs.add(p.Additional_Contact_6__c);
            if(p.Additional_Contact_7__c != null)
                contact_IDs.add(p.Additional_Contact_7__c);
                
            if(contact_IDs.size()==0)
                return;
                                                                                                
            List<Contact> lst_contacts = new List<Contact>([select id,PCN_Approver__c,Name,Email,domain__c from Contact where id in :contact_IDs]);
            //Add list of contacts which email sent out as an note
            String recipient_detail = '\nPCN Information: \n\n' + 'PCN Name: ' + p.Name +'\n';
            recipient_detail = recipient_detail + 'PCN Description: ' + p.PCN_Description__c +'\n' + 'PCN Type: ' + p.Type__c + '\n' + 'Email Sent Date: ' + System.today() +'\n\n\n';
            recipient_detail = recipient_detail + 'List of contacts receiving the PCN Notification: \n';
            for (Contact cc : lst_contacts)
                recipient_detail = recipient_detail + cc.Email + ' (' + cc.Name + ')' + '\n';
            Attachment email_list = new Attachment();
            email_list.ContentType = 'txt';
            email_list.Body = Blob.valueOf(recipient_detail);
            email_list.Name = String.valueOf('Email-Recipient-List.txt');
            email_list.ParentId = p.ID; 
            insert email_list;
            //                        
            //find attachments
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            //Attachment to send to sharepoint since file name to sharepoint should be PCN names
            List<Messaging.Emailfileattachment> fileAttachments_Sharepoint = new List<Messaging.Emailfileattachment>();
            //
            List<Attachment> lst_attachments = new List<Attachment>();
            lst_attachments = [select Name, Body, BodyLength from Attachment 
                                where ParentId = :p.ID and ContentType = 'application/pdf' order by LastModifiedDate desc limit 1];
			//Add to the list to send to sharepoint side
			for (Attachment a : lst_attachments){  // Add to attachment file list  
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();  
                    efa.setFileName(p.Name); 
                    efa.setBody(a.Body); 
                    fileAttachments_Sharepoint.add(efa);
			}
			//                        
            if(p.Include_Contact_List_when_sending_emails__c==true){
                lst_attachments.add(email_list);
                
                for (Attachment a : lst_attachments){  // Add to attachment file list  
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();  
                    efa.setFileName(a.Name); 
                    efa.setBody(a.Body); 
                    fileAttachments.add(efa);
                }
            }
            else {
                for (Attachment a : lst_attachments){  // Add to attachment file list  
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();  
                    efa.setFileName(a.Name); 
                    efa.setBody(a.Body); 
                    fileAttachments.add(efa);
                }
            }
            //
            List<Messaging.SingleEmailMessage> lst_mail = new List<Messaging.SingleEmailMessage>();
            boolean send_fyi_emails = false;
            boolean send_approvalRequire_emails = false;
            boolean send_approvalRequire_fyicontact_emails = false;
            //for (ID iid : contact_IDs){
            for (Contact c : lst_contacts){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(c.ID);
                mail.setSaveAsActivity(false);
                if(p.Send_Update_Revision_Email__c == true) {
                    //Use revision template
                    mail.setTemplateID('00X5000000199x0');
                }
                else if (p.Send_Emails__c==true && p.Send_Update_Revision_Email__c == false){ //use normal email process templates
					if(p.Type__c == 'Advanced Notification') {
						mail.setTemplateID('00X50000001AWd5');  //Advanced Notification type	
					}
					else {
	                    //PCN is approver by OPN, and Contact is marked as an approver Advanced Notification
	                    if (p.Type__c=='Approval Required' && p.Approvers_By_OPN__c == true && ContactIDs_approversbyOPN.contains(c.ID)){
	                        //Production: 00X50000001824Y
	                        mail.setTemplateID('00X50000001824Y');
	                        send_approvalRequire_emails = true;
	                    }
	                    //PCN is approver by OPN, and Contact is NOT marked as an approver  
	                    else if (p.Type__c=='Approval Required' && p.Approvers_By_OPN__c == true && !ContactIDs_approversbyOPN.contains(c.ID)){
	                        //Production: 00X5000000184Un
	                        mail.setTemplateID('00X5000000184Un');
	                        send_approvalRequire_fyicontact_emails = true;
	                    }
	                    else if (p.Type__c=='Approval Required' && p.Approvers_By_OPN__c == false && c.PCN_Approver__c == 'Yes'){
	                        //Production: 00X50000001824Y
	                        mail.setTemplateID('00X50000001824Y');
	                        send_approvalRequire_emails = true;
	                    }  
	                    else if (p.Type__c=='Approval Required' && p.Approvers_By_OPN__c == false && c.PCN_Approver__c != 'Yes'){
	                        //Production: 00X5000000184Un
	                        mail.setTemplateID('00X5000000184Un');
	                        send_approvalRequire_fyicontact_emails = true;
	                    }                   
	                    else {
	                        //Production: 00X50000001824O
	                        mail.setTemplateID('00X50000001824O');
	                        send_fyi_emails = true;
	                    } 
					} //end else
                }
                                  
                mail.setWhatId(p.ID);
                if(fileAttachments!=null && fileAttachments.size()>0)
                    mail.setFileAttachments(fileAttachments);
                lst_mail.add(mail);
            }
            //send a copy to Basmat & Galit & Noa & Zmora Fefer
            //Galit - 0035000001v4plZ - TEST send to Galit B only
            //Set<ID> MLNX_copy_Contact_IDs = new Set<ID>{'0035000001v4plZ'};
            Set<ID> MLNX_copy_Contact_IDs = new Set<ID>{'00350000021fl7I','0035000001v4plZ','00350000015SskE','0035000002TzGex'};
            //Contact IDs to sent to sharepoint PCN email alias, Khoa test=0035000001r79JV
            Set<ID> MLNX_copy_Sharepoint = new Set<ID>{'0035000002bH6c0','0035000001r79JV'};
            for (ID iid : MLNX_copy_Sharepoint){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(iid);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID('00X50000001sPeZ'); //generic template for alerts to Sharepoint
                    mail.setWhatId(p.ID);
                    if(fileAttachments_Sharepoint!=null && fileAttachments_Sharepoint.size()>0)
                        mail.setFileAttachments(fileAttachments_Sharepoint);
                    lst_mail.add(mail);
            }
            // end copy to Sharepoint
            //send revision emails - 1 template only
            if(p.Send_Update_Revision_Email__c == true) {
                for (ID iid : MLNX_copy_Contact_IDs){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(iid);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID('00X5000000199x0');
                    mail.setWhatId(p.ID);
                    if(fileAttachments!=null && fileAttachments.size()>0)
                        mail.setFileAttachments(fileAttachments);
                    lst_mail.add(mail);
                }
            }
            //send advanced notification to Galit/Noa/Basmat
            if(p.Type__c == 'Advanced Notification' && p.Send_Emails__c == true && p.Send_Update_Revision_Email__c == false) {
				for (ID iid : MLNX_copy_Contact_IDs){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(iid);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID('00X50000001AWd5');
                    mail.setWhatId(p.ID);
                    if(fileAttachments!=null && fileAttachments.size()>0)
                        mail.setFileAttachments(fileAttachments);
                    lst_mail.add(mail);
                }
			}  
            if (send_fyi_emails == true){
                for (ID iid : MLNX_copy_Contact_IDs){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(iid);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID('00X50000001824O');
                    mail.setWhatId(p.ID);
                    if(fileAttachments!=null && fileAttachments.size()>0)
                        mail.setFileAttachments(fileAttachments);
                    lst_mail.add(mail); 
                }           
                
            }
            if (send_approvalRequire_emails == true){
                for (ID iid : MLNX_copy_Contact_IDs){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(iid);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID('00X50000001824Y');
                    mail.setWhatId(p.ID);
                    if(fileAttachments!=null && fileAttachments.size()>0)
                        mail.setFileAttachments(fileAttachments);
                    lst_mail.add(mail); 
                }               
                //Note n = new Note(ParentID=p.ID,Title='Send Email Confirmation');
                //n.Body = 'Email Approval Required PCN Sent On: ' + System.today();
                //lst_notes.add(n);   
            }
            if (send_approvalRequire_fyicontact_emails == true){
                for (ID iid : MLNX_copy_Contact_IDs){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(iid);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID('00X5000000184Un');
                    mail.setWhatId(p.ID);
                    if(fileAttachments!=null && fileAttachments.size()>0)
                        mail.setFileAttachments(fileAttachments);
                    lst_mail.add(mail); 
                }               
            }
            Messaging.SendEmailResult[] r = Messaging.sendEmail(lst_mail);
            
        }
    }
    //
    if (set_ids.size()>0){
        pcns_updates = [select id,Send_Emails__c,Send_Email_Date__c,Send_Revision_Email_Date__c,Send_Update_Revision_Email__c,Publish_Date__c 
        					from PCN__c where id in :set_ids];
        for (PCN__c p : pcns_updates){
			if(p.Send_Emails__c == true && p.Send_Update_Revision_Email__c == false) {
	            p.Send_Email_Date__c = System.today();
	            p.Publish_Date__c = System.today();
			}
			else if (p.Send_Update_Revision_Email__c == true) {
				p.Send_Revision_Email_Date__c = System.today();
			}
        }       
        if(pcns_updates != null && pcns_updates.size()>0)
            update pcns_updates;
    }
    //           
}