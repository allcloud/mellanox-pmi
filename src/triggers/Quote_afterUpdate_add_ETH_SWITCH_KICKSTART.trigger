trigger Quote_afterUpdate_add_ETH_SWITCH_KICKSTART on Quote (after update) {
	//Trigger 1 - Set flag ETH_Quote__c
	Quote quote_to_update;
	ID quoteID;
	
	if (!Quote_ENV.updateQuote_ETH_Switch_check) {
        Quote_ENV.updateQuote_ETH_Switch_check=true;
        Map<ID,boolean> map_quoteID_ETH_Required = new Map<ID,boolean>();
        Set<ID> qIDs = new Set<ID>();
        List<Quote> all_quotes = new List<Quote>();
        List<Quote> quotes_updates = new List<Quote>();
        for (Quote q : Trigger.New){
        	qIDs.add(q.ID);
        	
        	if (q.ETH_Switch_Total__c!=null && q.ETH_Switch_Total__c>0){
				map_quoteID_ETH_Required.put(q.ID,true);
            }
            else {
            	map_quoteID_ETH_Required.put(q.ID,false);
            }
System.debug('...KN Quote trigger q.ETH_Switch_Total__c===' + q.ETH_Switch_Total__c);
System.debug('...KN Quote trigger map_quoteID_ETH_Required===' + map_quoteID_ETH_Required.get(q.id));            
            
        }
        if (qIDs.size()==0)
        	return;
        
        all_quotes = [select ID, ETH_Switch_Quote__c,QuoteNumber from Quote where ID in :qIDs];
        
        for (Quote qq : all_quotes){

        	if (map_quoteID_ETH_Required.get(qq.ID)!=null && map_quoteID_ETH_Required.get(qq.ID)!=qq.ETH_Switch_Quote__c){
        		qq.ETH_Switch_Quote__c = map_quoteID_ETH_Required.get(qq.ID);
        		quotes_updates.add(qq); 	
        	}    
       	    		
        }
        if(quotes_updates.size()>0)
        	update quotes_updates;
	}
	// Trigger 2 - ADD ETH KICKSTART
	// Need to change ID to production when deployed
	
	Set<ID> quotes_IDs = new Set<ID>();
    List<Quote> quotes = new List<Quote>();
    //Add EDR KICKSTART OPN: GPS-ETH-KICKST-6
    
    //ID ETH_KICKSTART_ID = '01t50000003Hw8o'; //producttion ID
    ID ETH_KICKSTART_ID = '01tR0000002Scj7'; //sandbox ID
    
    Integer Qty_ETH_KICKSTART = 1;
    Set<ID> set_ProductIDs = new Set<ID>();
    set_ProductIDs.add(ETH_KICKSTART_ID);
    //
    Set<Id> set_PriceBookID = new Set<Id>();
    Set<Id> set_PriceBookID_GPS = new Set<Id>();
		
    for (Quote q : Trigger.New){
		if (Trigger.oldmap.get(q.ID).ETH_Switch_Quote__c==false && q.ETH_Switch_Quote__c==true){
			quotes_IDs.add(q.id);
			quotes.add(q);
			set_PriceBookID_GPS.add(q.Pricebook2ID);
			set_PriceBookID.add(q.Pricebook2ID);
			set_PriceBookID.add(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c));	
			quoteID = q.ID;
		}
		//KN - 03-26-15
		//Address Lior O question, removing KICKSTART then add another EDR then it should re-add KICKSTART
		else if (q.ETH_Switch_Quote__c==true && q.ETH_Switch_Total__c != null && Trigger.oldmap.get(q.ID).ETH_Switch_Total__c != null
					&& q.ETH_Switch_Total__c > Trigger.oldmap.get(q.ID).ETH_Switch_Total__c ){
			quotes_IDs.add(q.id);
			quotes.add(q);
			set_PriceBookID_GPS.add(q.Pricebook2ID);
			set_PriceBookID.add(q.Pricebook2ID);
			set_PriceBookID.add(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c));
			quoteID = q.ID;	
		}
		//
	}
    if (quotes.size()==0)
    	return;	
    
    Map<ID,PricebookEntry> map_PricebookEntry = new Map<ID,PricebookEntry>([select id,Pricebook2ID,Product2ID,UnitPrice from PricebookEntry 
                                                                               where Product2Id in :set_ProductIDs 
                                                                               and Pricebook2ID in :set_PriceBookID and isActive=true]);
    Map<ID,Map<ID,PricebookEntry>> map_prodID_PricebookID_PBEntryID = new Map<ID,Map<ID,PricebookEntry>>();
    Map<ID,PricebookEntry> map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
    for (PricebookEntry pbentry : map_PricebookEntry.values()){
    	if(map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID)==null){
	        map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
	        map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
	        map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
		}
    	else {
    		map_PricebookID_PBEntryID = map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID);
        	map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
        	map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
    	}
	}   
	// this map is used to check for duplicated
	List<QuoteLineItem> lst_ETHKICKSTART_QuoteLineItem = new List<QuoteLineItem>([select id,PricebookentryID,Pricebookentry.Pricebook2Id,QuoteID from QuoteLineItem 
																				where QuoteID in :quotes_IDs
																				and Product__c = 'GPS-ETH-KICKST-6' 
																				and Pricebookentry.Pricebook2Id in :set_PriceBookID_GPS]);
    Map<ID,List<QuoteLineItem>> map_QuoteID_Quotelines_KICKSTART = new Map<ID,List<QuoteLineItem>>();
    List<QuoteLineItem> tmp_qlines = new List<QuoteLineItem>();
    for (QuoteLineItem qline : lst_ETHKICKSTART_QuoteLineItem){
    	if(map_QuoteID_Quotelines_KICKSTART.get(qline.QuoteID)==null){
        	tmp_qlines = new List<QuoteLineItem>();
			tmp_qlines.add(qline);
			map_QuoteID_Quotelines_KICKSTART.put(qline.QuoteID,tmp_qlines);				        			
        }
        else{
        	tmp_qlines = map_QuoteID_Quotelines_KICKSTART.get(qline.QuoteID);
        	tmp_qlines.add(qline);
			map_QuoteID_Quotelines_KICKSTART.put(qline.QuoteID,tmp_qlines);
        }
    } 
    //  
    List<QuoteLineItem> lst_QuoteLineItemToInsert = new List<QuoteLineItem>();
	List<QuoteLineItem> lst_QuoteLineItemToDelete = new List<QuoteLineItem>();
    QuoteLineItem qline;
    for (Quote qq : quotes){
		if(ETH_KICKSTART_ID!=null && map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID)!=null 
							&& map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID).get(qq.Pricebook2Id) != null )
		{    		
    		qline = new QuoteLineItem (QuoteID=qq.ID,
    							   PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID).get(qq.Pricebook2Id).ID,
    							   Quantity=Qty_ETH_KICKSTART,
		                           UnitPrice=map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID).get(qq.Pricebook2Id).UnitPrice,
		                           New_Price__c=map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID).get(qq.Pricebook2Id).UnitPrice,
		                           supportOPN_by_mapping__c=true);
			//Reset pricing based on Pricing drop-down Quotes
			if(Quote_ENV.PriceBookMap!=null && Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)!=null
				&& map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c))!=null) {
					qline.UnitPrice = map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice;
					qline.New_Price__c=map_prodID_PricebookID_PBEntryID.get(ETH_KICKSTART_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice;		
			}
			
			lst_QuoteLineItemToInsert.add(qline);
			
			if(map_QuoteID_Quotelines_KICKSTART.get(qq.ID) != null){
				for (QuoteLineItem qli : map_QuoteID_Quotelines_KICKSTART.get(qq.ID)){
			    	if (qli.PricebookEntryId == qline.PricebookEntryId)
			        	lst_QuoteLineItemToDelete.add(qli);	
			    }
			}
		}		                       
    }
    //Insert GPS first, then delete, order is important 
    if(lst_QuoteLineItemToInsert.size()>0){
    	insert lst_QuoteLineItemToInsert;
		if(quoteID != null) {
	    	quote_to_update = [select GPS_Got_Deleted__c from Quote where id = :quoteID];
	    	quote_to_update.GPS_Got_Deleted__c = '';
	    	update quote_to_update;
		}
    }
    
	if(lst_QuoteLineItemToDelete.size()>0)
        delete lst_QuoteLineItemToDelete;
	
}