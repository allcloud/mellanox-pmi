trigger update_contract_on_rollUp1 on Contract2__c (after update) {

List<ID> Roll_ContId  = new List<ID>();
List<Contract2__c> Roll_Cont  = new List<Contract2__c>();
map<id,id> ContractToRollTo = new map<id,id>();
List<Id> NewContIds = new List<Id>();

Map<Id,Contract2__c> NewCont = new Map<Id,Contract2__c>();


for(Contract2__c Cont:trigger.new)
{ 
 
if(Cont.Contract_Renew_RollUp_to__c != NULL && trigger.oldmap.get(Cont.id).Contract_Renew_RollUp_to__c == NULL)
  { Roll_ContID.add(Cont.id);
    Roll_Cont.add(Cont);
    NewContIds.add(Cont.Contract_Renew_RollUp_to__c);
    ContractToRollTo.put(Cont.id,Cont.Contract_Renew_RollUp_to__c);
  }
}

  
   
   
for(Contract2__c cnt2:[select id, Contract_Renew_RollUp_from__c from contract2__c where id in:NewContIds])

 {NewCont.put(cnt2.id,cnt2);}

for(Contract2__c Cont1:Roll_Cont)
{ 
 if(NewCont.containsKey(Cont1.Contract_Renew_RollUp_to__c))
 { NewCont.get(Cont1.Contract_Renew_RollUp_to__c).Contract_Renew_RollUp_from__c = Cont1.id; }        
  
} 
 if( !Roll_ContID.isempty())
 {                                                                                                                   
   AssetReassignment reassign = new AssetReassignment();
   reassign.query= 'select id,name , ASSET_FAMILY__C,  ASSET_TYPE__C, ASSET_KIND__C, BACKLOG__C, CITY_STATE__C,  CITY__C,COMMENTS_SPECIAL_HANDLING__C,' + 
 'CONTRACT_YEARS__C,  COUNTRY__C, CUST_PART_NUMBER__C,    CUSTOMER_CATEGORY_1__C, CUSTOMER_CATEGORY_3__C, CUSTOMER_CATEGORY_4__C, CUSTOMER_CATEGORY_5__C,'+
 'CUSTOMER_CATEGORY_6__C, CUSTOMER_NAME__C,   CUSTOMER_NO__C, CUSTOMER_S_PURCH_ORD__C,    DATE_LOANED__C, DATE_REQUESTED__C,  DATE__C,    DOC_MONTH__C,'+
 'DOC_QUTER__C,   DOC_YEAR__C,    DOCUMENT__C,    EOL_CHECK__C,   EXTENDED_WARRANTY_YEARS__C, FAX_NUMBER__C,  INTERNAL_STD_COST__C,'+ 
  'LN__C,  LOANER_SAMPLE_TYPE__C,  LOCATION__C,    MELLANOX_CONTACT__C,    MELLANOX_DEPARTMENT__C, NOTIFY_DATA__C, OLD_ID__C,  OPPORTUNITY_NUMBER__C,'+  
 'ORD_MONTH__C,   ORD_QUT__C, ORD_YEAR__C,    ORDER__C,   ORDER_TYPE__C,  PART_DESCRIPTION__C,    PART_NUMBER__C, PAYMENT_TERMS__C,   PENDING_RETURN_DATE__C, PHONE_NUMBER__C,'+      
  'PRICE__C,   PRODUCT_FAMILY__C,  QTY_FACTORY_UNITS__C,   QUANTITY_SHIPPED__C,    QUANTITY_TOTAL__C,  REFERENCE_MELLANOX_SO__C,   REMARKS__C, RENEWAL_CUSTOMER_S_PURCH_ORD__C,'+     
  'RENEWAL_ORDER__C,   RENEWAL_STATUS__C,  SAA14RMACOUNT__C,   SAA3_RMA_PART__C,   SAA_IGNORE__C,  SLS1__C,    SLS2__C,    SLS3__C,    SLS4__C,    SERIAL_NUMBER__C,   SERIAL_NUMBER_S__C,'+  
  'SHIP_TO_ADDRESS__C, SITE__C,    STATE__C,   STATUSDESCRIPTION__C,   STATUS__C,  STREET_ADDRESS__C,  UNDER_CONTRACT__C,  WRITE_OFF__C,   ZIP_CODE__C,    EXPECTED_SHIP_DATE__C,'+  
  'SHIP_STATUS__C, SHIP_TO_CUSTOMER__C,  MLNX_ASSET__C, Ship_Account__c, Ship_to_Customer_Account__c,Warranty_months__c ,IS_FRU__c, Parent_Serial__c,Original_Customer_PO__c,'+ 
  'Oracle_key__c,Contract2__c,Moved_from_contract__c,RMA_Replaced_by_serial__c,RMA_Returned_on_Order__c FROM Asset2__c WHERE Is_support__c = 0 AND contract2__c in' + ENV.getStringIdsForDynamicSoql(Roll_ContID);          
    
  reassign.Origin =  'RollUp';    
  reassign.NewContract =  ContractToRollTo;               
  ID batchprocessid = Database.executeBatch(reassign);
                                                                                                  
   update ( NewCont.values());       
  }     
}