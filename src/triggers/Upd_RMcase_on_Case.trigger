trigger Upd_RMcase_on_Case on Case (after update) {

map<id,string> CaseIds = new map<id,string>();
list<RMcase__c> RMCases = new list<RMCase__c>();
list<RMpriority__c> RMpriority = new list<RMpriority__c>();
list<id> UpdCases = new list<id>();
//map<id,string> CaseIds = new map<id,string>();
for(Case cs:trigger.new)
{
  if(cs.priority != trigger.oldmap.get(cs.id).priority)
   {  
     CaseIds.put(cs.id, cs.priority);
    }
    
  if( cs.AccountId != trigger.oldmap.get(cs.id).AccountId  ||
      cs.AE_Engineer__c != trigger.oldmap.get(cs.id).AE_Engineer__c || 
      cs.Requested_Delivery_Date__c != trigger.oldmap.get(cs.id).Requested_Delivery_Date__c ||
      cs.OwnerId != trigger.oldmap.get(cs.id).OwnerId ||
      cs.State__c != trigger.oldmap.get(cs.id).State__c||
      cs.priority != trigger.oldmap.get(cs.id).priority)
      
    {  UpdCases.add(cs.id); }                   
 }
 
 if(!CaseIds.isempty())   
 {  
   RMCases = [select id, RMpriority__c,sfcase__c,priority__c,flag__c   from RMcase__c where sfcase__c in: CaseIds.keySet()] ;
   RMpriority = [select RM_id__c, RM_Priority__c,SF_priority__c from RMpriority__c ];
 }
 
 if(!UpdCases.isempty())
 {
 RMCases = [select id, RMpriority__c,sfcase__c,priority__c,flag__c   from RMcase__c where sfcase__c in: UpdCases] ;
 }
 
 

for(RMCase__c RMc:RMCases)
{
   
  if(!caseIds.containsKey(RMc.sfcase__c))
  { 
   RMc.flag__c = true;
  }
                
  for(RMpriority__c rmp:RMpriority)
  {
   system.debug( 'RMP Priority: ' + rmp.SF_priority__c);
   system.debug( 'Saved Priority: ' + CaseIds.get(RMc.sfcase__c));
                      
    if(rmp.SF_priority__c != Null && rmp.SF_priority__c.contains( CaseIds.get(RMc.sfcase__c)))
    {
     if(RMc.RMpriority__c != rmp.RM_id__c)         
     {
      RMc.RMpriority__c = rmp.RM_id__c;
      RMc.Priority__c = rmp.RM_Priority__c;
      RMc.flag__c = true;
     }
    } 
     
  }
  
}
 update(RMCases); 
}