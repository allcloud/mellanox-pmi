trigger Quote_afterInsert_afterUpdate_captureApprovedPrice on Quote (after insert, after update) {
	Set<ID> quote_IDs = new Set<ID>();
	//Use to test pdf generation after approval
	List<ID> qIDs = new List<ID>();
	
	if(Trigger.isUpdate){
		for (Quote q : Trigger.new){
			if(Trigger.oldmap.get(q.id).Status != 'Approved' && q.Status == 'Approved'){
				quote_IDs.add(q.ID);
				qIDs.add(q.ID);
			}
		}
	}
	else {
		for (Quote q : Trigger.new){
			if(q.Status == 'Approved'){
				quote_IDs.add(q.ID);
				qIDs.add(q.ID);
			}
		}	
	}		
	if (quote_IDs.size()==0) return;
	List<QuoteLineItem> qlines_toUpdate = new List<QuoteLineItem>([select id,Approved_Price__c,UnitPrice,Approved_Qty__c,Quantity from QuoteLineItem where QuoteID in :quote_Ids]);
	for (QuoteLineItem qli : qlines_toUpdate){
		qli.Approved_Price__c = qli.UnitPrice;
		qli.Approved_Qty__c = qli.Quantity;
	}
	if(qlines_toUpdate.size()>0)
		update qlines_toUpdate;
	
	//Create PDFs and insert PDFs quotes after approval
	// omit out per Chris Q 03-23-15
	//Quote_PDF_Controller.addPDFAttach(userInfo.getSessionId(), qIDs);
	// 
}