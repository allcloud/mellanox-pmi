trigger Opportunity_Set_Sales_Tracking_Checkbox on Opportunity (before insert, before update, after Insert, after Update) {
	List<Opportunity> optys = new List<Opportunity>();
	List<Period> periods = new List<Period>([select StartDate, EndDate from Period where Type='Quarter' and EndDate >= :System.today() order by EndDate limit 1]);
	Date start_quarter = periods[0].StartDate;
	Date end_quarter = periods[0].StartDate.addmonths(6).adddays(-1);
	Map<ID,ID> map_regis_opty = new Map<ID,ID>();
	
	if(Trigger.isBefore) {	
		for (Opportunity oo : Trigger.new){
			if (oo.RecordTypeId == '01250000000Dtdw' && oo.StageName=='Commit' && oo.CloseDate >= start_quarter && oo.CloseDate <= end_quarter){
				optys.add(oo);
			}	
		}
		//if(optys.size()==0)
		//	return;
		for (Opportunity opty : optys)
			opty.Sales_Tracking__c = true;
	}
	//Trigger 2 - set lookup back to opportunity from registrations
	else if (Trigger.isAfter && Trigger.IsInsert) {
		for (Opportunity o : Trigger.new) {
			if(o.Opportunity_Registration__c != null )
				map_regis_opty.put(o.Opportunity_Registration__c, o.ID);	
		}
	}
	else if (Trigger.isAfter && Trigger.IsUpdate) {
		for (Opportunity o2 : Trigger.new) {
			if(o2.Opportunity_Registration__c != null && o2.Opportunity_Registration__c != Trigger.oldmap.get(o2.ID).Opportunity_Registration__c ) 
				map_regis_opty.put(o2.Opportunity_Registration__c, o2.ID);	
		}
	}
	if(map_regis_opty.size() == 0)
		return;
	List<Distributor_Oppy_Registrations__c> opty_regis = new List<Distributor_Oppy_Registrations__c>([select id, Opportunity__c 
																										from Distributor_Oppy_Registrations__c
																										where id in :map_regis_opty.keyset() ]);
	for (Distributor_Oppy_Registrations__c oreg : opty_regis) {
		if(map_regis_opty.get(oreg.ID) != null )
			oreg.Opportunity__c = map_regis_opty.get(oreg.ID); 
	}															
	
	if(opty_regis != null && opty_regis.size() > 0)
		Database.update (opty_regis, false);
																	
}