trigger BidControl_BeforeInsert_BeforeUpdate_AfterUpdate on Bid_Control__c (after update, before insert, before update) {
	Set<ID> bcn_IDs = new Set<ID>();
	Set<ID> approved_BCN_IDs = new Set<ID>();
	
	if(Trigger.IsInsert){
		for(Bid_Control__c b : Trigger.new) {
			b.Status__c = 'Draft';
		}
	}
	else if(Trigger.isUpdate && Trigger.isBefore) {
		for(Bid_Control__c b : Trigger.new) {
			if(b.Customer_Parent__c != Trigger.oldmap.get(b.id).Customer_Parent__c 
				|| b.End_Customer__c != Trigger.oldmap.get(b.id).End_Customer__c
				|| b.From_Date__c != Trigger.oldmap.get(b.id).From_Date__c
				|| b.To_Date__c != Trigger.oldmap.get(b.id).To_Date__c
				|| b.Price_Book__c != Trigger.oldmap.get(b.id).Price_Book__c)
					b.Status__c = 'Draft';
		}
	}
	else if(Trigger.isUpdate && Trigger.isAfter) {
		for(Bid_Control__c b : Trigger.new) {
			if(b.Price_Book__c != null && b.Price_Book__c != Trigger.oldmap.get(b.id).Price_Book__c) {
				bcn_IDs.add(b.ID);	
			}
			else if(b.Status__c == 'Approved' && b.Status__c != Trigger.oldmap.get(b.id).Status__c ) {
				approved_BCN_IDs.add(b.id);
			}
		}
	}
	if(bcn_IDs.size() == 0 && approved_BCN_IDs.size() == 0)
		return;
	List<Bid_Control_Lines__c> blines = new List<Bid_Control_Lines__c>([select id,List_Price__c,Minimum_Price__c,Requested_Price__c,
																			Line_Approval_Level__c, Product__c,PricebookID__c
																			from Bid_Control_Lines__c
																			where Bid_Control__c in :bcn_IDs]);
	List<Bid_Control_Lines__c> blines_approved = new List<Bid_Control_Lines__c>([select id,Requested_Price__c,Approved_Price__c
																			from Bid_Control_Lines__c
																			where Bid_Control__c in :approved_BCN_IDs]);	
	if(blines.size() == 0 && blines_approved.size()==0)
		return;
	//update Approved_Price to be equal Request_Price
	for (Bid_Control_Lines__c bb : blines_approved) {
		bb.Approved_Price__c = bb.Requested_Price__c;
	}
	if(blines_approved.size() > 0)
		update blines_approved;
	//	
	Set<ID> prods_IDs = new Set<ID>();
	Set<ID> PB_IDs = new Set<ID>();
	for (Bid_Control_Lines__c b : blines) {
		prods_IDs.add(b.Product__c);
		PB_IDs.add(b.PricebookID__c);
	}								
	//build map
	Map<ID,Product2> map_prods = new Map<ID,Product2>([select id, OEM_PL__c from Product2 where id in :prods_IDs]);
	
	List<PricebookEntry> lst_pbe = new List<PricebookEntry>([select id,Product2ID,Pricebook2ID,UnitPrice from PricebookEntry 
																	where Product2ID in :prods_IDs 
																	and Pricebook2ID in :PB_IDs]);
																	
	Map<ID,Map<ID,Double>> map_prodID_PricebookID_Price = new Map<ID,Map<ID,Double>>();
	Map<ID,Double> tmp_map_PricebookID_Price = new Map<ID,Double>();  
	for (PricebookEntry pbe : lst_pbe){
		if (map_prodID_PricebookID_Price.get(pbe.Product2ID)==null){
			tmp_map_PricebookID_Price = new Map<ID,Double>();
			tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
			map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
		}
		else {
			tmp_map_PricebookID_Price = map_prodID_PricebookID_Price.get(pbe.Product2ID);
			tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
			map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
		}
	}																				
	//
	Double min_price;
	for (Bid_Control_Lines__c b : blines) {
		if(map_prodID_PricebookID_Price.get(b.Product__c) != null && map_prodID_PricebookID_Price.get(b.Product__c).get(b.PricebookID__c) != null)
			b.List_Price__c = map_prodID_PricebookID_Price.get(b.Product__c).get(b.PricebookID__c);
		else
			b.List_Price__c = null;
			
		
		//Minimum Price on Bid Control Lines is a formula field
		if(map_prods.get(b.Product__c) != null && map_prods.get(b.Product__c).OEM_PL__c != null)
			min_price = map_prods.get(b.Product__c).OEM_PL__c;
		else
			min_price = null;
			
		//Set Line Level Approval
		if(min_price != null && b.Requested_Price__c < min_Price)
			b.Line_Approval_Level__c = 2;
		else if (b.List_Price__c != null && b.Requested_Price__c < b.List_Price__c)
			b.Line_Approval_Level__c = 1;
		else
			b.Line_Approval_Level__c = 0;
			
	}
	if(blines.size() > 0)
		update blines;											
}