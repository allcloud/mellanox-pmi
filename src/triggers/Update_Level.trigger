trigger Update_Level on Case (before insert, before update) {
	if(ENV.triggerShouldRun){
		
		ENV.triggerShouldRun = false;
		
		string SupportLevel;
		for(case cs:trigger.new)
			{if(trigger.isinsert || (trigger.isupdate && cs.Problem_Support_Level__c != trigger.oldmap.get(cs.id).Problem_Support_Level__c))
				
				{
					SupportLevel = cs.Problem_Support_Level__c;
					if(SupportLevel != NULL && cs.Problem_Support_Level__c.startsWith('L1')) {cs.Level__c= 'Level1';}        
					if(SupportLevel != NULL && cs.Problem_Support_Level__c.startsWith('L2')) {cs.Level__c= 'Level2';}      
					if(SupportLevel != NULL && cs.Problem_Support_Level__c.startsWith('L3')) {cs.Level__c= 'Level3';} 
				}
			}
			
	}
}