trigger Upd_child_Account_on_parent on Account (after update) {
    
    List<id> UpdParentAccounts = new List<id>();
    List<Account> UpdAccounts = new List<Account>();
    map<Account,id> mapChildToParent = new map<Account,id>();
    map<id,Account> mapParentIdToAccount = new map<id,Account>();
    
    for(Account Acc:trigger.new) {
    
        if(trigger.isupdate  &&  ( Acc.Highest_Expired_contract_rank__c !=trigger.oldmap.get(Acc.id).Highest_Expired_contract_rank__c ||
        Acc.Highest_Active_contract_rank__c !=trigger.oldmap.get(Acc.id).Highest_Active_contract_rank__c )) {
        
            UpdParentAccounts.add(Acc.id);
            mapParentIdToAccount.put( Acc.id,Acc);
        }
    }
    
    
    if(!UpdParentAccounts.isempty() && ENV.Upd_child_AccountTriggerShouldRun) {
    
        for(Account  chiAcc :[select id, parentid, Contract_type_final__c, Highest_Active_contract_rank__c  from account where parentid in :UpdParentAccounts]) {
         
            if( (chiAcc.Highest_Active_contract_rank__c == Null && mapParentIdToAccount.get(chiAcc.parentid).Highest_Active_contract_rank__c != NULL)||
            (chiAcc.Highest_Active_contract_rank__c < mapParentIdToAccount.get(chiAcc.parentid).Highest_Active_contract_rank__c)  ) {
            
                chiAcc.Contract_type_final__c =  mapParentIdToAccount.get(chiAcc.parentid).Contract_type_final__c;
                UpdAccounts.add(chiAcc);
            }
        }
        
         ENV.Upd_child_AccountTriggerShouldRun = false;
    }
    
    if(!UpdAccounts.isempty()) {
   		
   		update(UpdAccounts);
    }
}