trigger Create_Case_from_Call on Call_Center__c (after insert) 
{
 for(Call_Center__c CC: Trigger.new)
  { 
  
 if(CC.RecordTypeId == ENV.CallTypeSupport) 
 { 
 /* if(CC.Existing_case_id__c==NULL)  // create new case
  {  */
    System.debug('DK CC = '+ CC);
    System.debug('DK CC.Call_Center_Customer__c = '+ CC.Call_Center_Customer__c);
    System.debug('DK CC.Call_Center_Customer__c.contact__C = '+ CC.Call_Center_Customer__r.Contact__c);   
      
        /*database.DMLOptions dmo = new database.DMLOptions();    
        dmo.AssignmentRuleHeader.UseDefaultRule= true;*/
          
        Case CallCase = new Case();
        if(CC.Account__c == NULL || CC.Account__c == ENV.UnknownAccount || CC.Account__c == ENV.UnknownUSAccount || CC.Account__c == ENV.UnknownEMEAAccount  )
        {CallCase.OwnerId = ENV.AdminQueue;
         CallCase.RecordTypeId = ENV.CaseTypeAdmin;} 
       else{ if((CC.Contract_Type__c.contains('Expired')|| CC.Contract_Type__c == 'No') &&          
               CC.Strategic_Account__c == 'False' && CC.Is_Design_Customer__c == 'No')           
           {CallCase.OwnerId = ENV.AdminQueue;   
            CallCase.RecordTypeId = ENV.CaseTypeAdmin;}
        else{                                
        if(CC.ST1_Assigned__c==True)
        {CallCase.OwnerId = ENV.ST1;}
        if(CC.ST1_Assigned__c!=True )
        {CallCase.OwnerId = ENV.SystemQueue;}
        }
       }
  // CallCase.setOptions(dmo);    
        CallCase.Customer_Source__c = CC.Customer_source__c;
        CallCase.Origin = 'Phone'; 
        CallCase.Call_Customer_phones__c = CC.Contact_Phones__c;
        if(CC.Contact__c != Null)
        {
             CallCase.Contactid = CC.Contact__c;
        }
        else
        { 
               Contact NC = new Contact();
               NC.FirstName = CC.Customer_First_name__c; 
               NC.LastName = CC.Customer_Last_name__c;
               NC.Email = CC.Customer_email__c;
               NC.Phone = CC.Contact_Phones__c;
             if(CC.Account__c != NULL)
                {NC.AccountId= CC.Account__c;}
                else{ NC.AccountId= ENV.UnknownAccount; } // Unknown test
               insert(NC);
               CallCase.ContactId = NC.id;
        } 
          
        if(CC.Account__c != NULL)
        {
             CallCase.AccountId = CC.Account__c;
        }
        else
        {
            CallCase.AccountId = ENV.UnknownAccount; 
        }    
          
              CallCase.Call_Center__c = 'Support Case';      
              if(CC.Existing_case_id__c==NULL)
                 { CallCase.Subject = CC.subject__c;
                   CallCase.Description = CC.Problem_description__c + '\n'+ CC.Existing_Case_Description__c; 
                  }
              if(CC.Existing_case_id__c!=NULL)
                {  CallCase.Subject = 'Call center opened case for existing case #'+ CC.Existing_case__c +      
                                         '. Please delete this case after handeling the existing one';
                  CallCase.Description = CC.subject__c + '\n' +             
                   CC.Existing_Case_Description__c +'\n'+ CC.Problem_description__c; 
                }
             CallCase.Customer_Source__c = CC.Customer_Source__c;
                            
              
			 if (CC != null && CC.impact_net__c != null) {
	              
	              if (CC.impact_net__c.contains('Critical')) {
	              	
	              	 CallCase.priority = 'Fatal';
	          	  
	          	  } 
	          	   
	              if (CC.impact_net__c.contains('High')) {
	              	 
	              	 CallCase.priority = 'High';
	          	  } 
	          	 
	              if (CC.impact_net__c.contains('Medium')) { 
	              	
	              	CallCase.priority = 'Medium';
	              } 
	               
	              if (CC.impact_net__c.contains('Low')) { 
	              	
	              	CallCase.priority = 'Low';
	              } 
			 }  
                
         
               
             CallCase.Call_Center_Parent__c = cc.id;               
             CallCase.SN__c = CC.Serial_Number__c;             
             insert (CallCase);
        
 //     } // no existing case
    
   if(CC.Existing_case_id__c!=NULL)  // add comment to existing case

    {  casecomment CallCom = new casecomment();     
        CallCom.commentBody =  'Comment from CALL CENTER: Customer called for existing case' + '\n'+ 'Subject: ' + CC.subject__c + '\n'+ 'Description: ' + CC.Problem_description__c; 
        CallCom.ParentId = CC.Existing_case_id__c;
        CallCom.IsPublished = True;
        insert(CallCom);

     } // existing case
    
    } //support case
   } // for 
    
}