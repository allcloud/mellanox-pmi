trigger RMcase_before_update on RmCase__c (before update) {

list<string> AssigneesIDs = new list<string>();
list<string> AllAssigneesIDs = new list<string>();
map<id,string> map_case_assignees = new map<id,string>();
list<RMpriority__c> RMpriority = new list<RMpriority__c>();
map<string,string> map_RMusers_names = new map<string,string>();
list<RMcase__c> RMcases = new list<RMcase__c>();



for(RMcase__c rmc:trigger.new)
{
  if(trigger.isupdate && rmc.lastmodifiedById == ENV.IntegrationUser && rmc.Assignee_selected__c != trigger.oldmap.get(rmc.id).Assignee_selected__c && rmc.Assignee_selected__c != NULL )
   {
    map_case_assignees.put(rmc.id,rmc.Assignee_selected__c);
    AssigneesIDs= rmc.Assignee_selected__c.split(',');
    AllAssigneesIDs.addAll(AssigneesIDs);
    RMcases.add(rmc);
   }  
    
  if(trigger.isupdate && rmc.lastmodifiedById == ENV.IntegrationUser && rmc.Assignee_selected__c != trigger.oldmap.get(rmc.id).Assignee_selected__c && (rmc.Assignee_selected__c == '' || rmc.Assignee_selected__c == NULL) )
   { rmc.cc_list__c = '';}
                  
 } 

map<id,RMusers__c>  map_RMusers = new map<id,RMusers__c>([select RM_ID__c, name from RMusers__c where RM_ID__c in:AllAssigneesIDs]);

if(!map_RMusers.isempty())
{
  for(RMusers__c rmu:map_RMusers.values())
  {
   map_RMusers_names.put(rmu.RM_ID__c,rmu.name);
  }
  
 for(RMcase__c rmcs: RMcases)
 {
  string AssigneesNames =''; 
  for(string Aid:map_case_assignees.get(rmcs.id).split(','))
  {
   if(map_RMusers_names.containsKey(Aid))
   {
    if(AssigneesNames.length() == 0){ AssigneesNames = map_RMusers_names.get(Aid);}
    else {AssigneesNames = AssigneesNames + ',' + map_RMusers_names.get(Aid);}
   } 
  }
  
   rmcs.Cc_list__c = AssigneesNames; 
 
  } // for
 } //if  
}