trigger trg_Category_Sync on Category_Sync__c (after insert, before insert) {
	
	if (trigger.isafter && trigger.isInsert) {
		
		  Category_Sync_Handler handler = new Category_Sync_Handler();
		  handler.assignCategoriesBySharePointId(trigger.New);
	}

}