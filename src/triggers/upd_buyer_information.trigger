trigger upd_buyer_information on Contract2__c (before insert) {

map<string,BBB_Orders__c> mapOrderToBBB = new map<string,BBB_Orders__c>();
list<integer> newContracts = new list<integer>();
list<contract2__c> updContracts = new list<contract2__c>();
String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}​\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z​]{2,4}|[0-9]{1,3})';
Pattern MyPattern = Pattern.compile(emailRegex);

for(contract2__c con:trigger.new)
{
if(con.order__c != NULL)
 {
 newContracts.add(integer.ValueOf(con.order__c));
 updContracts.add(con);
 }
}

for(BBB_Orders__c bb:[select id,Sales_Order_Number__c, buyer_email__c,BILL_TO_ACCOUNT__c from BBB_Orders__c where Sales_Order_number__c in :newContracts])
{
 if(bb.buyer_email__c != NULL && bb.Sales_Order_Number__c != Null )
 mapOrderToBBB.put(string.ValueOf(bb.Sales_Order_number__c),bb);
}

for(Contract2__c cont:updContracts)
{

if( mapOrderToBBB.containsKey(cont.order__c))
 { 
 
 
Matcher MyMatcher = MyPattern.matcher(mapOrderToBBB.get(cont.order__c).buyer_email__c);
                            
if(MyMatcher.matches())
 {  cont.buyer_email__c =  mapOrderToBBB.get(cont.order__c).buyer_email__c;}
   cont.buyer_account__c =  mapOrderToBBB.get(cont.order__c).BILL_TO_ACCOUNT__c;
  }

 }

}