trigger PCN_Exclude_Companies on PCN__c (after update) {
	Set<ID> iids = new Set<ID>();
	String list_companies;
	List<String> companies = new List<String>();
	Set<String> set_companies = new Set<String>();
	List<PCN_Contacts__c> pcn_contacts_deletes = new List<PCN_Contacts__c>();
	String tmp;
	
	for (PCN__c p : Trigger.new) {
		if(p.Exclude_Companies__c != null && p.Exclude_Companies__c != '' && p.Exclude_Companies__c != Trigger.oldmap.get(p.ID).Exclude_Companies__c) {
			iids.add(p.id);
			list_companies = p.Exclude_Companies__c;
			//should only run once
			break;
		}
	}
	
	//only allow logics to be applied one time
	if (iids.size() == 0 || iids.size() > 1)
		return;
	if(list_companies != null) {		
		companies = list_companies.split(',');	
		
		for (String s : companies) {
			tmp = s.trim() + '%';
			set_companies.add(tmp);
		}	
		pcn_contacts_deletes = new List<PCN_Contacts__c>([select id from PCN_Contacts__c where PCN__c in :iids and Company__c like :set_companies]);
	}

	if(pcn_contacts_deletes != null && pcn_contacts_deletes.size() > 0)
		delete pcn_contacts_deletes;
}