trigger Attach_RMA_to_Contact on RMA__c (before insert) {
for( RMA__c R:trigger.new){
 if(R.Contact__c==NULL && R.E_Mail__c!=NULL)
 { List<Contact> RMA_Con = [select id, AccountId, Account.name from Contact where email=:R.E_Mail__c];   
    if(!RMA_Con.isEmpty())
    {R.Contact__c= RMA_Con[0].id;                    
     R.Account_Name__c=RMA_Con[0].AccountId;
     R.Account_Name_text__c=RMA_Con[0].Account.name;    
     List<User> RMA_User =[select id,name from User where ContactId=:RMA_Con[0].id];     
     if(!RMA_User.isempty())
     {R.OwnerId = RMA_User[0].id; }      
      
     }
 }
}
}