trigger ContentVersion_trg on ContentVersion (after insert, after update, before delete, before update, before insert) {
	
	ContentVersionHandler handler = new ContentVersionHandler();    
	
	 if ( trigger.isAfter && (trigger.isUpdate || trigger.isInsert) ) {
		
		handler.prepareMappingOfUsersAndContentVersions(trigger.New, trigger.oldMap);
		handler.createContentUpdatesPerNewContentVersion(trigger.New, trigger.oldMap);
		handler.createFeedItems(trigger.New);
		 
	}
	
	if ( trigger.isBefore) {
		
		if (trigger.isUpdate) {
			
			handler.updateMobileOPNOnOPNUpdate(trigger.New, trigger.oldMap);
		}
		
		else if (trigger.isInsert) {
			
			handler.updateMobileOPNOnOPNUpdate(trigger.New, null);
		}
	}
	
}