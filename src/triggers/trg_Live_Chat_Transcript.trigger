trigger trg_Live_Chat_Transcript on LiveChatTranscript (after insert) {
	
	if (trigger.isAfter && trigger.isInsert) {
		
		cls_trg_Live_Chat_Transcript handler = new cls_trg_Live_Chat_Transcript();
		handler.updateLiveChatTranscriptRelatedCase(trigger.New);  
	}

}