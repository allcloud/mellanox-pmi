trigger Loaner_before_update on Loaner_Request__c (before insert,before update) {
	List<Loaner_Request__c> loaners = new List<Loaner_Request__c>();
	List<Loaner_Comment__c> comments = new List<Loaner_Comment__c>();
	Loaner_Comment__c tmp_comm;
	 
	for (Loaner_Request__c LR : Trigger.new) {
		if (Trigger.isUpdate && LR.Notes_To_Requester__c != '' && LR.Notes_To_Requester__c != Trigger.oldmap.get(LR.id).Notes_To_Requester__c ) {
			loaners.add(LR);
		}	
	}
	if(loaners.size() == 0)
		return;
	for (Loaner_Request__c LR2 : loaners) {
		tmp_comm = new Loaner_Comment__c();
		tmp_comm.Comments__c = LR2.Notes_To_Requester__c;
		tmp_comm.Loaner__c = LR2.id;
		tmp_comm.To_User__c = LR2.Ownerid;
		tmp_comm.Send_Comments_To__c = 'Requester';
		comments.add(tmp_comm);		 	
	}
	
	if (comments.size() > 0)
		Database.insert(comments, false);
}