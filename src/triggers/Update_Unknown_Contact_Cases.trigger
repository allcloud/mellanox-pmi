trigger Update_Unknown_Contact_Cases on Contact (before update) {

List<Contact> unknownContacts = new List<Contact>();
List<id> unknownContactsId = new List<id>();
map<id,List<case>> CaseAccount = new map<id,List<case>>();
List<case> Cases = new List<case>();
for(contact cnt:trigger.new)
{
  if(trigger.oldmap.get(cnt.id).accountid != cnt.accountid 
     && (trigger.oldmap.get(cnt.id).accountid == env.UnknownAccount || trigger.oldmap.get(cnt.id).accountid== env.UnknownEMEAAccount || trigger.oldmap.get(cnt.id).accountid== env.UnknownUSAccount))
  
   { unknownContactsId.add(cnt.id);
     unknownContacts.add(cnt);}
 }  
  
if(!unknownContactsId.isempty())
{

for(case cs:[select id, accountid,contactid,ST1_Memeber__c from Case where contactid in:unknownContactsId ])
 {
  if(!CaseAccount.containsKey(cs.contactid)&& cs!=NULL)
    {List<case> NewCases = new List<case>();
     NewCases.add(cs);                 
     CaseAccount.put(cs.contactid,NewCases);} 
   else      
     { Cases = CaseAccount.get(cs.contactid);    
       Cases.add(cs);           
       CaseAccount.put(cs.contactid,Cases);   
      }                                                               
  }
    
 for( contact cnt1:unknownContacts)
  {   
   if(CaseAccount.containsKey(cnt1.id))                                
   
    { 
       Cases = CaseAccount.get(cnt1.id);  
       for(case cs1:Cases)
        {if(cs1.accountid != cnt1.accountid) cs1.accountid = cnt1.accountid;
         if(userinfo.getuserid() == ENV.ST1) // ST1 member
            {cs1.ST1_Memeber__c = userinfo.getName();}
        }
       update(cases);  
    }
  }
 }
}