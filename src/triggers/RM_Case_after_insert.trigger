trigger RM_Case_after_insert on RmCase__c (after insert,after update) 
{
    list<id> ParentIds           = new List<id>();
    List<RMcase__c> NewRMcases   = new List<RMcase__c>();
    list<id> AddedRMCases        = new List<id>();
    list<id> VersionRMCases        = new List<id>();
    list<case> CasesToUpdate     = new List<case>();
   
    map<id,RMcase__c> Case_toRMcase          = new map<id,RMcase__c>();
    map<Id,AttachmentIntegration__c> map_attachmentToattachmentIntegration = new map<Id,AttachmentIntegration__c>();

    for(RmCase__c RM:trigger.new)
    {
        
        if (trigger.isupdate && 
           RM.name != trigger.oldmap.get(RM.id).name && trigger.oldmap.get(RM.id).name != NULL )
        
        {
            AddedRMCases.add(RM.sfcase__c);
            Case_toRMcase.put(RM.sfcase__c, RM);
        }
        
      if (trigger.isupdate &&  RM.target_version__c != trigger.oldmap.get(RM.id).target_version__c )
      
      {
       VersionRMCases.add(RM.sfcase__c); 
       Case_toRMcase.put(RM.sfcase__c, RM);
      }
      
      if (trigger.isUpdate) {
        
        cls_RMCase handler = new cls_RMCase();
        handler.createFMDiscussionOnRmCaseIsClosed(trigger.New, trigger.OldMap);
      }
    }
   

    // -------------------- Updae RMcase numbers on case ---------------------
    if(AddedRMCases.size()>0)
    {
        CasesToUpdate = [select id, RM_Cases__c from case where id in:AddedRMCases];
        for(case cs:CasesToUpdate)
        {
             if(cs.RM_cases__c == NULL)
                cs.RM_cases__c = Case_toRMcase.get(cs.id).name;
            else if(!cs.RM_cases__c.contains(Case_toRMcase.get(cs.id).name) )
                cs.RM_cases__c = cs.RM_cases__c + ',' +Case_toRMcase.get(cs.id).name;
                             
       /*      if(cs.RM_Assignees__c == NULL)
                cs.RM_Assignees__c = Case_toRMcase.get(cs.id).Assignee__c;
            else
                cs.RM_Assignees__c = cs.RM_Assignees__c + ',' +Case_toRMcase.get(cs.id).Assignee__c;*/
                
        }
    }
    
    
 
   // -------------------- Updae Fix in release version on case ---------------------  
    
     if(VersionRMCases.size()>0)
    {
        CasesToUpdate = [select id, Fix_in_the_release_version__c from case where id in:VersionRMCases];
        for(case cs:CasesToUpdate)
        {
            if(cs.Fix_in_the_release_version__c == NULL)
                cs.Fix_in_the_release_version__c = Case_toRMcase.get(cs.id).target_version__c;
            else
                cs.Fix_in_the_release_version__c = cs.Fix_in_the_release_version__c + ',' +Case_toRMcase.get(cs.id).target_version__c;

         }
    }

    

    //----------------------------------------------------------


    if(CasesToUpdate.size()>0)
        update CasesToUpdate; 
        
        

}