/**************************************************************************************
* Description:
* The trigger can kick-off any Batch Apex class in the org
* You can only kick-off one Batch Job for every record in the BJI object.
***************************************************************************************/
trigger BatchJobInvoker on Batch_Job_Invoker__c (after insert, after update) {

    if(trigger.new[0].JobType__c == 'S2K'){
    	Id batchInstanceId;
    	
    	if (trigger.new[0].Status__c == 'New' && trigger.new[0].isAdvanced__c)
        	batchInstanceId = Database.executeBatch(new S2KBatchJob(trigger.new[0]));
        
        else if (trigger.new[0].Status__c == 'Custom Fields Creation Completed')
        	batchInstanceId = Database.executeBatch(new S2KStandard(trigger.new[0]));
        	
        else if (trigger.new[0].Status__c == 'Duplicate - Rejected'){
        	//TODO: Use an email template, don't HARDCODE message body
        	DateTime d = trigger.new[0].CreatedDate;
			String timeStr = d.format('MMMMM dd, yyyy hh:mm a');
	        String emailMessage = 'Your Solutions to Knowledge (Standard) batch job created ' + timeStr + ' has been REJECTED.';	                                
	        emailMessage += '<br/><br/>Reason: Duplicate batch job currently in progress.';                             
	        
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();	        
	        User submitter = [select Email from User where Id=:trigger.new[0].OwnerId LIMIT 1][0];
	        String[] toAddresses = new String[] {submitter.Email};
	        
	        mail.setToAddresses(toAddresses);
	        mail.setReplyTo('noreply@salesforce.com');
	        mail.setSenderDisplayName('KBApps S2K');
	        mail.setSubject('Solutions to Knowledge (Standard) - REJECTED');
	        mail.setPlainTextBody(emailMessage);
	        mail.setHtmlBody(emailMessage);
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ mail });
        }
        
        else if (trigger.new[0].Status__c == 'Rejected - Use Advanced Option'){
        	//TODO: Use an email template, don't HARDCODE message body
        	DateTime d = trigger.new[0].CreatedDate;
			String timeStr = d.format('MMMMM dd, yyyy hh:mm a');
	        String emailMessage = 'Your Solutions to Knowledge (Standard) batch job created ' + timeStr + ' has been REJECTED.';	                                
	        emailMessage += '<br/><br/>Reason: Article Type - ' + trigger.new[0].Article_Type__c + ' exists.  Please use the Advanced option to selectively copy Solutions to the Article Type.';                             
	        
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();	        
	        User submitter = [select Email from User where Id=:trigger.new[0].OwnerId LIMIT 1][0];
	        String[] toAddresses = new String[] {submitter.Email};
	        
	        mail.setToAddresses(toAddresses);
	        mail.setReplyTo('noreply@salesforce.com');
	        mail.setSenderDisplayName('KBApps S2K');
	        mail.setSubject('Solutions to Knowledge (Standard) - REJECTED');
	        mail.setPlainTextBody(emailMessage);
	        mail.setHtmlBody(emailMessage);
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ mail });
        }
    }
}