trigger trg_Fund_Request on SFDC_MDF__c (after update, before insert, after insert) {
	
	cls_trg_Fund_Request handler = new cls_trg_Fund_Request();
		
	if (trigger.isAfter) {	
		
		if (trigger.isUpdate) {
			
			handler.sendFundRequestToApproval(trigger.new, trigger.oldMap);
			//handler.sendEmailAndInvoiceAttachmentWhenRequsetsApproved(trigger.new, trigger.oldMap);
		}
		
		else if (trigger.isInsert) {
			
			//handler.createMDFShareForMDFRequet(trigger.new);
			handler.sendFundRequestToApproval(trigger.new, null);
		}
	}
	
	else if (trigger.isBefore) {
			
		if (trigger.isInsert) {
			
			system.debug('inside 20');
			//handler.updateFundRequestCAMAndTAM(trigger.New);
		}
	}
}