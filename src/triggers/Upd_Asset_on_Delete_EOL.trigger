trigger Upd_Asset_on_Delete_EOL on EOL__c (after delete) {


List<string> DelProdNums = new List<string>();
for(EOL__c eol:trigger.old)
{DelProdNums.add(eol.name);}

string query1 = 'select id,Part_Number__c,EOL_check__c from Asset2__c where ( Part_Number__c in '+ENV.getStringsForDynamicSoql(DelProdNums)+')';

 
 
   EOLAssetReassignment reassign = new EOLAssetReassignment();
   reassign.query= query1;           
   reassign.isinsert = 0;
   ID batchprocessid = Database.executeBatch(reassign);
}