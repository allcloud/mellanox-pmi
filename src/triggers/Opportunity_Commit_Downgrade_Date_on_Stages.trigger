trigger Opportunity_Commit_Downgrade_Date_on_Stages on Opportunity (after update) {
/*
	List<Opportunity> optys_commit_updates = new List<Opportunity>();
	List<Opportunity> optys_downgrade_updates = new List<Opportunity>();
	Set<ID> commit_IDs = new Set<ID>();
	Set<ID> downgrade_IDs = new Set<ID>();
	
	for (Opportunity o : Trigger.new){
		if(o.RecordTypeId=='01250000000Dtdw' && Trigger.oldmap.get(o.ID).StageName != o.StageName){
			if(o.StageName == 'Commit'){
				//o.Date_Downgraded_New_1__c = null;
				commit_IDs.add(o.id);
			}
			else if (o.StageName=='Pipeline' || o.StageName=='Best Case'){
				//o.Current_Date_Committed__c = null;
				downgrade_IDs.add(o.id);
			}
		}
	}
	if(commit_IDs.size()==0 && downgrade_IDs.size()==0)
		return;
	if(commit_IDs.size()>0){
		optys_commit_updates = [select id,Date_Downgraded_New_1__c from Opportunity where id in :commit_IDs];
		for (Opportunity opty : optys_commit_updates){
			opty.Date_Downgraded_New_1__c = null;
		}
	}
	if(downgrade_IDs.size()>0){
		optys_downgrade_updates = [select id,Current_Date_Committed__c from Opportunity where id in :downgrade_IDs];
		for (Opportunity opty : optys_downgrade_updates){
			opty.Current_Date_Committed__c = null;
		}
	}
	if(optys_commit_updates!=null && optys_commit_updates.size()>0)
		update optys_commit_updates;
	if(optys_downgrade_updates!=null && optys_downgrade_updates.size()>0)
		update optys_downgrade_updates;
*/
}