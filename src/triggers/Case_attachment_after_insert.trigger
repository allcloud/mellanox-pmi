trigger Case_attachment_after_insert on Attachment (after insert) 
{

//The trigger process Public attachments added from MyMellanox. Creates Attachment Integration and updates case fields related to last attachment

  list<Attachment> CustomerAttachment = new List<Attachment>();
  List<id> ParentIds = new List<id>();
  List<id> InternalParentIds = new List<id>();
 // map<id,List<RMcase__c>> CaseAttachments = new map<id,List<RMcase__c>>();
  map<id,Case> CustomerCases = new map<id,Case>();  

  List<AttachmentIntegration__c> NewAIs =new List<AttachmentIntegration__c>();
 // List<RMCase__c> RMcases = new List<RMCase__c>();  
  list<Case> lst_CaseToUpdate = new list<Case>();
  map<Id,Case> map_CaseToUpdate = new map<Id,Case>();
  system.debug('==>trigger.New '+trigger.New);
  for(Attachment Att:trigger.New)
  { 
    String parId = att.ParentId; 
    system.debug('==>Att.CreatedById '+Att.CreatedById);
    system.debug('==>userInfo.getUserType() '+userInfo.getUserType());  
    system.debug('==>userInfo.getUserId() '+userInfo.getUserId());                
    if(parId.startsWith('500')&&( userInfo.getUserType()!= 'Standard' )) // case attachment added from customer portal
    {  
      CustomerAttachment.add(att);       
      ParentIds.add(att.Parentid);   
      system.debug('==>att.Parentid '+att.Parentid);        
    }
  }  
  system.debug('==>ParentIds '+ParentIds);
  if(!ParentIds.isempty())
  {   
           
    //******* find creator for attachments with no Redmine case
    system.debug('===>ParentIds '+ParentIds);   
             
     
     for(Case cs:[Select Id,contactid,contact.name, Next_Attachment_Integration_Number__c  FROM Case Where id in :ParentIds])
      {  
        system.debug('===>cs '+cs);    
        CustomerCases.put(cs.id,cs);                       
      }                                                       
         
    system.debug('===>CustomerCases '+CustomerCases);  
    Decimal index=1;                                             
    for(Attachment Attch:CustomerAttachment)
    {
      system.debug('==>attach'+attch);
      
          
        if(CustomerCases.containsKey(Attch.parentId))          
        {                                          
          AttachmentIntegration__c AI = new AttachmentIntegration__c() ;
          AI.Attachment_id__c = Attch.id;          
          AI.name = Attch.name;  
          AI.size__c = Attch.BodyLength;               
          AI.Case_Id__c = Attch.ParentId; 
          AI.Is_Public__c = true;          
          AI.source__c = 'Customer';          
          AI.sync_flag__c = false;            
          AI.Created_by_contact__c =CustomerCases.get(Attch.parentId).contactid; 
          system.debug('===>Attch.parentId '+Attch.parentId);
          system.debug('===>CustomerCases.get(Attch.parentId) '+CustomerCases.get(Attch.parentId)); 
          if(CustomerCases.get(Attch.parentId)!=null && CustomerCases.get(Attch.parentId).Next_Attachment_Integration_Number__c !=null) 
          {
            AI.Attachment_Number__c = CustomerCases.get(Attch.parentId).Next_Attachment_Integration_Number__c;  
            CustomerCases.get(Attch.parentId).Next_Attachment_Integration_Number__c++; 
           
          }                                      
          newAIs.add(AI);            
        }                
  
                                                                           
    }  
    if(!CustomerCases.Isempty())
    {
      update CustomerCases.values();
    }
    if(!map_CaseToUpdate.IsEmpty())
    {
      //update lst_CaseToUpdate;
      update map_CaseToUpdate.values();
      system.debug('==>map_CaseToUpdate'+map_CaseToUpdate.values());
    }
    if(newAIs.size()>0)
    {
      insert newAIs;     
    }             
  } 
  
      if (trigger.isafter && trigger.isInsert)
        {   system.debug('inside trigger 5');
            cls_trg_Attachment handler = new cls_trg_Attachment();
            handler.checkAttExistsOnRMATrue(trigger.New);
        }
}