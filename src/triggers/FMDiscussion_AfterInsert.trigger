trigger FMDiscussion_AfterInsert on FM_Discussion__c (after insert) 
{ // inna ttttttt fffff
//  hghg
    List <ID> CasesId = new List <ID>();
    //List <ID> FmDiscussion = new List <ID>();
    set <ID> FmDiscussion = new set <ID>();
    List <ID> LastMB = new List <ID>();
     map<ID,string> CaseToCommentCreator = new map<ID,string>();
      map<ID,fm_discussion__c> CaseToLastFM = new map<ID,fm_discussion__c>();
   for(FM_Discussion__c fm: Trigger.new)
    {
        if(fm.Discussion__c!=null || fm.Discussion__c!='') 
         {
            CasesId.add(fm.Case__c);
            FmDiscussion.add(fm.Id); 
            CaseToCommentCreator.put(fm.Case__c,fm.CreatedBy__c); 
            CaseToLastFM.put(fm.Case__c,fm);                              
         }
        
        LastMB.add(fm.LastModifiedById);
    }

    if(CasesId.size()>0)
    {
        Map<ID, List<FM_Discussion__c>> map_caseIdToFMDiscussion = new Map <ID, List<FM_Discussion__c>>();
        
        List <Case> CasesList = [Select c.Send_me_comment__c, c.Is_Public_last_comment__c, c.Owner.Type, c.Owner.Email, c.Owner.Username, c.OwnerId, c.Assignee__r.Email, c.Assignee__c,c.AE_manager_email__c ,c.AE_manager_email_1__c ,c.AE_manager_email_2__c ,
                                (Select Case__c, Discussion__c, Public__c,Exposed_To_FM__c, CreatedDate, LastModifiedById, Id, Createdby__c, CreatedById, CreatedBy.Email, CreatedBy.Username From FM_Discussions__r  Where Id IN: FmDiscussion and  IsDeleted = false),
                                (Select MemberId From TeamMembers)  
                                 From Case c 
                                 Where Id IN: CasesId and IsDeleted=false];
        
        List <Case> CasesListForLatestComm = [Select c.Id, c.FM_Discussion_Field__c,c.comment_created_by__c,
                                             (Select Case__c, LastModifiedBy.Name, Discussion__c, Public__c,Exposed_To_FM__c, CreatedDate, LastModifiedById, Id, CreatedBy__c, CreatedById, CreatedBy.Email, CreatedBy.Username From FM_Discussions__r  Where IsDeleted = false Order By LastModifiedDate DESC)
                                              From Case c 
                                              Where Id IN: CasesId and IsDeleted=false];
        for(Case c: CasesListForLatestComm)
        {
            for(FM_Discussion__c fm: c.FM_Discussions__r)
            {
                if(!map_caseIdToFMDiscussion.containsKey(c.Id))
                {
                    map_caseIdToFMDiscussion.put(c.Id, new List <FM_Discussion__c>{fm});
                }
                else
                {
                    List <FM_Discussion__c> lst_tmpFm = map_caseIdToFMDiscussion.get(c.Id);
                    lst_tmpFm.add(fm);
                    map_caseIdToFMDiscussion.remove(c.Id);
                    map_caseIdToFMDiscussion.put(c.Id, lst_tmpFm);
                }
            }
        }
        
        
        List <Case> lst_caseToUpdate = new List<Case>();
        system.debug('BLAT CasesList: '+ CasesList);                        
        //CreatedBy.Profile.Id!=:Util.ProfileDesignSupportCus and CreatedBy.Profile.Id!=:Util.ProfileSystemSupportCus and CreatedBy.Profile.Id!=:Util.ProfileSystemAdmin
        //List <ID> GroupList = new List <ID>();
        List <ID> UserList = new List <ID>();
        Map <ID, ID> CaseToGroupOwner = new Map <ID, ID>();   
        Map <ID, List<ID>> GroupToMembers = new Map <ID,List<ID>>();
        Map<ID, List<ID>> CaseToUsers = new Map <ID,List<ID>>();       
        system.debug('BLAT CasesList: '+ CasesList);
        Boolean OwnerIsGroup = false;
        for(Case c: CasesList)
        {
            if(c.FM_Discussions__r.size()>0)
            {
                //Queue
                if(c.Owner.Type=='Queue')
                {
                    system.debug('BLAT GROUP');
                    OwnerIsGroup = true;
                    CaseToGroupOwner.put(c.Id,c.Owner.Id);
                    //GroupList.add(c.Owner.Id);
                }
                //CaseTeam
                if(c.TeamMembers.size()>0)
                {
                    for(CaseTeamMember tm:c.TeamMembers)
                    {
                        UserList.add(tm.MemberId);
                    }
                    CaseToUsers.put(c.Id,UserList);
                    system.debug('BLAT c.TeamMembers: '+ c.TeamMembers);
                }
                c.FM_Discussion_Field__c = clsFMDiscussion.updateLatestFMDiscussiont(LastMB,c, map_caseIdToFMDiscussion.get(c.Id));
                c.comment_created_by__c = CaseToCommentCreator.get(c.id);
                c.Is_Public_last_comment__c = CaseToLastFM.get(c.id).Public__c; 
                lst_caseToUpdate.add(c);
                //system.debug('BLAT GroupList: '+ GroupList);
            }
        }
        
        if(lst_caseToUpdate.size()>0)
        {
            update lst_caseToUpdate;
        }
 /*       List<Group> gr = new List<Group>();
        if(CaseToGroupOwner.size()>0)
        {
            gr = [Select (Select UserOrGroupId From GroupMembers ) From Group g Where Id IN: CaseToGroupOwner.values()];
        }
        for(Group g:gr)
        {
            List <ID> member = new List <ID>();
            system.debug('BLAT g: '+ g);
            for(GroupMember gm: g.GroupMembers)
            {
                member.add(gm.UserOrGroupId);
                UserList.add(gm.UserOrGroupId);
                system.debug('BLAT gm: '+ gm);
            }
            GroupToMembers.put(g.ID, member);
        }
        Map <ID, User> UsersMail  = new Map <ID, User>();
        if(UserList.size()>0)
        {
            UsersMail = new Map  <ID, User> ([Select u.Id, u.Email From User u Where Id IN: UserList]);
        }
        system.debug('BLAT UsersMail: '+ UsersMail); 
        set <String> toAddressTmp;
        set <String> toAddress;
        //Get all Address
        for(Case c: CasesList)
        {
            toAddressTmp = new set <String>();
            ID g = CaseToGroupOwner.get(c.Id);
            system.debug('BLAT CaseToGroupOwner: ' +CaseToGroupOwner);
            system.debug('BLAT g: ' + g);
            //Queue
            if(g!=null)
            {
                for(ID memberID: GroupToMembers.get(g))
                {
                    toAddressTmp.add(UsersMail.get(memberID).Email);
                }
            }
            //CaseTeam
            if(CaseToUsers.size()>0)
            {
                for(ID userID :CaseToUsers.get(c.Id))
                {
                    if(UsersMail.containsKey(userID))
                    {
                        toAddressTmp.add(UsersMail.get(userID).Email);
                    }
                }
            }
            //Case Owner --> User
            if(!OwnerIsGroup)
            {
                system.debug('BLAT OwnerIsGroup');
                toAddressTmp.add(c.Owner.Email);
                system.debug('BLAT toAddressTmp 1: '+ toAddressTmp);
            }
            toAddressTmp.add(c.Assignee__r.Email);       
            if(c.AE_manager_email__c != NULL)        
            {toAddressTmp.add(ENV.AEManagerEmail);} 
            if(c.AE_manager_email_1__c != NULL)        
            {toAddressTmp.add(c.AE_manager_email_1__c);}                                      
            if(c.AE_manager_email_2__c != NULL)        
            {toAddressTmp.add(c.AE_manager_email_2__c);}                                      
                                                             
            toAddress = new set <String>();                                     
            for(FM_Discussion__c cc: c.FM_Discussions__r)
            {
                //send mail
                toAddress=Util.removeCreatedBy(toAddressTmp,cc.CreatedBy.Email);
                List <String> Addresses = new List <String>(toAddress);
                List<String> ccAddresses = new List<String>();
                if(cc.Public__c && cc.CreatedById != ENV.webCustomer && cc.CreatedById !=ENV.emailToCaseUser && userInfo.getUserType()=='Standard') // only internally aded comment

                {
                    Util.SendSingleEmail(Addresses, ccAddresses,'supportadmin@mellanox.com' , 'Mellanox Support Admin',Util.TemplatePublicCaseComment, c.Id, '','');
                }
                else
                {
                    Util.SendSingleEmail(Addresses, ccAddresses,'supportadmin@mellanox.com' , 'Mellanox Support Admin',Util.TemplateInternalCaseComment , c.Id, '','');
                }
            } /for FM_discussion
        }  // for case 
        */
     } // if(caseId > size() )
     
}