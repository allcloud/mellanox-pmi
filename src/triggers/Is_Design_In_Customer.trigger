trigger Is_Design_In_Customer on Case (before update) {
 for(Case c:trigger.New){ 
   if(c.ContactId !=NULL && c.state__c=='Open'&& trigger.oldMap.get(c.id).state__c !='Assigned') 
    {   List<User> ConUser = [select id, profileId, name from user where contactid =:c.contactid];
        List<Contact> RelCon= [ select id, name,technical_owner__c, contact_type__c  FROM Contact WHERE id=:c.ContactId];  
        List<SelfServiceUser> SSPuser = [select Id, IsActive, ContactId from SelfServiceUser where ContactId =:c.ContactId];
    
        if(!ConUser.isempty())
        { List<Profile> ProfName=[select Name from profile where id = :ConUser[0].ProfileId];
          if (ProfName[0].Name =='System Support Customer') {c.Is_Sys_Support_Contact__c = 1;}
          if (ProfName[0].Name =='Design Support Customer') {c.Is_Sys_Support_Contact__c = 2;}     
          if (ProfName[0].Name =='Design Support Customer'&&!(RelCon[0].technical_owner__c== NULL))
          {
           List<User> TechUser = [ select id ,manager_email__c, name, email FROM User WHERE id=:RelCon[0].technical_owner__c]; 
           //c.State__c = 'Assigned';
           //c.Status = 'Assigned';
              c.FAE_manager_email__c = TechUser[0].manager_email__c;            
             if(c.Int_Open_Mail_Design_In__c != TRUE)
              {  
                 c.ownerid=TechUser[0].id;
                                     
                 Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
                  mail.setTargetObjectId(TechUser[0].id);
                  mail.SetSubject( 'Mellanox Case #'+ c.casenumber + ' : ' + c.subject + ' was updated');      
                  mail.SetPlainTextBody('****NEW CASE OPENED NOTIFICATION***** ' +'\n\n'+
                                 ' Case #'+c.casenumber +':'+ c.Subject +'\n'+ 
                                 '\n was opened by following contact: '+ ConUser[0].name + '\n'+ 
                                  '\n Case Owner: ' + TechUser[0].name+'\n' +
                                 '\n Click on the link to access the case:' + '\n' + 
                                 'https://mellanox.my.salesforce.com/'+c.id);      
  
                  mail.setReplyTo('supportadmin@mellanox.com');
                  mail.setSenderDisplayName('Mellanox Support Admin');
                  mail.setSaveAsActivity(false);
                  mail.setBccSender(false);
                  mail.setUseSignature(false);       
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail}); 
              }
           c.Int_Open_Mail_Design_In__c=TRUE;
            }
       }else {
              if (!SSPuser.isempty() && RelCon[0].Contact_Type__c == 'Mellanox System Customer'){ c.Is_Sys_Support_Contact__c = 1; }
              if (!SSPuser.isempty() && RelCon[0].Contact_Type__c == 'Mellanox Silicon Design-In customer' || !SSPuser.isempty() && RelCon[0].Contact_Type__c == 'Ezchip Design-In customer'){ c.Is_Sys_Support_Contact__c = 2;}
              if ((RelCon[0].Contact_Type__c == 'Mellanox Silicon Design-In customer' || RelCon[0].Contact_Type__c == 'Ezchip Design-In customer') &&!(RelCon[0].technical_owner__c== NULL))
              {
                  List<User> TechUser = [ select id , name FROM User WHERE id=:RelCon[0].technical_owner__c]; 
                 // c.State__c = 'Assigned';
                  //c.Status = 'Assigned';
              if(c.Int_Open_Mail_Design_In__c != TRUE)
              {
                  c.ownerid=TechUser[0].id;
                  Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
                  mail.setTargetObjectId(TechUser[0].id);
                  mail.SetSubject( 'Mellanox Case #'+ c.casenumber + ' : ' + c.subject + ' was updated');       
                  mail.SetPlainTextBody('****NEW CASE OPENED NOTIFICATION***** ' +'\n\n'+
                                 ' Case #'+c.casenumber +':'+ c.Subject +'\n'+ 
                                 '\n was opened by following contact: '+ RelCon[0].name + '\n'+ 
                                  '\n Case Owner: ' + TechUser[0].name+'\n' +
                                 '\n Click on the link to access the case:' + '\n' + 
                                 'https://mellanox.my.salesforce.com/'+c.id);      
  
                  mail.setReplyTo('supportadmin@mellanox.com');
                  mail.setSenderDisplayName('Mellanox Support Admin');
                  mail.setSaveAsActivity(false);
                  mail.setBccSender(false);
                  mail.setUseSignature(false);       
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail}); 
                 }
  
                  c.Int_Open_Mail_Design_In__c=TRUE;
                  
                  
               }  
             }
   } 
 }
 
 
 /*Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
                  mail.setTargetObjectId(TechUser[0].id);
                  mail.SetSubject( 'New Case num #'+ c.casenumber );       
                  mail.SetPlainTextBody('****NEW CASE OPENED NOTIFICATION***** ' +'\n\n'+
                                 'Case #'+c.casenumber +':'+ c.Subject +'\n'+ 
                                 '\n was opened by following contact: '+RelCon[0].name + '\n'+ 
                                  '\n Case Owner: ' + TechUser[0].name+'\n' +
                                 '\n Click on the link to access the case:' + '\n' + 
                                 'https://na3.salesforce.com/'+c.id);      
  
                  mail.setReplyTo('supportadmin@mellanox.com');
                  mail.setSenderDisplayName('Mellanox Support Admin');
                  mail.setSaveAsActivity(false);
                  mail.setBccSender(false);
                  mail.setUseSignature(false);       
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail}); */

}