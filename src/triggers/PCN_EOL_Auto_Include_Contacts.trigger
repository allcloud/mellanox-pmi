trigger PCN_EOL_Auto_Include_Contacts on PCN__c (before insert,before update) {
    List<PCN__c> lst_PCNs = new List<PCN__c>();
    List<PCN__c> lst_PCNs_2 = new List<PCN__c>();
    List<String> ECRs = new List<String>();
    
    // Customer__c
    if (Trigger.isUpdate){
    	for (PCN__c p : Trigger.new) {
    		if(Trigger.oldmap.get(p.ID).Change_Type__c != p.Change_Type__c ) {
				lst_PCNs.add(p);
    		}
    		else if(Trigger.oldmap.get(p.ID).Customer__c != p.Customer__c) {
				lst_PCNs.add(p);
    		} 
    		//ECR field ECR_X_Ref__c
    		if(Trigger.oldmap.get(p.ID).ECR_X_Ref__c != p.ECR_X_Ref__c && !String.isBlank(p.ECR_X_Ref__c) ) {
				lst_PCNs_2.add(p);
    		}
    		//
    	}
    } //END if Trigger.isUpdate
    else if (Trigger.isInsert){
    	for (PCN__c p : Trigger.new) {
			lst_PCNs.add(p);
			if( !String.isBlank(p.ECR_X_Ref__c) ) {
				lst_PCNs_2.add(p);
    		}
    	}
    }
    if(lst_PCNs.size() == 0 && lst_PCNs_2.size() == 0)
    	return;
    //set ECR for searching
    for (PCN__c p : lst_PCNs_2) {
    	if( !String.isBlank(p.ECR_X_Ref__c) ) {
    		ECRs = p.ECR_X_Ref__c.split(';');
    		p.ECR_for_search__c = '';
    		for (String s : ECRs) {
    			p.ECR_for_search__c += s + ' ';
    		}
    	}
    }
    //
    for (PCN__c p : lst_PCNs) {
        //Initialize values
		p.Additional_Contact_1__c = null;
		p.Additional_Contact_2__c = null;
		p.Additional_Contact_3__c = null;
		p.Additional_Contact_4__c = null;
		p.Additional_Contact_5__c = null;
		p.Additional_Contact_6__c = null;
		p.Additional_Contact_7__c = null;
		p.Additional_Contact_8__c = null;        	
        //
        if(p.Change_Type__c == 'EOL' ) {
            if(p.Customer__c == 'MELLANOX' ) {
            	p.Additional_Contact_1__c = '0035000002GNeWd'; //incl channel email alias
            }
            
            p.Additional_Contact_2__c = '00350000024e4du';  //incl ww sales org email alias
			p.Additional_Contact_3__c = '0035000002UQsjQ'; //incl Mellanox T2P Managers 
            p.Additional_Contact_4__c = '0035000000fu0kXAAQ'; //incl Luanne
            p.Additional_Contact_5__c = '0035000001YKQpT'; //incl Curtis N
            p.Additional_Contact_6__c = '0035000002OK9ZE'; //incl Rebecca R
            
            if (p.Customer__c == 'HP') {
            	p.Additional_Contact_7__c = '0035000002MngbI'; //Ian Wilkie
            	p.Additional_Contact_1__c = '0035000002XhZOr'; //Joe Yang
        	}
            if (p.Customer__c == 'DELL' ) {	
            	p.Additional_Contact_7__c = '0035000002EbNVC'; //Dell EOL alias
            }
            if (p.Customer__c == 'LENOVO') {
            	p.Additional_Contact_2__c = '0035000002EZJEQ'; //David Roos	
            }
            p.Additional_Contact_8__c = '003500000265Uew'; //including Dawn
        }
		else if (p.Customer__c == 'DELL') {
			p.Additional_Contact_1__c = '0035000001xozVm'; //Dell account team
		}
		else if (p.Customer__c == 'IBM' ) {
            p.Additional_Contact_1__c = '00350000023Q39S'; //IBM Team alias	
		}   
		else if (p.Customer__c == 'LENOVO') {
            p.Additional_Contact_1__c = '0035000002Gs4Dw'; //Lenovo PCN team
		}
		else if (p.Customer__c == 'ORACLE') {
			p.Additional_Contact_1__c = '0035000001YL3MJ'; //Donald Fiegel
			p.Additional_Contact_2__c = '0035000002BLNAN'; //Rich Cotton
			p.Additional_Contact_3__c = '0035000001yljF6'; //Amir H.
		}
		else if (p.Customer__c == 'EMC' ) {
			p.Additional_Contact_1__c = '00350000015Ud2T'; //Pat Kelley
			p.Additional_Contact_2__c = '0035000001EgyBdAAJ'; //Julien B
			p.Additional_Contact_3__c = '0035000001whOIb'; //Yaniv A
			p.Additional_Contact_4__c = '0035000002CCTYW'; //Branko Cenanovic
			p.Additional_Contact_5__c = '0035000001yljF6'; //Amir Hellershtein
		}
		else if (p.Customer__c == 'ISILON') {
			p.Additional_Contact_1__c = '00350000015Ud2T'; //Pat Kelley
			p.Additional_Contact_2__c = '0035000001EgyBdAAJ'; //Julien B
			p.Additional_Contact_3__c = '0035000001whOIb'; //Yaniv A
		}
		else if (p.Customer__c == 'FACEBOOK') {
			p.Additional_Contact_1__c = '0035000001xmroh';  //Michael R
			p.Additional_Contact_2__c = '0035000002SgDIQ';  //Yevgeni Reznik
		}
		else if (p.Customer__c == 'SGI') {
			p.Additional_Contact_1__c = '0035000001Ub3iM'; //Elad W
			p.Additional_Contact_2__c = '0035000001cgxAw'; //Luan N
			p.Additional_Contact_3__c = '0035000002BLNAN'; //Rich C
		}
		/*
		else if (p.Name.containsIgnoreCase('MICROSOFT') || p.Name.containsIgnoreCase('Microsoft')) {
			p.Additional_Contact_1__c = '00350000023IxP8'; //Yaneev Y 
			p.Additional_Contact_2__c = '0035000002AE1Z6'; //Adam R
			p.Additional_Contact_3__c = '00350000021elT4'; //Steve H
			p.Additional_Contact_4__c = '0035000001ITCwT'; //Carol Ann H
			p.Additional_Contact_5__c = '0035000001wkYny'; //Christopher Plain
			p.Additional_Contact_6__c = '0035000002T2zsw'; //Jeremy M
		}
		*/
		else if (p.Customer__c == 'NETAPP') {
			p.Additional_Contact_1__c = '0035000001YL3MJ'; //Donald Fiegel
			p.Additional_Contact_2__c = '0035000002BLNAN'; //Rich Cotton
			p.Additional_Contact_3__c = '0035000001yljF6'; //Amir H. 
		}
		else if (p.Customer__c == 'GEMINI') {
			p.Additional_Contact_1__c = '00350000023IxP8'; //Yaneev Y
			p.Additional_Contact_2__c = '0035000001uazfe'; //Chris S
			p.Additional_Contact_3__c = '0035000002V93jS'; //Richard P
		}
		else if (p.Customer__c == 'HUAWEI') {
			p.Additional_Contact_1__c = '0035000002NZ7UQ'; // Shai R
			p.Additional_Contact_2__c = '0035000001xo08S'; // Roger Y
		}
		else if (p.Customer__c == 'XIV') {
			p.Additional_Contact_1__c = '00350000015Ud2T'; //Pat Kelley
			p.Additional_Contact_2__c = '00350000023Q39S'; //IBM PCN Team
			p.Additional_Contact_3__c = '0035000000rlNIM'; //Nir Gal
			p.Additional_Contact_4__c = '0035000000gp9WZ'; //Erez C
		}
		else if (p.Customer__c == 'APPLE') {
			p.Additional_Contact_1__c = '0035000002ZsaYt'; //Apple Internal alias
		}
		else if (p.Customer__c == 'TWITTER') {
			p.Additional_Contact_1__c = '0035000001xmroh';  //Michael R
			p.Additional_Contact_2__c = '0035000000gpFZi'; //Pranav P
		}
		else if (p.Customer__c == 'US ARMY') {
			p.Additional_Contact_1__c = '0035000002V959A'; //Elizabeth B
			p.Additional_Contact_2__c = '0035000000xwKym'; //Alex N
			p.Additional_Contact_3__c = '0035000001v67l5'; //Stephen M
		}
		else if (p.Customer__c == 'AQUARIUS') {
			p.Additional_Contact_1__c = '0035000000rlNIM'; //Nir Gal
			p.Additional_Contact_2__c = '0035000001hSKyH'; //Leonid Z
			p.Additional_Contact_3__c = '0035000001tj3A1'; //Aweas R
			p.Additional_Contact_4__c =	'0035000001qaGj6'; //Boris N
		}
		else if (p.Customer__c == 'YANDEX') {
			p.Additional_Contact_1__c = '0035000001hSKyH'; //Leonid Z
			p.Additional_Contact_2__c =	'0035000001qaGj6'; //Boris N
		}
		else if (p.Customer__c == 'CADENCE') {
			p.Additional_Contact_1__c = '0035000002BLNAN'; //Rich Cotton
			p.Additional_Contact_2__c =	'0035000002WCdm1'; //Isaac N
		}
		else if (p.Customer__c == 'CHECKPOINT') {
			p.Additional_Contact_1__c = '0035000001tj3A1'; //Aweas R
			p.Additional_Contact_2__c =	'0035000002OMXO6'; //Yael B
		}
		else if (p.Customer__c == 'LONDON STOCK EXCHANGE') {
			p.Additional_Contact_1__c = '0035000001Uas3q'; //Richard Hastie
		}     
    }
}