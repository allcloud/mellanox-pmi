trigger Loaner_Product_AfterInsertUpdate on Loaner_Product__c (after insert, after update) {
	Set<ID> iids = new Set<ID>();
	if (Trigger.isInsert) {
		for (Loaner_Product__c LP : Trigger.new) {
			iids.add(LP.id);
		}
	}
	else {
		for (Loaner_Product__c LP : Trigger.new) {
			if (LP.Product__c != Trigger.oldmap.get(LP.ID).Product__c || LP.OPN_quantity__c != Trigger.oldmap.get(LP.id).OPN_quantity__c) {
				iids.add(LP.id);
			}
		}
	}//
	if (iids.size() == 0)
		return;
	List<Loaner_Product__c> parts = new List<Loaner_Product__c>([select OPN_quantity__c,Prod_Type_1__c,Freight_Cost__c
																from Loaner_Product__c where id in :iids]);
	for (Loaner_Product__c LP : parts) {
		if (LP.Prod_Type_1__c == 'HCA') {
			if (LP.OPN_quantity__c <=5 )
				LP.Freight_Cost__c = 100;
			else if (LP.OPN_quantity__c <=10 )
				LP.Freight_Cost__c = 150;
			else
				LP.Freight_Cost__c = 250;
		}//
		else if (LP.Prod_Type_1__c == 'CABLES') {
			if (LP.OPN_quantity__c <=5 )
				LP.Freight_Cost__c = 90;
			else if (LP.OPN_quantity__c <=10 )
				LP.Freight_Cost__c = 150;
			else
				LP.Freight_Cost__c = 300;
		}//
		if (LP.Prod_Type_1__c == 'SWITCH') {
			if (LP.OPN_quantity__c <=2 )
				LP.Freight_Cost__c = 378;
			else if (LP.OPN_quantity__c <=5 )
				LP.Freight_Cost__c = 900;
			else
				LP.Freight_Cost__c = 1500;
		}//
	}//end loop
	if (parts.size() > 0)
		Database.update(parts, false);																	
}