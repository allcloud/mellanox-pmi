trigger Approve_Opp_Reg_Mobile on Distributor_Oppy_Registrations__c (after update) {

list<id> Registrations = new  list<id>(); 
map<id,ProcessInstanceWorkitem> AppFlows = new map<id,ProcessInstanceWorkitem>();  
map<id,ProcessInstanceStep> AppStep = new map<id,ProcessInstanceStep>();  
map<id,Distributor_Oppy_Registrations__c> UpdOppReg = new map<id,Distributor_Oppy_Registrations__c>();

for(Distributor_Oppy_Registrations__c OpReg:trigger.new)
{ Registrations.add(OpReg.id); }

for( ProcessInstanceWorkitem AF:[Select p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId,
                      p.ProcessInstanceId,p.OriginalActorId,p.Id,p.ActorId          
       From ProcessInstanceWorkitem p where p.ProcessInstance.TargetObjectId in : Registrations])
{AppFlows.put(AF.ProcessInstance.TargetObjectId,AF);}

for(Distributor_Oppy_Registrations__c OpRegList:[Select id, Approve__c,Reject__c  from Distributor_Oppy_Registrations__c where id in:Registrations])
 {UpdOppReg.put(OpRegList.id,OpRegList);}


for(Distributor_Oppy_Registrations__c OReg:trigger.new)      
 {  if(OReg.Approve__c == True && trigger.oldMap.get(OReg.id).Approve__c == False && OReg.Discount_Granted__c != NULL && AppFlows.ContainsKey(OReg.id))              
     { Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
       req2.setComments('Approving request');
       req2.setAction('Approve');           
      //req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
       req2.setWorkitemId(AppFlows.get(OReg.id).id);             
       Approval.ProcessResult result2 =  Approval.process(req2);
     //System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
    // System.assertEquals('Approved', result2.getInstanceStatus(), 'Instance Status'+result2.getInstanceStatus());              
       UpdOppReg.get(OReg.id).Approve__c = False;                  
       Update(UpdOppReg.get(OReg.id));               
       }                  
  if(OReg.Reject__c == True && trigger.oldMap.get(OReg.id).Reject__c == False && OReg.Rejection_Reasons__c!= NULL && AppFlows.ContainsKey(OReg.id))              
     { Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
       req2.setComments('Rejecting request');
       req2.setAction('Reject');           
      //req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
       req2.setWorkitemId(AppFlows.get(OReg.id).id);             
       Approval.ProcessResult result2 =  Approval.process(req2);                     
       UpdOppReg.get(OReg.id).Reject__c = False;                  
       Update(UpdOppReg.get(OReg.id));                      
                                                        
       }                    
     }        
          
  }