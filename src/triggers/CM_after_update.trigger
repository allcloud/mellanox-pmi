trigger CM_after_update on Contract_Manager__c (after insert,after update) {


	Map<string,id> ContractOrder = new map<string,id>();
	List<string> Orders = new List<string>();
	List<string> PNs = new List<string>();
	List<String> serialNo_List = new List<String>();
	string SNs ;
	id SNcontract;

	for(Contract_Manager__c  CM:trigger.new){
	 
    	if(trigger.isinsert || trigger.isupdate){  // && CM.Order__c != trigger.oldmap.get(CM.id).Order__c)){
	  	
		    ContractOrder.put(CM.order__c,CM.contract__c);     
		    Orders.add(CM.order__c);
		    
		    if(CM.Part_number__c != NULL) {
		    	
		    	list<String> partNumbers_List = CM.Part_number__c.split(',');
		    	
		    	system.debug('partNumbers_List : ' + partNumbers_List);
		    	
		    	for (String partNo : partNumbers_List){
		    		
		    		PNs.add(partNo);
		    	}
		    }
	    	
		    if(CM.Serial_Numbers__c != NULL){
		    	
	       		serialNo_List = CM.Serial_Numbers__c.split(',');
	       		system.debug('serialNo_List : ' + serialNo_List);
	       		
	       		/*
	       		SNs = '(' + CM.Serial_Numbers__c + ')';
	       		system.debug('SNs : ' + SNs);
	       		*/
	        	SNcontract = CM.contract__c;
	        	
	        }
	    }
	
	}
	
	
	if(ContractOrder.values().size() >0 )
	{
		  AssetReassignment reassign = new AssetReassignment();
		  reassign.Origin =  'CM';   
		  reassign.query= 'select id,name , Contract2__c, Order__c  FROM Asset2__c WHERE order__c in' + ENV.getStringsForDynamicSoql(Orders);          
		  System.debug('Query = '+ reassign.query);
		      
		  if(PNs.size()>0){
		  
	  	  	 reassign.query= 'select id,name , Contract2__c, Order__c  FROM Asset2__c WHERE order__c in' + ENV.getStringsForDynamicSoql(Orders) + ' and Part_number__c in' + ENV.getStringsForDynamicSoql(PNs);
		  	
		  }
		    
		  if(!serialNo_List.isEmpty()){
			  
			  reassign.query= 'select id,name , Contract2__c, Order__c  FROM Asset2__c WHERE Serial_Number__c in ' + ENV.getStringsForDynamicSoql(serialNo_List) + ' OR Parent_Serial__c in '  + ENV.getStringsForDynamicSoql(serialNo_List);
			  System.debug('Query 62 = '+ reassign.query);
			  reassign.Origin =  'CM_SN';  
			  reassign.SNContract = SNcontract; 
		   }          
		   
		  reassign.OrderContract = ContractOrder;
		  //reassign.NewContract =  Cont;               
		  ID batchprocessid = Database.executeBatch(reassign);
	  }

}