trigger CaseComment_AfterInsert on CaseComment (after insert) 
{
    // Create new FM Discussion object from each new Case Comment

    system.debug('BLAT Util.CaseCommentCreated: ' + Util.CaseCommentCreated);
    if(!Util.CaseCommentCreated)
    {
        List<FM_Discussion__c> insFMDiscussion = new List<FM_Discussion__c>();
        for(CaseComment cc :Trigger.new)
        {
            FM_Discussion__c fmd = new FM_Discussion__c();
            fmd.Case__c = cc.ParentId;
            fmd.Public__c=cc.IsPublished;
            fmd.Discussion__c = cc.CommentBody;
            fmd.Sync_With_FM__c=true;
            fmd.Case_Comment_ID__c=CC.Id;
            insFMDiscussion.Add(fmd);
        if( cc.CommentBody.startsWith('Comment By Mellanox User')|| cc.CommentBody.startsWith('Comment By Contact:') || cc.CommentBody.startsWith('Comment By Customer:'))      
         { string first_row = (cc.CommentBody.split('\n',2))[0];    
           fmd.Email_CreatedBy__c =(first_row.split(':',2))[1];       
                    
         }      
        
        if( cc.CommentBody.startsWith('Comment By Mellanox User'))
         {
          if( cc.CommentBody.startsWith('Comment By Mellanox User AE'))
          {fmd.source__c = 'Mellanox User AE';}
           else{fmd.source__c = 'Mellanox User';}
          fmd.comment_target__c = 'External Comment'; //(comment created from email sent to customer)
          }                                                           
        else {fmd.source__c = 'Customer'; 
              fmd.comment_target__c = 'Comment by Customer '; //(comment created from email sent to customer)          
               }
        
      
        
             
         
    }
   
     if(!insFMDiscussion.isEmpty())
        {
            Util.CaseCommentCreated=true;
            insert insFMDiscussion;
        }
    }
}