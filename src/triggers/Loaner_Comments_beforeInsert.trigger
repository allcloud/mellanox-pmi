trigger Loaner_Comments_beforeInsert on Loaner_Comment__c (before insert) {
	List<Loaner_Comment__c> lst_comments = new List<Loaner_Comment__c>();
	Set<ID> ids = new Set<ID>();
	Set<ID> requester_ids = new Set<ID>();
	
	for (Loaner_Comment__c comm : Trigger.new) {
		if(comm.Send_Comments_To__c != null) {
			lst_comments.add(comm);
		}
		if(comm.Send_Comments_To__c == 'Current Approver') {
			ids.add(comm.Loaner__c);
		}
		else if(comm.Send_Comments_To__c == 'Requester') {
			requester_ids.add(comm.Loaner__c);
		}
	}
	if(lst_comments.size() == 0)
		return;
	List<ProcessInstanceWorkitem> flows = new List<ProcessInstanceWorkitem>([select ActorID, ProcessInstance.TargetObjectId 
																				from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId in :ids]);
	Map<ID,ID> map_loanerID_actorID	= new Map<ID,ID>();
	for (ProcessInstanceWorkitem p : flows) {
		map_loanerID_actorID.put(p.ProcessInstance.TargetObjectId, p.ActorID);
	}																			
	
	List<Loaner_Request__c> loaner_requests = new List<Loaner_Request__c>([select ID,ownerid from Loaner_Request__c where id in :requester_ids]);
	Map<ID,ID> map_loanerID_ownerID = new Map<ID,ID>();
	for (Loaner_Request__c LR : loaner_requests) {
		map_loanerID_ownerID.put(LR.id, LR.ownerid);
	}

	for (Loaner_Comment__c c : lst_comments) {
		if(c.Send_Comments_To__c == 'Requester') {
			c.To_User__c = map_loanerID_ownerID.get(c.Loaner__c) != null ? map_loanerID_ownerID.get(c.Loaner__c) : null; 
		}		
		else if (c.Send_Comments_To__c == 'Current Approver') {
			c.To_User__c = map_loanerID_actorID.get(c.Loaner__c) != null ? map_loanerID_actorID.get(c.Loaner__c) : null; 
		}
	}
}