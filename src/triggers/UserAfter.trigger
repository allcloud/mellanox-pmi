trigger UserAfter on User (after insert, after update, after delete) {

    if ( Trigger.isInsert ) {
        new CommunityProductSharingService().processAccountInitialSetupForUsers(Trigger.newMap, null);
    }
    
    else if (trigger.IsUpdate) {
    
        new CommunityProductSharingService().processAccountInitialSetupForUsers(Trigger.newMap, trigger.oldMap);
    }
}