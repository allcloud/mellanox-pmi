trigger PCN_Products_BeforeInsert_BeforeUpdate on PCN_Affected_Products__c (before insert, before update) {
	Set<String> prod_names = new Set<String>();
	List<PCN_Affected_Products__c> lst_pcn_products = new List<PCN_Affected_Products__c>();
	
	if(Trigger.isInsert){
		for (PCN_Affected_Products__c p : Trigger.new){
			if(p.OPN_Name_Bulk_load__c!=''){
				prod_names.add(p.OPN_Name_Bulk_load__c);
				lst_pcn_products.add(p);
			}			
		}
	}
	else {
		for (PCN_Affected_Products__c p : Trigger.new){
			if(Trigger.oldmap.get(p.id).OPN_Name_Bulk_load__c != p.OPN_Name_Bulk_load__c){
				prod_names.add(p.OPN_Name_Bulk_load__c);
				lst_pcn_products.add(p);			
			}
		}
	}
	if(prod_names.size()==0)
		return;
	
	List<Product2> lst_prods = new List<Product2>([select id,Name from Product2 where Name in :prod_names]);
	Map<String,ID> map_prod_to_ID = new Map<String,ID>();
	for (Product2 pp : lst_prods){
		map_prod_to_ID.put(pp.Name,pp.ID);
	}
	if(map_prod_to_ID.size()==0)
		return;
	
	for(PCN_Affected_Products__c p2 : lst_pcn_products){
		p2.Product__c = p2.OPN_Name_Bulk_load__c !='' ? map_prod_to_ID.get(p2.OPN_Name_Bulk_load__c) : null; 	
	}
}