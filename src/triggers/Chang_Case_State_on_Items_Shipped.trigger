trigger Chang_Case_State_on_Items_Shipped on RMA__c (after insert, after update) {
for (RMA__c RMA:trigger.new)
{ if(RMA.Items_Shipped__c == TRUE && trigger.oldMap.get(RMA.Id).Items_Shipped__c == FALSE && RMA.Case_Parent__c!=NULL)
  {list<Case> RelCase =[select id, state__c from Case where id=:RMA.Case_Parent__c ]; 
    if(RelCase[0].state__c == 'RMA Assigned')
        { RelCase[0].state__c = 'Assigned';
          update(RelCase[0]);
         }
   }
 }
}