trigger MS_Map_before_insert_update on Milestone_Map__c (before insert, before update) {

list<string> ProjectTypes = new list<string>();
list<string> MilestoneTypes = new list<string>();
list<RecordType> RecordTypes = new list<RecordType>();
list<Milestone_Map__c> MSmap = new list<Milestone_Map__c>();
map<string,id> MSRecTypeNameToId = new map<string,id>();
 map<string,id> PrRecTypeNameToId = new map<string,id>();
for(Milestone_Map__c MM:trigger.new)
{
  if(trigger.isinsert)
   {
    ProjectTypes.add(MM.Project_Type__c);
    MilestoneTypes.add(MM.milestone__c);
    MSmap.add(MM);
   }
 if(trigger.isupdate && (MM.Project_Type__c != trigger.oldmap.get(MM.id).Project_Type__c))
  {
   ProjectTypes.add(MM.Project_Type__c);
   MSmap.add(MM);
  }
 } 
 
 for(RecordType RecTp:[select id, name, SobjectType from RecordType where Name in :ProjectTypes ])
 {
     
   if(RecTp.SobjectType == 'Milestone1_Project__c')
   {
    PrRecTypeNameToId.put(RecTp.name, RecTp.Id);
   }
  
 }
 
if(!MSmap.isempty()) 
 {
  for( Milestone_Map__c mmp:MSmap)
  {
    mmp.Project_type_id__c =  string.ValueOf(PrRecTypeNameToId.get(mmp.Project_type__c)).left(15);
  }
 }
  
   
   
}