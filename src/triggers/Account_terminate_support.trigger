trigger Account_terminate_support on Account (after update) 
{
	List<id> ContUpdIds = new List<Id>();
	
	List<id> UpdAccIds = new List<id>();
	List<Contract2__c> UpdContrs= new List<Contract2__c>();
	
	for(Account Acc:trigger.new)
	{
		 if(Acc.terminate_support__c!= trigger.oldmap.get(Acc.id).terminate_support__c)
		 	{UpdAccIds.add(Acc.id);}
	}
	
	if(UpdAccIds.size() >0 )
	{
		 UpdContrs = [select id, status__c, Previous_Status__c, Contract_in_Credit_Hold__c  
		 			  from Contract2__c 
		 			  where account__c  in :UpdAccIds];
		 
		 for(Account Acc1:trigger.new)
		 {
			  if(Acc1.terminate_support__c == True)
			  {
				   for(contract2__c Cn:UpdContrs)
				   { 
					    Cn.Previous_Status__c = Cn.status__c ;                        
					    Cn.status__c = 'Expired'; 
					    Cn.Contract_in_Credit_Hold__c = True;
				    }
			  }           
			   if(Acc1.terminate_support__c == False)       
			   {             
				    for(contract2__c Cn:UpdContrs)
				    {
				    	Cn.status__c = Cn.Previous_Status__c; 
				    	Cn.Contract_in_Credit_Hold__c = False;
				    } 
			   }
		  }
	 }
	 update(UpdContrs);
}