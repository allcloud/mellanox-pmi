trigger update_Loaner_request_PO on BBB_Orders__c (after insert, after update) {

List<string> loansToUpd = new List<string>();
List<loaner_request__c> loansToUpdOrder = new List<loaner_request__c>();
map<string,decimal> LoanToPo = new map<string,decimal>();
map<string,decimal> LoanToReturnPo = new map<string,decimal>();
for(BBB_Orders__c bb:trigger.new)
{
 if(bb.line_type__c!=null && bb.line_type__c!='' && bb.line_type__c.contains('Ship and Bill'))
 {
  loansToUpd.add(bb.Customer_PO_number__c);
  LoanToPo.put(bb.Customer_PO_number__c,bb.Sales_Order_Number__c);
 }
 if(bb.line_type__c!=null && bb.line_type__c!='' && bb.line_type__c.contains('Return'))
 {
  loansToUpd.add(bb.Customer_PO_number__c);
  LoanToReturnPo.put(bb.Customer_PO_number__c,bb.Sales_Order_Number__c);
 }
 
 
}

loansToUpdOrder =[select id, Order__c,Return_Order__c, name from loaner_request__c where name in:loansToUpd];

for(loaner_request__c lr:loansToUpdOrder)
{
  if(LoanToPo.containsKey(lr.name))
  { lr.Order__c = string.valueOf(LoanToPo.get(lr.name));}
  
  if(LoanToReturnPo.containsKey(lr.name))
  { lr.Return_Order__c = string.valueOf(LoanToReturnPo.get(lr.name));}
 
 
}

//KN-update(loanstoUpdOrder)
Database.update(loansToUpdOrder,false);

}