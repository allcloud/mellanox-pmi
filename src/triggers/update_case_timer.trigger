trigger update_case_timer on Case (before update) {
if (ENV.triggerShouldRun) {
	ENV.triggerShouldRun = false;
	for(Case Cc:trigger.new)
	{
	     if(Cc.casetimeaddmin__c != trigger.oldmap.get(Cc.Id).casetimeaddmin__c || 
	            Cc.casetimeraddhour__c != trigger.oldmap.get(Cc.Id).casetimeraddhour__c) 
	      {
	        if (Cc.Total_Time_Elapsed__c == null)
	           { Cc.Total_Time_Elapsed__c = 0;}
	        double prev_val = Cc.Total_Time_Elapsed__c;
	        if(Cc.casetimeaddmin__c!=NULL && Cc.casetimeraddhour__c!= NULL)
	        {Cc.Total_Time_Elapsed__c = prev_val + Cc.casetimeaddmin__c+ Cc.casetimeraddhour__c*60;}
	        if(Cc.casetimeaddmin__c!=NULL && Cc.casetimeraddhour__c== NULL)
	        {Cc.Total_Time_Elapsed__c = prev_val + Cc.casetimeaddmin__c;}
	        if(Cc.casetimeaddmin__c==NULL && Cc.casetimeraddhour__c!= NULL)
	        {Cc.Total_Time_Elapsed__c = prev_val + Cc.casetimeraddhour__c*60;}
	
	        
	        Cc.casetimeaddmin__c =0;
	        Cc.casetimeraddhour__c = 0;
	        
	       }
	 }
}
}