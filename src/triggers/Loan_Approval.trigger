trigger Loan_Approval on Loan__c (after insert) {
 for(Loan__c Loan: Trigger.new)     
      {   
         if(trigger.isinsert) 
          {    
           if (Loan.Status__c == 'Created'){   
             approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
             req1.setComments('Submitting request for approval.'); 
             System.debug('YTR->1->'+Loan__c.id);
             req1.setObjectId(Loan.id);
             //req1.setNextApproverIds(idList);
             approval.ProcessResult result = Approval.process(req1); 
             System.assert(result.isSuccess());  
             System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
             
           }
          }
 }
}