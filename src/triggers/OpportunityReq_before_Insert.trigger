trigger OpportunityReq_before_Insert on Distributor_Oppy_Registrations__c (before insert, before update) {
list<string> Dist = new list<string>();
Map<id,Approval> OppRegApp = new Map<id,Approval>();
list<id> OppRegId = new list<id>();
Map<string,Account> RelAccount = new Map<string,Account>();
Map<string,User> ProdManager = new Map <string,User>();
List<string> DisState = new List<string>();
List<string> DisCountry = new List<string>();
Map<String,US_Territory_Map__c> TerMap = new Map<String,US_Territory_Map__c>();
List<id> SDid = new List<id>();
Map<id,user> SDinfo = new Map<id,user>();
List<string> EURcountries = new List<string>();
Map<String,US_Territory_Map__c> EURTerMap = new Map<String,US_Territory_Map__c>(); 
map<id,id> delegatedApproversMap = new map<id,id>();
list<id> approverUserIdSet = new list<id>();    



                        
for(Distributor_Oppy_Registrations__c Opp:Trigger.new)
{
    Dist.add(Opp.Distributor_Name__c);
    DisCountry.add(Opp.Partner_Country__c.toLowerCase());
    DisState.add(Opp.Partner_State__c.toLowerCase());     
    EURcountries.add(Opp.End_User_Country__c.toLowerCase());                  
    OppRegID.add(Opp.id);           
    SDid.add(Opp.sales_director_user__c);   
 }   

if(trigger.isUpdate)
 {       
     for(User  SD:[Select Id, Name, email  From User u where id in :SDid])
    { SDinfo.put(SD.id,SD);}
                              
   for(Distributor_Oppy_Registrations__c OppR:Trigger.new)
    {          
      if(OppR.Registration_Status__c == 'Reviewed' && trigger.oldMap.get(OppR.id).Registration_Status__c !='Reviewed' 
            && OppR.Certified_Reseller_Registration__c == false
            && OppR.Partner_First_Registration__c == false
            && OppR.OEM_Pricing_SEWP__c == false
            && OppR.SEWP__c == false
            && OppR.Discount_Granted__c == NULL )
        {OppR.adderror('\n PLEASE FILL IN THE "Discount Granted %" FIELD IN ORDER TO APPROVE THE OPPORTUNITY');}
      
      if(OppR.Registration_Status__c == 'Denied' && trigger.oldMap.get(OppR.id).Registration_Status__c !='Denied' && OppR.Rejection_Reasons__c == NULL )
        {OppR.adderror('\n THE "Rejection Reason" FIELD MUST BE FILLED IN ORDER TO REJECT THE OPPORTUNITY');}
  
      if(OppR.Sales_director_user__c != trigger.oldMap.get(OppR.id).Sales_director_user__c  && OppR.Sales_director_user__c != Null)  
        {OppR.Mellanox_Sales_Director_Email__c = SDinfo.get(OppR.Sales_director_user__c).email;               
         OppR.Mellanox_Sales_Director__c =  SDinfo.get(OppR.Sales_director_user__c).name; 
        }             
      if(OppR.Sales_director_user__c != trigger.oldMap.get(OppR.id).Sales_director_user__c  && OppR.Sales_director_user__c == Null)  
        {OppR.Mellanox_Sales_Director_Email__c = '';               
         OppR.Mellanox_Sales_Director__c =''; 
        }            
                                          
    }           
 }      

 if( trigger.isInsert)
 {
    for(Account Acc: [select a.id, a.name, a.Product_manager__c,  a.Product_Manager__r.Email, a.PM_additional_email__c,PM_Additional_Email_2__c from Account a where Name in :Dist])
     { RelAccount.put(Acc.name,Acc);}
 
    for(User  PM:[Select u.Id, u.Contact.Account.Name  From User u where u.Contact.Account.Name in :Dist and u.Contact.Is_Product_Manager__c = TRUE])
    { ProdManager.put(PM.Contact.Account.Name,PM);}
    
    for(US_Territory_Map__c  TM:[select id, Sales_Director__c, Sales_Director__r.email, Sales_Director__r.name, SD_Region__c, Country_LowerCase__c, State_LowerCase__c,VT_Region__c,EMEA_Channel_Mgr__c from US_Territory_Map__c where Country_LowerCase__c in :DisCountry and State_LowerCase__c in :DisState ] )
    { system.debug('SD Email: ' + TM.Sales_Director__r.email); 
      system.debug('SD Country: ' + TM.Country_LowerCase__c + TM.State_LowerCase__c);  
   
      TerMap.put((TM.Country_LowerCase__c + TM.State_LowerCase__c),TM);    
      if(TM.SD_Region__c =='EUR')
      { for(US_Territory_Map__c  ETM:[select id, Sales_Director__c, Sales_Director__r.email, Sales_Director__r.name, SD_Region__c, Country_LowerCase__c, State_LowerCase__c,VT_Region__c from US_Territory_Map__c where Country_LowerCase__c in :EURcountries])
        {EURTerMap.put(ETM.Country_LowerCase__c,ETM); }   

      } //if       
    }                
    for(Distributor_Oppy_Registrations__c OppReg:Trigger.new)
   {if(RelAccount.containsKey(OppReg.Distributor_Name__c))
     {OppReg.Distributor__c = RelAccount.get(OppReg.Distributor_Name__c).id;
      OppReg.Disty_Product_Manager_Email__c = RelAccount.get(OppReg.Distributor_Name__c).Product_Manager__r.Email;
      OppReg.Additional_Email__c  = RelAccount.get(OppReg.Distributor_Name__c).PM_additional_email__c;
      OppReg.PM_Additional_Email_2__c  = RelAccount.get(OppReg.Distributor_Name__c).PM_Additional_Email_2__c;
      
      if(ProdManager.ContainsKey(OppReg.Distributor_Name__c))
       {OppReg.OwnerId = ProdManager.get(OppReg.Distributor_Name__c).id;}
      if(TerMap.ContainsKey(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ))
      {    
       OppReg.SD_Region_Hidden__c = TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).SD_Region__c;
       OppReg.Registration_Region__c = TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).SD_Region__c;                                                 
            
       if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).SD_Region__c == 'EUR')    
       {     
       OppReg.Mellanox_Sales_Director_Email__c = EURTerMap.get(OppReg.End_User_Country__c.toLowerCase() ).Sales_Director__r.email;
       OppReg.Mellanox_Sales_Director__c = EURTerMap.get(OppReg.End_User_Country__c.toLowerCase() ).Sales_Director__r.name;
       OppReg.Sales_Director_User__c = EURTerMap.get(OppReg.End_User_Country__c.toLowerCase() ).Sales_Director__c;
                        
       }else      
       {                                                                                                                          
       OppReg.Mellanox_Sales_Director_Email__c = TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).Sales_Director__r.email;
       OppReg.Mellanox_Sales_Director__c = TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).Sales_Director__r.name;
       OppReg.Sales_Director_User__c = TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).Sales_Director__c;
       }                                                               
              
       //KN added 09-25-13
       //Assign Chuck T to be regional VP for USE
       if(OppReg.SD_Region_Hidden__c=='USE')
            OppReg.Regional_VP_User__c = '005500000013P66AAE';
       //Assign Gil Briman for ASI region and he's not yet as a Sales Director
       //if(OppReg.SD_Region_Hidden__c=='ASI' && OppReg.Sales_Director_User__c != '00550000001t0V4AAI' && OppReg.Sales_Director_User__c != '00550000001t0V4')
            //OppReg.Regional_VP_User__c = '00550000001t0V4AAI';
       //Assign Yossi Avni for EUR region and he's not yet as a Sales Director
       if(OppReg.SD_Region_Hidden__c=='EUR' && OppReg.Sales_Director_User__c != '0055000000152snAAA' && OppReg.Sales_Director_User__c != '0055000000152sn')
            OppReg.Regional_VP_User__c = '0055000000152snAAA';
       //end KN added -09-25-13
        
        //KN added -11-05-14, assign NA-Channel-Account-Managers
       if(OppReg.Partner_Country__c.toLowerCase() == 'usa' || OppReg.Partner_Country__c.toLowerCase() == 'canada') {
            if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ) != null) {
                if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).VT_Region__c == 'VAR-EAST')
                    OppReg.NA_Channel_Account_Mgr__c = '00550000003ykOW'; //Paul Michniak   
                else if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).VT_Region__c == 'VAR-CENTRAL')
                    OppReg.NA_Channel_Account_Mgr__c = '00550000003hF1g';  //Stuart Bing
                else if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).VT_Region__c == 'VAR-WEST')
                    OppReg.NA_Channel_Account_Mgr__c = '00550000001rY1V';  //Michael Stys
            }   
       }
       //KN - 11-18-14 -assign EMEA Channel Account Managers: either Michael Hieke, or Syd Virdi
       if(OppReg.SD_Region_Hidden__c=='EUR') {
            if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).EMEA_Channel_Mgr__c=='Michael Hieke')
                OppReg.EMEA_Channel_Manager__c = '00550000003huPu';
            else if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).EMEA_Channel_Mgr__c=='Syd Virdi')
                OppReg.EMEA_Channel_Manager__c = '00550000001rjMA';         
       }           
        //                                                                   
       system.debug('Country in Map: ' + OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() );                          
       OppReg.Registration_Status__c = 'Submitted';          
     /* //production
       if(OppReg.Distributor_Name__c.toLowerCase() == 'bell micro - emea') {OppReg.Additional_Email__c = 'Katie.Morris@avnet.com';}
       if(OppReg.Distributor_Name__c.toLowerCase() == 'hammer plc uk') {OppReg.Additional_Email__c = 'jasond@hammerplc.com';} */
       if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).SD_Region__c.toLowerCase()== 'use'){OppReg.Territory_Email__c = 'pat@mellanox.com'; }   
       if(TerMap.get(OppReg.Partner_Country__c.toLowerCase() + OppReg.Partner_State__c.toLowerCase() ).SD_Region__c.toLowerCase()== 'eur'){OppReg.Territory_Email__c = 'syd@mellanox.com'; }                             
      } 
    }
  }
 }
 
 
//----------------------------------------------------
/*
for(Distributor_Oppy_Registrations__c Opp:Trigger.new)
{
if(trigger.isInsert)
{
  Opp.Level1_Approver__c = ENV.Sales_Directors.get('Darrin Chen');
  
  if(Opp.Partner_Reseller_Name__c.contains('Info X') || Opp.Partner_Reseller_Name__c.contains('Info- X'))
  {}
  
 else{
                         
     if( Opp.Partner_Country__c.tolowerCase() == 'usa' ||Opp.Partner_Country__c.tolowerCase() == 'canada'  )
      {
       Opp.Level2_Approver__c = ENV.Sales_Directors.get('Sergio Gemo');
       Opp.Level2_Approver2__c = ENV.Sales_Directors.get('Gerry Hunt');
      }
      
     
     //--Latin-Central america  
    if(Opp.SD_Region_Hidden__c == 'USC' || Opp.SD_Region_Hidden__c == 'USC')
    {
     Opp.Level2_Approver__c = ENV.Sales_Directors.get('Sergio Gemo');
     Opp.Level3_Approver__c = ENV.Sales_Directors.get('Ozzie Gomez');
    
    }
    
   if(Opp.SD_Region_Hidden__c != 'USW' && Opp.SD_Region_Hidden__c != 'USE' && Opp.SD_Region_Hidden__c != 'USS'&& Opp.SD_Region_Hidden__c !='USC')
    { 
      Opp.Level2_Approver__c = Opp.Sales_Director_User__c;
     
    } 
 //-------------------- Sales Director Approval Atlantic
   
   if( Opp.Distributor_Name__c == 'ATLANTIK SYSTEME GMBH' )
   {
     Opp.Level2_Approver__c = ENV.Sales_Directors.get('Juergen Windler');
     Opp.Level2_Approver2__c = NULL;
   }
   
 //-------------------- Sales Director Arrow Asc
   
  if( Opp.Distributor_Name__c == 'ARROW ECS' )
   {
     Opp.Level2_Approver__c = ENV.Sales_Directors.get('Nimrod Gindi');
     Opp.Level2_Approver2__c = NULL;
   } 
   
   if(Opp.Partner_Reseller_Name__c == 'Dell') 
    {
        Opp.Level2_Approver__c = ENV.Sales_Directors.get('Jon Green');
        Opp.Level2_Approver2__c = NULL;
    }                
  
 
 
      
 //-------------- Marc Final Approval (discount > 20%)    
 if(Opp.Discount_Granted__c > 0.2 && Opp.Partner_First_Registration__c == False)    
 {
  if(Opp.Level3_Approver__c != NULL && Opp.Level4_Approver__c == NULL) {Opp.Level4_Approver__c = ENV.Sales_Directors.get('Marc Sultzbaugh');}
  if(Opp.Level2_Approver__c != NULL && Opp.Level3_Approver__c == NULL) {Opp.Level3_Approver__c = ENV.Sales_Directors.get('Marc Sultzbaugh');}
  if(Opp.Level2_Approver__c == NULL) {Opp.Level2_Approver__c = ENV.Sales_Directors.get('Marc Sultzbaugh');}
  }
 } // else  
 
 
                   
 
if(Opp.level1_approver__c!=NULL) {approverUserIdSet.add(Opp.level1_approver__c);}         
if(Opp.level2_approver__c!=NULL) {approverUserIdSet.add(Opp.level2_approver__c);}      
if(Opp.level2_approver2__c!=NULL) {approverUserIdSet.add(Opp.level2_approver2__c);}     
if(Opp.level3_approver__c!=NULL) {approverUserIdSet.add(Opp.level3_approver__c);}    
if(Opp.level4_approver__c!=NULL) {approverUserIdSet.add(Opp.level4_approver__c);}    

                      
}  // if trigger isinsert 
    
            
} 



if(approverUserIdSet.size() >0)
{
 for(User approvers:[select Id, Vacation_End_Date__c, Vacation_Start_Date__c, DelegatedApproverId  
                    from User   where id in:approverUserIdSet          
                    and Vacation_End_Date__c <> null  
                    and Vacation_Start_Date__c <> null  
                    and DelegatedApproverId <> null])        
  {  
    system.debug('delegatedApprover!: ' + approvers.DelegatedApproverId +'  +'+ approvers.Vacation_End_Date__c); 
 if (approvers.Vacation_Start_date__c <= System.today() && approvers.Vacation_End_date__C >= System.today()) {  

               
                 delegatedApproversMap.put(approvers.Id, approvers.DelegatedApproverId);  
               system.debug('delegatedApprovers: ' + delegatedApproversMap.values());  

           }  
  } 
 }        
 




 
for(Distributor_Oppy_Registrations__c Opp1:Trigger.new)
{
 
     
    if(delegatedApproversMap.containsKey(Opp1.level1_approver__c))  
     Opp1.level1_approver__c = delegatedApproversMap.get(Opp1.level1_approver__c);  

    if(delegatedApproversMap.containsKey(Opp1.level2_approver__c))  
    Opp1.level2_approver__c = delegatedApproversMap.get(Opp1.level2_approver__c);
    
    if(delegatedApproversMap.containsKey(Opp1.level2_approver2__c))  
    Opp1.level2_approver2__c = delegatedApproversMap.get(Opp1.level2_approver2__c);
    
    if(delegatedApproversMap.containsKey(Opp1.level3_approver__c))  
    Opp1.level3_approver__c = delegatedApproversMap.get(Opp1.level3_approver__c); 
    
    if(delegatedApproversMap.containsKey(Opp1.level4_approver__c))  
    Opp1.level4_approver__c = delegatedApproversMap.get(Opp1.level4_approver__c);    
}             
*/  
 
  
 }