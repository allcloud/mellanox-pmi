trigger Contact_Update_Number_PCN_Notification_Account on Contact (after insert, after update) {
	List<Contact> lst_contacts_insert = new List<Contact>();
	List<Contact> lst_contacts_update = new List<Contact>();
	Set<ID> Account_IDs = new Set<ID>();
	
	if(Trigger.isInsert){
		for (Contact c : Trigger.new){
			if (c.PCN_Notification__c==true){
				Account_IDs.add(c.AccountId);
				lst_contacts_insert.add(c);
			}
		}
	}
	else {
		for (Contact c : Trigger.new){
			if(Trigger.oldmap.get(c.ID).PCN_Notification__c != c.PCN_Notification__c){
				Account_IDs.add(c.AccountId);
				lst_contacts_update.add(c);
			}
		}
	}
	if(Account_IDs.size()==0)
		return;
	
	Map<ID,Account> map_ID_Accounts = new Map<ID,Account>([select id,Number_of_PCN_Notification_Contact__c from Account where id in :Account_IDs]);
	Account tmp_acct;
	for (Contact cc : lst_contacts_insert){
		tmp_acct = map_ID_Accounts.get(cc.AccountID);
		if(tmp_acct != null){
			if(tmp_acct.Number_of_PCN_Notification_Contact__c==null)
				tmp_acct.Number_of_PCN_Notification_Contact__c = 0;
			tmp_acct.Number_of_PCN_Notification_Contact__c = tmp_acct.Number_of_PCN_Notification_Contact__c + 1;
		}
	}
	//update
	for (Contact cc : lst_contacts_update){
		tmp_acct = map_ID_Accounts.get(cc.AccountID);
		if(tmp_acct != null){
			if(tmp_acct.Number_of_PCN_Notification_Contact__c==null)
				tmp_acct.Number_of_PCN_Notification_Contact__c = 0;
			if(cc.PCN_Notification__c == true)
				tmp_acct.Number_of_PCN_Notification_Contact__c = tmp_acct.Number_of_PCN_Notification_Contact__c + 1;
			else if(cc.PCN_Notification__c == false && tmp_acct.Number_of_PCN_Notification_Contact__c>0)
				tmp_acct.Number_of_PCN_Notification_Contact__c = tmp_acct.Number_of_PCN_Notification_Contact__c - 1;
		}
	}
	
	update map_ID_Accounts.values(); 
}