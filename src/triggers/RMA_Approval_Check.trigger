trigger RMA_Approval_Check on RMA__c (before insert, before update) {
List<id> RMAIds =new List<id>();
Set<id> DeDupRMA = new Set<id>();
List<RMA__c> RMAs =new List<RMA__c>();
List<string> Parts = new List<string>();
map<string,integer> PNcount = new map<string,integer>();
map<id,List<Serial_Number__c>> RMA_To_SN = new map<id,List<Serial_Number__c>>();
List<Serial_Number__c> SNlist = new List<Serial_Number__c>();              
 for (RMA__c Rma: trigger.new)
 {
    Integer Count =0; 
          
                       
 if( trigger.isupdate &&((RMA.Approved_SN__c ==NULL &&  Rma.RMA_Approved__c == true && trigger.oldMap.get(Rma.id).RMA_Approved__c == false )||
                      (Rma.RMA_State__c == 'Rejected' && trigger.oldMap.get(Rma.id).RMA_State__c != 'Rejected')))
   {             
                                           
    RMAIds.add(Rma.id);
    RMAs.add(Rma);
    DeDupRMA.add(Rma.id);
   }
 }

  
  if(!RMAIds.isempty())
  {
    for(Serial_Number__c SN: [select id,approval__c, serial__c, Problem_Description__c,Reject_reason__c, Mellanox_PN__c, RMA__c from Serial_Number__c  SN where SN.RMA__c in:RMAIds])
    {       
      if(!RMA_to_SN.containsKey(SN.RMA__c))          
      {  SNlist.clear();               
         SNlist.add(SN);            
         RMA_to_SN.put(Sn.RMA__c,SNlist);    
      } else              
      {              
        SNlist = RMA_to_SN.get(Sn.RMA__c);            
        SNlist.add(SN);            
        RMA_to_SN.put(Sn.RMA__c,SNlist);    
      }
   }                                 
  
   for (  RMA__c r :RMAs)
      {       
       PNcount.clear();
       for(Serial_number__c s:RMA_to_SN.get(r.id))            
       { 
         if(S.approval__c=='Approved')
         { 
           if(r.Approved_SN__c == null){ r.Approved_SN__c = S.serial__c  + '     - '+ s.Problem_Description__c; } 
           else{ r.Approved_SN__c = r.Approved_SN__c +'\n'+ s.serial__c +'     - '+ s.Problem_Description__c ;}               
              
           if (!PNcount.containsKey(s.Mellanox_PN__c))            
            { PNcount.put(s.Mellanox_PN__c,1);
            }else
             {PNcount.put(s.Mellanox_PN__c, PNcount.get(s.Mellanox_PN__c)+1); }                
                                                
          }             
       if(S.approval__c=='Not Approved')
         { 
           if(R.Rejected_SN__c == null){ R.Rejected_SN__c = S.serial__c + '  Reason: ' + S.Reject_reason__c; } 
           else{ R.Rejected_SN__c = R.Rejected_SN__c +'\n'+ S.serial__c + '  Reason: ' + S.Reject_reason__c ;}    
         }                 
                                  
       } // for:Serial Numbers
       for(string PN:PNcount.keyset()) //if RMA
        {                 
          if(r.PN_List__c == NULL) {r.PN_List__c = PN + '      Count:' +PNcount.get(PN); }
          else {r.PN_List__c = r.PN_List__c + '\n'+ PN + '      Count:' +PNcount.get(PN);}  
        } 
  } // for RMA
 }
}