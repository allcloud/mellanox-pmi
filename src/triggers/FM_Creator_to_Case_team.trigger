trigger FM_Creator_to_Case_team on FM_Discussion__c (after insert, after update) 
{
 
  List<FM_Discussion__c> changedFMs = new List<FM_Discussion__c>();
  List<CaseTeamMember> insCaseTeamMember = new List<CaseTeamMember>(); 
  List<ID> caseId = new List<ID>();
  Boolean flag=false;
  String query = 'Select MemberId, TeamRoleId From CaseTeamMember Where (' ;
  for(FM_Discussion__c fm : Trigger.new )
  {
    
    if((Trigger.isInsert && fm.source__c.startsWith('Mellanox User') && fm.CreatedById != ENV.emailToCaseUser && fm.CreatedById!= ENV.IntegrationUser ) ||
     ( Trigger.Isupdate && fm.source__c == 'Mellanox User' && trigger.oldmap.get(fm.id).source__c == NULL && fm.CreatedById != ENV.emailToCaseUser && fm.CreatedById!= ENV.IntegrationUser)  )
    {
            
         
          changedFMs.Add(fm);
       
        if(flag)
        {
            query+=' or ';
        }
        query+='(ParentId=\''+fm.case__c+'\' and MemberId=\''+fm.CreatedById+'\')';
        flag=true;
        caseId.Add(fm.case__c);
    
    }
  
  query+=')';
  
   
    try
    {   
        system.debug('query: ' +query);
        
        // Patch to avoid SOQL with empty parameters
        if (query != 'Select MemberId, TeamRoleId From CaseTeamMember Where ()' ) {
          List<CaseTeamMember> delTeamMember = Database.query(query);
          delete delTeamMember;
        }
    }
    Catch(Exception e)
    {
    }
  }
  
  for(FM_Discussion__c fm1: changedFMs)
  {
    CaseTeamMember ctmNew = new CaseTeamMember();
    ctmNew.ParentId = fm1.case__c;
    ctmNew.MemberId = fm1.createdbyId; 
    system.debug('blat ctmNew.MemberId '+ctmNew.MemberId);
    system.debug('blat newCase.Id '+ctmNew.ParentId);
    ctmNew.TeamRoleId = ENV.teamRoleMap.get('FAE');
    insCaseTeamMember.add(ctmNew);
   
     }

  if(insCaseTeamMember.size()>0)
  {
     insert insCaseTeamMember;
  }
  
}