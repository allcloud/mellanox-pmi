trigger DeDupMarketingLead on Lead (after insert) {

List<string> newLeadEmail = new List<string>();
map<string,Lead> LeadInfo = new map<string,Lead>();
set<id> newLeads = new set<id>();


for(lead ld:trigger.new)
{
 if(ld.Web_marketing_lead__c == True) 
  {
   newLeadEmail.add(ld.email);
   LeadInfo.put(ld.email,ld);
   newLeads.add(ld.id);
   }
}     
   
 if(newLeadEmail.size() >0 )
{
 LeadDeDupLead DeDup = new LeadDeDupLead();
 DeDup.query = 'select id,firstName,lastName, email,LeadSource, Lead_Source_Category__c,Web_form_campaign1__c,Lead_score__c FROM Lead WHERE email in '+ ENV.getStringsForDynamicSoql(newLeadEmail);
 DeDup.LeadInfo = LeadInfo;  
 DeDup.NewLeads = newLeads;
 ID batchprocessid = Database.executeBatch(DeDup);      
     
 LeadDeDupContact DeDupCnt = new LeadDeDupContact();
 DeDupCnt.query = 'select id, email,LeadSource, Lead_Source_Category__c  FROM Contact WHERE email in '+ ENV.getStringsForDynamicSoql(newLeadEmail);
 DeDupCnt.LeadInfo = LeadInfo;  
 ID batchprocessid1 = Database.executeBatch(DeDupCnt);      
 
    
} 
  
   
   
}