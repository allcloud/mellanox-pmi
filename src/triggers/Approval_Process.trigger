trigger Approval_Process on Distributor_Oppy_Registrations__c (after insert, after update) {
//Submit records for approval  the if condition matches with entry criteria  
    for(Distributor_Oppy_Registrations__c OppReg: Trigger.new)     
      {   
         if(trigger.isinsert) 
          {    
           if (OppReg.Registration_Status__c == 'Submitted'){   // && OppReg.ApprovalProcess__c == FALSE) {
             approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
             req1.setComments('Submitting request for approval.'); 
             req1.setObjectId(OppReg.id);
             //req1.setNextApproverIds(idList);
             approval.ProcessResult result = Approval.process(req1); 
             System.assert(result.isSuccess());  
             System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
             
           }
          }
            
          if(trigger.isupdate) 
          {     
           if( OppReg.Registration_Status__c == 'Approved' &&             
               OppReg.Discount_Granted__c > trigger.oldmap.get(OppReg.id).Discount_Granted__c &&               
               trigger.oldmap.get(OppReg.id).Discount_Granted__c>0 )
              {
               approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
               req1.setComments('Approval required since Discount Granted was changed'); 
               req1.setObjectId(OppReg.id);
               //req1.setNextApproverIds(idList);
               approval.ProcessResult result = Approval.process(req1); 
               System.assert(result.isSuccess());  
               System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
               
             }
           }

 }
}