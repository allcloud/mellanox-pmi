trigger Upd_AE_Engineer on Case (after update) {

 list<id> CaseIds = new List<id>();
 map<id,case> RelCase = new map<id,case>();    
                
 for(Case cs1:trigger.new){
      CaseIds.add(cs1.id);}   
                             
 for(case C:[select state__c, status, Assignee__c, id, AE_engineer__c, Escalated_AE_Time__c FROM Case c Where c.id in :CaseIds])       
    {RelCase.put(C.id,C);}       
    
                                                  
 for(Case cs:trigger.new){
  
  if(RelCase.containsKey(cs.id))
  {  
   if(Cs.state__c == 'Open AE' && cs.AE_engineer__c != NULL&& ENV.IsInQueue(cs.AE_engineer__c, 'AE Queue')== TRUE)
    {   RelCase.get(cs.id).state__c = 'Assigned AE';
        RelCase.get(cs.id).Assignee__c= cs.AE_engineer__c; 
        update(RelCase.get(cs.id));  
               
        //List<EmailTemplate> et = [Select Id From EmailTemplate Where DeveloperName='SUPPORT: New User Created Notification'];
  Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
  //String[] toAddresses = new String[]{'innag@mellanox.co.il'};
  //mail.setToAddresses(toAddresses);
    mail.setTemplateId('00X500000016r0u');   // "SUPPORT: Case escalation notification" template
    mail.setTargetObjectId('0035000000dlVS5'); // Contact AE queue with Yaelis email
    mail.setWhatId(cs.id); 
    mail.setReplyTo('supportadmin@mellanox.com'); 
    mail.setSenderDisplayName('Mellanox Support Admin');
    mail.setSaveAsActivity(false);
    mail.setBccSender(false);
    mail.setUseSignature(false);       
  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail}); 
  }

  if(Cs.state__c == 'Assigned AE' && cs.AE_engineer__c == NULL && ENV.IsInQueue(cs.Assignee__c, 'AE Queue'))         
    {RelCase.get(cs.id).AE_engineer__c = cs.Assignee__c;               
     RelCase.get(cs.id).Escalated_AE_Time__c = system.now();           
     update(RelCase.get(cs.id));
    }

   
   }
 }   
}