trigger PCN_add_sharingrules_AfterUpdate on PCN__c (before update) {
	Set<ID> set_IDs = new Set<ID>();
	List<PCN__c> lst_pcn_approval_required = new List<PCN__c>();
	
	for (PCN__c p : Trigger.new){
		if(Trigger.oldmap.get(p.ID).Type__c=='Private' && (p.Type__c=='FYI' || p.Type__c=='Approval Required') ){
			set_IDs.add(p.id);
			if(p.Type__c=='Approval Required')
				lst_pcn_approval_required.add(p);	
		}
	}
	if(set_IDs.size()==0)
		return;
		
		Map<ID,Set<ID>> map_pcn_setContacts_approvers_byOPNs = new Map<ID,Set<ID>>();
		Map<ID,Set<ID>> map_pcn_setContacts_approvers = new Map<ID,Set<ID>>();
		
		Map<ID,Set<ID>> map_pcn_setContacts = new Map<ID,Set<ID>>();
        Map<ID,List<PCN_Contacts__c>> map_pcn_list_pcncontacts = new Map<ID,List<PCN_Contacts__c>>();
        
        List<PCN_Contacts__c> tmp_lst_pcnContacts;
        List<PCN_Contacts__c> pcn_contacts = new List<PCN_Contacts__c>([select id,PCN__c,Contact__c,Contact_Approver__c,is_Approver_by_OPN__c,Approved__c 
        																	from PCN_Contacts__c 
                                                                            where PCN__c in :set_IDs and Contact__c != null
                                                                            ]);
        
        for (PCN_Contacts__c p : pcn_contacts){
            if(map_pcn_list_pcncontacts.get(p.PCN__c) == null) {
                tmp_lst_pcnContacts = new List<PCN_Contacts__c>();
                tmp_lst_pcnContacts.add(p);
                map_pcn_list_pcncontacts.put(p.PCN__c,tmp_lst_pcnContacts);
            }
            else {
                tmp_lst_pcnContacts = map_pcn_list_pcncontacts.get(p.PCN__c);
                tmp_lst_pcnContacts.add(p);
                map_pcn_list_pcncontacts.put(p.PCN__c,tmp_lst_pcnContacts);
            }
        }
        //
        Set<ID> all_Contacts = new Set<ID>();
        Set<ID> tmp_Contacts = new Set<ID>();
        //old logics for approvers
        Set<ID> tmp_Contacts_approvers = new Set<ID>();
        //new logics of approvers by OPNs
        Set<ID> tmp_Contacts_approvers_byOPN = new Set<ID>();
        
        for (ID iid : map_pcn_list_pcncontacts.keyset()){
            tmp_Contacts = new Set<ID>();
            tmp_Contacts_approvers = new Set<ID>();
            tmp_Contacts_approvers_byOPN = new Set<ID>();
            
            tmp_lst_pcnContacts = map_pcn_list_pcncontacts.get(iid);
            for (PCN_Contacts__c pp : tmp_lst_pcnContacts){
                //To count ONLY Contacts that are Approvers and not approve yet
                if(pp.Contact_Approver__c=='Yes' && pp.Approved__c == false)
                	tmp_Contacts_approvers.add(pp.Contact__c);	
                //Count approvers by OPNs settings
                if(pp.is_Approver_by_OPN__c==true && pp.Approved__c == false)
                	tmp_Contacts_approvers_byOPN.add(pp.Contact__c);
                //Use to find out set of all Contacts per PCN
                tmp_Contacts.add(pp.Contact__c);
                //use for query to find out userid associated w/ Contacts
                all_Contacts.add(pp.Contact__c);
            }
            map_pcn_setContacts.put(iid,tmp_Contacts);
            //Assign map PCN to Set of Contacts that are Approvers
            if(tmp_Contacts_approvers.size() > 0)
            	map_pcn_setContacts_approvers.put(iid,tmp_Contacts_approvers);
            //Assign map PCN to Set of Contacts that are Approvers by OPNs
            if(tmp_Contacts_approvers_byOPN.size() > 0)
            	map_pcn_setContacts_approvers_byOPNs.put(iid,tmp_Contacts_approvers_byOPN);
        }
        List<User> lst_users = new List<User>([select id, ContactID from User where isActive=true and ContactID in :all_Contacts]);
        Map<ID,ID> map_contactID_to_userID = new Map<ID,ID>(); 
        
        for (User u : lst_users){
            map_contactID_to_userID.put(u.ContactID, u.ID);
        }
        List<PCN__Share> lst_pcn_shares = new List<PCN__Share>();
        PCN__Share tmp_pcn_share; 
        for (ID iid : map_pcn_setContacts.keyset()){
            tmp_Contacts = map_pcn_setContacts.get(iid);
            if (tmp_Contacts != null){
                for (ID ContactID : tmp_Contacts){
					if(map_contactID_to_userID.get(ContactID) != null){
	                    tmp_pcn_share = new PCN__Share();
	                    tmp_pcn_share.ParentID = iid;
	                    tmp_pcn_share.AccessLevel = 'Read';
                        tmp_pcn_share.UserOrGroupId = map_contactID_to_userID.get(ContactID);
                        lst_pcn_shares.add(tmp_pcn_share);          
                    }
                }
            } 
        }
        //
        if(lst_pcn_shares.size()>0)
            Database.insert(lst_pcn_shares, false);	
        //Assign Total Number of Pending Approvals on PCN
        for (PCN__c p : lst_pcn_approval_required){
 			//For old logics and not approvers by OPNs
 			if(p.Approvers_By_OPN__c==false && map_pcn_setContacts_approvers != null 
 					&& map_pcn_setContacts_approvers.get(p.ID) != null)
 				p.Total_Pending_Approval__c = map_pcn_setContacts_approvers.get(p.ID).size();
 			//For approvers by OPNs PCNs
 			if(p.Approvers_By_OPN__c==true && map_pcn_setContacts_approvers_byOPNs != null 
 					&& map_pcn_setContacts_approvers_byOPNs.get(p.ID) != null )
 				p.Total_Pending_Approval__c = map_pcn_setContacts_approvers_byOPNs.get(p.ID).size();
        }
}