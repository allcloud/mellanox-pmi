// Version      Change Description                  Change made by      Change Date         Related Service Now Number
// V1.0         This trigger updates the field of   Tatiana Sematch     03/09/13            INC0033062- requested by Erez Cohen
//              Assginee and owner's manager email.                                         


trigger Case_Managers_Updates on Case (before insert, before update) {
	
	if(ENV.triggerShouldRun){
		
		ENV.triggerShouldRun = false;
		
		set <string> set_userid= new set <string> ();
			
			for(case CS:trigger.new)
			{
			   set_userid.add(CS.OwnerId);
			   set_userid.add(CS.Assignee__c);      
			}
			
			map <id,user> map_users =  new map <id,user> ([Select id, Manager_Email__c, Manager.Manager_Email__c From User  Where id IN: set_userid ]);    
			
			for(case CS:trigger.new)
			{   
				if ( CS.Assignee__c != null && string.valueof(CS.Assignee__c).startsWith(ENV.User_ID) && map_users.get(CS.Assignee__c) != null &&( (trigger.isinsert) ||
			         (trigger.isupdate && CS.Assignee__c !=  trigger.oldMap.get(CS.id).Assignee__c) ) )
				{
					system.debug( 'New Cases in which the assignee is not null:' + CS);
					system.debug( '==>email' + map_users.get(CS.Assignee__c).Manager_Email__c);
					CS.Case_Assignee_Manager_Email__c = map_users.get(CS.Assignee__c).Manager_Email__c;
					CS.Case_Assignee_Level_2_Manager_Email__c = map_users.get(CS.Assignee__c).Manager.Manager_Email__c;
				}
				
				if ( CS.OwnerId != null && string.valueof(CS.OwnerId).startsWith(ENV.User_ID)&& ( (trigger.isinsert ) || 
				     (trigger.isupdate &&CS.OwnerId != trigger.oldMap.get(CS.id).OwnerId ) ) )
				{
					system.debug( 'New Cases in which the owner is not null:' + CS);
					system.debug( '==>email' + map_users.get(CS.OwnerId).Manager_Email__c);
					CS.Case_Owner_s_Manager_Email__c = map_users.get(CS.OwnerId).Manager_Email__c;
					CS.Case_Owner_s_Level_2_Manager_Email__c = map_users.get(CS.OwnerId).Manager.Manager_Email__c;
				}
				if ( (trigger.isupdate && CS.Assignee__c == null && trigger.oldMap.get(CS.id).Assignee__c!= null) || (CS.Assignee__c != null && string.valueof(CS.Assignee__c).startsWith(ENV.Group_ID)) )
				{CS.Case_Assignee_Manager_Email__c = null;
				CS.Case_Assignee_Level_2_Manager_Email__c= null;}
				if ( CS.OwnerId != null && !string.valueof(CS.OwnerId).startsWith(ENV.User_ID))
				{CS.Case_Owner_s_Manager_Email__c = null;
				CS.Case_Owner_s_Level_2_Manager_Email__c = null;}	
			}
	}
}