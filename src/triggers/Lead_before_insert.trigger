trigger Lead_before_insert on Lead (before insert, before update) {

List<string> LeadCountry = new List<string>();
Map<string,regions__c> CountryToRegion = new  Map<string,regions__c>();
string FirstName;
integer length;
//ld.firstname = ld.firstname.substring(0,1).toUpperCase()+ ld.firstname.substring(1,ld.firstname.length).toLoweCase();
 

for(lead ld:trigger.new)
{
if(trigger.isinsert)
{
if(ld.firstname!=NULL)
{
length = ld.firstname.length();
ld.firstname  = ld.firstname.substring(0,1).toUpperCase()+ ld.firstname.substring(1,length).toLowerCase();
}

if(ld.lastname!=NULL)
{
length = ld.lastname.length();
ld.lastname  = ld.lastname.substring(0,1).toUpperCase()+ ld.lastname.substring(1,length).toLowerCase();
}

}


if (trigger.isinsert || (trigger.isupdate && ld.country__c!= trigger.oldmap.get(ld.id).country__c))
{LeadCountry.add(ld.country__c);}
}

if(!LeadCountry.isempty())
{ 
 for(regions__c rg:[select name,region__c, sales_director__c,version1__c, version2__c, version3__c, version4__c from Regions__c where name in :LeadCountry OR version1__c in :LeadCountry OR version2__c in :LeadCountry OR version3__c in :LeadCountry OR version4__c in :LeadCountry ])
 { CountryToRegion.put(rg.name, rg);
   if(rg.version1__c != NULL)
    {  CountryToRegion.put(rg.version1__c, rg); }
   if(rg.version2__c != NULL)
    {  CountryToRegion.put(rg.version2__c, rg); }
   if(rg.version3__c != NULL)
    {  CountryToRegion.put(rg.version3__c, rg); }
    if(rg.version4__c != NULL)
    {  CountryToRegion.put(rg.version4__c, rg); }
 }


for(lead ld1:trigger.new)
 { 
  if( CountryToRegion.containsKey(ld1.country__c)) 
    {ld1.region__c = CountryToRegion.get(ld1.country__c).region__c;
     ld1.sales_director__c = CountryToRegion.get(ld1.country__c).sales_director__c;
    }
    
 }


 
}

}