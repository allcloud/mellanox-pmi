trigger Upd_contact_account_by_domain on Contact (before insert) {

List<contact> UnknownContacts = new List<contact>();
List<string> ContactDomains = new List<string>();
List<Account> RelAccount = new List<Account>();
Map<string,id> DomainAccount = new Map<string,id>();
for(contact cnt:trigger.new)
{
  if(( cnt.createdbyid == ENV.webCustomer && 
     (cnt.accountId == ENV.UnknownAccount || cnt.accountId == ENV.UnknownEMEAAccount || cnt.accountId == ENV.UnknownUSAccount))
     ||
     cnt.accountId == '0015000000oqjxl') 
   ContactDomains.add(cnt.domain__c); 
   UnknownContacts.add(cnt);
} 

if(ContactDomains.size()>0) 
{                          
for(Account acc:[select id,lower_domain__c ,lower_domain1__c,lower_domain2__c   from Account where (lower_domain__c in :ContactDomains OR lower_domain1__c in :ContactDomains OR lower_domain2__c =:ContactDomains ) ORDER BY Primary_Domain__c DESC NULLS LAST, Highest_Active_contract_rank__c  DESC NULLS LAST])  
  {         
    if(acc.lower_domain__c !=NULL)       
      DomainAccount.put(acc.lower_domain__c, acc.id);
    if(acc.lower_domain1__c !=NULL)       
      DomainAccount.put(acc.lower_domain1__c, acc.id);
    if(acc.lower_domain2__c !=NULL)       
      DomainAccount.put(acc.lower_domain2__c, acc.id);
  }
}
                           
for(Contact cn:UnknownContacts)
{
  if(DomainAccount.containsKey(cn.domain__c))
   {
    cn.accountid = DomainAccount.get(cn.domain__c);
    }
}


}