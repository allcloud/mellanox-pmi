trigger Attachment_AfterInsert on Attachment (after insert)
{
	
	if (UserInfo.getUserId() == ENV.emailToCaseUser && !ENV.triggerRanOnce) {
		
		cls_trg_Attachment handler = new cls_trg_Attachment();
		handler.sortAttachmentsPrivatOrPublic(trigger.New, trigger.newMap);
		ENV.triggerRanOnce = true;  
	}
	
}