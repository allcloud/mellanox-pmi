trigger Upd_RMAstatus_by_state on RMA__c (before insert, before update) {
    
    Set<ID> RMA_IDs = new Set<ID>();
    
    for(RMA__c RMA :trigger.new){
    
        if(RMA.RMA_Status__c != null){
            
            if(RMA.RMA_state__c== 'Pending Sales Approval') { RMA.RMA_Status__c = 'Open';}
            if(RMA.RMA_state__c== 'Approved') { RMA.RMA_Status__c= 'Approved';}
            if(RMA.RMA_state__c== 'Support Group Approval') { RMA.RMA_Status__c= 'Assigned';}
            if(RMA.RMA_state__c== 'Execution in process') { RMA.RMA_Status__c= 'Approved';}
            if(RMA.RMA_state__c== 'Rejected') { RMA.RMA_Status__c= 'Rejected';}
        }
        
        if(trigger.isinsert && RMA.Mellanox_Site__c != null){
        
            if(rma.Is_OEM__c == 'Yes' || rma.RecordTypeID == ENV.Internal_RMA_RecordType){
                Rma.Mellanox_Site__c = ENV.EMEA_Mellanox_Site;
            }
            else{
                
                if(rma.equipment_location__c == 'US, CANADA, LATAM' && rma.Ship_Restrictions__c != 'non IL products' )
                {Rma.Mellanox_Site__c = ENV.US_Mellanox_Site;}
                if(rma.equipment_location__c == 'EMEA, Rest of the world' && rma.Ship_Restrictions__c != 'non IL products')
                {Rma.Mellanox_Site__c = ENV.EMEA_Mellanox_Site;}
                if(rma.equipment_location__c == 'APAC - Rest of APAC Countries' && rma.Ship_Restrictions__c != 'non IL products')
                {Rma.Mellanox_Site__c = ENV.HongKong_Mellanox_Site;}
                if(rma.equipment_location__c == 'APAC - China' && rma.Ship_Restrictions__c != 'non IL products')
                {Rma.Mellanox_Site__c = ENV.EMEA_Mellanox_Site;}
            }
        }
    }
}