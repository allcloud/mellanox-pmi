trigger Task_insert_add_PM_follower on task (after insert) {

List<string> ProjectIds = new List<string>();
List<EntitySubscription> FollowersList = new List<EntitySubscription>();
List<id> PMlist = new List<id>();
List<id> Tsklist = new List<id>();
Map<id,List<id>> ProjectToPMlist = new Map<id,List<id>>();
Map<id,List<id>> ProjectToTsk = new Map<id,List<id>>();
Map<id,string> PMtoUserType = new Map<id,string>();



if(trigger.isinsert)
{

for(Task ts:trigger.new)
 {
  if( ts.projectId__c != NULL)
  ProjectIds.add(ts.projectId__c);
  
   if(!ProjectToTsk.containsKey(ts.ProjectId__c))
     {
      Tsklist = new list<id>();
      Tsklist.add(ts.id);
      ProjectToTsk.put(ts.ProjectId__c,Tsklist);
     }else
     {
      Tsklist = ProjectToTsk.get(ts.ProjectId__c);
      Tsklist.add(ts.id);
      ProjectToTsk.put(ts.ProjectId__c,Tsklist); 
     } // else
 }
}
 
if(!ProjectIds.isempty())
{
system.debug('List of ProjectIds: ' + ProjectIds);
for(Project_member__c pm:[select Project_member__c,Project__c, Project_member__r.userType  from Project_member__c where Project__c in:ProjectIds and Project_member__c !=null])
{

system.debug('Adding PM member: ' + pm.Project_member__c );

  PMtoUserType.put(pm.Project_member__c,pm.Project_member__r.userType); 

 if(!ProjectToPMlist.containsKey(pm.Project__c))
     {
      PMlist = new list<id>();
      PMlist.add(pm.Project_member__c);
      ProjectToPMlist.put(pm.Project__c,PMlist);
     }else
     {
      PMlist = ProjectToPMlist.get(pm.Project__c);
      PMlist.add(pm.Project_member__c);
      ProjectToPMlist.put(pm.Project__c,PMlist); 
     }
} //for

if(!ProjectToPMlist.isempty())  // check if there is Project Member for project
{

for(Id prId:ProjectToTsk.keyset())
 {
   for(Id TS:ProjectToTsk.get(prId))
   {
     for(Id Pmem:ProjectToPMlist.get(prId))
        {
        
         system.debug('Follower Mem: ' + Pmem);
         system.debug('Follower Parent: ' + TS);
         EntitySubscription Follower = new EntitySubscription(); 
         if(PMtoUserType.get(pmem) != 'Standard')
          {Follower.NetworkId = ENV.CommunityId;}
         Follower.SubscriberId = Pmem;
         Follower.ParentId = TS;
         FollowersList.add(Follower);
        }//Pmem
     }  //Task
  }  


  

 
 
  insert( FollowersList);
  
 }//  if(!ProjectToPMlist.isempty()) 
} // if
}