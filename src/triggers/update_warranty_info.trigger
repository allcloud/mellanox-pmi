trigger update_warranty_info on Serial_Number__c(after insert, after update, before insert, before update) {
	List < string > UpdSNs = new List < string > ();
	List < id > SNIds = new List < id > ();
	List < string > int_UpdSNs = new List < string > ();
	List < id > int_SNIds = new List < id > ();

	Id InternalSerialRTId = [Select r.Name, r.Id From RecordType r where r.Name = 'Internal Serial'].Id;
	System.Debug('>>>> in section 1');
	if (trigger.isAfter) {

		for (Serial_Number__c SN: trigger.new) {
			if (trigger.isinsert ||
				(trigger.isupdate && SN.Serial__c != trigger.oldmap.get(SN.id).Serial__c) ||
				(trigger.isupdate && SN.Warranty_Valid_Date__c != trigger.oldmap.get(SN.id).Warranty_Valid_Date__c && SN.Asset__c == NULL)) {
				if (SN.RecordTypeId == InternalSerialRTId) {
					System.Debug('>>>> in section 2');
					int_UpdSNs.add(SN.Serial_upper__c);
					int_SNIds.add(SN.id);
				}
				else {
					UpdSNs.add(SN.Serial_upper__c);
					SNIds.add(SN.id);
				}
				system.debug('SN.id = ' + SN.id);
			}
		}
		if (UpdSNs.size() > 0) {
			AssetToSerialNum reassign = new AssetToSerialNum();
			reassign.query = 'select id,Part_Number__c, Asset_Status__c,SN_Warranty_Expire__c,Serial_Number__c, Is_OEM__c, Japan_Asset__c, RMA_type1__c from Asset2__c where  Serial_Number__c in' + ENV.getStringsForDynamicSoql(UpdSNs);
			reassign.NewSNs1 = SNIds;
			ID batchprocessid = Database.executeBatch(reassign);
		}
		if (int_UpdSNs.size() > 0) {
			system.debug('==> int_UpdSNs: ' + int_UpdSNs);
			InternalAssetToSerialNum in_reassign = new InternalAssetToSerialNum();
			in_reassign.query = 'select Ordered_Item__c,Order_Number__c,Serial_Number__c,Ship_Date__c from Asset_Internal__c where  Serial_Number__c IN:UpdSNs';
			in_reassign.UpdSNs = int_UpdSNs;
			in_reassign.NewSNs1 = int_SNIds;
			ID IAbatchprocessid = Database.executeBatch(in_reassign);

			system.debug('==>6 IAbatchprocessid: ' + IAbatchprocessid);

		}
	}


	else if (trigger.isbefore) {

		cls_trg_RMA_Serial_Numbers handler = new cls_trg_RMA_Serial_Numbers();

		if (trigger.isInsert) {

			handler.updateRelatedRMAINSAProject(trigger.New, null);
		}

		else if (trigger.isUpdate) {

			handler.updateRelatedRMAINSAProject(trigger.New, trigger.OldMap);
		}
	}
}