trigger trg_DuplicateRecordItem on DuplicateRecordItem (before Insert) {
    
    cls_trg_DuplicateRecordItem handler = new cls_trg_DuplicateRecordItem();
    
    handler.updateDupRecordItemsAccSource(trigger.New);
}