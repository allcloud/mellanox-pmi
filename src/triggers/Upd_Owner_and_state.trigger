trigger Upd_Owner_and_state on Case(before Update) {
	if(ENV.triggerShouldRun){
		ENV.triggerShouldRun = false;
		    for(Case  c:trigger.new)
		    {
		    
		        if( !string.valueOf(c.ownerid).startsWith('00G') && 
		            string.valueOf(trigger.oldmap.get(c.id).ownerid).startsWith('00G')&& 
		            c.state__c== 'Open')  
		 
		        {
		                  
		          c.assignee__c = c.ownerid;      
		          c.state__c = 'Assigned' ;
		          c.status = 'Assigned' ;
		                                
		        }
		        
		        if (c.ownerid == ENV.ST1 &&
		            string.valueOf(trigger.oldmap.get(c.id).ownerid).startsWith('00G')&& 
		            c.state__c== 'Open') 
		        {
		          c.assignee__c = userinfo.getuserid();      
		          c.state__c = 'Assigned' ;
		          c.status = 'Assigned' ;
		        }
		    }
	}
}