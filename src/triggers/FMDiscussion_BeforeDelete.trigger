//when an FM_Discussion record is deleted 
//then this trigger delete the related casecomment record
//and create a new deleted_FM_Discussion record

trigger FMDiscussion_BeforeDelete on FM_Discussion__c (before delete) 
{
    List<Deleted_FM_Discussion__c> insDeletedFMDiscussion = new List<Deleted_FM_Discussion__c>();   
    List<CaseComment> lst_deleteCaseComment = new List<CaseComment>();
    list<String> caseCommentsIdsAsStr_List = new list<String>();
    set<String> caseCommentsIds_Set = new set<String>();
    
    for(FM_Discussion__c fmd : Trigger.old) {
      
        insDeletedFMDiscussion.Add(new Deleted_FM_Discussion__c(Name=fmd.Id));
        if(fmd.Case_Comment_ID__c != null) {
        
        	caseCommentsIdsAsStr_List = fmd.Case_Comment_ID__c.split(',');
        	caseCommentsIds_Set.addAll(caseCommentsIdsAsStr_List);
        }
    }
    if(!insDeletedFMDiscussion.isEmpty())
    {
        insert insDeletedFMDiscussion;
    }
    
    list<CaseComment> caseCommsnts2Delete_List = new list<CaseComment>();
        
    if (!caseCommentsIds_Set.isEmpty()) {
    	
    	for (String caseCommentId_Str : caseCommentsIds_Set) {
    		
    		CaseComment caseComment2Delete = new CaseComment(Id = caseCommentId_Str);
    		caseCommsnts2Delete_List.add(caseComment2Delete);
    	}
    	
    	if (!caseCommsnts2Delete_List.isEmpty()) {
    		
    		delete caseCommsnts2Delete_List;
    	}
    }   
}