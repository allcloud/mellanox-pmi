trigger upd_RMA_on_Case on Case (after insert,after update) {

List<string> new_RMA_cs = new List<string>();

List<RMA__c> RMAs = new List<RMA__c>();
List<string> RMA_type_cases = new List<string>();
List<string> RMA_state_cases = new List<string>(); 
//map<string,string> RMA_to_Case  = new  map<string,string>(); 
map<string,RMA__c> RMAid_to_RMA  = new  map<string,RMA__c>(); 
map<string,List<Serial_Number__c>> SN_to_RMA  = new  map<string,List<Serial_Number__c>>(); 
List<Serial_Number__c> SNs = new List<Serial_Number__c>();
Map<id,string> FA_CaseToRMA = new Map<id,string>();
string AssigneeID;
user AssigneeUser = new user(); 
List<Serial_Number__c> All_SNs = new List<Serial_Number__c>();
List<RMA__c> Rel_RMA = new List<RMA__c>();


for(case cc:trigger.new)
{
 if(cc.RecordTypeId == '01250000000Dtey')  // RMA case
  {
   if(trigger.isupdate && ( ( cc.state__c == 'RMA Approved' && trigger.oldmap.get(cc.id).state__c != 'RMA Approved') || (cc.state__c == 'RMA Canceled'  && trigger.oldmap.get(cc.id).state__c != 'RMA Canceled' )) )
    {AssigneeID = cc.Assignee__c;}
    if(trigger.isupdate && (cc.RMA_type__c !=trigger.oldmap.get(cc.id).RMA_type__c))
      {RMA_type_cases.add(cc.RMA_request__c);}   
 
  if(trigger.isupdate && cc.Failure_analysis_request__c != trigger.oldmap.get(cc.id).Failure_analysis_request__c )
   { FA_CaseToRMA.put(cc.RMA_request__c, cc.Failure_analysis_request__c );}

    if(trigger.isupdate && ((cc.state__c == 'RMA Approved' && trigger.oldmap.get(cc.id).state__c != 'RMA Approved')
                           || (cc.state__c == 'RMA Canceled' && trigger.oldmap.get(cc.id).state__c != 'RMA Canceled')
                           || (cc.state__c == 'Move to OEM RMA'&& trigger.oldmap.get(cc.id).state__c != 'Move to OEM RMA')
                           || (cc.Failure_analysis_request__c != trigger.oldmap.get(cc.id).Failure_analysis_request__c)  ))                       
     {RMA_state_cases.add(cc.RMA_request__c);} 
  
  
    if(trigger.isinsert)
     {new_RMA_cs.add(cc.RMA_request__c);}
     
  }   

}


if(!new_RMA_cs.isempty())

{
  RMAs = [select id, RMA_state__c , RMA_Status__c, contact__c from RMA__c where id in:new_RMA_cs ];
}


if(!RMA_type_cases.isempty() || !RMA_state_cases.isempty())
{   All_SNs = [select id,Approval__c, RMA__c, RMA_type_by_support__c from serial_number__c where RMA__c in: RMA_type_cases or RMA__c in: RMA_state_cases  ];
    Rel_RMA = [select id, RMA_state__c , RMA_Status__c,Advanced__c, rma_Approved__c,Replaced_Japan_SN__c from RMA__c where id in:RMA_state_cases  ];
   /* if(AssigneeID != NULL)     
     {AssigneeUser =  [select id, email from user where id =:AssigneeID];}*/
  
  for(RMA__c R:Rel_RMA)    
    {RMAid_to_RMA.put(R.id,R);}
    
  for(Serial_number__c SN1:All_SNs)
   {
    if(!SN_to_RMA.containsKey(SN1.RMA__c))
    {
      SNs.clear();
      SNs.add(SN1); 
      SN_to_RMA.put(SN1.RMA__c,SNs);
    }
    else
    {
     SNs=SN_to_RMA.get(SN1.RMA__c); 
     SNs.add(SN1);
     SN_to_RMA.put(SN1.RMA__c,SNs);
    }
  }
}



  
for(case cc:trigger.new)
{
    if(trigger.isupdate &&  cc.state__c == 'RMA Approved' && trigger.oldmap.get(cc.id).state__c != 'RMA Approved' )
     { 
      if(SN_to_RMA.containsKey(cc.RMA_request__c))
      {
       for(Serial_number__c SN:SN_to_RMA.get(cc.RMA_request__c))
        {
         if(SN.Approval__c!= 'Not Approved')
           {SN.Approval__c = 'Approved';}
        }
       }
       for(RMA__c rm:Rel_RMA )
        {
         if(cc.Japan_Asset_Replaced__c == 'Yes')
         {rm.RMA_state__c = 'Shipped';
          rm.Replaced_Japan_SN__c = cc.Japan_Asset_Replacment_SN__c;}
         else{
         rm.RMA_state__c = 'Approved';
         rm.RMA_status__c = 'Approved'; 
          }
         rm.sup_case_assignee__c = cc.assignee__c;  
        if(cc.assignee_email__c.length()>0){  rm.assignee_email__c = cc.assignee_email__c;  }
         rm.rma_approved__c = true;
         }      
      }  
//----------------------update RMA on Failure_analysis_request ---------------------

  if(!FA_CaseToRMA.keySet().isempty())
   {  
     for(RMA__c rm:Rel_RMA )
        {rm.Failure_analysis_request__c = FA_CaseToRMA.get(cc.RMA_request__c);}
   }
//---------------------- Move to RMA Canceled ------------------------------------

   if(cc.RMA_Support_Project__c == null && trigger.isupdate && cc.state__c == 'RMA Canceled'  && trigger.oldmap.get(cc.id).state__c != 'RMA Canceled' )
     { 
       for(Serial_number__c SN:SN_to_RMA.get(cc.RMA_request__c))
        {                 
          SN.Approval__c = 'Not Approved';     
          SN.Reject_reason__c = cc.RMA_reject_reason__c;
        }
        
       for(RMA__c rm:Rel_RMA )
        {rm.RMA_state__c = 'Rejected';
         rm.RMA_status__c = 'Rejected';  
         rm.sup_case_assignee__c = cc.assignee__c;  
        if(cc.assignee_email__c != null && cc.assignee_email__c.length()>0){ rm.assignee_email__c = cc.assignee_email__c;}      
         }        
      }
//--------------------------- Move to  'Move to OEM RMA' ------------------------------------
   
  if(trigger.isupdate && cc.state__c == 'Move to OEM RMA'&& trigger.oldmap.get(cc.id).state__c != 'Move to OEM RMA')
   {
   for(RMA__c Rm:Rel_RMA)
    {
     Rm.RMA_state__c =  'Pending Sales Approval';
    }

   
   
   }
//-------------------------------------------All SN Advanced---------------------------

  if(trigger.isupdate && cc.RMA_type__c !=trigger.oldmap.get(cc.id).RMA_type__c)
   {
                
    if(SN_to_RMA.containsKey(cc.RMA_request__c))
    {
     for(Serial_number__c SN:SN_to_RMA.get(cc.RMA_request__c))
     {SN.RMA_type_by_support__c = cc.RMA_type__c; }        
        
    }      
   }
 //-------------------------------- Insert RMA ----------------------------- 
 if(trigger.isinsert &&!RMAs.isempty())   
  {  
    for(RMA__c Rm:RMAs)
    {
     Rm.RMA_state__c =  'Support Group Approval';
     Rm.Case_Parent__c = cc.id;
     RM.Contact__c = cc.contactId;
    }
}

  
                              

          
 } //cases for
update(All_SNs);
update(Rel_RMA);
update(RMAs);
update(RMAid_to_RMA.values());

}