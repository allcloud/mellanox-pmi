trigger Pr_Attachment_after_insert on PrivateAttachment__c (after insert) {

list<id> PAcases = new list<id>();
map<id,id> caseAttach = new map<id,id>();
list<case> updCases = new list<case>();


if(trigger.isinsert)
{
 for(PrivateAttachment__c PA:trigger.new)
  {
    PAcases.add(PA.case__c);      
    caseAttach.put(PA.case__c,PA.id);        
  }
}


for(case cs:[select id, private_attachments__c,ownerid from case where id in:PAcases])
{
  cs.private_attachments__c = caseAttach.get(cs.id);
  updCases.add(cs);
}

System.debug('... KN updcases ===' + updcases);

update updcases;
}