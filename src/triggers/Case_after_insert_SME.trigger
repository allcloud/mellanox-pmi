trigger Case_after_insert_SME on Case (after insert, after update) {

if(ENV.triggerShouldRun){
	
	ENV.triggerShouldRun = false;
	List<SME_Table__c> RelProds = new List<SME_Table__c>();
	List<string> ProdFam = new List<string>();
	Map<id,set<id>> casesSMEs = new Map<id,set<id>>();
	set<id> casesSet = new set<id>();
	Map<string,Set<string>> updUsr = new map<string,Set<string>>();
	Set<string> members = new Set<string>();
	Set<id> SMEs = new  Set<id>(); 
	List<id> SMEList = new  List<id>(); 
	List<id> Cases = new  List<id>(); 
	
	
	
	Map<id,string> Case_ProdFam = new Map<id,string> ();
	
	
	List<SME_Table__c> RelProd = new  List<SME_Table__c>();
	
	
	Map<id,id> Case_TeamMember = new Map<id,id> ();
	
	Map<string,set<id>> ProdFam_map = new Map<string,set<id>> ();
	
	
	
	List<CaseTeamMember> insCaseTeamMember = new  List<CaseTeamMember>();
	List<CaseTeamMember> AllTeamMembers = new  List<CaseTeamMember>();
	
	
	
	
	
	
	for (Case cs:trigger.new)
	{ 
	 if(((trigger.isinsert && cs.Board_Part_Number__c != null)||
	    (trigger.isupdate && cs.Board_Part_Number__c != trigger.oldmap.get(cs.id).Board_Part_Number__c && cs.Board_Part_Number__c != Null)||   
	    (trigger.isupdate && cs.priority != trigger.oldmap.get(cs.id).priority)||
	    (trigger.isupdate && cs.business_impact__c != trigger.oldmap.get(cs.id).business_impact__c))&&
	     cs.Case_type__c == 'System Case'&& (cs.priority =='High'||cs.priority =='Fatal'||cs.business_impact__c == 'High' || cs.business_impact__c == 'Critical' ))
	 {
	  ProdFam.add(cs.Board_Part_Number__c);
	  CasesSet.add(cs.id);
	  Cases.add(cs.id);
	  Case_ProdFam.put(cs.id,cs.Board_Part_Number__c);
	  }
	 } 
	  
	if(!ProdFam.isempty())
	
	{  
	  
	   RelProd =  [select id, name,Product_SME__c from SME_Table__c where name in :ProdFam];
	if(!RelProd.isempty())
	{
	  for(SME_Table__c pm:RelProd)
	  
	  {    
	   if(!ProdFam_Map.containsKey(pm.name)) 
	   {
	    SMEs.clear(); 
	    SMEs.add(pm.Product_SME__c);
	    ProdFam_Map.put(pm.name,SMEs);  
	   }else
	    {
	     SMEs =ProdFam_Map.get(pm.name);
	     SMEs.add(pm.Product_SME__c); 
	     ProdFam_Map.put(pm.name,SMEs); 
	    
	    }
	  }
	  
	
	
	
	AllTeamMembers = [Select MemberId, TeamRoleId,ParentId From CaseTeamMember Where ParentId in :Cases];
	system.debug('AllTeamMembers - '+ AllTeamMembers);
	
	if(!AllTeamMembers.isempty())
	 {
	   for(CaseTeamMember TM:AllTeamMembers)
	    { 
	       if(ProdFam_Map.get(Case_ProdFam.get(TM.ParentId)).contains(TM.MemberId)) // Already exists in CaesTeam  
	        { 
	         SMEs = ProdFam_Map.get(Case_ProdFam.get(TM.ParentId));
	         SMEs.remove(TM.MemberId);   
	         ProdFam_Map.put(Case_ProdFam.get(TM.ParentId),SMEs);
	         }
	    }  
	  }          
	
	System.debug('...KN ProdFam_Map===' + ProdFam_Map);
	     
	for(Id UpdCs: Cases)
	{
	  SMEList.clear();
	  SMEList.addAll(ProdFam_Map.get(Case_ProdFam.get(UpdCs)));  
	System.debug('...KN SMEList===' + SMEList);
	 for(id sm:SMEList)
	 {
	    CaseTeamMember ctmNew = new CaseTeamMember();
	    ctmNew.ParentId = UpdCs;
	    ctmNew.MemberId = sm;
	    ctmNew.TeamRoleId = ENV.teamRoleMap.get('SME');
	    insCaseTeamMember.add(ctmNew);
	  } 
	  
	}
	System.debug('...KN insCaseTeamMember===' + insCaseTeamMember);
	  if(insCaseTeamMember.size()>0)
	  {
	    insert insCaseTeamMember;
	  }
	 }
	 
	}
}
}