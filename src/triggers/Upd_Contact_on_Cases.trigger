trigger Upd_Contact_on_Cases on Case (after update,after insert, after delete) {

List<id> Upd_AccountIds = new List<id>();
List<account> Upd_Accounts = new List<account>();
map<id,case> map_Account_Closed_Cases = new map<id,case>();

if (trigger.isdelete)
{
	for(case cs:trigger.old) {
		 
	 	Upd_AccountIds.add(cs.accountId);
   		map_Account_Closed_Cases.put(cs.accountId, cs);
	}
}
if  (trigger.isinsert || trigger.isupdate )
{
	for(case cs:trigger.new)
	{
	  if(trigger.isinsert || (trigger.isupdate && cs.state__c == 'Closed' && trigger.oldmap.get(cs.id).state__c != 'Closed'))
	   Upd_AccountIds.add(cs.accountId);
	   map_Account_Closed_Cases.put(cs.accountId, cs);
	
	}
}

 
if(!Upd_AccountIds.isempty())
{
 Upd_Accounts = [select id, createdDate, last_case_date__c, last_case_age__c, last_case_isFatal__c, Number_Of_Cases__c from Account where id in:Upd_AccountIds];
 
 for(Account acc:Upd_Accounts)
 {
   acc.apex_updated__c =System.currentTimeMillis(); //to overcome the validation for non changing account for profiles
   
   if(trigger.isinsert)
    {
	    acc.last_case_date__c = NULL;
	    acc.last_case_created_date__c = system.now();
	    
	    if (acc.Number_Of_Cases__c == NULL)
	    { acc.Number_Of_Cases__c = 1;}
	    else
	    { acc.Number_Of_Cases__c = acc.Number_Of_Cases__c + 1;}
    }
   if(trigger.isupdate)
   {
     acc.last_case_date__c = system.now();
     acc.last_case_email__c = map_Account_Closed_Cases.get(acc.Id).ContactEmail__c;
     acc.last_case_age__c = date.valueof(map_Account_Closed_Cases.get(acc.Id).CreatedDate).daysBetween( date.valueof(map_Account_Closed_Cases.get(acc.Id).ClosedDate));
     if(map_Account_Closed_Cases.get(acc.Id).priority == 'Fatal') acc.last_case_isFatal__c =TRUE;
      else 
         acc.last_case_isFatal__c = FALSE;
    }
   
    if(trigger.isdelete) {
    	
    	 acc.Number_Of_Cases__c = (acc.Number_Of_Cases__c != null) ? acc.Number_Of_Cases__c - 1 : null;
	}     
         
  } 
  
 update(Upd_Accounts);  
 }
 
 }