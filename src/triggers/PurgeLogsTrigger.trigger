//This trigger is used to purge/delete the community log records once the configured purge limit is passed 
trigger PurgeLogsTrigger on Logger__c (after insert) {
	try{
		system.debug('inside after PurgeLogsTrigger--');
		if(Trigger.isAfter && Trigger.isInsert){
			LoggerSettings__c loggerSettings = JiveAPIUtil.getLoggerSettings();
			//system.debug('after trigger count --'+[select count(id) from Logger__c]);
			if(loggerSettings != null){
				Integer purgeLimit = Integer.valueOf(loggerSettings.PurgeRecordLimit__c);
				system.debug('purgeLimit--'+purgeLimit);
								
				if(purgeLimit > 0){
					//Get all the old logger records which fall beyond the configured purge limit
					List<Logger__c> loggersBeyondPurgeLimit = [select id from Logger__c order by createdDate desc limit 10000 OFFSET :purgeLimit];
					system.debug('loggersBeyondPurgeLimit--'+loggersBeyondPurgeLimit);
					if(!loggersBeyondPurgeLimit.isEmpty()){
						delete loggersBeyondPurgeLimit;
					}
				}
			}

		}
	}catch(Exception ex){
		system.debug('Exception in PurgeLogsTrigger--'+ex.getMessage());
	}
}