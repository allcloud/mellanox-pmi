trigger RegisProduct_Set_Approved_Cost on Opp_Reg_OPN__c (after insert, after update) {
	Set<ID> prod_IDs = new Set<ID>();
	Set<ID> set_ids = new Set<ID>();
	if (Trigger.isInsert){
		for (Opp_Reg_OPN__c oreg : Trigger.New){
			set_ids.add(oreg.ID);
			prod_IDs.add(oreg.OPN__c);
		}
	}
	else {
		for (Opp_Reg_OPN__c oreg : Trigger.New){
			if (Trigger.oldmap.get(oreg.ID).OPN__c != oreg.OPN__c)
				set_ids.add(oreg.ID);
				prod_IDs.add(oreg.OPN__c);
		}
	}
	if (set_ids.size()==0)
		return;
	List<Opp_Reg_OPN__c> regis_prod = new List<Opp_Reg_OPN__c>([select id,OPN__c,Opp_Reg__r.Discount_Granted__c,Opp_Reg__r.Partner_First_Registration__c,
																	Opp_Reg__r.Certified_Reseller_Registration__c,Opp_Reg__r.sewp__c,
																	Opp_Reg__r.OEM_Pricing_SEWP__c,
																	Opp_Reg__r.Partner_Reseller_Name__c,Approved_Cost__c,
																	Opp_Reg__r.SD_Region_Hidden__c
																from Opp_Reg_OPN__c where id in :set_ids]);
	ID certified_reseller_PB = '01s500000006J7K';
	ID Disty_PB = '01s500000006J09';
	ID Dell_PB = '01s500000006MJd';
	ID DB1_PB = '01s500000006J7P';
	ID OEM_PB = '01s500000006J7F';
	
	List<PricebookEntry> lst_pbe = new List<PricebookEntry>([select id,Product2ID,Pricebook2ID,UnitPrice from PricebookEntry 
																where Product2ID in :prod_IDs 
																and (Pricebook2ID = '01s500000006J7K' or Pricebook2ID='01s500000006J09'
																		or Pricebook2ID = '01s500000006MJd' or Pricebook2ID='01s500000006J7P'
																		or Pricebook2ID='01s500000006J7F')]);
	Map<ID,Map<ID,Double>> map_prodID_PricebookID_Price = new Map<ID,Map<ID,Double>>();
	Map<ID,Double> tmp_map_PricebookID_Price = new Map<ID,Double>();  
	for (PricebookEntry pbe : lst_pbe){
		if (map_prodID_PricebookID_Price.get(pbe.Product2ID)==null){
			tmp_map_PricebookID_Price = new Map<ID,Double>();
			tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
			map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
		}
		else {
			tmp_map_PricebookID_Price = map_prodID_PricebookID_Price.get(pbe.Product2ID);
			tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
			map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
		}
	}																					
	
	for (Opp_Reg_OPN__c oppreg : regis_prod){
		//Set DB Cost -03-06-15
		if(map_prodID_PricebookID_Price!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Disty_PB)!=null)
				oppreg.DB_Cost__c = map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Disty_PB);
		//
		if (oppreg.Opp_Reg__r.Partner_First_Registration__c==true || oppreg.Opp_Reg__r.Certified_Reseller_Registration__c==true){
			if (map_prodID_PricebookID_Price != null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(certified_reseller_PB)!=null)	
				//oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(certified_reseller_PB));
				oppreg.Approved_Cost__c = map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(certified_reseller_PB);
		}
		//SEWP - government contracts or OEM checkbox is checked
		else if (oppreg.Opp_Reg__r.sewp__c==true || oppreg.Opp_Reg__r.OEM_Pricing_SEWP__c==true){
			if (map_prodID_PricebookID_Price != null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(OEM_PB)!=null)	
				//oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(OEM_PB));
				oppreg.Approved_Cost__c = map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(OEM_PB);	
		}
		//	
		//DELL
		else if (oppreg.Opp_Reg__r.Partner_Reseller_Name__c!='' && oppreg.Opp_Reg__r.Partner_Reseller_Name__c.containsIgnoreCase('Dell')){
			if(map_prodID_PricebookID_Price!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Dell_PB)!=null && oppreg.Opp_Reg__r.Discount_Granted__c!=null)
				//oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Dell_PB) * (100 - oppreg.Opp_Reg__r.Discount_Granted__c) / 100);
				oppreg.Approved_Cost__c = map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Dell_PB) * (100 - oppreg.Opp_Reg__r.Discount_Granted__c) / 100.0;
		}
		//Exertis
		//
		else if (oppreg.Opp_Reg__r.Partner_Reseller_Name__c!='' && oppreg.Opp_Reg__r.Partner_Reseller_Name__c.containsIgnoreCase('Exertis') 
					&& (oppreg.Opp_Reg__r.SD_Region_Hidden__c=='EMEA' || oppreg.Opp_Reg__r.SD_Region_Hidden__c=='EUR') ){
			if(map_prodID_PricebookID_Price!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Dell_PB)!=null && oppreg.Opp_Reg__r.Discount_Granted__c!=null)
				oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Dell_PB) * (100 - oppreg.Opp_Reg__r.Discount_Granted__c) / 100);
		}			
		//Tech Express
		//
		else if (oppreg.Opp_Reg__r.Partner_Reseller_Name__c!='' && ( oppreg.Opp_Reg__r.Partner_Reseller_Name__c.containsIgnoreCase('Tech Express') 
					|| oppreg.Opp_Reg__r.Partner_Reseller_Name__c.containsIgnoreCase('TechExpress') ) ){
			if(map_prodID_PricebookID_Price!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Dell_PB)!=null && oppreg.Opp_Reg__r.Discount_Granted__c!=null)
				oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Dell_PB) * (100 - oppreg.Opp_Reg__r.Discount_Granted__c) / 100);
		}			
		//
		//5% Discount -- Would be DB1 pricebook
		else if (oppreg.Opp_Reg__r.Discount_Granted__c==5.0){
			if (map_prodID_PricebookID_Price != null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(DB1_PB)!=null)	
				//oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(DB1_PB));
				oppreg.Approved_Cost__c = map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(DB1_PB);
		}
		//
		//Colfax
		//Comment out per Luanne's request
		/*
		else if (oppreg.Opp_Reg__r.Partner_Reseller_Name__c!='' && oppreg.Opp_Reg__r.Partner_Reseller_Name__c.containsIgnoreCase('Colfax')){
			if(map_prodID_PricebookID_Price!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(DB1_PB)!=null && oppreg.Opp_Reg__r.Discount_Granted__c!=null)
				oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(DB1_PB) * (100 - oppreg.Opp_Reg__r.Discount_Granted__c) / 100);
		}			
		*/		
		else {
			if(map_prodID_PricebookID_Price!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c)!=null && map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Disty_PB)!=null && oppreg.Opp_Reg__r.Discount_Granted__c!=null)
				//oppreg.Approved_Cost__c = Math.round(map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Disty_PB) * (100 - oppreg.Opp_Reg__r.Discount_Granted__c) / 100);
				oppreg.Approved_Cost__c = map_prodID_PricebookID_Price.get(oppreg.OPN__c).get(Disty_PB) * (100 - oppreg.Opp_Reg__r.Discount_Granted__c) / 100.0;
		}
	}
	
	update regis_prod;
															
}