trigger Milestone_after_insert on Milestone1_Milestone__c (after insert,after update) {

List<id> ProjectIds = new List<id>();
List<EntitySubscription> FollowersList = new List<EntitySubscription>();
List<id> PMlist = new List<id>();
List<Milestone1_Milestone__c> MSlist = new List<Milestone1_Milestone__c>();
Map<id,List<id>> ProjectToPMlist = new Map<id,List<id>>();
Map<id,List<Milestone1_Milestone__c>> ProjectToMS = new Map<id,List<Milestone1_Milestone__c>>();
Map<id,string> PMtoUserType = new Map<id,string>();
List<Milestone1_Milestone__Share> sharesToCreate = new List<Milestone1_Milestone__Share>();
List<Milestone1_Milestone__Share> sharesToRemove = new List<Milestone1_Milestone__Share>();
List<id> MStoRemoveShare = new List<id>();

/*List <Milestone1_Milestone__Share> candidateShares = [SELECT Id, ParentId, UserOrGroupId,AccessLevel,RowCause FROM Milestone1_Milestone__Share where ParentId];*/
boolean AddFollowers = false;

if(trigger.isinsert)
AddFollowers = true;


for(Milestone1_Milestone__c ms:trigger.new)
 {
 
 if(trigger.isinsert ||
   (trigger.isupdate && ms.ispublic__c == true && trigger.oldmap.get(ms.id).ispublic__c == false )||  // milestone changed to public
   (trigger.isupdate && ms.ispublic__c == true && ms.ownerId != trigger.oldmap.get(ms.id).ownerId ))  // milestone owner was changed
 {
 
  ProjectIds.add(ms.project__c);
  
   if(!ProjectToMS.containsKey(ms.Project__c))
     {
      MSlist = new list<Milestone1_Milestone__c>();
      MSlist.add(ms);
      ProjectToMS.put(ms.Project__c,MSlist);
     }else
     {
      MSlist = ProjectToMS.get(ms.Project__c);
      MSlist.add(ms);
      ProjectToMS.put(ms.Project__c,MSlist); 
     } // else
 } //if trigger isinsert/update
 
 if(trigger.isupdate && ms.ispublic__c == false && trigger.oldmap.get(ms.id).ispublic__c == true )  // milestone changed to public
 {MStoRemoveShare.add(ms.id);}
 
} // for


// Remove Sharing rules
if(!MStoRemoveShare.isempty())
{ 
List <Milestone1_Milestone__Share> candidateShares =[SELECT Id, ParentId, UserOrGroupId,AccessLevel,RowCause FROM Milestone1_Milestone__Share where ParentId in:MStoRemoveShare AND RowCause = 'Manual'];
delete (candidateShares);

}


 
if(!ProjectIds.isempty())
{
for(Project_member__c pm:[select Project_member__c,Project__c, Project_member__r.userType  from Project_member__c where project__c in:ProjectIds and Project_member__c !=null])
{
   PMtoUserType.put(pm.Project_member__c, pm.Project_member__r.userType); 
 
 if(!ProjectToPMlist.containsKey(pm.Project__c))
     {
      PMlist = new list<id>();
      PMlist.add(pm.Project_member__c);
      ProjectToPMlist.put(pm.Project__c,PMlist);
     }else
     {
      PMlist = ProjectToPMlist.get(pm.Project__c);
      PMlist.add(pm.Project_member__c);
      ProjectToPMlist.put(pm.Project__c,PMlist); 
     }
} //for

if(!ProjectToPMlist.isempty())  // check if there is Project Member for project
{
for(Id prId:ProjectToMS.keyset())
 {
   for(Milestone1_Milestone__c MS:ProjectToMS.get(prId))
   {
     for(Id Pmem:ProjectToPMlist.get(prId))
        {
         // Add sharing to portal Project Mambers
         if(PMtoUserType.get(pmem) != 'Standard' && ms.ispublic__c == true)
          {
                   
           Milestone1_Milestone__Share MsShare = new Milestone1_Milestone__Share();
           MsShare.ParentId = MS.id;
           MsShare.UserOrGroupId = pmem;
           MsShare.AccessLevel = 'Edit';
           sharesToCreate.add(MsShare);
          }
          
          // Add all Project Mambers as Milestone followers
          if(AddFollowers == True)
          {
           EntitySubscription Follower = new EntitySubscription(); 
           if(PMtoUserType.get(pmem) != 'Standard')   // if follower is portal user
           { Follower.NetworkId = ENV.CommunityId;  }               
           Follower.SubscriberId = Pmem;
           Follower.ParentId = MS.id;
           FollowersList.add(Follower);
         } //Add followers  
        }  //Pmem
     }     //Milestone
  }  


 
  insert( FollowersList);
  List<Database.SaveResult> sr_list = Database.insert(sharesToCreate,false);
  
}//  if(!ProjectToPMlist.isempty())
} // if
}