trigger Change_Assigne_on_WaitForCustApproval on Case (before update) {
	
	if(ENV.triggerShouldRun){
		ENV.triggerShouldRun = false;
	
		 for(Case cs:trigger.new){
		    if(cs.state__c =='Wait for release'&& trigger.oldmap.get(cs.Id).State__c=='Assigned AE')
		    { cs.Assignee__c=cs.Ownerid;}    
		    if(cs.state__c =='Waiting for Customer Approval'&& trigger.oldmap.get(cs.Id).State__c=='Assigned AE')
		    { cs.Assignee__c=cs.Ownerid;}
		    if(cs.state__c =='Waiting for Customer Info'&& trigger.oldmap.get(cs.Id).State__c=='Assigned AE')
		    { cs.Assignee__c=cs.Ownerid;}
		                                               
		    if(cs.state__c =='Assigned'&& trigger.oldmap.get(cs.Id).State__c=='Assigned AE')
		    { cs.Assignee__c=cs.Ownerid;}
		    if(cs.state__c =='Assigned'&& trigger.oldmap.get(cs.Id).State__c=='Open AE')
		    { cs.Assignee__c=cs.Ownerid;}
		    
		 }
	}
}