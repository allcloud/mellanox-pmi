trigger QuoatelineItem_beforeInsert on QuoteLineItem (before insert) {
	
	 Set<String> BCN_nums = new Set<String>();	
	 Set<String> opty_ID = new Set<String>();
	 //Set<String> accountnames = new Set<String>();
	 Set<String> primarypartners = new Set<String>();	
	 List<QuoteLineItem> qlines_sync_approved = new List<QuoteLineItem>();
	 Set<Id> quate_Id = new Set<Id>();
	 Set<Id> set_PriceBookID = new Set<Id>();
	 Set<Id> set_ProductID = new Set<Id>();
	 Set<Id> set_PriceBookEntry = new Set<Id>();
	 Map<Id, Quote> map_quates = new  Map<Id, Quote>();
	 List<PricebookEntry> lst_PricebookEntry = new List<PricebookEntry>();
	 Map<Id, Map<Id, Decimal>> map_ProductId_MapPricebookEntry = new Map<Id, Map<Id, Decimal>>();
	 Map<String,Map<String,Double>> map_bcnnumber_prodname_prices = new Map<String,Map<String,Double>>();
	 Map<String,Map<String,Map<String,QuoteLineItem>>> map_optyID_Prod_Price = new Map<String,Map<String,Map<String,QuoteLineItem>>>();
	 
	 //add OEM PB to the list as Secondary PB might be needed
	 //If cannot find in Tier 1 PB, find it in OEM PB
	 // KN 02-05-14
	 ID OEM_PB_ID = '01s500000006J7F';
	 ID DB_PB_ID = '01s500000006J09';
	 set_PriceBookID.add(OEM_PB_ID);
	 set_PriceBookID.add(DB_PB_ID);
	 
	 Set<String> Tier1_customers = new Set<String>();
	 Tier1_customers.add('Hewlett-Packard');
	 Tier1_customers.add('HP Price List');
	 Tier1_customers.add('HP PRICE LIST');
	 Tier1_customers.add('IBM');
	 Tier1_customers.add('IBM Price List');
	 Tier1_customers.add('IBM PRICE LIST');
	 //Tier1_customers.add('Dell Pricebook');  //EXCEPT Dell would not be default to OEM
	 Tier1_customers.add('CRAY');
	 Tier1_customers.add('CRAY Price List');
	 Tier1_customers.add('CRAY PRICE LIST');
	 Tier1_customers.add('EMC');
	 Tier1_customers.add('EMC Price List');
	 Tier1_customers.add('EMC PRICE LIST');
	 Tier1_customers.add('SGI');
	 Tier1_customers.add('SGI Price List');
	 Tier1_customers.add('SGI PRICE LIST');
	 Tier1_customers.add('ORACLE');
	 Tier1_customers.add('TERADATA');
	 Tier1_customers.add('TERADATA Price List');
	 Tier1_customers.add('TERADATA PRICE LIST');
	 Tier1_customers.add('LENOVO');
	 // 
	 for (QuoteLineItem qli : Trigger.new)
	 {
		//only apply for un0sync quotes
		if(qli.is_sync__c == false) {
		 	if(qli.opportunityID__c != null && qli.opportunityID__c != '' )
		 		opty_ID.add(qli.opportunityID__c);
		 	//if(qli.Main_Account_txt__c != null && qli.Main_Account_txt__c != '')
		 		//accountnames.add(qli.Main_Account_txt__c);
		 	if(qli.Primary_Account_txt__c != null && qli.Primary_Account_txt__c != '')
		 		primarypartners.add(qli.Primary_Account_txt__c);
		} 
	 	if (qli.QuoteId != null)
	 		quate_Id.add(qli.QuoteId);
	 	if (qli.ProductId__c != null)
	 		set_ProductID.add(qli.ProductId__c);	
	 	if(qli.BCN_Number__c != '' && qli.BCN_Number__c != null)
	 		BCN_nums.add(qli.BCN_Number__c);	
	 }
		system.debug('KN2 quotelineitem bforeinsert set_ProductID===' + set_ProductID);
	  system.debug('KN2 quotelineitem bforeinsert opty_ID===' + opty_ID);
	  system.debug('KN2 quotelineitem bforeinsert primarypartners===' + primarypartners);  
     
	 if (quate_Id.size() > 0)
	 {
	 	  map_quates =  new Map<Id, Quote>([Select Id, New_Price_Book__c, (Select Id, PricebookEntry.Pricebook2Id, PricebookEntry.Product2Id, PricebookEntry.UnitPrice, QuoteId, PricebookEntryId, New_Price__c,Product__c From QuoteLineItems) 
                                From Quote q
                                where q.Id IN : quate_Id]);
     	  //KN added 9-22-15 - check approved pricings from a sync and approved quote
     	  if(opty_ID != null) {
     	  		qlines_sync_approved = [select opportunityID__c,Primary_Account_txt__c,ProductId__c,
     	  								UnitPrice,Quantity,TotalPrice,Quote_Rebate_Percent__c
     	  								from Quotelineitem where is_sync__c=true and Quote.status='Approved'
     	  								and opportunityID__c in :opty_ID
     	  								and /* Main_Account_txt__c in :accountnames and */ Primary_Account_txt__c in :primarypartners];		
System.debug('...KN2 qlines_sync_approved.size===' + qlines_sync_approved.size());
				// Map<String,Map<String,Map<String,QuoteLineItem>>>
			  Map<String,Map<String,QuoteLineItem>> tmp_partner_prod_qli;
     	  	  Map<String,QuoteLineItem> tmp_map_prod_qli;
     	  	  //Double tmp_price;
			  for(QuoteLineItem bb : qlines_sync_approved){
			  		/*factor in Rebate % Discount
			  		if(bb.Quote_Rebate_Percent__c != null && bb.Quote_Rebate_Percent__c > 0 && bb.UnitPrice > 0) 
			  			tmp_price = bb.UnitPrice * (1 - bb.Quote_Rebate_Percent__c/100.00) ;
			  		else
			  			tmp_price = bb.UnitPrice;
			  		*/
			  		if(map_optyID_Prod_Price.get(bb.opportunityID__c) == null){
			  			tmp_map_prod_qli = new Map<String,QuoteLineItem>();
			  			tmp_map_prod_qli.put(bb.ProductId__c,bb);
						tmp_partner_prod_qli = new Map<String,Map<String,QuoteLineItem>>();
			  			tmp_partner_prod_qli.put(bb.Primary_Account_txt__c,tmp_map_prod_qli);
			  			map_optyID_Prod_Price.put(bb.opportunityID__c,tmp_partner_prod_qli);
			  		}
			  		else {
			  			tmp_partner_prod_qli = map_optyID_Prod_Price.get(bb.opportunityID__c);
			  			if(tmp_partner_prod_qli.get(bb.Primary_Account_txt__c) == null) {
			  				tmp_map_prod_qli = new Map<String,QuoteLineItem>();
			  				tmp_map_prod_qli.put(bb.ProductId__c,bb);
			  				tmp_partner_prod_qli.put(bb.Primary_Account_txt__c,tmp_map_prod_qli);
			  			}
			  			else {
			  				tmp_map_prod_qli = tmp_partner_prod_qli.get(bb.Primary_Account_txt__c);
			  				tmp_map_prod_qli.put(bb.ProductId__c,bb);
			  				tmp_partner_prod_qli.put(bb.Primary_Account_txt__c,tmp_map_prod_qli);
			  			}
			  		}
			  }
     	  }
     	  //end added 9-22-15
     	  
     	  //KN added BCN 09-86-15
		  List<Bid_Control_Lines__c> blines = new List<Bid_Control_Lines__c>([select Bid_Control__r.Name,Requested_Price__c,Prod_Name__c
																			from Bid_Control_Lines__c where Bid_Control__r.Name in :BCN_nums]);
		  Map<String,Double> tmp_map_prod_BCNprices;
		  for(Bid_Control_Lines__c bb : blines){
		  		if(map_bcnnumber_prodname_prices.get(bb.Bid_Control__r.Name) == null){
		  			tmp_map_prod_BCNprices = new Map<String,Double>();
		  			tmp_map_prod_BCNprices.put(bb.Prod_Name__c,bb.Requested_Price__c);
		  			map_bcnnumber_prodname_prices.put(bb.Bid_Control__r.Name,tmp_map_prod_BCNprices);
		  		}
		  		else {
		  			tmp_map_prod_BCNprices = map_bcnnumber_prodname_prices.get(bb.Bid_Control__r.Name);
		  			tmp_map_prod_BCNprices.put(bb.Prod_Name__c,bb.Requested_Price__c);
		  			map_bcnnumber_prodname_prices.put(bb.Bid_Control__r.Name,tmp_map_prod_BCNprices);
		  		}
		  }//end BCN 
     	  for (Quote q : map_quates.values())
     	  {
     	  	if (q.New_Price_Book__c != null)
     	  	 	set_PriceBookID.add(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c));
     	  }
     	  
     	  
     	  if (set_PriceBookID.size() > 0)
     	  {
     	  	  
     	  	    lst_PricebookEntry = [Select p.UnitPrice, p.Product2Id, p.Pricebook2Id 
	                                From PricebookEntry p
	                                where p.Product2Id IN : set_ProductID AND p.Pricebook2Id IN : set_PriceBookID];
     	  
     	  	    
     	  	    system.debug('lst_PricebookEntry: '+lst_PricebookEntry);
		        for (PricebookEntry p : lst_PricebookEntry)
		        {
		            if (!map_ProductId_MapPricebookEntry.containsKey(p.Pricebook2Id))
		            {
		                map_ProductId_MapPricebookEntry.put(p.Pricebook2Id, new Map<Id, Decimal> {p.Product2Id => p.UnitPrice});
		            }
		            else
		            {
		                Map<Id, Decimal> m = map_ProductId_MapPricebookEntry.get(p.Pricebook2Id);
		                if (!m.containsKey(p.Product2Id))
		                {
		                    m.put(p.Product2Id, p.UnitPrice);
		                }
		            }
		        }
		        
		        system.debug('map_ProductId_MapPricebookEntry: '+map_ProductId_MapPricebookEntry);
				QuoteLineItem tmp_qline;
	            for (QuoteLineItem qli : Trigger.new)
	            {
					//Set BCN prices if Quote has BCN
					if(qli.BCN_Number__c != null && qli.BCN_Number__c != '' && qli.Product__c != ''){
						if(map_bcnnumber_prodname_prices != null && map_bcnnumber_prodname_prices.get(qli.BCN_Number__c) != null
							&& map_bcnnumber_prodname_prices.get(qli.BCN_Number__c).get(qli.Product__c) != null)
								qli.BCN_Price__c = 	map_bcnnumber_prodname_prices.get(qli.BCN_Number__c).get(qli.Product__c);
					}
					//copy over Approved prices from other approved and Sync quote on the same opportunity
System.debug('...KN3 qli.ProductId__c===' + qli.ProductId__c);
System.debug('...KN3 qli.opportunityID__c===' + qli.opportunityID__c);
System.debug('...KN3 map_optyID_Prod_Price.get(qli.opportunityID__c)===' + map_optyID_Prod_Price.get(qli.opportunityID__c));
// Map<String,Map<String,Map<String,QuoteLineItem>>>					

					if(qli.opportunityID__c != null && qli.opportunityID__c != '' && qli.is_sync__c==false && qli.ProductId__c != '' ) {
						if(map_optyID_Prod_Price.get(qli.opportunityID__c) != null 
							&& map_optyID_Prod_Price.get(qli.opportunityID__c).get(qli.Primary_Account_txt__c) != null
							&& map_optyID_Prod_Price.get(qli.opportunityID__c).get(qli.Primary_Account_txt__c).get(qli.ProductId__c) != null) {
								
								tmp_qline = map_optyID_Prod_Price.get(qli.opportunityID__c).get(qli.Primary_Account_txt__c).get(qli.ProductId__c);
								qli.Approved_Qty_from_Sync_Quote__c = Math.ceil(.95 * tmp_qline.Quantity);
								qli.Approved_Percent_from_Sync_Quote__c = tmp_qline.Quote_Rebate_Percent__c;
								if (tmp_qline.UnitPrice > 0 && tmp_qline.Quote_Rebate_Percent__c != null && tmp_qline.Quote_Rebate_Percent__c > 0)
									qli.Approved_Price_from_Sync_Quote__c = tmp_qline.UnitPrice * (1 - tmp_qline.Quote_Rebate_Percent__c/100.00) ;
								else 
									qli.Approved_Price_from_Sync_Quote__c = tmp_qline.UnitPrice;
						}
					}

					//
	            	Quote q = map_quates.get(qli.QuoteId);
	                if (map_ProductId_MapPricebookEntry.containsKey(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c)))
	                {
	                    if (map_ProductId_MapPricebookEntry.get(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c)).containsKey(qli.ProductId__c))  
	                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c)).get(qli.ProductId__c);
	                    else
	                        qli.New_Price__c = 0;

	                }
	     			
	     			//Default to DB PB if DELL
	     			if ( (qli.New_Price__c == null || qli.New_Price__c == 0) && (q.New_Price_Book__c == 'Dell Pricebook' || q.New_Price_Book__c == 'Dell') ){
	                	if (map_ProductId_MapPricebookEntry != null && map_ProductId_MapPricebookEntry.get(DB_PB_ID) != null && map_ProductId_MapPricebookEntry.get(DB_PB_ID).containsKey(qli.ProductId__c))  
	                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(DB_PB_ID).get(qli.ProductId__c);
	                    else
	                        qli.New_Price__c = 0;
	                }
	                //For Facebook, look into DB as default PB
		            else if ( (qli.New_Price__c == null || qli.New_Price__c == 0) && q.New_Price_Book__c == 'FACEBOOK' ){
	                	if (map_ProductId_MapPricebookEntry != null && map_ProductId_MapPricebookEntry.get(DB_PB_ID) != null && map_ProductId_MapPricebookEntry.get(DB_PB_ID).containsKey(qli.ProductId__c))  
	                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(DB_PB_ID).get(qli.ProductId__c);
	                    else
	                        qli.New_Price__c = 0;
	                }
	     			//Default to OEM PB
	                else if ( (qli.New_Price__c == null || qli.New_Price__c == 0) && Tier1_customers.contains(q.New_Price_Book__c) ){
	                	if (map_ProductId_MapPricebookEntry != null && map_ProductId_MapPricebookEntry.get(OEM_PB_ID) != null && map_ProductId_MapPricebookEntry.get(OEM_PB_ID).containsKey(qli.ProductId__c))  
	                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(OEM_PB_ID).get(qli.ProductId__c);
	                    else
	                        qli.New_Price__c = 0;
	                }
System.debug('...KN quotelineitem beforeInsert qli.New_Price__c 222===' + qli.New_Price__c);	              
	                //Re-assign UnitPrice on Quote lines in the case support quote lines are added, and UnitPrice = 0 or null
	                //This is to handle Quote_add_SupportOPN process if no UnitPrice is found (part does not have prices set up)
	                if(qli.supportOPN_by_mapping__c==true && (qli.UnitPrice == null && qli.UnitPrice == 0) ){
	                	qli.UnitPrice = qli.New_Price__c;
	                }
	                //
	                // KN added 06-12-14
		            // Handle cases where GPS-00001 is copied over from opportunit when quote creates from opty, and SalesPrice is also copied over and
		            //might affact 2ndLevel_approval ,,GPS-EDR- KICKST-4
		            if (qli.Product__c=='GPS-00001' || qli.Product__c=='GPS-XS-NC-STD' || qli.Product__c=='GPS-S-NC-STD' || qli.Product__c=='GPS-M-NC-STD' 
		            		|| qli.Product__c=='GPS-L-NC-STD' || qli.Product__c=='GPS-XL-NC-STD' || qli.Product__c=='GPS-TPM-01'
		            		|| qli.Product__c=='GPS-EDR-KICKST-4' || qli.Product__c=='GPS-ETH-KICKST-6' ){
		            	qli.UnitPrice = qli.New_Price__c;
		            }
		            // end KN 06-12-14
	            }
     	  }
	 }
}