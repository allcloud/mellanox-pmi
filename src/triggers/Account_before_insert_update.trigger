trigger Account_before_insert_update on Account (before insert, before update) {


for(Account Acc:trigger.new)
{

//Matching duplicate accounts key
    String canonical = Acc.Name.toUpperCase();                    
    Blob bsig = Crypto.generateDigest('MD5', Blob.valueOf(canonical));
    Acc.NAME_CODE__C = encodingUtil.convertToHex (bsig);
  
   if(trigger.isinsert)
   {if(Acc.Contract2_Type_Paranet_Account__c!= Null &&  Acc.Parent_Highest_Active_cont_rank__c > Acc.Highest_Active_contract_rank__c )
     {Acc.Contract_type_final__c =  Acc.Contract2_Type_Paranet_Account__c;}
     else{Acc.Contract_type_final__c =Acc.Contract_type__c ;}
    }    

   if(trigger.isupdate  && (Acc.Highest_Expired_contract_rank__c !=trigger.oldmap.get(Acc.id).Highest_Expired_contract_rank__c || 
     Acc.Highest_Active_contract_rank__c !=trigger.oldmap.get(Acc.id).Highest_Active_contract_rank__c ))
     { if(Acc.Contract2_Type_Paranet_Account__c!= Null && Acc.Parent_Highest_Active_cont_rank__c > Acc.Highest_Active_contract_rank__c )   
         { Acc.Contract_type_final__c =  Acc.Contract2_Type_Paranet_Account__c;}
       else{Acc.Contract_type_final__c =Acc.Contract_type__c ;}
     
     }
  
  if(trigger.isupdate  && Acc.parentid!=trigger.oldmap.get(Acc.id).parentid)
  {
   if(Acc.parentid == NULL) {Acc.Contract_type_final__c =Acc.Contract_type__c ;} 
   else
    {
     if (Acc.Highest_Active_contract_rank__c == NULL || Acc.Parent_Highest_Active_cont_rank__c > Acc.Highest_Active_contract_rank__c)
        {Acc.Contract_type_final__c =  Acc.Contract2_Type_Paranet_Account__c;}
     else {Acc.Contract_type_final__c =Acc.Contract_type__c ;}
     }
  }
  
  
if(trigger.isinsert || (trigger.isupdate && Acc.VT_Region_map__c != trigger.oldmap.get(Acc.id).VT_Region_map__c))
{  
  Acc.SC1__c = Acc.VT_calc__c;
  Acc.Region__c = Acc.Region_calc__c;
 
         }    
}
	
	if (trigger.isBefore) {
		
		cls_trg_Account handler = new cls_trg_Account();
		
		if (trigger.isInsert) {
			
			handler.updateRWDomainMultiPicklist(trigger.New, null);
		}
		
		else if (trigger.isUpdate) {
			
			handler.updateRWDomainMultiPicklist(trigger.New, trigger.oldMap);
		}
	}

}