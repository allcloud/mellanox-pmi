trigger MilestoneTask_after_update on Task (after update, after insert, after delete) {



  //Limit the size of list by using Sets which do not contain duplicate elements
  set<Id> MilestoneIds = new set<Id>();

  //When adding new Task or updating existing Task
  if(trigger.isInsert || trigger.isUpdate){
    for(task ts : trigger.new)
    {
    if(ts.whatId != NULL && (String.valueOf(ts.whatId)).StartsWith('a2O')) //Milestone task 
     { MilestoneIds.add(ts.WhatId);}
    }
  }

  //When deleting Milestone
  if(trigger.isDelete){
    for(task ts : trigger.old)
    {
    if(ts.whatId != NULL && (String.valueOf(ts.whatId)).StartsWith('a2O')) //Milestone task 
     { MilestoneIds.add(ts.WhatId);}
    }
  }

  //Map will contain one Account Id to one sum value
  map<Id,Date> MilestoneDeadlineMap = new map <Id,Date>();
  map<Id,Double> MilestoneTaskCountMap = new map <Id,Double>();
  map<Id,Double> MilestoneCompleteTaskCountMap = new map <Id,Double>();
  

  //Produce a sum of Milestone and add them to the map
  //use group by to have a single Account Id with a single sum value
  for(AggregateResult q : [select WhatId ,max(DueDate_Custom__c), count(id),sum(IsCompleted__c) from task where WhatId IN :MilestoneIds group by WhatId])
  {
      MilestoneDeadlineMap.put((Id)q.get('WhatId'),(Date)q.get('expr0'));
      MilestoneTaskCountMap.put((Id)q.get('WhatId'),(Double)q.get('expr1'));
      MilestoneCompleteTaskCountMap.put((Id)q.get('WhatId'),(Double)q.get('expr2'));
      
      
  }
  
  

  List<Milestone1_Milestone__c> MilestonesToUpdate = new List<Milestone1_Milestone__c>();

  //Run the for loop on Account using the non-duplicate set of Accounts Ids
  //Get the sum value from the map and create a list of Accounts to update
  for(Milestone1_Milestone__c o : [Select Id, Deadline__c,Number_of_Tasks__c,Num_Complete_tasks__c from Milestone1_Milestone__c where Id IN :MilestoneIds])
  { 
    o.Number_of_Tasks__c = MilestoneTaskCountMap.get(o.Id);
    o.Num_Complete_tasks__c = MilestoneCompleteTaskCountMap.get(o.Id);
    o.Deadline__c = MilestoneDeadlineMap.get(o.Id);
    MilestonesToUpdate.add(o);
  }

  update MilestonesToUpdate;

















/*
List<task> UpdTasks = new List<task>();
List<id> UpdTasksParentIds = new List<id>();
List<task> InsertTasks = new List<task>();
List<id> InsertTasksParentIds = new List<id>();
List<task> DeleteTasks = new List<task>();
List<id> DeleteTasksParentIds = new List<id>();
List<Milestone1_Milestone__c> MilestonesToUpd = new List<Milestone1_Milestone__c>();
Set<id> MilestonesToUpdSet = new Set<id>();

//map<id,Milestone1_Milestone__c> taskMilestones = new map<id,Milestone1_Milestone__c>();


if(trigger.isdelete)
{
for(task tsk:trigger.old)
 {
  if(tsk.whatId != NULL && (String.valueOf(tsk.whatId)).StartsWith('a2O')) //Milestone task 
   {
   DeleteTasks.add(tsk);
   DeleteTasksParentIds.add(tsk.whatId);
   }
 }
}

if(trigger.isinsert || trigger.isupdate)
{

for(task tsk:trigger.new)
{
  if(tsk.whatId != NULL && (String.valueOf(tsk.whatId)).StartsWith('a2O')) //Milestone task
  {
  
  
  if(trigger.isinsert)
   {
     InsertTasks.add(tsk);
     InsertTasksParentIds.add(tsk.whatId);
   }
   
  if(trigger.isupdate && 
    (tsk.ActivityDate != trigger.oldmap.get(tsk.id).ActivityDate ||
     (tsk.status != trigger.oldmap.get(tsk.id).status && ( tsk.status == 'Completed' || tsk.status == 'Rejected') &&  tsk.Complete_Date__c == NULL)))
  {
     UpdTasks.add(tsk);
     UpdTasksParentIds.add(tsk.whatId);
   }
  
 }// enf of Milestone task 
}
}

//*************** New Tasks Delete actions **************

if(!DeleteTasks.isempty())
{
map<id,Milestone1_Milestone__c> taskMilestones =new map<id,Milestone1_Milestone__c> ([Select id, Deadline__c, Number_of_Tasks__c ,Num_Complete_tasks__c
                                                                                      From Milestone1_Milestone__c where id in :DeleteTasksParentIds]);


for(task ts:DeleteTasks)
 {
 
  taskMilestones.get(ts.whatId).Number_of_Tasks__c = taskMilestones.get(ts.whatId).Number_of_Tasks__c -1;
   
     
        
    if(ts.status == 'Completed' || ts.status == 'Rejected')
    {
      taskMilestones.get(ts.whatId).Num_Complete_tasks__c = taskMilestones.get(ts.whatId).Num_Complete_tasks__c -1;
    }
     
    
        if(!MilestonesToUpdSet.contains(taskMilestones.get(ts.whatId).id))
         {
         
          system.debug('adding this Mstn '+taskMilestones.get(ts.whatId));        
          MilestonesToUpdSet.add(taskMilestones.get(ts.whatId).id);
          MilestonesToUpd.add(taskMilestones.get(ts.whatId));
         } 
    
   
   } // end of for
 
 
 update(MilestonesToUpd);
}  // end of Delete Tasks



//*************** New Tasks Insert actions **************
if(!InsertTasks.isempty())
{
map<id,Milestone1_Milestone__c> taskMilestones =new map<id,Milestone1_Milestone__c> ([Select id, Deadline__c, Number_of_Tasks__c ,Num_Complete_tasks__c
                                                                                      From Milestone1_Milestone__c where id in :InsertTasksParentIds]);

 for(task ts:InsertTasks)
 {
 
  taskMilestones.get(ts.whatId).Number_of_Tasks__c = taskMilestones.get(ts.whatId).Number_of_Tasks__c +1;
   
  if(taskMilestones.containsKey(ts.whatId)&& taskMilestones.get(ts.whatId).Deadline__c < ts.ActivityDate)
   {
    taskMilestones.get(ts.whatId).Deadline__c = ts.ActivityDate;
   }
    
      
        
    if(ts.status == 'Completed' || ts.status == 'Rejected')
    {
      taskMilestones.get(ts.whatId).Num_Complete_tasks__c = taskMilestones.get(ts.whatId).Num_Complete_tasks__c +1;
    }
     
    
        if(!MilestonesToUpdSet.contains(taskMilestones.get(ts.whatId).id))
         {
         
          system.debug('adding this Mstn '+taskMilestones.get(ts.whatId));        
          MilestonesToUpdSet.add(taskMilestones.get(ts.whatId).id);
          MilestonesToUpd.add(taskMilestones.get(ts.whatId));
         } 
    
   
   } // end of for
 
 
 update(MilestonesToUpd);
}  // end of Inser Tasks


//******************** Existing Task Update Action **************

if(!UpdTasks.isempty())
{
map<id,Milestone1_Milestone__c> taskMilestones =new map<id,Milestone1_Milestone__c> ([Select id, Deadline__c,Num_Complete_tasks__c
                                                                                      From Milestone1_Milestone__c where id in :UpdTasksParentIds]);

 for(task ts:UpdTasks)
 {
  if(taskMilestones.containsKey(ts.whatId)&& taskMilestones.get(ts.whatId).Deadline__c < ts.ActivityDate)
   {
    taskMilestones.get(ts.whatId).Deadline__c = ts.ActivityDate;
      
   }
   
  if((ts.status == 'Completed' && trigger.oldmap.get(ts.id).status != 'Completed' && trigger.oldmap.get(ts.id).status != 'Rejected' ) ||
     (ts.status == 'Rejected' && trigger.oldmap.get(ts.id).status != 'Rejected' && trigger.oldmap.get(ts.id).status != 'Completed'))
  {
      system.debug('adding 1 to counter '+ts.subject); 
    taskMilestones.get(ts.whatId).Num_Complete_tasks__c = taskMilestones.get(ts.whatId).Num_Complete_tasks__c +1 ;
  }
  
  
  if(!MilestonesToUpdSet.contains(taskMilestones.get(ts.whatId).id))
         {
         
          system.debug('adding this Mstn '+taskMilestones.get(ts.whatId));        
          MilestonesToUpdSet.add(taskMilestones.get(ts.whatId).id);
          MilestonesToUpd.add(taskMilestones.get(ts.whatId));
         } 
  
  
 }// for
 
 update(MilestonesToUpd);
} 
*/

}