// Version		Change Description 			 		Change made by		Change Date 		Related Service Now Number

// V1.0			This trigger should not work for 	Tatiana Sematch		29/08/13			Part of the Internal RMA
// 				Internal RMA's- as they shouldn't 											project
//				have related Cases




trigger create_RMA_Case on RMA__c (after update) {

    List<RMA__c> CaseRMAs = new List<RMA__c>();
    List<RMA__c> CaseOEM_RMAs = new List<RMA__c>();
    List<string> CaseRMAsId = new List<string>();
    List<Case> NewCases = new List<Case>();
    Set<id> WaitReturn = new Set<id>();
    List<id> SelectRMACases = new List<id>();
    List<Account> RelAccount = new List<Account>(); 
    string Domain; 
    List<string> Name = new List<string>(); 
    List<String> ShippRMA = new List<String>();
    List<RMA__c> RMA_Case_exist = new List<RMA__c>();
    Map<id,string> FA_caseToRma = new Map<id,string>();
    Map<id,boolean> AdvRep_caseToRma = new Map<id,boolean>();
    List<Case> UpdCases = new List<Case>();
    map<id,string> CaseToAWB = new map<id,string>();
    map<id,string> CaseToState = new map<id,string>();
     map<id,RMA__c> test = new map<id,RMA__c>();
    string RelCases;  
    List<case> RelCasesObj = new List<case>();
    private RecordType parentAccRecType; 
         
    for(RMA__c rma:trigger.new)
    {
    
    if (rma.RecordTypeID != ENV.Internal_RMA_RecordType)
    {
          
      
      if(rma.Is_OEM__c == 'Yes'|| (rma.is_OEM_RMA_case__c == True && trigger.oldmap.get(rma.id).is_OEM_RMA_case__c == false))
      { CaseOEM_RMAs.add(rma);}
            
      if((rma.Is_OEM__c == 'No'&& rma.is_RMA_case__c == False)||(rma.Is_OEM__c == 'Yes' && rma.RMA_state__c == 'Support Group Approval' && rma.is_RMA_case__c == False))
        {
            CaseRMAs.add(rma);
            CaseRMAsId.add(rma.id);
           if(rma.Case_Number__c != NULL)
           {RelCases = '%'+ rma.Case_Number__c; }
        }
     
      if(rma.Case_Parent__c != NULL && 
         ( (rma.RMA_state__c == 'Shipped' && trigger.oldmap.get(rma.id).RMA_state__c != 'Shipped') || (rma.RMA_state__c == 'Canceled - Not Return' && trigger.oldmap.get(rma.id).RMA_state__c != 'Canceled - Not Return')) ) 
         {  test.put( rma.Case_Parent__c,rma);      
          ShippRMA.add(rma.id);
          
          
          CaseToAWB.put(rma.Case_Parent__c,rma.AWB_Number__c);
          CaseToState.put(rma.Case_Parent__c,rma.RMA_State__c);
          SelectRMACases.add(rma.id);
          }
         
      if(rma.Case_Parent__c != NULL && rma.Advanced__c != trigger.oldmap.get(rma.id).Advanced__c)  
         {AdvRep_caseToRma.put(rma.Case_Parent__c,rma.Advanced__c);
          SelectRMACases.add(rma.id);
         }
                   
      if( rma.Case_Parent__c != NULL && rma.Failure_Analysis_Request__c != trigger.oldmap.get(rma.id).Failure_Analysis_Request__c)
        {FA_caseToRma.put(rma.Case_Parent__c,rma.Failure_Analysis_Request__c ); }
   
    }
    }
     
     
     
 //---------------------------------  RMA changes Case status to Waiting Return  --------------------   
     
/*    if(WaitReturn.size()>0 ||ShippRMA.size()>0 ||!AdvRep_caseToRma.isempty() )
    { 
        UpdCases  =[select id, state__c from case where RMA_Request__c in :SelectRMACases];
       
        for(case cs:UpdCases)
        {
        
          if( WaitReturn.contains(cs.id))   
           { cs.test__c = true;}
           
          if(ShippRMA.contains(cs.id))
          { 
           cs.state__c = 'Closed';
           cs.items_shipped__c = True;
           cs.AWB_num__c = CaseToAWB.get(cs.id);
           }
           
         if(AdvRep_caseToRma != null && AdvRep_caseToRma.get(cs.id)==TRUE)
          {  cs.advanced__c = true;}  
           
            
        }
      
        update(UpdCases); 
    } 
        
     
 */    
     
 //--------------------------------- Shipped RMA changes Case status to Closed --------------------   
     
    if(ShippRMA.size()>0)
    { 
        UpdCases  =[select id, state__c,RMA_Reject_reason__c, items_shipped__c from case where RMA_Request__c in :ShippRMA];
       
        for(case cs:UpdCases)
        {
          //test.get(cs.id).adderror('I am here');
           if(CaseToState.get(cs.id) == 'Shipped')
           {  cs.state__c = 'Closed';
              cs.items_shipped__c = True;
              cs.AWB_num__c = CaseToAWB.get(cs.id);
            }
            
           if(CaseToState.get(cs.id) == 'Canceled - Not Return')
           {  cs.state__c = 'RMA Canceled - not Return'; 
              cs.RMA_Reject_reason__c = 'Not returned within 30 days'; 
            }
        }
      
        try {	
        	update(UpdCases); 
        }
        
        catch(Exception e) {
        	
        	
        }
    } 
    
 //--------------------------------- Advanced Rep--------------------   
     
    if(AdvRep_caseToRma.keySet().size()>0)
    { 
        UpdCases  =[select id, state__c, advanced__c from case where id in :AdvRep_caseToRma.keySet()];
       
        for(case cs:UpdCases)
        {
            if(AdvRep_caseToRma.get(cs.id)==TRUE)
            cs.advanced__c = true;
            
         }
      
        update(UpdCases); 
    } 
 
   
    
    
//----------------------------update FA request on case --------------


if(FA_caseToRma.keySet().size()>0 || AdvRep_caseToRma.keySet().size()>0 )
    { 
        UpdCases  =[select id, Failure_Analysis_Request__c  from case where id in :FA_caseToRma.keySet()];
       
        for(case cs:UpdCases)
        {
           cs.Failure_Analysis_Request__c = FA_caseToRma.get(cs.id);
        }
      
        update(UpdCases); 
    } 


//----------------------------  
    
    
     
    if(CaseRMAsId.size() >0)
    {
        RMA_Case_exist = [select id, is_RMA_case__c from RMA__c where id in :CaseRMAsId];
        for(RMA__c rm2:RMA_Case_exist)
        {
            rm2.is_RMA_case__c = TRUE;
        }
        update(RMA_Case_exist);
    }
   
if(RelCases!=Null)
 { RelCasesObj= [select id from case where casenumber like :RelCases limit 1];  }
  
   for(RMA__c orm:CaseOEM_RMAs)
    {
     Case ORC = new Case();
     ORC.RecordTypeId = ENV.OEMRMA_RecordType;
     ORC.OwnerId = ENV.RMAQueue;
     ORC.Description = 'Billing Information:' + '\n\nBill State: ' + orm.Bill_City_State__c + '\n Bill Company: ' + orm.Bill_Company__c + '\n Bill Country: '+ 
        orm.Bill_Country__c + '\n Bill Phone:  ' + orm.Bill_Phone__c + '\n Bill Zip: ' + orm.Bill_Zip__c + '\n \n Ship Information:' +
         '\n Ship city:  ' + orm.Ship_City_State__c + '\n Ship Company:  ' + orm.Ship_Company__c + '\n Ship Address:  ' + orm.Ship_Company_Address_1__c + 
         '\n Ship contact:  ' + orm.Ship_Contact__c +'\n Ship Country:  ' + orm.Ship_Country__c + '\n Ship phone: ' + orm.Ship_Phone__c + '\n Ship Zip:  '+ orm.Ship_Zip__c ;
    ORC.origin = 'Web';
    ORC.RMA_request__c = orm.id;
    ORC.subject = 'Web RMA #' + orm.name +'.';
    ORC.RMA_type__c = 'Based on Serial Numbers'; 
      
    }
          
     parentAccRecType = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: 'ParentAccount'];     
          
    for(RMA__c rm:CaseRMAs)
    {
        // ------------------ Assign Case Owner according to assignment rules
        //database.DMLOptions dmo = new database.DMLOptions(); 
        //dmo.AssignmentRuleHeader.UseDefaultRule= true;
    
    
        Case RC = new Case();
        if(!RelCasesObj.isempty()) {RC.RMA_Related_case__c=RelCasesObj[0].id;}
        RC.RecordTypeId = '01250000000Dtey';
        RC.OwnerId = ENV.RMASupportQueue;
        RC.Failure_Analysis_Request__c = rm.Failure_Analysis_Request__c;
        RC.RMA_Case_Source__c = rm.Source__c;        
        RC.Description = 'Billing Information:' + '\n\nBill State: ' + rm.Bill_City_State__c + '\n Bill Company: ' + rm.Bill_Company__c + '\n Bill Country: '+ 
        rm.Bill_Country__c + '\n Bill Phone:  ' + rm.Bill_Phone__c + '\n Bill Zip: ' + rm.Bill_Zip__c + '\n \n Ship Information:' +
         '\n Ship city:  ' + rm.Ship_City_State__c + '\n Ship Company:  ' + rm.Ship_Company__c + '\n Ship Address:  ' + rm.Ship_Company_Address_1__c + 
         '\n Ship contact:  ' + rm.Ship_Contact__c +'\n Ship Country:  ' + rm.Ship_Country__c + '\n Ship phone: ' + rm.Ship_Phone__c + '\n Ship Zip:  '+ rm.Ship_Zip__c ;

      	list<Group> queueOwner;
		
      	if(rm.Source__c == 'NPU'){
      		queueOwner = [Select Id, Name From Group where Name='Unassigned- NPU'];
      		if(!queueOwner.isEmpty() ){
      			RC.ownerid = queueOwner[0].id;
      		}
      	}
      	else if(rm.Source__c == 'MC'){
      		queueOwner = [Select Id, Name From Group where Name='RMA CASE unassigned MC'];
      		if(!queueOwner.isEmpty() ){
      			RC.ownerid = queueOwner[0].id;
      		}
      	}
      	
        if(userinfo.getuserid() == ENV.ST1) // ST1 member      
        {
            RC.ST1_Memeber__c = userinfo.getName();
        }
        //RC.setOptions(dmo);
        
        RC.origin = 'Web';
        RC.RMA_request__c = rm.id;
        RC.subject = 'Web RMA #' + rm.name +'.';
        RC.RMA_type__c = 'Based on Serial Numbers';     
        if(rm.Account_name__c != NULL)
        {
            RC.Accountid = rm.Account_name__c;
        }
    
        if(rm.Contact__c != NULL)
        {
            RC.contactid = rm.contact__c;
        }
        else
        {
            Contact newCont = new Contact();
            if(rm.name__c != NULL)
            {
                name = rm.name__c.split(' ');
                newCont.firstName = name[0];
            }
            if(name.size() >1)
            {
                newCont.lastname = name[1];
            }
            else 
            {
                newCont.lastname = 'Unknown';
            } 
            
            newCont.email = ((rm.E_mail__c != null) ? rm.E_mail__c : null);
            
            newCont.Phone = ((rm.Contact_Phone__c != null) ? rm.Contact_Phone__c : null);
            
            newCont.company_name_from_web__c = rm.bill_Company__c;
            
            if (rm != null && rm.E_Mail__c != null)
            {
            	Domain = ((rm.E_mail__c.tolowercase().split('@'))[1]).split('\\.')[0];
            }
            
            RelAccount =[select id 
                         from Account 
                         where Has_Contract__c = 'Yes' 
                               AND (lower_domain__c = :Domain OR lower_domain1__c = :Domain OR lower_domain2__c =:Domain) AND RecordTypeId =: parentAccRecType.Id];  
            if(!RelAccount.isempty())
            {
                newCont.accountId = RelAccount[0].id ;
            }
            else
            {
                newCont.accountId = ENV.UnknownAccount ;
            }
            insert(newCont);
            RC.contactid = newCont.id; 
        }
        NewCases.add(RC);
    }
    
    if(NewCases != null && NewCases.size() > 0)
    {
        system.debug('NewCases size: ' +  NewCases.size());
        system.debug('NewCases : ' +  NewCases);
        insert(NewCases);
        for(case cs1:NewCases)
        {
         cs1.subject = cs1.subject  + '\n To access the case:  replaced with : https://mellanox.my.salesforce.com/'+ cs1.id ;
        }
        update(NewCases);
        
    }
}