trigger update_contract_before_update_insert on Contract2__c (before insert, before update) {
    
    List<ID> Upd_ContId  = new List<ID>();
    List<Contract2__c> Upd_Cont  = new List<Contract2__c>();
    map<id,id> ContractToRollUpOracle = new map<id,id>();
    integer terms = 0;
  
    
    for(Contract2__c Cont:trigger.new)
    {
        
        //------------- Contract type final calculation
        if(trigger.isinsert && Cont.CTF__c != NULL )
        {Cont.Contract_type__c = Cont.CTF__c;}
        
        if((trigger.isinsert && Cont.CTF__c ==NULL)||
        (trigger.isupdate && (Cont.HW_warranty_start_date__c!= trigger.oldmap.get(Cont.id).HW_warranty_start_date__c||
        Cont.SW_warranty_start_date__c!= trigger.oldmap.get(Cont.id).SW_warranty_start_date__c||
        Cont.Extended_warranty_start__c!= trigger.oldmap.get(Cont.id).Extended_warranty_start__c
        )
        ))
        {     
            if(Cont.HW_warranty_start_date__c!=NULL && Cont.HW_warranty_start_date__c <= system.today()&& Cont.HW_warranty_end_date__c>=system.today()) {
                Cont.CTF__c =Cont.HW_warranty_sla_type__c;
            }
            else{
                if(Cont.SW_warranty_start_date__c != NULL && Cont.SW_warranty_start_date__c <= system.today()&& Cont.SW_warranty_end_date__c >=system.today()) {
                    Cont.CTF__c =Cont.SW_warranty_sla_type__c;
                }
                else
                {
                    { if(Cont.Extended_warranty_start__c != NULL && Cont.Extended_warranty_start__c <= system.today()&& Cont.Extended_Warrenty_End__c >=system.today()) {
                        
                        Cont.CTF__c =Cont.Extended_warrenty_sla_type__c;
                       
                    }
                    else
                    {
                        Cont.CTF__c = Cont.Contract_type__c ;
                       
                    }
                    
                }}}
            }
            
            if(trigger.isUpdate && Cont.Contract_Type_text__c != trigger.oldMap.get(Cont.id).Contract_Type_text__c)
            {
                
                Cont.CTF__c = Cont.Contract_Type_text__c;
                
            }
            
            
            //------------------- Update Oracle Contract Number on insert ------------
            
            if(trigger.isinsert && Cont.Oracle_Contract_Name__c == NULL)
            {
                Cont.Oracle_Contract_Name__c = 'CON-'+Cont.Order__c;
                
            }
            
            
            
            //-------------------------- Update Contract End Date on Term(months) change ----------
            if(trigger.isupdate && Cont.Contract_Term_months__c != trigger.oldmap.get(Cont.id).Contract_Term_months__c )
            
            { date tmpDate = Cont.contract_start_date__c.addMonths((Cont.Contract_Term_months__c).intValue());
                
                // Cont.EndDate__c = tmpDate.addDays(90);
                Cont.EndDate__c = tmpDate;
                terms = 1;
            }
            
            //-------------------------------- Update Contract Status on EndDate change ------------------
            if(Cont.Contract_Renew_RollUp_to__c == NULL && Cont.Status__c != 'Draft' )
            
            {
                
                if( Cont.EndDate__c > system.today())
                {Cont.Status__c = 'Activated';}
                if( Cont.EndDate__c <= system.today()&& (Cont.Addtional_Extended_Warramty_Years__c==NULL ||Cont.Addtional_Extended_Warramty_Years__c == 0 ))
                {Cont.Status__c = 'Expired';}
                else
                {
                    if( Cont.EndDate__c <= system.today() && Cont.Addtional_Extended_Warramty_Years__c > 0 && Cont.Additional_Ext_Warranty_End_Date__c>system.today())
                    {Cont.Status__c = 'Activated';
                        Cont.Contract_type__c = 'Extended Warranty';
                    }
                    
                    if( Cont.EndDate__c < system.today()&& Cont.Addtional_Extended_Warramty_Years__c > 0 && Cont.Additional_Ext_Warranty_End_Date__c <= system.today())
                    {Cont.Status__c = 'Expired';
                        Cont.Contract_type__c = 'Extended Warranty';
                    }
                }
            }
            if(trigger.isupdate && Cont.EndDate__c != trigger.oldmap.get(Cont.id).EndDate__c && terms !=1) {
                
                if (Cont.EndDate__c != null && Cont.contract_start_date__c != null) {
                    
                    Cont.Contract_Term_months__c = Cont.contract_start_date__c.monthsBetween(Cont.EndDate__c);
                }
            }
            
            
            //------------------------------------Update Contract Status on Renewed/Roll Up -------------------
            if(trigger.isupdate && Cont.Contract_Renew_RollUp_to__c != trigger.oldmap.get(Cont.id).Contract_Renew_RollUp_to__c )
            { 
            	Cont.Status__c = 'Renewed / Roll Up';
        	}
            
        }
        
        if (trigger.isBefore && trigger.isUpdate && ENV.cls_trg_ContractTriggerShouldRun) {
            
            cls_trg_Contract handler = new cls_trg_Contract();
            handler.updateERIContractWhenApproved(trigger.New, trigger.oldMap);
            handler.closeContractRelatedOppAndGPSProjects(trigger.New, trigger.oldMap);
            ENV.cls_trg_ContractTriggerShouldRun = false;
        }
        
        else if (trigger.isBefore && trigger.isInsert && ENV.cls_trg_ContractTriggerShouldRun) {
                
                cls_trg_Contract handler = new cls_trg_Contract();
                handler.createContactRelatedToContract(trigger.New);
                ENV.cls_trg_ContractTriggerShouldRun = false;
        }
    }