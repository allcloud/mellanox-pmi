//Creates new Attachmnet when FM Discussion with Linc moved to Public (or Path through mode)

trigger FMDiscussion_AfterInsetUpdate on FM_Discussion__c (before insert, before update)
{
    List<FM_Discussion__c> lst_Fm  = new List<FM_Discussion__c>();
    List<ID> lst_DocId = new List<ID>(); 
    if(Trigger.isUpdate)
    {
        Map <ID,ID> map_idFmToIdattach = new Map <ID,ID>();
        List<ID> lst_delAttachment = new List<ID>();
        List <Attachment> lst_attachToUpdate = new List <Attachment>();
        for(FM_Discussion__c fm: Trigger.new)
        {
            if(fm.Public__c != Trigger.oldMap.get(fm.Id).Public__c && fm.DOC_ID__c!=null && fm.DOC_ID__c !='')
            {
                lst_Fm.Add(fm);
                lst_DocId.Add(fm.DOC_ID__c);
            }          
            if(fm.Path_Through__c == 'TRUE'&& fm.DOC_ID__c!='' && fm.DOC_ID__c!=null )
            {
                lst_Fm.Add(fm);
                lst_DocId.Add(fm.DOC_ID__c);
            }                                         
        }
        
        if(!map_idFmToIdattach.isEmpty())
        {
            Map <ID,FM_Discussion__c> map_idToFm= new Map <ID,FM_Discussion__c> ([Select f.Public__c, f.Id, f.Attachment_Link__c, f.Attachment_Id__c, (Select Name, IsPrivate From Attachments Where IsDeleted=false and Id IN: map_idFmToIdattach.values()) From FM_Discussion__c f Where IsDeleted=false and Id IN: map_idFmToIdattach.keySet()]);
            for(FM_Discussion__c fm: map_idToFm.values())
            {
                for(Attachment att: fm.Attachments)
                {
                    att.IsPrivate = !fm.Public__c;
                    lst_attachToUpdate.add(att);
                }
            }
        }
        
        if(lst_attachToUpdate.size()>0)
        {
            update lst_attachToUpdate;
        }
    }
    
    if(Trigger.isInsert)
    {
        for(FM_Discussion__c fm: Trigger.new)
        {
            if(fm.Public__c  && fm.DOC_ID__c!='' && fm.DOC_ID__c!=null )
            {
                lst_fm.add(fm);
                lst_DocId.Add(fm.DOC_ID__c);
            }
        }
    }
    if(Trigger.isInsert || Trigger.isUpdate)
    {
        Map<ID,Document> Docs = new Map<ID,Document>([select  Id, Name, body from Document where Id in :lst_DocId]);
        Map <ID,Attachment> map_idFmToAttach = new Map <ID,Attachment>();
        
        for(FM_Discussion__c fm: lst_Fm)
        {
            Attachment attach = new Attachment();
            attach.IsPrivate = false;
            attach.Name = Docs.get(fm.DOC_ID__c).Name;
            attach.ParentId = fm.Case__c;
            attach.Body = Docs.get(fm.DOC_ID__c).Body;
            map_idFmToAttach.put(fm.Id, attach);
        }
        
        if(!map_idFmToAttach.isEmpty())
        {
            insert map_idFmToAttach.values();
        }
        
        for(FM_Discussion__c fm: lst_Fm)
        {
            fm.Attachment_Id__c = map_idFmToAttach.get(fm.Id).Id;
        }
        
    }
}