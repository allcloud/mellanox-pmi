trigger Lead_auto_convert on Lead (after insert) {

/*
List<string> Domains = new List<string>();
List<Lead> UpdLeads = new List<Lead>();
List<string> Emails = new List<string>();
map<string,contact> ExistCont = new map<string,contact>();
//map<string,lead> Domain_to_Lead = new map<string,lead>();
List<Account> Accounts = new List<Account>();
map<string,id> Domain_to_Account = new map<string,id>();
map<string,id> Domain1_to_Account = new map<string,id>();
map<string,id> Domain2_to_Account = new map<string,id>();
List<lead> DupLead = new List<lead>();
List<id> DupLeadIds = new List<id>();
ID AccID;

  
for(lead ld:trigger.new)
{
 if(ld.Access_required__c == True && ld.Lead_type__c == 'Mellanox System Customer')
 {Domains.add(ld.domain__c.tolowercase());  
  UpdLeads.add(ld);
  Emails.add(ld.email);
  }
}

if(!Emails.isempty())
{
 for(Contact cnt:[select id, email, accountid,Duplicate_lead__c from Contact where email in :Emails and Login_Status_Backup__c ='approved,' ])
 {ExistCont.put(cnt.email,cnt);}
}


if(!Domains.isempty())
{
  Accounts =[select id, lower_domain__c,lower_domain1__c,lower_domain2__c  from Account where Has_Contract__c = 'Yes' AND (lower_domain__c in:Domains OR lower_domain1__c in:Domains OR lower_domain2__c in:Domains)]; 
  System.debug('I am here1');   

  for( Account ac:Accounts)
  {if(ac.lower_domain__c !=null && ac.lower_domain__c !='')
     Domain_to_Account.put(ac.lower_domain__c,ac.id);
   if(ac.lower_domain1__c !=null && ac.lower_domain__c !='')
     Domain1_to_Account.put(ac.lower_domain1__c,ac.id);
   if(ac.lower_domain2__c !=null && ac.lower_domain__c !='')
     Domain2_to_Account.put(ac.lower_domain2__c,ac.id);
   }
      
 }




for(lead ld1:UpdLeads)
 { if(ExistCont.containsKey(ld1.email))
    {         
     ExistCont.get(ld1.email).Duplicate_lead__c = true;              
     DupLeadIds.add(ld1.id);                         
  
    }
   else
   {
   if(Domain_to_Account.containsKey(ld1.domain__c.tolowercase()))
    AccId = Domain_to_Account.get(ld1.domain__c.tolowercase());
   if(Domain1_to_Account.containsKey(ld1.domain__c.tolowercase()))
    AccId = Domain1_to_Account.get(ld1.domain__c.tolowercase());
   if(Domain2_to_Account.containsKey(ld1.domain__c.tolowercase()))
    AccId = Domain2_to_Account.get(ld1.domain__c.tolowercase());
   if (AccID !=NULL)
   { 
    System.debug('Lead domain ='+ ld1.domain__c);                                       

    Database.LeadConvert lc = new database.LeadConvert();
    lc.doNotCreateOpportunity = true;
    lc.setLeadId(ld1.id);
    lc.setAccountId(AccID); 
    LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
    lc.setConvertedStatus(convertStatus.MasterLabel);


    Database.LeadConvertResult lcr = Database.convertLead(lc);
    System.debug('TRIGGER ERRORS =' + lcr.getErrors());                                       
   }
  } // else                  
 }
 DupLead =[select id from lead where id in:DupLeadIds];
 if(!DupLead.isempty())
 {delete(DupLead);}
 
 if(!ExistCont.isempty())                
 {update (ExistCont).values();} */
}