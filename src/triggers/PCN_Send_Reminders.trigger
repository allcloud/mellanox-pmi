trigger PCN_Send_Reminders on PCN__c (after update) {
	//Set<ID> iids = new Set<ID>(); 
	for (PCN__c p : Trigger.New){
        if( Trigger.oldmap.get(p.id).Send_Reminders_14_Days_Approval_Require__c==false && p.Send_Reminders_14_Days_Approval_Require__c==true) {
        	//iids.add(p.id);
        	//only pull out PCN_Contacts which Contact !+ null and PCN_Notification = true
            List<PCN_Contacts__c> pcn_contacts_updates = new List<PCN_Contacts__c>([select id,Contact__c,PCN__c,Contact__r.PCN_Approver__c,
            																				is_Approver_by_OPN__c,Contact_Email__c  
            																			from PCN_Contacts__c 
                                                                                        where Contact__c!=null 
                                                                                        and Contact_PCN_Notification__c=true 
                                                                                        and PCN__c=:p.ID
                                                                                        and ( (Contact_Approver__c = 'Yes' and PCN_Approver_by_OPN_flag__c = false) OR is_Approver_by_OPN__c = true )]);
            
            List<String> ccAddresses = new List<String>{'galitb@mellanox.com','khoa@mellanox.com'};
            List<String> toAddresses = new List<String>();
            Set<String> set_toAddresses = new Set<String>();
            
            Set<ID> contact_IDs = new Set<ID>();
            
            for (PCN_Contacts__c pcontact : pcn_contacts_updates){
                if(pcontact.Contact_Email__c != null && set_toAddresses.contains(pcontact.Contact_Email__c) == false) {
                	toAddresses.add(pcontact.Contact_Email__c);
                	set_toAddresses.add(pcontact.Contact_Email__c);
                }
            }   

            if(p.Additional_Contact_1__c != null)
                contact_IDs.add(p.Additional_Contact_1__c);
            if(p.Additional_Contact_2__c != null)
                contact_IDs.add(p.Additional_Contact_2__c);
            if(p.Additional_Contact_3__c != null)
                contact_IDs.add(p.Additional_Contact_3__c);
            if(p.Additional_Contact_4__c != null)
                contact_IDs.add(p.Additional_Contact_4__c);
            if(p.Additional_Contact_5__c != null)
                contact_IDs.add(p.Additional_Contact_5__c);
            if(p.Additional_Contact_6__c != null)
                contact_IDs.add(p.Additional_Contact_6__c);
            if(p.Additional_Contact_7__c != null)
                contact_IDs.add(p.Additional_Contact_7__c);
            
            for (Contact c : [select Email from Contact where id in :contact_IDs]) {
            	ccAddresses.add(c.Email);	
            }
            //send a copy to Basmat & Galit & Noa & Marie D
            //Set<ID> MLNX_copy_Contact_IDs = new Set<ID>{'0035000001v4plZ'}; //Galit onnly
            
            //Set<ID> MLNX_copy_Contact_IDs = new Set<ID>{'00350000021fl7I','0035000001v4plZ','00350000015SskE','00350000025TQFe'};    
			  Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
			   
			   if(toAddresses.size() > 0) {
				   mail.setToAddresses(toAddresses);
				   mail.setccAddresses(ccAddresses);
			   }
			   else {
			   	   mail.setToAddresses(ccAddresses);	
			   }
			   
			 //mail.setTemplateId('00XS0000000QNZ9');
			 //mail.setTargetObjectId('005S0000000EyF0'); //005S0000000EyF0    
			  mail.SetSubject( 'Reminder - Please Review PCN ' + p.Name);       
			   mail.SetPlainTextBody('Dear customer, \n \n Please note the following PCN that still requires your attention \n \n PCN Name: '
			   + p.Name +    
			  '\n \n PCN Description: ' + p.PCN_Description__c +  
			   '\n \n Published Date: ' + p.Publish_Date__c +
			   '\n \n Type: ' + p.Type__c +
			   '\n \n Mellanox Portal login: https://mymellanox.force.com/support/SupportLogin');      
			   
			  /* TEST:  mail.SetPlainTextBody('*** NEW USER CREATED ALERT *** \n \n New Customer Portal user was created \n Please'+    
			  ' grant him access permissions by assigning him to relevant public groups \n To access the user click on the link: \n  https://cs1.salesforce.com/'+ NewUser.Id);      
			   */    
			  mail.setReplyTo('galitb@mellanox.com');
			  //mail.setSenderDisplayName('Mellanox Support Admin');
			  mail.setOrgWideEmailAddressId('0D25000000001PC');
			  mail.setSaveAsActivity(false);
			  mail.setBccSender(false);
			  mail.setUseSignature(false);       
			  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail});
  
        } 
	}      
	//if(iids.size() == 0)
	//	return;
}