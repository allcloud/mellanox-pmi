trigger SME_approval_process on Case (after insert, after update) {

List<case> ApprovalCases = new List<case>();
List<id> ApprovalCasesIds = new List<id>();

List<string> ProdFam = new List<string>(); 
Map<string,list<id>> ProdFam_map = new Map<string,list<id>> (); 
List<id> SMEList = new  List<id>(); 
List<SME_Table__c> RelProd = new  List<SME_Table__c>();
List<id> SMEs = new  List<id>(); 
Map<string,case> updCases = new Map<string,case> (); 
List<id> nextApprover = new  List<id>(); 
Map<id,id> CaseOldAssignee = new Map<id,id> (); 
            
                                                
for(Case Cs: Trigger.new)     
      {            
            if(trigger.isupdate && Cs.case_type__c == 'System Case' && cs.in_approval__c==False &&  cs.Board_Part_Number__c != NULL  &&
            (( cs.State__c == 'Open AE' && trigger.oldmap.get(cs.id).state__c != 'Open AE' && cs.AE_engineer__c == NULL )
            ||(  trigger.oldmap.get(cs.id).AE_engineer__c == NULL && cs.AE_engineer__c != NULL && trigger.oldmap.get(cs.id).state__c != 'Open AE'))) 
              
            { ApprovalCases.add(Cs);    
              ApprovalCasesIds.add(Cs.id);   
              ProdFam.add(cs.Board_Part_Number__c);       
              CaseOldAssignee.put(cs.id, trigger.oldmap.get(cs.id).Assignee__c);               
            } 
        
      }


if(!ApprovalCases.isempty())

{   RelProd =  [select id, name,Product_SME__c from SME_Table__c where name in :ProdFam]; // The SME's table 
    for(case c:[select id, state__c, Assignee__c,Current_Day_of_week__c, escalated_ae_time__c, in_approval__c, SME_approver1__c, SME_approver2__C, SME_approver3__C, SME_approver4__C, SME_approver5__C, SME_approver6__C from case where id in:ApprovalCasesIds])  
     { updCases.put(c.id,c);} // updCases contains all the cases 
 
  if(!RelProd.isempty())
  {
	    for(SME_Table__c pm:RelProd)
	    {    
		     if(!ProdFam_Map.containsKey(pm.name)) 
		     {
			      SMEs = new  List<id>(); 
			      SMEs.add(pm.Product_SME__c);
			      ProdFam_Map.put(pm.name,SMEs);  
		     } else
		     {
			      SMEs =ProdFam_Map.get(pm.name);
			      SMEs.add(pm.Product_SME__c); 
			      ProdFam_Map.put(pm.name,SMEs); 
		     }
	    }
   }         
}  
    
        
   for( case cs1:ApprovalCases)           
    {        
      if (ProdFam_Map.containsKey(cs1.Board_Part_Number__c)  )  
      { updCases.get(cs1.id).in_Approval__c = true;         
      if(cs1.Current_Day_of_week__c != 0 &&  cs1.Current_Day_of_week__c != 6 && cs1.Current_Day_of_week__c != 5)       
     { updCases.get(cs1.id).state__c = 'Pending SME Approval'; 
       updCases.get(cs1.id).escalated_ae_time__c = NULL;       
       updCases.get(cs1.id).assignee__c =  CaseOldAssignee.get(cs1.id); }      
      if( ProdFam_Map.get(cs1.Board_Part_Number__c).size()>0){ updCases.get(cs1.id).SME_approver1__c = ProdFam_Map.get(cs1.Board_Part_Number__c)[0];}                 
      if( ProdFam_Map.get(cs1.Board_Part_Number__c).size()>1)      
        { updCases.get(cs1.id).SME_approver2__c = ProdFam_Map.get(cs1.Board_Part_Number__c)[1];}      
        else{updCases.get(cs1.id).SME_approver2__c = '00550000001qYZc';} // Ron Assulin     
      if( ProdFam_Map.get(cs1.Board_Part_Number__c).size()>2)        
        { updCases.get(cs1.id).SME_approver3__c = ProdFam_Map.get(cs1.Board_Part_Number__c)[2];}                 
        else {updCases.get(cs1.id).SME_approver3__c = '005500000015XiP';} // Yair Ifergan
      if( ProdFam_Map.get(cs1.Board_Part_Number__c).size()>3)        
        { updCases.get(cs1.id).SME_approver4__c = ProdFam_Map.get(cs1.Board_Part_Number__c)[3];}                 
        else {updCases.get(cs1.id).SME_approver4__c = '005500000015XiP';} // Yair Ifergan 
      if( ProdFam_Map.get(cs1.Board_Part_Number__c).size()>4)        
        { updCases.get(cs1.id).SME_approver5__c = ProdFam_Map.get(cs1.Board_Part_Number__c)[4];}                 
        else {updCases.get(cs1.id).SME_approver5__c = '00550000001qYZc';} // Ron Assulin 
      if( ProdFam_Map.get(cs1.Board_Part_Number__c).size()>5)        
        { updCases.get(cs1.id).SME_approver6__c = ProdFam_Map.get(cs1.Board_Part_Number__c)[5];}                 
        else {updCases.get(cs1.id).SME_approver6__c = '005500000015XiP';} // Yair Ifergan             
           
       }    
     }      

    if(!ApprovalCases.isempty())
    { update (updCases.values());}
                                   
    for( case cs1:ApprovalCases)           
    {         approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
      req1.setComments('Submitting request for approval.'); 
      req1.setObjectId(cs1.id);
     // nextApprover.add(ProdFam_Map.get(cs1.Board_Part_Number__c)[0]);           
    //  req1.setNextApproverIds(new Id[]{ProdFam_Map.get(cs1.Board_Part_Number__c)[0]});      
       approval.ProcessResult result = Approval.process(req1); 
      System.assert(result.isSuccess());  
      System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());       
                   
          
      if(cs1.Current_Day_of_week__c == 0 || cs1.Current_Day_of_week__c == 5 ||  (cs1.Current_Day_of_week__c == 6 && (cs1.priority == 'High' || cs1.priority == 'Fatal') ) ) // Saturday and Friday                    
      {              
      List<Id> newWorkItemIds = result.getNewWorkitemIds();

              
      Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();       
      req2.setWorkitemId(newWorkItemIds.get(0));

      
      req2.setComments('SME Request was approved automatically on weekend');
      req2.setAction('Approve');
      req2.NextApproverIds=(ProdFam_Map.get(cs1.Board_Part_Number__c));    
      Approval.ProcessResult result2 =  Approval.process(req2);           
    }                     


      }
   

if(!ApprovalCases.isempty())
{ update (updCases.values());}


}