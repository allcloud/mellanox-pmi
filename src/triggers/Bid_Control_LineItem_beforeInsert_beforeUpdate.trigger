trigger Bid_Control_LineItem_beforeInsert_beforeUpdate on Bid_Control_Lines__c (before insert, before update) {
	List<Bid_Control_Lines__c> blines = new List<Bid_Control_Lines__c>();
	Set<ID> PB_IDs = new Set<ID>();
	Set<ID> prods_IDs = new Set<ID>();
	
	for (Bid_Control_Lines__c b : Trigger.new) {
		if(Trigger.isInsert) {
			PB_IDs.add(b.PricebookID__c);
			prods_IDs.add(b.Product__c);
			blines.add(b);	
		}
		else {
			if( (b.Product__c != null && Trigger.oldmap.get(b.id).Product__c != b.Product__c) 
					|| Trigger.oldmap.get(b.id).Requested_Price__c != b.Requested_Price__c
					//|| Trigger.oldmap.get(b.id).List_Price__c != b.List_Price__c
					//|| Trigger.oldmap.get(b.id).Minimum_Price__c != b.Minimum_Price__c
			  	) {
				PB_IDs.add(b.PricebookID__c);
				prods_IDs.add(b.Product__c);
				blines.add(b);
			}
		}
	} //end for loop
	
	if(blines.size() == 0)
		return;
	
	//build map
	Map<ID,Product2> map_prods = new Map<ID,Product2>([select id, OEM_PL__c from Product2 where id in :prods_IDs]);
	
	List<PricebookEntry> lst_pbe = new List<PricebookEntry>([select id,Product2ID,Pricebook2ID,UnitPrice from PricebookEntry 
																	where Product2ID in :prods_IDs 
																	and Pricebook2ID in :PB_IDs]);
																	
	Map<ID,Map<ID,Double>> map_prodID_PricebookID_Price = new Map<ID,Map<ID,Double>>();
	Map<ID,Double> tmp_map_PricebookID_Price = new Map<ID,Double>();  
	for (PricebookEntry pbe : lst_pbe){
		if (map_prodID_PricebookID_Price.get(pbe.Product2ID)==null){
			tmp_map_PricebookID_Price = new Map<ID,Double>();
			tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
			map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
		}
		else {
			tmp_map_PricebookID_Price = map_prodID_PricebookID_Price.get(pbe.Product2ID);
			tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
			map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
		}
	}																				
	// Derive min price from product OEM_PL__c field	
	Double min_price;
	for (Bid_Control_Lines__c b : blines) {
		if(map_prodID_PricebookID_Price.get(b.Product__c) != null && map_prodID_PricebookID_Price.get(b.Product__c).get(b.PricebookID__c) != null)
			b.List_Price__c = map_prodID_PricebookID_Price.get(b.Product__c).get(b.PricebookID__c);
		else
			b.List_Price__c = null;
			
		
		//Minimum price is set by formula field to Product
		if(map_prods.get(b.Product__c) != null && map_prods.get(b.Product__c).OEM_PL__c != null)
			min_price = map_prods.get(b.Product__c).OEM_PL__c;
		else
			min_price = null;
		
		
		//Set Line Level Approval
		if(min_price != null && b.Requested_Price__c < min_price)
			b.Line_Approval_Level__c = 2;
		else if (b.List_Price__c != null && b.Requested_Price__c < b.List_Price__c)
			b.Line_Approval_Level__c = 1;
		else
			b.Line_Approval_Level__c = 0;
			
	}
	
}