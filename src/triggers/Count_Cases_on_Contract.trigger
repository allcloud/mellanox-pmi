trigger Count_Cases_on_Contract on Case (after insert,after update,after delete) {

List<id> Accounts = new List<id>();
map<id,List<contract2__c>> contracts = new map<id,List<contract2__c>>(); 
map<id,id> AddCase = new map<id,id>(); 
map<id,id> RemoveCase = new map<id,id>(); 
List<case> cases = new List<case>();
List<contract2__c> allCont = new list<contract2__c>();
List<contract2__c> updContracts = new List<contract2__c>();
Set<string> updContractsSet = new Set<string>();

if(!trigger.isdelete)
{
for(Case cs:trigger.new)
{
 if(trigger.isinsert&& cs.Accountid!= NULL)    
  { Accounts.add(cs.Accountid);      
    AddCase.put(cs.id,cs.Accountid);       
    Cases.add(cs);           
  }          
        
         
 if (trigger.isupdate && cs.Accountid!=trigger.oldmap.get(cs.id).AccountId)
  {        
    Accounts.add(cs.Accountid);        
    Accounts.add(trigger.oldmap.get(cs.id).AccountId);      
    RemoveCase.put(cs.id,trigger.oldmap.get(cs.id).AccountId);      
    AddCase.put(cs.id,cs.Accountid);            
    cases.add(cs);
  }
 }//for 
}
                      
if(trigger.isdelete)
{                                                  
  for(Case csd:trigger.old)
  {
  if(csd.Accountid!= NULL)    
   { Accounts.add(csd.Accountid);      
    RemoveCase.put(csd.id,csd.Accountid);       
    Cases.add(csd);           
   }  
 }                      
}     

if(Accounts.size()>0)
{
 for(contract2__c con:[select id,name,account__c, support_cases__c, rma_cases__c,Case_Contact_Email__c from contract2__c where account__c in :Accounts])
 {        
   if(!contracts.containsKey(con.account__c ))                 
    {List<contract2__c> NewAllCont = new list<contract2__c>();     
     NewAllcont.add(con);         
     contracts.put(con.account__c,NewAllcont);        
     }        
    else                         
    {           
     allcont =contracts.get(con.account__c);    
     allcont.add(con);           
     contracts.put(con.account__c,allcont);                
    }          
                                    
 }

}

for(case css:cases)
{
  if(contracts.containskey(AddCase.get(css.Id)))
  {           
   for(Contract2__c cnt: contracts.get(AddCase.get(css.Id)))
   {
    if(!updContractsSet.contains(cnt.name))      
     {system.debug('adding upd contract' + cnt.name + '  Case num:' + css.casenumber  ); 
       if(css.ContactEmail__c != null) cnt.Case_Contact_Email__c = css.ContactEmail__c;
       updContractsSet.add(cnt.name);      
       updContracts.add(cnt);
     }                                               
    if(css.recordtypeId == ENV.CaseTypeSupport)              
    {               
      if(cnt.support_cases__c == NULL)             
       {cnt.support_cases__c = 1;}
       else                     
            { cnt.support_cases__c = cnt.support_cases__c +1; }        
                
    }  // support case aded              
                    
         
    if(css.recordtypeId == ENV.CaseTypeRMA)              
    {               
      if(cnt.RMA_cases__c  == NULL)      
       {cnt.RMA_cases__c = 1;}
       else                 
         { cnt.RMA_cases__c = cnt.RMA_cases__c +1; }        
     } // RMA cases              
    } // for contracts 
   } //if contract exists for new case account     
                                       
   if(contracts.containskey(RemoveCase.get(css.Id)))
   {         
     for(Contract2__c cnt1: contracts.get(RemoveCase.get(css.Id)))
      {
       if(!updContractsSet.contains(cnt1.name))      
       { 
         system.debug('removing upd contract' + cnt1.name + '  Case num:' + css.casenumber ); 
         updContractsSet.add(cnt1.name);      
         updContracts.add(cnt1);
       }
         
                                                
       if(css.recordtypeId == ENV.CaseTypeSupport)              
       {               
         if(cnt1.support_cases__c != NULL)             
         cnt1.support_cases__c = cnt1.support_cases__c -1;        
       }        
      
      if(css.recordtypeId == ENV.CaseTypeRMA)              
       {               
        if(cnt1.RMA_cases__c != NULL)             
        cnt1.RMA_cases__c = cnt1.RMA_cases__c -1;         
       } //rma                 
     }//for contracts     
   } //if contract exists for old case account              
 } //for                 
 update UpdContracts;
 }