trigger Call_Customer_Before_Insert_Update on Call_Center_Customer__c (before insert, before update) {
    
    
    List<string> CusEmails = new List<string>(); 
    List<string> CusSN = new List<string>(); 
    List<string> CusAccounts = new List<string>();  
    Map<string,Contact> RelCon = new Map<string,Contact>();
    Map<string,Account> RelAccount = new Map<string,Account>();
    Map<string,Id> RelAsset = new Map<string,Id>();
    Map<string,string> RegByAssetAccount = new Map<string,string>();
    //Contact FoundContact = new Contact();
    String Domain;
    String Domain1;
    List<Account> DomainAccount = new List<Account>();

    for(Call_Center_Customer__c Ccen:trigger.new) 
    {
    
        if(Ccen.Contact_name__c != Null || Ccen.Existing_Case__c != Null)
         { 
            Contact FoundContact = [select id,email, FirstName, LastName, Contact_Origin__c, c.Account.Name,c.Account.support_center__c, c.Account.Id, c.AccountId, c.Account.Name_Lower__c 
                                   from Contact c    
                                    where id = :Ccen.Contact_name__c OR id = :Ccen.Existing_Case_contact__c LIMIT 1];   
           Ccen.Email__c = FoundContact.email;
           Ccen.First_name__c = FoundContact.firstname;
           Ccen.Last_name__c = FoundContact.LastName;
           Ccen.company__c = FoundContact.Account.Name; 
           Ccen.Geographic__c = FoundContact.Account.support_center__c;
           Ccen.Account__c = FoundContact.Account.id;
           Ccen.Account_name__c = FoundContact.Account.id;
           Ccen.Contact__c = FoundContact.Id;
           if(FoundContact.Contact_Origin__c != NULL && FoundContact.Contact_Origin__c != '')
           {Ccen.Customer_Source__c = FoundContact.Contact_Origin__c;}
           else {Ccen.Customer_Source__c = 'Mellanox';}
         } // end of FoundContact
         else 
         {
             if(Ccen.Account_name__c != NULL)
              { 
                    if(Ccen.email__c == NULL )
                        { Ccen.email__c.adderror('Please fill the customer email');}
                    if(Ccen.First_name__c == NULL)
                        { Ccen.First_name__c.adderror('Please fill the customer First Name');}
                    if(Ccen.Last_Name__c == NULL)
                        { Ccen.Last_Name__c.adderror('Please fill the customer Last Name');}
                    if(Ccen.Customer_source__c == NULL)
                        { Ccen.Customer_source__c.adderror('Please fill the customer Source');}
                    
                    Account FoundAccount = [select id, Name, support_center__c from Account Where id = :Ccen.Account_name__c];
                    Ccen.Account__c = FoundAccount.id;
                    Ccen.Company__c = FoundAccount.Name;
                    if(FoundAccount.support_center__c != NULL && FoundAccount.support_center__c != '')
                    {Ccen.Geographic__c = FoundAccount.support_center__c;}
              } // end if Account not empty
             
             if(Ccen.Email__c != NULL)
                {CusEmails.add(Ccen.Email__c); }
             if(Ccen.Serial_Number__c != NULL)
                {CusSN.add(Ccen.Serial_Number__c);} 
                          
       } // end else (contact not found)
     }   
    if(!CusEmails.isempty()) 
     {                                               
          for(Contact Con:[select id,email, c.Account.Name, c.Account.support_center__c, c.Account.Id, c.AccountId, c.Account.Name_Lower__c, c.FirstName, c.LastName, c.Contact_Origin__c 
                            from Contact c    
                           where Email in :CusEmails])   
            {
                RelCon.put(Con.email,Con);
            }
     }   
    /*if(!CusAccounts.isempty()) 
     {                                               
      for(Account Acc:[select id,Name_Lower__c, support_center__c from Account where Name_Lower__c in :CusAccounts])   
        {RelAccount.put(Acc.Name_Lower__c,Acc);}
      
     }  */ 
     
     if(!CusSN.isempty())
     {
     for(Asset Ast:[select id, A.Contract2__r.Account__c,A.Serial_Number__c  from Asset A    
          where Serial_Number__c in :CusSN])   
        {RelAsset.put(Ast.Serial_Number__c,Ast.Contract2__r.Account__c);}
         
    
      }      
       
     
    
    for(Call_Center_Customer__c CC: Trigger.new)
     {  
       if(RelCon.containsKey(CC.Email__c))     
       {  
          CC.Contact__c= RelCon.get(CC.Email__c).id;     
          CC.Account__c = RelCon.get(CC.Email__c).Account.Id;
          CC.First_name__c = RelCon.get(CC.Email__c).FirstName;
          CC.Last_name__c = RelCon.get(CC.Email__c).LastName;
          if(RelCon.get(CC.Email__c).Account.support_center__c != NULL && RelCon.get(CC.Email__c).Account.support_center__c != '' )
          {CC.Geographic__c =RelCon.get(CC.Email__c).Account.support_center__c;}
          if(RelCon.get(CC.Email__c).Contact_Origin__c != NULL && RelCon.get(CC.Email__c).Contact_Origin__c!= '')
          {CC.Customer_Source__c = RelCon.get(CC.Email__c).Contact_Origin__c;}
    
       }else
       
       { 
         if(CC.Contact_name__c == NULL)
         { 
          if(CC.email__c == NULL ||CC.email__c == '')
              { CC.email__c.adderror('Please fill the customer email');}
          if(CC.First_name__c == NULL)
              { CC.First_name__c.adderror('Please fill the customer First Name');}
          if(CC.Last_Name__c == NULL)
              { CC.Last_Name__c.adderror('Please fill the customer Last Name');}
          if(CC.Customer_source__c == NULL)
              { CC.Customer_source__c.adderror('Please fill the customer Source');}
          if(CC.geographic__c == NULL)
              { CC.geographic__c.adderror('Please fill the customer Geographic Location');}   
               
             Contact NewCon = new Contact();
             NewCon.FirstName = CC.First_name__c;
             if(CC.Last_Name__c != NULL)   
             {NewCon.LastName = CC.Last_Name__c;}
             else {NewCon.LastName = 'Unknown';} 
             NewCon.email=CC.email__c;
             NewCon.Contact_Origin__c =CC.Customer_Source__c;           
             NewCon.phone = CC.Phone_1__c;               
             if(CC.Account_name__c != NULL)
              {NewCon.AccountId = CC.Account_name__c;}
             else
              {
                NewCon.Company_Name_from_Web__c = CC.company__c;
                                    
              if(cc.Email__c != NULL)
               {Domain = cc.domain__c;
                Domain1 = cc.domain_com__c;


              //  DomainAccount =[select id from Account where Has_Contract__c = 'Yes' AND (lower_domain__c = :Domain OR lower_domain1__c = :Domain OR lower_domain2__c =:Domain)];  
                  DomainAccount =[select id from Account where recordtypeId = '01250000000DP1n' AND (lower_domain__c = :Domain OR lower_domain1__c = :Domain OR lower_domain2__c =:Domain or   lower_domain__c = :Domain1 OR lower_domain1__c = :Domain1 OR lower_domain2__c =:Domain1) ORDER BY  Primary_Domain__c DESC NULLS LAST, Highest_Active_contract_rank__c  DESC NULLS LAST LIMIT 1];  

              if(!DomainAccount.isempty())
                {NewCon.accountId = DomainAccount[0].id ;}
                 else{
                      if(CC.geographic__c == 'Global')
                        {NewCon.AccountId = ENV.UnknownAccount;}
                      if(CC.geographic__c == 'US')
                        {NewCon.AccountId = ENV.UnknownUSAccount;}
                      if(CC.geographic__c == 'EMEA'||CC.geographic__c == 'APAC')
                        {NewCon.AccountId = ENV.UnknownEMEAAccount;}

                      }  // end else (domain not found)
               } // end of domain calc
             }  // end if Account == null
               
            if(cc.Contact__c == NULL) 
            {
             insert  NewCon;
             cc.New_Contact__c = NewCon.id;
             cc.Contact__c = NewCon.id;
            }
            } 
          
         /*if (RelAccount.containsKey(CC.Company__c.tolowercase())) 
          {  CC.Account__c = RelAccount.get(CC.Company__c.tolowercase()).id;
             if(RelAccount.get(CC.Company__c.tolowercase()).support_center__c != NULL)
              {CC.Geographic__c =RelAccount.get(CC.Company__c.tolowercase()).support_center__c;}
           }*/
        }
        
       if(RelAsset.containsKey(CC.Serial_Number__c))
        {
         CC.Account__c = RelAsset.get(CC.Serial_Number__c);
        } 
    
       
      }   
          
}