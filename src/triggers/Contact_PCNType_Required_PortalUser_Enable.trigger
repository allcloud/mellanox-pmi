trigger Contact_PCNType_Required_PortalUser_Enable on Contact (after insert, after update) {
	Set<ID> setIDs = new Set<ID>();
	List<Contact> lst_contacts = new List<Contact>();
	if(Trigger.isInsert){
		for (Contact cc : Trigger.New){
			if (cc.PCN_Approver__c=='Yes'){
				lst_contacts.add(cc);	
				setIDs.add(cc.ID);
			}				
		}
	}
	else {
		for (Contact cc : Trigger.New){
			if (Trigger.oldMap.get(cc.ID).PCN_Approver__c!='Yes' && cc.PCN_Approver__c=='Yes'){
				lst_contacts.add(cc);
				setIDs.add(cc.ID);	
			}				
		}
	}	
	if(lst_contacts.size()==0)
		return;
	List<User> lst_users = new List<User>([Select IsPortalEnabled, ContactId From User where ContactID in :setIDs and IsPortalEnabled=true]);
	Map<ID,User> map_contactID_User = new Map<ID,User>();
	for (User u : lst_users){
		map_contactID_User.put(u.ContactID,u);
	} 				
	for (Contact c : lst_contacts){
		if(map_contactID_User!=null && map_contactID_User.get(c.ID)==null)
			c.addError('Contact must have portal user enabled first before PCN_Type set to Approval Required');
	}
}