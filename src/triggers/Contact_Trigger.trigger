//Copyright (c) 2012, Rima Rohana , BLAT-LAPIDOT
//All rights reserved.
//
 
//
// History
//
// Version    Date          Author               Comments / updates
// 1.0.0      17/12/13      Rima Rohana          initial 
//                                               
//                                              
//                                              

//
 
//
// Overview / Description
// The trigger is fired whenever there is an update on a Contact and CRM_Content_Permissions was changed.
// it delete the user group member of the contact from the old group and 
// it insert new group member to the new group 
// it use a futuer class because it make DML opration on GroupMember object
// that otherwise it throws MIXED_DML_OPERATION error ..  
 
//
trigger Contact_Trigger on Contact (after update, after insert) 
{
  if(trigger.isAfter && trigger.isUpdate && ENV.createCRMGroup)
  {
    
    map<Id,string> map_ContactIdToNewGroupId = new map<Id,string>();
    map<Id,string> map_ContactIdToOldGroupId = new map<Id,string>();
    set<Id> set_ContactId = new set<Id>();
    for(Contact c: trigger.new)
    {
      if( c.CRM_Content_Permissions__c != trigger.oldMap.get(c.Id).CRM_Content_Permissions__c )
      {
        system.debug('==>c.CRM_Content_Permissions__c : '+c.CRM_Content_Permissions__c);
        set_ContactId.add(c.Id);
        //map hold the contact should add to group as member
        if(map_ContactIdToNewGroupId.get(c.Id) == null && c.CRM_Content_Permissions__c != 'Other' && c.CRM_Content_Permissions__c != '')
          map_ContactIdToNewGroupId.put(c.Id,c.CRM_Content_Permissions__c);
        //map hold the contact should remove its memnber from group   
        system.debug('==>trigger.oldMap.get(c.Id).CRM_Content_Permissions__c '+trigger.oldMap.get(c.Id).CRM_Content_Permissions__c);
        if(map_ContactIdToOldGroupId.get(c.Id) == null && trigger.oldMap.get(c.Id).CRM_Content_Permissions__c != 'Other' && trigger.oldMap.get(c.Id).CRM_Content_Permissions__c != '')  
          map_ContactIdToOldGroupId.put(c.Id,trigger.oldMap.get(c.Id).CRM_Content_Permissions__c);
      }
    }
    system.debug('==>map_ContactIdToOldGroupId '+map_ContactIdToOldGroupId);
    if(!map_ContactIdToNewGroupId.IsEmpty() || !map_ContactIdToOldGroupId.IsEmpty())
    {
      // call future method
      cls_Contact_Trigger.ManageGroupMember(set_ContactId,map_ContactIdToNewGroupId,map_ContactIdToOldGroupId) ;
    }
  }
  
  if (trigger.isAfter && trigger.isInsert) {
    
    clsContact handler = new clsContact();
    cls_Contact_Trigger handler2 = new cls_Contact_Trigger();
    handler.createCommunityUserHPAccount(trigger.New);
    handler2.UpdateAccountDomainFromContact(trigger.New, null);
  }
  
  if (trigger.isAfter && trigger.isUpdate) {
    
      cls_Contact_Trigger handler2 = new cls_Contact_Trigger();
      handler2.UpdateAccountDomainFromContact(trigger.New, trigger.oldMap);
  }
  
  
}