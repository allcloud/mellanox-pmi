trigger Bid_Control_LineItem_AfterInsert_AfterUpdate_AfterDelete on Bid_Control_Lines__c (after delete, after insert, after update) {
	Set<ID> BCN_IDs = new Set<ID>();
	if(Trigger.IsInsert || Trigger.IsUpdate){
		for (Bid_Control_Lines__c b : Trigger.new) {
			if(Trigger.isInsert) {
				BCN_IDs.add(b.Bid_Control__c);
			}
			else {
				if((b.Product__c != null && Trigger.oldmap.get(b.id).Product__c != b.Product__c) 
						|| Trigger.oldmap.get(b.id).Requested_Price__c != b.Requested_Price__c) {
					BCN_IDs.add(b.Bid_Control__c);
				}
			}
		} 
	}
	else {
		for (Bid_Control_Lines__c bb : Trigger.old) {
			BCN_IDs.add(Trigger.oldmap.get(bb.id).Bid_Control__c);
		}
	}
		
	if(BCN_IDs.size()==0)
		return;
		
	List<Bid_Control__c> lst_bcns = new List<Bid_Control__c>([select id, Status__c from Bid_Control__c where id in :BCN_IDs]);
	for (Bid_Control__c b : lst_bcns){
		b.Status__c = 'Draft';
	}
	if(lst_bcns.size() > 0)
		update lst_bcns;
}