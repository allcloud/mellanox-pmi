trigger PCN_Contacts_submit_approvals on PCN_Contacts__c (after update) {
	Set<ID> PCN_IDs_approved = new Set<ID>();
	Set<ID> PCN_IDs_rejected = new Set<ID>();
	
	for (PCN_Contacts__c p : Trigger.new) {
		if( (p.PCN_Approver_by_OPN_flag__c && p.is_Approver_by_OPN__c == true) || (!p.PCN_Approver_by_OPN_flag__c && p.Contact_Approver__c=='Yes'))
			if(Trigger.oldmap.get(p.ID).Approved__c == false && p.Approved__c==true)
				PCN_IDs_approved.add(p.PCN__c);
			if(Trigger.oldmap.get(p.ID).Rejected__c == false && p.Rejected__c==true)
				PCN_IDs_rejected.add(p.PCN__c);
	}
	
	if (PCN_IDs_approved.size() == 0 && PCN_IDs_rejected.size() == 0)
		return;
	
	List<PCN__c> lst_pcns = new List<PCN__c>([select id,Total_Pending_Approval__c,Total_Rejections__c 
													from PCN__c where id in :PCN_IDs_approved OR id in :PCN_IDs_rejected]);
	
	for (PCN__c p : lst_pcns) {
		if (PCN_IDs_approved.contains(p.ID)){
			p.Total_Pending_Approval__c = (p.Total_Pending_Approval__c != null && p.Total_Pending_Approval__c>0) ? (p.Total_Pending_Approval__c - 1) : p.Total_Pending_Approval__c; 
		}
		
		if (PCN_IDs_rejected.contains(p.ID)){
			if (p.Total_Rejections__c == null)
				p.Total_Rejections__c = 0;
			p.Total_Rejections__c++; 	
		}
	}
	
	if(lst_pcns != null && lst_pcns.size() > 0)
		Database.update(lst_pcns,false);
															
}