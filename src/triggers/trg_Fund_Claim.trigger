trigger trg_Fund_Claim on SFDC_MDF_Claim__c (after update, before insert, after insert) {
	
	cls_trg_Fund_Claim handler = new cls_trg_Fund_Claim();
		
	if (trigger.isAfter) {	
		
		if (trigger.isUpdate) {
			
			handler.sendFundClaimsToApproval(trigger.new, trigger.oldMap);
			handler.sendEmailAndInvoiceAttachmentWhenClaimIsApproved(trigger.new, trigger.oldMap);
			handler.updateFundClaimsApprovalRequest(trigger.new, trigger.oldMap);
		}
		
		else if (trigger.isInsert) {
			
			handler.sendFundClaimsToApproval(trigger.new, null);
		}
	}
	
	else if (trigger.isBefore) {
			
		if (trigger.isInsert) {
			
			handler.updateFundClaimsTAM(trigger.New);
		}
	}

}