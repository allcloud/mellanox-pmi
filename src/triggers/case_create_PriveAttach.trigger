trigger case_create_PriveAttach on Case (after insert) {

list<PrivateAttachment__c> NewPAs = new list<PrivateAttachment__c>();

//Create new Private Attachment object
if(trigger.isinsert)
{            
 for(case css:trigger.new)                          
  {    
    
      PrivateAttachment__c PA = new PrivateAttachment__c();        
      PA.Case__c = css.id;         
      NewPAs.add(PA);      
    
   }       
 
 
  insert NewPAs;      

 }

}