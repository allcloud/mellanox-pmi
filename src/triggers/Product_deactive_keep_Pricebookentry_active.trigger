trigger Product_deactive_keep_Pricebookentry_active on Product2 (before update, after update) {
    Set<ID> product_IDs = new Set<ID>();
    Set<ID> prodIDs_Need_min_prices = new Set<ID>();
    ID actualPB = '01s500000006N1V';
    ID DB_PB = '01s500000006J09';
    ID OEM_PB = '01s500000006J7F';
    List<Product2> prods_set_minprices = new List<Product2>();
    if (Trigger.isBefore) {
        for (Product2 pp : Trigger.new){
           if(pp.Agile_Status__c == 'EOL' || pp.Agile_Status__c == 'EOS')
               pp.isActive = false;
           else   if(pp.Agile_Status__c == 'NPI' || pp.Agile_Status__c == 'GA') {
                prodIDs_Need_min_prices.add (pp.ID);
                prods_set_minprices.add(pp);
            }
        }
        if(prodIDs_Need_min_prices.size() == 0)
            return;
        //build map
        List<Pricebookentry> lst_pbentry = new List<Pricebookentry>([select id,Product2ID,pricebook2id,UnitPrice 
                                                                    from Pricebookentry where product2Id in :prodIDs_Need_min_prices
                                                                    and (pricebook2id=:DB_PB or pricebook2id=:OEM_PB) ]);       
        if(lst_pbentry==null || lst_pbentry.size()==0)
            return;
        Map<ID,Map<String,Double>> map_prod_PB_Price = new Map<ID,Map<String,Double>>();
        Map<String,Double> map_PB_Price = new Map<String,Double>();
         
        for (Pricebookentry pbe : lst_pbentry) {
            if(map_prod_PB_Price.get(pbe.Product2ID) == null) {
                map_PB_Price = new Map<String,Double>();
                map_PB_Price.put(pbe.Pricebook2ID, pbe.UnitPrice);
                map_prod_PB_Price.put(pbe.Product2ID, map_PB_Price);
            }
            else {
                map_PB_Price = map_prod_PB_Price.get(pbe.Product2ID);
                map_PB_Price.put(pbe.Pricebook2ID, pbe.UnitPrice);
                map_prod_PB_Price.put(pbe.Product2ID, map_PB_Price);
            }
        } //end map
        
        for (Product2 prod : prods_set_minprices) {
            if(prod.Agile_Status__c == 'NPI') {
                if(map_prod_PB_Price.get(prod.ID) != null && map_prod_PB_Price.get(prod.ID).get(DB_PB) != null)
                    prod.OEM_PL__c = map_prod_PB_Price.get(prod.ID).get(DB_PB);
            }
            else if (prod.Agile_Status__c == 'GA') {
                if(map_prod_PB_Price.get(prod.ID) != null && map_prod_PB_Price.get(prod.ID).get(OEM_PB) != null)
                    prod.OEM_PL__c = map_prod_PB_Price.get(prod.ID).get(OEM_PB);
                prod.Availability__c = '';
            }
        } 
    } //end before Trigger
    
    if(Trigger.isAfter) {
        for (Product2 p : Trigger.new){
            if (Trigger.oldmap.get(p.ID).isActive==true && p.isActive==false){
                product_IDs.add(p.ID);
            }
        }
        if(product_IDs.size()==0)
            return;
        List<Pricebookentry> list_pbe = new List<Pricebookentry>([select id,isActive,UseStandardPrice from Pricebookentry where pricebook2id=:actualPB and product2Id in :product_IDs]);
        if(list_pbe==null || list_pbe.size()==0)
            return;
        
        for (Pricebookentry pbe : list_pbe){
            pbe.IsActive = true;
        }
        update list_pbe;
    }
}