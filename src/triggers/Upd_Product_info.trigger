trigger Upd_Product_info on Asset2__c (before insert) {

list<asset2__c> UpdAssets = new list<asset2__c>();
list<asset2__c> tmpList = new list<asset2__c>();
list<string> SelectAssets = new list<string>();
List<product2> Products = new list<product2>();
Map<string,list<Asset2__c>> RelatedProducts = new Map<string,list<Asset2__c>>();

 for(Asset2__c ast:trigger.new)
  {
    if(trigger.isinsert)
    {
    
    UpdAssets.add(ast);
    SelectAssets.add(ast.Part_Number__c); 
    if(RelatedProducts.containsKey(ast.part_number__c))
    { tmpList = RelatedProducts.get(ast.part_number__c);
      tmpList.add(ast);
       
    }
     else
     { 
        tmpList =new list<asset2__c>();
        tmpList.add(ast);
     }
     RelatedProducts.put(ast.part_number__c,tmpList);  
    }
  }
  
  
 if(!SelectAssets.isempty())
 {
   Products = [select id,Japan_support_product__c,Support_Plus_Product__c,name from product2 p where p.name in :SelectAssets];
 }
 
 if(!Products.isempty())
     {
         for(product2 pr:Products)
         {
             if(pr.Japan_support_product__c == true)
             {
                for(asset2__c ast1: RelatedProducts.get(pr.name))
                { ast1.Japan_SKU__c = true;}
             }
             
             if(pr.Support_Plus_Product__c !=  null)
             {
                for(asset2__c ast1: RelatedProducts.get(pr.name))
                { ast1.Support_Plus_Product__c  = pr.Support_Plus_Product__c;}
             }
             
         }  
     }
  

}