//Copyright (c) 2013, Rima Rohana , BLAT-LAPIDOT
//All rights reserved.
//

// Overview / Description
//
// This is a trigger on Case object after insert and update events 

//

// History
//
// Version                           Date           Author          Ticket in service-now     Comments
// copy from production : 0.0.1      25/7/13        Rima Rohana     INC0017496                If case assigned to Network team group member, 
//                                                                                            Eyal P will be added as follow up as well. 
//8.1.14 -> deleted everything related to eyal following his request
//  0.0.2

trigger Case_after_insert_Account_team on Case (after insert, after update) 
{
    String GroupName ='AE Eyal P group';
    List<Account> RelAccounts = new List<Account>();
    List<string> accounts     = new List<string>();
    List<string> cases        = new List<string>();
    Set<string> members       = new Set<string>();
    Map<id,id> Case_account   = new map<id,id> ();
    Map<string,Set<string>> updUsr          = new map<string,Set<string>>();
    List<CaseTeamMember> AllTeamMembers     = new List<CaseTeamMember>();
    Map<id,Set<string>> Case_TeamMembers    = new map<id,Set<string>> ();
    Map<id,Set<string>> Account_TeamMembers = new map<id,Set<string>> ();
    List<CaseTeamMember> insCaseTeamMember = new List<CaseTeamMember>() ;
    
    //set<Id> set_CaseOwnerIds = new set<Id>();
    List <Case> lst_Cases  = new List<Case>();

    for (Case cs:trigger.new)
    { 
        if((trigger.isinsert && cs.accountId != Null)||(trigger.isupdate && cs.accountId != trigger.oldmap.get(cs.id).accountId && cs.accountId != Null))
        {
            accounts.add(cs.accountId );
            cases.add(cs.id);
            Case_account.put(cs.id,cs.AccountId);
        }
        if( (trigger.IsInsert || (trigger.IsUpdate && (cs.OwnerId != trigger.oldmap.get(cs.id).OwnerId) ) ) )
        {
            lst_Cases.add(cs);

        }
      
    } 
        
    if(!Accounts.isempty())
    {  
    
        RelAccounts =  [select id, Case_team_user1__c, Case_team_user2__c, Case_team_user3__c,Case_team_user4__c,Case_team_user5__c,Case_team_user6__c,         
                        Case_team_user1__r.isactive, Case_team_user2__r.isactive,Case_team_user3__r.isactive,Case_team_user5__r.isactive,Case_team_user4__r.isactive,Case_team_user6__r.isactive  
                        from Account where id in :accounts];
            
        for(Account acc:RelAccounts)
        {
            members.clear();
            if(acc.Case_team_user1__c!=Null && acc.Case_team_user1__r.isactive) {members.add(acc.Case_team_user1__c);}
            if(acc.Case_team_user2__c!=Null && acc.Case_team_user2__r.isactive) {members.add(acc.Case_team_user2__c);}
            if(acc.Case_team_user3__c!=Null && acc.Case_team_user3__r.isactive) {members.add(acc.Case_team_user3__c);}
            if(acc.Case_team_user4__c!=Null && acc.Case_team_user4__r.isactive) {members.add(acc.Case_team_user4__c);}
            if(acc.Case_team_user5__c!=Null && acc.Case_team_user5__r.isactive) {members.add(acc.Case_team_user5__c);}
            if(acc.Case_team_user6__c!=Null && acc.Case_team_user6__r.isactive) {members.add(acc.Case_team_user6__c);}
            
            if(!members.isempty()){Account_TeamMembers.put(acc.id,members);}
        }
        
        AllTeamMembers = [Select MemberId, TeamRoleId,ParentId From CaseTeamMember Where ParentId in :Cases];
        system.debug('AllTeamMembers - '+ AllTeamMembers);
        
        for(CaseTeamMember TM:AllTeamMembers)
        { 
            string Account = Case_account.get(TM.ParentId);
            if(Account_TeamMembers.containsKey(Account))   // Account has Owners
            {
                if(!Case_TeamMembers.containskey(TM.ParentId))
                { 
                    Members.clear();
                    Members.addAll(Account_TeamMembers.get(Account));
                    if(Account_TeamMembers.get(Account).Contains(TM.MemberId))
                    {
                         Members.remove(TM.MemberId);
                    }
                    Case_TeamMembers.put(TM.ParentId,Members);
                }
                else
                {
                    if(Account_TeamMembers.get(Account).Contains(TM.MemberId))
                    { 
                        Members.remove(TM.MemberId);
                        Case_TeamMembers.put(TM.ParentId,Members);
                    }
                }
            }       
        }
        system.debug('Case_TeamMembers -'+ Case_TeamMembers);
        
        for(String UpdCs: Cases)
        {
            if(Case_TeamMembers.containsKey(UpdCs))
            {
                for(String TM:Case_TeamMembers.get(UpdCs))
                {
                    CaseTeamMember ctmNew = new CaseTeamMember();
                    ctmNew.ParentId = UpdCs;
                    ctmNew.MemberId = TM;
                    ctmNew.TeamRoleId = ENV.teamRoleMap.get('Account Owner');
                    insCaseTeamMember.add(ctmNew);
                }
            }
            else
            {
                if(Account_TeamMembers.containsKey(Case_account.get(UpdCs)))
                { 
                    for(String TM1:Account_TeamMembers.get(Case_account.get(UpdCs)))
                    {
                        CaseTeamMember ctmNew = new CaseTeamMember();
                        ctmNew.ParentId = UpdCs;
                        ctmNew.MemberId = TM1;
                        ctmNew.TeamRoleId = ENV.teamRoleMap.get('Account Owner');
                        insCaseTeamMember.add(ctmNew);
                    }
                } 
            }
        }
        
        if(insCaseTeamMember.size()>0)
        {
            insert insCaseTeamMember;
        }
    }
}