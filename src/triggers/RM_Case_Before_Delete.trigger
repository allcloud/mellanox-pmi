trigger RM_Case_Before_Delete on RmCase__c (before delete) {
	
	if (trigger.isBefore && trigger.isDelete) {
		
		cls_trg_RmCase handler = new cls_trg_RmCase();
		handler.updateParentRMCaseHistoryUponDelteOfRelatedRmCase(trigger.old);
	}

}