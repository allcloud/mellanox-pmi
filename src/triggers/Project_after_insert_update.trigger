trigger Project_after_insert_update on Milestone1_Project__c (after insert, after update) {

   map<Milestone1_Project__c,string> projectTypes = new map<Milestone1_Project__c,string>(); 
   list<Milestone1_Project__c> NewProjects = new list<Milestone1_Project__c>();
   list<Project_Member__c> ProjectMemInsert = new list<Project_Member__c>();  
   map<string,list<Milestone_Map__c>> PrTypeToMilestone = new map<string,list<Milestone_Map__c>>(); 
   map<string,set<String>> PrTypeToMilestoneName = new map<string,set<String>>(); 
    map<string,list<Milestone_Map__c>> PrTypeToSortMilestone = new map<string,list<Milestone_Map__c>>(); 
   map<string,string> MsTypeToMsName = new map<string,string>();  
   map<string,list<Milestone_Map__c>> MilestoneToTask = new map<string,list<Milestone_Map__c>>(); 
   map<string,string> PrIdtoPRName = new map<string,string>();  
   List<Milestone1_Milestone__c> MilestoneToInsert = new List<Milestone1_Milestone__c>(); 
  
   List<Task> TasksToInsert = new List<Task>();
   list<Milestone_Map__c> MilestoneList = new list<Milestone_Map__c>();
   list<Milestone_Map__c> TaskList = new list<Milestone_Map__c>();
   list<Milestone_Map__c> sortList = new list<Milestone_Map__c>();
   list<Milestone_Map__c> tmpList = new list<Milestone_Map__c>();
   set<string> tmpNameList = new set<string>();
   list<Milestone_Map__c> tmpList1 = new list<Milestone_Map__c>();
   date NewStartDate = system.today();
    
    
   if(trigger.isinsert)
   {
      
       
      for(Milestone1_Project__c PR:trigger.new)
      {
        NewProjects.add(PR);
        
        if((!PR.Design_In_Indicator__c && PR.blank_project__c == false) || (PR.Design_In_Indicator__c && PR.BLANK_Design_In_Project__c== false))          
        {projectTypes.put(PR,PR.RecordTypeId);
         PrIdtoPRName.put(PR.id,Pr.Name);
        } 
          
      }
      
      cls_trg_Project_Milestone handler = new cls_trg_Project_Milestone();
      handler.updateClonedProjectsWithRelatedChildrenRecords(trigger.new);  
   }
  
/*  if(!NewProjects.isempty())
  {
  for(Milestone1_Project__c prn:NewProjects)
  {
   if(prn.FAE_owner__c != NULL)
   {
    Project_Member__c PM = new Project_Member__c();
    PM.Project_Member__c = prn.FAE_owner__c;
    PM.role__c = 'FAE Owner';
    ProjectMemInsert.add(PM);
    }
   if(prn.PM_owner__c != NULL)
   {
    Project_Member__c PM = new Project_Member__c();
    PM.Project_Member__c = prn.PM_owner__c;
    PM.role__c = 'PM Owner';
    ProjectMemInsert.add(PM);
   }
   if(prn.c_Sales_owner__c != NULL)
   {
    Project_Member__c PM = new Project_Member__c();
    PM.Project_Member__c = prn.c_Sales_owner__c;
    PM.role__c = 'Sales Owner';
    ProjectMemInsert.add(PM);
   }
  }// for
 insert ProjectMemInsert;
 }  
   */
   
 if( !projectTypes.isempty() )
 {
 MilestoneList = [select milestone__c,milestone_type_id__c,Project_Type__c,Project_Type_Id__c,Task__c,Task_Order__c,Milestone_Order__c, IsPublic__c from Milestone_Map__c where Project_Type_id__c in :projectTypes.values() ];
 }

 // MilestoneList = SortMilestoneMap( MilestoneList1); 
    
  for( Milestone_Map__c mmp: MilestoneList)
    {
 //      MsTypeToMsName.put(mmp.milestone_type_id__c,mmp.milestone__c); 
        
      if( !PrTypeToMilestone.containsKey(mmp.Project_Type_Id__c))
      {
         tmpNameList = new set<string>();
         tmpList = new list<Milestone_Map__c>();
         tmpList.add(mmp);
         tmpNameList.add(mmp.milestone__c);
                
      }
       else
       { tmpList = PrTypeToMilestone.get(mmp.Project_Type_Id__c);
         tmpNameList = PrTypeToMilestoneName.get(mmp.Project_Type_Id__c);
         if(!tmpNameList.contains(mmp.milestone__c))
         { tmpList.add(mmp);
           tmpNameList.add(mmp.milestone__c);
         }
                
       }
        
      PrTypeToMilestone.put(mmp.Project_Type_Id__c,tmpList);
      PrTypeToMilestoneName.put(mmp.Project_Type_Id__c,tmpNameList);
        
      
        
    // -----------------  Fill in Milestone to Tasks map    
        
     if( !MilestoneToTask .containsKey(mmp.milestone__c))
      {
         tmpList1 = new list<Milestone_Map__c>();
         tmpList1.add(mmp);
      }
       else
       { tmpList1 = MilestoneToTask.get(mmp.milestone__c);
         tmpList1.add(mmp);
       }
        
      MilestoneToTask.put(mmp.milestone__c,tmpList1);     
        
    
        
      
        
    }   
      
  for(Milestone1_Project__c PR:projectTypes.keySet())     
  {
    if( PrTypeToMilestone.containsKey(PR.recordTypeId)) // there is a list of predefined Milestones for this Project
    { 
      integer i =0; 
     // MilestoneList = SortMilestoneMap( PrTypeToSortMilestone.get(projectTypes.get(PR))); 
    //  sortList.addAll(PrTypeToMilestone.get(projectTypes.get(PR))); // converting set to list
      MilestoneList = SortMilestoneMap(PrTypeToMilestone.get(projectTypes.get(PR)),'Milestone');
      for(Milestone_Map__c MSmap:MilestoneList) //Create Milestones
      {
         Milestone1_Milestone__c MS = new Milestone1_Milestone__c();
         MS.Project__c = PR.id;
         MS.name = MSmap.Milestone__c;
         MS.recordtypeId = MSmap.Milestone_type_Id__c;
         MS.Kickoff__c = date.valueof(PR.createdDate)+i;
         MS.Deadline__c = date.valueof(PR.createdDate)+i+6;
         MS.IsPublic__c =  MSmap.IsPublic__c;
         MilestoneToInsert.add(MS); 
         i=i+7;
      }   
    } 
  }
  insert(MilestoneToInsert);  
    
   for(Milestone1_Milestone__c mm: MilestoneToInsert)
   { 
  
      mm.Kickoff__c = NewStartDate;
     if( MilestoneToTask.containsKey(mm.name))
     { 
         
      TaskList = SortMilestoneMap(MilestoneToTask.get(mm.name),'Task');   
      integer j=0;   
      for( Milestone_Map__c MSmap1:TaskList)
      { 
       task tsk = new task();  
       tsk.WhatId =  mm.id;
       tsk.subject = MSmap1.task__c; 
       tsk.Start_Date__c =mm.Kickoff__c+j;
       tsk.ActivityDate = mm.Kickoff__c+j+1;
       tsk.projectId__c = string.valueOf(mm.project__c).left(15);
       tsk.projectName__c =  PrIdtoPRName.get(mm.project__c);
       TasksToInsert.add(tsk);
     
       mm.Deadline__c = tsk.ActivityDate;
       NewStartDate = tsk.ActivityDate; 
        j=j+2;
       
       
      }
     } 
   }   
       
   
   insert(TasksToInsert); 
   update(MilestoneToInsert);   
   
 
 
 
 public static List< Milestone_Map__c> SortMilestoneMap(List< Milestone_Map__c> ListToSort, string sortType)
    {
        if(ListToSort == null || ListToSort.size() <= 1)
            return ListToSort;
            
        List< Milestone_Map__c> Less = new List< Milestone_Map__c>();
        List< Milestone_Map__c> Greater = new List< Milestone_Map__c>();
        integer pivot = 0;
        
        // save the pivot and remove it from the list
        Milestone_Map__c pivotValue = ListToSort[pivot];
        ListToSort.remove(pivot);
        
            
        for(Milestone_Map__c x : ListToSort)
        {
            
            
          if(sortType == 'Milestone')
          {   
            //system.debug('x.getLabel() : ' + x.getLabel() + ' --- pivotValue.getLabel(): ' +pivotValue.getLabel());
            if(x.Milestone_Order__c <= pivotValue.Milestone_Order__c)
            {
              Less.add(x);
            }
            else 
            {
                if(x.Milestone_Order__c > pivotValue.Milestone_Order__c) 
                {
                     system.debug('****2');
                    Greater.add(x);
                }
            }
          }   
            
            
          if(sortType == 'Task')
          {   
            //system.debug('x.getLabel() : ' + x.getLabel() + ' --- pivotValue.getLabel(): ' +pivotValue.getLabel());
            if(x.Task_Order__c <= pivotValue.Task_Order__c)
            {
              Less.add(x);
            }
            else 
            {
                if(x.Task_Order__c > pivotValue.Task_Order__c) 
                {
                     system.debug('****2');
                    Greater.add(x);
                }
            }
          }     
            
            
        }
        List<Milestone_Map__c> returnList = new List<Milestone_Map__c> ();
        returnList.addAll(SortMilestoneMap(Less,sortType));
        returnList.add(pivotValue);
        returnList.addAll(SortMilestoneMap(Greater,sortType));
        system.debug('returnList : ' + returnList);
        return returnList; 
    }  
    
    if (trigger.isAfter) {
        
        cls_trg_Project_Milestone handler = new cls_trg_Project_Milestone();
        
        if (trigger.isInsert) {
            
            handler.ProjectMembersProjectUpdatedFlagRelated2Projects(trigger.New, null);
            handler.create2MembersWhenDiagnosticPackProjectIsCreated(trigger.New);
        }
        
        else if (trigger.isUpdate) {
            
            handler.ProjectMembersProjectUpdatedFlagRelated2Projects(trigger.New, trigger.oldMap);
        }
    }
 
 
   
   
    
    
}