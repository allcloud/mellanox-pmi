trigger Quote_afterInsertUpdate_MLNXCARE on Quote (after update) {
	ID quoteID;
	Quote quote_to_update;
	// Trigger 2 - ADD MLNX CARE
	// Need to change ID to production when deployed
	
	Set<ID> quotes_IDs = new Set<ID>();
    List<Quote> quotes = new List<Quote>();
    //Add MLNX CARE: GPS-MCARE-1Y , GPS-MCARE-1K-1Y
    
	ID MCARE_1K_ID = '01t50000002rvP5';	//prod ID
	//ID MCARE_1K_ID = '01t50000002rvP5';	//Sandbox ID
	
	ID MCARE_180_Nodes = '01t50000002rvP4';	//prod ID
	//ID MCARE_180_Nodes = '01t50000002rvP4';	//Sandbox ID
		    
    Set<ID> set_ProductIDs = new Set<ID>();
    set_ProductIDs.add(MCARE_180_Nodes);
    set_ProductIDs.add(MCARE_1K_ID);
    //
    Set<Id> set_PriceBookID = new Set<Id>();
    Set<Id> set_PriceBookID_GPS = new Set<Id>();
		
    for (Quote q : Trigger.New){
		//KN - 6-19-15
		//Address Lior O question, removing MLNX CARE then add another UFM make it >=180 Qty then it should re-add MLNX CARE
		
		if (q.UFM_MLNXCARE_Sum_Qty__c != null && Trigger.oldmap.get(q.ID).UFM_MLNXCARE_Sum_Qty__c != q.UFM_MLNXCARE_Sum_Qty__c
					&& q.UFM_MLNXCARE_Sum_Qty__c >= 180 ) {
			quotes_IDs.add(q.id);
			quotes.add(q);
			set_PriceBookID_GPS.add(q.Pricebook2ID);
			set_PriceBookID.add(q.Pricebook2ID);
			set_PriceBookID.add(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c));
			quoteID = q.ID;	
		}
		
	}
    if (quotes.size()==0)
    	return;	
    
    Map<ID,PricebookEntry> map_PricebookEntry = new Map<ID,PricebookEntry>([select id,Pricebook2ID,Product2ID,UnitPrice from PricebookEntry 
                                                                               where Product2Id in :set_ProductIDs 
                                                                               and Pricebook2ID in :set_PriceBookID and isActive=true]);
    Map<ID,Map<ID,PricebookEntry>> map_prodID_PricebookID_PBEntryID = new Map<ID,Map<ID,PricebookEntry>>();
    Map<ID,PricebookEntry> map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
    for (PricebookEntry pbentry : map_PricebookEntry.values()){
    	if(map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID)==null){
	        map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
	        map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
	        map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
		}
    	else {
    		map_PricebookID_PBEntryID = map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID);
        	map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
        	map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
    	}
	}   
	// this map is used to check for duplicated GPS already existed
	List<QuoteLineItem> lst_MCARE_QuoteLineItem = new List<QuoteLineItem>([select id,PricebookentryID,Pricebookentry.Pricebook2Id,QuoteID from QuoteLineItem 
																				where QuoteID in :quotes_IDs
																				and (Product__c = 'GPS-MCARE-1Y' OR Product__c = 'GPS-MCARE-1K-1Y')
																				]);
																				
																				//and Pricebookentry.Pricebook2Id in :set_PriceBookID_GPS]);
    List<QuoteLineItem> lst_QuoteLineItemToInsert = new List<QuoteLineItem>();
	List<QuoteLineItem> lst_QuoteLineItemToDelete = new List<QuoteLineItem>();
    QuoteLineItem qline;
    for (Quote qq : quotes){
	
    	if(qq.UFM_MLNXCARE_Sum_Qty__c != null && qq.UFM_MLNXCARE_Sum_Qty__c >=180 && qq.UFM_MLNXCARE_Sum_Qty__c <1000 
    			//&& (qq.MLNX_CARE_180_250_Nodes__c == true || qq.MLNX_CARE_250_1K_Nodes__c == true) 
    			) {
			if(map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes)!=null 
								&& map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes).get(qq.Pricebook2Id) != null )
			{    		
	    		qline = new QuoteLineItem (QuoteID=qq.ID,
	    							   PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes).get(qq.Pricebook2Id).ID,
	    							   //Quantity=Qty_EDR_KICKSTART,
			                           UnitPrice=map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes).get(qq.Pricebook2Id).UnitPrice,
			                           New_Price__c=map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes).get(qq.Pricebook2Id).UnitPrice,
			                           supportOPN_by_mapping__c=true);
				//Reset pricing based on Pricing drop-down Quotes
				if(Quote_ENV.PriceBookMap!=null && Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)!=null
					&& map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c))!=null) {
						qline.UnitPrice = map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice;
						qline.New_Price__c=map_prodID_PricebookID_PBEntryID.get(MCARE_180_Nodes).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice;		
				}
				//Determine Qty based on # of Nodes
				if(qq.UFM_MLNXCARE_Sum_Qty__c >= 250)
					qline.Quantity = qq.UFM_MLNXCARE_Sum_Qty__c;
				else if(qq.UFM_MLNXCARE_Sum_Qty__c < 250)
					qline.Quantity = Math.round(3000/qline.UnitPrice);
				//
				lst_QuoteLineItemToInsert.add(qline);
				
			}
    	}  //end if(qq.MLNX_CARE_180_250_Nodes__c == true || qq.MLNX_CARE_250_1K_Nodes__c == true) {
    	else if (qq.UFM_MLNXCARE_Sum_Qty__c != null && qq.UFM_MLNXCARE_Sum_Qty__c >=1000 /*&& qq.MLNX_CARE1K_4K_Nodes__c == true */ ) {
    		if(map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID)!=null 
								&& map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID).get(qq.Pricebook2Id) != null )
			{    		
	    		qline = new QuoteLineItem (QuoteID=qq.ID,
	    							   PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID).get(qq.Pricebook2Id).ID,
	    							   Quantity=qq.UFM_MLNXCARE_Sum_Qty__c,
			                           UnitPrice=map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID).get(qq.Pricebook2Id).UnitPrice,
			                           New_Price__c=map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID).get(qq.Pricebook2Id).UnitPrice,
			                           supportOPN_by_mapping__c=true);
				//Reset pricing based on Pricing drop-down Quotes
				if(Quote_ENV.PriceBookMap!=null && Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)!=null
					&& map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c))!=null) {
						qline.UnitPrice = map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice;
						qline.New_Price__c=map_prodID_PricebookID_PBEntryID.get(MCARE_1K_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice;		
				}
				lst_QuoteLineItemToInsert.add(qline);
				
			}
    	}	//end else if (qq.MLNX_CARE1K_4K_Nodes__c == true) {
    	 	                       
    }
    //Insert GPS first, then delete, order is important 
    if(lst_QuoteLineItemToInsert.size()>0){
    	insert lst_QuoteLineItemToInsert;
		if(quoteID != null) {
	    	quote_to_update = [select GPS_Got_Deleted__c from Quote where id = :quoteID];
	    	quote_to_update.GPS_Got_Deleted__c = '';
	    	update quote_to_update;
		}
    }
    
	if(lst_MCARE_QuoteLineItem.size()>0)
        delete lst_MCARE_QuoteLineItem;
	
	//end 2nd trigger
}