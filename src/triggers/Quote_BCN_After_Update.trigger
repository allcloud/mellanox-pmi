trigger Quote_BCN_After_Update on Quote (after update) {
	Set<ID> BCN_IDs = new Set<ID>();
	Set<ID> quoteIDs = new Set<ID>();
	Map<ID,Quote> map_quotes = new Map<ID,Quote>();
	Map<ID,Map<String,Double>> map_BCNID_Product_Prices = new Map<ID,Map<String,Double>>();
	List<QuoteLineItem> quotelines_updates = new List<QuoteLineItem>();
	  
	for (Quote q : Trigger.new) {
		if(q.Bid_Control_Number__c != null && q.Bid_Control_Number__c != Trigger.oldmap.get(q.ID).Bid_Control_Number__c){
			map_quotes.put(q.ID,q);
			BCN_IDs.add(q.Bid_Control_Number__c);
		}
		if (q.Bid_Control_Number__c != Trigger.oldmap.get(q.ID).Bid_Control_Number__c && q.Bid_Control_Number__c == null) {
			quoteIDs.add(q.ID);
		}
	}
	if(map_quotes.size() == 0 && quoteIDs.size() == 0)
		return;
	if (quoteIDs.size() > 0) {
		List<QuoteLineItem> lst_quotelines_2 = new List<QuoteLineItem>([select BCN_Price__c from QuoteLineItem
																		where QuoteID in :quoteIDs]);
		for (QuoteLineItem qli : lst_quotelines_2) {
			qli.BCN_Price__c = null;
			quotelines_updates.add(qli);
		}																			
	}
	//changing BCN and BCN is NOT null
	else if(map_quotes.size() > 0) {
		List<Bid_Control_Lines__c> lst_bcn_lines = new List<Bid_Control_Lines__c>([select Bid_Control__c,Prod_Name__c,Requested_Price__c
																					from Bid_Control_Lines__c where Bid_Control__c in :BCN_IDs]);
		Map<String,Double> tmp_map_prod_price = new Map<String,Double>();
		for ( Bid_Control_Lines__c bb : lst_bcn_lines) {
			if(map_BCNID_Product_Prices.get(bb.Bid_Control__c) == null){
				tmp_map_prod_price = new Map<String,Double>();
				tmp_map_prod_price.put(bb.Prod_Name__c,bb.Requested_Price__c);
				map_BCNID_Product_Prices.put(bb.Bid_Control__c,tmp_map_prod_price);
			}
			else {
				tmp_map_prod_price = map_BCNID_Product_Prices.get(bb.Bid_Control__c);
				tmp_map_prod_price.put(bb.Prod_Name__c,bb.Requested_Price__c);
				map_BCNID_Product_Prices.put(bb.Bid_Control__c,tmp_map_prod_price);
			}
		}//end map building
		
		Map<ID,List<QuoteLineItem>> map_quoteID_quotelines = new Map<ID,List<QuoteLineItem>>();
		List<QuoteLineItem> tmp_quotelines = new List<QuoteLineItem>();  
		List<QuoteLineItem> lst_quotelines = new List<QuoteLineItem>([select Product__c,BCN_Price__c,QuoteID from QuoteLineItem
																		where QuoteID in :map_quotes.keyset()]);
		for (QuoteLineItem qline : lst_quotelines) {
			if(map_quoteID_quotelines.get(qline.QuoteID) == null) {
				tmp_quotelines = new List<QuoteLineItem>();
				tmp_quotelines.add(qline);
				map_quoteID_quotelines.put(qline.QuoteID,tmp_quotelines);
			}
			else {
				tmp_quotelines = map_quoteID_quotelines.get(qline.QuoteID);
				tmp_quotelines.add(qline);
				map_quoteID_quotelines.put(qline.QuoteID,tmp_quotelines);
			}
		} //end second map quote to qlines
		
		for (Quote qq : map_quotes.values()) {
			if(map_quoteID_quotelines != null && map_quoteID_quotelines.get(qq.ID) != null){
				for (QuoteLineItem qlines2 : map_quoteID_quotelines.get(qq.ID)){
					if(map_BCNID_Product_Prices != null && map_BCNID_Product_Prices.get(qq.Bid_Control_Number__c) != null
						&& map_BCNID_Product_Prices.get(qq.Bid_Control_Number__c).get(qlines2.Product__c) != null ) {
							qlines2.BCN_Price__c = map_BCNID_Product_Prices.get(qq.Bid_Control_Number__c).get(qlines2.Product__c);
							quotelines_updates.add(qlines2); 		
					}
				}
			}	
		}
	}
		
	if(quotelines_updates.size() >0)
		update quotelines_updates;																				
	
}