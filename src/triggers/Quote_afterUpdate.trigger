trigger Quote_afterUpdate on Quote (after update) 
{

    //trigger the after update/insert/delete trigger for quotelineitem. 
  
    /*if (!ENV.cameFromQuote)
    {
       //if (Trigger.new[0].LineItemCount > 0 )
        //if(Trigger.new[0].Big_Cluster_Opportunity__c!=trigger.oldmap.get(Trigger.new[0].Id).Big_Cluster_Opportunity__c)
       // {
            QuoteLineItem relatedQuoteLineItems = [Select q.QuoteId, q.Exclude_cables_checked__c, q.Big_Cluster_from_Quote__c 
                                                         From QuoteLineItem q
                                                         Where q.QuoteId = : Trigger.new[0].Id
                                                         limit 1];
                                                         
            System.debug('DK quote : ' + Trigger.new[0].Id);
            
            if (relatedQuoteLineItems.Big_Cluster_from_Quote__c)
            {
                relatedQuoteLineItems.Big_Cluster_from_Quote__c=false;
            }
            else
            {
                relatedQuoteLineItems.Big_Cluster_from_Quote__c=true;
            }
            
            ENV.cameFromQuote=true;
            
            update relatedQuoteLineItems;
        //}
            
    }*/
    
    /*******************************************
    Trigger 1 
    *******************************************/
    Set<Id> quate_Id = new Set<Id>();
    QuoteLineItem relatedQuoteLineItems;
    
    for (Quote q : Trigger.new)
    {
        if (q.Big_Cluster_Opportunity__c!=trigger.oldmap.get(q.Id).Big_Cluster_Opportunity__c)
        {
            quate_Id.add(q.Id);
        }
          if (q.Exclude_Cables_From_Support__c!=trigger.oldmap.get(q.Id).Exclude_Cables_From_Support__c)
        {
            quate_Id.add(q.Id);
        }
        
        
    }
    
     
    if (quate_Id.size() > 0)
    {
        relatedQuoteLineItems = [Select q.QuoteId, q.Exclude_cables_checked__c, q.Big_Cluster_from_Quote__c 
                                                     From QuoteLineItem q
                                                     Where q.QuoteId IN : quate_Id
                                                     limit 1];
                                                     
        System.debug('DK quote : ' + Trigger.new[0].Id);
        
        if (relatedQuoteLineItems.Big_Cluster_from_Quote__c)
        {
            relatedQuoteLineItems.Big_Cluster_from_Quote__c=false;
        }
        else
        {
            relatedQuoteLineItems.Big_Cluster_from_Quote__c=true;
        }
        
        //ENV.cameFromQuote=true;
        
        update relatedQuoteLineItems;
    }
    /*******************************************
     end of Trigger 1 
    *******************************************/
    
    /*******************************************
    Trigger 2 
    *******************************************/
    Map<Id, Quote> map_quates = new  Map<Id, Quote>();
    Set<Id> set_QuatesID = new Set<Id>();
    Set<Id> set_ProductID = new Set<Id>();
    Set<Id> set_PriceBookID = new Set<Id>();
    List<PricebookEntry> lst_PricebookEntry = new List<PricebookEntry>();
    Map<Id, Map<Id, Decimal>> map_ProductId_MapPricebookEntry = new Map<Id, Map<Id, Decimal>>();
    List<QuoteLineItem> lst_QuoteLineItemToUpdate = new List<QuoteLineItem>();
    
    Set<Id> set_OpportunityId = new Set<Id>();
    List<Quote> lst_QuateToContinue = new List<Quote>();
    List<Opportunity> lst_opportunityToUpdate = new List<Opportunity>();
    //add OEM PB to the list as Secondary PB might be needed
	 //If cannot find in Tier 1 PB, find it in OEM PB
	 // KN 02-05-14
	 ID OEM_PB_ID = '01s500000006J7F';
	 ID DB_PB_ID = '01s500000006J09';
	 set_PriceBookID.add(OEM_PB_ID);
	 set_PriceBookID.add(DB_PB_ID);
	 
	 Set<String> Tier1_customers = new Set<String>();
	 Tier1_customers.add('Hewlett-Packard');
	 Tier1_customers.add('HP Price List');
	 Tier1_customers.add('HP PRICE LIST');
	 Tier1_customers.add('IBM');
	 Tier1_customers.add('IBM Price List');
	 Tier1_customers.add('IBM PRICE LIST');
	 //Tier1_customers.add('Dell Pricebook');  //EXCEPT Dell would not be default to OEM
	 Tier1_customers.add('CRAY');
	 Tier1_customers.add('CRAY Price List');
	 Tier1_customers.add('CRAY PRICE LIST');
	 Tier1_customers.add('EMC');
	 Tier1_customers.add('EMC Price List');
	 Tier1_customers.add('EMC PRICE LIST');
	 Tier1_customers.add('SGI');
	 Tier1_customers.add('SGI Price List');
	 Tier1_customers.add('SGI PRICE LIST');
	 Tier1_customers.add('ORACLE');
	 Tier1_customers.add('TERADATA');
	 Tier1_customers.add('TERADATA Price List');
	 Tier1_customers.add('TERADATA PRICE LIST');
	 Tier1_customers.add('LENOVO');
	 //
	 
    for (Quote q : Trigger.new)
    {
        if (q.New_Price_Book__c != Trigger.oldMap.get(q.Id).New_Price_Book__c)
        {
            set_QuatesID.add(q.Id);
            set_PriceBookID.add(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c));
        }
        if  (q.IsSyncing == true)
        {
            set_OpportunityId.add(q.OpportunityId);
            lst_QuateToContinue.add(q);
        }
    }
    
    
    if (set_QuatesID.size() > 0)
    {
        
        map_quates =  new Map<Id, Quote>([Select Id, (Select Id, PricebookEntry.Pricebook2Id, PricebookEntry.Product2Id, PricebookEntry.UnitPrice, QuoteId, PricebookEntryId, New_Price__c,Product__c From QuoteLineItems) 
                                From Quote q
                                where q.Id IN : set_QuatesID]);
                        
        for (Quote q : map_quates.values())
        {
            for (QuoteLineItem qli : q.QuoteLineItems)
            {
                set_ProductID.add(qli.PricebookEntry.Product2Id);
            }
        }
        system.debug('set_ProductID: '+set_ProductID);
        lst_PricebookEntry = [Select p.UnitPrice, p.Product2Id, p.Pricebook2Id 
                                From PricebookEntry p
                                where p.Product2Id IN : set_ProductID AND p.Pricebook2Id IN : set_PriceBookID];
        
        system.debug('lst_PricebookEntry: '+lst_PricebookEntry);
        for (PricebookEntry p : lst_PricebookEntry)
        {
            if (!map_ProductId_MapPricebookEntry.containsKey(p.Pricebook2Id))
            {
                map_ProductId_MapPricebookEntry.put(p.Pricebook2Id, new Map<Id, Decimal> {p.Product2Id => p.UnitPrice});
            }
            else
            {
                Map<Id, Decimal> m = map_ProductId_MapPricebookEntry.get(p.Pricebook2Id);
                if (!m.containsKey(p.Product2Id))
                {
                    m.put(p.Product2Id, p.UnitPrice);
                }
            }
        }
        system.debug('map_ProductId_MapPricebookEntry: '+map_ProductId_MapPricebookEntry);
        for (Quote q : Trigger.new)
        {
            for (QuoteLineItem qli : map_quates.get(q.Id).QuoteLineItems)
            {
                if (map_ProductId_MapPricebookEntry.containsKey(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c)))
                {
                    if (map_ProductId_MapPricebookEntry.get(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c)).containsKey(qli.PricebookEntry.Product2Id))  
                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c)).get(qli.PricebookEntry.Product2Id);
                    else
                        qli.New_Price__c = 0;
                    //lst_QuoteLineItemToUpdate.add(qli);
                }
                else
                {
                     qli.New_Price__c = 0;
                     //lst_QuoteLineItemToUpdate.add(qli);
                }
                //KN add 02-05-14, if cannot find in Tier1-PB, look into OEM
                //For DELL, look into DB as default PB
	            if ( (qli.New_Price__c==null || qli.New_Price__c == 0) && (q.New_Price_Book__c == 'Dell Pricebook' || q.New_Price_Book__c == 'Dell') ){
	            	if (map_ProductId_MapPricebookEntry.get(DB_PB_ID).containsKey(qli.PricebookEntry.Product2Id))  
                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(DB_PB_ID).get(qli.PricebookEntry.Product2Id);
                    else
                        qli.New_Price__c = 0;
	            }
	            //For Facebook, look into DB as default PB
	            else if ( (qli.New_Price__c==null || qli.New_Price__c == 0) && q.New_Price_Book__c == 'FACEBOOK' ){
	            	if (map_ProductId_MapPricebookEntry.get(DB_PB_ID) != null && map_ProductId_MapPricebookEntry.get(DB_PB_ID).containsKey(qli.PricebookEntry.Product2Id))  
                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(DB_PB_ID).get(qli.PricebookEntry.Product2Id);
                    else
                        qli.New_Price__c = 0;
	            }
	            else if ( (qli.New_Price__c==null || qli.New_Price__c == 0) && Tier1_customers.contains(q.New_Price_Book__c) ){
	            	if (map_ProductId_MapPricebookEntry.get(OEM_PB_ID) != null && map_ProductId_MapPricebookEntry.get(OEM_PB_ID).containsKey(qli.PricebookEntry.Product2Id))  
                        qli.New_Price__c = map_ProductId_MapPricebookEntry.get(OEM_PB_ID).get(qli.PricebookEntry.Product2Id);
                    else
                        qli.New_Price__c = 0;
	            }
	                //
	            // KN added 06-12-14
	            // Handle cases where GPS-00001 is copied over from opportunit when quote creates from opty, and SalesPrice is also copied over and
	            //might affact 2ndLevel_approval
	            /*if (qli.Product__c=='GPS-00001' || qli.Product__c=='GPS-XS-NC-STD' || qli.Product__c=='GPS-S-NC-STD' || qli.Product__c=='GPS-M-NC-STD' 
	            		|| qli.Product__c=='GPS-L-NC-STD' || qli.Product__c=='GPS-XL-NC-STD' || qli.Product__c=='GPS-TPM-01'){
	            	qli.UnitPrice = qli.New_Price__c;
	            }*/
	            // end KN 06-12-14
	            
                lst_QuoteLineItemToUpdate.add(qli);
            }
        }
        system.debug('lst_QuoteLineItemToUpdate: '+lst_QuoteLineItemToUpdate);
        if (lst_QuoteLineItemToUpdate.size() > 0){
			//old code
			//update lst_QuoteLineItemToUpdate;
			
			// KN fixed
			try{
		    	update lst_QuoteLineItemToUpdate;
			} catch (System.DmlException e){
			     for (Quote qq : Trigger.new) {
			          qq.addError('DMLException!!! Unable to update all quote lines when saving this quote. ');
			     }
			     
			}        
	        //            
                 
        }                              
    } 
    /*******************************************
    End of Trigger 2 
    *******************************************/
    /*******************************************
    Trigger 3 
    *******************************************/
    if (set_OpportunityId.size() > 0)
    {
          Map<Id, Opportunity> map_opportunity = new Map<Id, Opportunity>([select Id, Primary_Partner_N__c,Distribution_Partner__c From Opportunity  where Id IN : set_OpportunityId]);
          
          for (Quote q : lst_QuateToContinue)
          {
              Opportunity opp = map_opportunity.get(q.OpportunityId);
              
              opp.Primary_Partner_N__c = (q.Account__c != null) ? q.Account__c : opp.Primary_Partner_N__c;
              opp.Distribution_Partner__c = (q.Distributor_SI__c != null) ? q.Distributor_SI__c : opp.Distribution_Partner__c; 
              lst_opportunityToUpdate.add(opp);
          }
          if (lst_opportunityToUpdate.size() > 0)
                update lst_opportunityToUpdate;
    }

}