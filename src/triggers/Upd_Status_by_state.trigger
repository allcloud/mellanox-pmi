//Copyright (c) 2013, Rima Rohana , BLAT-LAPIDOT
//All rights reserved.
//

// Overview / Description
//
// This is a trigger on Case object befor insert and update events 
// that update the state\status of the case.

//

// History
//
// Version    Date          Author          Ticket in service-now     Comments
// 1.0.1      9/7/13        Rima Rohana     INC0028474                handle the case when status = "Soft Closed - Pending Approval"  
// 1.0.2      09/03/14      Tanya Cohen     RedMine-#379060           When a customer closes a case status should be closed and state should remain "Soft Close - Pending Approval" 
// 1.0.3      18.8.14       Tanya Cohen     TASK0014366               When case is in the state of W4I, and the Ae engineer is changed, it should automatically change the assignee to be = AE engineer

trigger Upd_Status_by_state on Case(before Insert, before Update) {
if(ENV.triggerShouldRun){
    ENV.triggerShouldRun = false;

for(Case  c:trigger.new){
   
  if(trigger.isupdate){                 
   if((c.state__c == 'Assigned' || c.state__c == 'Open' ) && trigger.oldmap.get(c.id).state__c == 'Closed')
      {c.status ='Assigned';}
      
   if(c.state__c == 'Closed' && c.status != 'Closed') 
   {
     c.status = 'Closed';
   }
  
      
   if(c.status== 'Closed' && trigger.oldmap.get(c.id).status != 'Closed' && userinfo.getUserType() == 'Standard'  && userinfo.getProfileId() != '00e50000000pe9K') 
   {
     c.state__c = 'Closed';
   }
     
   
      
  if(c.state__c== 'Open')
  {        
     c.status='Open';
                       
    if(trigger.isupdate)        
      {                                
        if(trigger.oldmap.get(c.id).state__c == 'Assigned')            
         { 
         
           c.assignee__c = Null;            
           c.ownerid = env.SystemQueue;
           system.debug('Upd_Status_by_state Case ownerid 53 :' + c.ownerid);
           }          
      }       
    }
    
    
  if(c.state__c== 'Assigned'){
     system.debug('===> c.status'+c.status); 
     c.status='Assigned';          
     }
     
  if(c.state__c== 'RMA Assigned'){
     c.status='RMA Assigned';
     }  
     
     
            
  if( c.state__c== 'On Hold' || c.state__c== 'Wait for Implementation' ){
     c.status='Assigned';
     }
     
 
                                                                                    
  if(c.state__c== 'Waiting for Customer info'){
     c.status='Waiting for Customer Info';
     }
  if(c.state__c== 'Waiting for Customer Approval'){
     c.status='Waiting for Customer Approval';
     }  
   if(c.state__c== 'RMA Approved'){
     c.status='RMA Approved';
     }     
       
 //***************** Changing State to Assigned AE **************************      
       
  if(trigger.isupdate && c.state__c== 'Assigned AE' && trigger.oldmap.get(c.id).state__c != 'Assigned AE')
  {
     c.status='Assigned';
                  
     if(c.AE_engineer__c == NULL && trigger.oldmap.get(c.id).state__c == 'Open AE' && ENV.IsInQueue(c.Assignee__c, 'AE Permissions')== TRUE)            
      {
        c.AE_engineer__c = c.Assignee__c;         
        if (c.AE_PM_Escalated__c == 'AE') {c.AE_manager_email__c= ENV.AEManagerEmail;}                                
       //??  c.Escalated_AE_Time__c = system.now();              
       // c.Escalated_AE_day_num__c = ((system.now().date()).toStartOfWeek()).daysBetween (system.now().date());                    
      }     
        //Someone trying to move to 'Assigned AE' before  'Open AE'          
     if( trigger.oldmap.get(c.id).state__c != 'Open AE' && c.AE_engineer__c==NULL && trigger.oldmap.get(c.id).AE_engineer__c == NULL)            
       { c.state__c.adderror('To escalete the case to AE, please move the case to "Open AE" state');} 
       
       // Moving to 'Assigned AE' not for first time     
     if( trigger.oldmap.get(c.id).state__c != 'Open AE' && c.AE_engineer__c!=NULL)            
       { c.Assignee__c= c.AE_engineer__c;}                                          
   }    
  
    if(trigger.isupdate && c.state__c== 'Assigned AE' && trigger.oldmap.get(c.id).AE_engineer__c != c.AE_engineer__c)
    {
        c.Assignee__c= c.AE_engineer__c;
    }

   
  if(trigger.isupdate && c.state__c== 'Open AE')
  
  {           
  //  *******button*******    if(trigger.oldmap.get(c.id).state__c != 'Open AE') {c.assignee__c = env.AEqueue;} 
  
  
 //***************** Changing Assignee when in 'Open AE' will move state to 'Assigned AE'
             
      if(c.assignee__c != trigger.oldmap.get(c.id).assignee__c && trigger.oldmap.get(c.id).assignee__c == Env.AEqueueCP )
        {
         if(!ENV.IsInQueue(c.Assignee__c, 'AE Permissions'))
         {c.adderror('The selected Assignee does not have required AE permissions');}
         else{
                c.state__c = 'Assigned AE';
                c.ae_engineer__c = c.assignee__c;
             } 
                     
        }     // When case in 'Open AE' user changed the assignee, move to Assigned AE 
        
      if( trigger.oldmap.get(c.id).state__c  == 'Pending SME Approval')  //For new SME approval flow (cant set Assignee to CP user by Workflow)
        { c.assignee__c = env.AEqueueCP;}
           
 
/************button***********************
        if(c.AE_engineer__c != NULL){        
            c.state__c = 'Assigned AE';          
            c.Assignee__c= c.AE_engineer__c;           
           }     */                       
     c.status='Assigned';
     }          
   
            
           
                                                                                                  
  if(c.state__c== 'Wait for Release')
    { c.status='Resolved and Wait for Release';                                                                                                                                                                                                                    
     }
  
  if(trigger.isupdate && c.state__c== 'Wait for Implementation' && trigger.oldmap.get(c.id).AE_Engineer__c != c.AE_Engineer__c && c.AE_Engineer__c!=NULL && c.AE_PM_Escalated__c!= NULL)
  {  c.Assignee__c= c.AE_engineer__c;  }    
//System.debug('... KN END trigger Upd_Status_by_state c.State__c===' + c.State__c);           
   }
   
 }
}
  }