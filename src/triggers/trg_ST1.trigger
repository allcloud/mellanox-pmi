trigger trg_ST1 on ST1_Shift__c (before insert) {
	
	ST1_Handler handler = new ST1_Handler();
 	
 	if (trigger.isbefore && trigger.isInsert) {
     
        handler.updateST1ParamsOnInsert(trigger.New);
      	handler.updateLastST1ParamsOnInsert(trigger.New);
    }   
}