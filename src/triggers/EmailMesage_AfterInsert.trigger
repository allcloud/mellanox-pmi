// need to remove the function "toLowerCase"!!


trigger EmailMesage_AfterInsert on EmailMessage (after insert) 
{
    
    private String createdBy;
    List<ID> caseId = new List<ID>();
    set<String> userEmail = new set<String>();
    
    // splitting the message
    for(EmailMessage e :Trigger.new)
    {
        system.debug('===>e '+ e);
        system.debug('===>e '+ e.ActivityId);
        system.debug('===>e '+ e.Id);
        system.debug('===>e '+ e.CreatedById);
        system.debug('===>e '+ e.ParentId);
        system.debug('===>e '+ e.HasAttachment);
        if(e.Status!='3')
        {   
            caseId.Add(e.ParentId);
            // creates a list object named email and split it 
            List<String> email = e.FromAddress.toLowerCase().split('@');
            // if the email object isn't empty 
            if(email != null && email[1]!=null)
            {
                List<String> email1 = email[1].split('\\.');
                email1 = email[1].split('\\.');

                if(email1!=null && email1[0]!=null)
                {
                    if(email1[0].toLowerCase() == 'mellanox')
                    {
                        if(!userEmail.contains(e.FromAddress.toLowerCase()))
                        {
                            userEmail.Add(e.FromAddress.toLowerCase());
                        }
                    }
                }
            }
        }
    }
    // the map containes cases which have the email ID
    Map<ID,Case> mapCase = new Map<ID,Case>([SELECT c.Id, c.Contact.Email,c.Contact.Name, c.Contact.Id, c.ContactId,c.origin, c.EmailCount__c, c.First_time_response__c FROM Case c WHERE c.Id in :caseId and c.IsDeleted=false]);
    List<User> lstUser = [SELECT Id,Email,Name,ProfileId FROM User WHERE Email in :userEmail and IsActive=true];
    Map<String,User> mapUser = new Map<String,User>();
    
    for(User u :lstUser)
    {
        mapUser.put(u.Email,u);
    }
    List<Case> updCase = new List<Case>();
    List<CaseComment> insCaseComment = new List<CaseComment>(); 
    
    for(EmailMessage e :Trigger.new)
    { 
       // gets the case from the email
       Case tmpCase = mapCase.get(e.ParentId);

        if(e.Status!='3' && e.ParentId!=null)
        { 

            String comm='';
           
            if(e.FromAddress == tmpCase.Contact.Email && !userEmail.contains(e.FromAddress.toLowerCase() ) )
            {   CaseComment c = new CaseComment();
                c.ParentId = tmpCase.Id;
                c.IsPublished = true;
                comm = 'Comment By Contact: '+ tmpCase.Contact.Name+'\n '+getTheLastMessage(e.TextBody, e.HtmlBody);
                createdBy = tmpCase.Contact.Name;
                if(comm.length()>3200)
                {
                    c.CommentBody = 'Comment By Contact: '+tmpCase.Contact.Name+'\n '+'Email was received, firs lines are added as comment. For full text go to email section.  \n' + comm.substring(0, 3000);
                }               
                else
                {
                   
                    c.CommentBody = comm;
                }
                insCaseComment.Add(c);
            } // if contact emal
                        
            else
            { 
             // if the email is not spam
             if(!ENV.IsSpamEmail(e.FromAddress)) 
              {
                    // define a new casecomment named c                
                    CaseComment c = new CaseComment();
                    // the new CC parentID will be the email's case ID
                    c.ParentId = tmpCase.Id;
                   // c.IsPublished = true;
                    Boolean IsSFDCEmail=false;
                    
                    //Beginning of process - if it is a mellanox email check who is the User
                    List<String> email = e.FromAddress.split('@');
                    if(email != null && email[1]!=null)
                    {
                        List<String> email1 = email[1].split('\\.');
                        if(email1!=null && email1[0]!=null)
                        {
                            if(email1[0].toLowerCase() == 'salesforce')
                            {
                                IsSFDCEmail=true;
                            }
                        }
                    }
                    if(!IsSFDCEmail)
                    { 
                        if(userEmail.contains(e.FromAddress.toLowerCase()))
                        { 
                        
                          String subject = e.subject;
                        
                          if(subject!=null && subject.trim().toLowerCase().startsWith('external'))
                           {
                              c.IsPublished = true;
                            } else
                             c.IsPublished = false;  
                              
                            
                          if( tmpCase.First_time_response__c == NULL)
                           {tmpCase.First_time_response__c = system.now();}
                                       
                          String userName = e.FromAddress;         
                          if( mapUser.containsKey(e.FromAddress.toLowerCase()) )         
                           {              
                            User u = mapUser.get(e.FromAddress.toLowerCase());
                            userName = u.Name;          
                           } 
                           
                            if( mapUser.containsKey(e.FromAddress.toLowerCase()) && mapUser.get(e.FromAddress.toLowerCase()).profileId == ENV.ProfileAE)
                            {
                            comm = 'Comment By Mellanox User AE: '+userName+'\n '+getTheLastMessage(e.TextBody, e.HtmlBody);
                            
                            }
                            else                              
                           { 
                            comm = 'Comment By Mellanox User: '+userName+'\n '+getTheLastMessage(e.TextBody , e.HtmlBody);
                           }
                           
                           createdBy = userName;
                         
                            if(comm.length()>3200)
                            {
                               if( mapUser.containsKey(e.FromAddress.toLowerCase()) && mapUser.get(e.FromAddress.toLowerCase()).profileId == ENV.ProfileAE)
                               {  comm = 'Comment By Mellanox User AE: '+userName+'\n '+ 'Email was received, firs lines are added as comment. For full text go to email section.  \n' + comm.substring(0, 3000);}
                                        
                               else{  comm = 'Comment By Mellanox User: '+userName+'\n '+ 'Email was received, firs lines are added as comment. For full text go to email section.  \n' + comm.substring(0, 3000);}
                            }
                            
                        } // End of process -  if Mellanox email
                        // IF it's not a mellanox user we define the content of the comment
                        // by retrieving it from the email 
                        else 
                        {
                            // using the function "getTheLastMessage" defined at the end to retrieve 
                            // the last message from the emails chained.
                           if (comm.length()>3200) {
                            
                                //comm = getTheLastMessage(e.TextBody , e.HtmlBody);
                                comm = 'Comment By Customer: '+e.FromAddress+'\n '+ 'Received via email, first lines are added as comment. For full text go to email section.  \n' + getTheLastMessage(e.TextBody , e.HtmlBody).substring(0, 3000);
                           }
                           
                           else {
                            
                                comm = 'Comment By Customer: '+e.FromAddress+'\n '+getTheLastMessage(e.TextBody , e.HtmlBody);
                           }
                                                      
                             
                            
                            createdBy = e.FromAddress;
                             c.IsPublished = true;
                        }
                        
                        c.CommentBody = comm;
                        insCaseComment.Add(c);
                    }// if(!SFDC)
               }//if not Spam                         
            }//else (not contact email)
        } // if(parentId!=NULL
        if(insCaseComment.size()>0)
        { 
           if(!(tmpCase.origin == 'Email' || tmpCase.origin == 'Japan-Support'|| tmpCase.origin == 'UFM CallHome')|| e.Incoming == False) {
            
        	  try {
	            		
            		upsert insCaseComment;
            	}
            	
            	catch(Exception ex) {
            		
            		
            	}
    	   }
            
            else if((tmpCase.origin == 'Email' || tmpCase.origin == 'Japan-Support'|| tmpCase.origin == 'UFM CallHome') && e.Incoming == True) {
           
	          	if( tmpCase.EmailCount__c !=NULL ) {
	            
	            	try {
	            		
	            		upsert insCaseComment;
	            	}
	            	
	            	catch(Exception ex1) {
	            		
	            		
	            	}
	        	}
            }
        }

       if(e.Incoming == True)
       {
         if(tmpCase.EmailCount__c ==NULL)
         {tmpCase.EmailCount__c = 1;}
        else
         {tmpCase.EmailCount__c = tmpCase.EmailCount__c +1;}
         
         tmpCase.Last_Email_Created_By__c = createdBy;
         update tmpCase; 
       }     
       if(e.Incoming == False && tmpCase !=NULL)
                      
      { update tmpCase; }//if outgoing email
                                    
  } //for
// function used to retrieve the email TextBody    
    public String getTheLastMessage(String email , string HTMLemail)
    {
       // emailparts will be returned if the email body isn't empty
       List<String> emailParts ;
      if(email!= NULL)
      {
      // the func toLowerCase Converts all of the characters in the String to lowercase
        emailParts = email.split('--original message--');
        if(emailParts.size()>1)
        {
            return emailParts[0];
        }
        else
        {
            emailParts = email.split('from:'); 
            if(emailParts.size()>1)
            {
                return emailParts[0];
            }
            else{
            emailParts = email.split('From:'); 
            if(emailParts.size()>1)
            {
                return emailParts[0];
            } 
                        
            else 
            {
            emailParts = email.split('Mellanox Support Admin wrote:'); 
            if(emailParts.size()>1)
             {
                return emailParts[0];
             }
             else 
             {
              emailParts = email.split('wrote:'); 
               if(emailParts.size()>1)
               {
                return emailParts[0];
                }
               else
               { 
                 emailParts = email.split('Customer Support Engineer,'); 
                if(emailParts.size()>1)
                {
                return emailParts[0];
                }
               }// else ' Custome Support..' 
                
             } //else 'wrote'
           }//else 'Admin wrote'                                        
        }//else 'From'
       } //else 'from'       
        return email;
       }
       
       //HTML emailparts will be returned if the email body isn't empty
       List<String> HTMLemailParts ;
       
       if (HTMLemail!= NULL)
       {    
            // the func toLowerCase Converts all of the characters in the String to lowercase
            HTMLemailParts = HTMLemail.split('--original message--');
            if(HTMLemailParts.size()>1)
            {
                return HTMLemailParts[0];
            }
            else
            {
                HTMLemailParts = HTMLemail.split('from:'); 
                if(HTMLemailParts.size()>1)
                {
                    return HTMLemailParts[0];
                }
                else{
                HTMLemailParts = HTMLemail.split('From:'); 
                if(HTMLemailParts.size()>1)
                {
                    return HTMLemailParts[0];
                } 
                            
                else 
                {
                HTMLemailParts = HTMLemail.split('Mellanox Support Admin wrote:'); 
                if(HTMLemailParts.size()>1)
                 {
                    return HTMLemailParts[0];
                 }
                 else 
                 {
                  HTMLemailParts = HTMLemail.split('wrote:'); 
                   if(HTMLemailParts.size()>1)
                   {
                    return HTMLemailParts[0];
                    }
                   else
                   { 
                     HTMLemailParts = HTMLemail.split('Customer Support Engineer,'); 
                    if(HTMLemailParts.size()>1)
                    {
                    return HTMLemailParts[0];
                    }
                   }// else ' Custome Support..' 
                    
                 } //else 'wrote'
               }//else 'Admin wrote'                                        
            }//else 'From'
           } //else 'from'       
            return HTMLemail;
       }
       
             
      return 'Empty message';
    }
    
    //cls_trg_EmailMessage handler = new cls_trg_EmailMessage();
   // handler.executeFutureSortAttachmentsPrivatOrPublic(trigger.New, createdBy);  
}