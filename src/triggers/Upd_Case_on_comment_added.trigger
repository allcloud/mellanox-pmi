trigger Upd_Case_on_comment_added on CaseComment (after insert, after update) 
{
     List<id> CaseParent = new List<id>();
     List<id> LastMB = new List<id>();
     map<ID,Case> map_caseToId = new map<id,Case>();

     for(CaseComment Com:trigger.New)
     { 
        CaseParent.add(Com.ParentId);
        LastMB.add(Com.LastModifiedById);
     }
 
    Map <ID, List<CaseComment>> map_caseIdToCaseComments = new Map <ID, List<CaseComment>>();
    for (Case CP:[Select c.Id,  c.Contact.id, c.contact.email,mail_customer_cc__c, case_comments_field__C ,         
                  Is_new_internal_comment__c, internal_Comment__c, cc_Email_Address_1__c, cc_Email_Address_2__c,        
                  cc_Email_Address_3__c, cc_Email_Address_4__c, cc_Email_Address_5__c, cc_Email_Address_6__c,        
                  Case_source__c,SuppliedEmail,recordTypeId,
                 (Select ParentId, IsPublished, LastModifiedBy.Name, CommentBody, CreatedById, CreatedDate, LastModifiedById From CaseComments Where IsDeleted = false and IsPublished = true Order By LastModifiedDate DESC)   
                  FROM Case c 
                  Where c.id in :CaseParent])
    {
        map_caseToId.put(CP.id,CP);
        for(CaseComment cc: CP.CaseComments)
        {
            if(!map_caseIdToCaseComments.containsKey(CP.Id))
            {
                map_caseIdToCaseComments.put(CP.Id, new List <CaseComment>{cc});
            }
            else
            {
                List <CaseComment> lst_tmpCc = map_caseIdToCaseComments.get(CP.Id);
                lst_tmpCc.add(cc);
                map_caseIdToCaseComments.remove(CP.Id);
                map_caseIdToCaseComments.put(CP.Id, lst_tmpCc);
            }
        }
    }
    List <Case> lst_caseToUpdate = new List <Case>(); 
    for(ID id_case: map_caseIdToCaseComments.keySet())
    {
        map_caseToId.get(id_case).Case_Comments_Field__c = clsCase.updateLatestCaseComment(LastMB,map_caseToId.get(id_case), map_caseIdToCaseComments.get(id_case));
        lst_caseToUpdate.add(map_caseToId.get(id_case));
    }
    if(lst_caseToUpdate.size()>0)
    {
        update lst_caseToUpdate;
    }
     
    
    for(CaseComment Comment:trigger.New)
    {
                               
    /*   if(comment.IsPublished==True)
        {    
            if (userinfo.getUserType()== 'Standard' && userinfo.getUserId()!=ENV.webCustomer )    //Comment added Internally    
            {        
                id PubComment_template = env.Temp_PublicCommentContact_NoAccess;  // Template for no Access contact    
                     
              
                if(map_caseToId.get(comment.ParentId).recordTypeId == ENV.CaseTypeAdmin)    
                { PubComment_template = ENV.Temp_PublicCommentContact_Admin;}        
                       
                if( map_caseToId.get(comment.ParentId).Contact != NULL)        
                 {
                  String[] toAddressesCc = new String[]{map_caseToId.get(comment.ParentId).Contact.Email};
                  Util.SendSingleEmail(toAddressesCc, null, 'supportadmin@mellanox.com', 'Mellanox Support Admin', PubComment_template, map_caseToId.get(comment.ParentId).id, '', '');
                  system.debug('BLAT Util.SendSingleEmail 80');                      
                  }
             }       

             
            // Send email notification on Public Comment to Cc customer       
             List <String> toAddresses = new List<String>();
            if(map_caseToId.get(comment.ParentId).cc_Email_Address_1__c!=null)
            {
                toAddresses.add(map_caseToId.get(comment.ParentId).cc_Email_Address_1__c);
            }
            if(map_caseToId.get(comment.ParentId).cc_Email_Address_2__c!=null)
            {
                toAddresses.add(map_caseToId.get(comment.ParentId).cc_Email_Address_2__c);
            }
            if(map_caseToId.get(comment.ParentId).cc_Email_Address_3__c!=null)
            {
                toAddresses.add(map_caseToId.get(comment.ParentId).cc_Email_Address_3__c);
            }
            if(map_caseToId.get(comment.ParentId).cc_Email_Address_4__c!=null)
            {
                toAddresses.add(map_caseToId.get(comment.ParentId).cc_Email_Address_4__c);
            }
            if(map_caseToId.get(comment.ParentId).cc_Email_Address_5__c!=null)
            {
                toAddresses.add(map_caseToId.get(comment.ParentId).cc_Email_Address_5__c);
            }
            if(map_caseToId.get(comment.ParentId).cc_Email_Address_6__c!=null)
            {
                toAddresses.add(map_caseToId.get(comment.ParentId).cc_Email_Address_6__c);
            }
        Util.SendSingleEmail(toAddresses, null, 'supportadmin@mellanox.com', 'Mellanox Support Admin', env.Temp_PublicCommentCc, map_caseToId.get(comment.ParentId).id, '', '');
                    }*/
        if(comment.IsPublished==False)
        {         
            map_caseToId.get(comment.ParentId).Is_new_internal_comment__c = true;        
            map_caseToId.get(comment.ParentId).Internal_comment__c = Comment.CommentBody;       
        }
     
    update(map_caseToId.get(comment.ParentId));
  }
}