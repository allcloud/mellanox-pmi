trigger Opportunity_Set_PriceList_Forecasts on Opportunity (before insert, after update) {
	if (Trigger.isInsert){
		for (Opportunity ooo : Trigger.new){
			/*
			if (ooo.Pricing_Forecast_Used__c == 'Dell Pricebook' || ooo.Pricing_Forecast_Used__c == 'Hewlett-Packard'
					|| ooo.Pricing_Forecast_Used__c == 'IBM' || ooo.Pricing_Forecast_Used__c == 'CRAY'
					|| ooo.Pricing_Forecast_Used__c == 'EMC' || ooo.Pricing_Forecast_Used__c == 'ORACLE'
					|| ooo.Pricing_Forecast_Used__c == 'SGI' || ooo.Pricing_Forecast_Used__c == 'TERADATA') {
				ooo.Pricebook2Id = Quote_ENV.PriceBookMap.get('Standard Price Book');
			}
			else if (ooo.Pricing_Forecast_Used__c != '' && ooo.Pricing_Forecast_Used__c != null){
				ooo.Pricebook2Id = Quote_ENV.PriceBookMap.get(ooo.Pricing_Forecast_Used__c);
			}
			*/
			//Always set to Standard PB for OEM deals
			if (ooo.RecordTypeID == '01250000000DOn2'){
				ooo.Pricebook2Id = Quote_ENV.PriceBookMap.get('Standard Price Book');
			}
		}	
	}
	else {
	
		List<Opportunity> optys = new List<Opportunity>();
		
		for (Opportunity o : Trigger.new) {
			if(o.RecordTypeID == '01250000000DOn2' && o.Pricing_Forecast_Used__c != Trigger.oldmap.get(o.ID).Pricing_Forecast_Used__c && o.Pricing_Forecast_Used__c != '') {
				optys.add(o);
			}	
		}
		if (optys.size() == 0)
			return;
		//Assumtion - handle 1 opty changes each time
		List<OpportunityLineItem> optylines = new List<OpportunityLineItem>([select id, UnitPrice,PricebookEntry.Product2Id from OpportunityLineItem 
																				where OpportunityID = :optys[0].id ]);
		List<OpportunityLineItemSchedule> optylines_schedules = new List<OpportunityLineItemSchedule>([select id, OpportunityLineItem.OpportunityId,
																										OpportunityLineItemID, Quantity, Revenue
																										from OpportunityLineItemSchedule where OpportunityLineItem.OpportunityId = :optys[0].id]);
		ID OEM_PB_ID = '01s500000006J7F';
		ID curr_PB_ID = Quote_ENV.PriceBookMap.get(optys[0].Pricing_Forecast_Used__c);
		
		if (curr_PB_ID == null || optylines.size() == 0)
			return;
		//map optyline to optylines-schedules
		List<OpportunityLineItemSchedule> oline_sch_updates = new List<OpportunityLineItemSchedule>();
		Map<ID, List<OpportunityLineItemSchedule>> map_optylineID_to_schedules = new Map<ID, List<OpportunityLineItemSchedule>>();
		List<OpportunityLineItemSchedule> tmp_oline_schedules = new List<OpportunityLineItemSchedule>();
		for (OpportunityLineItemSchedule osch : optylines_schedules){
			if(map_optylineID_to_schedules.get(osch.OpportunityLineItemID) == null){
				tmp_oline_schedules = new List<OpportunityLineItemSchedule>();
				tmp_oline_schedules.add(osch);
				map_optylineID_to_schedules.put(osch.OpportunityLineItemID,tmp_oline_schedules);
			}
			else {
				tmp_oline_schedules = map_optylineID_to_schedules.get(osch.OpportunityLineItemID);
				tmp_oline_schedules.add(osch);
				map_optylineID_to_schedules.put(osch.OpportunityLineItemID,tmp_oline_schedules);
			}
		}
		//			
		Set<ID> prod_IDs = new Set<ID>();
		for (OpportunityLineItem oline : optylines) {
			prod_IDs.add(oline.PricebookEntry.Product2Id);
		}
		//build map
		List<PricebookEntry> lst_pbe = new List<PricebookEntry>([select id,Product2ID,Pricebook2ID,UnitPrice from PricebookEntry 
																	where isActive=true and Product2ID in :prod_IDs 
																	and (Pricebook2ID = :curr_PB_ID or Pricebook2ID = :OEM_PB_ID)]);
																	
		Map<ID,Map<ID,Double>> map_prodID_PricebookID_Price = new Map<ID,Map<ID,Double>>();
		Map<ID,Double> tmp_map_PricebookID_Price = new Map<ID,Double>();  
		for (PricebookEntry pbe : lst_pbe){
			if (map_prodID_PricebookID_Price.get(pbe.Product2ID)==null){
				tmp_map_PricebookID_Price = new Map<ID,Double>();
				tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
				map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
			}
			else {
				tmp_map_PricebookID_Price = map_prodID_PricebookID_Price.get(pbe.Product2ID);
				tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
				map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
			}
		}																				
		//																				
		if(map_prodID_PricebookID_Price.size() == 0)
			return;
		/*
		Set<String> Tier1_customers = new Set<String>();
		Tier1_customers.add('Hewlett-Packard');
		Tier1_customers.add('IBM');
		Tier1_customers.add('Dell Pricebook');
		Tier1_customers.add('CRAY');
		Tier1_customers.add('EMC');
		Tier1_customers.add('SGI');
		Tier1_customers.add('ORACLE');
		Tier1_customers.add('TERADATA');
		*/
		//Assign Prices
		for (OpportunityLineItem oo : optylines) {
			if(map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id) != null) { 
				if(curr_PB_ID !=null && map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id).get(curr_PB_ID) != null
					&& map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id).get(curr_PB_ID) != oo.UnitPrice) {
						oo.UnitPrice = map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id).get(curr_PB_ID);
						//update opty line schedules
						if(map_optylineID_to_schedules.get(oo.ID) != null) {
							for (OpportunityLineItemSchedule osch : map_optylineID_to_schedules.get(oo.ID)){
								osch.Revenue = osch.Quantity * oo.UnitPrice;
								oline_sch_updates.add(osch);
							}
						}
				}
				//if can't find prices in the custom PB, need to look into OEM PB
				else if (curr_PB_ID !=null && map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id).get(curr_PB_ID) == null 
								&& map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id).get(OEM_PB_ID) != null
								&& map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id).get(OEM_PB_ID) != oo.UnitPrice) {
						oo.UnitPrice = map_prodID_PricebookID_Price.get(oo.PricebookEntry.Product2Id).get(OEM_PB_ID);
						//update opty line schedules
						if(map_optylineID_to_schedules.get(oo.ID) != null) {
							for (OpportunityLineItemSchedule osch : map_optylineID_to_schedules.get(oo.ID)){
								osch.Revenue = osch.Quantity * oo.UnitPrice;
								oline_sch_updates.add(osch);
							}
						}
				}
			}
		}
		//
		if(optylines.size() > 0)
			update optylines;
		if(oline_sch_updates.size() > 0)
			update oline_sch_updates;
	}		
}