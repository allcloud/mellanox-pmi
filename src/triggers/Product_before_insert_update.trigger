trigger Product_before_insert_update on Product2 (before insert, before update, after insert) {

	if (trigger.isBefore) {
		for(Product2 Pr:trigger.new) {
		     
			 //Matching duplicate accounts key   
			 String canonical = Pr.Name.toUpperCase(); 
			 Blob bsig = Crypto.generateDigest('MD5', Blob.valueOf(canonical));  
			 Pr.NAME_CODE__C = encodingUtil.convertToHex (bsig);
		 }
		 
		 cls_trg_Product2 handler = new cls_trg_Product2();   
		 if (trigger.isUpdate) {
		 	
		 	handler.updateProductPhotoUrlOnProductDetailChange(trigger.new, trigger.oldMap);
		 	handler.assignProductDetailsToNewProducts(trigger.new, trigger.oldMap);
		 }
		 
		 else if (trigger.isInsert) {
		 	
		 	handler.assignProductDetailsToNewProducts(trigger.new, null);
		 }
	}
	
	else if (trigger.isAfter) {
		
		if (trigger.isInsert) {
			
			cls_trg_Product2 handler = new cls_trg_Product2();
			handler.assignAssetsToProductByPartNumbers(trigger.new);
		}
	}
	
}