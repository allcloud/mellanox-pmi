trigger OppLineItem_After_Insert_Update on OpportunityLineItem (before insert) {
		List<OpportunityLineItem> lst_optylines = new List<OpportunityLineItem>();
		
		Set<ID> prod_IDs = new Set<ID>();
		Set<ID> PB_IDs = new Set<ID>();
		/*
		Set<String> Tier1_customers = new Set<String>();
		Tier1_customers.add('Hewlett-Packard');		
		Tier1_customers.add('IBM');
		Tier1_customers.add('Dell Pricebook');
		Tier1_customers.add('CRAY');
		Tier1_customers.add('EMC');
		Tier1_customers.add('SGI');
		Tier1_customers.add('ORACLE');
		Tier1_customers.add('TERADATA');
		*/	
		for (OpportunityLineItem oo : Trigger.new){
			if(oo.OpportunityRecordType__c == '01250000000DOn2' && oo.Opportunity_PB__c != '' ){
					lst_optylines.add(oo);
					PB_IDs.add(Quote_ENV.PriceBookMap.get(oo.Opportunity_PB__c));
					prod_IDs.add(oo.Product_ID__c);
			}
		}
	
		if(lst_optylines.size() == 0)
			return;
		
		ID OEM_PB_ID = '01s500000006J7F';
		PB_IDs.add(OEM_PB_ID);
		
		
	//build map
			List<PricebookEntry> lst_pbe = new List<PricebookEntry>([select id,Product2ID,Pricebook2ID,UnitPrice from PricebookEntry 
																		where isActive=true and Product2ID in :prod_IDs 
																		and Pricebook2ID in :PB_IDs]);
																		
			Map<ID,Map<ID,Double>> map_prodID_PricebookID_Price = new Map<ID,Map<ID,Double>>();
			Map<ID,Double> tmp_map_PricebookID_Price = new Map<ID,Double>();  
			for (PricebookEntry pbe : lst_pbe){
				if (map_prodID_PricebookID_Price.get(pbe.Product2ID)==null){
					tmp_map_PricebookID_Price = new Map<ID,Double>();
					tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
					map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
				}
				else {
					tmp_map_PricebookID_Price = map_prodID_PricebookID_Price.get(pbe.Product2ID);
					tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe.UnitPrice);
					map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
				}
			}																				
	//
		//																				
		if(map_prodID_PricebookID_Price.size() == 0)
			return;
			//
		for(OpportunityLineItem o : lst_optylines){
			if(map_prodID_PricebookID_Price.get(o.Product_ID__c) != null) { 
					if(Quote_ENV.PriceBookMap.get(o.Opportunity_PB__c) != null 
						&& map_prodID_PricebookID_Price.get(o.Product_ID__c).get(Quote_ENV.PriceBookMap.get(o.Opportunity_PB__c)) != null) {
							o.UnitPrice = map_prodID_PricebookID_Price.get(o.Product_ID__c).get(Quote_ENV.PriceBookMap.get(o.Opportunity_PB__c));
					}
					else if (map_prodID_PricebookID_Price.get(o.Product_ID__c).get(OEM_PB_ID) != null) {
							o.UnitPrice = map_prodID_PricebookID_Price.get(o.Product_ID__c).get(OEM_PB_ID);
					}
			}	
			System.debug('...KN o.UnitPrice===' + o.UnitPrice);		
		}
		
}