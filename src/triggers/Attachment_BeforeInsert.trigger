trigger Attachment_BeforeInsert on Attachment (before insert) 
{
	List<Attachment> attach = new List<Attachment>();
	List<ID> emailMessageId = new List<ID>(); 
	for(Attachment a : Trigger.new)
	{
		String attachParentId = a.ParentId;
		if(attachParentId.startsWith(EmailMessage.sObjectType.getDescribe().getKeyPrefix()))
		{
			attach.Add(a);
			emailMessageId.Add(a.ParentId);
		}
	}
	if(emailMessageId.size()>0)
	{
		Map<ID,EmailMessage> emailMess = new Map<ID,EmailMessage>([Select e.ParentId From EmailMessage e where e.Id in :emailMessageId and e.IsDeleted=false]);
		for(Attachment a : attach)
		{
			EmailMessage e = emailMess.get(a.ParentId);
			if(e.ParentId!=null)
			{
				a.IsPrivate=false;
				a.ParentId = e.ParentId;
			}
		}
	}
}