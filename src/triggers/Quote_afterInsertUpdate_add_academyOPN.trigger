trigger Quote_afterInsertUpdate_add_academyOPN on Quote (after update) {
	//Trigger 1 - Set flag AcademyOPN_Required__c
	if (!Quote_ENV.updateQuote_run_first) {
        Quote_ENV.updateQuote_run_first=true;
        Map<ID,boolean> map_quoteID_AcademyOPN_Required = new Map<ID,boolean>();
        Set<ID> qIDs = new Set<ID>();
        List<Quote> all_quotes = new List<Quote>();
        List<Quote> quotes_updates = new List<Quote>();
        for (Quote q : Trigger.New){
        	//Only apply to new quotes created after 11/11/13
        	
        	if (q.isNewGPS__c == false)
        		continue;
        	//	
        	qIDs.add(q.ID);
        	
        	if (q.UFM_Total__c!=null && q.UFM_Total__c>0 && q.Partner_Account_Type__c!='OEM'){
				map_quoteID_AcademyOPN_Required.put(q.ID,true);
            }
            else if (q.Switch_Total__c!=null && q.Switch_Total__c>0 && q.Partner_Account_Type__c!='OEM'){
				map_quoteID_AcademyOPN_Required.put(q.ID,true);
            }
            else {
            	map_quoteID_AcademyOPN_Required.put(q.ID,false);
            }
        }
        if (qIDs.size()==0)
        	return;
        
        all_quotes = [select ID, AcademyOPN_Required__c,QuoteNumber from Quote where ID in :qIDs];
        
        for (Quote qq : all_quotes){

        	if (map_quoteID_AcademyOPN_Required.get(qq.ID)!=null && map_quoteID_AcademyOPN_Required.get(qq.ID)!=qq.AcademyOPN_Required__c){
        		qq.AcademyOPN_Required__c = map_quoteID_AcademyOPN_Required.get(qq.ID);
        		quotes_updates.add(qq);  	
        	}        		
        }
        if(quotes_updates.size()>0)
        	update quotes_updates;
	}
	// Trigger 2 - ADD Academy OPN products
	Set<ID> quotes_IDs = new Set<ID>();
    List<Quote> quotes = new List<Quote>();
    //Add Academy OPN: MTR-ALLLIBINC-12M
    ID AcademyOPN_ID = '01t500000025yp2';
    Integer Qty_AcademyOPN = 2;
    Set<ID> set_ProductIDs = new Set<ID>();
    set_ProductIDs.add('01t500000025yp2');
    //
    Set<Id> set_PriceBookID = new Set<Id>();
    Set<Id> set_PriceBookID_GPS = new Set<Id>();
		
    for (Quote q : Trigger.New){
		if (Trigger.oldmap.get(q.ID).AcademyOPN_Required__c==false && q.AcademyOPN_Required__c==true){
			quotes_IDs.add(q.id);
			quotes.add(q);
			set_PriceBookID_GPS.add(q.Pricebook2ID);
			set_PriceBookID.add(q.Pricebook2ID);
			set_PriceBookID.add(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c));	
		}
	}
    if (quotes.size()==0)
    	return;	
    
    Map<ID,PricebookEntry> map_PricebookEntry = new Map<ID,PricebookEntry>([select id,Pricebook2ID,Product2ID,UnitPrice from PricebookEntry 
                                                                               where Product2Id in :set_ProductIDs 
                                                                               and Pricebook2ID in :set_PriceBookID and isActive=true]);
    Map<ID,Map<ID,PricebookEntry>> map_prodID_PricebookID_PBEntryID = new Map<ID,Map<ID,PricebookEntry>>();
    Map<ID,PricebookEntry> map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
    for (PricebookEntry pbentry : map_PricebookEntry.values()){
    	if(map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID)==null){
	        map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
	        map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
	        map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
		}
    	else {
    		map_PricebookID_PBEntryID = map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID);
        	map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
        	map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
    	}
	}   
	// this map is used to check for duplicated AcademyOPN already existed
	List<QuoteLineItem> lst_AcademyOPN_QuoteLineItem = new List<QuoteLineItem>([select id,PricebookentryID,Pricebookentry.Pricebook2Id,QuoteID from QuoteLineItem 
																				where QuoteID in :quotes_IDs
																				and Product__c = 'MTR-ALLLIBINC-12M' 
																				and Pricebookentry.Pricebook2Id in :set_PriceBookID_GPS]);
    Map<ID,List<QuoteLineItem>> map_QuoteID_Quotelines_AcademyOPN = new Map<ID,List<QuoteLineItem>>();
    List<QuoteLineItem> tmp_qlines = new List<QuoteLineItem>();
    for (QuoteLineItem qline : lst_AcademyOPN_QuoteLineItem){
    	if(map_QuoteID_Quotelines_AcademyOPN.get(qline.QuoteID)==null){
        	tmp_qlines = new List<QuoteLineItem>();
			tmp_qlines.add(qline);
			map_QuoteID_Quotelines_AcademyOPN.put(qline.QuoteID,tmp_qlines);				        			
        }
        else{
        	tmp_qlines = map_QuoteID_Quotelines_AcademyOPN.get(qline.QuoteID);
        	tmp_qlines.add(qline);
			map_QuoteID_Quotelines_AcademyOPN.put(qline.QuoteID,tmp_qlines);
        }
    } 
    //  
    List<QuoteLineItem> lst_QuoteLineItemToInsert = new List<QuoteLineItem>();
	List<QuoteLineItem> lst_QuoteLineItemToDelete = new List<QuoteLineItem>();
    QuoteLineItem qline;
    for (Quote qq : quotes){
		if(AcademyOPN_ID!=null && map_prodID_PricebookID_PBEntryID.get(AcademyOPN_ID)!=null 
							&& map_prodID_PricebookID_PBEntryID.get(AcademyOPN_ID).get(qq.Pricebook2Id)!=null
							&& Quote_ENV.PriceBookMap!=null && Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)!=null  
                    		&& map_prodID_PricebookID_PBEntryID.get(AcademyOPN_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c))!=null )
		{    		
    		qline = new QuoteLineItem (QuoteID=qq.ID,
    							   PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(AcademyOPN_ID).get(qq.Pricebook2Id).ID,
    							   Quantity=Qty_AcademyOPN,
		                           UnitPrice=map_prodID_PricebookID_PBEntryID.get(AcademyOPN_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice,
		                           New_Price__c=map_prodID_PricebookID_PBEntryID.get(AcademyOPN_ID).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice,
		                           supportOPN_by_mapping__c=true);
			
			lst_QuoteLineItemToInsert.add(qline);
			
			if(map_QuoteID_Quotelines_AcademyOPN.get(qq.ID) != null){
				for (QuoteLineItem qli : map_QuoteID_Quotelines_AcademyOPN.get(qq.ID)){
			    	if (qli.PricebookEntryId == qline.PricebookEntryId)
			        	lst_QuoteLineItemToDelete.add(qli);	
			    }
			}
		}		                       
    }
    //Insert GPS first, then delete, order is important 
    if(lst_QuoteLineItemToInsert.size()>0)
    	insert lst_QuoteLineItemToInsert;
	if(lst_QuoteLineItemToDelete.size()>0)
        delete lst_QuoteLineItemToDelete;
}