trigger Opportunity_Reg_Ufter_upd on Distributor_Oppy_Registrations__c (after insert, after update) {
 List<string> SDEmails = new List<string>();
 Map<string,User> SD_User = new Map<string,User>();
 List<id> ThisOpp = new List<id>();
Map<id, Distributor_Oppy_Registrations__c> UpdOpps = new Map<id,Distributor_Oppy_Registrations__c>();
 
for(Distributor_Oppy_Registrations__c Opp:trigger.new)
 { SDEmails.add(Opp.Mellanox_Sales_Director_Email__c);
   ThisOpp.add(Opp.id);
 }

for(Distributor_Oppy_Registrations__c Opp1:[select id, Sales_Director_User__c from Distributor_Oppy_Registrations__c where id in :ThisOpp])
{ UpdOpps.put(Opp1.id,Opp1);}


for(User SD:[select id, email from User where Email in :SDEmails])
{ SD_User.put(SD.email,SD);}

  for (Distributor_Oppy_Registrations__c OppReg:trigger.new)
   { 
    if(SD_User.containsKey(OppReg.Mellanox_Sales_Director_Email__c))
      { Distributor_Oppy_Registrations__c NewOReg = UpdOpps.get(OppReg.id);
        NewOReg.Sales_Director_User__c = SD_User.get(OppReg.Mellanox_Sales_Director_Email__c).id;
        Update (NewOReg);
      }
   }
}