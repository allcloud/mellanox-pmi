trigger Task_before_insert_update on Task (before insert, before update, after insert) {
    
    Map<task,id> TasksMilestones = new Map<task,id>();
    Map<id,milestone1_milestone__c> MilestoneToProject = new Map<id,milestone1_milestone__c>();
    
    if (trigger.isBefore) {
	   
	    for(Task ts:trigger.new)
	    {
	        
	        if(ts.whatId != NULL && (String.valueOf(ts.whatId)).StartsWith('a2O')) //Milestone task
	        {
	            if(trigger.isinsert || (trigger.isupdate && ts.ActivityDate != trigger.oldmap.get(ts.id).activityDate))
	            {
	                ts.DueDate_Custom__c = ts.ActivityDate;
	            }
	            
	            if(trigger.isinsert && ts.projectId__c==NULL)
	            {
	                
	                TasksMilestones.put(ts,ts.whatId);
	                
	            }
	        }
	    }
	    
	    
	    if(!TasksMilestones.isempty())
	    {
	        for(milestone1_milestone__c ms:[select id,project__c, project__r.name from milestone1_milestone__c where id in: TasksMilestones.values()])
	        {
	            MilestoneToProject.put(ms.id,ms);
	        }
	        
	        
	        for(task tsk:TasksMilestones.keyset())
	        {
	            tsk.projectid__c = string.valueOf(( MilestoneToProject.get(tsk.whatId).project__c)).substring(0, 15);
	            tsk.projectName__c = MilestoneToProject.get(tsk.whatid).project__r.name;
	        }
	    }
    }
    
    else if (trigger.isAfter && trigger.isInsert) {
        
        cls_trg_Task handler = new cls_trg_Task();
        handler.updateTasksRelatedLeads(trigger.New);
    }
}