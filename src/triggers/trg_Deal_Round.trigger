trigger trg_Deal_Round on Deal_Round__c (after update) {
	
	cls_trg_Deal_Round handler = new cls_trg_Deal_Round();
	
	if (trigger.isAfter) {
		
		if (trigger.isUpdate) {
			
			handler.sendEmailToRateDeals(trigger.New, trigger.OldMap);
		}
	}

}