trigger Case_AfterUpdate_AfterInsert_AE on Case (after insert, after update) 
{
 /*
  List<Case> changedCases = new List<Case>();
  List<CaseTeamMember> insCaseTeamMember = new List<CaseTeamMember>(); 
  List<ID> caseId = new List<ID>();
  Boolean flag=false;
  String query = 'Select MemberId, TeamRoleId From CaseTeamMember Where (' ;
  for(Case c : Trigger.new )
  {
    if(Trigger.isInsert)
    {
      if(c.AE_ENGINEER__c !=null)
      {
        changedCases.Add(c);
      }
    }
    else if(Trigger.isUpdate)
    {
      if(c.AE_ENGINEER__c != Null)
      {
        if(c.AE_ENGINEER__c!=null)
        {
          changedCases.Add(c);
        }
        if(flag)
        {
            query+=' or ';
        }
        query+='(ParentId=\''+c.Id+'\' and MemberId=\''+c.AE_ENGINEER__c+'\')';
        flag=true;
        caseId.Add(c.Id);
      }
    }
  }
  query+=')';
  if(Trigger.isUpdate)
  {
    if(caseId.size()>0)
    {
        query+=' or (TeamRoleId=\''+ENV.teamRoleMap.get('AE Engineer')+'\' and ParentId in '+ENV.getStringIdsForDynamicSoql(caseId)+')';  
        
    }
    try
    {
        system.debug('query: ' +query);
        List<CaseTeamMember> delTeamMember = Database.query(query);
        delete delTeamMember;
    }
    Catch(Exception e)
    {
    }
  }
  
  for(Case newCase: changedCases)
  {
    CaseTeamMember ctmNew = new CaseTeamMember();
    ctmNew.ParentId = newCase.Id;
    ctmNew.MemberId = newCase.AE_ENGINEER__c; 
    system.debug('blat ctmNew.MemberId '+ctmNew.MemberId);
    system.debug('blat newCase.Id '+newCase.Id);
    ctmNew.TeamRoleId = ENV.teamRoleMap.get('AE Engineer');
    insCaseTeamMember.add(ctmNew);
   
     }

  if(insCaseTeamMember.size()>0)
  {    
     insert insCaseTeamMember;
  }
  */
  
    if (trigger.isAfter && trigger.isUpdate) {  
  		
  		if (ENV.triggerShouldRun) {
	  		
	  		clsCase handler = new clsCase();
	  		handler.addTeamMemberToCaseRelatedProject(trigger.New, trigger.OldMap);
	  		handler.populateCaseOwnerTextOnCaseOwnerChange(trigger.New, trigger.OldMap);
	  		ENV.triggerShouldRun = false;
  		}
    }
    
    else if (trigger.isAfter && trigger.isInsert) {
    	
    	clsCase handler = new clsCase();
    	handler.createCaseCommentForCaseRelatedToPOCProject(trigger.New);
    	handler.populateCaseOwnerTextOnCaseOwnerChange(trigger.New, null);
    }
}