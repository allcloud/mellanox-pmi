trigger PCN_Products_AfterInsert on PCN_Affected_Products__c (after insert, after delete) {
	Set<String> products = new Set<String>();
	Set<String> products_2 = new Set<String>();
	Set<String> products_3 = new Set<String>();
	Set<String> products_4 = new Set<String>();
	Set<String> products_5 = new Set<String>();
	Set<String> products_6 = new Set<String>();
	ID PCN_ID;
	Date dd = System.today().addyears(-3);
	Integer ii = 0;
	Integer jj = 0;
	
	if(Trigger.isInsert){
		PCN_ID = Trigger.new[0].PCN__c;
		for (PCN_Affected_Products__c p : Trigger.New){
System.debug('...KN p.PCN_Approver_by_OPN__c===' + p.PCN_Approver_by_OPN__c);
			//Skip approver by OPNs -PCNs
			if(p.PCN_Approver_by_OPN__c == true)
				continue;
			//
			if(p.OPN_Name_Bulk_load__c == '' || p.OPN_Name_Bulk_load__c == null) {
				if(ii < 5)
					products.add(p.Product_Name__c);
				else
					products_2.add(p.Product_Name__c);
				ii++;
			}
			else {
				if(jj < 15)
					products.add(p.Product_Name__c);
				else if(jj <= 30)
					products_2.add(p.Product_Name__c);
				else if(jj <= 50)
					products_3.add(p.Product_Name__c);
				else if(jj <= 100)
					products_4.add(p.Product_Name__c);
				else if(jj <= 150)
					products_5.add(p.Product_Name__c);
				else
					products_6.add(p.Product_Name__c);
				
				jj++;
			}
		}
		//limit search to only 2 years or less if multiple products
		if(products_5.size() > 0)
			dd = System.today().addyears(-2);
		//
		Set<String> accts = new Set<String>();
		Map<String, Set<String>> map_prod_setAccountName = new Map<String, Set<String>>();
		Set<String> tmp_setAcct = new Set<String>(); 
		//NOt handling Ship_To_Account yet---Ship_to_Account__c 
		List<BBB_Orders__c> bbb_orders = [select id,Ordered_Item__c,Customer_Parent__c From BBB_Orders__c 
											where Ordered_Item__c in :products and Customer_Parent__c!=null and Customer_Parent__c!=''
											and Transaction_QTY__c >0 
											and (NOT Name like '9%') and (NOT Name like '7%') 
											and (NOT Name like '6%') and (NOT Name like '100%') and (NOT Name like '-1%')
											and (S__c = 'BLS' or S__c = 'BxLOG')
											and BBBSDT__c >= :dd];
		if(products_2.size() > 0) {
			List<BBB_Orders__c> bbb_orders_2 = [select id,Ordered_Item__c,Customer_Parent__c From BBB_Orders__c 
											where Ordered_Item__c in :products_2 and Customer_Parent__c!=null and Customer_Parent__c!=''
											and Transaction_QTY__c >0 and (NOT Name like '9%') and (NOT Name like '7%')
											and (NOT Name like '6%') and (NOT Name like '100%') and (NOT Name like '-1%')
											and (S__c = 'BLS' or S__c = 'BxLOG')
											and BBBSDT__c >= :dd];
			//need this when adding in PCN_Contact__c
			products.addall(products_2);
			
			if(bbb_orders_2 != null && bbb_orders_2.size() > 0)
				bbb_orders.addall(bbb_orders_2);
		}
		if(products_3.size() > 0) {
			List<BBB_Orders__c> bbb_orders_3 = [select id,Ordered_Item__c,Customer_Parent__c From BBB_Orders__c 
											where Ordered_Item__c in :products_3 and Customer_Parent__c!=null and Customer_Parent__c!=''
											and Transaction_QTY__c >0 and (NOT Name like '9%') and (NOT Name like '7%')
											and (NOT Name like '6%') and (NOT Name like '100%') and (NOT Name like '-1%')
											and (S__c = 'BLS' or S__c = 'BxLOG')
											and BBBSDT__c >= :dd];
			//need this when adding in PCN_Contact__c
			products.addall(products_3);
			
			if(bbb_orders_3 != null && bbb_orders_3.size() > 0)
				bbb_orders.addall(bbb_orders_3);
		}
		if(products_4.size() > 0) {
			List<BBB_Orders__c> bbb_orders_4 = [select id,Ordered_Item__c,Customer_Parent__c From BBB_Orders__c 
											where Ordered_Item__c in :products_4 and Customer_Parent__c!=null and Customer_Parent__c!=''
											and Transaction_QTY__c >0 and (NOT Name like '9%') and (NOT Name like '7%')
											and (NOT Name like '6%') and (NOT Name like '100%') and (NOT Name like '-1%')
											and (S__c = 'BLS' or S__c = 'BxLOG')
											and BBBSDT__c >= :dd];
			//need this when adding in PCN_Contact__c
			products.addall(products_4);
			
			if(bbb_orders_4 != null && bbb_orders_4.size() > 0)
				bbb_orders.addall(bbb_orders_4);
		}
		if(products_5.size() > 0) {
			List<BBB_Orders__c> bbb_orders_5 = [select id,Ordered_Item__c,Customer_Parent__c From BBB_Orders__c 
											where Ordered_Item__c in :products_5 and Customer_Parent__c!=null and Customer_Parent__c!=''
											and Transaction_QTY__c >0 and (NOT Name like '9%') and (NOT Name like '7%')
											and (NOT Name like '6%') and (NOT Name like '100%') and (NOT Name like '-1%')
											and (S__c = 'BLS' or S__c = 'BxLOG')
											and BBBSDT__c >= :dd];
			//need this when adding in PCN_Contact__c
			products.addall(products_5);
			
			if(bbb_orders_5 != null && bbb_orders_5.size() > 0)
				bbb_orders.addall(bbb_orders_5);
		}
		if(products_6.size() > 0) {
			List<BBB_Orders__c> bbb_orders_6 = [select id,Ordered_Item__c,Customer_Parent__c From BBB_Orders__c 
											where Ordered_Item__c in :products_6 and Customer_Parent__c!=null and Customer_Parent__c!=''
											and Transaction_QTY__c >0 and (NOT Name like '9%') and (NOT Name like '7%')
											and (NOT Name like '6%') and (NOT Name like '100%') and (NOT Name like '-1%')
											and (S__c = 'BLS' or S__c = 'BxLOG')
											and BBBSDT__c >= :dd];
			//need this when adding in PCN_Contact__c
			products.addall(products_6);
			
			if(bbb_orders_6 != null && bbb_orders_6.size() > 0)
				bbb_orders.addall(bbb_orders_6);
		}
		//end -break down list
		for (BBB_Orders__c bbb : bbb_orders){
				accts.add(bbb.Customer_Parent__c.toUpperCase());
				//accts.add(bbb.Ship_to_Account__c);
				if (map_prod_setAccountName.get(bbb.Ordered_Item__c)==null){
					tmp_setAcct = new Set<String>();
					tmp_setAcct.add(bbb.Customer_Parent__c.toUpperCase());
					map_prod_setAccountName.put(bbb.Ordered_Item__c,tmp_setAcct);
				}
				else {
					tmp_setAcct = map_prod_setAccountName.get(bbb.Ordered_Item__c);
					tmp_setAcct.add(bbb.Customer_Parent__c.toUpperCase());
					map_prod_setAccountName.put(bbb.Ordered_Item__c,tmp_setAcct);
				}
		}	
	System.debug('... KN map_prod_setAccountName ==='+map_prod_setAccountName);		
		List<Contact> lst_contacts = [select id,Account.Name,Name,PCN_Notification__c from Contact where Account.Name in :accts and PCN_Notification__c=true];
		Map<String,Set<Contact>> map_AcctName_SetContact = new Map<String,Set<Contact>>();
		Set<Contact> tmp_setContact = new Set<Contact>();  
		Set<Contact> set_contacts = new Set<Contact>();
		for (Contact c : lst_contacts){
			set_contacts.add(c);
			if(map_AcctName_SetContact.get(c.Account.Name)==null){
				tmp_setContact = new Set<Contact>();
				tmp_setContact.add(c);
				map_AcctName_SetContact.put(c.Account.Name,tmp_setContact);							
			}
			else {
				tmp_setContact = map_AcctName_SetContact.get(c.Account.Name);
				tmp_setContact.add(c);
				map_AcctName_SetContact.put(c.Account.Name,tmp_setContact);
			}
		}
	System.debug('... KN map_AcctName_SetContact ==='+map_AcctName_SetContact);	
		List<PCN_Contacts__c> pcn_contacts = new List<PCN_Contacts__c>();
		//for (Contact cc : set_contacts){
		//	pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=cc.ID,Name=cc.Name));
		//}
		for (String p : products){
			if(map_prod_setAccountName!=null && map_prod_setAccountName.get(p)!=null){
				for (String acct : map_prod_setAccountName.get(p)){
					tmp_setContact = map_AcctName_SetContact.get(acct);
					if(tmp_setContact!=null){	
						for (Contact cc : tmp_setContact){
							pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=cc.ID,Product__c=p,Company__c=acct));	
						}
					}
					else {
						pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Product__c=p,Company__c=acct));
					}				
				}
			}				
		}	
		if (pcn_contacts.size()>0)
			insert pcn_contacts;
	}
	//For delete
	
	else {
		PCN_ID = Trigger.old[0].PCN__c;
		String pcn_type = Trigger.old[0].PCN_Type__c;
System.debug('... trigger to delete KN pcn_type==='+pcn_type);
		for (PCN_Affected_Products__c p : Trigger.Old){
			products.add(p.Product_Name__c);
		}
		Map<ID,PCN_Contacts__c> pcnContacts_toDelete = new Map<ID,PCN_Contacts__c>([select id from PCN_Contacts__c where PCN__c = :PCN_ID and Product__c in :products]);
		if (pcnContacts_toDelete.size()>0){
			//Check: should we delete PCN_Comments ?
			//List<PCN_Comment__c> lst_PCN_comments = new List<PCN_Comment__c>([select pcc.id from PCN_Comment__c pcc where pcc.PCN_Contacts__c in :pcnContacts_toDelete.keyset()]);
			//if (lst_PCN_comments != null && lst_PCN_comments.size()>0)
			//	delete lst_PCN_comments;
			delete pcnContacts_toDelete.values();
		}			
		
		//Re-calculate # total pending approvals on PCN
		PCN__c pcn_update = [select id, Type__c from PCN__c where id = :PCN_ID];
		if (pcn_update != null) {
			pcn_update.Type__c = 'Private';
			update pcn_update;
			pcn_update.Type__c = pcn_type;
			update pcn_update;
		}
		//Only when delete need to re-calc, Insert already took care when PCN Type is changed
		/*if (pcn_type == 'Approval Required')
			PCN_update_PendingApproval_async.update_NumberPending(PCN_ID); 
		*/
	}	
		
}