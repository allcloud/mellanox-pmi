trigger trg_CSI_Assets on CSI_Asset__c (after insert, after update) {
	
	if (trigger.isAfter) {
		cls_trg_CSI_Assets handler = new cls_trg_CSI_Assets();
		
		if(trigger.isInsert){
			
			handler.insertCSIAssetsLog(trigger.New);
		}
		else if(trigger.isupdate){

			handler.updateCSIAssetsLog(trigger.new, trigger.oldMap);
		}
	}
}