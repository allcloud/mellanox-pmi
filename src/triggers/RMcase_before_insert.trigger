trigger RMcase_before_insert on RmCase__c (before insert) {


list<id> CaseIds = new list<id>();
map<id,string> map_case_priority = new map<id,string>();
list<RMpriority__c> RMpriority = new list<RMpriority__c>();


for(RMcase__c rmc:trigger.new)
{
  CaseIds.add(rmc.sfcase__c);
 } 

for(case cs:[select id, priority from case where id in:CaseIds])
{
  map_case_priority.put(cs.id,cs.priority);
} 

RMpriority = [select RM_id__c, RM_Priority__c,SF_priority__c from RMpriority__c ];


for(RMCase__c RMc:trigger.new)
{
  for(RMpriority__c rmp:RMpriority)
  {
                         
    if(rmp.SF_priority__c != Null && rmp.SF_priority__c.contains( map_case_priority.get(RMc.sfcase__c)))
    {
     RMc.RMpriority__c = rmp.RM_id__c;
     RMc.Priority__c = rmp.RM_Priority__c;
    } 
     
  }
}

}