trigger Mcare_case_AfterInsert on Case (after update, after insert) {
    
    // Deleting Mcare case with same description abd adding it as comment to old case
    
    List<case> newCases = new List<case>();
    Map<string,case> newCasesMap = new Map<string,case>();
    Map<string,case> McareMap = new Map<string,case>();
    List<case> DelCases = new List<case>();
    Map<id,case> DelCasesMap = new Map<id,case>();
    Set<id> newCasesIds = new Set<id>();
    map<id,id> CaseContacts = new map<id,id>();
    List<FM_Discussion__c> InsertFMdiscussion = new List<FM_Discussion__c>();
    set<Id> accountIds_Set = new set<Id>();
    map<Id, Account> accountIds2Accounts_Map;
    
    if (trigger.isUpdate) {   
    	
    	clsCase handler = new clsCase();
    	handler.deleteMCareCases(trigger.New, trigger.oldMap);
	    for(case cs:trigger.new)
	    {
	    	system.debug('cs.origin : ' + cs.origin);
	    	system.debug('cs : ' + cs);
	    	system.debug('cs.Mcare_case_identifier__c : ' + cs.Mcare_case_identifier__c);
	    	system.debug('trigger.oldmap.get(cs.id).Mcare_case_identifier__c : ' + trigger.oldmap.get(cs.id).Mcare_case_identifier__c);
	        if(cs.origin == 'UFM CallHome' && (cs.Mcare_case_identifier__c !=null && trigger.oldmap.get(cs.id).Mcare_case_identifier__c == null ))
	        {
	            CaseContacts.put(cs.id, cs.AccountId);
	            newCases.add(cs);
	            newCasesMap.put(cs.Mcare_case_identifier__c,cs);
	            system.debug('newCasesMap!!!: ' + newCasesMap.KeySet());
	            system.debug('Case Contacts!!!: ' + CaseContacts);
	            
	        }
	        
	    }
	    
	   
	    if(!newCases.isempty())
	    { 
	    	list<case> relatedCasesByMCare_List = [SELECT id, Delete_This_Case__c, Mcare_case_identifier__c,description, Mcare_case_char__c, contactId, AccountId, Contact.AccountId  FROM Case WHERE state__c != 'Closed' AND Delete_This_Case__c != true AND Mcare_case_identifier__c in :newCasesMap.KeySet() AND AccountId in :CaseContacts.values()];
	       	
	       	
	       	if (relatedCasesByMCare_List.isEmpty()) {
	       		
	       		//DelCases.add(DelCasesMap.get(.id));
	       	}
	       	
	       	
	        system.debug('relatedCasesByMCare_List : ' + relatedCasesByMCare_List);
	       
	        for(case css : relatedCasesByMCare_List) {
	        
	            system.debug('New Case Id : ' + newCasesMap.get(css.Mcare_case_identifier__c).id);
	            system.debug('related Case Id : ' + css.id);
	            
	            if(newCasesMap.get(css.Mcare_case_identifier__c).id != css.id && !css.Delete_This_Case__c) {
	            
	            	McareMap.put(css.Mcare_case_identifier__c,css);
            	}
	            else {   
	            	
	            	DelCasesMap.put(css.id,css);
            	}
	            	system.debug('DelCasesMap: ' + DelCasesMap);
	           	    system.debug('McareMap: ' + McareMap);
	        }
	        
	        for(case cs1: newCases)
	        {
	            if(McareMap.containsKey(cs1.Mcare_case_identifier__c) && cs1.contactId != NULL )
	            {
	                
	                system.debug('New char: ' + cs1.description.length() );
	                system.debug('cs1.Mcare_case_identifier__c).AccountId : ' + McareMap.get(cs1.Mcare_case_identifier__c).AccountId );
	                system.debug('cs1.Mcare_case_identifier__c).Contact.AccountId : ' + McareMap.get(cs1.Mcare_case_identifier__c).Contact.AccountId );
	                
	                if(  cs1.description.length() < McareMap.get(cs1.Mcare_case_identifier__c).Mcare_case_char__c + 10  && McareMap.get(cs1.Mcare_case_identifier__c).Contact.AccountId == cs1.AccountId )
	                {
	                    
                        system.debug('inside 73 ');
                        FM_Discussion__c FM = new FM_Discussion__c();
                        FM.Discussion__c = cs1.description;
                        FM.Case__c = McareMap.get(cs1.Mcare_case_identifier__c).id;
                        FM.Source__c = 'UFM CallHome';
                        InsertFMdiscussion.add(FM);
                        system.debug('InsertFMdiscussion: ' + InsertFMdiscussion);
                        DelCases.add(DelCasesMap.get(cs1.id));
                        system.debug('DelCases: ' + DelCases);
	                }     
	            }
	        }  
	        
	        try {
	        		
	        	 
	        	clsCase theHandler = new clsCase(); 
	        	   	
	        	insert InsertFMdiscussion;
	        	 
	        }
	        
	        catch(Exception e) {
	        	
	        	system.debug('Error inserting FM Discussions : ' + e.getMessage());
	        }
	        
	        system.debug('InsertFMdiscussion : ' + InsertFMdiscussion);
	        
	        
	        if (relatedCasesByMCare_List.size() == 1) {
	        	
	        	Case theCase = new Case(Id = relatedCasesByMCare_List[0].Id);
	        	
	        	theCase.mcare_send_emails__c = true;
	        	update theCase;
	        	return;
	        }
	        
	        
	        try {
	        		
	        	for (Case theCase  : DelCases)	{
	        		
    		 		theCase.Delete_This_Case__c = true;
	        	}        	
	        	//delete DelCases;
	        	update DelCases;
	        }
	        
	        catch(Exception me) {
	        	
	        	system.debug('Error deleting Cases : ' + me.getMessage());
	        }
	    } 
    }
    
    else if (trigger.isInsert) {
    	
    	list<case> cases2Delete_List = new list<Case>();
    	
    	for (Case newCase : trigger.New) {
    		
    		system.debug('newCase.Origin : ' + newCase.Origin);
    		system.debug('newCase.Description : ' + newCase.Description);
    		system.debug('newCase.SuppliedEmail : ' + newCase.SuppliedEmail);
    		
    		if (newCase.Origin != null && newCase.Origin.EqualsIgnoreCase('UFM CallHome') && newCase.Description != null
    		 && newCase.Description.ContainsIgnoreCase('FabricHealth Report completed with 84 Errors and 9 Warnings')
    		 && newCase.SuppliedEmail != null && newCase.SuppliedEmail.containsIgnoreCase('mlnxcare@softlayer')) {
    			
    			accountIds_Set.add(newCase.AccountId);
    		 }
    	}
    	
    	 if (!accountIds_Set.isEmpty()) {
	    
	        accountIds2Accounts_Map = new map<Id, Account>([SELECT Id, Name FROM Account WHERE ID IN : accountIds_Set]);
	    }
	    
    	
    	for (Case newCase : trigger.New) {
    		
    		if (newCase.Origin != null && newCase.Origin.EqualsIgnoreCase('UFM CallHome') && newCase.Description != null
    		 && newCase.Description.ContainsIgnoreCase('FabricHealth Report completed with 84 Errors and 9 Warnings')
    		 && accountIds2Accounts_Map != null && newCase.AccountId != null && accountIds2Accounts_Map.containsKey(newCase.AccountId) && accountIds2Accounts_Map.get(newCase.AccountId).Name.EqualsIgnoreCase('SOFTLAYER TECHNOLOGIES')) {
    		 	
    		 	case case2Delete = new Case(Id  = newCase.Id);
    		 	cases2Delete_List.add(case2Delete);
    		 }
    	}
    	
    	if (!cases2Delete_List.isEmpty()) {
    		
    		delete cases2Delete_List;
    	}
    }
}