trigger Attachment_BeforeDelete on Attachment (before delete) 
{
	list<AttachmentIntegration__c> lst_AttIntegerationToDelete = new list<AttachmentIntegration__c>();
	for(AttachmentIntegration__c attIn:[Select  a.Id, a.Attachment_Id__c From AttachmentIntegration__c a where a.Attachment_Id__c IN: trigger.oldMap.keyset() and a.IsDeleted = false])
	{
		lst_AttIntegerationToDelete.add(attIn);
	}
	
	system.debug('===> lst_AttIntegerationToDelete: '+lst_AttIntegerationToDelete);
	if(!lst_AttIntegerationToDelete.isEmpty())
		delete lst_AttIntegerationToDelete;
		
		if (trigger.isBefore && trigger.isDelete)
		{
			cls_trg_Attachment handler = new cls_trg_Attachment();
			handler.checkAttExistsOnRMAFalse(trigger.old);
		}
}