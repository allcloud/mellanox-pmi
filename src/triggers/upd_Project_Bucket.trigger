trigger upd_Project_Bucket on RmProject__c (before insert) {

list<string> ProjectNames  = new list<string>();
map<string,RM_Project_Bucket__c> mapProjToBucket = new map<string,RM_Project_Bucket__c>();



for(RM_Project_Bucket__c rmb: [select Project_Name__c, Bucket__c,Second_bucket__c from RM_Project_Bucket__c])
{
 mapProjToBucket.put(rmb.Project_Name__c,rmb);
}

for(RmProject__c rmp:trigger.new)
{
 if(mapProjToBucket.containsKey(rmp.Project__c))
 {
   rmp.Bucket__c = mapProjToBucket.get(rmp.Project__c).bucket__c;
   rmp.Second_Bucket__c = mapProjToBucket.get(rmp.Project__c).second_bucket__c;
  }
 
 }

}