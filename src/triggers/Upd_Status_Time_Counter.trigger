trigger Upd_Status_Time_Counter on Case(before Insert, before Update) {

for(Case  c:trigger.new){
  if(Trigger.isUpdate){
    for(Case  a:trigger.old){
     if(c.casenumber==a.casenumber)
     {
	       if( c.recordTypeId!=a.recordTypeId && a.recordTypeId == Env.CaseTypeAdmin  )
	        { if(c.state_openAdmin_time_counter__c ==NULL){c.state_open_time_counter__c =0;}  
	          if(a.state__c=='Open' ){             
	            if(a.state_openAdmin_time_counter__c !=NULL)   
	               {c.state_openAdmin_time_counter__c = a.state_openAdmin_time_counter__c + a.time_slot_calc__c;}  
	             else {c.state_openAdmin_time_counter__c = a.time_slot_calc__c;}
	            } 
	        } 
	       if(c.state__c!=a.state__c)
	        {
	               
	          if(c.state_open_time_counter__c ==NULL){c.state_open_time_counter__c =0;}  
	          if(c.state_assigned_time_counter__c ==NULL){c.state_assigned_time_counter__c=0;}
	          if(c.State_RMA_Assigned_Time_Counter__c  ==NULL){c.State_RMA_Assigned_Time_Counter__c  =0;}
	          if(c.State_OpenAE_Time_Counter__c==NULL){c.State_OpenAE_Time_Counter__c =0;}
	          if(c.State_AssignedAE_Time_Counter__c==NULL){c.State_AssignedAE_Time_Counter__c =0;}
	          if(c.State_WaitForCustInfo_Time_Counter__c==NULL){c.State_WaitForCustInfo_Time_Counter__c =0;}    
	          if(c.State_WaitForCustApp_Time_Counter__c==NULL){c.State_WaitForCustApp_Time_Counter__c =0;}    
	          if(c.State_WaitForRelease_Time_Counter__c==NULL){c.State_WaitForRelease_Time_Counter__c =0;}        
	          if(c.State_OnHold_Time_Counter__c==NULL){c.State_OnHold_Time_Counter__c =0;}
	          if(c.State_AssignedRD_Time_Counter__c==NULL){c.State_AssignedRD_Time_Counter__c=0;}
	          if(c.State_Wait_for_Implementation_Time_Count__c==NULL){c.State_Wait_for_Implementation_Time_Count__c=0;}        
	         // if(c.time_slot_calc__c==NULL){c.time_slot_calc__c =0;}
	  
	  
	  
	  
	      
	        
	          if(a.state__c=='Open'){             
	            if(a.state_open_time_counter__c !=NULL)   
	               {c.state_open_time_counter__c = a.state_open_time_counter__c + a.time_slot_calc__c;}  
	             else {c.state_open_time_counter__c = a.time_slot_calc__c;}
	            }   
	              
	               
	              
	          if(a.state__c=='Assigned'){          
	           if(a.state_assigned_time_counter__c !=NULL)        
	            {c.state_assigned_time_counter__c=a.state_assigned_time_counter__c +a.time_slot_calc__c;}
	           else {c.state_assigned_time_counter__c=a.time_slot_calc__c;}          
	           }      
	                            
	           if(a.state__c=='RMA Assigned'){     
	            if(a.State_RMA_Assigned_Time_Counter__c  !=NULL)          
	             {c.State_RMA_Assigned_Time_Counter__c = a.State_RMA_Assigned_Time_Counter__c +a.time_slot_calc__c;}
	            else{c.State_RMA_Assigned_Time_Counter__c = a.time_slot_calc__c;} 
	           }
	    
	          if(a.state__c=='Open AE' || a.assignee__c == ENV.AEqueue){      
	            if(a.State_OpenAE_Time_Counter__c!=NULL)
	            {c.State_OpenAE_Time_Counter__c = a.State_OpenAE_Time_Counter__c +a.time_slot_calc__c;}
	            else{c.State_OpenAE_Time_Counter__c = a.time_slot_calc__c;}     
	           }
	          if(a.state__c=='Assigned AE'){      
	            if(a.State_AssignedAE_Time_Counter__c!=NULL)
	            {c.State_AssignedAE_Time_Counter__c = a.State_AssignedAE_Time_Counter__c +a.time_slot_calc__c;}
	            else{c.State_AssignedAE_Time_Counter__c = a.time_slot_calc__c;}        
	           }
	          
	           if(a.state__c=='Wait for Implementation'){      
	            if(a.State_Wait_for_Implementation_Time_Count__c!=NULL)
	            {c.State_Wait_for_Implementation_Time_Count__c= a.State_Wait_for_Implementation_Time_Count__c+a.time_slot_calc__c;}
	            else{c.State_Wait_for_Implementation_Time_Count__c= a.time_slot_calc__c;}        
	           }     
	   
	          if(a.state__c=='Waiting for Customer Info'){ 
	              
	      
	            if(a.State_WaitForCustInfo_Time_Counter__c!=NULL)       
	            {c.State_WaitForCustInfo_Time_Counter__c = a.State_WaitForCustInfo_Time_Counter__c +a.time_slot_calc__c;}
	                   
	            else{c.State_WaitForCustInfo_Time_Counter__c=a.time_slot_calc__c;}
	           }
	       
	          if(a.state__c=='Waiting for Customer Approval'){
	                
	            if(a.State_WaitForCustApp_Time_Counter__c!=NULL)  
	             {c.State_WaitForCustApp_Time_Counter__c = a.State_WaitForCustApp_Time_Counter__c +a.time_slot_calc__c;}
	            else{c.State_WaitForCustApp_Time_Counter__c = a.time_slot_calc__c;}
	            } 
	                     
	                
	          if(a.state__c=='Wait for Release'){            
	            if(a.State_WaitForRelease_Time_Counter__c!=NULL)          
	             { c.State_WaitForRelease_Time_Counter__c = a.State_WaitForRelease_Time_Counter__c +a.time_slot_calc__c;}
	          else{ c.State_WaitForRelease_Time_Counter__c = a.time_slot_calc__c;}
	          }                         
	                      
	          if(a.state__c=='On Hold'){            
	            if(a.State_OnHold_Time_Counter__c!=NULL)          
	             { c.State_OnHold_Time_Counter__c = a.State_OnHold_Time_Counter__c +a.time_slot_calc__c;}
	          else{ c.State_OnHold_Time_Counter__c = a.time_slot_calc__c;}
	          }
	          
	         if(a.state__c=='Closed'){            
	         if(a.State_Closed_Time_Counter__c!=NULL)          
	         { c.State_Closed_Time_Counter__c = a.State_Closed_Time_Counter__c +a.time_slot_calc__c;}
	         else{ c.State_Closed_Time_Counter__c = a.time_slot_calc__c;}   
	          }
	
	
	
	             c.state_update_time__c = system.now(); 
	       c.All_Int_Sates__c =(c.State_Open_time_counter__c + c.State_Assigned_Time_counter__c +         
	       c.State_AssignedAE_Time_Counter__c + c.State_OpenAE_Time_Counter__c +           
	       c.State_WaitForRelease_Time_Counter__c+ c.State_RMA_Assigned_Time_Counter__c)*24*60 ;      
	       }  //END OF if(c.state__c!=a.state__c)
       
       	if (c.OwnerId !=a.OwnerId && a.OwnerId == ENV.OFFHRqueue)
       	{if(a.Total_time_in_OFFHR_Queue__c!=NULL)          
	         { c.Total_time_in_OFFHR_Queue__c = a.Total_time_in_OFFHR_Queue__c +a.time_slot_calc__c;}
	         else{ c.Total_time_in_OFFHR_Queue__c = a.time_slot_calc__c;}
	     }
       
       }//END OF if(c.casenumber==a.casenumber)
     } 
   }
}
}