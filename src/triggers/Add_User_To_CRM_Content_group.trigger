trigger Add_User_To_CRM_Content_group on User (after insert, before update) {
   
     if (trigger.isAfter && trigger.isInsert) {
        List<id>  ConIds= new List<id>();   
        Map<id, contact> UserContacts = new Map<id, contact>(); 
        List<string> GroupNames =new List<String>{'Design NDA', 'System NDA', 'Non NDA', 'IPT NDA' , 'IPT 28G NDA', 'Ezchip NDA', 'Cisco OEM'};
        Map<string,group> UserGroup = new Map<string,group>();
        Profile design_In_Profile = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox Design In Community User'];
        Profile MyMlnx_Distributor_Profile = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox Distributor User'];
        Profile MyMlnx_Sales_Profile = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox Sales User'];
        Profile ezchip_design_In_Profile =  [SELECT Id, Name FROM Profile WHERE Name =: 'MyMellanox Ezchip Design In Community User'];

        for(User U:trigger.new){ConIds.add(U.Contactid);}
        
        for(Contact Con : [select id, CRM_Content_Permissions__c, Account.MyMellanox_DesignIn_Group__c from contact where id in :ConIds])
        {UserContacts.put(Con.id, Con);}
        
        for(Group Gr : [select id, name from Group where name in :GroupNames])
        {UserGroup.put(Gr.name, Gr);}
         
        list<GroupMember> GroupMember2Insert_List = new list<GroupMember>();
          
         for(User newUser : trigger.new) {
            
            system.debug('newUser.ProfileId : ' + newUser.ProfileId);

            if (newUser.ProfileId != MyMlnx_Distributor_Profile.Id && newUser.ProfileId != MyMlnx_Sales_Profile.Id) {
	             Id userGroupId;
	             boolean userIsDesignInGroup = false;
	             boolean userIsEzchipDesignInGroup = false;
	             
	             if(UserContacts.get(newUser.contactid)!=NULL) {
	              
	                 if (newUser.ProfileId == design_In_Profile.Id) {
	                    
	                    if (UserContacts.get(newUser.contactid).Account.MyMellanox_DesignIn_Group__c != null) {
	                        
	                        String accountDIGroup_Str = UserContacts.get(newUser.contactid).Account.MyMellanox_DesignIn_Group__c;
	                        
	                        if (accountDIGroup_Str.EqualsIgnoreCase('EMC')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxEMCDesignInId;
	                        }
	                        
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('Dell')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxDELLDesignInId;
	                        }
	                        
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('IBM')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxIBMDesignInId;
	                        }
	                        
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('Oracle')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxOracleDesignInId;
	                        }
	                        
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('HP')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxHPDesignInId;
	                        }
	                        
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('Intel')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxIntelDesignInId;
	                        }
	                        
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('LENOVO')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxLENOVODesignInId;
	                        } 
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('SGI')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxSGIDesignInId;
	                        }	                        
	                        else if (accountDIGroup_Str.EqualsIgnoreCase('Bull')) {
	                            
	                        //  userGroupId = MyMellanoxSettings.MyMellanoxEMCDesignInId;
	                        }
	                        
	                        userIsDesignInGroup = true;
	                    }
	                 }
	                 
	                 else if (newUser.ProfileId == ezchip_design_In_Profile.Id) {
	                    
	                    if (UserContacts.get(newUser.contactid).Account.MyMellanox_DesignIn_Group__c != null) {
	                        
	                        String accountDIGroup_Str = UserContacts.get(newUser.contactid).Account.MyMellanox_DesignIn_Group__c;
	                        
	                        if (accountDIGroup_Str.EqualsIgnoreCase('Cisco')) {
	                            
	                            userGroupId = MyMellanoxSettings.MyMellanoxCiscoDesignInId;
	                            userIsEzchipDesignInGroup = true;
	                        }                      
	                        
	                    }
	                 }
	                 
	                 String AssignedGroup =UserContacts.get(newUser.contactid).CRM_Content_Permissions__c;
	                
	                 for(String s:GroupNames) {
	                 
	                    if(UserContacts.get(newUser.contactid).CRM_Content_Permissions__c==s) {
	                    
	                        if(UserGroup.get(s)!=NULL) {
	                            
	                            GroupMember ContentMember = new GroupMember();
	                            
	                            if (userIsDesignInGroup) {
	                                
	                                ContentMember.GroupId = userGroupId;
	                                system.debug('userGroup: DesignIn' + userGroupId);
	                            }
	                            else if (userIsEzchipDesignInGroup){
	                            	ContentMember.GroupId = userGroupId;
	                            	system.debug('userGroup: Ezchip DesignIn' + userGroupId);
	                            }
	                            else {
	                                
	                                ContentMember.GroupId = UserGroup.get(s).id; 
	                                system.debug('userGroupId 136 :' + UserGroup.get(s).id);
	                            }  
	                            
	                            ContentMember.UserOrGroupId = newUser.id;
	            				system.debug('UserOrGroupId 139 :' + newUser.id);
	            				
	                            if (ENV.createGroupMember) {
	                           
	                                if (ContentMember.Id == null)
	                                {
	                                    GroupMember2Insert_List.add(ContentMember);
	                                }
	                                
	                            }
	                        }
	                    }
	                 }
	                 
	                 userIsDesignInGroup = false;
	                 userIsEzchipDesignInGroup = false;
	             }
	             
	             if (!GroupMember2Insert_List.isEmpty()) {
	                
	              insert GroupMember2Insert_List;
	                 
	             }
            }
         }
     }
     
      if (trigger.isBefore && trigger.isUpdate) {
        
        User_trg handler = new User_trg();
        handler.deleteUsersRelatedContentUpdates(trigger.New, trigger.OldMap);
     }
     
}