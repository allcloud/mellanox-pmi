trigger PCN_Products_ApproveByOPN on PCN_Affected_Products__c (before insert) {
	ID PCN_ID;
	String PCNName;
	String companyname;
	List<PCN_Affected_Products__c> lst_PCN_AffectedProducts = new List<PCN_Affected_Products__c>();
	Set<String> products = new Set<String>();
	List<PCN_Approver__c> lst_approvers = new List<PCN_Approver__c>();
	Map<String,PCN_Approver__c> map_OPN_PCNApprover = new Map<String,PCN_Approver__c>(); 
	List<Contact> cts;
	boolean exist_IBM_Strg_XIV = false;
	List<PCN__c> pcns_update = new List<PCN__c>();
	
	if (Trigger.isInsert) {
		PCN_ID = Trigger.new[0].PCN__c;
		PCNName = Trigger.new[0].PCN_Name__c;
		//For now , just IBM PCN only
		if (PCNName.containsIgnoreCase('IBM') || PCNName.containsIgnoreCase('XIV')) {
			companyname='IBM';
		}
		else if (PCNName.containsIgnoreCase('Lenovo')) {
			companyname='LENOVO';
		}
		else if (PCNName.containsIgnoreCase('EMC')) {
			companyname='EMC';
		}
		else if (PCNName.containsIgnoreCase('DELL')) {
			companyname='DELL';
		} 
		else if (PCNName.containsIgnoreCase('ISILON')) {
			//companyname='EMC - ISILON';
			companyname='ISILON';
		}
		for (PCN_Affected_Products__c p : Trigger.New){
			if(p.PCN_Approver_by_OPN__c == true) {
				if( !String.isBlank(p.OPN_Name_Bulk_load__c))
					products.add(p.OPN_Name_Bulk_load__c);
				else
					products.add(p.Product_Name__c);
				lst_PCN_AffectedProducts.add(p);
			}
		}
		if (products.size() == 0)
			return;
		Set<String> contacts_not_in_map = new Set<String>();
		List<String> lst_contacts = new List<String>();
		
		Map<String,List<String>> map_group_notificationContacts = new Map<String,List<String>>();
		for (PCN_Approver_Map__c pp : PCN_Approver_Map__c.getall().values()) {
			if ( pp.Approver__c != null && !Quote_ENV.Map_Contacts_ID.containskey(pp.Approver__c.trim()) ) {
				contacts_not_in_map.add(pp.Approver__c.trim());
			}
			if(pp.Notification__c != null){
				lst_contacts = pp.Notification__c.split(',');
				for (String ss : lst_contacts){
					if( !Quote_ENV.Map_Contacts_ID.containsKey(ss.trim()) )
						contacts_not_in_map.add(ss.trim());	
				}
				map_group_notificationContacts.put(pp.Name,lst_contacts);
			}
		}
		Map<String,ID> map_contacts_not_in_map = new Map<String,ID>();
		if(contacts_not_in_map.size() > 0){
			if(companyname == 'EMC' || companyname == 'ISILON'){
				cts = new List<Contact>([select id, name from Contact where Name in :contacts_not_in_map and (Account_Name_text__c='EMC' OR Account_Name_text__c='EMC - ISILON')]);
			}
			else
				cts = new List<Contact>([select id, name from Contact where Name in :contacts_not_in_map and Account_Name_text__c=:companyname]);
			for (Contact cc : cts){
				map_contacts_not_in_map.put(cc.Name,cc.ID);	
			}
		}
		//IBM
		if (PCNName != null && companyname=='IBM'){
			lst_approvers = [select OPN__c, Group__c from PCN_Approver__c where Company__c=:companyname and OPN__c in :products];
			for (PCN_Approver__c pp : lst_approvers) {
				map_OPN_PCNApprover.put(pp.OPN__c, pp);
			}
		}
		//EMC
		if (PCNName != null && (companyname=='EMC' || companyname=='ISILON')){
			lst_approvers = [select OPN__c, Group__c from PCN_Approver__c where OPN__c in :products and (Company__c='EMC' or Company__c='EMC - ISILON')];
			for (PCN_Approver__c pp : lst_approvers) {
				map_OPN_PCNApprover.put(pp.OPN__c, pp);
			}
		}
		//LENOVO or DELL
		if (PCNName != null && (companyname=='LENOVO' || companyname=='DELL') ){
			lst_approvers = [select OPN__c, Group__c from PCN_Approver__c where Company__c=:companyname and OPN__c in :products];
			for (PCN_Approver__c pp : lst_approvers) {
				map_OPN_PCNApprover.put(pp.OPN__c, pp);
			}
		}
		//
		List<PCN_Contacts__c> pcn_contacts = new List<PCN_Contacts__c>();
		
		PCN_Approver_Map__c tmp_pcn_approver;
		List<String> lst_contacts_notification = new List<String>();
				
		for (PCN_Affected_Products__c p : lst_PCN_AffectedProducts) {
			if (map_OPN_PCNApprover.get(p.Product_Name__c) != null || map_OPN_PCNApprover.get(p.OPN_Name_Bulk_load__c) != null ) {
				if(map_OPN_PCNApprover.get(p.Product_Name__c) != null)
					p.Product_Group_ApproveByOPN__c = map_OPN_PCNApprover.get(p.Product_Name__c).Group__c;
				else if (map_OPN_PCNApprover.get(p.OPN_Name_Bulk_load__c) != null )
					p.Product_Group_ApproveByOPN__c = map_OPN_PCNApprover.get(p.OPN_Name_Bulk_load__c).Group__c;
					
				tmp_pcn_approver = PCN_Approver_Map__c.getvalues(p.Product_Group_ApproveByOPN__c);
				//KN added 10-19-15 detect exist_IBM_Strg_XIV = false;
				if (p.Product_Group_ApproveByOPN__c == 'IBM_Strg_XIV' || p.Product_Group_ApproveByOPN__c == 'IBM-Strg-XIV')
					exist_IBM_Strg_XIV = true;
				if(tmp_pcn_approver != null && tmp_pcn_approver.Approver__c != null ) {
					if(Quote_ENV.Map_Contacts_ID.containsKey(tmp_pcn_approver.Approver__c))
						p.Approver_By_OPN__c = Quote_ENV.Map_Contacts_ID.get(tmp_pcn_approver.Approver__c);
					else if (map_contacts_not_in_map.containsKey(tmp_pcn_approver.Approver__c))
						p.Approver_By_OPN__c = map_contacts_not_in_map.get(tmp_pcn_approver.Approver__c);
				}
			}
			//Add approver first
			if(p.Approver_By_OPN__c != null) {
				if( !String.isBlank(p.Product_Name__c))
					pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=p.Approver_By_OPN__c,Group_ApprovedByOPN__c=p.Product_Group_ApproveByOPN__c,
														Product__c=p.Product_Name__c,Company__c=companyname,is_Approver_by_OPN__c=true));
				else if ( !String.isBlank(p.OPN_Name_Bulk_load__c))
					pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=p.Approver_By_OPN__c,Group_ApprovedByOPN__c=p.Product_Group_ApproveByOPN__c,
														Product__c=p.OPN_Name_Bulk_load__c,Company__c=companyname,is_Approver_by_OPN__c=true)); 													
			}
			//
			if(p.Product_Group_ApproveByOPN__c != null ){
				lst_contacts_notification = map_group_notificationContacts.get(p.Product_Group_ApproveByOPN__c);
				if(lst_contacts_notification != null) {						
					for (String s : lst_contacts_notification) {
						s = s.trim();
						if(Quote_ENV.Map_Contacts_ID.containsKey(s)) {
							if( !String.isBlank(p.Product_Name__c))
								pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=Quote_ENV.Map_Contacts_ID.get(s),Group_ApprovedByOPN__c=p.Product_Group_ApproveByOPN__c,
																	Product__c=p.Product_Name__c,Company__c=companyname));
							else if ( !String.isBlank(p.OPN_Name_Bulk_load__c))
								pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=Quote_ENV.Map_Contacts_ID.get(s),Group_ApprovedByOPN__c=p.Product_Group_ApproveByOPN__c,
																	Product__c=p.OPN_Name_Bulk_load__c,Company__c=companyname));																									
						}																			
						else if( map_contacts_not_in_map.containsKey(s) ) {
							if( !String.isBlank(p.Product_Name__c))
								pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=map_contacts_not_in_map.get(s),Group_ApprovedByOPN__c=p.Product_Group_ApproveByOPN__c,
																	Product__c=p.Product_Name__c,Company__c=companyname));
							else if ( !String.isBlank(p.OPN_Name_Bulk_load__c))
								pcn_contacts.add (new PCN_Contacts__c(PCN__c=PCN_ID,Contact__c=map_contacts_not_in_map.get(s),Group_ApprovedByOPN__c=p.Product_Group_ApproveByOPN__c,
																	Product__c=p.OPN_Name_Bulk_load__c,Company__c=companyname));																	
						}																		
					}
				}
			}
		} //end for loop
		if(exist_IBM_Strg_XIV == true) {
			pcns_update = [select id, Additional_Contact_3__c,Additional_Contact_4__c from PCN__c where id = :PCN_ID];
			for (PCN__c p : pcns_update) {
				p.Additional_Contact_3__c = '0035000000rlNIM'; //Nir Gal
				p.Additional_Contact_4__c = '0035000000gp9WZ'; //Erez C
			}
			if (pcns_update != null && pcns_update.size() > 0)
				Database.update(pcns_update, false);
		}
		if (pcn_contacts.size()>0)
			insert pcn_contacts;
	} //END if (Trigger.isInsert)		
}