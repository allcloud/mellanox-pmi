trigger contract_after_insert_update on Contract2__c (after insert, after update) {

List<contract2__c> UpdContracts = new List<contract2__c>();
List<string> OracleNames = new List<string>();
map<string,id> OracleToId = new map<string,id>();


for(contract2__c cnt:trigger.new)
{
 if(trigger.isinsert || ( trigger.isupdate && cnt.Renewed_Contract_text__c != trigger.oldmap.get(cnt.id).Renewed_Contract_text__c))
 { OracleNames.add(cnt.Renewed_Contract_text__c);
   OracleToID.put(cnt.Renewed_Contract_text__c,cnt.id);
  }
}

if(!OracleNames.isempty())
{UpdContracts= [select id,Contract_Renew_RollUp_to__c,Contract_Number__c, Status__c  from contract2__c where Contract_Name_final__c in :OracleNames];}
//{UpdContracts= [select id,Contract_Renew_RollUp_to__c,Contract_Number__c, Status__c  from contract2__c where Oracle_Contract_Name__c in :OracleNames];}

if(!UpdContracts.isempty()) 
{
  for(contract2__c cnt1:UpdContracts)
   {
    cnt1.Contract_Renew_RollUp_to__c = OracleToID.get(cnt1.Contract_Number__c);     
    cnt1.Status__c = 'Renewed / Roll Up';
   }
 update(UpdContracts);  
   
}  

if (trigger.isAfter && trigger.isUpdate) {
	
		cls_trg_Contract handler = new cls_trg_Contract();
		handler.deleteRealtedOppsAndQuotesWhenPendingPOUnChecked(trigger.New, trigger.oldMap);
}
	
}