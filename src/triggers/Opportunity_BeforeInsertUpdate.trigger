trigger Opportunity_BeforeInsertUpdate on Opportunity ( before insert, before update) {
/*integer flag = 0;
List<id> SaleDirs = new List<id>();
Map<id,string> OwnerId_VTReg = new Map<id,string>();
for(Opportunity Opp:trigger.new)
{   if(trigger.isinsert || (trigger.isupdate && Opp.Ownerid != trigger.oldmap.get(Opp.id).Ownerid))
    {SaleDirs.add(Opp.Ownerid);}
}

if(SaleDirs.size() >0)
{
    for(US_Territory_Map__c Tmap: [select Sales_Director__c, VT__c from US_Territory_Map__c where sales_director__c in :SaleDirs])
     { 
       OwnerId_VTReg.put(Tmap.Sales_Director__c,Tmap.VT__c);
     }
     
 }

for(Opportunity Opp:trigger.new)
  {
   if(trigger.isinsert  || (trigger.isupdate && Opp.Ownerid != trigger.oldmap.get(Opp.id).Ownerid)) 
    {
            if(OwnerId_VTReg.containsKey(Opp.Ownerid)==TRUE)
      { 
       
       Opp.Territory__c = OwnerID_VTReg.get(Opp.Ownerid);   
      }
       else         
       {
         Opp.adderror('The Opportunity Owner is not assigned to any region, and can not open opportunity. Please contact the administrator');
       }
      }
 
   //Matching duplicate accounts key
    String canonical = Opp.Name.toUpperCase();                    
    Blob bsig = Crypto.generateDigest('MD5', Blob.valueOf(canonical));
    Opp.OpportunityCode__c = encodingUtil.convertToHex (bsig);
    
  
    
    
   
    
      
        
  }*/ 
  
	for (Opportunity op : Trigger.new)
    {
    	if(trigger.isinsert)
       	{
        	
        	if (op.RecordTypeId == ENV.map_opportunityRecordTypeTOID.get('End_User_Opportunity'))
        	{
        		System.debug('The Price book is set to  be standard price book' + ENV.PriceBookMap.get('Standard Price Book'));
        		op.Pricebook2Id = ENV.PriceBookMap.get('Standard Price Book');
        	}
		}
    }
}