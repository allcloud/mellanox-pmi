trigger Case_Create_Contact_from_web on Case ( after insert) 
{
    List<Contact> NewContacts = new List<Contact>();
    List<id> NewCases = new List<id>();
    Map<Id,Case> UpdCases = new Map<Id,Case>();
    List<string> SupEmails = new List<string>();
    Map<string,id> ExistDupContact = new Map<string,id>();
    String Domain;
    String Domain1;
    String PathCode;

    List<Account> RelAccount = new List<Account>();
    for(Case c:trigger.new)
        {NewCases.add(c.id);
        if(c.Unknown_contact__c == 'Yes')
         SupEmails.add(c.SuppliedEmail);}
    
    for(case cs:[select id, contactid from case where id in :NewCases])
        {UpdCases.put(cs.id,cs);}        
        
  if(!SupEmails.isempty())   
  { for(Contact con:[select id,email, accountId from contact where email in:SupEmails order by account.has_contract__c asc])
       ExistDupContact.put(con.email,con.id);
   }     
    
    for(Case cc:trigger.new)
    { 
        //if(cc.contact == NULL && cc.SuppliedEmail!= NULL)
        if(cc.Unknown_contact__c == 'Yes')   
        {
         if( ExistDupContact.containsKey(cc.SuppliedEmail))
         {
            
           UpdCases.get(cc.id).contactId = ExistDupContact.get(cc.SuppliedEmail); 
           update(UpdCases.get(cc.id)); 
                
            
         }else
          {                                                                                                
            Contact new_contact = new Contact();
            if(cc.SuppliedName !=NULL)
            {    List<string> Name = cc.SuppliedName.split(' ', 2);
                  new_contact.FirstName = Name[0];
                  if(Name.size()>1) 
                  {new_contact.LastName = Name[1];}   
                  else 
                  {new_contact.LastName = 'Not Supplied';}   
                                   
            }
             else
             {  new_contact.FirstName ='No';
                new_contact.LastName ='Name'; 
             }                                     
             new_contact.email = cc.SuppliedEmail;
             new_contact.company_name_from_web__c = cc.SuppliedCompany;              
             Domain = cc.Web_domain__c;             
             Domain1 = cc.Web_Domain_Com__c; 
             if(cc.subject.toLowerCase().startsWith('cnt'))
             {
                PathCode = cc.subject.toLowerCase().substring(0,13).trim(); 
                RelAccount =[select id from Account where   Path_Code__c = :PathCode];
             }
                
             system.debug('Domain =' + Domain);
            // RelAccount =[select id from Account where Has_Contract__c = 'Yes' AND (lower_domain__c = :Domain OR lower_domain1__c = :Domain OR lower_domain2__c =:Domain)];  

              if(RelAccount.isempty()) 
              RelAccount =[select id from Account where (lower_domain__c = :Domain OR lower_domain1__c = :Domain OR lower_domain2__c =:Domain or   lower_domain__c = :Domain1 OR lower_domain1__c = :Domain1 OR lower_domain2__c =:Domain1) ORDER BY Primary_Domain__c DESC NULLS LAST, Highest_Active_contract_rank__c  DESC NULLS LAST];  

     
       
       if(!RelAccount.isempty())
             {new_contact.accountId = RelAccount[0].id ;}
              else
               { 
                 if(Domain.contains('ibm') || Domain1.contains('ibm'))
                 {new_contact.accountId  =ENV.IBMAccount; }
                 
                 else
                    new_contact.accountId = ENV.UnknownAccount ;}
                                    
     
             NewContacts.add(new_contact) ;
           
             insert( new_contact);      
           
             UpdCases.get(cc.id).contactId = new_contact.id; 
             update(UpdCases.get(cc.id));                                             
        } // else
      }  // If unknown contact
    }// for 
}