//Copyright (c) 2012, Rima Rohana , BLAT-LAPIDOT
//All rights reserved.
//
 
//
// History
//
// Version    Date          Author               Comments / updates
// 2.0.0      14/10/13      Rima Rohana          INC0033341 - 
//                                               a.Before going on vacation, AE/PM adds in profile name of one ( or + ) followers. Followers need to be of AE/PM profile
//                                               b.Follower will be added to case team of all unclosed cases of the absent AE/PM, and to the cases in AE Q for which the absent Ae is Escalation Q Owner
//                                               c.The AE Engineer field does no change though automatically
//                                               d.Once AE/PM comes back, he deletes the follower, and these are removed from case team.

//
 
//
// Overview / Description
// The trigger is fired whenever there is an update on a User.
 
// A- User.
// B- Follow up on user A.
 
// The trigger checks two different situations:
// The field of Follow up my cases was changed, and does not equal to Null after the change.
// The trigger updates all not closed cases of which A is the owner, and adds B to be in the Case team with "Follow Up" role.
// The Follow up user (B) was changed to be someone else.
// The previous follow up user is removed from all not closed cases (if he has the "Follow Up" role).

//


trigger Case_Follow_up on User (after update) 
{
    set<Id> set_NewFollowers = new set<Id>();       //hold the new followers Id of the user
    set<Id> set_OldFollowers = new set<Id>();       //hold the old followers Id of the user
    set<Id> set_UsersOOO     = new set<Id>();       //hold all the user that update their followers fields
    set<Id> set_FollwersNotChange           ;       //hold the follwer with no change (they was members in the case team and still)
    set<Id> set_UpdateTeamMemIds            ;       //hold the ids of the members that their role need to be updated 
    list<Case> lst_Cases                     = new list<Case>();            //hold the cases that will effect by the user updating his followers
    list<CaseTeamMember> lst_ctmToAdd        = new list<CaseTeamMember>();  //hold list of all the follwers need to be add
    list<CaseTeamMember> lst_ctmToRemove     = new list<CaseTeamMember>();  //hold list of all the follwers need to be remove
    list<CaseTeamMember> lst_ctmToUpdate     = new list<CaseTeamMember>();  //hold list of all the follwers need to be update role 
    
    
    
    for(user u:trigger.new)
    {
        if(trigger.oldmap.get(u.id).follow_up1__c != u.follow_up1__c )
        {
            if(u.follow_up1__c != null)
                set_NewFollowers.add(u.follow_up1__c );
                
            if(trigger.oldmap.get(u.id).follow_up1__c != null)
                set_OldFollowers.add(trigger.oldmap.get(u.id).follow_up1__c);
                
            set_UsersOOO.add(u.Id);
            
        }
        if(trigger.oldmap.get(u.id).follow_up2__c != u.follow_up2__c )
        {
            if(u.follow_up2__c != null)
                set_NewFollowers.add(u.follow_up2__c );
                
            if(trigger.oldmap.get(u.id).follow_up2__c != null)  
                set_OldFollowers.add(trigger.oldmap.get(u.id).follow_up2__c);
                
            set_UsersOOO.add(u.Id);
        }
        if(trigger.oldmap.get(u.id).follow_up3__c != u.follow_up3__c )
        {
            if(u.follow_up3__c != null)
                set_NewFollowers.add(u.follow_up3__c );
            
            if(trigger.oldmap.get(u.id).follow_up3__c != null)  
                set_OldFollowers.add(trigger.oldmap.get(u.id).follow_up3__c);
                
            set_UsersOOO.add(u.Id);
        }
        
    }
    system.debug('==>set_UsersOOO '+set_UsersOOO);
    system.debug('==>set_OldFollowers '+set_OldFollowers);
    system.debug('==>set_NewFollowers '+set_NewFollowers);
    
    if(!set_UsersOOO.IsEmpty())
    {
        lst_Cases =[select id , Account.Default_Follower__c, (Select Id, ParentId, MemberId, TeamRoleId  From TeamMembers) 
                    From Case 
                    Where (ownerid in:set_UsersOOO AND state__c != 'Closed') 
                    OR  (AE_Engineer__c  in:set_UsersOOO AND state__c != 'Closed' AND state__c != 'Open AE') 
                    OR  ( ( Escalation_Q_Owner__c  in:set_UsersOOO OR Escalation_Q_Owner2__c  in:set_UsersOOO) AND state__c = 'Open AE')];
    }
    
    for(Case cs: lst_Cases)
    {
        set_FollwersNotChange = new set<Id>();
        set_UpdateTeamMemIds  = new set<Id>();
        for(CaseTeamMember ctm :cs.TeamMembers)
        {
            // the case team member of role follwer
            
                if( ctm.TeamRoleId == ENV.CaseTEamFollower1 || ctm.TeamRoleId == ENV.CaseTEamFollower2 || ctm.TeamRoleId == ENV.CaseTEamFollower3)
                {
                    //scenario 1: the follower was and still after user update his folowers
                    if(set_OldFollowers.contains(ctm.MemberId) && set_NewFollowers.contains(ctm.MemberId))
                    {
                       
                       if (cs.Account.Default_Follower__c != null) {
                       	
                       	   set_FollwersNotChange.add(cs.Account.Default_Follower__c);
                       }
                       
                       else {
                       		 
                       		 set_FollwersNotChange.add(ctm.MemberId);
                       }
                    }
                    else
                    {
                        //scenario 2: the follower has role 1 
                        if(ctm.TeamRoleId == ENV.CaseTEamFollower1)
                        {
                            // 1.1 the follower removed
                            if( (set_OldFollowers.contains(ctm.MemberId) || (cs.Account.Default_Follower__c != null && ctm.MemberId == cs.Account.Default_Follower__c))  && !set_NewFollowers.contains(ctm.MemberId) ) {
                                
                                lst_ctmToRemove.add(ctm);
                            }
                                
                            // 1.2 the follower added by second user    
                            else if( !set_OldFollowers.contains(ctm.MemberId) && (set_NewFollowers.contains(ctm.MemberId) || (cs.Account.Default_Follower__c != null && ctm.MemberId == cs.Account.Default_Follower__c)))
                            {
                                ctm.TeamRoleId  = ENV.CaseTEamFollower2;
                                lst_ctmToUpdate.add(ctm);
                                set_UpdateTeamMemIds.add(ctm.MemberId);
                            }
                        }
                        
                        //scenario 3: the follower has role r or 3
                        else 
                        {
                            // 1.1 the follower removed
                            if( ( set_OldFollowers.contains(ctm.MemberId) || (cs.Account.Default_Follower__c != null && cs.Account.Default_Follower__c == ctm.MemberId) ) && !set_NewFollowers.contains(ctm.MemberId) )
                            {
                                if(ctm.TeamRoleId == ENV.CaseTEamFollower2)
                                    ctm.TeamRoleId  = ENV.CaseTEamFollower1;
                                else
                                    ctm.TeamRoleId  = ENV.CaseTEamFollower2;
                                    
                                lst_ctmToUpdate.add(ctm);
                                set_UpdateTeamMemIds.add(ctm.MemberId);
                            }
                            
                            // 1.2 the follower added by another user   
                            else if( !set_OldFollowers.contains(ctm.MemberId) && (set_NewFollowers.contains(ctm.MemberId) || (cs.Account.Default_Follower__c != null && cs.Account.Default_Follower__c == ctm.MemberId)) )
                            {
                                if(ctm.TeamRoleId == ENV.CaseTEamFollower2)
                                    ctm.TeamRoleId  = ENV.CaseTEamFollower3;
                                else
                                    set_FollwersNotChange.add(ctm.MemberId);
                                    
                                lst_ctmToUpdate.add(ctm);
                                set_UpdateTeamMemIds.add(ctm.MemberId);
                            }
                        }
                    }
                }
                //scenario 4 the new follower already exsit in the case team memeber but not as role follower - do nothing with it 
                else if( set_NewFollowers.contains(ctm.MemberId) || ((cs.Account.Default_Follower__c != null && cs.Account.Default_Follower__c == ctm.MemberId)))
                {
                    set_FollwersNotChange.add(ctm.MemberId);
                }
            
        }
        
        system.debug('==>set_FollwersNotChange '+set_FollwersNotChange);
        system.debug('==>set_UpdateTeamMemIds  '+set_UpdateTeamMemIds);
        
        //create new case team members where he is not one of the update members and not one of the not change members
        for(Id nct: set_NewFollowers)
        {
            if( !(set_FollwersNotChange.contains(nct) || set_UpdateTeamMemIds.contains(nct) ) )
            {
	            
	            CaseTeamMember Newctm;
	            
	            if (cs.Account.Default_Follower__c != null) {
	            	
	            	Newctm = new CaseTeamMember(ParentId = cs.Id, MemberId = cs.Account.Default_Follower__c, TeamRoleId = ENV.CaseTEamFollower1  );
	            }
                	
                else {	
                	
                	 Newctm = new CaseTeamMember(ParentId = cs.Id, MemberId = nct, TeamRoleId = ENV.CaseTEamFollower1  );
                }
                
	            lst_ctmToAdd.add(Newctm);
            }
        }
    }
    
    //update members
    system.debug('==>lst_ctmToRemove '+lst_ctmToRemove);
    if(!lst_ctmToUpdate.IsEmpty())
    {
        update lst_ctmToUpdate ;
    }
    
    //delete members
    system.debug('==>lst_ctmToRemove '+lst_ctmToRemove);
    if(!lst_ctmToRemove.IsEmpty())
    {
        delete lst_ctmToRemove;
    } 
    
    //insert members
    system.debug('==>lst_ctmToAdd '+lst_ctmToAdd);
    if(!lst_ctmToAdd.IsEmpty())
    {
        DataBase.SaveResult[] rs = DataBase.Insert(lst_ctmToAdd, false); 
        
        for (DataBase.SaveResult result : rs) {
        	
        	if (!result.isSuccess()) {
        		
        		system.debug('ERROR : ' + result.getErrors());
        	}
        }
    }
}
    
    

/*
trigger Case_Follow_up on User (after update) 
{
    
    Set<string> updUsr      = new Set<string>();
    Set<string> RemoveTmUsr = new Set<string>();
    List<string> Owner      = new List<string>();
    List<case> Cases        = new List<case>();
    set<id> CasesId         = new set<id>();
    Set<string> Members     = new Set<string>();
    List<CaseTeamMember> TMtoRemove        = new  List<CaseTeamMember>();
    List<CaseTeamMember> AllTeamMembers    = new  List<CaseTeamMember>();
    List<CaseTeamMember> insCaseTeamMember = new  List<CaseTeamMember>();
    Map<id,Set<string>> Case_TeamMembers   = new  Map<id,Set<string>> ();
    
    
    for(user u:trigger.new)
    {
    
        double is_follow_user = 0;
        
        if(trigger.oldmap.get(u.id).follow_up1__c != u.follow_up1__c )
        {
            if(u.follow_up1__c != NULL){ updUsr.add(u.follow_up1__c);}
            if(trigger.oldmap.get(u.id).follow_up1__c != NULL){ RemoveTmUsr.add(trigger.oldmap.get(u.id).follow_up1__c );}
            is_follow_user =1;
        }
        
        if(trigger.oldmap.get(u.id).follow_up2__c != u.follow_up2__c )
        {
            if(u.follow_up2__c != NULL){ updUsr.add(u.follow_up2__c);}
            if(trigger.oldmap.get(u.id).follow_up2__c != NULL){ RemoveTmUsr.add(trigger.oldmap.get(u.id).follow_up2__c );}
            is_follow_user =1;
        }
        
        if(trigger.oldmap.get(u.id).follow_up3__c != u.follow_up3__c )
        {  
            if(u.follow_up3__c != NULL) {updUsr.add(u.follow_up3__c);} 
            if(trigger.oldmap.get(u.id).follow_up3__c != NULL) {RemoveTmUsr.add(trigger.oldmap.get(u.id).follow_up3__c);}
            is_follow_user =1;
        }
        
        if(is_follow_user==1)
        {
            owner.add(u.id);
        }
    
    }
    
    if(!owner.isempty())
    {
        cases =[select id 
                From Case 
                Where (ownerid in:Owner AND state__c != 'Closed') ];
                //OR  (AE_Engineer__c  in:Owner AND state__c != 'Closed' AND state__c != 'Open AE') 
                //OR ( ( Escalation_Q_Owner__c  in:Owner OR Escalation_Q_Owner2__c  in:Owner) AND state__c = 'Open AE')
        
        system.debug('==>test cases test'+cases)    ;
        system.debug('==>test cases test size'+cases.size());   
        for(case cs:cases)
        { 
            CasesId.add(cs.id);
        }
    }
    
    system.debug('AllCases - '+ cases);
    system.debug('UpdUsr - '+ UpdUsr);
    
    AllTeamMembers = [Select MemberId, TeamRoleId,ParentId From CaseTeamMember Where ParentId in :CasesId];
    system.debug('AllTeamMembers - '+ AllTeamMembers);
    
    for(CaseTeamMember TM:AllTeamMembers)
    {  
        if(updUsr.Contains(TM.MemberId))
        {
            if(!Case_TeamMembers.containskey(TM.ParentId))
            {  
                Members.addAll(updUsr);
                Members.remove(TM.MemberId);   
                Case_TeamMembers.put(TM.ParentId,Members);
                system.debug('Populating1: - '+ Case_TeamMembers.get(TM.ParentId) + ' FROM CASE: ' + TM.ParentId);
            }
            else
            {
                Members = Case_TeamMembers.get(TM.ParentId);    
                Members.remove(TM.MemberId);    
                Case_TeamMembers.put(TM.ParentId,Members);
                system.debug('Populating2: - '+ Members + ' FROM CASE: ' + TM.ParentId);
            }
        } //if caseTeamUser is in FollowUp        
    } //for
    
    Set<CaseTeamMember> Check_list   = new Set<CaseTeamMember>();      
    for(Case UpdCs: Cases)
    {
        if(Case_TeamMembers.containsKey(UpdCs.id))
        {   
            system.debug('UPD_CASE #' + UpdCs.id+ '  Case_TeamMembers -'+ Case_TeamMembers.get(UpdCs.id));
                                             
            for(String TM:Case_TeamMembers.get(UpdCs.id))
            {
                CaseTeamMember ctmNew = new CaseTeamMember();
                ctmNew.ParentId = UpdCs.Id;
                ctmNew.MemberId = TM;
                system.debug('blat ctmNew.MemberId '+ctmNew.MemberId);
                system.debug('blat newCase.Id '+UpdCs.Id);
                ctmNew.TeamRoleId = ENV.teamRoleMap.get('Follow Up');
                insCaseTeamMember.add(ctmNew);
                
            }
        }
        else
        {
            for(String TM1:updUsr)
            {
                CaseTeamMember ctmNew = new CaseTeamMember();
                ctmNew.ParentId = UpdCs.Id;
                ctmNew.MemberId = TM1;
                ctmNew.TeamRoleId = ENV.teamRoleMap.get('Follow Up');
                insCaseTeamMember.add(ctmNew);
                
            }
        }
    }
    
    for(CaseTeamMember CTM:AllTeamMembers)
    {
        if(RemoveTmUsr.contains(CTM.MemberId) && CTM.TeamRoleID == ENV.teamRoleMap.get('Follow Up'))
        { TMtoRemove.add(CTM);}
    }  
    
    system.debug('===>TMtoRemove '+TMtoRemove);
    if(!TMtoRemove.isempty())
    {
        delete TMtoRemove;
    } 
    
    
    if(insCaseTeamMember.size()>0)
    {
        system.debug('===>insCaseTeamMember '+ insCaseTeamMember);
        insert insCaseTeamMember;
        
    }
    
     
    
}*/