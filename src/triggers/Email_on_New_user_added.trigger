trigger Email_on_New_user_added on User (after insert) {
 for( User NewUser:trigger.new){
     List<Profile> Pr = [Select Id, Name From Profile Where Id=:NewUser.ProfileId];
     if(Pr[0].Name == 'System Support Customer'  || Pr[0].Name == 'Design Support Customer')
{
  //List<EmailTemplate> et = [Select Id From EmailTemplate Where DeveloperName='SUPPORT: New User Created Notification'];
  Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
  String[] toAddresses = new String[]{'innag@mellanox.co.il','hani@mellanox.co.il', 'evgeny@mellanox.co.il'};
   mail.setToAddresses(toAddresses);
 //mail.setTemplateId('00XS0000000QNZ9');
 //mail.setTargetObjectId('005S0000000EyF0'); //005S0000000EyF0    
  mail.SetSubject( 'New User was created');       
   mail.SetPlainTextBody('*** NEW USER CREATED ALERT *** \n \n New Customer Portal user was created \n Please'+    
  ' grant him access permissions by assigning him to relevant public groups \n To access the user click on the link: \n  replaced with :  https://mellanox.my.salesforce.com/'+ NewUser.Id);      
   
  /* TEST:  mail.SetPlainTextBody('*** NEW USER CREATED ALERT *** \n \n New Customer Portal user was created \n Please'+    
  ' grant him access permissions by assigning him to relevant public groups \n To access the user click on the link: \n  https://cs1.salesforce.com/'+ NewUser.Id);      
   */    
  mail.setReplyTo('innag@mellanox.co.il');
  mail.setSenderDisplayName('Mellanox Support Admin');
  mail.setSaveAsActivity(false);
  mail.setBccSender(false);
  mail.setUseSignature(false);       
  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail}); 
}

 }
}