trigger FMDiscussion_BeforeInsertUpdate on FM_Discussion__c (before insert,before update) 
{
    system.debug('BLAT FM Util.CaseCommentCreated: ' + Util.CaseCommentCreated);
    if(!Util.CaseCommentCreated)
    {
        List<ID> lst_CaseId = new List<ID>();
        Map<Integer,CaseComment> insCaseComments = new Map<Integer,CaseComment>();
        Map<Integer,FM_Discussion__c> currFMDiscussion = new Map<Integer,FM_Discussion__c>();
        List<CaseComment> delCaseComments = new List<CaseComment>();
        Integer CCInd=0;
        list<FM_Discussion__c> fm2Update_List = new list<FM_Discussion__c>();
        
        set<String> caseCommentsIds_Set= new set<String>();
        
        for(FM_Discussion__c fmd : Trigger.new)
        {
            lst_CaseId.add(fmd.Case__c);
            if(fmd.Public__c && (Trigger.isInsert || (Trigger.isUpdate && fmd.Public__c!=Trigger.oldMap.get(fmd.Id).Public__c)))
            {
                fmd.comment_target__c= 'External Comment';
               
             
                system.debug('inside 28');
                insCaseComments = createCaseCommentsLimitBody(fmd.Discussion__c, fmd, CCInd);
                
                if(!insCaseComments.isEmpty()) {
            
                     Util.CaseCommentCreated=true;
                     
                     list<CaseComment> caseComments2Create_List = new list<CaseComment>();
                     
                     for (Integer i = insCaseComments.values().size() ; i > 0; i --) {
                        
                        caseComments2Create_List.add(insCaseComments.values()[i - 1]);
                     }
                     
                     insert caseComments2Create_List;
                     for (CaseComment cc : insCaseComments.values()) {
                            
                          if (fmd.Case_Comment_ID__c != null) {
                            
                              fmd.Case_Comment_ID__c += cc.Id + ',';
                          }
                          
                          else {
                            
                            fmd.Case_Comment_ID__c = cc.Id + ',';
                          }
                          system.debug('cc.Id : ' + cc.Id); 
                          system.debug('fmd.Case_Comment_ID__c : ' + fmd.Case_Comment_ID__c);   
                     }
                
                     fmd.Case_Comment_ID__c = fmd.Case_Comment_ID__c.subString(0, fmd.Case_Comment_ID__c.length() -1);
                     
                 }
                
                currFMDiscussion.put(CCInd,fmd);
                CCInd++;
            }
            else if(Trigger.isUpdate && !fmd.Public__c && fmd.Public__c!=Trigger.oldMap.get(fmd.Id).Public__c)
            {
                if(fmd.Case_Comment_ID__c!=null)
                {
                    //delCaseComments.Add(new CaseComment(Id=fmd.Case_Comment_ID__c));
                    
                    list<String> caseCommentsIdsAstr_List = fmd.Case_Comment_ID__c.split(',');
                    caseCommentsIds_Set.addAll(caseCommentsIdsAstr_List);
                }
                fmd.Case_Comment_ID__c='';
            }
            
        }
        
        list<CaseComment> caseCommsnts2Delete_List = new list<CaseComment>();
        
        if (!caseCommentsIds_Set.isEmpty()) {
            
            for (String caseCommentId_Str : caseCommentsIds_Set) {
                
                CaseComment caseComment2Delete = new CaseComment(Id = caseCommentId_Str);
                caseCommsnts2Delete_List.add(caseComment2Delete);
            }
            
            if (!caseCommsnts2Delete_List.isEmpty()) {
                
                delete caseCommsnts2Delete_List;
            }
        }
        system.debug('BLAT lst_CaseId: ' + lst_CaseId);
        if(!delCaseComments.isEmpty())
        {
            delete delCaseComments;
        }
        
        /* if(!lst_CaseId.isEmpty())
        {
            for(Integer ind : currFMDiscussion.keySet())
            {
                CaseComment CC = insCaseComments.get(ind);
               // system.debug('BLAT CC: ' + CC);
               // system.debug('BLAT CC.ParentId: ' + CC.ParentId);
               // currFMDiscussion.get(ind).Case_Comment_ID__c = CC.Id;
            }
            
        }
        */
    }
        if(Trigger.isInsert)
        {
            List<ID> lst_CaseId = new List <ID>();
            for(FM_Discussion__c fmd : Trigger.new)
            {
                lst_CaseId.add(fmd.Case__c);
            }
            List<Case> lst_Case = [Select c.Id, c.FM_Discussion_Index__c From Case c where c.Id in :lst_CaseId and IsDeleted=false];
            Map<ID,Integer> map_CaseIdDiscussionInd = new Map<ID,Integer>();      
            
                               
            system.debug('BLAT lst_Case: ' + lst_Case);
            for(Case c : lst_Case)
            {          
                
                system.debug('BLAT c: ' + c);
                if(c.FM_Discussion_Index__c==null)
                {
                    system.debug('BLAT 65 c.FM_Discussion_Index__c: ' + c.FM_Discussion_Index__c);
                    map_CaseIdDiscussionInd.put(c.Id,0);
                }
                else
                {
                    map_CaseIdDiscussionInd.put(c.Id,c.FM_Discussion_Index__c.intValue());
                }
                system.debug('BLAT map_CaseIdDiscussionInd: ' + map_CaseIdDiscussionInd);
            }
            for(FM_Discussion__c fmd : Trigger.new)
            {
                system.debug('BLAT fmd: ' + fmd);
                if(map_CaseIdDiscussionInd.containsKey(fmd.Case__c))
                {
                    system.debug('BLAT IF 79');
                    if(map_CaseIdDiscussionInd.get(fmd.Case__c)==null)
                    {
                        system.debug('BLAT IF 82');
                        fmd.Index_Number__c = 1;
                    }
                    else
                    {
                        system.debug('BLAT IF 87');
                        fmd.Index_Number__c = map_CaseIdDiscussionInd.get(fmd.Case__c)+1;  
                                 
                    }             
                    map_CaseIdDiscussionInd.remove(fmd.Case__c);
                    map_CaseIdDiscussionInd.put(fmd.Case__c,fmd.Index_Number__c.intValue());
                }
            }
        } 
        
    private String byteAbbreviate(String stringToAbbreviate, Integer byteLength) {
    
        //First abbrevate using the character length. This will get us close
        if (stringToAbbreviate != null) {
            stringToAbbreviate = stringToAbbreviate.abbreviate(byteLength);
            Integer abbLength = byteLength;
            
            while (Blob.valueOf(stringToAbbreviate).size() > byteLength) {
              
              abbLength = abbLength - 1;
              stringToAbbreviate = stringToAbbreviate.abbreviate(abbLength);
            }
        }
        
        return stringToAbbreviate;
    }
    
    private Map<Integer,CaseComment> createCaseCommentsLimitBody(String body, FM_Discussion__c fmd, Integer CCInd) {
        
        Map<Integer,CaseComment> insCaseComments = new Map<Integer,CaseComment>();
        String fittedBody = '';
        String remainedBody = body;
        
        boolean endOfcycle = false;
        
       if (remainedBody != null) {
            
           while (!endOfcycle) {
                 
                if(remainedBody.length() > 3850) {
                    
                    fittedBody = remainedBody.substring(0,3850);
                    remainedBody = remainedBody.substring(3850);
                    
                    CaseComment tmpCC = new CaseComment();
                    tmpCC.ParentId = fmd.Case__c;
                    tmpCC.IsPublished = true;
                    tmpCC.CommentBody = fittedBody + ' \n \n  ** PLEASE CONTINUE TO THE NEXT COMMENT';
                    insCaseComments.put(CCInd,tmpCC);
                    CCInd ++;
                    fittedBody = '';
                }
                
                else {
                    endOfcycle = true;
                    CaseComment tmpCC = new CaseComment();
                    tmpCC.ParentId = fmd.Case__c;
                    tmpCC.IsPublished = true;
                    tmpCC.CommentBody = remainedBody;
                    insCaseComments.put(CCInd,tmpCC);
                }
            }   
        }
        
        
        
        return insCaseComments;
    }
}