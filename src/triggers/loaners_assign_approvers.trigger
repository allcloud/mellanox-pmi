trigger loaners_assign_approvers on Loaner_Request__c (before insert, before update) {
    User curr_user;
    ID sales_owner_ID;
    List<Loaner_Request__c> lst_loaners = new List<Loaner_Request__c>();
    Set<ID> level2_approvers = new Set<ID>();
    Integer no_rows;
    
    for(Loaner_Request__c LR:Trigger.new){
        if(trigger.isinsert)
        {
            lst_loaners.add(LR);
        }        
		//KN add 02-18-15 SVP approval for EDR loaners
        if(trigger.isupdate && LR.Status__c == 'Submitted' && trigger.oldMap.get(LR.id).Status__c != 'Submitted') {
        	lst_loaners.add(LR);
        	/*
        	no_rows = [select count() from Loaner_Product__c where Loaner_Request__c = :LR.ID and Need_SVP_approval__c=true];
        	if(no_rows > 0)
        		LR.Require_SVP_approval__c = true;
        	else
        		LR.Require_SVP_approval__c = false;
        	*/
        }
        if(LR.Status__c == 'Submitted'){
        	no_rows = [select count() from Loaner_Product__c where Loaner_Request__c = :LR.ID and Need_SVP_approval__c=true];
        	if(no_rows > 0)
        		LR.Require_SVP_approval__c = true;
        	else
        		LR.Require_SVP_approval__c = false;
        }
        //      
        if(trigger.isinsert && LR.Sales_owner__c == NULL)     
        {
            LR.Sales_owner__c = LR.createdbyId;
        }
        //KN added 11-10-14
        if(Trigger.isInsert && LR.Loaner_Owner_Marketing__c != null && Quote_ENV.Marketing_users.get(LR.Loaner_Owner_Marketing__c) != null) {
        	LR.Loaner_Owner_Email_Marketing__c = Quote_ENV.Marketing_users.get(LR.Loaner_Owner_Marketing__c);
        }
        //KN 05-27-15
        if(LR.Sales_owner__c != null)
        	sales_owner_ID = LR.Sales_owner__c;
        else
        	sales_owner_ID = Userinfo.getUserId();
        //
    } 
    if(lst_loaners.size()==0)
        return;
    //
    level2_approvers.add('005500000013P66'); //Chuck T
    level2_approvers.add('00550000001s3jf'); //Dale D
    level2_approvers.add('00550000001t0V4'); //Gil Briman
    //level2_approvers.add('005500000015ZVa'); //Sergio G replaced w/ Peter Waxman - 02-04-2014
    level2_approvers.add('005500000015ZVr');  //Peter Waxman
    level2_approvers.add('0055000000152sn'); //Yossi Avni
    level2_approvers.add('00550000001t25m'); //Steen Gunderson
    level2_approvers.add('005500000011h1E'); //Darrin Chen
    level2_approvers.add('005500000015ZVp'); //Amir Prescher
    level2_approvers.add('00550000001tV2y'); //Aviram Gutman
    level2_approvers.add('00550000001r3oE'); //Gilad Shainer
    level2_approvers.add('005500000035NVy'); //Kevin Deierling
     
    // old logic route to direct manager
    //curr_user = [select id,Email,Name,Level1_Manager_ID__c,Level2_Manager_ID__c from User where ID=:UserInfo.getUserID()];
    //KN 05-27-15 new logic route to sales manager
    //TESTING --- ONLY Basmat and Gerry will have new approval flow 
    //if(UserInfo.getName() == 'Gerry Hunt' || UserInfo.getName() == 'Basmat Jehuda')
    	curr_user = [select id,Email,Name,Level1_Manager_ID__c,Level2_Manager_ID__c from User where ID=:sales_owner_ID];
    //old logic route to manager of submitters 
    	//curr_user = [select id,Email,Name,Level1_Manager_ID__c,Level2_Manager_ID__c from User where ID=:UserInfo.getUserID()];
    	
    for(Loaner_Request__c LR:lst_loaners){
        //Exceptions
        /*
        if(curr_user.Name == 'Christina Quinto'){
            LR.Level1_approver__c = '005500000015ZVr'; //set to Peter W
        }
        else if (curr_user.Name == 'Noa Aharon'){
            LR.Level1_approver__c = '00550000000wuVp'; //Ron A.
        }
        else */
        if(UserInfo.getName() == 'Marketing Group') {
        	if(LR.Loaner_Approver_Marketing__c == 'Kevin Deierling') {
        		LR.Level1_approver__c = '005500000035NVy'; //Kevin D.
        	}
        	else
        		LR.Level1_approver__c = '00550000001r3oE'; //Gilad S.
        }
        //
        else {
            LR.Level1_approver__c = curr_user.Level1_Manager_ID__c!='NA' ? curr_user.Level1_Manager_ID__c : null;
            //If level1 is not null and level1 NOT in the level2_approvers set above
            if(LR.Level1_approver__c!=null && !level2_approvers.contains(curr_user.Level1_Manager_ID__c)){
                //not assign level2_manager if it's Marc S.
                LR.Level2_approver__c = (curr_user.Level2_Manager_ID__c!='NA' && curr_user.Level2_Manager_ID__c!='005500000011cs7') ? curr_user.Level2_Manager_ID__c : null;
            }
        }           
    }
    
    //Comment out 08-05-13
    /*
    if(trigger.isUpdate)
    {            
       for(Loaner_Request__c LR:Trigger.new)
       {          
          if(LR.Status__c == 'Rejected' && trigger.oldMap.get(LR.id).Status__c !='Rejected' && LR.Rejection_Reason__c == NULL )
            {LR.adderror('\n THE "Rejection Reason" FIELD MUST BE FILLED IN ORDER TO REJECT THE LOANER');}
      
         if(LR.Status__c == 'Rejected' && trigger.oldMap.get(LR.id).Status__c !='Rejected' && LR.Rejection_Reason__c == 'Other' && LR.Other_reason__c == NULL )
            {LR.adderror('\n PLEASE DESCRIBE THE REASON IN "Other Reason" FIELD');}
       }//for   
    }//if
    */
}