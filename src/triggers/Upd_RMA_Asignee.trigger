// Version		Change Description 			 		Change made by		Change Date 		Related Service Now Number

// V1.0			This trigger should not work for 	Tatiana Sematch		29/08/13			Part of the Internal RMA
// 				Internal RMA's as requested by Liro S										project


trigger Upd_RMA_Asignee on RMA__c ( before update) {


integer SN_upd = 0;
for(RMA__c RMA:trigger.new)
 { 
 	if (rma.RecordTypeID != ENV.Internal_RMA_RecordType)
    {
	  SN_upd = 0;
	  if(RMA.SN_family_upd__c == TRUE && trigger.oldMap.get(RMA.id).SN_family_upd__c == FALSE)
	   	{SN_upd = 1;}
	  if(RMA.Assignee__c == NULL && SN_upd != 1 )
	    {RMA.Assignee__c =Userinfo.getUserId();}
    }
 }

}