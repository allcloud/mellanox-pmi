trigger trg_CustomPriceBookEntry on Custom_Price_Book__c (after insert) {
	
	if (trigger.isAfter && trigger.isInsert) {
		
		cls_trg_PriceBookEntry handler = new cls_trg_PriceBookEntry();
		handler.executeFutureCreateStandardPBEntrieslist(trigger.New);
	}
}