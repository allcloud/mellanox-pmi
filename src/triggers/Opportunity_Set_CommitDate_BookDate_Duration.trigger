trigger Opportunity_Set_CommitDate_BookDate_Duration on Opportunity (before insert, before update) {
	List<Opportunity> closedwon_optys = new List<Opportunity>();
	List<Opportunity> commit_optys = new List<Opportunity>();
	List<Opportunity> downgrade_optys = new List<Opportunity>();
	Set<ID> optys_previous_as_Pipeline = new Set<ID>();
	
	//Reset when cloning opportunities
	if(Trigger.isInsert){
		for (Opportunity o : Trigger.new){
			//o.Last_Updated_Date__c = System.today();
			o.Previous_Stage__c = '';
			o.Date_Downgraded_New_1__c = null;
			o.Original_Date_Committed__c = null;
			o.Date_Committed_Most_Recent__c = null;
			o.Current_Date_Committed__c = null;	
			o.Amt_Commit__c = null;
			o.Book_Date_Commit__c = null;
			
			//Assign Inside Sales Reps for email notification
			if(o.Sub_Channel__c == 'CTK' || o.Sub_Channel__c=='SEA' || o.Sub_Channel__c=='JPN'){
				o.Inside_Sales_Rep_1__c = 'yaelgo@mellanox.com';
				//o.Inside_Sales_Rep_2__c = 'rivif@mellanox.com';
				//for Japan , add keikot@mellanox.com
				if(o.Sub_Channel__c=='JPN')
					o.Inside_Sales_Rep_2__c = 'keikot@mellanox.com';
			}
			else if (o.Sub_Channel__c=='MFS')
				o.Inside_Sales_Rep_1__c = 'kyle@mellanoxfederal.com';
			else if (o.Sub_Channel__c=='BIZ'){
				o.Inside_Sales_Rep_1__c = 'Akkima@mellanox.com';
				//o.Inside_Sales_Rep_2__c = 'mglazer@mellanox.com';
			}
			else if (o.Sub_Channel__c=='Americas'){
				o.Inside_Sales_Rep_1__c = 'gerryh@mellanox.com';
				o.Inside_Sales_Rep_2__c = 'shannond@mellanox.com';
			}
			else if (o.Sub_Channel__c=='EUR'){
				o.Inside_Sales_Rep_1__c = 'tamarb@mellanox.com';
				o.Inside_Sales_Rep_2__c = 'netalyh@mellanox.com';
				o.Inside_Sales_Rep_3__c = 'eladhi@mellanox.com';
				o.Inside_Sales_Rep_4__c = 'malik@mellanox.com';
			}
			//
		}
	}
	
	else if(Trigger.isUpdate){
		for (Opportunity o : Trigger.new){
			//Assign Inside sales rep
			//if(Trigger.oldmap.get(o.ID).Sub_Channel__c != o.Sub_Channel__c) {
			if(o.Sub_Channel__c != null) {
				if(o.Sub_Channel__c == 'CTK' || o.Sub_Channel__c=='SEA' || o.Sub_Channel__c=='JPN'){
					o.Inside_Sales_Rep_1__c = 'yaelgo@mellanox.com';
					o.Inside_Sales_Rep_2__c = null;
					//for Japan , add keikot@mellanox.com
					if(o.Sub_Channel__c=='JPN')
						o.Inside_Sales_Rep_2__c = 'keikot@mellanox.com';
				}
				else if (o.Sub_Channel__c=='MFS')
					o.Inside_Sales_Rep_1__c = 'kyle@mellanoxfederal.com';
				else if (o.Sub_Channel__c=='BIZ'){
					o.Inside_Sales_Rep_1__c = 'Akkima@mellanox.com';
					//o.Inside_Sales_Rep_2__c = 'mglazer@mellanox.com';
				}
				else if (o.Sub_Channel__c=='Americas'){
					o.Inside_Sales_Rep_1__c = 'gerryh@mellanox.com';
					o.Inside_Sales_Rep_2__c = 'shannond@mellanox.com';
				}
				else if (o.Sub_Channel__c=='EUR'){
					o.Inside_Sales_Rep_1__c = 'tamarb@mellanox.com';
					o.Inside_Sales_Rep_2__c = 'netalyh@mellanox.com';
					o.Inside_Sales_Rep_3__c = 'eladhi@mellanox.com';
					o.Inside_Sales_Rep_4__c = 'malik@mellanox.com';
				}	
			}
			//end assign Inside Sales Reps
			if(Trigger.oldmap.get(o.ID).StageName != o.StageName || Trigger.oldmap.get(o.ID).Amount != o.Amount
					|| Trigger.oldmap.get(o.ID).Next_Step_to_PO__c != o.Next_Step_to_PO__c){
				o.Last_Updated_Date__c = System.today();
				
				if(Trigger.oldmap.get(o.ID).StageName != o.StageName)
					o.Previous_Stage__c = Trigger.oldmap.get(o.ID).StageName;
			} 
			if(o.RecordTypeId=='01250000000Dtdw'){
				if(Trigger.oldmap.get(o.ID).StageName != o.StageName && o.StageName=='Commit'
						&& (Trigger.oldmap.get(o.ID).StageName=='Pipeline' || Trigger.oldmap.get(o.ID).StageName=='Best Case'))
				{
					
					commit_optys.add(o);
				}
				else if (Trigger.oldmap.get(o.ID).StageName != o.StageName
							&& Trigger.oldmap.get(o.ID).StageName == 'Commit' 
							&& (o.StageName=='Pipeline' || o.StageName=='Best Case'))
				{
					downgrade_optys.add(o);
				}	
				//Handle cases where optys move to 'Closed-Won' from 'Pipeline'/'BestCase' without going thru 'Commit' first
				else if	(Trigger.oldmap.get(o.ID).StageName != o.StageName && o.StageName=='Closed Won'
						&& (Trigger.oldmap.get(o.ID).StageName=='Pipeline' || Trigger.oldmap.get(o.ID).StageName=='Best Case'))
				{
					closedwon_optys.add(o);	
				}			
			}
		}
	}
	
	if(commit_optys.size()==0 && downgrade_optys.size()==0)
		return;
	
	for (Opportunity opty2 : downgrade_optys){
		//opty2.Previous_Stage__c = 'Commit';
		//Create a new date field instead of formula field
		opty2.Date_Downgraded_New_1__c = System.today();
		//Must re-init to null from workflow		
		opty2.Current_Date_Committed__c = null;
	}
	
	for (Opportunity opty : commit_optys){
		opty.Current_Date_Committed__c = System.today();
		//Set the very first time Commit		
		if (opty.Original_Date_Committed__c == null){
			opty.Original_Date_Committed__c = System.today();
			opty.Date_Committed_Most_Recent__c = System.today();
			//Capture amount 1st time move to Commit
			opty.Amt_Commit__c = opty.Amount;
		}

		if(opty.Date_Downgraded_New_1__c != null && opty.Date_Committed_Most_Recent__c != null){
			opty.Date_Committed_Most_Recent__c = opty.Date_Downgraded_New_1__c.daysbetween(System.today()) <=2 ? opty.Date_Committed_Most_Recent__c : System.today();  
		}
		//Re-init Date Downgrade
		opty.Date_Downgraded_New_1__c = null;
		
		//Set Book Date @Committ the very first time
		if(opty.Book_Date_Commit__c == null){
			opty.Book_Date_Commit__c = opty.Required_Ship_Date__c;
		}
	}
	
	for (Opportunity opty3 : closedwon_optys){
		opty3.Date_Downgraded_New_1__c = null;
	}
}