trigger Mcare_case_beforeUpdate on Case (before update) {
if(ENV.triggerShouldRun){
	ENV.triggerShouldRun = false;
list<id> caseContactIds = new list<id>();
map<id,id> RelContactMap = new map<id,id>();
list<case> mcareCases = new list<case>();

for(case cs:trigger.new)
{

 if(cs.origin == 'UFM CallHome' && (cs.state__c == 'Assigned' && trigger.oldmap.get(cs.id).state__c == 'Open' ))
  {
    caseContactIds.add(cs.contactId);
    mcareCases.add(cs);   
  }
} 

if(!caseContactIds.isempty())
{
 for(contact con:[select id,Mcare_Case_Contact__c from contact where id in:caseContactIds])
  { 
   RelContactMap.put(con.id,con.Mcare_Case_Contact__c);
  }
 }
 
for(case cs1:mcareCases)
{ 
 if(RelContactMap!=null && RelContactMap.containsKey(cs1.contactId))
  	
  	if (RelContactMap.get(cs1.contactId) != null) {
  	 	cs1.contactId = RelContactMap.get(cs1.contactId);
  	}
}
}
}