trigger Upd_Contact_Account_After_Inser_Update on Contact (after insert, after update) {

List<id> Accs = new List<id>();
List<id> Contacts = new List<id>();
Map<id,Account> RelAccounts = new Map<id,Account>();
Map<id,User> RelUsers = new Map<id,User>();

for(Contact con:trigger.new)
 { Accs.add(con.AccountId);
   Contacts.add(con.id); }

for(Account ConAcc:[select id, Product_Manager__c from Account where id in :Accs])
{ RelAccounts.put(ConAcc.id,ConAcc);}



if(trigger.isUpdate)
{
    for(User ConUs:[select id, contactid from User where contactid in :Contacts])
    { RelUsers.put(ConUs.contactid,ConUs);}
 for (contact c:trigger.new)
 {
    if(c.Is_product_manager__c !=  trigger.oldmap.get(c.id).Is_product_manager__c && c.Is_product_manager__c == TRUE)
    {
     if(!RelUsers.ContainsKey(c.id))
     {c.adderror(' Only Customer Portal user can be a Product Manager. Please first enable Portal access for Contact and then set him to Product Manager');
     } else
        {RelAccounts.get(c.accountid).Product_Manager__c =c.id;
         update(RelAccounts.get(c.accountid));
        }
    }
  }
}


if(trigger.isInsert)
{ for (contact c:trigger.new)
  {if(c.Is_product_manager__c == TRUE)
    { c.adderror('Only Customer Portal user Contact can be a Product Manager \n Please first enable Portal access for contact and then set him as Product Manager');
        //RelAccounts.get(c.account).Product_Manager__c =c.id;
      //update(RelAccounts.get(c.account));
     }
  }
 }
}