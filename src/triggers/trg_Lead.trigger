trigger trg_Lead on Lead (after insert, before insert, before update, after update) {
    
    cls_trg_Lead handler = new cls_trg_Lead();
    if (trigger.isInsert) {
        
        if (trigger.isAfter) {
            
            handler.assignAssetsToLeadsBySN(trigger.New);
            handler.updateLeadOwner(trigger.New, null);
         //   handler.updateLeadCampaign(trigger.New);   
             
        }
        
        else {
            
            handler.updateRelatedDiscoverOrgFields(trigger.new, null);
            handler.updateLeadAccount(trigger.new);
            handler.updateAddressCodes(trigger.new, null);
            
            //handler.countNumberOfLicensesperLeadByEmail(trigger.New);
        }
    }
    else if (trigger.isUpdate) {
        
        if (trigger.isAfter) {
            
            handler.updateLeadOwner(trigger.New, trigger.oldMap);
            //handler.updateAddressCodes(trigger.new, trigger.oldMap);
        }
        
        else {
            
            handler.updateRelatedDiscoverOrgFields(trigger.new, trigger.oldMap);
        }
    }
}