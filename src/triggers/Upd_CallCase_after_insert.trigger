trigger Upd_CallCase_after_insert on Case (after insert) {

List<id> CallPar = new List<id>();
Map<id,Call_center__c> CallCenDet = new Map<id,Call_center__c>();
for(case cs:trigger.new)
{
 if(cs.origin == 'phone')
 { CallPar.add(cs.Call_Center_Parent__c);}
}

if(!CallPar.isempty())
 {
  for(Call_center__c CCen:[select id, case_number__c, related_case__c from Call_center__c where id in:CallPar])
  { CallCenDet.put(CCen.id,CCen);}
 }
 
for(case cs1:trigger.new)
{

if(CallCenDet.containsKey(cs1.Call_Center_Parent__c))
 {
  CallCenDet.get(cs1.Call_Center_Parent__c).Case_number__c = cs1.casenumber;
  CallCenDet.get(cs1.Call_Center_Parent__c).Related_case__c = cs1.id;
  Update(CallCenDet.get(cs1.Call_Center_Parent__c));
  }
}
}