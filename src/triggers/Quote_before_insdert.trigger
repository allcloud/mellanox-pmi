trigger Quote_before_insdert on Quote (before insert, before update) {
   
    for (Quote q : Trigger.new)
    {
    	//KN added
    	if(q.Status=='Approved')
    		q.X2nd_Level_Approval__c = 0;
    	//
    	if(trigger.isinsert)
       	{
        	system.debug('q.New_Price_Book__c: '+q.New_Price_Book__c);
        	system.debug('q.Price_List__c: '+q.Price_List__c);
        	if (q.New_Price_Book__c == null || q.New_Price_Book__c == '' )
        	    q.New_Price_Book__c = q.Price_List__c;
		}
       
       if(trigger.isupdate)
       {
        	if(trigger.oldmap.get(q.id).Status=='Approved' && q.Status == 'Approved'  )
        	{ 
        		if( trigger.oldmap.get(q.id).IsSyncing == false && q.IsSyncing == true)
        		{
        		}
        		else
	        	{
					//Only under these following changes would trigger re-set Quote Status
	        		if(trigger.oldmap.get(q.id).Account__c!=q.Account__c || trigger.oldmap.get(q.id).New_Price_Book__c!=q.New_Price_Book__c)
	        					q.Status = 'Draft';
	        	}
        	}
                 
       }
     }
}