trigger QuoteLineItem_afterUpdateafterInsertafterDelete on QuoteLineItem (after delete, after undelete, after update , after insert) 
{

    public Map<ID, PricebookEntry> mapPBE ;//will hold the values and object of the price book entries for the quote
    public Map<ID, Product2> mapProduct ;// will hold the values and object of the product from the entries for the quote
    public Map<ID, QuoteLineItem> mapQLI ;// will hold the values and object of the quotelineitem of the quote
    public List<QuoteLineItem> lstQliToUpdate = new List<QuoteLineItem>();// holds the objects to update
    public List<ID> pbeFromQliIDs ;// alist with all the ids for price book entries from the quote lines
    public List<ID> productFromQbeIDs ;// a list of all ids of products related to the price book entries retrieved
    public List<metaInfo> qliConnections = new List<metaInfo>();// the connection between all objects List 
    public Quote relQuote ;//the quote releven to the trigger
    public Decimal hardwarePrice ;//the amount of hw in the quote
    public Decimal softwarePrice ;//the amount of sw in the quote
    public Decimal hardwareWOcablesPrice ;// the amount of hw with out cables in the quote
    public Decimal quote2ndApproval ;//the status of the 2nd approval process
    public Decimal supportProductPrice ; //the amount of the support products in quote
    public String relQuoteId ;//the relevent quote id
    public Boolean doesExistsDiscountProduct ;//indicates of a discount product in the quote
    //KN added
    Double Switch_total=0;
    Double UFM_total=0;
    Map<ID,boolean> map_quoteID_deletedGPS = new Map<ID,boolean>();
    Double EDR_total=0;
    Double ETH_Switch_total=0;
    Map<ID,boolean> map_quoteID_deletedKICKSTART = new Map<ID,boolean>();
    Integer MLNXCARE_UFM_Total_Qty = 0;
    //Use to determine as if quote might have other support products besides system-generated GPS
    boolean hasOtherSupport = false;
    //KN 10-02-15 , auto-approval based on previous approved quotes
    Double total_value_excludeRebate = 0.01;
    Double rebate_amt = 0;
    Double rebate_percent;
    /*
    This structure will hold the connection between the objects in inuse in this trigger
    */
    public class metaInfo 
    {
        public QuoteLineItem qli = new QuoteLineItem();
        public Product2 product = new Product2();
        public PricebookEntry pbe = new PricebookEntry();
        public Decimal salesPrice ;
        public Decimal totalPrice ;
        public Decimal x2ndApproval ;
        
        public metaInfo (QuoteLineItem quoteLI, PricebookEntry relPbe,  Product2 p2)
        {
            qli = quoteLI ;
            product = p2 ;
            pbe = relPbe ;
            x2ndApproval = 0;
            salesPrice = quoteLI.UnitPrice;
        }
        
        private void getApprovalLevel ()
        {
            //Comment out -Christina Q 12-15-14
            //if (qli.Product_Type__c != 'HW Support' && qli.Product_Type__c != 'SW Support' && product.ProductCode != 'SUP-00012')
            if(true)
            {   
System.debug('...KN3 rebate_%===' + rebate_percent); 
System.debug('...KN3 Approved_Percent_from_Sync_Quote__c===' + qli.Approved_Percent_from_Sync_Quote__c); 
            	//quote is still 'Approved' when UnitPrice>=Approved_Price and Quantity does not drop below 5% of Approved_Quantity
                if(qli.Quote.Status=='Approved' && qli.UnitPrice >= 0 && qli.Approved_Price__c!=null && qli.UnitPrice >= qli.Approved_Price__c 
                	&& qli.Approved_Qty__c!=null && qli.Quantity >= Math.ceil(.95 * qli.Approved_Qty__c)){
                	//skip calculate x2ndApproval in this case
                }
                //KN added 9-8-15 BCN logics
                else if(qli.Quote.Status=='Approved' && qli.BCN_Price__c!=null && qli.UnitPrice == qli.BCN_Price__c){
                	//skip calculate x2ndApproval in this case
                }
                //KN 10-02-15 rebate line has to be less than that approved earlier
                else if(qli.Quote.Status=='Approved' && qli.UnitPrice < 0
                			&& (qli.Approved_Price_from_Sync_Quote__c != null && qli.UnitPrice >= qli.Approved_Price_from_Sync_Quote__c 
                				|| qli.Approved_Price__c!=null && qli.UnitPrice >= qli.Approved_Price__c)) {
System.debug('...KN3 inside approval rbate line auto');                				
                }	
                //KN 09-22-15 logics to pull in Approved prices from other sync and approevd quotes
                else if(qli.Quote.Status=='Approved' 
                			&& qli.Approved_Price_from_Sync_Quote__c!=null && qli.UnitPrice >= qli.Approved_Price_from_Sync_Quote__c
                			&& qli.Approved_Qty_from_Sync_Quote__c!=null && qli.Quantity >= qli.Approved_Qty_from_Sync_Quote__c
                			&& (qli.Approved_Percent_from_Sync_Quote__c == null || 
                				(qli.Approved_Percent_from_Sync_Quote__c != null && rebate_percent <= qli.Approved_Percent_from_Sync_Quote__c))  )
                {

                }
				else{ 
					//If match BCN price, auto-approve 09-08-15 for that line
	                if (qli.UnitPrice == qli.BCN_Price__c)
	                	x2ndApproval = 0;
	                //handle REBATE line
	                else if ( qli.UnitPrice < 0
	                			&& qli.Approved_Price_from_Sync_Quote__c != null && qli.UnitPrice >= qli.Approved_Price_from_Sync_Quote__c) {
System.debug('...KN3 inside 222 approval rbate line auto');
	                	x2ndApproval = 0;              
	                }
	                //if above or higher than the approved prices from other quotes, auto approve that line
	                else if (qli.Approved_Price_from_Sync_Quote__c != null && qli.UnitPrice >= qli.Approved_Price_from_Sync_Quote__c
	                			&& qli.Approved_Qty_from_Sync_Quote__c != null && qli.Quantity >= qli.Approved_Qty_from_Sync_Quote__c
	                			&& (qli.Approved_Percent_from_Sync_Quote__c == null 
	                					|| (qli.Approved_Percent_from_Sync_Quote__c != null && rebate_percent <= qli.Approved_Percent_from_Sync_Quote__c)) )
	                {
	                	x2ndApproval = 0;
	                }
	                else if (qli.UnitPrice < qli.Minimum_Price__c)
	                {
						//KN added 08-20-13
						//Address cases where qlines below min price, BUT within 5%
						//orig code: x2ndApproval = 2;
						if(qli.UnitPrice < qli.Minimum_Price__c * .95)
	                    	x2ndApproval = 2;
	                    else
	               			x2ndApproval = 1.5;
	               		//KN 1-29-15 per Chris auto approve if products not NPI, Early BOM, Prelim, Pre-Prod, Prototype and >= Quote List Price
	               		if(qli.Product_Agile_Status__c != 'Early BOM' && qli.Product_Agile_Status__c != 'NPI' 
	               			&& qli.Product_Agile_Status__c != 'Preliminary' && qli.Product_Agile_Status__c != 'Pre-Production'
	               			&& qli.Product_Agile_Status__c != 'Prototype'
	               			&& qli.UnitPrice >= qli.List_Price_Holder__c)
	               				x2ndApproval = 0;
	                }
	                else
	                {
	                    if (qli.UnitPrice < qli.List_Price_Holder__c && x2ndApproval <= 1)
	                    {
	                        x2ndApproval = 1;
	                    }
	                    else
	                    {
	                        if (qli.UnitPrice < qli.List_Price_Holder__c && x2ndApproval == 0)
	                        {
	                            x2ndApproval = 0;
	                        }
	                    }
	                }
				}
            }
        }
        
        private Decimal calculateSupportSalesPrice (Decimal hwPrice, Decimal swPrice)
        {
            if (qli.Product_Type__c == 'HW Support')
            {
                qli.UnitPrice = product.Support_Percent_Amount__c *hwPrice/100;
            }
            if (qli.Product_Type__c == 'SW Support')
            {
                qli.UnitPrice = product.Support_Percent_Amount__c *swPrice/100;
            }
            salesPrice= qli.UnitPrice*qli.Quantity;
            return salesPrice ; 
        }
    }
//System.debug('... KN ... isQuoteUpdate ===' + ENV.isQuoteUpdate);    
    /*
    if the value of ENV.isQuoteUpdate is not true then start the functionality
    */
    if (!ENV.isQuoteUpdate)
    {
        //INIT
        pbeFromQliIDs = new List<ID>();
        productFromQbeIDs = new List<ID>();
        ENV.isQuoteUpdate = true;
        hardwarePrice = 0;
        softwarePrice = 0;
        hardwareWOcablesPrice = 0;
        supportProductPrice = 0; 
        relQuoteId ='';
        doesExistsDiscountProduct = false;
		//KN added
	    List<Pro_Service_Products__c> proserve_prods = [Select Products__c,Products_UFM_Only__c,isNewGPS__c 
	                                                            From Pro_Service_Products__c p];
	    Set<String> GPS_prods = new Set<String>();
	    Set<String> GPS_prods_new = new Set<String>();
	    for (Pro_Service_Products__c psp : proserve_prods){
			if(psp.isNewGPS__c==false){			
		    	GPS_prods.add(psp.Products__c);
		        GPS_prods.add(psp.Products_UFM_Only__c);
			}
			else{
				GPS_prods_new.add(psp.Products__c);
		        GPS_prods_new.add(psp.Products_UFM_Only__c);
			}    
	    }        
        
        if (Trigger.isDelete ) {
            relQuoteId = Trigger.old[0].QuoteId;
			
            for (QuoteLineItem qline : Trigger.Old){
				//Detect when deleting KICKSTART
				if(qline.Product__c == 'GPS-EDR-KICKST-4') {
					map_quoteID_deletedKICKSTART.put(qline.QuoteID,true);
				}
            	if(qline.isNewGPS__c==false){
	            	if (GPS_prods.contains(qline.Product__c)){
	            		map_quoteID_deletedGPS.put(qline.QuoteID,true);
	            		break;	
	            	}
            	}
            	else{
            		if (GPS_prods_new.contains(qline.Product__c)){
	            		map_quoteID_deletedGPS.put(qline.QuoteID,true);
	            		break;	
	            	}
            	}
            }
        }
        //Handle the case where we delete GPS, then re-added
        else if (Trigger.isInsert){
        	relQuoteId = Trigger.new[0].QuoteId;
        	for (QuoteLineItem qline : Trigger.New){
        		
				if(qline.Product__c == 'GPS-EDR-KICKST-4') {
					map_quoteID_deletedKICKSTART.put(qline.QuoteID,false);
				}
				if(qline.isNewGPS__c==false){
	            	if (GPS_prods.contains(qline.Product__c)){
	            		map_quoteID_deletedGPS.put(qline.QuoteID,false);
	            		break;	
	            	}
				}
				else {
					if (GPS_prods_new.contains(qline.Product__c)){
	            		map_quoteID_deletedGPS.put(qline.QuoteID,false);
	            		break;	
	            	}
				}
            }
        }
        else 
            relQuoteId = Trigger.new[0].QuoteId;
        //
        
        /* Build the details for the quote*/
        relQuote = [Select q.Account__c, q.Support_Renew_Quote__c,q.Id, q.Exclude_Cables_From_Support__c, q.Big_Cluster_Opportunity__c, q.Sum_Amount_of_SW__c, 
        					q.Sum_Amount_of_HW__c, q.Sum_Amount_of_HW_Without_Cables__c, q.X2nd_Level_Approval__c, ERI_Quote__c, 
        					Switch_Total__c,UFM_Total__c,Node_Count__c,Partner_GPS_Qualified__c,GPS_Required__c,Status,
        					isNewGPS__c,Partner_Account_Type__c,EDR_Total__c,EDR_Quote__c,ETH_Switch_Quote__c,ETH_Switch_Total__c,
        					UFM_MLNXCARE_Sum_Qty__c
                    From Quote q 
                    Where q.Id = : relQuoteID
                    limit 1];
        
        System.debug('DK relQuote (Account) is : '+ relQuote.Account__c);
                    
        quote2ndApproval = -1;
        
        mapQLI = new Map<ID, QuoteLineItem>([Select q.Minimum_Price__c ,q.UnitPrice, q.QuoteId, q.Id, q.Product_Type__c, 
        										q.PricebookEntryId, q.Product__c, q.TotalPrice, q.List_Price_Holder__c, q.Quantity,
        										product_type1__c,q.UFM_Product__c,Approved_Price__c,Quote.Status,Approved_Qty__c,
        										isProductSupport__c,Product_Agile_Status__c,BCN_Price__c,Approved_Price_from_Sync_Quote__c,
        										Approved_Qty_from_Sync_Quote__c,Approved_Percent_from_Sync_Quote__c,
        										Prod_Classification__c
                   								From QuoteLineItem q 
                   								Where q.QuoteId = : relQuoteID]);
        
        if (mapQLI != null)
        {          
            for (QuoteLineItem q : mapQLI.values())
            {
                pbeFromQliIDs.add(q.PricebookEntryId);
                if (q.Product__c=='ADD-ON - LARGE CLUSTER')
                {
                    doesExistsDiscountProduct = true;
                }
                
            }
        }
         
        mapPBE = new Map<ID, PricebookEntry>([Select p.Product2Id, p.Id, (Select Id 
                                                                          From QuoteLineItems 
                                                                          Where Id = : mapQLI.keySet()) 
                                              From PricebookEntry p 
                                              Where Id = : pbeFromQliIDs]);
        
        if (mapPBE != null)
        {
            for (PricebookEntry p : mapPBE.values())
            {
                productFromQbeIDs.add(p.Product2Id);
            }

        }
        
        mapProduct = new Map<ID, Product2>([Select p.Support_Percent_Amount__c, p.Product_Type1__c, p.Id, p.Type__c, p.Name, p.OEM_PL__c, p.ProductCode 
                                            From Product2 p
                                            Where p.Id = : productFromQbeIDs]);
//System.debug('...KN mapQLI.values()==='+mapQLI.values());        
        if (mapProduct != null)
        {
            for (QuoteLineItem qli : mapQLI.values())
            {
                qliConnections.add(new metaInfo(qli, mapPBE.get(qli.PricebookEntryId), mapProduct.get(mapPBE.get(qli.PricebookEntryId).Product2Id)));
            }
        }
        
        /* Calculations Executes*/
        if (qliConnections.size() == mapQLI.size())
        {
            for (metaInfo mi : qliConnections)
            {
                //originial - moved to further down KN 10-02-15
                //mi.getApprovalLevel();
	            if(mi.qli.TotalPrice > 0)
	            	total_value_excludeRebate += mi.qli.TotalPrice;
	            else
	                rebate_amt +=  mi.qli.TotalPrice * -1 ;
				
        		if(mi.qli.product_type1__c=='SWITCH' || mi.qli.product_type1__c=='GATEWAY' || mi.qli.product_type1__c=='SYSTEM')        
	                Switch_total += mi.qli.UnitPrice * mi.qli.Quantity;
	            if (mi.qli.UFM_Product__c==1)    
    	            UFM_total += mi.qli.UnitPrice * mi.qli.Quantity;
    	        //Handle ETH Switch quote
    	        if(mi.qli.Prod_Classification__c=='ETH Switches')
    	        	ETH_Switch_Total += mi.qli.UnitPrice * mi.qli.Quantity;
    	        //EDR Quote - auto insert KICKSTART
    	        if(Quote_ENV.EDR_Products != null && Quote_ENV.EDR_Products.containsKey(mi.qli.Product__c))
    	        	EDR_total += mi.qli.UnitPrice * mi.qli.Quantity;
    	        //KN - Sum up total Qty UFM for MLNX CARE Auto Insert
    	        if(Quote_ENV.UFM_MLNXCARE_Products != null && Quote_ENV.UFM_MLNXCARE_Products.containsKey(mi.qli.Product__c)) {
    	        	if(mi.qli.Product__c == 'MUA9002F-2SF-250')
    	        		MLNXCARE_UFM_Total_Qty += mi.qli.Quantity.intValue() * 250;
    	        	else if(mi.qli.Product__c == 'MUA9002F-2SF-500')
    	        		MLNXCARE_UFM_Total_Qty += mi.qli.Quantity.intValue() * 500;
    	        	else if(mi.qli.Product__c == 'MUA9002F-2SF-1K')
    	        		MLNXCARE_UFM_Total_Qty += mi.qli.Quantity.intValue() * 1000;
    	        	else if(mi.qli.Product__c == 'MUA9002F-2SF-2K')
    	        		MLNXCARE_UFM_Total_Qty += mi.qli.Quantity.intValue() * 2000;
    	        	else if(mi.qli.Product__c == 'MUA9002F-2SF-4K')
    	        		MLNXCARE_UFM_Total_Qty += mi.qli.Quantity.intValue() * 4000;
    	        	else
    	        		MLNXCARE_UFM_Total_Qty += mi.qli.Quantity.intValue();
    	        }
    	        //
				//KN 02-14-14 Determine as if there is any other support product   
				if(mi.qli.isProductSupport__c == 1 || Quote_ENV.GPS_auto_insert_Products.containsKey(mi.qli.Product__c))
					hasOtherSupport = true;
            }
            //check rebate percentage compared to previous approved quote
          
            rebate_percent = Math.round(rebate_amt/total_value_excludeRebate * 100.00);
            //
            for (metaInfo mi : qliConnections)
            
            {
                //KN 10-02-15 moved here
                mi.getApprovalLevel();
                if(mi.x2ndApproval > quote2ndApproval)
                    {
                        quote2ndApproval = mi.x2ndApproval;
                    }
                System.debug('DK HERE mi.product.Type__c='+mi.product.Type__c);
                System.debug('DK HERE mi.product.Product_Type1__c='+mi.product.Product_Type1__c);
                //calculates Sales Price att
                if (/*mi.product.Type__c == 'HW' null &&*/ mi.product.Product_Type1__c == 'CABLES')
                {
                    if (mi.product.ProductCode != 'SUP-00012')
                        hardwarePrice += mi.qli.TotalPrice;
                }
                else
                {
                    if (mi.product.Type__c == 'HW' && /*DK*/ mi.product.Product_Type1__c != 'CABLES')
                    {   
                        if (mi.product.ProductCode != 'SUP-00012')
                        {
                            hardwareWOcablesPrice += mi.qli.TotalPrice;
                            hardwarePrice += mi.qli.TotalPrice;
                        }
                    }
                    if (mi.product.Type__c == 'SW')
                    {
                        softwarePrice += mi.qli.TotalPrice;
                    }
                }
            }
            
            for (metaInfo mi : qliConnections) 
            { /* ek removing support price calculation

                if (mi.product.Type__c != null && !relQuote.Support_Renew_Quote__c) 
                {
                    if ((mi.product.Type__c==('HW Support') || mi.product.Type__c==('SW Support')) && relQuote.Exclude_Cables_From_Support__c)
                    {
                        supportProductPrice += mi.calculateSupportSalesPrice(hardwareWOcablesPrice, softwarePrice);
                    }
                    else
                    {
                        if (!relQuote.Exclude_Cables_From_Support__c && (mi.product.Type__c==('HW Support') || mi.product.Type__c==('SW Support')))
                        {
                            supportProductPrice += mi.calculateSupportSalesPrice(hardwarePrice, softwarePrice);
                        }
                    }
                }*/
            }
            
            if (supportProductPrice > 0 && doesExistsDiscountProduct)
            {
                Integer counter = 0 ;
                Integer placeOfDiscountInList = -1 ;
                for (metaInfo mi : qliConnections) 
                {
                    if (mi.product.ProductCode=='SUP-00012')
                    {
                        placeOfDiscountInList = counter;
                    }
                    counter ++;
                }
                
                if (placeOfDiscountInList > -1)
                {
                    
                    delete qliConnections[placeOfDiscountInList].qli;
                    qliConnections.remove(placeOfDiscountInList);
                }
                doesExistsDiscountProduct = false;
                
            }
            
            // in case of a big cluster as true :
            if (supportProductPrice > 0 && relQuote.Big_Cluster_Opportunity__c /*&& !doesExistsDiscountProduct*/)
            {
				
				
                    
                Product2 p2 = [Select p.Support_Percent__c, p.Support_Percent_Amount__c, p.ProductCode, p.Id, (Select Id 
                                                                                                                From PricebookEntries) 
                               From Product2 p
                               Where p.ProductCode='SUP-00012'
                               limit 1];
                Decimal amountToDiscount = supportProductPrice * 0.2 * -1;
                QuoteLineItem qli2 = new QuoteLineItem();
                qli2.PricebookEntryId = p2.PricebookEntries[1].Id;
                qli2.UnitPrice = amountToDiscount;
                qli2.QuoteId = relQuote.Id;
                qli2.Quantity=1;
                insert qli2;                
            }
            
            for (metaInfo mi : qliConnections) 
            {
                lstQliToUpdate.add(mi.qli);
            }         
            update lstQliToUpdate;
            relQuote.Sum_Amount_of_HW__c = hardwarePrice;
            relQuote.Sum_Amount_of_HW_Without_Cables__c = hardwareWOcablesPrice;
            relQuote.Sum_Amount_of_SW__c = softwarePrice;
            //KN changed
            //reset this field GPS_Got_Deleted message
            
            if (hasOtherSupport == true || (relQuote.GPS_Required__c == false && relQuote.EDR_Quote__c == false))
            	relQuote.GPS_Got_Deleted__c = '';
            relQuote.X2nd_Level_Approval__c = quote2ndApproval;
            
            //If Users delete system-generated GPS, but quote has other support products then no need for re-approval
            if(Switch_total > 5000 
            		&& map_quoteID_deletedGPS.get(relQuote.ID)==true && hasOtherSupport == false){
				relQuote.GPS_Got_Deleted__c = ' and requires GPS products to be restored.' ;
            	
            	//If delete GPS, and Switch_Total > 600k, need 2nd level approval
            	if (relQuote.Switch_Total__c > 600000){
            		relQuote.X2nd_Level_Approval__c = 2;
            	}
            	else
            		relQuote.X2nd_Level_Approval__c = relQuote.X2nd_Level_Approval__c <=1 ? 1 : relQuote.X2nd_Level_Approval__c;
            }
            else if(UFM_total > 5000 
            		&& map_quoteID_deletedGPS.get(relQuote.ID)==true && hasOtherSupport == false){
            	relQuote.GPS_Got_Deleted__c = ' and requires GPS products to be restored.' ;
            	
            	//If delete GPS, and Switch_Total > 600k, need 2nd level approval
            	if (relQuote.UFM_Total__c > 600000){
            		relQuote.X2nd_Level_Approval__c = 2;
            	}
            	else
            		relQuote.X2nd_Level_Approval__c = relQuote.X2nd_Level_Approval__c <=1 ? 1 : relQuote.X2nd_Level_Approval__c;
            }
            //detect when deleting KICKSTART GPS
            else if(EDR_total > 0 
            		&& map_quoteID_deletedKICKSTART.get(relQuote.ID)==true && hasOtherSupport == false){
            	relQuote.GPS_Got_Deleted__c = ' and requires GPS products to be restored.' ;
            	//If delete GPS, and Switch_Total > 600k, need 2nd level approval
            	if (relQuote.EDR_Total__c > 600000){
            		relQuote.X2nd_Level_Approval__c = 2;
            	}
            	else
            		relQuote.X2nd_Level_Approval__c = relQuote.X2nd_Level_Approval__c <=1 ? 1 : relQuote.X2nd_Level_Approval__c;
            }
            //
            //
            if(relQuote.Status=='Approved' && relQuote.X2nd_Level_Approval__c>0 && !relQuote.ERI_Quote__c)
            	relQuote.Status = 'Draft';
            //KN added
			relQuote.Switch_Total__c = Switch_total;
			relQuote.UFM_Total__c = UFM_total;
			relQuote.EDR_Total__c = EDR_total;
			relQuote.ETH_Switch_Total__c = ETH_Switch_total; 	
			relQuote.UFM_MLNXCARE_Sum_Qty__c = MLNXCARE_UFM_Total_Qty;		            
			//if (relQuote.Partner_GPS_Qualified__c==0){
                if ((relQuote.Switch_Total__c!=null && relQuote.Switch_Total__c>5000) || (relQuote.UFM_Total__c!=null && relQuote.UFM_Total__c>5000)) {
					//KN added 03-12-15 dont auto add GPS for EDR quotes since EDR quotes already have GPS KICKSTART
					if(relQuote.EDR_Total__c == null || relQuote.EDR_Total__c <= 0)
						relQuote.GPS_Required__c=true;
					//dont auto add GPS for EDR quotes since ETH Switch quotes already have GPS KICKSTART
					else if(relQuote.ETH_Switch_Total__c == null || relQuote.ETH_Switch_Total__c <= 0)
						relQuote.GPS_Required__c=true;						
                }  
            //}
			
			// Skip case where relQuote.Partner_GPS_Qualified__c==1 for NOW
			/*
            else if (relQuote.Partner_GPS_Qualified__c==1 && relQuote.Node_Count__c != null && relQuote.Node_Count__c > 648){
                if ((relQuote.Switch_Total__c!=null && relQuote.Switch_Total__c>0) || (relQuote.UFM_Total__c!=null && relQuote.UFM_Total__c>0)) {
                    relQuote.GPS_Required__c=true;					
                }
            }
            */
            //KN add 12-2-15 - Add ETH Switch KICKSTART GPS for ETH quotes
            if (relQuote.ETH_Switch_Total__c!=null && relQuote.ETH_Switch_Total__c>0){
				relQuote.ETH_Switch_Quote__c=true;
            }
            //KN add 03-12-15 - Add EDR KICKSTART GPS
            if (relQuote.EDR_Total__c!=null && relQuote.EDR_Total__c>0){
				relQuote.EDR_Quote__c=true;
            }
            //KN added - 11/04/13 - add Academy OPN
            if (relQuote.isNewGPS__c==true && relQuote.UFM_Total__c!=null && relQuote.UFM_Total__c>0 & relQuote.Partner_Account_Type__c!='OEM'){
				relQuote.AcademyOPN_Required__c=true;
            }
            else if (relQuote.isNewGPS__c==true && relQuote.Switch_Total__c!=null && relQuote.Switch_Total__c>0 & relQuote.Partner_Account_Type__c!='OEM'){
            	relQuote.AcademyOPN_Required__c=true;					
            }
            //
            try {	
            	update relQuote;
            }
            catch(Exception e) {
            	if (trigger.New != null && !trigger.New.isEmpty()) {
            		trigger.New[0].addError('Cannot update the Quote : ' + e.getMessage());
            	}
            }
            //
        }
    }
}