trigger trg_Product_Detail on ProductDetails__c (after update) {
	
	cls_trg_Product_Detail handler = new cls_trg_Product_Detail();
	if (trigger.isAfter) {
		
		if (trigger.isUpdate) {
			
			handler.updateRelatedProductsImageURL(trigger.New, trigger.OldMap);
		}
	}

}