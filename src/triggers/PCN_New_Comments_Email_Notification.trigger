trigger PCN_New_Comments_Email_Notification on PCN_Comment__c (after insert) {

	List<PCN_Comment__c> lst_comments = new List<PCN_Comment__c>();
	for (PCN_Comment__c pcm : Trigger.New){
		if(pcm.Contact_Email__c!=null)
			lst_comments.add(pcm);	
	}
	if(lst_comments.size()==0)
		return;
	List<Messaging.SingleEmailMessage> lst_mail = new List<Messaging.SingleEmailMessage>();
	Messaging.SingleEmailMessage email;
	String [] toAddresses;
	for (PCN_Comment__c pcm : lst_comments){
		if(pcm.Comments_from_Portal__c==true)
			continue;
		email = new Messaging.SingleEmailMessage();
		//comment from business owner-Matt
		if(pcm.Comments_from_Portal__c==false)
			toAddresses = new String[] {pcm.Contact_Email__c};
		
		//else
		//	toAddresses = new String[] {'Randazzo@Mellanox.com'};
			//test
			//toAddresses = new String[] {'khoa@Mellanox.com'};
		
		email.setToAddresses(toAddresses);						
		email.setSubject('New Comment is Added to: '+ pcm.PCN_Name__c);
		/*
		if(pcm.Comments_from_Portal__c==true)
	        email.setPlainTextBody('\nDear Mellanox,' + '\n' + '\nBelow is the new comment added to the system' + '\n' + '\nPCN Name: \t' + pcm.PCN_Name__c   
        							+ '\n' + '\nPCN Description: \t' + pcm.PCN_Description__c + '\n'
        							+ '\n' + '\nPCN Link: \t' + 'https://na3.salesforce.com/' + pcm.PCN__r.ID + '\n'
        							+ '\n' + '\nOwner: \t' + pcm.Owner_Name__c + '\n' 
        							+'\nEmail: \t' + pcm.Owner_Email__c +'\n'
        							+ '\nComment Detail: \t' + pcm.Comments__c + '\n' +'\n' +'\n'
        							+ 'Best Regards, ' + '\n'
        							+ pcm.Owner_Name__c);
		else
		*/
			email.setPlainTextBody('\nDear customer,' + '\n' + '\nBelow is the new comment added to the system' + '\n' + '\nPCN Name: \t' + pcm.PCN_Name__c   
        							+ '\n' + '\nPCN Description: \t' + pcm.PCN_Description__c + '\n'
        							+ '\n' + '\nOwner: \t' + pcm.Owner_Name__c + '\n' 
        							+'\nEmail: \t' + pcm.Owner_Email__c +'\n'
        							+ '\nComment Detail: \t' + pcm.Comments__c + '\n' +'\n' +'\n'
        							+ 'Best Regards, ' + '\n'
        							+ pcm.Owner_Name__c);			        							 
        lst_mail.add(email);  
	}
	Messaging.SendEmailResult[] r = Messaging.sendEmail(lst_mail);	
//	
}