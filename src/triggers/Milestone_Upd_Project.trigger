trigger Milestone_Upd_Project on Milestone1_Milestone__c (after update, after insert, after delete) {
/*
List<Milestone1_Milestone__c> UpdMilestones = new List<Milestone1_Milestone__c>();
List<id> UpdMSProjectIds = new List<id>();
List<Milestone1_Milestone__c> InsertMilestones = new List<Milestone1_Milestone__c>();
List<id> InsertMSParojectIds = new List<id>();
List<Milestone1_Milestone__c> DeleteMilestones = new List<Milestone1_Milestone__c>();
List<id> DeleteMSProjectIds = new List<id>();
List<Milestone1_Project__c> ProjectsToUpd = new List<Milestone1_Project__c>();
Set<id> ProjectsToUpdSet = new Set<id>();

//map<id,Milestone1_Milestone__c> taskMilestones = new map<id,Milestone1_Milestone__c>();


if(trigger.isdelete)
{
for(Milestone1_Milestone__c ms:trigger.old)
 {
   {
   DeleteMilestones.add(ms);
   DeleteMSProjectIds.add(ms.project__c);
   }
 }
}

if(trigger.isinsert || trigger.isupdate)
{

for(Milestone1_Milestone__c ms:trigger.new)
{
    
  if(trigger.isinsert)
   {
     InsertMilestones.add(ms);
     InsertMSParojectIds.add(ms.project__c);
   }
   
  if(trigger.isupdate && 
    (ms.deadline__c != trigger.oldmap.get(ms.id).Deadline__c) )
    
  {
     UpdMilestones.add(ms);
     UpdMSProjectIds.add(ms.project__c);
   }
  
 
} //for
} // if



//*************** New Tasks Delete actions **************

if(!DeleteMilestones.isempty())
{
map<id,Milestone1_project__c> MSprojects =new map<id,Milestone1_Project__c> ([Select id, Deadline__c
                                                                                      From Milestone1_Project__c where id in :DeleteMSProjectIds]);


for(Milestone1_Milestone__c ms:DeleteMilestones)
 {
 
    
    
        if(!ProjectsToUpdSet.contains(MSprojects.get(ms.project__c).id))
         {
         
         // system.debug('adding this Proj '+MSprojects.get(ms.project__c));        
          ProjectsToUpdSet.add(MSprojects.get(ms.project__c).id);
          ProjectsToUpd.add(MSprojects.get(ms.project__c));
         } 
    
   
   } // end of for
 
 
 update(ProjectsToUpd);
}  // end of Delete Milestones



//*************** New Tasks Insert actions **************
if(!InsertMilestones.isempty())
{
map<id,Milestone1_project__c> MSprojects =new map<id,Milestone1_project__c> ([Select id, Deadline__c
                                                                                      From Milestone1_Project__c where id in :InsertMSParojectIds]);

 for(Milestone1_milestone__c ms:InsertMilestones)
 {
 
  
   
  if(MSprojects.containsKey(ms.project__c)&& MSprojects.get(ms.project__c).Deadline__c < ms.deadline__c)
   {
    MSprojects.get(ms.project__c).Deadline__c = ms.Deadline__c;
   }
    
      
        
       
    
        if(!ProjectsToUpdSet.contains(MSprojects.get(ms.project__c).id))
         {
         
          //system.debug('adding this Mstn '+taskMilestones.get(ts.whatId));        
          ProjectsToUpdSet.add(MSprojects.get(ms.project__c).id);
          ProjectsToUpd.add(MSprojects.get(ms.project__c));
         } 
    
   
   } // end of for
 
 
 update(ProjectsToUpd);
}  // end of Inser Milestones


//******************** Existing Task Update Action **************

if(!UpdMilestones.isempty())
{
map<id,Milestone1_Project__c> MSprojects =new map<id,Milestone1_Project__c> ([Select id, Deadline__c
                                                                                      From Milestone1_Project__c where id in :UpdMSProjectIds]);

 for(Milestone1_Milestone__c ms:UpdMilestones)
 {
  if(MSprojects.containsKey(ms.project__c)&& MSprojects.get(ms.project__c).Deadline__c < ms.Deadline__c)
   {
    MSprojects.get(ms.project__c).Deadline__c = ms.Deadline__c;
      
   }
   
    
  
  if(!ProjectsToUpdSet.contains(MSprojects.get(ms.project__c).id))
         {
         
         // system.debug('adding this Mstn '+taskMilestones.get(ts.whatId));        
          ProjectsToUpdSet.add(MSprojects.get(ms.project__c).id);
          ProjectsToUpd.add(MSprojects.get(ms.project__c));
         } 
  
  
 }// for
 
 update(ProjectsToUpd); 
} 
*/

}