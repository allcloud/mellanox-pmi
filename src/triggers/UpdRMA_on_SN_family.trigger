trigger UpdRMA_on_SN_family on Serial_Number__c (after update) {

map<id,string> RMAtoUpdate = new map<id,string>();

for( Serial_Number__c SN:trigger.new)
 {
 if(( SN.RMA_type_by_support__c != trigger.oldmap.get(SN.id).RMA_type_by_support__c ) ||
     (SN.Advanced_Replacement__c== True && trigger.oldmap.get(SN.id).Advanced_Replacement__c == False ))
  {  
           RMAtoUpdate.put(SN.RMA__c,SN.RMA_type_final__c);  }
  
 }
 system.debug('RMAs = ' + RMAtoUpdate.keySet());

 if(!RMAtoUpdate.isempty())     
 {                
  List<RMA__c> RMAList =[select id,  Advanced__c  from RMA__c where id in :RMAtoUpdate.keySet() AND Account_Name__r.No_Advanced_Replacement__c!= TRUE] ;
  system.debug('RMA list = ' + RMAList);
 

   for(RMA__c R:RMAList)
   { system.debug('Rma updated = ' + R.id);

    if( RMAtoUpdate.containsKey(r.id))
    if( (RMAtoUpdate.get(r.id)).contains('Advanced'))
     {R.Advanced__c = true;} else
      {R.Advanced__c = false;}
   }

   update RMAList;
 }
}