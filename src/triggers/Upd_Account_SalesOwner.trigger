trigger Upd_Account_SalesOwner on Opportunity (after update) {

List<id> OppAcctIds = new List<id>();
Map<id,Opportunity> AcctToOpps = new Map<id,Opportunity>();
List<Account> UpdAccounts = new List<Account>();

for(Opportunity Opp:trigger.new)
{
 if(Opp.StageName == 'Closed Won' && trigger.oldmap.get(Opp.id).StageName != 'Closed Won')
 { 
  OppAcctIds.add(Opp.AccountId);
  AcctToOpps.put(Opp.AccountId,Opp);
 }
}


if(!OppAcctIds.isempty())
{
 {UpdAccounts = [select id,Sales_Owner__c from Account where id in :OppAcctIds];}

 for(Account Acc:UpdAccounts)
 {
  Acc.Sales_Owner__c = AcctToOpps.get(Acc.id).OwnerId;
 }
 
 update(UpdAccounts); 
}
 
 

}