trigger Quote_afterInsert_afterUpdate_add_GPS on Quote (after update) {

	//Trigger 1 - Set flag GPS_Required__c
	if (!Quote_ENV.updateQuote_run_once) {
        Quote_ENV.updateQuote_run_once=true;
        Map<ID,boolean> map_quoteID_GPS_Required = new Map<ID,boolean>();
        Set<ID> qIDs = new Set<ID>();
        List<Quote> all_quotes = new List<Quote>();
        List<Quote> quotes_updates = new List<Quote>();
        for (Quote q : Trigger.New){
        	qIDs.add(q.ID);
        	//Changed 01-28-14, NO NEED to check for Partner_GPS_Require
        	//if (q.Partner_GPS_Qualified__c==0){
                if ((q.Switch_Total__c!=null && q.Switch_Total__c>5000) || (q.UFM_Total__c!=null && q.UFM_Total__c>5000)) {
					//KN added 03-12-15 if EDR quotes skip this since EDR quotes have KICKSTART GPS parts already added
					//map_quoteID_GPS_Required.put(q.ID,true);
					//KN 12-2-15, if ETH already added KICKSTART-ETH
					if(q.EDR_Total__c == null || q.EDR_Total__c <= 0)
						map_quoteID_GPS_Required.put(q.ID,true);
					else if(q.ETH_Switch_Total__c == null || q.ETH_Switch_Total__c <= 0)
						map_quoteID_GPS_Required.put(q.ID,true);
					else
						map_quoteID_GPS_Required.put(q.ID,false);
					//
                }  
                else
                	map_quoteID_GPS_Required.put(q.ID,false);	             
            //}
            //else {
            //	map_quoteID_GPS_Required.put(q.ID,false);
            //}
        }
        if (qIDs.size()==0)
        	return;
        
        all_quotes = [select ID, GPS_Required__c,QuoteNumber,isNewGPS__c from Quote where ID in :qIDs];
        
        for (Quote qq : all_quotes){
System.debug('... KN map_quoteID_GPS_Required.get(qq.ID)==='+map_quoteID_GPS_Required.get(qq.ID));
        	if (map_quoteID_GPS_Required.get(qq.ID)!=null && map_quoteID_GPS_Required.get(qq.ID)!=qq.GPS_Required__c){
        		qq.GPS_Required__c = map_quoteID_GPS_Required.get(qq.ID);
        		quotes_updates.add(qq);  	
        	}        		
        }
        if(quotes_updates.size()>0)
        	update quotes_updates;
	}        
	// Trigger 2 - ADD GPS products
	
		Set<ID> quotes_IDs = new Set<ID>();
        List<Quote> quotes = new List<Quote>();
        Set<ID> set_ProductIDs = new Set<ID>();
        Set<Id> set_PriceBookID = new Set<Id>();
        Set<Id> set_PriceBookID_GPS = new Set<Id>();
		
        for (Quote q : Trigger.New){
			if (Trigger.oldmap.get(q.ID).GPS_Required__c==false && q.GPS_Required__c==true){
				quotes_IDs.add(q.id);
				quotes.add(q);
				set_PriceBookID_GPS.add(q.Pricebook2ID);
				set_PriceBookID.add(q.Pricebook2ID);
				set_PriceBookID.add(Quote_ENV.PriceBookMap.get(q.New_Price_Book__c));	
			}
        }
        if (quotes.size()==0)
        	return;
        //Amount used to determine which GPS to be added:
        //Amount will be th larger of Switch or UFM
        //KN 01-28-2014
        Boolean Switch_amount_exist;
        
        if (quotes[0].Switch_Total__c != null && quotes[0].Switch_Total__c > 0){
			Switch_amount_exist = true;
        }
        else
        	Switch_amount_exist = false;	
        //
        
        List<Pro_Service_Products__c> proserve_prods = [Select Min_Nodes__c,Max_Nodes__c,Qty__c,Qty_UFM_Only__c,Products__c,
        													Products_ID__c,Products_UFM_Only__c,Products_UFM_Only_ID__c,
        													Min_Amount__c, Max_Amount__c 
                                                            From Pro_Service_Products__c p 
                                                            where isNewGPS__c = :quotes[0].isNewGPS__c
                                                            order by Min_Amount__c];
        for (Pro_Service_Products__c psp : proserve_prods){
            set_ProductIDs.add(psp.Products_ID__c);
            set_ProductIDs.add(psp.Products_UFM_Only_ID__c);    
        }
        Map<ID,PricebookEntry> map_PricebookEntry = new Map<ID,PricebookEntry>([select id,Pricebook2ID,Product2ID,UnitPrice from PricebookEntry 
                                                                                where Product2Id in :set_ProductIDs and Pricebook2ID in :set_PriceBookID and isActive=true]);

        Map<ID,Map<ID,PricebookEntry>> map_prodID_PricebookID_PBEntryID = new Map<ID,Map<ID,PricebookEntry>>();
        Map<ID,PricebookEntry> map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
        for (PricebookEntry pbentry : map_PricebookEntry.values()){
            if(map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID)==null){
                map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
                map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
                map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
            }
            else {
                map_PricebookID_PBEntryID = map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID);
                map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
                map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
            }
        }   
        // this map is used to check for duplicated GPS already existed
		List<QuoteLineItem> lst_GPS_QuoteLineItem = new List<QuoteLineItem>([select id,PricebookentryID,Pricebookentry.Pricebook2Id,QuoteID from QuoteLineItem 
																				where QuoteID in :quotes_IDs and Pricebookentry.Pricebook2Id in :set_PriceBookID_GPS]);
        Map<ID,List<QuoteLineItem>> map_QuoteID_Quotelines_GPS = new Map<ID,List<QuoteLineItem>>();
        List<QuoteLineItem> tmp_qlines = new List<QuoteLineItem>();
        for (QuoteLineItem qline : lst_GPS_QuoteLineItem){
        	if(map_QuoteID_Quotelines_GPS.get(qline.QuoteID)==null){
        		tmp_qlines = new List<QuoteLineItem>();
				tmp_qlines.add(qline);
				map_QuoteID_Quotelines_GPS.put(qline.QuoteID,tmp_qlines);				        			
        	}
        	else{
        		tmp_qlines = map_QuoteID_Quotelines_GPS.get(qline.QuoteID);
        		tmp_qlines.add(qline);
				map_QuoteID_Quotelines_GPS.put(qline.QuoteID,tmp_qlines);
        	}
        } 
        //
        List<QuoteLineItem> lst_QuoteLineItemToInsert = new List<QuoteLineItem>();
        List<QuoteLineItem> lst_QuoteLineItemToDelete = new List<QuoteLineItem>();
        QuoteLineItem qline;
        //boolean exist_dup = false;                                                                        
        for (Quote qq : quotes){
            for (Pro_Service_Products__c psp : proserve_prods){
                //old logic
                //if (qq.Node_Count__c >= psp.Min_Nodes__c && qq.Node_Count__c <= psp.Max_Nodes__c){
                if (Switch_amount_exist = true){
                    if (qq.Switch_Total__c > psp.Min_Amount__c && qq.Switch_Total__c <= psp.Max_Amount__c){
                    	if(psp.Products_ID__c!=null && map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c)!=null && map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(qq.Pricebook2Id)!=null 
                    		&& map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c)!=null
                    		&& map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c))!=null ){
		                        qline = new QuoteLineItem (QuoteID=qq.ID,PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(qq.Pricebook2Id).ID,Quantity=psp.Qty__c,
		                                                    UnitPrice=map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice,
		                                                    New_Price__c=map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice,
		                                                    supportOPN_by_mapping__c=true);
		                        
		                        if(map_QuoteID_Quotelines_GPS.get(qq.ID) != null){
		                        	for (QuoteLineItem qli : map_QuoteID_Quotelines_GPS.get(qq.ID)){
		                        		if (qli.PricebookEntryId == qline.PricebookEntryId)
		                        			lst_QuoteLineItemToDelete.add(qli);	
		                        	}
		                        }
		                        lst_QuoteLineItemToInsert.add(qline);
		                        //for >600,000 amount, we have to add 2 products
		                        if (qq.Switch_Total__c <= 600000)
                        			break;
                    	}                                                                       
                    }
                    
                }
                //Handle UFM amount
                else {
                    if (qq.UFM_Total__c > psp.Min_Amount__c && qq.UFM_Total__c <= psp.Max_Amount__c){
						if(psp.Products_UFM_Only_ID__c!=null && map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c)!=null && map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(qq.Pricebook2Id)!=null
							&& map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c)!=null
							&& map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c))!=null){                    	
		                        qline = new QuoteLineItem (QuoteID=qq.ID, PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(qq.Pricebook2Id).ID,
		                                                    Quantity=psp.Qty_UFM_Only__c,UnitPrice=map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice,
		                                                    New_Price__c=map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(Quote_ENV.PriceBookMap.get(qq.New_Price_Book__c)).UnitPrice,
		                                                    supportOPN_by_mapping__c=true);
		                        
		                        if(map_QuoteID_Quotelines_GPS.get(qq.ID) != null){
		                        	for (QuoteLineItem qli : map_QuoteID_Quotelines_GPS.get(qq.ID)){
		                        		if (qli.PricebookEntryId == qline.PricebookEntryId)
		                        			lst_QuoteLineItemToDelete.add(qli);	
		                        	}
		                        }
		                        lst_QuoteLineItemToInsert.add(qline);
		                        
		                        //for >600,000 amount, we have to add 2 products
                    			if (qq.UFM_Total__c <= 600000)
                        			break; 
		                        
						}		                        
                    }
                }
            }       
        }
        //Insert GPS first, then delete, order is important 
        if(lst_QuoteLineItemToInsert.size()>0)
            insert lst_QuoteLineItemToInsert;
		if(lst_QuoteLineItemToDelete.size()>0)
            delete lst_QuoteLineItemToDelete;            
    //}
    //                       
}