trigger CreateCommunityUserOrContact on Case (before insert, before update)
{
    /*
    //This is to populate CaseType__c which is used in Report - Non-community Cases vs. Community Cases
    if(Trigger.isBefore && Trigger.isInsert){
        for(Case c: Trigger.new){
            c.CaseType__c = (c.Origin == 'Jive') ? 'Community Cases' : 'Non-community Cases';
        }
    }else if(Trigger.isBefore && Trigger.isUpdate){
        for(Case c: Trigger.new){
            if(c.Origin != Trigger.oldMap.get(c.id).Origin){
                c.CaseType__c = (c.Origin == 'Jive') ? 'Community Cases' : 'Non-community Cases';
            }
        }
    }
    */
    if(ENV.triggerShouldRun){
    	ENV.triggerShouldRun = false;
    
    if(Trigger.isBefore && Trigger.isInsert){
        //check custom setting if we need to create a contact or not    
        List<Case> listOfNewCases = Trigger.new;
        Integer numberOfCases = listOfNewCases.size();
        String web_email = '';
        if(numberOfCases == 1) {
        //put first element of list in Case object...
            Case c=listOfNewCases.get(0);   
            JiveCommunitySettings__c jcs;
            system.debug('PremiumSupportGroup__c'+c.PremiumSupportGroup__c);
            
            if (c.Jive_Author_Email__c != null && c.Status == 'New' && c.Origin == 'Jive') {
                
                Set<String> set_newAuthorEmailsOnCommUsr = new Set<String>();
                Set<String> set_newAuthorEmails = new Set<String>();
                Map<String,String>  map_newAuthorEmailNameOnCommUsr= new Map<String,String>();
                Map<String,String>  map_newAuthorEmailName= new Map<String,String>();
                Map<String,Id> map_Email_CommUsrId = new Map<String,Id>();
                Map<String,Id> map_Email_ContactID = new Map<String,Id>();
                jcs = JiveCommunitySettings__c.getValues('DefaultSetting');
                if(jcs==NULL) {
        
                    jcs=new JiveCommunitySettings__c
                    (  
                    Name ='DefaultSetting' ,
                    Community_Base_URL__c ='https://ec2.compute.amazonaws.com:8090',
                    Create_Contact__c     = false,
                    Jive_URL__c           = 'https://jive-community.jiveon.com'
                    ); 
                }
                system.debug('Custom setting'+jcs.Create_Contact__c);
        
                //put Jive_Author_Email__c from new cases in a set       
                set_newAuthorEmailsOnCommUsr.add( c.Jive_Author_Email__c );
             
                //put in map key = Jive_Author_Email__c Value=Jive_Author_Name__c
                map_newAuthorEmailNameOnCommUsr.put(c.Jive_Author_Email__c,c.Jive_Author_Name__c);
                  
                //if new contact needs to be created
                if(jcs.Create_Contact__c==true)  {  
            
                //put in set Jive_Author_Email__c 
                set_newAuthorEmails.add( c.Jive_Author_Email__c );
             
                //put in map key = Jive_Author_Email__c Value=Jive_Author_Name__c
                map_newAuthorEmailName.put(c.Jive_Author_Email__c,c.Jive_Author_Name__c);
        
            }
            
     
            /*
                //create new community user
                for (Jive_Community_User__c commUsr : [SELECT Id, Email__c FROM Jive_Community_User__c WHERE Email__c =:set_newAuthorEmailsOnCommUsr] )
                {
                    //key=commUsr.Email__c value=commUsr.Id
                    map_Email_CommUsrId.put(commUsr.Email__c,commUsr.Id);
            
                    //Remove all existing emails from the set when community user with same email already exists
                    set_newAuthorEmailsOnCommUsr.remove(commUsr.Email__c );
                
                    //Remove all existing emails from the map when community user with same email already exists
                    map_newAuthorEmailNameOnCommUsr.remove(commUsr.Email__c);
                }
            
                List<Jive_Community_User__c> list_communityUsersToInsert = new List<Jive_Community_User__c>();
            
                for ( String anEmail :set_newAuthorEmailsOnCommUsr) {
        
                    //first check if the key exists or not
                    if(map_newAuthorEmailNameOnCommUsr.containsKey(anEmail)){
                        list_communityUsersToInsert.add
                        ( 
                            new Jive_Community_User__c
                            (  
                    
                                Name = map_newAuthorEmailNameOnCommUsr.get(anEmail),
                                Last_Name__c   = map_newAuthorEmailNameOnCommUsr.get(anEmail),
                                Email__c       = anEmail
                            )
                        );
                    }
                }
        
                //check if list is not empty
                try {
        
                    if(list_communityUsersToInsert.size()>0) {
                        insert list_communityUsersToInsert;
                        System.debug('Community User inserted ');
                    }
                }
                catch(Exception ex) {
        
                    system.debug('Exception occurred while inserting community user'+ex.getMessage());
        
                }
        
                // update the map of exixting community users
                for ( Jive_Community_User__c aContact : list_communityUsersToInsert)
                {
                    map_Email_CommUsrId.put( aContact.Email__c, aContact.Id );
                }
                //community user insertion complete
                
                */
                
                
                //contact insertion
                map<String, Id> emails2AccountIds_Map = new map<String, Id>();
                set<String> emailsForContactsNotFound_Set = new set<String>();
                 
                if(jcs.Create_Contact__c==true) {
                    
                    cls_Contact_Trigger handler = new cls_Contact_Trigger();
                    
                    for ( Contact aContact : [SELECT Id, Email FROM Contact WHERE Email IN :set_newAuthorEmails] ){
                        //  Map of all key=contact Email value=contact Id
                        map_Email_ContactID.put( aContact.Email, aContact.Id );
                        //remove contact emails if contact with same email already exists
                        set_newAuthorEmails.remove( aContact.Email );
                        map_newAuthorEmailName.remove(aContact.Email);
           
                    }
                    
                    for (String email : set_newAuthorEmails) {
                    
                        if (!map_Email_ContactID.containsKey(email)) {
                        
                            emailsForContactsNotFound_Set.add(email);
                        }
                    }
                    
                    if (!emailsForContactsNotFound_Set.isEmpty()) {
                    
                        for (String theEmail : emailsForContactsNotFound_Set) {
                            
                            Account acc = new Account();
                            try {
                                
                                acc = handler.getAccountByEmailDomain(theEmail, null);
                            }
                            
                            catch(Exception e) {
                            
                            }
                            
                            if (acc.Id != null) {
                            
                                emails2AccountIds_Map.put(theEmail, acc.Id);                                
                            }
                        }
                    }
                    
                    else {
                    
                        c.ContactId = map_Email_ContactID.get(c.Jive_Author_Email__c);
                    }
                    
                    
                    if (!emails2AccountIds_Map.values().isEmpty()) {
                        
                        List<Contact> list_contactsToInsert = new List<Contact>();
                        if (emails2AccountIds_Map.containsKey(c.Jive_Author_Email__c)) {
                        
                            Contact con  = new Contact();
                            con.Email = c.Jive_Author_Email__c;
                            con.LastName = c.Jive_Author_Name__c;
                            con.AccountId = emails2AccountIds_Map.get(c.Jive_Author_Email__c);
                            con.Contact_Source__c = 'Jive';
                            insert con;
                            c.ContactId = con.Id;
                        }
                    }
                
                    
                    if (c.ContactId == null) {
                    
                         c.ContactId = jcs.Jive_General_Contact_ID__c;
                     }
                    
                  
                  /*
                    Account Jive_Unknown_Account = [SELECT ID FROM Account WHERE Name =: 'JIVE UNKNOWN'];
                   
                    for ( String anEmail : set_newAuthorEmails ){
                
                        if(map_newAuthorEmailName.containsKey(anEmail)) {
                        
                            Contact con  = new Contact();
                            con.LastName = map_newAuthorEmailName.get(anEmail);
                            con.Email    = anEmail;
                            
                            // Elad Kaplan 28/07/2015   Added a functionality to assign an Account to the created Contact
                            try {    
                                con.AccountId = handler.getAccountByEmailDomain(con.ID, null).Id;
                            }
                            
                            catch(Exception e) {
                                
                                system.debug('Could not assign Account : ' + e.getMessage());
                            }
                            
                            if (con.AccountId == null) {
                            
                                con.AccountId = Jive_Unknown_Account.Id; 
                                c.SuppliedEmail = 'wefsef@swefs.com';
                            }
                            //list_contactsToInsert.add(con);
                        }
                    }
                
                  */ 
                
                //  Map the new contact in map
                
    
            } //end of if
        
            //contact insertion complete
        
            // Populate the contact ID based on the email address
            //check the custom setting again and based on that populate contact name or cummunity user contact name
            //if ( c.Jive_Author_Email__c!=NULL &&  c.Status == 'New' && c.Origin == 'Jive' ) {
          /*      
            c.Community_Contact_Name__c=map_Email_CommUsrId.get(c.Jive_Author_Email__c);
            
            System.debug('contact name'+c.Community_Contact_Name__c);
            
            System.debug('Community contact name in case updated');
            
            if(jcs.Create_Contact__c==true) {
                    c.ContactId = map_Email_ContactID.get(c.Jive_Author_Email__c );
            
                    System.debug('contact name in case updated');
            }
            
            else {
               
                Integer count=[SELECT count() FROM Contact WHERE Email =:c.Jive_Author_Email__c limit 1] ;
                if(count==1)
                {
                    Contact aContact = [SELECT Id FROM Contact WHERE Email =:c.Jive_Author_Email__c limit 1] ;
                    c.ContactId  = aContact.ID;
                     
                }
            }
            
            */
        }
        
        }
    }
    }  
}