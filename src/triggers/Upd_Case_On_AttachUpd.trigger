trigger Upd_Case_On_AttachUpd  on AttachmentIntegration__c (after insert) {

// update case on attachment added from Redmine

List<id> UpdCaseIds = new List<id>();
map<Id,Case> UpdCases ;
list<AttachmentIntegration__c> lst_FromRedMine = new list<AttachmentIntegration__c>();
list<Case> lst_CaseToUpdate = new list<Case>();

                           
for(AttachmentIntegration__c A:trigger.New)
{
 
  if(trigger.isinsert && A.source__c == 'Redmine')
 {
    UpdCaseIds.add(A.Case_Id__c);
    AttachmentIntegration__c temp = new AttachmentIntegration__c(Id=A.Id,Case_Id__c=A.Case_Id__c);
    lst_FromRedMine.add(temp);
  
 }

}      
          
             
                               
if(!lst_FromRedMine.IsEmpty()) 
{
  system.debug('==>lst_FromRedMine: '+lst_FromRedMine);
 UpdCases = new map<Id,Case>([Select Id, Next_Attachment_Integration_Number__c FROM Case C Where C.id in:UpdCaseIds]);
  for(AttachmentIntegration__c atin:lst_FromRedMine)
  {
    
    if(UpdCases.get(atin.Case_Id__c)!=null)
    {
      
      atin.Attachment_Number__c = UpdCases.get(atin.Case_Id__c).Next_Attachment_Integration_Number__c;
      
      if(UpdCases.get(atin.Case_Id__c).Next_Attachment_Integration_Number__c == null )
      {
        UpdCases.get(atin.Case_Id__c).Next_Attachment_Integration_Number__c =0;
      }
      
      UpdCases.get(atin.Case_Id__c).Next_Attachment_Integration_Number__c =UpdCases.get(atin.Case_Id__c).Next_Attachment_Integration_Number__c+1;
     
      lst_CaseToUpdate.add(UpdCases.get(atin.Case_Id__c));
      
    }
  }
  system.debug('==>lst_FromRedMine: '+lst_FromRedMine);
  
  update lst_FromRedMine;
  update lst_CaseToUpdate;
}        

}