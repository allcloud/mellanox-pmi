trigger Upd_Case_Internally_Opened on Case (before insert) {

List<Profile> ProfName=[select Name from profile where id = :userinfo.getProfileId()];

for(Case c:trigger.New){ 
      if(c.recordTypeId != ENV.CSI_case && c.state__c=='Open' && c.origin != 'Phone') 
      {                         
       if ((ProfName[0].Name =='System Support Profile')||(ProfName[0].Name =='FAE Support Profile') ||
       (ProfName[0].Name =='AE Profile') ||(ProfName[0].Name =='System Support Manager' )||       
       (ProfName[0].Name == 'Mellanox Sales Rep'))
           {   
               c.State__c = 'Assigned';
               c.Status = 'Assigned';
               c.Assignee__c = userinfo.getUserId();
               c.ownerid=userinfo.getUserId();
         }  
      }
  }
}