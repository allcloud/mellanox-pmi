trigger upd_Product_on_support_delete on Support_Item__c (after delete) {

List<id> UpdProducts = new List<id>();
Map<id,integer> NumOfSup = new Map<id,integer>();
List<product2> UpdProds = new List<product2>();


for(Support_Item__c SI:trigger.old)
{
 UpdProducts.add(SI.MlxProduct__c);
 if(!NumOfSup.containsKey(SI.MlxProduct__c))
  {NumOfSup.put(SI.MlxProduct__c,1);  }
 else
 {NumOfSup.put(SI.MlxProduct__c,NumOfSup.get(SI.MlxProduct__c)+1);}
system.debug('UpdProducts - '+ UpdProducts);
}

 
                   
 for(product2 Pr:[select id, num_of_sup__c from product2 where id in :UpdProducts] )
 {  
  if(Pr.num_of_sup__c >0)       
      
  {       
     Pr.num_of_sup__c = Pr.num_of_sup__c - NumOfSup.get(Pr.Id);          
     UpdProds.add(Pr);            
  }
 }            
  if(!UpdProds.isempty())                   
   { update(UpdProds);  }       
 

}