trigger Case_before_Insert_before_update on Case (before Insert, before update,after update) {
    
    
    if(ENV.triggerShouldRunCase ){
       
          
        List<id> CaseContacts = new List<id>();
        List<id> TechOwnersIds = new List<id>();
        //map<id,user> ConUser =new map<id,user>();
        map<id,contact> RelCon =new map<id,contact>();
        map<id,Profile> ProfName = new map<id,Profile>();
        List<id> UpdAccountInfo = new List<id>();
        List<Account> AccountEmails = new List<Account>();
        map<id,string> AccountToSalesEmail = new map<id,string>();
        map<id,string> AccountToBDEmail = new map<id,string>();
        map<id,string> AccountToVTEmail = new map<id,string>();
        map<id,string> CaseAEclass = new map<id,string>();
        map<id,string> CaseAEsubclass = new map<id,string>();
        map<ID,AE_Queue_Manager__c> map_IdToAEmanager ;
        list<case> AEcases = new List<case>();
        clsCase caseHandler;
        
        
        
        for (Case cs:trigger.new)
        {
            if (trigger.isBefore && trigger.isupdate && cs.SE_Priority__c != trigger.oldmap.get(cs.id).SE_Priority__c)
            {
                cs.Related_SE__c = UserInfo.getUserId();
            }
        }
        
        
        
        
        for(case c:trigger.new)
        {
            //updates the ID of ST1 shutdown record
            /*   c.ST1_Controller__c = ENV.ST1_shut_downID;
            // end of update
            // Validation that the new owner is not system queue / ST1 queue / ST1 user
            if( trigger.isupdate && (c.OwnerID ==ENV.ST1 ||c.OwnerID ==ENV.ST1AutoUnassigned_Queue ||c.OwnerID ==ENV.UnassignedST1 ) && c.ST1_Controller__r.ST1_Shutdown__c)
            {c.adderror('You can not change the owner to be system queue - ST1 is shut down, Please contact you administrator');}
            // end of validation
            */
            
            if(trigger.isBefore) {
                
                if (trigger.isupdate &&  (trigger.oldmap.get(c.id).Escalation_Q_Owner__c == c.Escalation_Q_Owner__c )&&
                    ((trigger.oldmap.get(c.id).AE_Class__c != NULL  && trigger.oldmap.get(c.id).AE_Class__c != c.AE_Class__c) ||
                (trigger.oldmap.get(c.id).AE_Sub_Class__c != NULL && trigger.oldmap.get(c.id).AE_Sub_Class__c != c.AE_Sub_Class__c))) {
                    
                    CaseAEclass.put(c.id,c.AE_Class__c);
                    CaseAEsubclass.put(c.id,c.AE_Sub_Class__c);
                    AEcases.add(c);
                }
                
                
                if (Trigger.isInsert || (Trigger.isUpdate && c.ContactId !=NULL && c.state__c=='Open'&& trigger.oldMap.get(c.id).state__c !='Assigned')) {
                    
                    CaseContacts.add(c.contactid);
                }
                
                if((Trigger.isInsert && c.AccountId != NULL) || (Trigger.isUpdate && c.AccountId !=NULL && trigger.oldMap.get(c.id).AccountId != c.AccountId )) {
                    
                    UpdAccountInfo.add(c.AccountId );
                }
                
                
                if(c.case_type__c == 'System case') {
                    
                    if(c.priority == 'low' && c.business_impact__c == 'low') {
                        
                        c.esc_priority__c = 'Low';
                    }
                    
                    if(c.priority == 'high' || c.priority == 'fatal' || c.business_impact__c == 'high'||c.business_impact__c == 'Critical') {
                        
                        c.esc_priority__c = 'High';
                    }
                    
                    if ((c.business_impact__c == 'medium' && (c.priority == 'medium'||c.priority == 'low'))
                    || (c.priority == 'medium' && (c.business_impact__c == 'low' ||c.business_impact__c == 'low'))) {
                        
                        c.esc_priority__c  = 'Medium';
                    }
                    
                    if (trigger.isupdate && c.What_is_the_Impact_in_the_network__c !=trigger.oldmap.get(c.id).What_is_the_Impact_in_the_network__c ) {
                        
                        if(C.What_is_the_Impact_in_the_network__c.contains('Critical')) {
                            
                            C.priority = 'Fatal';
                        }
                        if(C.What_is_the_Impact_in_the_network__c.contains('High')) {
                            
                            C.priority = 'High';
                        }
                        if(C.What_is_the_Impact_in_the_network__c.contains('Medium')) {
                            
                            C.priority = 'Medium';
                        }
                        if(C.What_is_the_Impact_in_the_network__c.contains('Low')) {
                            
                            C.priority = 'Low';
                        }
                    }
                }
            }
            
        } // end of for
        
        
        //****************************** Changint the AE Q Owner based on class
        if(!AEcases.isempty())
        {
            
             map_IdToAEmanager = new map<ID,AE_Queue_Manager__c>([ Select a.SystemModstamp, a.Sub_Class__c, a.Q_Owner__c, a.OwnerId, a.Name, a.Id,a.Class__c,
            (Select Id, Name, AE_Queue_Manager__c, Owner__r.name From Optional_AE_Owner__r) From AE_Queue_Manager__c a Where a.class__c  in:CaseAEclass.values() and a.Sub_Class__c in:CaseAEsubclass.values()]);
            
            for( AE_Queue_Manager__c  AE : map_IdToAEmanager.values())
            
            {
                for(case cs:AEcases)
                {
                    if(cs.AE_class__c == AE.Class__c && cs.AE_sub_class__c == AE.Sub_Class__c)
                    {
                        cs.Escalation_Q_Owner__c = AE.Q_Owner__c;
                        cs.Optional_AE_owners__c = NULL;
                        for(Optional_AE_Owner__c op_AE: AE.Optional_AE_Owner__r)
                        {
                            if(cs.Optional_AE_owners__c == NULL)
                            {cs.Optional_AE_owners__c  = op_AE.Owner__r.name;}
                            else { cs.Optional_AE_owners__c  = cs.Optional_AE_owners__c +', '+ op_AE.Owner__r.name;}
                        } // for optional owner
                        
                        
                    } // if
                } // for case
                
            } // for AE Q manager
            
            
        } // if AE cases
        
        //***************************************************************
        if(!UpdAccountInfo.isempty())
        {
            AccountEmails = [Select id, a.Sales_Owner__r.Email, a.BusDev_Owner__r.Email,VT_Region_map__r.Sales_Director__r.email From Account a where id in:UpdAccountInfo];
            for(Account Acc:AccountEmails)
            {
                if(Acc.Sales_Owner__r.Email != NULL)
                { AccountToSalesEmail.put(Acc.id, Acc.Sales_Owner__r.Email );}
                if(Acc.BusDev_Owner__r.Email != NULL)
                { AccountToBDEmail.put(Acc.id, Acc.BusDev_Owner__r.Email );}
                if(Acc.VT_Region_map__r.Sales_Director__r.email != NULL)
                { AccountToVTEmail.put(Acc.id, Acc.VT_Region_map__r.Sales_Director__r.email );}
            }
        }
        
        
        if(CaseContacts.size()>0)
        {
            
            
            for(Contact Cont: [Select c.Deny_Access__c, c.Account.Technical_owner__c, c.Contact_Type__c, c.Name, c.Technical_Owner__r.Email, c.TechOwner_Email__c, c.Technical_Owner__r.manager_email__c, c.Technical_Owner__r.Name, c.Technical_Owner__c,c.Path_through__c From Contact c WHERE id in :CaseContacts and IsDeleted = FALSE])
            {
                RelCon.put(Cont.id,Cont);
            }
            
        }
        
        
       
        // ******************************************************************************************
        if(trigger.isinsert)
        {
            
            for(Case css : Trigger.New)
            {
                //before insert triggers
                System.debug('before insert: ');
                System.debug('contact Id: ' +  css.ContactId);
                System.debug('state: ' +  css.state__c);
                
                
                //---------------------Upd_Assignee_for_Design_In-------------------------------------------------------------------
                if(css.ContactId !=NULL && css.state__c=='Open')
                {
                    
                    System.debug('Inside first if ');
                    
                    // System.debug('RElated contact  ' + RelCon.get(css.contactid));
                    // System.debug('RElated contact techOwner  ' + RelCon.get(css.contactid).technical_owner__c);
                    
                    if ( RelCon.get(css.contactid) != null && !(RelCon.get(css.contactid).technical_owner__c== NULL) )
                    {
                        
                        System.debug('Inside second if ');
                        
                        css.Is_Design_Customer__c=1;
                        
                    }
                }              //if
            }                 // for
        }                   // trigger.isinsert
        
        
        //---------------------------Upd_Case_Contact_Access-------------------------------------
        for(Case css:Trigger.New){
            if(AccountToSalesEmail.ContainsKey(css.AccountId))
            {css.Account_SO_email__c = AccountToSalesEmail.get(css.AccountId);}
            if(AccountToBDEmail.ContainsKey(css.AccountId))
            {css.Account_BD_email__c = AccountToBDEmail.get(css.AccountId);}
            if(AccountToVTEmail.ContainsKey(css.AccountId))
            {css.Account_VT_email__c = AccountToVTEmail.get(css.AccountId);}
            
            system.debug('css.ownerid : ' + css.ownerid);
            system.debug('css.RecordTypeId : ' + css.RecordTypeId);
            system.debug('ENV.CaseTypeRMA : ' + ENV.CaseTypeRMA);
            system.debug('RelCon.get(css.contactid) : ' + RelCon.get(css.contactid));
            //system.debug('RelCon.get(css.contactid).technical_owner__c : ' + RelCon.get(css.contactid).technical_owner__c);
           // system.debug('RelCon.get(css.contactid).Contact_Type__c : ' + RelCon.get(css.contactid).Contact_Type__c);
            //system.debug('RelCon.get(css.contactid).Account.Technical_owner__c : ' + RelCon.get(css.contactid).Account.Technical_owner__c);
                
             
            if(trigger.isBefore && trigger.isupdate){
                 
                clsCase handler = new clsCase();
                handler.updateNewCasesPriorityToFatalFromParentCases(trigger.New);
                //--------------Is_Design_In-----------------------
                if(css.ContactId !=NULL && css.state__c=='Open'&& trigger.oldMap.get(css.id).state__c !='Assigned')
                {
                    
                    if (css.RecordTypeId != ENV.CaseTypeRMA && RelCon.get(css.contactid) != null &&
                        ((RelCon.get(css.contactid).technical_owner__c != NULL) ||  ((RelCon.get(css.contactid).Contact_Type__c == 'Mellanox Silicon Design-In customer'|| RelCon.get(css.contactid).Contact_Type__c== 'Ezchip Design-In customer') && RelCon.get(css.contactid).Account.Technical_owner__c != NULL)))
                    {
                        system.debug('css.Int_Open_Mail_Design_In__c : ' + css.Int_Open_Mail_Design_In__c);
                        if(css.Int_Open_Mail_Design_In__c != TRUE)
                        {
                            system.debug('RelCon.get(css.contactid) : ' + RelCon.get(css.contactid));
                            if (RelCon.get(css.contactid) != null) {
                                
                                
                                system.debug('RelCon.get(css.contactid).technical_owner__c : ' + RelCon.get(css.contactid).technical_owner__c);
                                system.debug('RelCon.get(css.contactid).Account.technical_owner__c : ' + RelCon.get(css.contactid).Account.technical_owner__c);
                                if(RelCon.get(css.contactid).technical_owner__c != null)
                                css.ownerid=RelCon.get(css.contactid).technical_owner__c;
                                else
                                if (RelCon.get(css.contactid).Account.technical_owner__c != null) {
                                    css.ownerid=RelCon.get(css.contactid).Account.technical_owner__c;
                                }
                                
                                 
                            }
                            css.FAE_manager_email__c = RelCon.get(css.contactid).technical_owner__r.manager_email__c;
                            String[] toAddresses = new String[]{RelCon.get(css.contactid).TechOwner_Email__c};
                            
                            
                            string tmpAddresses = RelCon.get(css.contactid).technical_owner__r.manager_email__c;
                            system.debug('BLAT tmpAddress TRIGGER: '+ tmpAddresses);
                            system.debug('BLAT toAddress TRIGGER: '+ toAddresses);
                            Util.SendSingleEmail(toAddresses, null, 'supportadmin@mellanox.com', 'Mellanox Support Admin', ENV.TemplateNewCaseOpened_Design_In, css.id, '', '');
                        }
                        
                        css.Int_Open_Mail_Design_In__c=TRUE;
                          
                        
                    }
                    
                } //if
            }//trigger is update
            
            system.debug('css.ownerid after : ' + css.ownerid);
        } // for
        
        
        
        
        //if (!ENV.caseTriggerRanOnce) {
            
           // if (trigger.isAfter && trigger.isUpdate) {
                
                //caseHandler = new clsCase();
                //caseHandler.createNewFMDiscussionWhenCaseReolutionIsPopulated(trigger.New, trigger.oldMap);
               // ENV.caseTriggerRanOnce = true;
           // }
        //}
    }
}

//********************************************************************************