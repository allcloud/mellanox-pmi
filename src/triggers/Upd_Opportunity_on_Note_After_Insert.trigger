trigger Upd_Opportunity_on_Note_After_Insert on Note (after insert) {
list<id> noteParents = new list<id>();
 map<id,Opportunity> OppId_StatCom = new map<id,Opportunity>();
for(Note nt:trigger.new )
 {
   noteParents.add(nt.ParentId); 	
 }

 for(Opportunity Opp:[select Status_Comment__c, id from Opportunity where Id in :noteParents])
  {
	OppId_StatCom.put(Opp.id, Opp);
	
  }

 for(Note N:trigger.new)
  {  
	if(OppId_StatCom.containsKey(N.ParentId))
	{
		OppId_StatCom.get(N.ParentId).Status_Comment__c = N.Title + '\n\n' + N.Body;
		Update(OppId_StatCom.get(N.ParentId));
	}
  }

}