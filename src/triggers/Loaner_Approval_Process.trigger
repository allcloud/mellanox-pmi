trigger Loaner_Approval_Process on Loaner_Request__c (after insert, after update) {
//Submit records for approval  the if condition matches with entry criteria  

    for(Loaner_Request__c LR: Trigger.new)     
      {   
         if(trigger.isupdate &&     
               ((LR.Num_of_Products__c>0 && trigger.oldmap.get(LR.id).Num_of_Products__c==0)||     
                (LR.Status__c == 'Submitted' && trigger.oldmap.get(LR.id).Status__c== 'Draft')) ) 
          {    
           if (LR.Status__c == 'Submitted'){   // 
             approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
             req1.setComments('Submitting Request for approval.'); 
             req1.setObjectId(LR.id);
             //req1.setNextApproverIds(idList);
             approval.ProcessResult result = Approval.process(req1); 
             System.assert(result.isSuccess());  
             System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
             
           }
          }
            
         /* if(trigger.isupdate) 
          {     
           if( OppReg.Registration_Status__c == 'Approved' &&             
               OppReg.Discount_Granted__c > trigger.oldmap.get(OppReg.id).Discount_Granted__c &&               
               trigger.oldmap.get(OppReg.id).Discount_Granted__c>0 )
              {
               approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
               req1.setComments('Approval required since Discount Granted was changed'); 
               req1.setObjectId(OppReg.id);
               //req1.setNextApproverIds(idList);
               approval.ProcessResult result = Approval.process(req1); 
               System.assert(result.isSuccess());  
               System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
               
             }
           }*/

 }
}