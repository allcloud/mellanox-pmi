trigger Project_Member_after_insert on Project_Member__c(after insert) {

 public static Map<id,string> PMtoUserType = new Map<id,string>(); 
  
 if (Trigger.isInsert) {
 
 List<Milestone1_Project__Share> sharesToCreate = new List<Milestone1_Project__Share>();
 List<EntitySubscription> FollowersList = new List<EntitySubscription>();
 Map<id,List<id>> ProjectToMem = new Map<id,List<id>>();
 Map<id,List<Milestone1_Milestone__c>> ProjectToMSlist = new Map<id,List<Milestone1_Milestone__c>>();
 Map<id,List<id>> ProjectToTasklist = new Map<id,List<id>>();
 List<Id> ProjectIds = new List<Id>();
 List<string> ProjectIdsTxt = new List<string>();
 list<id> Memlist = new list<id>();
 list<Milestone1_Milestone__c> MSlist = new list<Milestone1_Milestone__c>();
 list<id> Tsklist = new list<id>();
 list<id> FollowObjects = new list<id>();
 set<id> Objects = new set<id>();
 map<id,set<id>> ExistingFollowers = new map<id,set<id>>();
 List<Milestone1_Milestone__Share> MSsharesToCreate = new List<Milestone1_Milestone__Share>();

 
 for (Project_Member__c  contact : Trigger.new) 
 {
// System.debug('CommunityID' + ConnectApi.Communities.getCommunities());

 if(contact.Project_Member__c != NULL)
  { 
    ProjectIds.add(contact.Project__c);
    ProjectIdsTxt.add(String.ValueOf((contact.Project__c)).left(15));
    FollowObjects.add(contact.Project__c);
    PMtoUserType.put(contact.Project_Member__c, contact.Member_UserType__c);   
    
    if(!ProjectToMem.containsKey(contact.Project__c))
     {
      Memlist = new list<id>();
      Memlist.add(contact.Project_Member__c);
      ProjectToMem.put(contact.Project__c,Memlist);
     }else
     {
      Memlist = ProjectToMem.get(contact.Project__c);
      Memlist.add(contact.Project_Member__c);
      ProjectToMem.put(contact.Project__c,Memlist); 
     } // else
   } //if
    
 
 //***************** Adding sharing rule for community user
  
   Milestone1_Project__Share projectShare = new Milestone1_Project__Share();
   system.debug('im here' + contact.Project__c +',' +  contact.Project_Member__c);
   projectShare.ParentId = contact.Project__c;
   projectShare.UserOrGroupId = contact.Project_Member__c;
   projectShare.AccessLevel = 'Edit';
   sharesToCreate.add(projectShare);
   
   
 }//for
 
//**************************Adding Followers to Milestones/Tasks**************** 


    
 if( !ProjectIds.isempty())
 {  
  for( task tsk:[Select id,ProjectId__c from task where ProjectId18__c in:ProjectIds])
  {
    if(!ProjectToTasklist.containsKey(tsk.ProjectId__c))
     {
      Tsklist = new list<id>();
      Tsklist.add(tsk.id);
      ProjectToTasklist.put(tsk.ProjectId__c,Tsklist);
     }else
     {
      Tsklist = ProjectToTasklist.get( tsk.ProjectId__c);
      Tsklist.add(tsk.id);
      ProjectToTasklist.put(tsk.ProjectId__c,Tsklist); 
     }
     
    FollowObjects.add(tsk.id);
    
  }  
  
  
  for( Milestone1_Milestone__c ms:[Select id,Project__c, ispublic__c from Milestone1_Milestone__c where Project__c in:ProjectIds])
  {
    if(!ProjectToMSlist.containsKey(ms.Project__c))
     {
      MSlist = new list<Milestone1_Milestone__c>();
      MSlist.add(ms);
      ProjectToMSlist.put(ms.Project__c,MSlist);
     }else
     {
      MSlist = ProjectToMSlist.get( ms.Project__c);
      MSlist.add(ms);
      ProjectToMSlist.put(ms.Project__c,MSlist); 
     }
     
    FollowObjects.add(ms.id);
    
     
  } 
  
 //********** list of existing followers
for(EntitySubscription es: [select SubscriberId, ParentId  from EntitySubscription where ParentId in: FollowObjects LIMIT 1000 ])
  {
    if(!ExistingFollowers.containsKey(es.SubscriberId))
    {Objects = new set<id>();}
    else
    {Objects =  ExistingFollowers.get(es.SubscriberId);}
    
    Objects.add(es.ParentId );
    ExistingFollowers.put(es.SubscriberId,Objects);
   
  } 
  
  
  
if(ProjectToMSlist!= NULL && !ProjectToMSlist.isempty()) // if there is a milestone
 {
  
  for(Id prId:ProjectToMSlist.keyset())
   {
     for(Milestone1_Milestone__c MS:ProjectToMSlist.get(prId))
     {
       for(Id Pmem:ProjectToMem.get(prId))
        {
          if(PMtoUserType.get(pmem)!= 'Standard' && MS.ispublic__c == true)
          {
           Milestone1_Milestone__Share MsShare = new Milestone1_Milestone__Share();
           MsShare.ParentId = MS.id;
           MsShare.UserOrGroupId = pmem;
           MsShare.AccessLevel = 'Edit';
           MSsharesToCreate.add(MsShare);
          }
         
          if(!ExistingFollowers.containsKey(Pmem)|| !ExistingFollowers.get(Pmem).contains(MS.id))
          {
           addFollower(Pmem, MS.id, FollowersList );
          }
        }//Pmem
     }  //Milestone
   }  //Project
 } //if(!ProjectToMSlist.isempty())  
 
 
if(ProjectToTasklist!= NULL && !ProjectToTasklist.isempty()) // if there is a task
 {  
  for(Id prId:ProjectToTasklist.keyset())
   {
     for(Id ts:ProjectToTasklist.get(prId))
     {
       for(Id Pmem:ProjectToMem.get(prId))
        {
         if(!ExistingFollowers.containsKey(Pmem)|| !ExistingFollowers.get(Pmem).contains(ts))
         {
         addFollower(Pmem, ts, FollowersList );
         } 
        }//Pmem
     }  //Milestone
   }  //Project
 } //if(!ProjectToTasklist.isempty()) 
 
 
for(Id prId:ProjectToMem.keyset()) 
{
 for(Id Pmem:ProjectToMem.get(prId))
  {
   if(!ExistingFollowers.containsKey(Pmem)|| !ExistingFollowers.get(Pmem).contains(prId))
    {
      addFollower(Pmem, prId, FollowersList );
     }
   } // for pmem
 }         
 insert( FollowersList);
  
 } 


    
 
//**************************Adding to Sharing Rule****************** 
  List<Database.SaveResult> sr_list = Database.insert(sharesToCreate,false);
  List<Database.SaveResult> ms_list = Database.insert(MSsharesToCreate,false); 
 
  for(Database.SaveResult sr:sr_list)
  {  
    // Process the save results.  
        if(sr.isSuccess()){
         // Indicates success   
         // return true;
      }
       else {
         // Get first save result error.
         Database.Error err = sr.getErrors()[0];
         
         // These sharing records are not required and thus an insert exception is acceptable.     
         if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
             err.getMessage().contains('AccessLevel')){
            // Indicates success.   
          //  return true;
         }
         else{
            // Indicates failure. 
            //  return false;
            }
          }
    }
      
  }
  
  
  
  public void addFollower(id Follower, id FollowObject, list<EntitySubscription>FollowersList )
    {
         EntitySubscription Fl = new EntitySubscription();
         if(PMtoUserType.get(Follower)!= 'Standard')
          {Fl.NetworkId = ENV.CommunityId;}
         Fl.SubscriberId = Follower;
         Fl.ParentId = FollowObject;
         FollowersList.add(Fl);
    } 
    
 
 } 

//test class----------------------------------

/*
private class Test_PpojectMember {

static testMethod void testManualShareRead(){
      // Select users for the test.
      List<User> users = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
      Id User1Id = users[0].Id;
      Id User2Id = users[1].Id;
   
      // Create new job.
      CLS_ObjectCreator cls = new CLS_ObjectCreator();
      Account acc = cls.createAccount();
      acc.name = 'ProjAccount';
      insert(acc);
      Milestone1_Project__c Proj = cls.CreateMPproject( Acc.id );
      insert proj ;
      Project_Member__c pm = cls.createProjectMember(prpj.id, User1Id);    
      inser pm;          
      // Insert manual share for user who is not record owner.
      System.assertEquals(manualShareRead(proj.Id, user2Id), true);
   
      // Query job sharing records.
      List<Job__Share> jShrs = [SELECT Id, UserOrGroupId, AccessLevel, 
         RowCause FROM job__share WHERE ParentId = :j.Id AND UserOrGroupId= :user2Id];
      
      // Test for only one manual share on job.
      System.assertEquals(jShrs.size(), 1, 'Set the object\'s sharing model to Private.');
      
      // Test attributes of manual share.
      System.assertEquals(jShrs[0].AccessLevel, 'Read');
      System.assertEquals(jShrs[0].RowCause, 'Manual');
      System.assertEquals(jShrs[0].UserOrGroupId, user2Id);
      
      // Test invalid job Id.      delete j;   
   
      // Insert manual share for deleted job id. 
      System.assertEquals(manualShareRead(j.Id, user2Id), false);
   }  

}

*/