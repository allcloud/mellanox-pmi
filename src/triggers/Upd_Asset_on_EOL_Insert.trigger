trigger Upd_Asset_on_EOL_Insert on EOL__c (after insert) {

map<string, date> EOS = new map<string, date> ();
List<string> NewProdNums = new List<string>();
for(EOL__c eol:trigger.new)
{NewProdNums.add(eol.name);
 EOS.put(eol.name, eol.EOS_date__c);
}
  
String query1 = 'select id,Part_Number__c,EOL_check__c, EOS_date__c from Asset2__c where ( Part_Number__c in '+ENV.getStringsForDynamicSoql(NewProdNums)+')';

   EOLAssetReassignment reassign = new EOLAssetReassignment();
   reassign.query= query1;              
   reassign.isinsert = 1;
   reassign.EOS = EOS;
   ID batchprocessid = Database.executeBatch(reassign);
                                                                                                  

}