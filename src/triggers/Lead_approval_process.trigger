trigger Lead_approval_process on Lead (after insert, after update) {



//Submit records for approval  the if condition matches with entry criteria  
    for(Lead ld: Trigger.new)     
      {   
         if(trigger.isupdate && ld.support_lead_status__c == 'New' && ld.sales_director__c != NULL &&
           (ld.LeadSource == 'Web - UFM Evaluation Registration') && (ld.SN__c == null)) 
          {    
             
             approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
             req1.setComments('Submitting request for approval.'); 
             req1.setObjectId(ld.id);
             //req1.setNextApproverIds(idList);
             approval.ProcessResult result = Approval.process(req1); 
             System.assert(result.isSuccess());  
             System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
             
           
          }

  }

/*

  Developed for License portal

Submit records for approval  the if condition matches with entry criteria  
    for(Lead ld: Trigger.new) {     
       
        if ( trigger.isupdate && ld.support_lead_status__c == 'New' && ld.sales_director__c != NULL &&
             ld.LeadSource == 'Web - UFM Evaluation Registration' && ( ld.SN__c == null 
             || ( ld.Asset_Status__c != null && !ld.Asset_Status__c.EqualsIgnoreCase('Out Of Warranty'))) ) {
              
             approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
             req1.setComments('Submitting request for approval.'); 
             req1.setObjectId(ld.id);
             approval.ProcessResult result;
            
            try {   
                 result = Approval.process(req1); 
            }
            catch(Exception e) {
                
                 system.debug('Error in approval' + e.getMessage());
            }
            
        }
    } */
}