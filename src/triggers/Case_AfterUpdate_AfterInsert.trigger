trigger Case_AfterUpdate_AfterInsert on Case (after insert, after update) 
{
/*
 Map<case,id> CaseProject = new Map<case,id>();

 List<Case_in_Project__c> CP_toInsert = new List<Case_in_Project__c>();

  for(case cs:trigger.new)
  {
   if((trigger.isinsert && cs.Related_Project__c != Null) ||
       (trigger.isupdate && cs.Related_Project__c != NULL && cs.Related_Project__c != trigger.oldmap.get(cs.id).Related_Project__c)) 
     {
       CaseProject.put(cs, cs.Related_Project__c);
      }
   }    
  
 if(!CaseProject.isempty())
 
 {
   for(case csp:CaseProject.Keyset())
   
   {
     Case_in_Project__c CP = New Case_in_Project__c();
     CP.name = csp.CaseNumber;
     CP.Case__c = csp.Id;
     CP.Project__c = CaseProject.get(csp);
     CP_toInsert.add(CP); 
     
   }
   
  insert(CP_toInsert); 
 
 }
*/

	if (trigger.isAfter && trigger.isUpdate) {
		
		clsCase handler = new clsCase();
		handler.addCaseTeamMembersOnCaseUpdateToFatal(trigger.New, trigger.oldMap);
		handler.updateCaseRelatedCasesPriority(trigger.New, trigger.oldMap);
		handler.addCaseTemMemberWhenKoturaIsChecked(trigger.New, trigger.oldMap);
	}
	
	else if (trigger.isAfter && trigger.isInsert) {
		
		clsCase handler = new clsCase();
		handler.updateCaseRelatedCasesPriority(trigger.New, null);
		handler.addCaseTemMemberWhenKoturaIsChecked(trigger.New, null);
		handler.sendPushNotificationOnNewMobileCase(trigger.New);
	}

}