trigger Asset_before_Insert on Asset2__c (before insert,before update, after insert) {

map<string, date> EOLs = new map<string,date>();
list<string> IsEOLPart  = new list<string>();
list<Asset2__c> TermAssets  = new list<Asset2__c>();
set<string> month12AssetsSuf = new set<string>();
set<string> month24AssetsSuf = new set<string>();
set<string> month36AssetsSuf = new set<string>();
set<string> month48AssetsSuf = new set<string>();
set<string> month60AssetsSuf = new set<string>();
set<string> month12Assets = new set<string>();
set<string> month24Assets = new set<string>();
set<string> month36Assets = new set<string>();

list<asset2__c> MyMellanoxAssets = new list<asset2__c>();

	if (Trigger.isBefore) {
		
		for(Asset2__c Ast:trigger.new)
		{
		 if(trigger.isinsert && Ast.Part_Number__c != NULL)
		  {
		    IsEOLPart.add(Ast.Part_Number__c);
		  }
		  
		  
		  if((trigger.isinsert && Ast.MyMellanox_Asset__c == true)||
		      (trigger.isupdate && Ast.MyMellanox_Asset__c == true && Ast.Contract2__c!=trigger.oldmap.get(Ast.id).Contract2__c))
		  
		  {   MyMellanoxAssets.add(Ast);}
		  
		 if(Ast.Asset_Type__c == 'Support' && Ast.Part_Number__c != 'SERVICE CHARGE' &&
		 (trigger.isinsert || (trigger.isupdate && Ast.Part_number__c!=trigger.oldmap.get(Ast.id).Part_number__c)))
		 {
		   TermAssets.add(Ast);
		 }
		 
		}
		
		for(Asset2__c ass:MyMellanoxAssets)
		{ass.MyMellanox_contract__c = ass.contract2__c;}
		
		  
		for(EOL__c eol:[select name,EOS_Date__c from EOL__c where name in :IsEOLPart])
		
		{
		  EOLs.put(eol.name, eol.EOS_Date__c);
		}
		
		
		for(Asset2__c Ast:trigger.new) 
		{
		 if(trigger.isinsert && EOLs.containsKey(Ast.Part_Number__c))
		 
		 { Ast.EOL_Check__c = true; 
		   ASt.EOS_Date__c = EOLs.get(Ast.Part_Number__c);
		 }
		}    
		  
		if(!TermAssets.isempty())
		{
		 month12AssetsSuf = new set<string>{'1B','1S','1G','S1','G1','1SP','1GP','1SB','1OS','004','006','040','042','010','011','001','0-B1','0-1','1SNBD','1GNBD','1S-4H','1G-4H','1-24x7OS'};
		 month24AssetsSuf = new set<string>{'2B','S2','-B1'};
		 month36AssetsSuf = new set<string>{'3S','3B','3S','3G','3OS','3SB','S3','G3','B2','G2','3SP','3SP','3GP','005','007','041','043','031','032','-02','0-2','3SNBD','3GNBD','3S-4H','3G-4H','3-24x7OS'};
		 month48AssetsSuf = new set<string>{'4B','4S','4G','4B','4SP','4GP','4OS','4SB','0-3','-B3','0-3','4-24x7OS'};
		 month60AssetsSuf = new set<string>{'5B','5S','5G','5SP','5SB','5OS','5GP','5-24x7OS'};
		 month36Assets = new set<string>{'SUP-00031-IBM','SUP-00032-IBM','SUP-00010-IBM','SUP-00005-IBM','SUP-00011-IBM','SUP-00007-IBM','SUP-00003','SUP-00028'};
		 month12Assets = new set<string>{'MTEXTW3600-1','SUP-00001-IBM','SUP-00004-IBM'};  
		   
		  
		for(Asset2__c ass:TermAssets)
		 {
		 
		 ass.asset_term_n__c = 12;
		 
		 if(month12AssetsSuf.contains( ass.Part_Number__c.right(2))||
		    month12AssetsSuf.contains( ass.Part_Number__c.right(3))||
		    month12AssetsSuf.contains( ass.Part_Number__c.right(5))||
		    month12AssetsSuf.contains( ass.Part_Number__c.right(8))||
		    month12Assets.contains( ass.Part_Number__c))
		  {ass.asset_term_n__c = 12;}
		 
		 if(month24AssetsSuf.contains( ass.Part_Number__c.right(2))||
		    month24AssetsSuf.contains( ass.Part_Number__c.right(3)))
		   {ass.asset_term_n__c = 24;}
		 
		 
		 if(month36AssetsSuf.contains( ass.Part_Number__c.right(2))||
		    month36AssetsSuf.contains( ass.Part_Number__c.right(3))||
		    month36AssetsSuf.contains( ass.Part_Number__c.right(5))||
		    month36AssetsSuf.contains( ass.Part_Number__c.right(8))||
		    month36Assets.contains( ass.Part_Number__c))
		   {ass.asset_term_n__c = 36;}
		 
		 if(month48AssetsSuf.contains( ass.Part_Number__c.right(2))||
		    month48AssetsSuf.contains( ass.Part_Number__c.right(3))||
		    month48AssetsSuf.contains( ass.Part_Number__c.right(8)))
		   {ass.asset_term_n__c = 48;}
		 
		 if(month60AssetsSuf.contains( ass.Part_Number__c.right(2))||
		    month60AssetsSuf.contains( ass.Part_Number__c.right(8))||
		    month60AssetsSuf.contains( ass.Part_Number__c.right(3)))
		   {ass.asset_term_n__c = 60;}
		 
		 if(ass.Part_Number__c.contains('030') ||ass.Part_Number__c.contains('027') ||ass.Part_Number__c.contains('MTFMAM') )
		 {ass.asset_term_n__c = 0;}
		 
		 
		  
		 }
		 
		 }
	}
	
	else if (trigger.isAfter && trigger.isInsert) {
		
		cls_trg_Asset2 handler = new cls_trg_Asset2();
		handler.assignAssetsDateToRelatedLicenses(trigger.New);     
	}
	
	if (trigger.isBefore && trigger.isInsert) {
		
		cls_trg_Asset2 handler = new cls_trg_Asset2();
		handler.relateNewAssetsWithProducts(trigger.New); 
	}
	
	

}