//Updates relatedCase Comment when Public FM Discussion is updated

trigger FMDiscussion_BeforeUpdate on FM_Discussion__c (before update) 
{
    Map<ID, FM_Discussion__c> map_oldFMDiscussion = Trigger.oldmap;
    Map <ID, FM_Discussion__c> map_CaseCommentIdToFM = new Map <ID, FM_Discussion__c>();
    for(FM_Discussion__c fm: Trigger.new)
    {
        if(fm.Discussion__c != map_oldFMDiscussion.get(fm.Id).Discussion__c && fm.Public__c)
        {
            map_CaseCommentIdToFM.put(fm.Case_Comment_ID__c, fm);
            fm.Sync_With_FM__c = true;
        }
    }
    
    if(map_CaseCommentIdToFM.size()>0)
    {
        List <CaseComment> lst_caseCommentToUpdate = new List <CaseComment>();
        //Map <ID, CaseComment> map_caseComment = new Map <ID, CaseComment>([Select c.Id, c.CommentBody From CaseComment c Where IsDeleted = false and Id IN: map_CaseCommentIdToFM.keySet()]);
        for(ID ccId: map_CaseCommentIdToFM.keyset())
        {
            CaseComment c = new CaseComment(Id=ccId);
            c.CommentBody = map_CaseCommentIdToFM.get(ccId).Discussion__c;
            lst_caseCommentToUpdate.add(c);
        }
        
        if(lst_caseCommentToUpdate.size()>0)
        {
            update lst_caseCommentToUpdate;
        }
    }
}