trigger RMDiscussion_on_insert on RmDiscussion__c (after insert, after update) {

List<RmDiscussion__c> To_SF_RMdiscuss = new List<RmDiscussion__c>();
List<FM_Discussion__c> New_FMdiscuss = new List<FM_Discussion__c>();
List<RmDiscussion__c> To_Delete_RM = new List<RmDiscussion__c>();
List<id> To_Delete_RMid = new List<id>();
List<RmDiscussion__c> Update_RMdiscuss = new List<RmDiscussion__c>();
List<id> Upd_RM_disc = new List<id>();





for(RmDiscussion__c RMD:trigger.new)
{
 if(trigger.isinsert && RMD.Sync_Flag__c == '2')
 {To_SF_RMdiscuss.add(RMD);    
  Upd_RM_disc.add(RMD.id); }    
     
 if(trigger.isupdate && RMD.Sync_Flag__c == '0' && trigger.oldmap.get(RMD.id).Sync_Flag__c !='0')
 {  To_Delete_RMid.add(RMD.id);}      
 
}                  


if(To_SF_RMdiscuss.size()>0)
 for(RmDiscussion__c rm:[select id,Sync_Flag__c from RmDiscussion__c where id in:Upd_RM_disc])
  { rm.Sync_Flag__c = '0';       
    Update_RMdiscuss.add(rm);     }           
          
if(To_Delete_RMid.size()>0)
To_Delete_RM =[select id from RmDiscussion__c where id in:To_Delete_RMid and isdeleted =false];
           
                    
                                                   

//inserting New discussions from Redmine to SF

for(RmDiscussion__c RMDis:To_SF_RMdiscuss)
{
 FM_discussion__c NFM = new FM_discussion__c();         
 NFM.Discussion__c = RMDis.RM_Discussion__c;          
 NFM.RM_Case1__c  = RMDis.SF_RM_Case__c;        
 NFM.Case__c = RMDis.SF_Case__c;         
 NFM.RM_Created_by__c = RMDis.Created_by__c;          
 NFM.Public__c = false;           
 NFM.from_RM__c = true;            
 New_FMdiscuss.add(NFM);
} 



if(New_FMdiscuss.size()>0)
{ insert New_FMdiscuss;
  update Update_RMdiscuss;             
 }   

if(To_Delete_RM.size()>0)
{ delete To_Delete_RM;}
  
}