// Tatiana Cohen: A trigger that summarizes data from Miletones to projects

trigger SummarizeMilestones on Milestone1_Milestone__c (after delete, after insert, after update) {

//CustomObject  = Milestone
// Account = Project

  //Limit the size of list by using Sets which do not contain duplicate elements
  set<Id> ProjectIds = new set<Id>();

  //When adding new Milestone or updating existing Milestone
  if(trigger.isInsert || trigger.isUpdate){
    for(Milestone1_Milestone__c M : trigger.new){
      ProjectIds.add(M.Project__c);
    }
  }

  //When deleting Milestone
  if(trigger.isDelete){
    for(Milestone1_Milestone__c M : trigger.old){
      ProjectIds.add(M.Project__c);
    }
  }

  //Map will contain one Account Id to one sum value
  map<Id,Double> ProjectFSEMap = new map <Id,Double>();
  map<Id,Double> ProjectFSE2Map = new map <Id,Double>();
  map<Id,Double> ProjectPDMMap = new map <Id,Double>();
  map<Id,Double> ProjectExpensesMap = new map <Id,Double>();
  map<Id,Double> ProjectLaborExpensesMap = new map <Id,Double>();
  map<Id,Double> ProjectPersonDaysMap = new map <Id,Double>();
  map<Id,Date> ProjectDeadlineMap = new map <Id,Date>();
  map<Id,Double> ProjectTotalDuration = new map <Id,Double>();

  //Produce a sum of Milestone and add them to the map
  //use group by to have a single Account Id with a single sum value
  for(AggregateResult q : [select Project__c ,sum(FSE_HR__c), sum(FSE2_HR__c), sum(PDM_HR__c), sum(Expenses__c), sum(Labor_Expenses__c), sum(Number_Onsite_Person_Days__c), max(deadline__c), sum(Duration_Hours__c)
    from Milestone1_Milestone__c where Project__c IN :ProjectIds group by Project__c]){
      ProjectFSEMap.put((Id)q.get('Project__c'),(Double)q.get('expr0'));
      ProjectFSE2Map.put((Id)q.get('Project__c'),(Double)q.get('expr1'));
      ProjectPDMMap.put((Id)q.get('Project__c'),(Double)q.get('expr2'));
      ProjectExpensesMap.put((Id)q.get('Project__c'),(Double)q.get('expr3'));
      ProjectLaborExpensesMap.put((Id)q.get('Project__c'),(Double)q.get('expr4'));
      ProjectPersonDaysMap.put((Id)q.get('Project__c'),(Double)q.get('expr5'));
      ProjectDeadlineMap.put((Id)q.get('Project__c'),(Date)q.get('expr6'));
      ProjectTotalDuration.put((Id)q.get('Project__c'),(Double)q.get('expr7'));
  }
  
 for(Milestone1_Milestone__c ms : [Select Id,deadline__c  from Milestone1_Milestone__c where Project__c IN :ProjectIds AND Complete__c = FALSE order by deadline__c ASC])
  {
  
  }
  

  List<Milestone1_Project__c> ProjectsToUpdate = new List<Milestone1_Project__c>();

  //Run the for loop on Account using the non-duplicate set of Accounts Ids
  //Get the sum value from the map and create a list of Accounts to update
  for(Milestone1_Project__c o : [Select Id,recordtypeid, Total_FSE_HR__c, Total_FSE2_HR__c, Total_PDM_HR__c, Total_Expenses__c, Deadline__c from Milestone1_Project__c where Id IN :ProjectIds])
  {
    
    o.Total_FSE_HR__c = ProjectFSEMap.get(o.Id);
    o.Total_FSE2_HR__c = ProjectFSE2Map.get(o.Id);
    o.Total_PDM_HR__c = ProjectPDMMap.get(o.Id);
    o.Total_Expenses__c= ProjectExpensesMap.get(o.Id);
    o.Total_Labor_Expenses__c=ProjectLaborExpensesMap.get(o.Id);
    o.Total_Number_Onsite_Person_Days__c = ProjectPersonDaysMap.get(o.Id);
    o.Total_Duration_Hours__c = ProjectTotalDuration.get(o.Id);
    if (o.recordtypeid != env.ProjectTypeMap.get('Service Delivery Project'))
    	 o.Deadline__c = ProjectDeadlineMap.get(o.Id);
    ProjectsToUpdate.add(o);
  }

  update ProjectsToUpdate;
  
  	if (trigger.isAfter)
  	{
  		cls_trg_Milestone handler = new cls_trg_Milestone();
  		if (trigger.isInsert)
  		{
  		    handler.updateTheProjectNextMilestoneOnMilestone( trigger.new, null, false);
  		    handler.calculatAllRealtedMilestonesCumulativeDone(trigger.new, null);
  		}
  		
  		if (trigger.isUpdate)
  		{
  			handler.updateTheProjectNextMilestoneOnMilestone( trigger.new , trigger.oldMap, true);
  			handler.calculatAllRealtedMilestonesCumulativeDone(trigger.New, null);
  		}
  		
  		else if (trigger.isDelete) {
  			
  			handler.calculatAllRealtedMilestonesCumulativeDone(null, trigger.Old);
  		}
  	}

}