trigger trg_Case on Case (before insert, after update, before update, after insert) {
    
    clsCase handler = new clsCase();

        if (trigger.isBefore && trigger.isInsert && !ENV.triggerShouldRun_List[0]) {
            
            handler.updateLiveAgentCasesWithContacts(trigger.New);
            handler.updateAcademyCaseSubject(trigger.New);
            ENV.triggerShouldRun_List[0] = true;
        }
        
        else if (trigger.isAfter && trigger.isUpdate && !ENV.triggerShouldRun_List[1]) {
            
            handler.caseCriticalBug(trigger.New, trigger.OldMap);
            handler.updateContactOnEmail2CaseEZChip(trigger.New);
            //ENV.triggerShouldRun_List[1] = true;
        }
        
        else if (trigger.isBefore && trigger.isUpdate && !ENV.triggerShouldRun_List[2]) {
            
            handler.updateCasePreviousAssigneeWhenAssigneeChanged(trigger.New, trigger.OldMap);
            ENV.triggerShouldRun_List[2] = true;
        }
        
        else if (trigger.isAfter && trigger.isInsert && !ENV.triggerShouldRun_List[3]) {
            
            handler.updateAcademyContactInterestedInTraining(trigger.New);
            ENV.triggerShouldRun_List[3] = true;
        }

}