public   class DynamicTabs 
{
    
    public List<String> Categories{
    get {
      if (Categories== null) {
 
        Categories= new List<String>();
        Schema.DescribeFieldResult field = SFDC_Issue__c.Type__c.getDescribe();
 
        for (Schema.PicklistEntry f : field.getPicklistValues())
          Categories.add(f.getLabel());
 
      }
      return Categories;          
    }
    set;
  }
  
   public list<Component.Apex.Tab> myTabs {get;set;}
   public Component.Apex.TabPanel myTabPanel {get;set;}
    public DynamicTabs()
    {
        Component.Apex.TabPanel myTabPanel = new Component.Apex.TabPanel();
         myTabs = new List<Component.Apex.Tab>();
    }
    
    
}