public class VFLoanerForm{
        //Johny Code from 14-11-2011
        public boolean showFields {get;set;}
        public boolean sameasShipment {get;set;}
        public boolean isMarketingUser {get; set;}
        public boolean hasNPI {get;set;}
        
        Set<ID> product_IDs = new Set<ID>();
        Map<ID,Product2> map_products = new Map<ID,Product2>();
        
        public class LoanerProducts
    {
                
        public Loaner_product__c  LoanOPN {get;set;}
        public Integer ind {get;set;}
        public boolean Required {get;set;}
        public boolean RequiredOPN {get;set;}
        public String Agilestatus {get; set;}  
                        
                
        public LoanerProducts(Loaner_product__c  LnOpn,Integer ind )
        {
                 
            this.LoanOPN = LnOpn;
            this.ind = ind;
            Required = false;
            RequiredOPN = false;
            Agilestatus = '';
        }
                
        
        
      }
   

    public List<LoanerProducts> lst_LoanerProducts {get;set;}
    public List<Loaner_product__c> lst_LoanerOPNTobeInserted;
    Public boolean PaidByMellanox {get;set;}
    public String opnId {get;set;}
    //Johny End Code from 14-11-2011

    public Loaner_Request__c odisiloaner {get;set;}
    public boolean bSupportPortal{get;set;}
    public List<SelectOption> DistributerState {get;set;}
    public String ErrMsg {get;set;}
    
        public VFLoanerForm()   
        {
                showFields = false;
                sameasShipment =false;
                hasNPI = false;
                
          lst_LoanerOPNTobeInserted = new List<Loaner_product__c>(); 
          odisiloaner=new Loaner_Request__c();      
          bSupportPortal=IsSupportPortal();
          
          //JOhny added code in 14-11-2011
        lst_LoanerProducts = new List<LoanerProducts>();
        for(Integer i=1;i<=5;i++)  
        {
            lst_LoanerProducts.Add(new LoanerProducts(new Loaner_Product__c(),i));
          //  lst_MellanoxProducts[i-1].Required = true;

      }
        lst_LoanerProducts[0].RequiredOPN = true;
        //End johny Code 14-11-2011    
        }
        public VFLoanerForm(ApexPages.StandardController controller) 
    {
        //Determine as if Marketing user
        //KN 11-10-14
        if(UserInfo.getName() == 'Marketing Group') {
        	isMarketingUser = true;	
        }
        else
        	isMarketingUser = false;
        //
        hasNPI = false;
        sameasShipment =false;
        showFields = false;
        lst_LoanerOPNTobeInserted = new List<Loaner_Product__c>(); 
        odisiloaner=new Loaner_Request__c();
        bSupportPortal=IsSupportPortal();
        //JOhny added code in 14-11-2011
        lst_LoanerProducts = new List<LoanerProducts>();
        for(Integer i=1;i<=5;i++)  
        {
            lst_LoanerProducts.Add(new LoanerProducts(new Loaner_Product__c(),i));
           // lst_MellanoxProducts[i-1].Required = true;

        }
         lst_LoanerProducts[0].RequiredOPN = true;
        //End johny Code 14-11-2011
    }
       private boolean IsSupportPortal()
    {
        return true;
    }
    private void SetDefaults()
    {
       /* //return true;
        //Set default disti here
        Account a=[select Name,Product_Manager__c from Account where Is_Distributor__c=true and Name=:odisiloaner.Distributor_Name__c];
       // if(a!=null)
           // odisiloaner.OwnerId=a.Product_Manager__c;
           */
    }
    
    public List<selectOption> getShipToStates()
    {
        return retAllStates(odisiloaner.Ship_Country__c);
    }  
    public List<selectOption> getEndUserStates()
    {
        return retAllStates(odisiloaner.Country__c);
    }  
    private List<selectOption> retAllStates(String selectedCountry)
    {
        List<selectoption> soptions = new List<selectOption>();
        soptions.Add(new selectoption('','- None -'));
        if(selectedCountry!=null && selectedCountry!='')
        {
            List<US_Territory_Map__c> stateOptions = [Select u.Id, u.Name, u.Country__c From US_Territory_Map__c u where IsDeleted=false and u.Country__c=:selectedCountry Order By Name];
            set<string> stateset = new set<string>();
            List <String> lst_state = new List <String>();
            for(US_Territory_Map__c c: stateOptions)
            {
                if(!stateset.contains(c.Name))
                {
                    stateset.Add(c.Name);
                    lst_state.Add(c.Name);
                }
            }
            
            for(String s : lst_state )
            {
                soptions.Add(new selectoption(s,s));
            }
        }
        return soptions;
    }

    
    public List<selectOption> getAllCountries()
    {
        List<US_Territory_Map__c> countryOptions = [Select u.Id, u.Country__c From US_Territory_Map__c u where IsDeleted=false Order By Country__c];
        system.debug('BLAT countryOptions: ' + countryOptions);
        set<string> countryset = new set<string>();
        List <String> lst_country = new List <String>();
        for(US_Territory_Map__c c: countryOptions)
        {
            if(!countryset.contains(c.Country__c)&& c.Country__c != NULL)
            {
                countryset.Add(c.Country__c);
                lst_country.Add(c.Country__c);
            }
        }
        system.debug('BLAT countryset: ' + countryset);
        List<selectoption> coptions = new List<selectOption>();
        coptions.Add(new selectoption('','- None -'));
        
        for(String s : lst_country)
        {
            system.debug('BLAT s: ' + s);
            coptions.Add(new selectoption(s,s));
        }
        
        system.debug('BLAT coptions: ' + coptions);
        return coptions;
        
    }

    
    public PageReference Reset()
    {
        PageReference cPage = ApexPages.CurrentPage();
        cPage.SetRedirect(true);
        
        //JOhny added code in 14-11-2011
        lst_LoanerProducts = new List<LoanerProducts>();
        for(Integer i=1;i<=5;i++)  
        {
            lst_LoanerProducts.Add(new LoanerProducts(new Loaner_Product__c(),i));
            lst_LoanerProducts[i-1].Required = true;

        }
        lst_LoanerProducts[0].RequiredOPN = true;
        //End johny Code 14-11-2011
        return cPage;
    }
     public PageReference SaveDraft()
    {      
    
        try
        {
        	//if(!ValidateRequierFields()&& !ValidateSubmitRequierFields()) {
        	if(true) {
                   system.debug('Saving');
                    SetDefaults();          
                    Loaner_Request__c odisiLoanertemp=new Loaner_Request__c ();
                    odisiLoanertemp=odisiloaner;
                    odisiLoanertemp.status__c = 'Draft';
                    //KN added-Handling MFS loaners
System.debug('...KN ...Userinfo.getUserId()='+Userinfo.getUserId());                    
                    if(Userinfo.getUserId()== '005500000013i95AAA' || Userinfo.getUserId()== '00550000001sVlaAAE'){
                    	odisiLoanertemp.MFS_Loaner__c = true;
                    }
                    //
                    insert odisiLoanertemp;  
                    if(lst_LoanerProducts != null && lst_LoanerProducts.size() > 0)
                    {
                                                
                        for(LoanerProducts  mp :lst_LoanerProducts)
                        {
                                if(mp.LoanOPN != null ) 
                                {
                                        if(mp.LoanOPN.Product__c != null)
                                        {
                                                system.debug('adding first obj 1');
                                                system.debug('odisiLoanertemp.id : '  + odisiLoanertemp.id);
                                                system.debug('mp.OppRegOPN.Opp_Reg__c  : ' + mp.LoanOPN.Product__c );
                                                mp.LoanOPN.Loaner_Request__c = odisiLoanertemp.id;
                                                lst_LoanerOPNTobeInserted.add(mp.LoanOPN);
                                        }
                                }
                        }
                        if(lst_LoanerOPNTobeInserted != null && lst_LoanerOPNTobeInserted.size() > 0)
                        {
                                insert lst_LoanerOPNTobeInserted;
                        }
                    }       
                    odisiloaner=odisiLoanertemp.clone(false,true);
                    ID guestUserID = ENV.SiteUser;
                  if(Userinfo.getUserId()!= guestUserID)
                  {
                    //Pagereference p = new Pagereference('https://na3.salesforce.com/a23/o'); 
					Pagereference p = new Pagereference('/a23/o');     
                    return p;
                  }
                  else
                  {
                    Pagereference p = new Pagereference('http://www.mellanox.com/content/pages.php?pg=rma&action=success');
                    return p;
                  }

                    
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Registration has been submitted'));
                
            } //close if(!ValidateRequierFields()&& !ValidateSubmitRequierFields())
            return null;
        }
        catch(exception e)
        {
        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage()));
            return null;
        }
    }     
    public PageReference Save()
    {      
    
        try
        {
               if(!ValidateRequierFields()&& !ValidateSubmitRequierFields())
                {
                    system.debug('Saving');
                    SetDefaults();          
                    Loaner_Request__c odisiLoanertemp=new Loaner_Request__c ();
                    odisiLoanertemp=odisiloaner;
                    if(odisiLoanertemp.Requestor_same_as_Contact__c) 
                        {     
                         odisiLoanertemp.contact__c = odisiLoanertemp.Requestor_name__c;
                         odisiLoanertemp.contact_email__c = odisiLoanertemp.Requestor_email__c;
                         odisiLoanertemp.contact_phone__c = odisiLoanertemp.Requestor_phone__c;
                        }
					//KN added-Handling MFS loaners
                    if(Userinfo.getUserId()== '005500000013i95AAA' || Userinfo.getUserId()== '00550000001sVlaAAE'){
                    	odisiLoanertemp.MFS_Loaner__c = true;
                    }
                    //
                    if(hasNPI == false)
                    	odisiLoanertemp.Acknowledge_NPI_products__c = false;
                    	
                    insert odisiLoanertemp;  
                    if(lst_LoanerProducts != null && lst_LoanerProducts.size() > 0)
                    {
                                                
                        for(LoanerProducts  mp :lst_LoanerProducts)
                        {
                                if(mp.LoanOPN != null ) 
                                {
                                        if(mp.LoanOPN.Product__c != null)
                                        {
                                                system.debug('adding first obj 1');
                                                system.debug('odisiLoanertemp.id : '  + odisiLoanertemp.id);
                                                system.debug('mp.OppRegOPN.Opp_Reg__c  : ' + mp.LoanOPN.Product__c );
                                                mp.LoanOPN.Loaner_Request__c = odisiLoanertemp.id;
                                                lst_LoanerOPNTobeInserted.add(mp.LoanOPN);
                                        }
                                }
                        }
                        if(lst_LoanerOPNTobeInserted != null && lst_LoanerOPNTobeInserted.size() > 0)
                        {
                                insert lst_LoanerOPNTobeInserted;
                        }
                    }       
                    odisiloaner=odisiLoanertemp.clone(false,true);
                    ID guestUserID = ENV.SiteUser;
                  if(Userinfo.getUserId()!= guestUserID)
                  {
                    //Pagereference p = new Pagereference('https://na3.salesforce.com/a23/o');  
                    Pagereference p = new Pagereference('/a23/o');            
                    return p;
                  }
                  else
                  {
                    Pagereference p = new Pagereference('http://www.mellanox.com/content/pages.php?pg=rma&action=success');
                    return p;
                  }

                    
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Registration has been submitted'));
                }
               return null;
        }
        catch(exception e)
        {
        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage()));
            return null;
        }
    }      
    private Boolean ValidateSubmitRequierFields()
    {      
     Boolean IsError=false;
     if( odisiloaner.Customer_Type__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Customer Type\' field'));
            IsError=true;
            }

     if( odisiloaner.Loan_requested_Delivery_date__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Loan requested Delivery date\' field'));
            IsError=true;
            }
     if( odisiloaner.Loan_Type__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Loaner Type \' field'));
            IsError=true;
            }
     if( odisiloaner.Justification__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Loan Justification \' field'));
            IsError=true;
            }
    if( odisiloaner.Project_Name__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Project Name \' field'));
            IsError=true;
            }
   if( odisiloaner.Freight__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Freight \' field'));
            IsError=true;
            }
   if( odisiloaner.Service__c== NULL && odisiloaner.Freight__c == 'Paid by Requestor Company' )      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Customer Carrier\' and \'Service \' field'));
            IsError=true;
            }

  if( odisiloaner.End_Customer_Company__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'End Customer  \' field'));
            IsError=true;
            }

  if( odisiloaner.Country__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'End Customer Country \' field'));
            IsError=true;
            }

  if( odisiloaner.EndCustState__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'End Customer State\' field'));
            IsError=true;
            }      
  if( odisiloaner.Ship_company__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship to Company\' field'));
            IsError=true;
            }
      
 if( odisiloaner.Ship_Zip__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Zip\' field'));
            IsError=true;
            }

 if( odisiloaner.Ship_Street_Address__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Street Address\' field'));
            IsError=true;
            }

//if( odisiloaner.Ship_to_contact__c== NULL)
if( odisiloaner.Ship_to_contact__c== NULL || odisiloaner.Ship_to_contact_First_Name__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship To Contact First Name and Last Name\' field'));
            IsError=true;
            }

if( odisiloaner.Ship_City__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship City\' field'));
            IsError=true;
            }

if( odisiloaner.Ship_Email__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Email\' field'));
            IsError=true;
            }

if( odisiloaner.Ship_Country__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Country\' field'));
            IsError=true;
            }
if( odisiloaner.Ship_Phone__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Phone\' field'));
            IsError=true;
            }

if( odisiloaner.ShipState__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship State\' field'));
            IsError=true;
            }


   
     return IsError;
                                             
    }      
                     
                                 
    private Boolean ValidateRequierFields()
    {         Boolean IsErrorOccured=false;
        	//Init
        	product_IDs.clear();
        	hasNPI = false;
        	
           if( odisiloaner.Internal_request_type__c == NULL || odisiloaner.Internal_request_type__c == '')      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Internal Request Type\' field'));
            IsErrorOccured=true;
            }
                    
            if( odisiloaner.Loan_Type__c != 'Sample' && odisiloaner.evaluation_period__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Evaluation period\' field'));
            IsErrorOccured=true;
            }
		
		if( odisiloaner.Deviation_PCN_related__c == 'Yes' && odisiloaner.Deviation_Hold_Comments__c == NULL )      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please enter Comments when select Deviation-PCN Related field as Yes'));
            IsErrorOccured=true;
            }
		//	
          if( odisiloaner.carrier__c == 'Other' && odisiloaner.other_carrier__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Other Carrier\' field'));
            IsErrorOccured=true;
            }


  
          
            if(odisiloaner.Country__c==null ||  odisiloaner.Country__c=='')
             {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'End User Country: You must enter a value'));
                IsErrorOccured=true;
             }

             if(odisiloaner.Ship_Country__c==null ||  odisiloaner.Ship_Country__c=='')
             {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Ship to Country: You must enter a value'));
                IsErrorOccured=true;
             }

            
            
            
            
            
            
              //Mellanox Product Section    
             if(lst_LoanerProducts == null || lst_LoanerProducts.size() == 0)
              {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Loaner Request must have a Product'));
                  IsErrorOccured=true;
              }
              
              
              
              
              
             if(lst_LoanerProducts != null && lst_LoanerProducts.size() > 0)
                {
                   if(lst_LoanerProducts [0].LoanOPN.Product__c==null)           
                    {                      
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Product OPN 1: You must enter a value'));                                     
                    IsErrorOccured=true;                
                    }
                    for(LoanerProducts mp: lst_LoanerProducts)
                    {   
                        if( mp.LoanOPN.Product__c==null && mp.LoanOPN.OPN_Quantity__c==null)
                        {
                            //do nothing, as no data was entered
                        }else if(mp.LoanOPN.Product__c !=null && mp.LoanOPN.OPN_Quantity__c ==null)
                        
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Mellanox Product ' + mp.ind+' should have a quantity'));
                            IsErrorOccured=true;
                        }  else if( mp.LoanOPN.OPN_Quantity__c !=null && mp.LoanOPN.Product__c ==null )
                                                      
                        {
                             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'OPN Quantity should have OPN Product. Line ' + mp.ind +'.'));
                             IsErrorOccured=true;
                        } 
                        //KN added 3-4-15
                        if(IsErrorOccured == false){
                        	product_IDs.add(mp.LoanOPN.Product__c);	
                        }
                        //                          
                    }//end for loop
                    
                    //KN added 3-4-15, if no error query
                    
                    if(IsErrorOccured == false){
                    	map_products = new Map<id,Product2>([select id,Agile_Status__c from Product2 where ID in :product_IDs and Agile_Status__c='NPI']);
						if(map_products.size()>0) {
							
								hasNPI = true;
								if(odisiloaner.Acknowledge_NPI_products__c == false && hasNPI == true) {								
									IsErrorOccured=true;
									ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'This loaner has NPI product(s). Scroll down to the product section to view detail. Pls confirm you acknowledge this by marking checkbox \'Acknowledge NPI products in this loaner\' before click submit again'));
								}
																	
		                    	for(LoanerProducts mp2: lst_LoanerProducts){
		                    		if(mp2.LoanOPN.Product__c != null && map_products.containsKey(mp2.LoanOPN.Product__c))
		                    			mp2.Agilestatus = 'NPI';
		                    	}
								
						}
                    }
                    //
                    
                }
                //END Johny CODE 17-11-2011
                  
              
                return IsErrorOccured;
    }
    //Johny Code added on 14-11-2011
    public void AddLoanerOPN()
    {
        if(lst_LoanerProducts.size()<40)
        {
            lst_LoanerProducts.Add(new LoanerProducts(new Loaner_product__c(),lst_LoanerProducts.size()+1));
        }
    }
    public void RemoveLoanerOPN()
    {
        if(lst_LoanerProducts.size()>1)
        {
            lst_LoanerProducts.remove(lst_LoanerProducts.size() -1 );
            
        }
    }
    
   public void ContactSameAsRequestor()
  {
        odisiloaner.Requestor_same_as_Contact__c = !odisiloaner.Requestor_same_as_Contact__c;
        
    }
    
    
   public void Freight()
   
  {
  
    if(odisiloaner.Freight__c != NULL)
    PaidByMellanox = false;
  }
 public string ContactSameAsRequestorField()
  {
  return odisiloaner.Requestor_Name__c;
  }
  
  public boolean getFreightVal()
    {
     if(odisiloaner.Freight__c != NULL)
     return true;
     return false;
    } 
    
    public void showFieldsFunc()
    {
        system.debug('Start - showFieldsFunc');
        String STR = system.currentPageReference().getParameters().get('topicSelection');
        
        system.debug('odisiloaner.Freight__c : ' + odisiloaner.Freight__c );
        
        
                if(odisiloaner.Freight__c  == 'Paid by Mellanox' || odisiloaner.Freight__c == 'Customer Will Place PO for Shipping')
                      showFields = true;
                 else
                        showFields = false;
        
        system.debug('END - showFieldsFunc');
        //return null;
        
    }
    // KN added 1/25/13
    public void setDefaultServiceLevel()
    {
        String STR = system.currentPageReference().getParameters().get('CustomerCarrierSelection');
        if(odisiloaner.Carrier__c  == 'FED EX DOMESTIC')
        	odisiloaner.Service__c = 'FED EX GROUND';
        else if(odisiloaner.Carrier__c  == 'UPS DOMESTIC')
        	odisiloaner.Service__c = 'UPS GROUND';
    }     
    //     

   public String getCustType() {
        return   'OEM Account - Chuck’s team <br>End User Account – Sergio’s team';
    }
                           
   
    public void sameasshipmentfunc()
    {
        system.debug('Start - sameasshipment');
        
        
        
        system.debug('the var value is : ' + sameasShipment);
        system.debug('the odisiloaner.End_Customer_Company__c: ' +  odisiloaner.End_Customer_Company__c);
        system.debug('the odisiloaner.End_Cust_Zip__c: ' +  odisiloaner.End_Cust_Zip__c);
        system.debug('the odisiloaner.End_Cust_City__c: ' +  odisiloaner.End_Cust_City__c);
        system.debug('the odisiloaner.EndCustState__c: ' +  odisiloaner.EndCustState__c);
        system.debug('the odisiloaner.Contact_Person__c: ' +  odisiloaner.Contact_Person__c);
        system.debug('the odisiloaner.Email__c: ' +  odisiloaner.Email__c);
        system.debug('the odisiloaner.End_cust_Phone__c: ' +  odisiloaner.End_cust_Phone__c);
        system.debug('the odisiloaner.End_Cust_Street_Address__c: ' +  odisiloaner.End_Cust_Street_Address__c);
        system.debug('the odisiloaner.End_Cust_Zip__c: ' +  odisiloaner.End_Cust_Zip__c);
        
    /*    if(sameasShipment)
        {
                
                odisiloaner.Ship_Zip__c = odisiloaner.End_Cust_Zip__c;
                odisiloaner.Ship_City__c = odisiloaner.End_Cust_City__c;
                odisiloaner.ShipState__c = odisiloaner.EndCustState__c;
                odisiloaner.Ship_to_Contact__c = odisiloaner.Contact_Person__c;
                odisiloaner.Ship_Email__c = odisiloaner.Email__c;
                odisiloaner.Ship_Phone__c = odisiloaner.End_cust_Phone__c;
                odisiloaner.Ship_Street_Address__c = odisiloaner.End_Cust_Street_Address__c;
                odisiloaner.Ship_Zip__c = odisiloaner.End_Cust_Zip__c;
                odisiloaner.Ship_Country__c = odisiloaner.Country__c;
                
                
                system.debug('odisiloaner.Ship_Zip__c : ' + odisiloaner.Ship_Zip__c);
                system.debug('odisiloaner.Ship_City__c : ' + odisiloaner.Ship_City__c);
                system.debug('odisiloaner.ShipState__c : ' + odisiloaner.ShipState__c);
                system.debug('odisiloaner.Ship_to_Contact__c : ' + odisiloaner.Ship_to_Contact__c);
                system.debug('odisiloaner.Ship_Email__c : ' + odisiloaner.Ship_Email__c);
                system.debug('odisiloaner.Ship_Phone__c : ' + odisiloaner.Ship_Phone__c);
                system.debug('odisiloaner.Ship_Street_Address__c : ' + odisiloaner.Ship_Street_Address__c);
                system.debug('odisiloaner.Ship_Zip__c : ' + odisiloaner.Ship_Zip__c);
                
        }*/
        
        system.debug('END - sameasshipment');
        //return null;
        
    }
    //End Johny code in 14-11-2011

}