global class Forward_Pricing_Roll_in_Effective implements Schedulable{
	//This would be run the 1st day of every quarter
	//Roll in forwarding pricing into current Q pricing (Pricebookentry)
	//Re-price all existing forecasts
	global void execute(SchedulableContext SC) {
		Date currdate = System.today();
		List<Period> periods = new List<Period>([select StartDate,EndDate from Period where Type='Quarter' and Enddate >= :currdate order by EndDate limit 2]);
		List<MLNX_Price_Book__c> lst_forward_pricing = new List<MLNX_Price_Book__c>([select id, Current_Price__c,Current_Quarter_Date__c,
																						Pricebook_Name__c,Product__c,
																						X1Quarter_out_Price__c,X2Quarter_out_Price__c,
																						X3Quarter_out_Price__c,X4Quarter_out_Price__c,
																						X5Quarter_out_Price__c,X6Quarter_out_Price__c,
																						X7Quarter_out_Price__c,X8Quarter_out_Price__c,X9Quarter_out_Price__c
																						from MLNX_Price_Book__c
																						where (X1Quarter_out_Price__c != null or X2Quarter_out_Price__c != null
																								or X3Quarter_out_Price__c != null or X4Quarter_out_Price__c != null
																								or X5Quarter_out_Price__c != null or X6Quarter_out_Price__c != null
																								or X7Quarter_out_Price__c != null or X8Quarter_out_Price__c != null
																								or X9Quarter_out_Price__c != null) ]);
		ID StandardPB = '01s5000000062WY';
		List<MLNX_Price_Book__c> lst_FW_need_updatePBEForecasts = new List<MLNX_Price_Book__c>();
		Set<String> set_prodIDs = new Set<String>();
		Set<ID> set_PBIDs = new Set<ID>(); //include Standard PB initially
		set_PBIDs.add(StandardPB);
		Set<String> set_PBs = new Set<String>();
		
		Map<ID,Map<String,MLNX_Price_Book__c>> map_prod_PB_FWPricing = new Map<ID,Map<String,MLNX_Price_Book__c>>();
		Map<String,MLNX_Price_Book__c> map_PB_FWPricing = new Map<String,MLNX_Price_Book__c>();
		 		
		for (MLNX_Price_Book__c m : lst_forward_pricing) {
			//assume logic is run 1st day of every quarter
			m.Current_Quarter_Date__c = currdate;
			//convert to 15 char IDs since the query on OpportunityLineItemSchedule take 15 char ID on products
			set_prodIDs.add(String.valueOf(m.Product__c).substring(0, 15));
			set_PBs.add(m.Pricebook_Name__c);
			
			if(Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c) != null)
				set_PBIDs.add(Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c));
			
			m.Current_Price__c = m.X1Quarter_out_Price__c;
			m.X1Quarter_out_Price__c = m.X2Quarter_out_Price__c;
			m.X2Quarter_out_Price__c = m.X3Quarter_out_Price__c;
			m.X3Quarter_out_Price__c = m.X4Quarter_out_Price__c;
			m.X4Quarter_out_Price__c = m.X5Quarter_out_Price__c;
			m.X5Quarter_out_Price__c = m.X6Quarter_out_Price__c;
			m.X6Quarter_out_Price__c = m.X7Quarter_out_Price__c;
			m.X7Quarter_out_Price__c = m.X8Quarter_out_Price__c;
			m.X8Quarter_out_Price__c = m.X9Quarter_out_Price__c;
			
			if(m.Current_Price__c != null || m.X1Quarter_out_Price__c != null) {
				lst_FW_need_updatePBEForecasts.add(m);
			}
		}										
		
		for (MLNX_Price_Book__c m : lst_FW_need_updatePBEForecasts) {
			//build map for retrival later
			if (map_prod_PB_FWPricing.get(m.Product__c) == null) {
				map_PB_FWPricing = new Map<String,MLNX_Price_Book__c>();
				map_PB_FWPricing.put(m.Pricebook_Name__c,m);
				map_prod_PB_FWPricing.put(m.Product__c,map_PB_FWPricing);									
			}
			else {
				map_PB_FWPricing = map_prod_PB_FWPricing.get(m.Product__c);
				map_PB_FWPricing.put(m.Pricebook_Name__c,m);
				map_prod_PB_FWPricing.put(m.Product__c,map_PB_FWPricing);
			}//end map			
		}
		
		update lst_forward_pricing;
		//build map PBE update or insert
		List<PricebookEntry> lst_pbe = new List<PricebookEntry>([select id,Product2ID,Pricebook2ID,UnitPrice,isActive,UseStandardPrice
																	from PricebookEntry 
																	where Product2ID in :set_prodIDs 
																	and Pricebook2ID in :set_PBIDs]);
		List<PricebookEntry> lst_pbe_updates = new List<PricebookEntry>();
		List<PricebookEntry> lst_pbe_inserts = new List<PricebookEntry>();
		List<PricebookEntry> lst_pbe_inserts_Std_PB = new List<PricebookEntry>();
																			
		Map<ID,Map<ID,PricebookEntry>> map_prodID_PricebookID_Price = new Map<ID,Map<ID,PricebookEntry>>();
		Map<ID,PricebookEntry> tmp_map_PricebookID_Price = new Map<ID,PricebookEntry>();  
		for (PricebookEntry pbe : lst_pbe){
			if (map_prodID_PricebookID_Price.get(pbe.Product2ID)==null){
				tmp_map_PricebookID_Price = new Map<ID,PricebookEntry>();
				tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe);
				map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
			}
			else {
				tmp_map_PricebookID_Price = map_prodID_PricebookID_Price.get(pbe.Product2ID);
				tmp_map_PricebookID_Price.put(pbe.Pricebook2ID,pbe);
				map_prodID_PricebookID_Price.put(pbe.Product2ID,tmp_map_PricebookID_Price);
			}
		}																				
		//
		PricebookEntry tmp_PBE, tmp_PBE_Std;
		for (MLNX_Price_Book__c m : lst_FW_need_updatePBEForecasts) {
			if(m.Current_Price__c != null) {
				tmp_PBE = null;
				if(map_prodID_PricebookID_Price.get(m.Product__c) != null 
					&& Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c) != null
					&& map_prodID_PricebookID_Price.get(m.Product__c).get(Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c)) != null )
						tmp_PBE = map_prodID_PricebookID_Price.get(m.Product__c).get(Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c));
						if(tmp_PBE != null) {
							tmp_PBE.UnitPrice = m.Current_Price__c;
							tmp_PBE.UseStandardPrice = false;
							tmp_PBE.isActive = true;
							lst_pbe_updates.add(tmp_PBE);
						}
				//does not have entries yet
				/*
				else if(map_prodID_PricebookID_Price.get(m.Product__c) != null 
					&& Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c) != null
					&& map_prodID_PricebookID_Price.get(m.Product__c).get(Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c)) == null ) {
						
				} */
				else {
					//Add Standard PBE first
					if(map_prodID_PricebookID_Price.get(m.Product__c) == null || (map_prodID_PricebookID_Price.get(m.Product__c) != null && map_prodID_PricebookID_Price.get(m.Product__c).get(StandardPB) == null) ) {
						tmp_PBE_Std = new Pricebookentry(Product2ID = m.Product__c,UnitPrice=m.Current_Price__c,isActive=true,Pricebook2ID=StandardPB);
						lst_pbe_inserts_Std_PB.add(tmp_PBE_Std);
					}
					tmp_PBE = new Pricebookentry(Product2ID = m.Product__c,UnitPrice=m.Current_Price__c,isActive=true,UseStandardPrice=false);
					if(Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c) != null) {
						tmp_PBE.Pricebook2ID = Quote_ENV.PriceBookMap.get(m.Pricebook_Name__c);
						lst_pbe_inserts.add(tmp_PBE);
					}
				}
			}
		}	// end for loop
		
		if(lst_pbe_updates.size() > 0)
			update lst_pbe_updates;
		if(lst_pbe_inserts_Std_PB.size() > 0)
			insert lst_pbe_inserts_Std_PB;
		if(lst_pbe_inserts.size() > 0)
			insert lst_pbe_inserts;				
		
		//update forecasts
		//
System.debug('...KN currdate===' + currdate);
System.debug('...KN set_prodIDs===' + set_prodIDs);
System.debug('...KN set_PBs===' + set_PBs);		
		List<OpportunityLineItemSchedule> optylines_schedules = new List<OpportunityLineItemSchedule>
																		([select id,OpportunityLineItem.Opportunity_PB__c,
																				ScheduleDate, Quantity, Revenue,
																				OpportunityLineItem.Product_ID__c
																				from OpportunityLineItemSchedule 
																				//where (ScheduleDate = :currdate or ScheduleDate = :currdate.addmonths(3)) 
																				where ScheduleDate >= :periods[0].StartDate and ScheduleDate <= :periods[1].EndDate
																				and OpportunityLineItem.Product_ID__c in :set_prodIDs
																				and OpportunityLineItem.Opportunity_PB__c in :set_PBs ]);
		//map_prod_PB_FWPricing
		List<OpportunityLineItemSchedule> optylines_schedules_updates = new List<OpportunityLineItemSchedule>();
		MLNX_Price_Book__c tmp_FW;
		
		for (OpportunityLineItemSchedule o : optylines_schedules) {
System.debug('...KN2 o.OpportunityLineItem.Opportunity_PB__c===' + o.OpportunityLineItem.Opportunity_PB__c);
System.debug('...KN2 o.OpportunityLineItem.Product_ID__c===' + o.OpportunityLineItem.Product_ID__c);
System.debug('...KN2 o.ScheduleDate===' + o.ScheduleDate);			
			if(map_prod_PB_FWPricing.get(o.OpportunityLineItem.Product_ID__c) != null && 
				map_prod_PB_FWPricing.get(o.OpportunityLineItem.Product_ID__c).get(o.OpportunityLineItem.Opportunity_PB__c) != null) {
					tmp_FW = map_prod_PB_FWPricing.get(o.OpportunityLineItem.Product_ID__c).get(o.OpportunityLineItem.Opportunity_PB__c);
					if (o.ScheduleDate <= periods[0].EndDate && tmp_FW.Current_Price__c != null) 
						o.Revenue = o.Quantity * tmp_FW.Current_Price__c;		
					else if (o.ScheduleDate <= periods[1].EndDate && tmp_FW.X1Quarter_out_Price__c != null) 
						o.Revenue = o.Quantity * tmp_FW.X1Quarter_out_Price__c;
					optylines_schedules_updates.add(o);
			}
		}
		if(optylines_schedules_updates.size() > 0)
			update optylines_schedules_updates;																														
	}//end only function
}