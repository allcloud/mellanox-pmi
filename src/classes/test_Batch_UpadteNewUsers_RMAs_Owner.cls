@isTest
public with sharing class test_Batch_UpadteNewUsers_RMAs_Owner {
	
	static testMethod void test_thebatch() {
		
		Contact c = new Contact();
        c.AccountId = '0015000000K0e8F';
        c.LastName = 'Test';
        c.Title = 'Test';
        c.Email = 'Test@Somewhere.com';
        c.CRM_Content_Permissions__c = 'Other';
        insert c;
        
        User u = new User();
        //Profile p = [Select p.Name, p.Id From Profile p Where Name='Call Center User' limit 1];
        //system.debug('BLAT p: ' + p);       
        u.Username = String.valueOf(math.random()*10) +'@gmail.com';
        u.Email = c.Email;
        u.CommunityNickname = 'Test';
        u.Alias = 'pogo1'; // we fabricate the alias above
        u.FirstName = 'first';
        u.LastName = 'last';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = '00e50000000pQX9';
        u.LanguageLocaleKey = 'en_US';
        u.IsActive = false;
        u.ContactId = c.Id;
        u.UserRoleId='00E50000000khfy';
        u.IsActive = true;
        insert u;
        
        CLS_ObjectCreator creator = new CLS_ObjectCreator();
        
        RMA__c newRma = creator.CreateRMA();
        newRma.Contact__c = c.Id;
        insert newRma;
        
        Test.startTest();
        
        scheduleUpadteNewUsers_RMAs_Owner sch = new scheduleUpadteNewUsers_RMAs_Owner();

        String jobId = System.schedule('ScheduleCases',
        '0 0 0 8 9 ? 2020', sch);
              
        Test.stopTest();
	}	
}