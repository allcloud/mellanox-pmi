global class scheduledPortalAccess implements Schedulable{   
 
 
  global void execute(SchedulableContext SC) {  
  
    List<Contact> Contacts = new List<Contact>();
    List<Contact> UpdContacts = new List<Contact>();
   
    Date myDate = Date.newInstance(2014, 10, 15);
	Time myTime = Time.newInstance(3, 3, 3, 0);
	DateTime contactCreatedDateLimit = DateTime.newInstance(myDate, myTime);
        
    Contacts = [select id, firstName, LastName, Portal_Approval__c,Portal_Access__c, Pre_portal__c, AccountId, Portal_Disable_date__c,Portal_Approval_date__c,email, disable_portal_access__c  from contact
    WHERE CreatedDate >: contactCreatedDateLimit]; 
   
    set<Id> AccountIds_Set = new set<Id>();
      
    for (Contact Con : Contacts) { 
     
        AccountIds_Set.add(con.AccountId);
    }   
     
    map<Id, Account> accounts2Contacts_Map = new Map<Id, Account>([SELECT Id, Customer_Type__c FROM Account WHERE Id IN : AccountIds_Set]);
    list<String> contactsAsJSON_List = new list<String>();
    
    for(contact cnt:Contacts) {
     
        if( cnt.Portal_Approval__c == 'Yes' && cnt.Portal_Access__c == null 
        && (cnt.Pre_portal__c == NULL || cnt.Pre_portal__c == 'No') ) {
          
               cnt = updateContactPortalApprovalYes(cnt, accounts2Contacts_Map);
            UpdContacts.add(cnt);
            
            String contactAsJSON_Str = JSON.serialize(cnt);
            contactsAsJSON_List.add(contactAsJSON_Str);   
        }   
    
        if(cnt.Portal_Approval__c == 'No' && cnt.Pre_portal__c == 'Yes'&& cnt.disable_portal_access__c == TRUE) {
          
            cnt.Portal_Disable_date__c = system.today();
            cnt.Pre_portal__c = 'No';  
            UpdContacts.add(cnt);   
           }
      }  
                         
    Update(UpdContacts); 
    
    createUsersOnContactUpdate(contactsAsJSON_List);  
   } 
   
   private Contact updateContactPortalApprovalYes(Contact cnt , map<Id, Account> accounts2Contacts_Map) {
     
     cnt.Portal_Approval_date__c = system.today();                                          
      cnt.Pre_portal__c = 'Yes';
      cnt.CRM_Content_Permissions__c = 'System NDA';
      
      if (accounts2Contacts_Map.get(cnt.AccountId).Customer_Type__c.EqualsIgnoreCase('OEM')) {
        
        cnt.Portal_Access__c = 'Mellanox System OEM Customer';
      }
      
      else {
        
        cnt.Portal_Access__c = 'Mellanox System Customer';
      }
      
      return cnt;      
 }
 
    @future
    private static void createUsersOnContactUpdate(list<String> contactsAsJSON_List) {  
     
       list<Contact> contacts_List = new list<Contact>();
       list<User> user2Create_List = new list<User>();
       
       Profile systemProfile = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox System Support Community User'];    
    Profile OEMProfile = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox System OEM Community User']; 
    
    system.debug('contactsAsJSON_List : ' + contactsAsJSON_List);    
       
       for (String contactAsJSON_Str : contactsAsJSON_List) {
         
         Contact theContact = new Contact();
         theContact = (Contact)JSON.deserialize(contactAsJSON_Str, Contact.class);
         contacts_List.add(theContact);
       }
       
       
       
       for (Contact TheContact : contacts_List) {
         
         system.debug('TheContact.Email : ' + TheContact.Email);
         system.debug('TheContact.firstName : ' + TheContact.firstName);
         system.debug('TheContact.LastName : ' + TheContact.LastName);
         system.debug('TheContact.Id : ' + TheContact.Id);
         
          
           User newUser = new User( userName = TheContact.Email, Email = TheContact.Email, FirstName = TheContact.firstName, LastName=TheContact.LastName, 
                       Alias=TheContact.LastName.substring(0,1), TimeZoneSidKey='America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', 
                       LocaleSidKey='en_US', ContactId= TheContact.Id);
                       
        if (TheContact.Portal_Access__c.EqualsIgnoreCase('Mellanox System OEM Customer')) {
          
          newUser.ProfileId = OEMProfile.Id;
        } 
        
        else {
          
          newUser.ProfileId = systemProfile.Id;   
        }  
        
        user2Create_List.add(newUser);
        
        if (!user2Create_List.isEmpty()) {
          
            Database.SaveResult[]  result = Database.insert(user2Create_List, false);
             
            for (Database.SaveResult sr : result) {
              
                if(!sr.isSuccess()) {
              
                    system.debug('==>error : '+sr.getErrors());
                }
              
                else {
                
                    system.debug('user2Insert Id :' + newUser.Id);
                }
             }
          }            
       }
    }
}