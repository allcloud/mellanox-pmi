@isTest
public with sharing class test_cls_WebService {
	
	static testMethod void test_ActivateDeactivate() {
		
		Contact c = new Contact();
        c.AccountId = '0015000000K0e8F';
        c.LastName = 'Test';
        c.Title = 'Test';
        c.Email = 'Test@Somewhere.com';
        c.CRM_Content_Permissions__c = 'Other';
        insert c;
        
        cls_WebService.activateUser(c.Id);
        cls_WebService.deactivateUser(c.Id);
        
        User u = new User();
              
        u.Username = String.valueOf(math.random()*10) +'@gmail.com';
        u.Email = 'kkkk@ssss.com';
        u.CommunityNickname = 'Test';
        u.Alias = 'pogo1'; // we fabricate the alias above
        u.FirstName = 'first';
        u.LastName = 'last';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = '00e50000000pQX9';
        u.LanguageLocaleKey = 'en_US';
        u.IsActive = false;
        u.ContactId = c.Id;
        u.UserRoleId='00E50000000khfy';
        insert u;
        
/*
        cls_WebService.activateUser(c.Id);
        cls_WebService.activateUser(c.Id);
        cls_WebService.deactivateUser(c.Id);
        cls_WebService.deactivateUser(c.Id);
        cls_WebService.activateUser(c.Id);
*/         
        
	}
}