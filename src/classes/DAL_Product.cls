public with sharing class DAL_Product {
	
	public static list<Product2> getProductsByPartNumbers()
	{
		return [SELECT Id, Name, Part_Number__c FROM Product2 WHERE Dangerous_goods__c =: true];
	}
	
	public static list<Product2> getProductsByPartNumbersForAssets(set<String> assetsPartNumbers_set)
	{
		return [SELECT Id, Name, Part_Number__c FROM Product2 WHERE Dangerous_goods__c =: true and Part_Number__c IN : assetsPartNumbers_set];
	}

}