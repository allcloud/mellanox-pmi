//public class JivePaginationUtil
public virtual with sharing class JivePaginationUtil 
{
    
public Integer currentConPage {get; set;}
public Integer pageCountCon {get; set;}
public Integer pageSize {get; set;}
public Integer listConSize {get; set;}
public  JivePaginationUtil( ){
    try{
        this.pageSize=5;
        this.pageCountCon=1;
        this.currentConPage=1;
        System.debug('In  JivePaginationUtil Constructor--'+pageSize);
    }catch(Exception ex){
        system.debug('Exception In  JivePaginationUtil Constructor--'+ex.getMessage());
    }
}


public virtual PageReference pageChangedCon() {
        String pageNo = ApexPages.currentPage().getParameters().get('pageNo');
        System.debug('In pageChangedCon--pageNo--'+pageNo);
        if(pageNo != NULL && (Pattern.compile('[1-9]{1}[0-9]*').matcher(pageNo).matches())) {
            currentConPage = Integer.valueOf(pageNo);
            }
        return NULL;
        }

//pagination        
 public List<String> getConPageNumbers() {
        System.debug('In getConPageNumbers--pageCountCon-- '+pageCountCon);
        List<String> pageNos = new List<String>();
        for(Integer i=1;i<=pageCountCon;i++) {
            String s = String.valueOf(i);
            pageNos.add(s);
            }
        system.debug('pageNos--'+pageNos);
        return pageNos;
 }
  
//public List<SelectOption> pageLst {get; set;}
public List<SelectOption> pageLst;
public virtual List<SelectOption> getPageLst(){
    system.debug('inside getPageLst--'+pageLst);
    if(pageLst == null){
        pageLst = new List<SelectOption>();
        pageLst.add(new SelectOption('5','5'));
        pageLst.add(new SelectOption('10','10'));
        pageLst.add(new SelectOption('15','15'));
        pageLst.add(new SelectOption('20','20'));
    }
    return pageLst;
}
 
 
 
}