public class cls_trg_Project_Milestone {
    
    public void updateClonedProjectsWithRelatedChildrenRecords(list<Milestone1_Project__c> projects_List){
        
        // A map between the Salesforce_ID__c(String representing the Id of the source of which this record was cloned from)
        // and the new record being cloned
        map<String, Milestone1_Project__c> projectsSalesforceIds2Projects_Map = new map<String, Milestone1_Project__c>();
         
        for (Milestone1_Project__c project : projects_List){
            
            if (project.Salesforce_ID__c != null && !project.Salesforce_ID__c.EqualsIgnoreCase(''))
            {  
                projectsSalesforceIds2Projects_Map.put(project.Salesforce_ID__c, project);
            }
        }
        
        // Assuming the map will hold values only when a record is cloned
        if (!projectsSalesforceIds2Projects_Map.isEmpty()){
        
            // List of the projects that the trigger.New records were cloned from with their related children records
            list<Milestone1_Project__c> originalProjectsClonedFrom_List = DAL_Project.getProjectsRelatedRecordsBySalesforceIds(projectsSalesforceIds2Projects_Map.keySet());
            // list of project related project members to insert all project members created upon cloning of record
            list<Project_Member__c> projectRelatedProjectMembers_List;
            
            if (!originalProjectsClonedFrom_List.isEmpty()){
                
                projectRelatedProjectMembers_List = new list<Project_Member__c>();
                for (Milestone1_Project__c project : originalProjectsClonedFrom_List){
                 
                    if (!project.Customer_Group_Member__r.isEmpty()){
                         
                        String projectIdStr =  String.valueOf(project.Id);
                        if (String.valueOf(project.Id).length() > 15){
                        
                            projectIdStr = String.valueOf(project.Id).substring(0,15); 
                        }  
                        for ( Project_Member__c member : project.Customer_Group_Member__r){
                            
                            Project_Member__c newMember = new Project_Member__c();
                            newMember.RecordTypeId = member.RecordTypeId;
                            newMember.Project__c = projectsSalesforceIds2Projects_Map.get(projectIdStr).Id;
                            newMember.Project_Member__c = member.Project_Member__c;
                            projectRelatedProjectMembers_List.add(newMember);
                        }
                    }
                }
                
                if (!projectRelatedProjectMembers_List.isEmpty())
                {
                    try{
                        Database.insert(projectRelatedProjectMembers_List, false);
                    }
                    
                    catch(Exception e){
                        system.debug('A problem cloning the Project : ' + e.getMessage());
                    }
                    
                }
            }
        }
    }
    
    // This method checks if the Project's Total_Duration_Hours__c is larger then 40 and if so,
    // it will go over it's related project memebers and turn their Project_Updated_Flag__c to TRUE
    public void ProjectMembersProjectUpdatedFlagRelated2Projects(list<Milestone1_Project__c> newProjects_List, map<Id, Milestone1_Project__c> oldProjects_Map) {
    	
    	list<Project_Member__c> projectMembers2Update_List = new list<Project_Member__c>();
    	set<String> changedProjectsIds_Set = new set<String>();
    	
    	for (Milestone1_Project__c newProject : newProjects_List) {	
    		
	    	if (newProject.Total_Duration_Hours__c != null) {	
	    		
	    		if (trigger.isInsert || (trigger.isUpdate && newProject.Total_Duration_Hours__c != oldProjects_Map.get(newProject.Id).Total_Duration_Hours__c) ) {
	    			
	    			if (newProject.Total_Duration_Hours__c > 40){	
	    				
	    				changedProjectsIds_Set.add(newProject.Id);
	    			}
	    		}
	    	}
    	}
    	
    	list <Milestone1_Project__c> relatedProjects_List;
    	
    	if (!changedProjectsIds_Set.isEmpty()) {
    		
    		relatedProjects_List = (DAL_Project.getProjectsRelatedRecordsBySalesforceIds(changedProjectsIds_Set));
    	
    		if (!relatedProjects_List.isEmpty()) {
    			
    			for (Milestone1_Project__c changedProject : relatedProjects_List) {
    				
    				if (!changedProject.Customer_Group_Member__r.isEmpty()) {
    					
    					for (Project_Member__c member : changedProject.Customer_Group_Member__r) {
    						
    						member.Project_Updated_Flag__c = true;
    						projectMembers2Update_List.add(member);
    					}
    				}
    			}
    			
    			if (!projectMembers2Update_List.isEmpty()) {
    				
    				try {
    					
    					update projectMembers2Update_List;
    				}
    				
    				catch(Exception e) {
    					
    					newProjects_List[0].addError('Could not update Project Members : ' + e.getMessage());
    				}
    			}
    		}
    	}
    }
    
    public void create2MembersWhenDiagnosticPackProjectIsCreated(list<Milestone1_Project__c> newProjects_List) {
    	
    	RecordType diagnosticPackRecordType = [SELECT Id FROM RecordType WHERE developerName =: 'Diagnostics_Pack' and SobjectType =: 'Milestone1_Project__c'];
    	
    	list<Project_Member__c> members2Create_List = new list<Project_Member__c>();
    	
    	for (Milestone1_Project__c project : newProjects_List) {
    		
    		if (project.RecordTypeId == diagnosticPackRecordType.Id) {
    			
	    		for (Integer i = 0; i < 2; i++) {		
	    			
	    			Project_Member__c newMember = new Project_Member__c();
	    			newMember.Project__c = project.id;
	    			
	    		    if (i == 0) {	
	    				newMember.Project_Member__c = '005500000014Kwv';  // Eyal Pitchadze
	    				newMember.Role__c = 'Project Manager';
	    		    }
	    		    
	    		    else {
	    		    	
	    		    	newMember.Project_Member__c = '00550000001tAvM'; // Ori Lapid 
	    		    	newMember.Role__c = 'Sales';
	    		    }
	    		    
	    		    members2Create_List.add(newMember);
	    		}
    		}
    	}
    	
    	if(!members2Create_List.isEmpty()) {
    		
    		insert members2Create_List;
    	}
    }
   
}