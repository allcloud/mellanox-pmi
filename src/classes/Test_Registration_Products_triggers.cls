/**
  */
@isTest
private class Test_Registration_Products_triggers {

    static testMethod void myUnitTest() {
        CLS_ObjectCreator cls = new CLS_ObjectCreator();
        Distributor_Oppy_Registrations__c odisioppreg = new Distributor_Oppy_Registrations__c();
        
        odisioppreg.Distributor_Name__c='BELL MICROPRODUCTS';
        odisioppreg.Requestor_Name__c='test';
        odisioppreg.Requestor_Email__c='Test@test.com';
        odisioppreg.Disty_Contact_Email__c='distiTest@test.com';
        odisioppreg.Disty_Contact_Name__c='disiTest';
        odisioppreg.Disty_Country__c = 'USA';
        odisioppreg.Disty_State__c = 'LA';
        
        odisioppreg.Disty_Phone__c='408544000';
        odisioppreg.Partner_City__c='SanJose';
        odisioppreg.Partner_Contact_Email__c='partnerTest@test.com';
        odisioppreg.Partner_Contact_Name__c='partnerTest';
        odisioppreg.Partner_Contact_Phone__c='4085447000';
        odisioppreg.Partner_Country__c='US';

        odisioppreg.Partner_State__c='CA';
        odisioppreg.Partner_Reseller_Name__c = 'Test';
        odisioppreg.End_User__c ='test user';
        odisioppreg.End_User_Contact_Name__c = ' test';
        
        odisioppreg.End_User_Contact_Email__c ='test@mail.com';
        odisioppreg.End_User_Country__c = 'USA';

        odisioppreg.End_User_State__c = 'CA';
        odisioppreg.End_User_Contact_Phone__c = '4089160001';
        
        odisioppreg.Opportunity_Close_Date__c=DateTime.now().date();
        odisioppreg.Name='TestOptyRegis_1';
        odisioppreg.Opportunity_Ship_Date__c=DateTime.now().date();

        insert odisioppreg;
        
        Product2 opr2 = cls.createProduct();
        opr2.Num_of_Sup__c = 1;
        opr2.Product_Type1__c = 'SERVICE';
        opr2.Family = 'HW SUPPORT';
        opr2.Disty_Status__c = 'Released - Dell Excluded';
        opr2.IsActive = true;
        insert opr2;
        
                
        List<PricebookEntry> lst_pbe = new List<PricebookEntry>();
        lst_pbe.add(new PricebookEntry (isActive=true, Product2id=opr2.id,Pricebook2id='01s5000000062WYAAY',UnitPrice=100));
        lst_pbe.add(new PricebookEntry (isActive=true, Product2id=opr2.id,Pricebook2id='01s500000006J09AAE',UnitPrice=85));
        lst_pbe.add(new PricebookEntry (isActive=true, Product2id=opr2.id,Pricebook2id='01s500000006J7KAAU',UnitPrice=70));
        insert lst_pbe;
        
        list<Opp_Reg_OPN__c> opRgOpns_lst = new list<Opp_Reg_OPN__c>(); 
        
        Opp_Reg_OPN__c opRegOpn1 = new Opp_Reg_OPN__c();
        opRegOpn1.Opp_Reg__c = odisioppreg.id;
        
        opRegOpn1.OPN__c = opr2.Id;
        opRegOpn1.OPN_Quantity__c = 10;
        opRegOpn1.OPN_Requested_Discount__c = 25;
        
        opRgOpns_lst.add(opRegOpn1);
        insert opRgOpns_lst;
        
    }
}