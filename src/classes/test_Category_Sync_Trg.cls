@isTest
public class test_Category_Sync_Trg {
   
    // This method tests the functionality and behaviour of the 'trg_Category_Sync' trigger and the 'Category_Sync_Handler' class
    static testMethod void test_assignCategoriesBySharePointId() {
        
        // Prepering data for insertion
        HowTo__kav howTo = new HowTo__kav();
        howTo.SharePoint_ID__c = '22224';
        howTo.Title = 'How to title';
        howTo.UrlName = 'How-to-title';
     //   howTo.PublishStatus = 'Online';
        howTo.Language = 'en_US';
        //insert howTo;
        
        FAQ__kav kav = new FAQ__kav();
        kav.SharePoint_ID__c = '22225';
        kav.Title = 'How to title';
        kav.UrlName = 'How-to-title-two';
  //      kav.PublishStatus = 'Online';
        kav.Language = 'en_US';
        //insert kav;
        
        CRA__kav cra = new CRA__kav();
        cra.SharePoint_ID__c = '22226';
        cra.Title = 'How to title';
        cra.UrlName = 'How-to-title-three';
     //   cra.PublishStatus = 'Online';
        cra.Language = 'en_US';
        //insert cra;
        
        Common_Errors__kav error = new Common_Errors__kav();
        error.SharePoint_ID__c = '22227';
        error.Title = 'How to title';
        error.UrlName = 'How-to-title-four';
      //  error.PublishStatus = 'Online';
        error.Language = 'en_US';
        //insert error;
        
        
        // Creating the object records for testing 
        Category_Sync__c catSync = new Category_Sync__c();
        catSync.Name = 'Elad Test';
        catSync.SP_Article_Id__c = howTo.SharePoint_ID__c;
        catSync.DATACATEGORYNAME__c  = 'Software';
        catSync.DATACATEGORYGROUPNAME__c = 'Mellanox_Products';
        catSync.Article_Type__c  = 'HowTo';
        
        Category_Sync__c catSync1 = new Category_Sync__c();
        catSync1.Name = 'Elad Test 1';
        catSync1.SP_Article_Id__c = kav.SharePoint_ID__c;
        catSync1.DATACATEGORYNAME__c  = 'Software';
        catSync1.DATACATEGORYGROUPNAME__c = 'Mellanox_Products';
        catSync1.Article_Type__c  = 'FAQ';
        
        Category_Sync__c catSync2 = new Category_Sync__c();
        catSync2.Name = 'Elad Test 2';
        catSync2.SP_Article_Id__c = cra.SharePoint_ID__c;
        catSync2.DATACATEGORYNAME__c  = 'Software';
        catSync2.DATACATEGORYGROUPNAME__c = 'Mellanox_Products';
        catSync2.Article_Type__c  = 'CRA';
        
        Category_Sync__c catSync3 = new Category_Sync__c();
        catSync3.Name = 'Elad Test 3';
        catSync3.SP_Article_Id__c = error.SharePoint_ID__c;
        catSync3.DATACATEGORYNAME__c  = 'Software';
        catSync3.DATACATEGORYGROUPNAME__c = 'Mellanox_Products';
        catSync3.Article_Type__c  = 'Common Error';
        
        // We're querying for Inna's user to run the tests as her user, as she is one of 
        // the only users, authorizes to insert new 'Category_Sync__c' records
        User userAdmin = [SELECT Id FROM User WHERE UserPermissionsKnowledgeUser = true limit 1];
        
        
        System.runAs(userAdmin) {
        
            // Running TDD (test Driven development)
            system.test.startTest();
            
            insert howTo;
            insert kav;
            insert cra;  
            insert error;  
            
            insert catSync;
            
            insert catSync1;
            
            insert catSync2;
            
            insert catSync3;
            
            system.test.stopTest();
        }
        
    }

}