public class VFOverrideOppLineItem 
{
    public Opportunity currOpp {get;set;}
    public OpportunityLineItem currOli {get;set;}
    
    public VFOverrideOppLineItem(ApexPages.StandardController controller)
    {       
        ID OliId = controller.getRecord().Id;
        system.debug('BLAT Apexpages.currentPage().getUrl(): ' + Apexpages.currentPage().getUrl());
        currOli = [Select Id, OpportunityId From OpportunityLineItem Where IsDeleted=false and Id=:OliId limit 1][0];
        currOpp = [Select R90_Force_Update__c, Id, R90_Forecast_Flag__c From Opportunity Where Id =: currOli.OpportunityId and IsDeleted = false];  
    }
    
    public static testMethod void Testeo()
    {   US_Territory_Map__c tm = new US_Territory_Map__c( SD_Region__c = 'EMEA', Region__c = '0000'); 
        tm.Sales_Director__c = '00550000000wuVp';
        insert tm;
        Account acc1 = new Account(name = 'test22',support_center__c = 'Global',is_oracle_parent__c = 'Yes',  type = 'OEM', RecordTypeID = '01250000000DP1n');
        insert acc1;

        Opportunity Op = new Opportunity(ownerid ='00550000000wuVp',primary_partner_N__c= acc1.id,Name = 'test opp', StageName = 'Discovery', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
        insert op;
        Pricebook2 p = new Pricebook2(Name = 'TestPriceBook', IsActive = true);
        insert p;
        Pricebook2 pricebookStandard = [Select p.Name, p.Id From Pricebook2 p where p.Name like 'Standard%' limit 1][0];
        op.Pricebook2Id = pricebookStandard.Id;
        update op;
        Product2 product = new Product2(Name = 'Test Product', Inventory_Item_id__c ='123450987');
        insert product; 
        PricebookEntry pre = new PricebookEntry(Product2Id = product.Id,Pricebook2Id = pricebookStandard.Id,UnitPrice = 100,UseStandardPrice = false,IsActive=true);
        insert pre;
        OpportunityLineItem oli = new OpportunityLineItem (OpportunityId = op.Id,PricebookEntryId = pre.Id,Quantity = 1,UnitPrice = 1000);
        insert oli; 
        Apexpages.currentPage().getParameters().put('Id',oli.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(oli);
        VFOverrideOppLineItem cont = new VFOverrideOppLineItem(stdController);
    }
}