global class Schedule_Cases_Breached_EMC implements Schedulable{
    
    // This metohd executes the class 'Batch_Cases_Breached_EMC' with a scheduler
    global void execute(SchedulableContext SC) {
    
        Batch_Cases_Breached_EMC batch = new Batch_Cases_Breached_EMC();
        Database.executeBatch(batch);
   }

}