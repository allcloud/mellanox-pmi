public with sharing class cls_trg_Product2 {
	
	// This method will connect all assets that relates to the New product (WHERE Asset Part Number  = Product Name) via the lookup field 'Product__c'
	// every time a new product is created
	public void assignAssetsToProductByPartNumbers(list<Product2> newProducts_List) {
		
		 map<String, Id> productsNames2ProductIds_Map = new  map<String, Id>();
		 
		 for (Product2 newProduct : newProducts_List) {
		 	
		 	productsNames2ProductIds_Map.put(newProduct.Name, newProduct.Id);
		 }
		 
		 list<Asset2__c> productsRelatedAssets_List = getAssetsRelatedToNewProducts(productsNames2ProductIds_Map);
		 
		 for (Asset2__c asset : productsRelatedAssets_List) {
		 	
		 	if (productsNames2ProductIds_Map.containsKey(asset.part_number__c)) {
		 		
		 		asset.Product__c = productsNames2ProductIds_Map.get(asset.part_number__c);
		 	}
		 }
		 
		 update productsRelatedAssets_List;
	}
	
	//This method returns a list of all assets related to new Products by Product Name (Asset Part Number  = Product Name)
	private list<Asset2__c> getAssetsRelatedToNewProducts(map<String, Id> productsNames2ProductIds_Map) {
		
		list<Asset2__c> productsRelatedAssets_List = [SELECT Id, part_number__c, Product__c FROM Asset2__c WHERE part_number__c IN : productsNames2ProductIds_Map.keySet()];
		
		return productsRelatedAssets_List;
	}
	
	// This method will update the product field 'Image_URL__c' with the Salesfroce org prefix + the Product detail 'StaticResourceImage__c' 
	// every time the Product details on the Product is changed OR every time a new Product is created and it has a product detail associated to it
	public void updateProductPhotoUrlOnProductDetailChange(list<Product2> newProducts_List, map<Id, Product2> oldProducts_Map) {
		
		map<Id, ProductDetails__c> productDetailsIds2ProductDetails_Map = getRelatedProductDetails(newProducts_List, oldProducts_Map);
		
		for (Product2 newProduct : newProducts_List) {
			
			if (newProduct.Product_Detail__c != null) {
				
				if (productDetailsIds2ProductDetails_Map != null && productDetailsIds2ProductDetails_Map.containsKey(newProduct.Product_Detail__c)) {
					
					if (productDetailsIds2ProductDetails_Map.get(newProduct.Product_Detail__c).StaticResourceImage__c != null) {
						
						newProduct.Image_URL__c = 'https://mymellanox.force.com/support/resource/' +productDetailsIds2ProductDetails_Map.get(newProduct.Product_Detail__c).StaticResourceImage__c;
					}
				}
			}
		}
	}
	
	// This method returns a map of Product Details map with Product details Ids as Keys and Product details as values
	private map<Id, ProductDetails__c> getRelatedProductDetails(list<Product2> newProducts_List, map<Id, Product2> oldProducts_Map) {
		
		map<Id, ProductDetails__c> productDetailsIds2ProductDetails_Map;
		set<Id> productDetailsIds_Set = new set<Id>();
		
		for (Product2 newProduct : newProducts_List) {
			
			if (newProduct.Product_Detail__c != null) {
				
				if (newProduct.Product_Detail__c != oldProducts_Map.get(newProduct.Id).Product_Detail__c) {
					
					productDetailsIds_Set.add(newProduct.Product_Detail__c);
				}  
			}
		}
		
		if (!productDetailsIds_Set.isEmpty()) {
			
			productDetailsIds2ProductDetails_Map = new map<Id, ProductDetails__c>([SELECT Id, StaticResourceImage__c FROM ProductDetails__c
																				   WHERE Id IN : productDetailsIds_Set]);
		}
		
		return productDetailsIds2ProductDetails_Map;
	}
	
	// This method will assign Product Detail to the newly created Product (Product2) if the Product has a model ('MODEL_NUMBER__c') 
	// and will update the field 'Image_URL__c' from select Product Detail 
	public void assignProductDetailsToNewProducts(list<Product2> newProducts_List, map<Id, Product2> oldProducts_Map) {
		
		map<String, ProductDetails__c> productDetailsNames2ProductDetailsIds_Map = getRelatedProductDetailsOnModelChangeOrInsert(newProducts_List, oldProducts_Map);
		
		for (Product2 newProduct : newProducts_List) {  
			
			if (productDetailsNames2ProductDetailsIds_Map.containsKey(newProduct.Product_Detail_Name__c)) {
				
				if (newProduct.Product_Detail__c == null) {
					
					newProduct.Product_Detail__c = productDetailsNames2ProductDetailsIds_Map.get(newProduct.Product_Detail_Name__c).Id;
				}
				if (productDetailsNames2ProductDetailsIds_Map.get(newProduct.Product_Detail_Name__c).StaticResourceImage__c != null) {
						
					newProduct.Image_URL__c = 'https://mymellanox.force.com/support/resource/' + productDetailsNames2ProductDetailsIds_Map.get(newProduct.Product_Detail_Name__c).StaticResourceImage__c;
				}
			}
		}
	}
	
	// This method returns a map of Product Details names as keys and Product Details as values
	private map<String, ProductDetails__c> getRelatedProductDetailsOnModelChangeOrInsert(list<Product2> newProducts_List, map<Id, Product2> oldProducts_Map) {
		
		map<String, ProductDetails__c> productDetailsNames2ProductDetailsIds_Map = new map<String, ProductDetails__c>();
		set<String> productDetailsNames_Set = new set<String>();
		
		for (Product2 newProduct : newProducts_List) {
			
			if (newProduct.Product_Detail_Name__c != null) { 
				
				if (oldProducts_Map  != null) {
					
					if (newProduct.Product_Detail_Name__c != oldProducts_Map.get(newProduct.Id).Product_Detail_Name__c) {
						
						productDetailsNames_Set.add(newProduct.Product_Detail_Name__c);
					}	
				}
				
				else {
					
					productDetailsNames_Set.add(newProduct.Product_Detail_Name__c);
				}
			}
		}
		
		if (!productDetailsNames_Set.isEmpty()) {
			
			for (ProductDetails__c productDetail : [SELECT Id, StaticResourceImage__c, Name FROM ProductDetails__c WHERE Name IN : productDetailsNames_Set]) {
				
				productDetailsNames2ProductDetailsIds_Map.put(productDetail.Name, productDetail);
			}
		}
		
		return productDetailsNames2ProductDetailsIds_Map;
	}
}