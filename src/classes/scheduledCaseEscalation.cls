global class scheduledCaseEscalation implements Schedulable{  

global void execute(SchedulableContext SC) {


integer SendEmail =0;
string EmailTemplate = '';
List<case> Cases = new List<case>();
List<user> InternalUsers = new List<user>();
List<String> toAddresses = new List<String>();

Cases = [select id,ownerId from case c where state__c = 'Open' AND (ownerId = :ENV.SystemQueue OR ownerId = :ENV.UnassignedUS OR ownerId = :ENV.UnassignedEMEA)  AND              
  c.Account.Contract_Type_Final__c != 'Gold' AND  IS_SPAM__c = 'NO' AND  recordTypeId = :ENV.caseTypeSupport AND
  ST1AUTOASSIGN_fast_forward__c = False AND ST1_Move_2_GSO__c = False AND Is_design_customer__c != 1 AND c.Account.Strategic_Customer__c = False
  ]; 


for(case c:Cases)
{
  c.ownerId = ENV.UnassignedST1;  
          
} 

update Cases;
}
}