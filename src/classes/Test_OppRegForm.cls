@isTest
private class Test_OppRegForm {

    static testMethod void Test() 
    {
        Test.setCurrentPageReference(Page.OppRegForm);
        OppRegFormController oform=new OppRegFormController();
        Pagereference pg;
        pg=oform.Reset();
        System.assertEquals('/apex/oppregform', pg.getUrl());
        oform.GetDistiList();
        oform.AddOppRegOPN();
        oform.RemoveOppRegOPN();
        List<selectOption> allCount = oform.getAllCountries();
        
        oform.odisioppreg.Distributor_Name__c='BELL MICROPRODUCTS';
        oform.odisioppreg.Requestor_Name__c='test';
        oform.odisioppreg.Requestor_Email__c='Test@test.com';
        oform.odisioppreg.Disty_Contact_Email__c='distiTest@test.com';
        oform.odisioppreg.Disty_Contact_Name__c='disiTest';
        oform.odisioppreg.Disty_Country__c = 'USA';
        List<selectOption> def  = oform.getDistStates();
        
        oform.odisioppreg.Disty_State__c = 'LA';
        
        oform.odisioppreg.Disty_Phone__c='408544000';
        oform.odisioppreg.Partner_City__c='SanJose';
        oform.odisioppreg.Partner_Contact_Email__c='partnerTest@test.com';
        oform.odisioppreg.Partner_Contact_Name__c='partnerTest';
        oform.odisioppreg.Partner_Contact_Phone__c='4085447000';
        oform.odisioppreg.Partner_Country__c='US';
        List<selectOption> def1  =  oform.getPartnerStates();
        oform.odisioppreg.Partner_State__c='CA';
        oform.odisioppreg.Partner_Reseller_Name__c = 'Test';
        oform.odisioppreg.End_User__c ='test user';
        oform.odisioppreg.End_User_Contact_Name__c = ' test';
        
        oform.odisioppreg.End_User_Contact_Email__c ='test@mail.com';
        oform.odisioppreg.End_User_Country__c = 'Cyprus';
        List<selectOption> def2  =  oform.getEndUserStates();
        oform.odisioppreg.End_User_State__c = 'EU';
        oform.odisioppreg.End_User_Contact_Phone__c = '45304958';
        //oform.odisioppreg.
        //oform.odisioppreg.
        oform.odisioppreg.Opportunity_Close_Date__c=DateTime.now().date();
        oform.odisioppreg.Name='TestOppTest99929';
        oform.odisioppreg.Opportunity_Ship_Date__c=DateTime.now().date();
      
       
        //oform.odisioppreg.OPN_1_MSRP__c=100;
        //oform.odisioppreg.OPN_1_Quantity__c=10;
       // oform.odisioppreg.OPN_1_Requested_Cost__c=10;
        //oform.odisioppreg.OPN_1_Requested_Discount__c=10;
        CLS_ObjectCreator cls = new CLS_ObjectCreator();
        
        Product2 opr2 = cls.createProduct();
        opr2.Num_of_Sup__c = 1;
        insert opr2;               
                      
        Product2 SupOpr2 = cls.createProduct();         
        SupOpr2.Inventory_Item_id__c = '222';          
        SupOpr2.Name = 'inna support444';     
        insert SupOpr2;
                    
                             
                 
      //oform.lst_MellanoxProducts[0].OppRegOPN.OPN__c = 'a0C50000002T52yEAC';
        oform.lst_MellanoxProducts[0].OppRegOPN.OPN__c = opr2.Id;        
        oform.lst_MellanoxProducts[0].OppRegOPN.Name = 'RegProd';
         
        oform.lst_MellanoxProducts[0].OppRegOPN.OPN_Quantity__c = 2;
        oform.lst_MellanoxProducts[0].OppRegOPN.OPN_Requested_Discount__c = 10;
        oform.lst_MellanoxProducts[0].getRelevantSupportedItems();
        //oform.lst_MellanoxProducts[0].setSupItmSelectedFrmList('a0C50000002T52iEAC');
        
        //oform.lst_MellanoxProducts[0].supItmSelectedFrmList = 'a0C50000002T52iEAC';
        oform.lst_MellanoxProducts[0].OppRegOPN_SupportItem.OPN_Quantity__c = 2;
        //oform.lst_MellanoxProducts[0].OppRegOPN_SupportItem.Mellanox_OPN__c = 'a0C50000002T52iEAC';
         
        oform.Save();
        
        integer count=[select count() from Distributor_Oppy_Registrations__c where Name=:oform.odisioppreg.Name];
        //system.assert(count>0);
        
        OppRegFormController oform2 =new OppRegFormController();
        //oform.lst_MellanoxProducts[0].OppRegOPN.Mellanox_OPN__c = 'a0C50000002T52yEAC';
        oform.lst_MellanoxProducts[0].OppRegOPN.OPN__c = opr2.Id;
        
        oform.lst_MellanoxProducts[0].OppRegOPN.OPN_Quantity__c = 2;
        oform.lst_MellanoxProducts[0].OppRegOPN.OPN_Requested_Discount__c = 10;
        oform.lst_MellanoxProducts[0].getRelevantSupportedItems();
        //oform.lst_MellanoxProducts[0].setSupItmSelectedFrmList('a0C50000002T52iEAC');
        oform.lst_MellanoxProducts[0].setSupItmSelectedFrmList(opr2.Id);
        
        
        //oform.lst_MellanoxProducts[0].supItmSelectedFrmList = 'a0C50000002T52iEAC';
        oform.lst_MellanoxProducts[0].OppRegOPN_SupportItem.OPN_Quantity__c = 2;
        //oform.lst_MellanoxProducts[0].OppRegOPN_SupportItem.Mellanox_OPN__c = 'a0C50000002T52iEAC';
        
        
        
        //oform.lst_MellanoxProducts[1].OppRegOPN.Mellanox_OPN__c = 'a0C50000002T52yEAC';
        oform.lst_MellanoxProducts[1].OppRegOPN.OPN__c = opr2.Id;
        oform.lst_MellanoxProducts[2].OppRegOPN.OPN_Quantity__c = 2;
        oform.lst_MellanoxProducts[3].OppRegOPN.OPN_Requested_Discount__c = 10;
        oform.lst_MellanoxProducts[4].OppRegOPN_SupportItem.OPN_Quantity__c = 2;
        
        oform2.Save();
        
        
        // TO DO: implement unit test 
    }
}