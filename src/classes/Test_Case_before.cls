/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
*
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest(seeAllData=true)
public class Test_Case_before {
    
    
    static testMethod void Case_after_insert_Account_team() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        Account acc  = obj.CreateAccount();
        acc.name = 'testcaseMarch';
        List<User> NewUsr = new List<User>();
        
        
        
        Contact usrcont  = obj.CreateContact(acc);
        insert usrcont;
        
        
        User u1 = obj.CreateUser();
        u1.CommunityNickname = 'nntest';
        u1.Username = 'nntest@ww.com';
        
        NewUsr.add(u1);
        User u2 = obj.CreateUser();
        u2.Username = 'nntest1@ww.com';
        u2.CommunityNickname = 'nntest1';
        NewUsr.add(u2);
        User u3 = obj.CreateUser();
        u3.CommunityNickname = 'nntest2';
        u3.Username = 'nntest2@ww.com';
        NewUsr.add(u3);
        User u4 = obj.CreateUser();
        u4.CommunityNickname = 'nntest3';
        u4.Username = 'nntest3@ww.com';
        
        NewUsr.add(u4);
        insert  NewUsr;
        
        acc.Case_team_user1__c = u1.id;
        acc.Case_team_user2__c = u2.id;
        acc.Case_team_user3__c = u3.id;
        acc.Sales_Owner__c = u1.id;
        acc.BusDev_Owner__c = u2.id;
        insert acc;
        
        Case Cs = obj.Create_case(acc, usrcont);
        Case Cs1 = obj.Create_case(acc, usrcont);
        
        insert cs;
        //insert cs1;
        
    }
    static testMethod void myUnitTest() {
        
        SLA__c sla = new SLA__c(severity__c='Low');
        
        sla.Fix__c = 6;
        sla.WA__c = 7;
        
        insert sla;
        
        Case c = new Case(priority = 'Low');
        c.Escalation_Q_Owner__c = UserInfo.getUserId();
        c.Optional_AE_Owners__c = UserInfo.getLastName();
        
        Case c1 = new Case(Problem_Support_Level__c = 'L1 aaa');
        Case c2 = new Case(Problem_Support_Level__c = 'L2 aaa');
        Case c3 = new Case(Problem_Support_Level__c = 'L3 aaa');
        
        
        insert c;
        // insert c1;
        // insert c2;
        // insert c3;
    }
    
    
    static testMethod void myUnitTest_2() {
        
        User u = [Select u.Id from User u Where u.IsActive = true LIMIT 1];
        
        Contact con = new Contact(LastName='Bsh',
        Contact_Type__c ='Mellanox Silicon Design-In customer',
        technical_owner__c = u.Id);
        
        insert con;
        
        Case c = new Case(priority = 'Low',ContactId = con.Id,
        state__c = 'Open');
        
        insert c;
    }
    
    static testMethod void myUnitTest_3() {
        
        User u = [Select u.Id from User u Where u.IsActive = true LIMIT 1];
        
        Contact con = new Contact(LastName='Bsh',
        Contact_Type__c ='Mellanox Silicon Design-In customer',
        technical_owner__c = u.Id,
        Deny_Access__c = 'Customer Portal Access',
        CRM_Content_Permissions__c = 'Other');
        
        insert con;
        
        Case c = new Case(priority = 'Low',ContactId = con.Id);
        
        insert c;
        
        c.State__c = 'Open';
        
        update c;
    }
    
    static testMethod void myUnitTest_4() {
        
        User u = [Select u.Id from User u Where u.IsActive = true LIMIT 1];
        
        Contact con = new Contact(LastName='Bsh',
        Contact_Type__c ='Mellanox Silicon Design-In customer',
        technical_owner__c = u.Id,
        Deny_Access__c = 'Self Service Portal Access',
        CRM_Content_Permissions__c = 'Other');
        
        insert con;
        
        Case c = new Case(priority = 'Low',ContactId = con.Id);
        
        insert c;
        c.State__c = 'Open';
        update c;
        c.AE_Class__c = 'AE class';
        c.AE_Sub_Class__c = 'AE sub class';
        c.AE_engineer__c = '005500000013kUq';
        c.AE_PM_Escalated__c = 'AE';
        c.FM_state__c = 'Assigned';
        c.Requested_Delivery_Date__c = system.today();
        
        update c;
    }
    
    static testMethod void test_addTeamMemberToCaseRelatedProject() {
        
        Id userId =  system.UserInfo.getUserId();
        
        id recordTypePOCId = [SELECT Id FROM RecordType WHERE developerName =: 'Project_POC'].Id;
        
        Milestone1_Project__c relatedProj = [SELECT Id, RecordTypeId FROM Milestone1_Project__c WHERE RecordTypeId =: recordTypePOCId and Deadline__c != null limit 1];
        
        
        Contact con = new Contact(LastName='Bsh',
        Contact_Type__c ='Mellanox Silicon Design-In customer',
        technical_owner__c = userId);
        
        insert con;
        
        Case c = new Case(priority = 'Low',ContactId = con.Id,
        state__c = 'Open');
        
        c.Related_Project__c = relatedProj.id;
        
        insert c;
        
        c.Assignee__c = userId;
        
        update c;
    }
    
    static testMethod void test_caseCriticalBug() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        Id RMcaseRecordType_Id = [SELECT Id FROM RecordType WHERE DeveloperName =: 'RM_case'].Id;
        
        Account acc = obj.createAccount();
        acc.Type = 'OEM';
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        Case cs = obj.Create_case(acc, con);
        cs.Business_impact__c = 'Critical';
        cs.AE_PM_Escalated__c = 'PM';
        cs.AE_Class__c = 'Host';
        cs.AE_Sub_Class__c = 'UFM';
        cs.AE_Engineer__c = UserInfo.getUserId();
        insert cs;
        
        RMCase__c newFRCase = new RMCase__c();
        newFRCase.sfcase__c = cs.Id;
        newFRCase.RecordTypeId = RMcaseRecordType_Id;
        
        
        insert newFRCase;
        
        cs.Critical_Bug__c = true;
        
        update cs;
        
        cs.Critical_Bug__c = false;
        
        update cs;
        
        RMCase__c newFRCase2 = new RMCase__c();
        newFRCase2.sfcase__c = cs.Id;
        newFRCase2.RecordTypeId = RMcaseRecordType_Id;
        insert newFRCase2;
        
        cs.Critical_Bug__c = true;
        
        Test.startTest();
        update cs;
        
        cs.Critical_Bug__c = false;
        
        update cs;
        
        RMCase__c newFRCase3 = new RMCase__c();
        newFRCase3.sfcase__c = cs.Id;
        newFRCase3.RecordTypeId = RMcaseRecordType_Id;
        insert newFRCase3;
        
        cs.Critical_Bug__c = true;
        
        update cs;
        
        cs.Critical_Bug__c = false;
        
        update cs;
        
        RMCase__c newFRCase4 = new RMCase__c();
        newFRCase4.sfcase__c = cs.Id;
        newFRCase4.RecordTypeId = RMcaseRecordType_Id;
        insert newFRCase4;
        
        cs.Critical_Bug__c = true;
        
        update cs;
    }
    
    static testMethod void test_addCaseTeamMembersOnCaseUpdateToFatal() {
        
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        acc.Type = 'OEM';
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        User u = new User();
        //Profile p = [Select p.Name, p.Id From Profile p Where Name='Call Center User' limit 1];
        //system.debug('BLAT p: ' + p);
        u.Username = String.valueOf(math.random()*10) +'@gmail.com';
        u.Email = 'kkkk@ssss.com';
        // u.CommunityNickname = 'Test';
        u.Alias = 'pogo1'; // we fabricate the alias above
        u.FirstName = 'Ofer';
        u.LastName = 'Muki';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = '00e50000000oCgX';
        u.LanguageLocaleKey = 'en_US';
        u.IsActive = true;
        //u.ContactId = c.Id;
        //u.UserRoleId='00E50000000khfy';
        insert u;
        
        Case cs = obj.Create_case(acc, con);
        cs.Business_impact__c = 'Critical';
        cs.AE_PM_Escalated__c = 'PM';
        cs.AE_Class__c = 'Host';
        cs.AE_Sub_Class__c = 'UFM';
        cs.AE_Engineer__c = UserInfo.getUserId();
        cs.Assignee__c = system.UserInfo.getUserId();
        Test.startTest();
        insert cs;
        
        cs.ISFATAL__c = true;
        cs.priority = 'Fatal';
        update cs;
        
        Test.StopTest();
    }
    
    static testmethod void testCasePriortityFatal() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        acc.Type = 'OEM';
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        
        Case cs = obj.Create_case(acc, con);
        cs.Business_impact__c = 'Critical';
        cs.AE_PM_Escalated__c = 'PM';
        cs.AE_Class__c = 'Host';
        cs.AE_Sub_Class__c = 'UFM';
        cs.AE_Engineer__c = UserInfo.getUserId();
        insert cs;
        
        Case cs1 = obj.Create_case(acc, con);
        cs1.Business_impact__c = 'Critical';
        cs1.AE_PM_Escalated__c = 'PM';
        cs1.AE_Class__c = 'Host';
        cs1.AE_Sub_Class__c = 'UFM';
        cs1.AE_Engineer__c = UserInfo.getUserId();
        cs1.RMA_Related_case__c = cs.Id;
        
        Test.startTest();
        insert cs1;
        
        cs.ISFATAL__c = true;
        cs.priority = 'Fatal';
        
        cs.RecordTypeId = ENV.CaseTypeSupport;
        
        update cs;
        
        Test.StopTest();
    }
    
    static testmethod void testCasePriortityFatalChild() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        acc.Type = 'OEM';
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        
        Case cs = obj.Create_case(acc, con);
        cs.Business_impact__c = 'Critical';
        cs.AE_PM_Escalated__c = 'PM';
        cs.AE_Class__c = 'Host';
        cs.AE_Sub_Class__c = 'UFM';
        cs.AE_Engineer__c = UserInfo.getUserId();
        
        cs.ISFATAL__c = true;
        cs.priority = 'Fatal';
        
        Test.StartTest();
        cs.RecordTypeId = ENV.CaseTypeSupport;
        //insert cs;
        
        Case cs2 = obj.Create_case(acc, con);
        cs2.Business_impact__c = 'Critical';
        cs2.AE_PM_Escalated__c = 'PM';
        cs2.AE_Class__c = 'Host';
        cs2.AE_Sub_Class__c = 'UFM';
        cs2.AE_Engineer__c = UserInfo.getUserId();
        cs2.RMA_Related_case__c = cs.Id;
        insert cs2;
        
        cs2.Delete_This_Case__c = true;
        update cs2;
        
        cs2.Delete_This_Case__c = false;
        update cs2;
        
        Test.StopTest();
        
    }
    
    static testmethod void testCaseCreatedFromMobile() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        acc.Type = 'OEM';
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        
        Case cs = obj.Create_case(acc, con);
        cs.Business_impact__c = 'Critical';
        cs.AE_PM_Escalated__c = 'PM';
        cs.AE_Class__c = 'Host';
        cs.AE_Sub_Class__c = 'UFM';
        cs.AE_Engineer__c = UserInfo.getUserId();
        
        cs.ISFATAL__c = true;
        cs.priority = 'Fatal';
        
        Test.StartTest();
        cs.RecordTypeId = ENV.CaseTypeSupport;
        cs.Origin = 'Mobile';
        insert cs;
        
        Test.StopTest();
    }
    
}