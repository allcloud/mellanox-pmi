global class mlnxMoblieLoginController {
    
     global String username {get; set;}
    global String password {get; set;}
    
     
    global PageReference login() {
       
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        
        if (startUrl == null) {
            
            startUrl = '/support/apex/mlnxMobileHomeV2';
        }
        
        
        if (username != 'test@mellanox.com' && username != 'taniacom@mellanox.com'
            && username != 'katia311@gmail.com' && username != 'Patelpranav@gmail.com'
            && username != 'telyas@gmail.com' && username != 'Yairgoldel@gmail.com'
            && username != 'tcpadam@gmail.com' && username != 'p.hofstad@btinternet.com'
            && username != 'zachengel@gmail.com' && username != 'udiw@mellanox.com'
            && username != '' && username != '') {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'You are not authorized to access the application'));
                return null;
            }
                
            return Site.login(username, password, startUrl);
    }
        
        
    //global SiteLoginController () {}
    
    global static testMethod void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        mlnxMoblieLoginController controller = new mlnxMoblieLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                             
    }    

}