@isTest(seeAllData=true)
public with sharing class test_VF_Product_Subscription {

	static testMethod void test_VF_Product_Subscription() {
		
		Id userId = system.UserInfo.getUserId();
		
		ProductDetails__c theProdDetail = [SELECT Id FROM ProductDetails__c WHERE Active__c = true limit 1];
		
		Product_Subscriptions__c prodSubs1 = new Product_Subscriptions__c();
		prodSubs1.Subscriber__c = userId;
		prodSubs1.Product__c = theProdDetail.Id;
		insert prodSubs1;
		
		Product_Subscriptions__c prodSubs = new Product_Subscriptions__c();
		prodSubs.Subscriber__c = userId;
		prodSubs.Product__c = theProdDetail.Id;
		insert prodSubs;
		
		ContentVersion content = [SELECT Id, ContentDocumentId FROM ContentVersion limit 1];
		
		Document_Subscriptions__c docSubs = new Document_Subscriptions__c();
		docSubs.User__c = userId;
		docSubs.Document__c = content.ContentDocumentId;
		insert docSubs;
		
		apexPages.Standardcontroller controller = new apexPages.Standardcontroller(prodSubs);
        VF_Product_Subscription handler = new VF_Product_Subscription(controller );
        
        ApexPages.currentPage().getParameters().put('rowIndex', '1');
        ApexPages.currentPage().getParameters().put('rowIndex1', '1');
        handler.deleteRow();
        handler.restoreRow();
        handler.testHelper();
       // handler.deleteDocsRow();
       // handler.restoreDocsRow();
		
	}
}