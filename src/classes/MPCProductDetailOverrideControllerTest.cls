@isTest
private class MPCProductDetailOverrideControllerTest {

    /*static testMethod void testRedirect() {
        MPCProductDetailOverrideController cntrlr = new MPCProductDetailOverrideController(new ApexPages.StandardController(new Account()));
        System.assertNotEquals(null, cntrlr.redirect());
    }*/

    static testMethod void testNoRedirect() {
        MPCProductDetailOverrideController cntrlr = new MPCProductDetailOverrideController(new ApexPages.StandardController(new Account()));
        System.assertEquals(null, cntrlr.redirect());
    }
}