global class LeadDeDupLead implements Database.Batchable<sObject>{

global string query;
global map<String,Lead> LeadInfo;
global list<Lead> UpdLeads = new list<Lead>();
global set<Lead> Set_UpdLeads = new set<Lead>();

global map<id,CampaignMember> NewMembers = new map<id,CampaignMember>();
global List<CampaignMember> UpdMembers = new list<CampaignMember>();

global list<Lead> DelLeads = new list<Lead>();
global set<Lead> Set_DelLeads = new set<Lead>();

global list<id> MemLead = new list<id>();
global list<string> MemCampaign = new list<string>(); 
global set<id> newLeads;
 

global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query );
                   }
 

global void execute(Database.BatchableContext BC, List<sObject> scope){  
 
for(sObject s : scope){ Lead DupLd = (Lead)s;
system.debug('Lead = ' + DupLd);
               
 if(LeadInfo.containsKey(DupLd.email)&& !newLeads.contains(DupLd.id) && !LeadInfo.get(DupLd.email).isdeleted  )   
 { system.debug('DupLd = ' + DupLd.id);
   system.debug('LeadToDelete = ' + LeadInfo.get(DupLd.email).Lastname);


   DupLd.leadSource =LeadInfo.get(DupLd.email).leadSource;
   DupLd.Lead_Source_Category__c =LeadInfo.get(DupLd.email).Lead_Source_Category__c;   
   DupLd.Web_form_campaign1__c =LeadInfo.get(DupLd.email).Web_form_campaign1__c;        
  /* if(DupLd.Lead_score__c!=NULL){DupLd.Lead_score__c= DupLd.Lead_score__c + LeadInfo.get(DupLd.email).Lead_score__c;}      
   else {DupLd.Lead_score__c = LeadInfo.get(DupLd.email).Lead_score__c;} */   
        
   UpdLeads.add(DupLd);      
   system.debug('Lead to Update = ' + UpdLeads);
             
                                       
   if(!Set_DelLeads.contains(LeadInfo.get(DupLd.email))) 
    {DelLeads.add(LeadInfo.get(DupLd.email));   
     Set_DelLeads.add(LeadInfo.get(DupLd.email));   
    }              
                   
   system.debug('Lead to Delete Final = ' + DelLeads);

         
  if(LeadInfo.get(DupLd.email).Web_Form_Campaign1__c != NULL)
   {
    CampaignMember CM = new CampaignMember();
    CM.CampaignId = LeadInfo.get(DupLd.email).Web_Form_Campaign1__c;
    CM.LeadId = DupLd.id;
   // CM.CM_score__c = LeadInfo.get(DupLd.email).Lead_score__c;
    NewMembers.put(DupLd.id,CM);          
    MemLead.add(DupLd.id);          
    MemCampaign.add(LeadInfo.get(DupLd.email).Web_Form_Campaign1__c);        
    system.debug('UpdLeads = ' + UpdLeads);
   }  
 } //if duplicate lead                                                                                 
} // for
Delete (DelLeads); 
system.debug('Lead to Delete = ' + LeadInfo.values());

for(CampaignMember CMr:[select id,Times_attended__c, LeadId from CampaignMember where LeadId in :MemLead and  CampaignId in:MemCampaign])
{
  if(NewMembers.containsKey(CMr.LeadId))
    {            
     NewMembers.remove(CMr.LeadId);            
     if(CMr.Times_attended__c != NULL) CMr.Times_attended__c = CMr.Times_attended__c +1;
     else  CMr.Times_attended__c = 2;               
     UpdMembers.add(CMr);          
    }        
 }                           
insert(NewMembers.values());
update(UpdMembers);
              
Update(UpdLeads);
system.debug('Lead to Update = ' + UpdLeads);
          
}               
global void finish(Database.BatchableContext BC){
/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.setToAddresses(new String[] {'innag@mellanox.co.il'});
mail.setReplyTo('batch@acme.com');
mail.setSenderDisplayName('Batch Processing');
mail.setSubject('Batch Process Completed');
mail.setPlainTextBody('Query :'+query);
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

}
}