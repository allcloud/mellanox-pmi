public with sharing class ContactSearchController 
{
public string ProductSelected;
public integer newvalue;
        
       public map<ID, Release__c> map_ProdIdToProd ; 

        public String selectedTab{get;set;}
        
 
        // the soql without the order and limit
        private String soql {get;set;}

        // the collection of contacts to display
        public List<SFDC_Issue__c> contacts {get;set;}
 
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
   
  
  // init the controller and display some sample data when the page loads
  public ContactSearchController() {
        
    userSelected = '';     
    
    soql = 'select  profile_selected__c, Status__c, Product__r.name,Product__c, Release__c,Issue_Name__c,Issue_Description__c,Fix_in_Release__c,WorkAround__c,Issue_Category__c, type__c from SFDC_Issue__c where name != null'; // and Category__c =\'Release Notes Known issues\'';
    runQuery();
  }
 
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
 
        public pagereference setSelectedTab()
        {
                selectedTab = System.currentPageReference().getParameters().get('tab');
                system.debug('selectedTab : ' + selectedTab);
                
               return null; 
        }
 
  // runs the actual query
  public void runQuery() {
 
    try {
      contacts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
    }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String issuename = Apexpages.currentPage().getParameters().get('issuename');
   // String priority = Apexpages.currentPage().getParameters().get('priority');
    String product = Apexpages.currentPage().getParameters().get('product');
    String issuecategory = Apexpages.currentPage().getParameters().get('issuecategory');
    String release = Apexpages.currentPage().getParameters().get('release');
    String fixrelease = Apexpages.currentPage().getParameters().get('fixrelease');
    
    //String issuecategory= Apexpages.currentPage().getParameters().get('issuecategory');
   // selectedTab = Apexpages.currentPage().getParameters().get('tab');
    
    if(productselected !=product)
     {newvalue = 1;}
    
    ProductSelected = product ;
    if(newvalue == 1)
  {  Releases =Releases ;
    IssueCategories =IssueCategories ;}
    
    system.debug('ProductSelected' + ProductSelected);
    system.debug('Releases ' + Releases );
    
    
    
    
    
    
   String category=  userSelected;
    system.debug('category = ' + category); 
   
     
    soql = 'select profile_selected__c,Status__c, Product__r.name,Release__c, Product__c,Issue_Name__c,Issue_Description__c,Fix_in_Release__c,WorkAround__c,Issue_Category__c, type__c from SFDC_Issue__c where name != null';
    if (issuename != null && !issuename.equals('')  )
      soql += ' and Issue_Name__c LIKE \'%'+issuename+'%\'';
 /*  if (priority!= null && !priority.equals('') )
      soql += ' and SFDC_Issue_Priority__c LIKE \''+String.escapeSingleQuotes(priority)+'%\'';*/
    if (fixrelease != null && !fixrelease.equals('')&& !fixrelease.equals('All')  )
      soql += ' and Fix_in_Release__r.name = \''+String.escapeSingleQuotes(fixrelease)+'\'';  
   if (product != null && !product.equals('')  )
      soql += ' and Product__r.name = \''+String.escapeSingleQuotes(product)+'\'';  
   if (release != null && !release.equals('') && !release.equals('All')  )
      soql += ' and Release__c LIKE \'%'+release+'%\''; 
   if (issuecategory != null && !issuecategory.equals('') && !issuecategory.equals('All')  )
      soql += ' and Issue_Category__r.name = \''+String.escapeSingleQuotes(issuecategory)+'\'';        
      
  /*   if (status != null && !status.equals('')  )
      soql += ' and SFDC_Issue_Status__c LIKE \''+String.escapeSingleQuotes(status)+'%\''; */
     
    if( category!= null && !category.equals('') )
          soql += ' and Type__c = \''+String.escapeSingleQuotes(category)+'\''; 

    // run the query again
    runQuery();
 
    return null;
  }
 
  // use apex describe to build the picklist values
  public List<String> Statuses {
    get {
      if (Statuses == null) {
 
        Statuses = new List<String>();
        Schema.DescribeFieldResult field = SFDC_Issue__c.SFDC_Issue_Status__c.getDescribe();
 
        for (Schema.PicklistEntry f : field.getPicklistValues())
          Statuses.add(f.getLabel());
 
      }
      return Statuses;          
    }
    set;
  }
  
  public List<Release__c> Releases {
    get {
      //if (Releases == null) {
      if(ProductSelected != null)
      {
        Releases = [select name from Release__c where product__r.name =: ProductSelected.trim() ];// 'MLNX-OS'] ;
        return Releases ;
      }else
      {
       return null;
       }
     // }
     // return Releases ;          
    }
    set;
  }
  
  
  public List<Issue_Category__c> IssueCategories{
    get {
      //if (Releases == null) {
      if(ProductSelected != null)
      {
        IssueCategories = [select name from Issue_Category__c where product__r.name =: ProductSelected.trim() ];// 'MLNX-OS'] ;
        return IssueCategories ;
      }else
      {
       return null;
       }
     // }
     // return Releases ;          
    }
    set;
  }
  
   
  
public List<Issue_Product__c> Products {
    get {
      if (Products == null) {
 
        Products = [select id, name from Issue_Product__c] ;
              
 
      }
      return Products;          
    }
    set;
  }
    
  
  
  
 
  
  public string userSelected {get;set;}
  
  public List<SelectOption> categories
    {
        get
        {   
        
            List<SelectOption> options = new List<SelectOption>(); 
            //get tthe picklist values from the object field picklist
          //  Schema.DescribeFieldResult fieldResult = SFDC_Issue__c.Category__c.getDescribe();
          Schema.DescribeFieldResult fieldResult = SFDC_Issue__c.Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
            {
                options.add(new SelectOption(f.getLabel(), f.getLabel()));
            }
            
            return options;
        }set; 
    }
  

 
}