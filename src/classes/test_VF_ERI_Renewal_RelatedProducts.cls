@isTest(seeAllData=true)
public with sharing class test_VF_ERI_Renewal_RelatedProducts {
	
	static testMethod void test_Renewal_RelatedProducts() {
		
		Quote theQuote = [SELECT Id, OpportunityId, Opportunity.Old_Contract__c FROM Quote WHERE Opportunity.Old_Contract__c != null order by createdDate desc limit 1];
				
		Contract2__c theContract = new Contract2__c(Id = theQuote.Opportunity.Old_Contract__c);
		
		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(theContract);
		VF_ERI_Renewal_RelatedProducts handler = new VF_ERI_Renewal_RelatedProducts();
		handler.contractId = theContract.Id; 
		handler.theContract = theContract;
		handler.getAllParams();
	}
}