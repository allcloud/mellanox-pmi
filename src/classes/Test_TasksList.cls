@isTest
public with sharing class Test_TasksList {
	
	static testMethod void test_TasksList(){
		
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		
		Account acc = creator.createAccount();
	 	insert acc;
	 	
	 	Milestone1_project__c project = creator.CreateMPproject(acc.id);
		insert project;
		
		Task t = creator.CreateTask();
		insert t;
		
		 Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(project);
		 TasksList tList = new TasksList(controller);
		 
		
		 tList.getTasks();
		 tList.turnToEditMode();
		 t.Subject = 'test1234';
		 tList.saveTasks();
		 
		 
		
		
	}

}