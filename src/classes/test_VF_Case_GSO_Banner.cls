@isTest
public with sharing class test_VF_Case_GSO_Banner {
    
    static testMethod void test_theVf() {
        
        CLS_ObjectCreator creator = new CLS_ObjectCreator();
        Id currentUserId = system.UserInfo.getUserId();
        
        Case c = new Case();
        c.RecordTypeId = ENV.GetRecordTypeIdByName('Admin Case');
        c.Type = 'Feature Request';
        c.OwnerId ='005500000014N80';
        insert c;
        
        Test.StartTest();
        ApexPages.standardController thePage = new Apexpages.Standardcontroller(c);
        VF_Case_GSO_Banner controller = new VF_Case_GSO_Banner(thePage);
        Test.StopTest();
    }
}