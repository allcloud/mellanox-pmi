public with sharing class VF_AddAttachmentToRMA 
{
	public Id RMAId {get;set;} 
	public Attachment attachment1 {get;set;} 
	public Attachment attachment2 {get;set;} 
	public Attachment attachment3 {get;set;} 
	public String fName1 {get;set;}
	public String fName2 {get;set;}
	public String fName3 {get;set;}
	public Boolean EndOfPage {get;set;}
	public Integer UrlStatus   {get;set;}
	public My_Mellanox_Setting__c settings = My_Mellanox_Setting__c.getInstance();
	
	public VF_AddAttachmentToRMA()
	{
		system.debug('==>VF_AddAttachmentToRMA');
		RMAID = ApexPages.currentPage().getParameters().get('RMAId');
		attachment1 = new Attachment ();
		attachment2 = new Attachment ();
		attachment3 = new Attachment ();
	}
	
	public Attachment attachment 
    {
        get 
        {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
	
	public pagereference upload() 
    {  
    	EndOfPage = false;
    	system.debug('==>RMAID '+RMAID);
    	system.debug('==>fName1  '+fName1);
    	system.debug('==>attachment1 '+attachment1); 
    	system.debug('==>attachment1 Name : '+fName1); 
     	if(RMAID != null)
     	{  
	        if(attachment1 != null )
	        {
	        	attachment1.Name = fName1;
	            attachment1.OwnerId = UserInfo.getUserId();
	            attachment1.ParentId = RMAID; 
	            attachment1.IsPrivate = true;
	            attachment1.Description = attachment1.Description;
	            system.debug('==>attachment 1 27'+attachment1);
	            try 
	            {
	                insert attachment1;
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
	            } 
	            catch (DMLException e) 
	            {
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
	            } 
	        }
	        if(attachment2 != null )
	        {    
	        	attachment2.Name = fName2;
	            attachment2.OwnerId = UserInfo.getUserId();
	            attachment2.ParentId = RMAID; 
	            attachment2.IsPrivate = true;
	            attachment2.Description = attachment2.Description;
	            system.debug('==>attachment 2 43'+attachment2);
	            try 
	            {
	                insert attachment2;
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
	            } 
	            catch (DMLException e) 
	            {  
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
	            } 
	        }
	        if(attachment3 != null )
	        {
	            attachment3.Name = fName3;
	            attachment3.OwnerId = UserInfo.getUserId();
	            attachment3.ParentId = RMAID; 
	            attachment3.IsPrivate = true;
	            attachment3.Description = attachment3.Description;
	            system.debug('==>attachment3 59'+attachment3);
	            try 
	            {
	                insert attachment3;
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
	            } 
	            catch (DMLException e) 
	            {
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
	            } 
	        }
     	}
     	
     	ID guestUserID = system.UserInfo.getUserId();
    	Pagereference p;
    	String url;
    	
        
        User currentUser = [SELECT Id, UserType, ProfileId FROM User WHERE Id =: guestUserID];
        
        system.debug('currentUser.UserType : ' + currentUser.UserType);
        system.debug('currentUser.ProfileId : ' + currentUser.ProfileId);
        system.debug('settings.MyMellanox_Distributor_User__c : ' + settings.MyMellanox_Distributor_User__c);
        system.debug('settings.MyMellanox_Distributor_User__c : ' + settings.MyMellanox_Distributor_User__c);
        
        
        String MellanoxWebSite = Site.getCurrentSiteUrl();
    	
        
        if (MellanoxWebSite != null && (MellanoxWebSite.containsIgnoreCase('mellanox.com') || MellanoxWebSite.containsIgnoreCase('mellanox.secure')) ) {
              
              url = 'http://mellanox.com/content/pages.php?pg=customer_support&action=rma_success';
              UrlStatus = 1;
            }
            
        else if (currentUser.UserType != null && currentUser.UserType.EqualsIgnoreCase('PowerPartner') ) {
          
    		
    		if (currentUser.ProfileId == settings.MyMellanox_Distributor_User__c || currentUser.ProfileId == settings.MyMellanox_Sales_User__c) {
    			
    			url = '/support/supportHome';
    			UrlStatus = 4;
    		}
    			
    		else {	
    			
    			url = '/support/a0E/o';
    			UrlStatus = 2;
    		}
          
        }
                
            
        else {
        
            system.debug('==>guestUser ');
           // Pagereference p = new Pagereference(ENV.refrenceToRMA);
           url = '/a0E/o';
            UrlStatus = 0;
        }
        
         p = new Pagereference(url);
         p.setRedirect(true);
        
        
        
        system.debug('return');
        return p;
    }
 
}