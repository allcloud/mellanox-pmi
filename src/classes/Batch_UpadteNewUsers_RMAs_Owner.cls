global class Batch_UpadteNewUsers_RMAs_Owner implements Database.Batchable<User>{    
	
	global Iterable<User> start(Database.BatchableContext BC) {
    
    	List<User> newlyCreatedUsers_List = [SELECT Id, ContactId FROM User WHERE CreatedDate >: date.today().addDays(-1)];
    	
    	return newlyCreatedUsers_List;
	}
          										       
											      
	 global void execute(Database.BatchableContext BC, List<User> scope) {
 	
 	    List<User> newlyCreatedUsers_List = scope;	
 	  
 	  	set<Id> ContactIds_Set = new set<Id>();
		map<Id, Id> contactIds2UserIds_Map = new map<Id, Id>();
		
		for (User newUser : newlyCreatedUsers_List) {
			
			if (newUser.ContactId != null) {
				
				ContactIds_Set.add(newUser.ContactId);
				contactIds2UserIds_Map.put(newUser.ContactId, newUser.Id);
			}
		}
		
		if (!ContactIds_Set.isEmpty()) {
			
		   list<RMA__c> rma2Update_List = new list<RMA__c>();
			
			map<Id, list<RMA__c>> contacts2ListsOfRMA_Map = getMapOfContacts2RMA(ContactIds_Set);
			
			if (!contacts2ListsOfRMA_Map.values().isEmpty()) {
				
				for (Id contactId : contacts2ListsOfRMA_Map.keySet()) {
					
					for (RMA__c theRma : contacts2ListsOfRMA_Map.get(contactId)) {
						
						if (contactIds2UserIds_Map.containsKey(theRma.Contact__c)) {
							
							theRma.OwnerId = contactIds2UserIds_Map.get(theRma.Contact__c);
							rma2Update_List.add(theRma);
						}
					}
				}
				
				if (!rma2Update_List.isEmpty()) {
					
					update rma2Update_List;
				}
			}	
		}		       
     }
     
     // This methos will return a map of User's contact ids as keys and lists of RMAs as values
	private map<Id, list<RMA__c>> getMapOfContacts2RMA(set<Id> ContactIds_Set) {
		
		map<Id, list<RMA__c>> contacts2ListsOfRMA_Map = new map<Id, list<RMA__c>>();
			
		list<RMA__c> relatedRMAs_List = [SELECT Id, OwnerId, Contact__c FROM RMA__c WHERE Contact__c IN : ContactIds_Set];
		
		if (!ContactIds_Set.isEmpty()) {
			
			for (RMA__c currentRMA : relatedRMAs_List) {
				
				if (contacts2ListsOfRMA_Map.containsKey(currentRMA.Contact__c)) {
					
					contacts2ListsOfRMA_Map.get(currentRMA.Contact__c).add(currentRMA);
				}
				
				else {
					
					contacts2ListsOfRMA_Map.put(currentRMA.Contact__c, new list<RMA__c>{currentRMA});
				}
			}
		}
		
		return contacts2ListsOfRMA_Map;
	}

	global void finish(Database.BatchableContext BC) {
     	   
     }
}