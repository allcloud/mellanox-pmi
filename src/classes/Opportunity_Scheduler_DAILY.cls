global class Opportunity_Scheduler_DAILY implements Schedulable {

	global void execute(SchedulableContext SC) {
		List<Opportunity> optys_updates = new List<Opportunity>([select id, StageName, Amount, Record_type_ID__c,Closed_Lost_By__c,
																		Quarters_from_OrigBookDate_to_TODAY__c
																		from Opportunity
																		where Record_type_ID__c = '01250000000Dtdw'
																		and Quarters_from_OrigBookDate_to_TODAY__c != null 
																		and Quarters_from_OrigBookDate_to_TODAY__c > 2
																		and Amount < 50000
																		and (StageName='Pipeline' or StageName = 'Best Case') ]);
		
		//if (optys_updates != null && optys_updates.size() == 0)
		//	return;
		
		for (Opportunity o : optys_updates){
			o.StageName = 'Closed Lost';
			o.Closed_Lost_By__c = 'Christina Quinto' + '-' + String.valueOf(System.today() + '- deals less than 50k and Original Book Date vs Book Date is more than 2 Quarters away.');
		}
		
		if (optys_updates != null && optys_updates.size() == 0)
			Database.update(optys_updates, false);
		//Run logic to calculate Credit Type
		//KN 11-12-15
		List<VT_Map__c> lst_VTMap = new List<VT_Map__c>([Select VT__c,VT_Region_Text__c,Sales_Director_text__c,
															Sales_Director__c,Manager_txt__c,Country_Roll_Up__c 
														From VT_Map__c where Xactly_Default__c =true order by VT_Region_Text__c]);
		Map<String,VT_Map__c> map_SalesDirector_VTMap = new Map<String,VT_Map__c>();
		Map<String,String> map_VTRegion_country_rollup = new Map<String,String>();
		
		for (VT_Map__c v : lst_VTMap){
			map_SalesDirector_VTMap.put(v.Sales_Director_text__c,v);
			map_VTRegion_country_rollup.put(v.VT_Region_Text__c, v.Country_Roll_Up__c);
		}	
		List<Period> pers = new List<Period>([select StartDate,Enddate from Period where Type='Quarter' and EndDate >= :System.today() order by StartDate ]);												
		Date firstdate_currQ = pers[0].StartDate;
		Date firstdate_prev3Q = firstdate_currQ.addmonths(-9);
		
		List<Opportunity> optys_past_3Q = new List<Opportunity>([select id,Owner.Name,VT__c,Opportunity_VT_Region__c,Credit_Type__c,Region_group__c,Account.Name
																	from Opportunity
																	where Record_type_ID__c = '01250000000Dtdw'
																	and Required_Ship_Date__c >= :firstdate_prev3Q 
																	and Required_Ship_Date__c < :firstdate_currQ
																	and StageName = 'Closed Won' ]);
		
		List<Opportunity> optys_CurrQ = new List<Opportunity>([select id,Owner.Name,VT__c,Opportunity_VT_Region__c,Credit_Type__c,
																		Region_group__c,Account.Name
																	from Opportunity
																	where Record_type_ID__c = '01250000000Dtdw'
																	and Required_Ship_Date__c >= :firstdate_currQ 
																	and (StageName = 'Pipeline' OR StageName='Best Case' 
																			OR StageName='Commit' OR StageName = 'Closed Won') ]);
		//sum all into 1 list for processing
		optys_CurrQ.addall(optys_past_3Q);
		List<Opportunity> optys_updates_2 = new List<Opportunity>();
		String mgr;
		
		for (Opportunity o : optys_CurrQ) {
			if (o.Region_group__c == 'MFS' || o.Region_group__c == 'Americas') {
				if(o.VT__c == 'VAR' || o.VT__c=='MFS' || o.VT__c == 'USA' 
						|| o.VT__c == 'MFS/USA' || o.VT__c=='OTH' || String.isBlank(o.VT__c) ) {
					if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
						&& (map_SalesDirector_VTMap.get(o.Owner.Name).VT__c=='USA' 
								|| map_SalesDirector_VTMap.get(o.Owner.Name).VT__c=='MFS'
								|| map_SalesDirector_VTMap.get(o.Owner.Name).VT__c=='SVC') ) {
							if(o.Credit_Type__c != 'REVENUE') {
								o.Credit_Type__c = 'REVENUE';
								optys_updates_2.add(o);
							}		
					}
				}
				else { //not VAR, not MFS, not USA
					if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
						&& (map_SalesDirector_VTMap.get(o.Owner.Name).VT__c=='USA' || map_SalesDirector_VTMap.get(o.Owner.Name).VT__c=='MFS')) {
							if(o.Credit_Type__c != 'GLOBAL') {
								o.Credit_Type__c = 'GLOBAL';
								optys_updates_2.add(o);
							}		
					}
				}
			}
			else if (o.Region_group__c == 'EUR') {
				if(o.VT__c=='OTH' || String.isBlank(o.VT__c) ) {
					if(o.Credit_Type__c != 'REVENUE') {
						o.Credit_Type__c = 'REVENUE';
						optys_updates_2.add(o);
					}
				}
				else if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
					&& (map_SalesDirector_VTMap.get(o.Owner.Name).VT_Region_Text__c == o.Opportunity_VT_Region__c 
							|| map_SalesDirector_VTMap.get(o.Owner.Name).VT__c=='SVC') )	{
						if(o.Credit_Type__c != 'REVENUE') {
							o.Credit_Type__c = 'REVENUE';
							optys_updates_2.add(o);
						}
				}
				else if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
					&& map_SalesDirector_VTMap.get(o.Owner.Name).VT__c != o.VT__c 
					&& o.VT__c != 'OTH' && !String.isBlank(o.VT__c) )	{
						if(o.Credit_Type__c != 'GLOBAL') {
								o.Credit_Type__c = 'GLOBAL';
								optys_updates_2.add(o);
						}
				}
				else if(map_SalesDirector_VTMap.get(o.Owner.Name) != null && map_SalesDirector_VTMap.get(o.Owner.Name).Manager_txt__c != null) {
					mgr = map_SalesDirector_VTMap.get(o.Owner.Name).Manager_txt__c;
					if(map_SalesDirector_VTMap.get(mgr) != null 
							&& map_SalesDirector_VTMap.get(mgr).VT_Region_Text__c == o.Opportunity_VT_Region__c ) {
						if(o.Credit_Type__c != 'LOCAL - R') {
							o.Credit_Type__c = 'LOCAL - R';
							optys_updates_2.add(o);
						}			
					}
				}
				else if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
					&& map_SalesDirector_VTMap.get(o.Owner.Name).VT__c == o.VT__c )	{
						if(map_SalesDirector_VTMap.get(o.Owner.Name).VT_Region_Text__c != o.Opportunity_VT_Region__c) {
							if(map_SalesDirector_VTMap.get(o.Owner.Name).Manager_txt__c != null) {
								mgr = map_SalesDirector_VTMap.get(o.Owner.Name).Manager_txt__c;
								if(map_SalesDirector_VTMap.get(mgr) != null 
									&& map_SalesDirector_VTMap.get(mgr).VT_Region_Text__c != o.Opportunity_VT_Region__c ) {
										if(o.Credit_Type__c != 'LOCAL - RM') {
											o.Credit_Type__c = 'LOCAL - RM';
											optys_updates_2.add(o);
										}			
								}
							}
						}	
				}
				//
				else {
					if(o.Credit_Type__c != 'REVENUE') {
						o.Credit_Type__c = 'REVENUE';
						optys_updates_2.add(o);
					}
				}//end default case -Revenue
			}
			else if (o.Region_group__c == 'ASI') {
				if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
					&& (String.isBlank(map_SalesDirector_VTMap.get(o.Owner.Name).VT_Region_Text__c)
							|| map_SalesDirector_VTMap.get(o.Owner.Name).VT__c == 'SVC'
							|| map_SalesDirector_VTMap.get(o.Owner.Name).VT_Region_Text__c == o.Opportunity_VT_Region__c) )	{
						if(o.Credit_Type__c != 'REVENUE') {
							o.Credit_Type__c = 'REVENUE';
							optys_updates_2.add(o);
						}
				}
				else if (o.VT__c=='OTH' || String.isBlank(o.VT__c) ){
					if(o.Credit_Type__c != 'REVENUE') {
						o.Credit_Type__c = 'REVENUE';
						optys_updates_2.add(o);
					}
				}
				else if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
					&& map_SalesDirector_VTMap.get(o.Owner.Name).VT__c != o.VT__c )	{
						if(o.Credit_Type__c != 'GLOBAL') {
								o.Credit_Type__c = 'GLOBAL';
								optys_updates_2.add(o);
						}
				}
				else if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
					&& map_SalesDirector_VTMap.get(o.Owner.Name).VT__c == 'ASI' && o.VT__c == 'ASI')	{
						if(map_SalesDirector_VTMap.get(o.Owner.Name) != null 
							&& map_SalesDirector_VTMap.get(o.Owner.Name).Country_Roll_Up__c == 'CHN' ) {
								if(o.Opportunity_VT_Region__c == 'ASI-CHN4' || o.Opportunity_VT_Region__c == 'ASI-CHN5') {
									if(o.Credit_Type__c != 'LOCAL - R') {
										o.Credit_Type__c = 'LOCAL - R';
										optys_updates_2.add(o);
									}
								}
								else { //ASI-CHN1, ASI-CHN3
									if(o.Credit_Type__c != 'REVENUE') {
									o.Credit_Type__c = 'REVENUE';
									optys_updates_2.add(o);
									}
								} //end else { //ASI-CHN1, ASI-CHN3

						} else {
							if(o.Credit_Type__c != 'LOCAL - RMC') {
								o.Credit_Type__c = 'LOCAL - RMC';
								optys_updates_2.add(o);
							}
						}	
				}
			}	
			else if (o.Region_group__c == 'BIZ') {
				if (o.Account.name != null 
						&& (o.Account.name.containsIgnoreCase('GOOGLE') || o.Account.name.containsIgnoreCase('AMAZON') 
							|| o.Account.name.containsIgnoreCase('MICROSOFT') || o.Account.name.containsIgnoreCase('FACEBOOK')
							|| o.Account.name.containsIgnoreCase('APPLE') ) ) {
						if(o.Credit_Type__c != 'REVENUE') {
							o.Credit_Type__c = 'REVENUE';
							optys_updates_2.add(o);
						}	
				}
				else if (map_SalesDirector_VTMap.get(o.Owner.Name) != null && map_SalesDirector_VTMap.get(o.Owner.Name).VT__c == 'BIZ'
							&& o.VT__c != 'VAR' && o.VT__c != 'BIZ') {
						if(o.Credit_Type__c != 'GLOBAL') {
							o.Credit_Type__c = 'GLOBAL';
							optys_updates_2.add(o);
						}								
				}
			}//end BIZ
		} //end for (Opportunity o : optys_CurrQ) {
		if(optys_updates_2.size() >0 )
			//update optys_updates_2;
			Database.update(optys_updates_2,false);																																							 
		//
	}
}