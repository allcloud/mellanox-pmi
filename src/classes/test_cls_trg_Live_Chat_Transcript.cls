/**
 * Class containing tests for cls_trg_Live_Chat_Transcript
 */
@isTest 
public with sharing class test_cls_trg_Live_Chat_Transcript {
	 static testMethod void test_cls_trg_Live_Chat_TranscriptMethod() {
        
        User u = [Select u.Id, u.ContactId from User u Where u.IsActive = true and u.ContactId != null LIMIT 1];
        
        Case c = new Case(priority = 'Low',ContactId = u.ContactId, state__c = 'Open');
        
        insert c;
        
        LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;
        
        LiveChatTranscript transcript = new LiveChatTranscript();
        transcript.ContactId = u.ContactId;
        transcript.CaseId = c.Id;
        transcript.Body = '(-0800) ( 4s ) Tal: Thank you for contacting Mellanox. My name is Tal. I will be happy to assist you today.';
        transcript.LiveChatVisitorId = visitor.Id;

        insert transcript;
	 }
}