public with sharing class myController 
{
	public ID id;
    
    public myController(ApexPages.StandardController controller) 
    {
		//id = Apexpages.currentPage().getParameters().get('Id');
		//getDeliverAsPDF();
    }

	public PageReference getDeliverAsPDF() 
	//public void getDeliverAsPDF()
	{
	
		// Reference the page, pass in a parameter to force PDF
	     id = Apexpages.currentPage().getParameters().get('Id');
	     system.debug('id is : ' + id);
	     
	     PageReference pdf =  Page.vf_RMA_letter;
	     
	//PageReference pdf = new PageReference('Page.vf_RMA_letter');
	
	     pdf.getParameters().put('id',id);
		 system.debug('pdf.getParameters().get id:  ' + pdf.getParameters().get('id'));
		 
	     //pdf.setRedirect(true);
	
	     // Grab it!
		 system.debug('pdf.getContent() : ' + pdf.getContent().size());
	     //String b = pdf.getContent().toString();
		  blob b = pdf.getContent();
	     // Create an email
	
	     Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
	
	      email.setSubject('RMA Letter!');
	
	     String [] toAddresses = new String[] {'jabuhana@gmail.com','innag@mellanox.co.il'};
	
	     email.setToAddresses(toAddresses);
	
	     email.setPlainTextBody('Here is the body of the email');
	
	     // Create an email attachment
	     // email.setHtmlBody(b);
	      
	     Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
	
	     efa.setFileName('RMA_Letter.pdf'); // neat - set name of PDF
	
	     efa.setBody(b); //attach the PDF
	
	     email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
	
	     // send it, ignoring any errors (bad!)
	
	     Messaging.SendEmailResult [] r =
	
	             Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
	
       return new Pagereference('/'+id);

	}
}