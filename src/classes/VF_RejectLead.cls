public with sharing class VF_RejectLead {
	
	 public Lead lead{get;set;}
 
    public VF_RejectLead(ApexPages.StandardController stdController){
        this.lead = (Lead)stdCOntroller.getRecord();
    }
     
    public void saveLead() {
        try{
            update Lead;
        }catch(exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error Occured while updating the lead '+e.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }

}