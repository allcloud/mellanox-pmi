@isTest
public with sharing class test_VF_AdminQ_Cases {
	
	static testMethod void test_VF_AdminQ_CasesMethods() {
		
		Contact  currentContact = new Contact(Accountid = ENV.UnknownAccount, firstName = 'First', LastName = 'Last', Phone = '050-5555555', Email ='Test@someService.com', Company_Name_from_Web__c = 'test Company');
	    insert currentContact;
	    
	    Id currentUserId = system.UserInfo.getUserId();
	    
	    Case currentCase = new case(accountid = ENV.UnknownAccount, contactid = currentContact.id);
	    currentCase.Assignee__c = currentUserId;
	    currentCase.Application_Reason__c = 'Sales/POC related inquiry';
	    insert currentCase; 
	    
	    ApexPages.currentPage().getParameters().put('contactId',currentContact.Id); 
	    ApexPages.currentPage().getParameters().put('caseId',currentCase.Id); 
	  
        VF_AdminQ_Cases vf = new VF_AdminQ_Cases();
        vf.currentCase.Application_Reason__c = 'Sales/POC related inquiry';
        vf.chooseFromMainPickList();
        vf.getRegionList();
        vf.selectedRegion = 'US';
        vf.selectFromRegionList();
        vf.selectedRegion = 'EMEA';
        vf.selectFromRegionList();
        vf.currentContact.firstName = 'First';
        vf.currentContact.LastName = 'Last';
        vf.currentContact.Phone = '050-5555555';
        vf.currentContact.Email = 'Test@someService.com';
        vf.currentContact.Company_Name_from_Web__c = 'test Company';
        vf.updateContactInfo();
        
	}
	
	static testMethod void test_VF_AdminQ_CasesMethods1() {
		
		Contact  UnContact = new Contact(Accountid = ENV.UnknownAccount, firstName = 'First', LastName = 'Last');
	    insert UnContact;
	    
	    Id currentUserId = system.UserInfo.getUserId();
	    
	    Case currentCase = new case(accountid = ENV.UnknownAccount, contactid = UnContact.id);
	    currentCase.Assignee__c = currentUserId;
	    currentCase.Application_Reason__c = 'RMA for products hardware malfunction';
	    insert currentCase; 
	    
	    ApexPages.currentPage().getParameters().put('contactId',UnContact.Id); 
	    ApexPages.currentPage().getParameters().put('caseId',currentCase.Id); 
	  
        VF_AdminQ_Cases vf = new VF_AdminQ_Cases();
        vf.currentCase.Application_Reason__c = 'RMA for products hardware malfunction'; 
        vf.chooseFromMainPickList();       
	}
	
	static testMethod void test_VF_AdminQ_CasesMethods2() {
		
		Contact  UnContact = new Contact(Accountid = ENV.UnknownAccount, firstName = 'First', LastName = 'Last');
	    insert UnContact;
	    
	    Id currentUserId = system.UserInfo.getUserId();
	    
	    Case currentCase = new case(accountid = ENV.UnknownAccount, contactid = UnContact.id);
	    currentCase.Assignee__c = currentUserId;
	    currentCase.Application_Reason__c = 'Missing product Documentation';
	    insert currentCase; 
	    
	    ApexPages.currentPage().getParameters().put('contactId',UnContact.Id); 
	    ApexPages.currentPage().getParameters().put('caseId',currentCase.Id); 
	  
        VF_AdminQ_Cases vf = new VF_AdminQ_Cases();
        vf.currentCase.Application_Reason__c = 'Missing product Documentation';
        vf.chooseFromMainPickList();
        
	}
	
	static testMethod void test_VF_AdminQ_CasesMethods3() {
		
		Contact  UnContact = new Contact(Accountid = ENV.UnknownAccount, firstName = 'First', LastName = 'Last');
	    insert UnContact;
	    
	    Id currentUserId = system.UserInfo.getUserId();
	    
	    Case currentCase = new case(accountid = ENV.UnknownAccount, contactid = UnContact.id);
	    currentCase.Assignee__c = currentUserId;
	    currentCase.Application_Reason__c = 'None';
	    insert currentCase; 
	    
	    ApexPages.currentPage().getParameters().put('contactId',UnContact.Id); 
	    ApexPages.currentPage().getParameters().put('caseId',currentCase.Id); 
	  
        VF_AdminQ_Cases vf = new VF_AdminQ_Cases();
        vf.currentCase.Application_Reason__c = 'None';
        vf.chooseFromMainPickList();
        
	}
}