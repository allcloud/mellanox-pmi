public with sharing class DAL_RMA {
	
	public static list<RMA__c> getRMAsAndAttachments(set<Id> rmasIds_Set)
	{
		return [Select r.Id, (Select Id From Attachments) From RMA__c r Where r.Id IN : rmasIds_Set];
	}

}