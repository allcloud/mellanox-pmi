public class VF_Case_INSA_Project_Banner {
    
    public Case theCase {get;set;}
    public boolean is_INSA {get;set;}

    public VF_Case_INSA_Project_Banner(ApexPages.standardController controller) 
    { 
        is_INSA = false;
        Id CaseId = controller.getId();
        
        theCase = [SELECT id, RMA_Support_Project__c FROM Case WHERE Id =: CaseId];
        
        if ( theCase.RMA_Support_Project__c != null)  { 
            
            is_INSA = true;
        }
    }
}