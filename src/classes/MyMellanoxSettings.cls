public class MyMellanoxSettings {

    private static My_Mellanox_Setting__c settings {
        get {
            return My_Mellanox_Setting__c.getInstance();
        }
    }

    public static ID DesignInUserProfileId {
        get {
            return settings != null ? settings.Design_In_Profile_ID__c : '00e50000001FhsD';
        }
    }
    
    public static ID EzchipDesignInUserProfileId {
        get {
            return settings != null ? settings.Ezchip_Design_In_Profile_ID__c : '00e50000001Jn5b';
        }
    }
    
     public static ID MyMellanoxLicenseUser {
        get {
            return settings != null ? settings.MyMellanox_License_User__c : '00eR0000000MT1v';
        }
    }

    public static ID SystemSupportUserProfileId {
        get {
            return settings != null ? settings.System_Support_Profile_ID__c : '00e50000001Fhyp';
        }
    }
    
    public static ID internalSystemSupportUserProfileId {
        get {
            return settings != null ? settings.Internal_System_Support_Profile__c : '00e50000000pQWp';
        }
    }

    public static ID DesignInUsersGroupId {
        get {
            return settings != null ? settings.Design_In_Users_Group_ID__c : '00GR00000015iGm';
        }
    }
    
    public static ID EzchipDesignInUsersGroupId {
        get {
            return settings != null ? settings.Ezchip_Design_In_Users_Group_ID__c : '00G50000002laiy';
        }
    }
    
   public static ID MyMellanoxCiscoDesignInId{
        get {
            return settings != null ? settings.MyMellanox_Cisco_DesignIn__c : '00G50000002laj3';
        }
    }
    
    public static ID SystemSupportUsersGroupId {
        get {
            return settings != null ? settings.System_Support_Users_Group_ID__c : '00GR00000015iGr';
        }
    }
    
    public static ID SystemOEMId {   
        get {
            return settings != null ? settings.System_OEM_Profile_ID__c : '00e50000001FiYj';
        }
    }
    
      public static ID MyMellanoxFPGAAdapterId {   
        get {
            return settings != null ? settings.FPGA_Adapter__c : '00G50000002V1Uk';
        }
    }
    
      public static ID MyMellanoxHPDesignInId {   
        get {
            return settings != null ? settings.MyMellanox_HP_DesignIn__c : '00GW0000000d4jm';
        }
    }
    
     public static ID MyMellanoxBullDesignInId {   
        get {
            return settings != null ? settings.MyMellanox_Bull_DesignIn__c : '';
        }
    }
    
     public static ID MyMellanoxEMCDesignInId {   
        get {
            return settings != null ? settings.MyMellanox_EMC_DesignIn__c : '00GW0000000d4jX';
        }
    }
    
     public static ID MyMellanoxIBMDesignInId {   
        get {
            return settings != null ? settings.MyMellanox_IBM_DesignIn__c : '00GW0000000d4jc';
        }
    }
    
    public static ID MyMellanoxDELLDesignInId {   
        get {
            return settings != null ? settings.MyMellanox_Dell_DesignIn__c : '00G5000000251n1';
        }
    }
    
     public static ID MyMellanoxIntelDesignInId {   
        get {
            return settings != null ? settings.MyMellanox_Oracle_DesignIn__c : '00GW0000000d4jh';
        }
    }
    
    public static ID MyMellanoxLENOVODesignInId {   
        get { 
            return settings != null ? settings.MyMellanox_LENOVO_DesignIn__c : '00G500000025XO9';
        }
    }
    
    
    public static ID MyMellanoxSGIDesignInId {   
        get {
            return settings != null ? settings.MyMellanox_SGI_DesignIn__c : '00G50000002UojV';
        }
    }
    
    
    public static ID MyMellanoxOracleDesignInId {   
        get {
            return settings != null ? settings.System_OEM_Profile_ID__c : '00GW0000000d4jr';
        }
    }
}