global with sharing class mlnxMobileHomeCtrl {
	global mlnxMobileHomeCtrl() {
		
	}

	@RemoteAction
	global static String getRecordFromBarcode(String bcStr) {
    	String result;
    	//List<String> eles=bcStr.split(':');
    
		//String code=eles[1].trim();
    	List<Asset2__c> assets=[select id, Name, Serial_Number__c from Asset2__c where Serial_Number__c=:bcStr];
		if (0!=assets.size()) {
			result=assets[0].id;
		} else {
			result='Error: no record matching ' + bcStr + ' found';
		}
    
		return result;
	}
}