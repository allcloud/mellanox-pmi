public with sharing class VF_MDF_Request {
    
    public SFDC_MDF__c theMDF {get;set;}
    public Attachment attachment1 {get;set;}   
    private User currentUser;
    public boolean displayEditMode {get;set;}
    public boolean displayEditButton {get;set;}
    public boolean displayEditSection {get;set;}
    public boolean attachmentExists {get;set;}
    private Id mdfId;

    //Picklist of tnteger values to hold file count  
    public List<SelectOption> filesCountList {get; set;}  
    //Selected count  
    public String FileCount {get; set;} 
       
    public List<Attachment> allFileList {get; set;} 
    
    // Constructor where the rendering of the page is determines 
    public VF_MDF_Request(ApexPages.StandardController controller) {
        
        mdfId = controller.getId();
        displayEditMode = true;
        Id currentUserId = UserInfo.getUserId();
            
        currentUser = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id =: currentUserId];
        
        if (mdfId != null) {    
            
            displayEditMode = false;
             
            theMDF = [SELECT s.Total_Activity_Cost__c, s.Target_Audience__c, s.TAM__c,  Units_Sold__c,
            s.Status__c, s.Select_Budget_Warning__c, s.Request_ID__c,
            s.Purpose_of_Activity__c, s.Other_Vendors_Participating__c, s.Other_Activity_Type__c,
            s.Name, s.Mellanox_attendees__c, s.Mellanox_Attendee_Requested__c, s.Marketing_Dollars_Requested__c,
            s.Level_of_Sponsorship__c, s.Fund_Request_Type__c, (SELECT Id FROM Attachments), 
            s.Claim_Deadline_Date__c, s.CAM__c, s.Budget__c, s.Approved__c, Leads__c, Attendees__c,
            s.Approved_Date__c, s.Amount__c, s.Age_Days__c, s.Additional_Notes__c, s.Activity_Vertical_Market_Focus__c,
             s.Activity_Support__c, s.Activity_Start_Date__c, s.Activity_Product_Focus__c, 
            s.Activity_End_Date__c, s.Activity_Description__c, s.Activity_Country__c, s.Activity_City__c,
            s.Account__c From SFDC_MDF__c s WHERE Id =: mdfId];
            
            if (theMDF.Status__c != 'Approved' && theMDF.Status__c != 'Rejected') {
                
                displayEditButton = true;
            }
            
            attachmentExists = (!theMDF.Attachments.isEmpty()) ? true : false;
            
            system.debug('theMDF : ' + theMDF);
        }
        
        else {
            displayEditSection = false;
            filesCountList = new List<SelectOption>() ;  
            FileCount = '' ;  
            allFileList = new List<Attachment>() ;
             
              
            //Adding values count list - you can change this according to your need  
            for(Integer i = 1 ; i < 11 ; i++)  
                filesCountList.add(new SelectOption(''+i , ''+i)) ;  
            
            theMDF = new SFDC_MDF__c();
            attachment1 = new Attachment();
            
            theMDF.Account__c = (currentUser.ContactId != null && currentUser.Contact.AccountId != null) ? currentUser.Contact.AccountId : '001R000000t4EeP';
        }
    }
    
    // This method initiates upon loading/refreshing the 'VF_MDF_Request' page. it will look for existing 'Attachment_Holder__c' if exists and if it has attachments
    // they will be deleted, if it doesn't exist, it will create new 'Attachment_Holder__c' with the current user's Id as it's Name 
    public void createAttachmentHolder() {
         
        list<Attachment_Holder__c> holder_List = [SELECT ID, (SELECT Id FROM Attachments) FROM Attachment_Holder__c WHERE Name =: currentUser.Id];
        
        Attachment_Holder__c holder;
        
        if (holder_List.isEmpty()) {
            
            holder = new Attachment_Holder__c();
            holder.Name = String.valueOf(currentUser.Id);
            insert holder;
        }
        
        else {
            
            holder = holder_List[0];
            if (!holder.Attachments.isEmpty()) {
                
                try {   
                    delete holder.Attachments;
                    holder.Attachment_Uploaded__c = false;
                    update holder;
                }
                
                catch(Exception e) {
                    
                    system.debug('Error : ' + e.getMessage());
                }
            }
        }
    }
    
    // This method will save the MDF Record and will set it's status 'Submitted'
    public pageReference submitForApproval() {
        
        PageReference nextPage = saveTheMDF(true, true);
        return nextPage;
    }
    
    // This method will save the MDF Record and will set it's status 'Draft'
     public pageReference saveAsDraft() {
        
        PageReference nextPage = saveTheMDF(false, false);
        return nextPage;
    }
    
    // This method will save the new MDF and will assign it all the uploaded ataachments
    public pageReference saveTheMDF(boolean submitForApproval, boolean attIsMandatory) {
        
        
        list<Attachment_Holder__c> holder_List = [SELECT ID, (SELECT Id FROM Attachments) FROM Attachment_Holder__c WHERE Name =: currentUser.Id];
        
        if (theMDF != null && theMDF.Id != null) {
        	
        	list<Attachment> relatedAttachments_list = [SELECT Id FROM Attachment WHERE ParentId =: theMDF.Id];
        	
        	if (!relatedAttachments_list.isEmpty()) {
        		
        		attIsMandatory = false;
        	}
        }
        
        
        if ( (!holder_List.isEmpty() && !holder_List[0].Attachments.isEmpty()) 
        	   || !attIsMandatory) {
            
            if (theMDF.Fund_Request_Type__c != null 
                && (theMdf.Units_Sold__c != null || theMdf.Leads__c != null || theMdf.Attendees__c != null)
                && theMDF.Name != null && theMDF.Activity_Description__c != null
                && theMDF.Purpose_of_Activity__c != null && theMDF.Activity_Start_Date__c != null
                && theMDF.Activity_End_Date__c != null && theMDF.Activity_Product_Focus__c != null
                && theMDF.Activity_Vertical_Market_Focus__c != null 
                && theMDF.Marketing_Dollars_Requested__c != null && theMDF.Total_Activity_Cost__c != null
                && theMDF.Target_Audience__c != null) {
                
                if (theMDF.Account__c == null) {
                
                 theMDF.Account__c ='001R000000t4EeP';
                }
                                    
                try {   
                    
                    theMDF.Status__c = (submitForApproval) ? 'Submitted' : 'Draft';
                    theMDF.Requestor_Name__c = UserInfo.getUserId();
                    upsert theMDF;
                }
                
                catch(Exception e) {
                    
                    String message = e.getMessage();
                    String specificMessage = message.substringAfter('EXCEPTION,');
                    specificMessage = specificMessage.substring(0, specificMessage.length() -2);
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, message));
                    return null;  
                }
                
                Id AttHolderId = holder_List[0].Id;
                transferAttachmentsToMDFAsync(theMDF.Id, AttHolderId);
                theMDF = new SFDC_MDF__c();
                
                system.debug('Inside 129');
                String openerUrl = '/apex/VF_MDF_Request?submitted=true';
                PageReference nextPage = new PageReference(openerUrl);        
                
                nextPage.setRedirect(true);
                return nextPage;  
                
            }
            
            else {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'All mandatory fields need to be filled up'));
            }
        }
        
        else {
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'At least 1 Attachments needs to be uploaded')); 
        }
        
        return null;
     }
    
    // This method will save all uploaded Attachments under the Attachment Holder
    public Pagereference SaveAttachments() {
        
        Attachment_Holder__c holder;
        try {
            holder = [SELECT ID FROM Attachment_Holder__c WHERE Name =: currentUser.Id];
        }
        
        catch(Exception e) {
            
            holder = new Attachment_Holder__c();
            holder.Name = currentUser.Id;
            insert holder;
        }
        
        String holderId = holder.Id;  
        if(holderId == null || holderId == '')  
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'No record is associated. Please pass record Id in parameter.'));  
        if(FileCount == null || FileCount == '')  
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select how many files you want to upload.'));  
  
        List<Attachment> listToInsert = new List<Attachment>() ;  
          
        //Attachment a = new Attachment(parentId = accid, name=myfile.name, body = myfile.body);  
        for(Attachment a: allFileList)  
        {  
            if(a.name != '' && a.name != '' && a.body != null)  
                listToInsert.add(new Attachment(parentId = holderId, name = a.name, body = a.body)) ;  
        }  
          
        //Inserting attachments  
        if(listToInsert.size() > 0)  
        {  
            insert listToInsert ;
            holder.Attachment_Uploaded__c = true;
            update holder;
            String openerUrl = '/apex/VF_Contract_Renewal_PO_success_page';
            PageReference nextPage = new PageReference(openerUrl);        
            
            nextPage.setRedirect(true);
            return nextPage;   
        }  
        else  
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select at-least one file'));  
              
        return null;  
    }  
      
    // This method will add/substract places for uploading attachments on the Attachments upload page  
    public void ChangeCount()  
    {  
        if (allFileList != null) {  
            allFileList.clear() ;
        }  
        //Adding multiple attachments instance  
	     if (FileCount != null) {
	        for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)  
	            allFileList.add(new Attachment()) ;  
	     }
         
    }  
    
    // This method will Asynchronically tranfer all uploaded attachments to the new 'Fund Request' and will delete the Attachment holder record
    @future
    private static void transferAttachmentsToMDFAsync(Id theMDFId, Id AttHolderId) {
        
        list<Attachment> attachments_List = [SELECT Id, Body, Name FROM Attachment WHERE ParentId =: AttHolderId];
        
        list<Attachment> attachments2Create_List = new list<Attachment>();
        
        system.debug('attachments_List : ' + attachments_List);
        
        if (!attachments_List.isEmpty()) {
            
            for (Attachment att : attachments_List) {
                
                Attachment clonedAtt = att.clone();
                clonedAtt.ParentId = theMDFId;
                attachments2Create_List.add(clonedAtt);
            }
            
            insert attachments2Create_List;
            Attachment_Holder__c attHolder2Delete = new Attachment_Holder__c(Id = AttHolderId); 
            delete attHolder2Delete;
        }
    }
    
    // This method will redirect the user to the Editable section in the page where he can edit the MDF record
    public void turnExitingRecordEditable() {
        
         
        theMDF.Id = mdfId;
        system.debug('theMDF : ' + theMDF);
        displayEditSection = true;   
    }
    
    // This method will redirect the user back to the Display section in the page after saving his changes Or display an error message 
    // if a validation rule has blocked the upldate
    public void turnExitingRecordNonEditable() {
        
        system.debug('theMDF : ' + theMDF);
        
        if ( theMDF.Fund_Request_Type__c != null 
            && (theMdf.Units_Sold__c != null || theMdf.Leads__c != null || theMdf.Attendees__c != null)
            && theMDF.Name != null && theMDF.Activity_Description__c != null
            && theMDF.Purpose_of_Activity__c != null && theMDF.Activity_Start_Date__c != null
            && theMDF.Activity_End_Date__c != null && theMDF.Activity_Product_Focus__c != null
            && theMDF.Activity_Vertical_Market_Focus__c != null 
            && theMDF.Marketing_Dollars_Requested__c != null && theMDF.Total_Activity_Cost__c != null
            && theMDF.Target_Audience__c != null && (theMDF.Fund_Request_Type__c != 'Other' || theMDF.Other_Activity_Type__c != null)) {
                
                
             try {
                
                if (theMDF.Fund_Request_Type__c != 'Other') {
                    
                    theMDF.Other_Activity_Type__c = null;
                }
                    update theMDF;
                    
             }
             
             catch(Exception e) {
                
                String message = e.getMessage();
                String specificMessage = message.substringAfter('EXCEPTION,');
                specificMessage = specificMessage.substring(0, specificMessage.length() -2);
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, specificMessage));
                return;
             }
            displayEditSection = false;
        }
        
        else {
            
            String specificMessage = 'All Mandatory fields should be filled up';
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, specificMessage));
        }
    }
    
    // This method will redirect the user back to the Display section in the page without any changes
    public void cancelRequest() {
        
        theMDF = [SELECT s.Total_Activity_Cost__c, s.Target_Audience__c, s.TAM__c, 
            s.Status__c, s.Select_Budget_Warning__c, s.Request_ID__c, 
            s.Purpose_of_Activity__c,  s.Other_Vendors_Participating__c, s.Other_Activity_Type__c,
            s.Name, s.Mellanox_attendees__c, s.Mellanox_Attendee_Requested__c, s.Marketing_Dollars_Requested__c,
            s.Level_of_Sponsorship__c, s.Fund_Request_Type__c, 
            s.Claim_Deadline_Date__c, s.CAM__c, s.Budget__c, s.Approved__c, 
            s.Approved_Date__c, s.Amount__c, s.Age_Days__c, s.Additional_Notes__c, s.Activity_Vertical_Market_Focus__c,
             s.Activity_Support__c, s.Activity_Start_Date__c, s.Activity_Product_Focus__c, 
            s.Activity_End_Date__c, s.Activity_Description__c, s.Activity_Country__c, s.Activity_City__c,
             s.Account__c From SFDC_MDF__c s WHERE Id =: mdfId];
             
        displayEditSection = false;
    }
}