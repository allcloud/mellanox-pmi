global class LeadDeDupContact implements Database.Batchable<sObject>{

global string query;
global map<String,Lead> LeadInfo;
global list<Contact> UpdContacts = new list<Contact>();
global List<CampaignMember> UpdMembers = new list<CampaignMember>();

global list<Lead> DelLeads = new list<Lead>();
global set<Lead> Set_DelLeads = new set<Lead>();

global map<id,CampaignMember> NewMembers = new map<id,CampaignMember>();
global list<id> MemContact = new list<id>();
global list<string> MemCampaign = new list<string>(); 

global set<id> newLeads;
 

global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query );
 }
                   
global void execute(Database.BatchableContext BC, List<sObject> scope){  
 
for(sObject s : scope){ Contact DupCnt = (Contact)s;
system.debug('Lead = ' + DupCnt);
 if(LeadInfo.containsKey(DupCnt.email) && !LeadInfo.get(DupCnt.email).isdeleted  )   
  {
   DupCnt.leadSource =LeadInfo.get(DupCnt.email).leadSource;  
   DupCnt.Lead_Source_Category__c =LeadInfo.get(DupCnt.email).Lead_Source_Category__c;                     
   UpdContacts.add(DupCnt);  
   
     
  if(!Set_DelLeads.contains(LeadInfo.get(DupCnt.email))) 
    {DelLeads.add(LeadInfo.get(DupCnt.email));   
     Set_DelLeads.add(LeadInfo.get(DupCnt.email));   
    }  
                    
  if(LeadInfo.get(DupCnt.email).Web_Form_Campaign1__c != NULL)
  {
   CampaignMember CM = new CampaignMember();
   CM.CampaignId = LeadInfo.get(DupCnt.email).Web_Form_Campaign1__c;
   CM.ContactId = DupCnt.id;
   NewMembers.put(DupCnt.id,CM);          
   MemContact.add(DupCnt.id);          
   MemCampaign.add(LeadInfo.get(DupCnt.email).Web_Form_Campaign1__c);     
   system.debug('UpdContacts = ' + UpdContacts);
   }    
  }
 }

Delete (DelLeads); 

for(CampaignMember CMr:[select id,Times_attended__c, ContactId from CampaignMember where ContactId in :MemContact and  CampaignId in:MemCampaign])
{
  if(NewMembers.containsKey(CMr.ContactId))
    {            
     NewMembers.remove(CMr.ContactId);            
     if(CMr.Times_attended__c != NULL) CMr.Times_attended__c = CMr.Times_attended__c +1;
     else  CMr.Times_attended__c = 2;               
     UpdMembers.add(CMr);          
    }        
 }                           
insert(NewMembers.values());
update(UpdMembers);


system.debug('Lead to Delete = ' + LeadInfo.values());


Update(UpdContacts);
system.debug('Lead to Update = ' + UpdContacts);

      
}               
global void finish(Database.BatchableContext BC){
/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.setToAddresses(new String[] {'innag@mellanox.co.il'});
mail.setReplyTo('batch@acme.com');
mail.setSenderDisplayName('Batch Processing');
mail.setSubject('Batch Process Completed');
mail.setPlainTextBody('Query :'+query);
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

}
}