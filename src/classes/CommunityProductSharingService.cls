public class CommunityProductSharingService {

    public void processAccountInitialSetupForUsers(Map<ID, User> users, map <Id, User> oldUsers_Map) {

        List<ID> accountIds = new List<ID>();
        list<User> EZChipUsers_list = new List<User>();
        
        for ( User u : 
                [select Contact.AccountId , UserRoleId, ProfileId
                 from User 
                 where Id in :users.keySet()
                    and ProfileId in (:MyMellanoxSettings.DesignInUserProfileId, :MyMellanoxSettings.SystemSupportUserProfileId, :MyMellanoxSettings.SystemOEMId, :MyMellanoxSettings.EzchipDesignInUserProfileId)] ) {
            
            if (trigger.isInsert || (trigger.isUpdate && u.UserRoleId != oldUsers_Map.get(u.Id).UserRoleId) )           
                        
            accountIds.add(u.Contact.AccountId);
            
            system.debug('user ProfileId :' + u.ProfileId );
            
            if (u.ProfileId == MyMellanoxSettings.EzchipDesignInUserProfileId) {
            
                EZChipUsers_list.add(u);
            }
        }
        System.debug('accountIds: ' + accountIds);

        if ( !accountIds.isEmpty() ) {

            // Query to select Accounts whose first user is being created
            List<AggregateResult> results = 
                [select Contact.AccountId accountId, count(Id) cnt
                from User
                where Contact.AccountId in :accountIds
                and UserType =: 'PowerPartner' 
                group by Contact.AccountId
               ];        //  having count(Id) = 1
            System.debug('results: ' + results);

            if ( !results.isEmpty() ) {
                     
                    
                accountIds.clear();
                for ( AggregateResult res : results ) {
                    accountIds.add((ID) res.get('accountId'));
                }
                System.debug('accountIds: ' + accountIds);

                processAccountInitialSetup(accountIds, EZChipUsers_list);
            } //END: If ( Results )
        } //END: If (AccountIDs)
    }
    
   
    public void processAccountInitialSetup(List<ID> accountIds, list<User> EZChipUsers_list) {

        List<ID> allRoleIds = new List<ID>();
        Map<ID, ID> acctIdToManagerRoleIdMap = new Map<ID, ID>();
        Map<ID, ID> acctIdToUserRoleIdMap = new Map<ID, ID>();
        map<Id, Id> acctIds2EzChiPRoles_Map = new Map<ID, ID>();

        for ( UserRole role : 
                     [SELECT PortalAccountId, PortalRole
                      FROM UserRole 
                      WHERE PortalAccountId in :accountIds] ) {
              
                   if (!EZChipUsers_list.isEmpty()) {
                   	
                   	  for (User ezChipUser : EZChipUsers_list) {
              
		                  acctIds2EzChiPRoles_Map.put(role.PortalAccountId, role.Id);
                	  }               
                   }    
                      
                if ( role.PortalRole == 'Manager' ) {     
                    System.debug('inside 73' );
                  	acctIdToManagerRoleIdMap.put(role.PortalAccountId, role.Id);
                    allRoleIds.add(role.Id);
                }    
                
                else if ( role.PortalRole == 'Worker' ) {
                    System.debug('inside 79' );
                    acctIdToUserRoleIdMap.put(role.PortalAccountId, role.Id);
                    allRoleIds.add(role.Id);
                }
        }
        System.debug('acctIdToManagerRoleIdMap: ' + acctIdToManagerRoleIdMap);
        System.debug('acctIdToUserRoleIdMap: ' + acctIdToUserRoleIdMap);
        System.debug('allRoleIds: ' + allRoleIds);

        Map<ID,Group> roleIdToGroup = new Map<Id, Group>();
        for ( Group grp : [select RelatedId from Group where RelatedId in :allRoleIds] ) {
            roleIdToGroup.put(grp.RelatedId, grp);
        }
        System.debug('roleIdToGroup: ' + roleIdToGroup);

        List<GroupMember> grpMembers = new List<GroupMember>();
        for ( ID acctId : accountIds ) {
            if ( acctIdToManagerRoleIdMap.containsKey(acctId) ) {
            	
            	 System.debug('inside 95' );
                grpMembers.add(
                    new GroupMember(
                        GroupId = MyMellanoxSettings.DesignInUsersGroupId, 
                        UserOrGroupId = roleIdToGroup.get(acctIdToManagerRoleIdMap.get(acctId)).Id));
            }

            if ( acctIdToUserRoleIdMap.containsKey(acctId) ) {
            	 System.debug('inside 103' );
                grpMembers.add(
                    new GroupMember(
                        GroupId = MyMellanoxSettings.SystemSupportUsersGroupId, 
                        UserOrGroupId = roleIdToGroup.get(acctIdToUserRoleIdMap.get(acctId)).Id));
            }
            
            
            else if ( acctIds2EzChiPRoles_Map.containsKey(acctId) ) {
                system.debug('MyMellanoxSettings.EzchipDesignInUsersGroupId :' + roleIdToGroup.get(acctIds2EzChiPRoles_Map.get(acctId)) );
                 System.debug('inside 113' );
                grpMembers.add(
                    new GroupMember(                    	
                        GroupId = MyMellanoxSettings.EzchipDesignInUsersGroupId, 
                        UserOrGroupId = roleIdToGroup.get(acctIds2EzChiPRoles_Map.get(acctId)).Id));
            }
            
        } //END: For (AccountIDs)
        System.debug('grpMembers: ' + grpMembers);
        insert grpMembers;
    }
}