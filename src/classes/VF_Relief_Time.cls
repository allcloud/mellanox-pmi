public with sharing class VF_Relief_Time{   
      
    public ID caseId;
    
    public VF_Relief_time(ApexPages.StandardController controller) 
    {
        //id = Apexpages.currentPage().getParameters().get('Id');
        //getDeliverAsPDF();
    }

    public PageReference getRelief_Time() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         caseId = Apexpages.currentPage().getParameters().get('Id');
                 
        //get the related  case
         Case c = [Select c.Priority, c.Relief_target_num__c,c.AccountId, c.Relief_time__c,c.Relief_given_duration__c,c.State_WaitForCustInfo_Time_Counter__c, c.State_OpenAdmin_Time_Counter__c, c.CreatedDate, c.State_OnHold_Time_Counter__c  From Case c WHERE c.id =: caseId];
         Account Acc = [Select Support_Level__c From Account a WHERE a.id =: c.AccountId];

        c.Relief_time__c = system.now();
         date dt = date.valueOf(c.CreatedDate);
        if(c.State_OnHold_Time_Counter__c==NULL) {c.State_OnHold_Time_Counter__c=0;}
        if(c.State_WaitForCustInfo_Time_Counter__c==NULL){c.State_WaitForCustInfo_Time_Counter__c =0;}
        if(c.State_OpenAdmin_Time_Counter__c == NULL) {c.State_OpenAdmin_Time_Counter__c=0;} 
        c.Relief_given_duration__c = ((dt.daysbetween(system.today()) - c.State_OnHold_Time_Counter__c )- c.State_WaitForCustInfo_Time_Counter__c - c.State_OpenAdmin_Time_Counter__c)*24;
        c.Relief_target_num__c = 9999;
       if(acc.Support_Level__c == 'Silver' ) 
        {
       if(c.Priority == 'Fatal'){c.Relief_target_num__c = 24;} 
       if(c.Priority == 'High'){c.Relief_target_num__c = 48;} 
       if(c.Priority == 'Medium'){c.Relief_target_num__c = 144;} 
        }

       if(acc.Support_Level__c == 'Gold' ) 
        {
       if(c.Priority == 'Fatal'){c.Relief_target_num__c = 4;} 
       if(c.Priority == 'High'){c.Relief_target_num__c = 8;} 
       if(c.Priority == 'Medium'){c.Relief_target_num__c = 24;} 
       if(c.Priority == 'Low'){c.Relief_target_num__c = 144;} 
        }
           
            
        update c;
        return new Pagereference('/'+caseId);
  
    }                                   
                          
 }