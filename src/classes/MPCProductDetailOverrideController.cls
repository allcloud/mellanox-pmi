public with sharing class MPCProductDetailOverrideController {
	String recordId;
	   
	public MPCProductDetailOverrideController(ApexPages.StandardController 
		   controller) {recordId = controller.getId();}
	
	public boolean inCommunity() {
		return Network.getNetworkId() != null;
	}
	
	public PageReference redirect() {
		if (inCommunity()) 
		{
			PageReference customPage =  Page.SupportProductItem;
			customPage.setRedirect(true);
			customPage.getParameters().put('id', recordId);
			return customPage;
		} else {
			return null; //otherwise stay on the same page  
		}
	}
}