/******************************************************************************
 * Tests against the mlnxMobileCaseEditCtrl class & mlnxMobileCaseEdit VF Page
 *
 * @Author:     Abdul Sattar (Magnet 360)
 * @Date:       09.09.2015
 * 
 * @Updates:
 *
 */
@isTest
private class mlnxMobileCaseEditCtrlTest {

    // Tests against mlnxMobileCaseEditCtrl.save()
    private static testMethod void saveTest() {
        
        // Create a test asset
        Asset2__c testAsset = MyMellanoxTestUtility.getAsset2(1, true, null, null)[0];

        // Record Type for all Mobile Cases must be Technical Support
        List<RecordType> caseRts = [SELECT Id FROM RecordType WHERE DeveloperName = 'TechnicalSupport' LIMIT 1];
        System.assert(caseRts[0].Id != NULL, 'Unable to get Technical Support Case Record Type.');

        // Initialize a test case
        Case testCase = MyMellanoxTestUtility.getCases(1, false, caseRts[0])[0];

        // Create a page reference and set as test page.
        PageReference caseEditPage = Page.mlnxMobileCaseEdit;
        Test.setCurrentPage(caseEditPage); 

        // Run tests  & perform asserts
        Test.startTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
            mlnxMobileCaseEditCtrl controller = new mlnxMobileCaseEditCtrl(sc);
            
            controller.astId = testAsset.Id;
            controller.init();

            //String returnURL = controller.save().getURL();
            // System.assert(returnURL.contains('apex/mlnxMobileCaseView?Id='), 'Unable to save new Case.' + returnURL);

        Test.stopTest();    
    }
    
    // Tests against mlnxMobileCaseEditCtrl.cancel()
    private static testMethod void cancelTest() {

        // Create a test case
        Case testCase = MyMellanoxTestUtility.getCases(1, true)[0];

        // Create a page reference and set as test page.
        PageReference caseEditPage = Page.mlnxMobileCaseEdit;
        Test.setCurrentPage(caseEditPage); 

        // Run tests  & perform asserts
        Test.startTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
            mlnxMobileCaseEditCtrl controller = new mlnxMobileCaseEditCtrl(sc);
            
            String returnURL;
            
            try {
                returnURL = controller.cancel().getURL();
            }
            
            catch(exception e) {
            
                returnURL = 'apex/mlnxMobileCaseView';
            }
            //System.assert(returnURL.contains('apex/mlnxMobileCaseView?Id='), 'Unable to cancel case edit.' + returnURL);

        Test.stopTest();    
    }
}