/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAddLoanProducts {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        List <Product2> LstProduct = new List <Product2>();
        Product2 prod1 = new Product2(Name='prod1');
        prod1.Inventory_Item_id__c='1111111111';
 
        Product2 prod2 = new Product2(Name='prod2');
        prod2.Inventory_Item_id__c = '222222222';

        Product2 prod3 = new Product2(Name='prod3');
        prod3.Inventory_Item_id__c = '333333333';
                
        LstProduct.add(prod1);
        LstProduct.add(prod2);
        LstProduct.add(prod3);
        
        insert LstProduct;
        
        Loan__c loan1 = new Loan__c();
        insert loan1;
        
        PageReference pageRef = Page.AddLoanProducts;
        
        Test.setCurrentPageReference(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(loan1);
        vfAddLoanProducts ctl = new vfAddLoanProducts(stdController);

        ctl.searchProducts();
        
        ctl.byKeyword = 'prod';
              
        ctl.searchProducts();
        
        ctl.add();
        
        //ctl.addAndSearch();
        
        vfAddLoanProducts.ProductSearchResult[] results = ctl.lst_Products;
        
        results[0].selected = true;
        
        ctl.addAndSearch();
        
        results[0].Qty = 'abc';
        
        ctl.addAndSearch();
        
        results[0].Qty = '1';
        
        ctl.addAndSearch();
        
        ctl.cancel();
    }
}