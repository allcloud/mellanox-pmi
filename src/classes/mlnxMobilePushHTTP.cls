global class mlnxMobilePushHTTP {
    
    // This method will send JSON to the "Parse" server that will handle Push Notifications
    @future(callout=true)
    global static void createHTTPRequestForPushNotif( list<String> arrayEmails_List, list<String> messages_List ) {
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.parse.com/1/functions/sendPushToEmailsArr');
        req.setMethod('POST');
        
       
        String headerStr1 = Label.httpHeader1;
        String headerStr2 = Label.httpHeader2;
        
        map<String, list<String>> values_Map = new map<String, list<String>>();
        values_Map.put('emailsArr', arrayEmails_List);
        values_Map.put('message', messages_List);
        
        String jsonMap_Str = JSON.serialize(values_Map);
        
        system.debug('jsonMap_Str : ' + jsonMap_Str);
        
        req.setHeader('X-Parse-Application-Id', headerStr1);
        req.setHeader('X-Parse-REST-API-Key', headerStr2);
        req.setBody(jsonMap_Str);
        
        // Create a new http object to send the request object
        // A response object is generated as a result of the request
        Http http = new Http();
        
        try {	
        	HTTPResponse res = http.send(req);
        }
        
        catch (exception e) {
        	
        	system.debug('Callout Error : '  + e.getMessage());
        }
    }
}