@isTest
private class Test_Serial_Numbers{
    
    static testMethod void UpdRMA_on_SN_family () {
    
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        RMA__c R = obj.createRMA();
        insert R;
        
        Serial_number__c SN = obj.createSN(R);
        insert SN;
        
        SN.advanced_replacement__c = True;
        SN.rma_type_by_support__c = 'Advanced Replacement';
        SN.Warranty_Valid_Date__c = system.today();
        update SN;
        
    }
    static testMethod void update_warranty_info ()
    
    {
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        List<string>  UpdSNs = new List<string>();
        List<string>  UpdIds = new List<string>();
        
        
        Account acc = obj.createAccount();
        insert acc;
        
        RMA__c rm = obj.createRMA();
        insert rm;
        
        
        Serial_number__c SN = obj.createSN(rm);
        SN.serial__c = '1234561';
        insert SN;
        
        UpdSNs.add(SN.serial__c );
        UpdIds.add(SN.id);
        
        Contract2__c c2 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8, Contract_Start_Date__c = system.today()+5);
        insert c2;
        
        
        
        List <Asset2__c> assts = new List<Asset2__c>();
        for(integer i = 0; i<200; i++){
            Asset2__c ast = obj.createAsset(c2,'test'+i);
            ast.serial_number__c = '123456'+ i;
            assts.add(ast);
        }
        
        insert assts;
        
        
        system.Test.StartTest();
        AssetToSerialNum reassign = new AssetToSerialNum();
        reassign.query = 'select id,Part_Number__c, Asset_Status__c,SN_Warranty_Expire__c,Serial_Number__c,Japan_Asset__c,SN_lower__c, Is_OEM__c,  RMA_type1__c from Asset2__c where  Serial_Number__c =\''+sn.serial__c + '\'';
        reassign.NewSNs1 = UpdIds;
        ID batchprocessid = Database.executeBatch(reassign);
        
        
        system.Test.StopTest();
    }
    
    static testMethod void test_updateRelatedRMAINSAProject() {
    	
    	CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        List<string>  UpdSNs = new List<string>();
        List<string>  UpdIds = new List<string>();
        
        
        Account acc = obj.createAccount();
        insert acc;
        
        RMA__c rm = obj.createRMA();
        insert rm;
        
        
        Serial_number__c SN = obj.createSN(rm);
        SN.serial__c = '1234561';
        insert SN;
        
        Contract2__c c2 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8, Contract_Start_Date__c = system.today()+5);
        insert c2;
        
        
        
        Asset2__c ast = obj.createAsset(c2,'test');
        ast.serial_number__c = '123456';
        ast.Project__c = 'INSA';
       
        insert ast;
        
        SN.Asset__c = ast.Id;
        
        Test.StartTest();
        
        update SN;
        
        Test.StopTest();
        
    }
}