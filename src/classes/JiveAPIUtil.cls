/**
    * @Author- Persistent
    * @Description-This class had methods that callout Addon REST API's.   
    */

public class JiveAPIUtil
{
    static string baseUrl; 
    static string jiveCommunityUrl;
    static Http h;

    //For Logger
    //public static final string apiRequestEndpoint;
    static string apiRequestEndpoint;
    static string apiRequestMethod;
    static string apiRequestBody;
    
    public static String getApiRequestEndpoint(){
        return apiRequestEndpoint;
    }
    public static String getApiRequestMethod(){
        return apiRequestMethod;
    }
    public static String getApiRequestBody(){
        return apiRequestBody;
    }
    
    static {
      
        h = new Http();
        JiveCommunitySettings__c dataSet = getJiveCustomSettings();
       //change this
        baseUrl = (dataSet.Community_Base_URL__c != NULL)? dataSet.Community_Base_URL__c:'';
        jiveCommunityUrl= (dataSet.Jive_URL__c!= NULL)? dataSet.Jive_URL__c:'';
        System.debug('Base Url'+baseUrl);
                
    }

    /**
    * @Author- Persistent
    * @Description-This  method is used to callout Addon REST API.  
    */
    public static HTTPResponse executeRest(string relUrl, string method ,String bodyJSON) {
    
        HttpRequest req;
        try{
            System.debug('Inside executeRest');
            req = new HttpRequest();
           
            req.setEndpoint(baseUrl+relUrl);
            apiRequestEndpoint =  baseUrl+relUrl;
           
            apiRequestMethod = method;
            apiRequestBody = bodyJSON;       
            System.debug('ENDPOINT is'+baseUrl+relUrl);
            
            req.setMethod(method);
            req.setHeader('Content-Type','application/json');
            
            //ask prashanth what should be the timeout value
             req.setTimeout(60000);  //60000 miliseconds = 60 sec
           
                system.debug('bodyJSON'+bodyJSON );
               req.setBody(bodyJSON);  
               system.debug('bodyJSON'+bodyJSON );
               system.debug('req.body--'+req.getBody() );
          
        }catch(Exception ex){
            system.debug('exception in executeRest'+ex.getMessage());
        }
        if(!Test.isRunningTest()){
            return h.send(req);
        }else{
            return null;
        }
       
    }//executeRest ends
    
    
  /**
    * @Author- Persistent
    * @Description-This  method is used retrieve JiveCommunitySettings  dataset values .  
    */
    public static JiveCommunitySettings__c getJiveCustomSettings() {
    
        JiveCommunitySettings__c jiveSetting = JiveCommunitySettings__c.getValues('DefaultSetting');
       
        return jiveSetting;
    }//getJiveCustomSettings ends
    
    
  /**
    * @Author- Persistent
    * @Description-This  method is used retrieve  LoggerSettings dataset values .  
    */
    public static LoggerSettings__c getLoggerSettings() {
    
        LoggerSettings__c loggerSettings = LoggerSettings__c.getValues('Default Logger Settings');
        
       
        return loggerSettings;
    }//getLoggerSettings ends
    
 
    
  /**
    * @Author- Persistent
    * @Description-This  method is used to callout Addon REST API to get all Jive discussion replies. 
    */
    public static HTTPResponse retrieveCaseThreads(String contentId)
    {
    
        String response;
        System.debug('inside sendContentFilters');
        HTTPResponse res;
        JiveContentIdWrapper contentIdWrapper;
     String retrieveCaseThreadsURI = '/discussionthreads' ; 
     
     if(!Test.isRunningTest()){
            contentIdWrapper=new JiveContentIdWrapper(jiveCommunityUrl,ContentId  );
           
            String requestParametersJSON = JSON.serialize(contentIdWrapper);
           
            system.debug('requestParametersJSON'+requestParametersJSON );
            
            res = executeRest(retrieveCaseThreadsURI,'POST',requestParametersJSON);
            
            response = res.getBody();
           
            System.debug('Response from Community is'+response) ;
    
        
    
      }
      else{
         response= '{"status": "ok"}';
      
      }
      return res;
      
    
    
    }//retrieveCaseThreads ends
    
    //change the method name to sendContentFilters  
     
    
  /**
    * @Author- Persistent
    * @Description-This method is used to callout Addon REST API to search Jive Community . 
    */
    public static HTTPResponse sendContentFilters(String filterText,String filterDate,String filterAuthor,Boolean grp,Boolean blog,Boolean document,Boolean space,String typeOfDiss){
    
      String response;
      JiveContentFilterWrapper filterWrapper;
      System.debug('inside sendContentFilters');
      HTTPResponse res;
      String searchContentURI = '/search' ; 
      if(!Test.isRunningTest()){
            filterWrapper=new JiveContentFilterWrapper(filterText,filterAuthor,filterDate,jiveCommunityUrl,grp,blog,document,space,typeOfDiss);
            
            String requestParametersJSON = JSON.serialize(filterWrapper);
            system.debug('requestParametersJSON'+requestParametersJSON );
            res = executeRest(searchContentURI,'POST',requestParametersJSON);
            response = res.getBody();
            System.debug('Response from Community is'+response);
    
      }
      else if(Test.isRunningTest()){
             response= '{"status": "ok"}';
     
      }
     
      return res;   
    
    }        
    
  
  /**
    * @Author- Persistent
    * @Description-This method is used to callout Addon REST API to retrieve Jive community spaces   
    */
    public static HTTPResponse getContentsByType(String contentType){
    
      String response;
      System.debug('inside getContentsByType--contentType--'+contentType);
      HTTPResponse res;
      
String contentListByTypeURI = '/getContentListByTypes' ;
      if(String.isNotBlank(contentType) && String.isNotBlank(contentListByTypeURI)){
          if(!Test.isRunningTest()){
                String jsonBodyRequest = JSON.serialize(new ContentsByTypeRequestWrapper(jiveCommunityUrl,contentType));
                System.debug('jsonBodyRequest is--'+jsonBodyRequest) ;
                res = executeRest(contentListByTypeURI,'POST',jsonBodyRequest);
                response = res.getBody();
                System.debug('Response from Community is--'+response) ;
                
          }
          else if(Test.isRunningTest()){
                 response= '{"status": "ok"}';
         
          }
      }
      
      return res;   
    
    }
   
  
     
    /**
     * @Author- Persistent
     * @Description-This method is used to callout Addon REST API to retrieve Jive community spaces. It includes Logger Implemetation   
     */
    @future (callout=true)
    public static void sendPostbackMessage(String jiveUrl,Map<String,Boolean> map_optionReply_caseId,Map <String,String> map_contentId_caseId,Map <String,String> map_caseComment_caseId,Set <String> set_CaseId,String parentId, Id loggerId,String attID,String attName,String userEmail,String userName ){
        Logger__c logger;
        Id caseRecordId;
        String contentId;
        Boolean isRetry;
        Boolean isSuccess;
        String logMessage;
        Boolean isCorrectAnswerPostbackFailure = false;
        try{     
            System.debug('inside sendPostbackMessage  ');        

            if(String.isNotBlank(loggerId)){
                isRetry = true;
                List<Logger__c> loggerList = [Select Status__c, SFObject__c, SFID__c, Retry__c, RetryCount__c, Name, Logtype__c, LoggedTime__c, LastModifiedDate, JiveId__c, JiveContentType__c, IsErrorLog__c, Id, FeatureType__c, ErrorMessage__c, Description__c, CreatedDate From Logger__c where id=:loggerId];
                logger = loggerList.isEmpty() ? null : loggerList[0];
            }else{
                isRetry = false;
            }

            JiveMessageWrapper mesgWrapper ;

            for (String caseId:set_CaseId)  {

                mesgWrapper = new JiveMessageWrapper(jiveUrl,map_optionReply_caseId.get(caseId),map_contentId_caseId.get(caseId),map_caseComment_caseId.get(caseId),parentId,attID,attName,userEmail,userName);
                caseRecordId = caseId;
                contentId = map_contentId_caseId.get(caseId);
            }        
            String postBackURI = '/csoemjc/salesforce/postback';
           
            String mesgReplyJSON = JSON.serialize(mesgWrapper);
            System.debug('REQUEST JSON FORMAT :'+mesgReplyJSON);


            String response;
            HTTPResponse res; 
            if(!Test.isRunningTest()){
                res = executeRest(postBackURI,'POST',mesgReplyJSON);
                response = res.getBody();
                isSuccess = (res.getStatusCode() == 200);
                logMessage = 'Response from Jive ---> '+response;
            }
            system.debug('response--'+response);
           

            if(Test.isRunningTest()){
                response= '{"statusCode":400,"entity":{"error":{"status":400,"message":"Another message is already marked as the answer to this discussion."}}}';
                isSuccess = false;
            }

            //Deserializing the actual response from Jive community
            Map<String, Object> apiResponseBodyMap = (Map<String, Object>)JSON.deserializeUntyped(response);
            system.debug('apiResponseBodyMap--'+apiResponseBodyMap);

            if(apiResponseBodyMap != null){
                Integer jiveStatusCode = (Integer)apiResponseBodyMap.get('statusCode');
                system.debug('jiveStatusCode--'+jiveStatusCode);

                if(jiveStatusCode != null && jiveStatusCode != 200){
                    isSuccess = false;
                    system.debug('isSuccess--'+isSuccess);
                }


                Map<String, Object> entityMap = (Map<String, Object>)apiResponseBodyMap.get('entity');
                system.debug('entityMap--'+entityMap);

                if(entityMap != null){
                    Map<String, Object> errorMap = (Map<String, Object>)entityMap.get('error');
                    system.debug('errorMap--'+errorMap);

                    if(errorMap != null){
                        Integer jiveStatus = (Integer)errorMap.get('status');
                        String jiveMessage = (String)errorMap.get('message');
                        system.debug('jiveStatus--'+jiveStatus);
                        system.debug('jiveMessage--'+jiveMessage);

                        //Log the error from Jive Community - Posting a Correct Answer to a Jive question for which correct answer already exists
                        if(jiveStatus == 400 && jiveMessage == 'Another message is already marked as the answer to this discussion.'){
                            isSuccess = false;
                            isCorrectAnswerPostbackFailure = true;
                        }
                    }
                }
            }
           
            system.debug('loggerId--'+loggerId+'--logger record--'+logger);


        }catch(Exception ex){
            system.debug('Exception inside sendPostbackMessage method--'+ex.getMessage());
            isSuccess = false;
            logMessage = ex.getMessage();
        }finally{
            //Logging - Success/Failure/Manual Retry
            system.debug('inside finally--isSuccess--'+isSuccess+'--isRetry--'+isRetry+'--caseRecordId--'+caseRecordId+'--contentId--'+'--logMessage--'+logMessage+'--logger--'+logger);
         
            upsertLoggerForCommunityPostback(logger, isSuccess, isRetry, caseRecordId, contentId, logMessage, isCorrectAnswerPostbackFailure);
        }



    }//sendPostbackMessage ends

     
     
  /**
    * @Author- Persistent
    * @Description-Wrapper class used for sending REST request to get all Jive Community spaces     
    */ 
    public class ContentsByTypeRequestWrapper{
        public String contentType;
        public String jiveURL;
        public ContentsByTypeRequestWrapper(String jiveURL,String contentType){
            this.jiveURL= jiveURL;
            this.contentType = contentType;
        }
    }//ContentsByTypeRequestWrapper ends
    
   
  /**
    * @Author- Persistent
    * @Description-Wrapper class used for REST response to get all Jive Community spaces     
    */ 
    public class ContentsByTypeResponseWrapper{
        public String id;
        public String name;
        public String contentType;
        public String url;
        public ContentsByTypeResponseWrapper(String id,String name,String url,String contentType){
            this.id= id;
            this.name= name;
            this.contentType = contentType;
            this.url=url;
        }
    }//ContentsByTypeResponseWrapper ends
    
    
    /**
    * @Author- Persistent
    * @Description- This method is used to create or update a Logger involved in Closed Case comments Postback feature.
    *               Common method Invoked for both Normal Flow from Close Case Page and Retry button from Logger record
    */
    public static void upsertLoggerForCommunityPostback(Logger__c logger, Boolean isSuccess, Boolean isRetry, Id caseRecordId, String contentId, String logMessage, Boolean isCorrectAnswerPostbackFailure){
        system.debug('isSuccess--'+isSuccess+'--isRetry--'+'--caseRecordId--'+caseRecordId+'--contentId--'+'--logMessage--'+logMessage+'--logger--'+logger);
        if(logger == null){//No logger exists and hence this is not Retry but First time Callout 
            //Log the Error
            
            if(isSuccess){//Donot log success cases
                return;
            }
            
            logger = new Logger__c();
            logger.IsErrorLog__c = isSuccess ? false : true;
           
            logger.FeatureType__c = isSuccess ? 'General' : (isCorrectAnswerPostbackFailure == true ? 'Community Postback - Correct Answer Postback Failure' : 'Community Postback - Postback Failure');
            logger.Logtype__c = 'Callout';
            logger.Status__c = isSuccess ? 'Success' : 'Failure';
            logger.SFObject__c = 'Case';
            logger.SFID__c = caseRecordId;
            logger.JiveContentType__c = 'Question';
            logger.JiveId__c = contentId;
            logger.APIRequestEndpoint__c = String.isNotBlank(apiRequestEndpoint) ? apiRequestEndpoint : (apiRequestEndpoint=JiveAPIUtil.getApiRequestEndpoint());
            logger.APIRequestMethod__c = String.isNotBlank(apiRequestMethod) ? apiRequestMethod : (apiRequestMethod=JiveAPIUtil.getApiRequestMethod());
            logger.APIRequestBody__c = String.isNotBlank(apiRequestBody) ? apiRequestBody : (apiRequestBody=JiveAPIUtil.getApiRequestBody());
            
            if(isSuccess){
                logger.Description__c = logMessage;
            }else{
                logger.ErrorMessage__c = logMessage;
            }
           
            system.debug('logger--'+logger);
            insert logger;
           
        }else{
            //update current logger record
            if(isRetry){//Logger exists and Retry is performed
                logger.Status__c = isSuccess ? 'Resolved' : 'Re-Attempted';
                logger.Retry__c = isSuccess ? 'Not Applicable' : 'In-Progress';
                logger.RetryCount__c = (logger.RetryCount__c != null) ? (logger.RetryCount__c+1) : 1;
                logger.ErrorMessage__c += '--Retry Attempt '+logger.RetryCount__c+' @'+system.now().format('yyyy-MM-dd HH:mm:ss') + '-->'+ logMessage;
                logger.LoggedTime__c = system.now();
                system.debug('logger--'+logger);
                update logger;
            }
        }
    }
    
    
    
    /**
    * @Author- Persistent
    * @Description-This method is used to generate a Logger object record with the required parameters   
    */ 
    public static Logger__c generateLogger(Logger__c logger, Boolean isSuccess, Boolean isRetry, String featureType, String logType, String sfObject, String jiveContentType, Id sfObjectId, String contentId, String logMessage){
        system.debug('inside generateLogger--isSuccess--'+isSuccess+'--isRetry--'+'--sfObjectId--'+sfObjectId+'--contentId--'+'--logMessage--'+logMessage+'--logger--'+logger);
        if(logger == null){//Insert logger - First time Callout - Success/Failure - Not Retry
            //Log the Error
            logger = new Logger__c();
            logger.IsErrorLog__c = isSuccess ? false : true;
            logger.FeatureType__c = isSuccess ? 'General' : featureType;
            logger.Logtype__c = logType;
            logger.Status__c = isSuccess ? 'Success' : 'Failure';
            logger.SFObject__c = sfObject;
            logger.SFID__c = sfObjectId;
            logger.JiveContentType__c = jiveContentType;
            logger.JiveId__c = contentId;
            logger.APIRequestEndpoint__c = String.isNotBlank(apiRequestEndpoint) ? apiRequestEndpoint : (apiRequestEndpoint=JiveAPIUtil.getApiRequestEndpoint());
            logger.APIRequestMethod__c = String.isNotBlank(apiRequestMethod) ? apiRequestMethod : (apiRequestMethod=JiveAPIUtil.getApiRequestMethod());
            logger.APIRequestBody__c = String.isNotBlank(apiRequestBody) ? apiRequestBody : (apiRequestBody=JiveAPIUtil.getApiRequestBody());
            
            //Populate description with logMessage(success) if action is successful
            if(isSuccess){
                logger.Description__c = logMessage;
            }else{//Populate ErrorMessage with logMessage(error) if action is failed
                logger.ErrorMessage__c = logMessage;
            }
        }else{
            //update current logger record
            if(isRetry){
                logger.Status__c = isSuccess ? 'Resolved' : 'Re-Attempted';
                logger.Retry__c = isSuccess ? 'Not Applicable' : 'In-Progress';
                logger.RetryCount__c = (logger.RetryCount__c != null) ? (logger.RetryCount__c+1) : 1;
                logger.ErrorMessage__c += '--Retry Attempt '+logger.RetryCount__c+' @'+system.now().format('yyyy-MM-dd HH:mm:ss') + '-->'+ logMessage;
                logger.LoggedTime__c = system.now();
                
                //Update the Jive Id of logger when retry is successful for the 1st time KA sync for a new doc
                if(String.isBlank(logger.JiveId__c) &&  String.isNotBlank(contentId)){
                    logger.JiveId__c = contentId;
                }
            }
        }
        
        system.debug('inside generateLogger--logger--'+logger);
        return logger;
    }
        
        
}//JiveAPIUtil ends