public with sharing class MPCProjectOverrideController {
	private final Milestone1_Project__c record;
	   
	public MPCProjectOverrideController(ApexPages.StandardController 
		   controller) {
		   		//List <String> fields = new List <String>{'Product_Family__c'};
		   		//controller.addFields(fields);
		   		this.record = (Milestone1_Project__c)controller.getRecord();
		   }
	
	public boolean inCommunity() {
		return Network.getNetworkId() != null;
	}
	
	public PageReference redirect() {
		if (inCommunity()) 
		{
			PageReference customPage =  Page.MPCProjectDetail;
			customPage.setRedirect(true);
			customPage.getParameters().put('id', record.Id);
			return customPage;
		} else {
			return null; //otherwise stay on the same page  
		}
	}
}