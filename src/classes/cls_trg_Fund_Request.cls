public class cls_trg_Fund_Request {
          
    // This method will send all the fund requests that their statuses have changed to 'Submitted' for approval
    public void sendFundRequestToApproval(list<SFDC_MDF__c> newMDFs_List, map<Id, SFDC_MDF__c> oldMDFs_Map) {
        
        list<SFDC_MDF__c> MDFsForApproval_List = getListOfMDFsForApproval(newMDFs_List, oldMDFs_Map);
        
        for (SFDC_MDF__c mdfForApproval : MDFsForApproval_List) {
            
            // Create an approval request for the Fund Requst
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Sending the fund Requst for approval');
            req1.setObjectId(mdfForApproval.id);
            //req1.setNextApproverIds(new Id[] {mdfForApproval.Next_Approver__c});
            Approval.ProcessResult result;
            // Submit the approval request for the Opportunity
            try {   
                
                result = Approval.process(req1);
            }
            
            catch(Exception e) {
                
                system.debug('Error :' + e.getMessage());
            }
        }
    }
    
   
    
    // This method will return a list of fund requests for approval
    private list<SFDC_MDF__c> getListOfMDFsForApproval(list<SFDC_MDF__c> newMDFs_List, map<Id, SFDC_MDF__c> oldMDFs_Map) {
        
        list<SFDC_MDF__c> MDFsForApproval_List = new list<SFDC_MDF__c>();
        
        for (SFDC_MDF__c newMDF : newMDFs_List) {
            
            if (newMDF.Status__c != null) {
                
                if (oldMDFs_Map != null) {  
                    if (newMDF.Status__c.EqualsIgnoreCase('Submitted') && newMDF.Status__c != oldMDFs_Map.get(newMDF.Id).Status__c) {
                        
                        MDFsForApproval_List.add(newMDF);
                    }
                }
                
                else if (newMDF.Status__c.EqualsIgnoreCase('Submitted')) {
                    
                    MDFsForApproval_List.add(newMDF);
                }
            }
        }
        
        return MDFsForApproval_List;
    }
    
    
      /*
    // This method will fill up the 'CAM' and 'TAM' fields when a new Fund Request record is created, according to their Account's country region
    public void updateFundRequestCAMAndTAM (list<SFDC_MDF__c> newMDFs_List) {
        
        map<String, map<Id, String>> countriesAndStates2AccountsMap_Map =  getMapOfAccountsAndTheirBillingCountries(newMDFs_List);
       system.debug('countriesAndStates2AccountsMap_Map : ' + countriesAndStates2AccountsMap_Map);
        map<Id, String> accounts2Countries_Map = new map<Id, String>();
        map<Id, String> accounts2StatesCodes_Map = new map<Id, String>();
        
        if (countriesAndStates2AccountsMap_Map.containsKey('BillingCountryCode')) {
            
            accounts2Countries_Map = countriesAndStates2AccountsMap_Map.get('BillingCountryCode');
        }
        
        if (countriesAndStates2AccountsMap_Map.containsKey('BillingStateCode')) {
            
            accounts2StatesCodes_Map = countriesAndStates2AccountsMap_Map.get('BillingStateCode');
        }
        
        
        
        map<String, list<Id>> statesCodes2CAMandTAMList_Map = getMapOfStatesAndCAMandTAM(accounts2StatesCodes_Map);
        map<String, list<Id>> countryCodes2CAMandTAMList_Map = getMapOfCountriesAndCAMandTAM(accounts2Countries_Map);
        system.debug('statesCodes2CAMandTAMList_Map : ' + statesCodes2CAMandTAMList_Map);
        system.debug('countryCodes2CAMandTAMList_Map : ' + countryCodes2CAMandTAMList_Map);
        system.debug('accounts2StatesCodes_Map : ' + accounts2StatesCodes_Map);
        
        for (SFDC_MDF__c newMDF : newMDFs_List) {
            
            if ( accounts2Countries_Map.containsKey(newMDF.Account__c) ) {    
                system.debug('inside 84');
                String countryCode_Str = accounts2Countries_Map.get(newMDF.Account__c);
                 if (countryCodes2CAMandTAMList_Map.containsKey(countryCode_Str)) {
                     system.debug('inside 88');
                    newMDF.CAM__c = countryCodes2CAMandTAMList_Map.get(countryCode_Str)[0];
                    
                    if (countryCodes2CAMandTAMList_Map.get(countryCode_Str).size() > 1) {
                        system.debug('inside 92');
                        newMDF.TAM__c = countryCodes2CAMandTAMList_Map.get(countryCode_Str)[1];
                    }
                }
            }
            
            else {
                
                if (accounts2StatesCodes_Map.containsKey(newMDF.Account__c)) {
                    system.debug('inside 101');
                    String stateCode_Str = accounts2StatesCodes_Map.get(newMDF.Account__c);
                    
                     system.debug('stateCode_Str : ' + stateCode_Str);
                    
                    if (statesCodes2CAMandTAMList_Map.containsKey(stateCode_Str)) {
                     system.debug('inside 107');
                        newMDF.CAM__c = statesCodes2CAMandTAMList_Map.get(stateCode_Str)[0];
                        
                        if (statesCodes2CAMandTAMList_Map.get(stateCode_Str).size() > 1) {
                            system.debug('inside 111');
                            newMDF.TAM__c = statesCodes2CAMandTAMList_Map.get(stateCode_Str)[1];
                        }
                        
                    }
                }
            }
        }
    }
    
    // This method returns a map of ther Fund Request related Account IDs as keys and Account Billing Country as values
    private map<String, map<Id, String>> getMapOfAccountsAndTheirBillingCountries(list<SFDC_MDF__c> newMDFs_List) {
        
        map<String, map<Id, String>> countriesAndStates2AccountsMap_Map = new map<String, map<Id, String>>();
        map<Id, String> accounts2Countries_Map = new map<Id, String>();
        map<Id, String> accounts2StatesCodes_Map = new map<Id, String>();
        set<Id> relatedAccountsIds_Set = new set<Id>();
                
        for (SFDC_MDF__c newMDF : newMDFs_List) {
            
            relatedAccountsIds_Set.add(newMDF.Account__c);
        }
        
        if (!relatedAccountsIds_Set.isEmpty()) {
            
            for (Account acc : [SELECT Id, BillingCountryCode, BillingStateCode FROM Account WHERE Id IN : relatedAccountsIds_Set]) {
                
                if (acc.BillingCountryCode != null && !acc.BillingCountryCode.EqualsIgnoreCase('US')) {  
                    
                    accounts2Countries_Map.put(acc.Id, acc.BillingCountryCode);
                }
                
                else if (acc.BillingStateCode != null){
                    
                    accounts2StatesCodes_Map.put(acc.Id, acc.BillingStateCode);
                }
            }
            
            countriesAndStates2AccountsMap_Map.put('BillingStateCode', accounts2StatesCodes_Map);
            countriesAndStates2AccountsMap_Map.put('BillingCountryCode', accounts2Countries_Map);
            system.debug('countriesAndStates2AccountsMap_Map : ' + countriesAndStates2AccountsMap_Map);
        }
        
        return countriesAndStates2AccountsMap_Map;
    }
    
    // This method returns a map of countryCodes as keys and list of Users Ids (to populate CAM__c and TAM__c)
    private map<String, list<Id>> getMapOfCountriesAndCAMandTAM(map<Id, String> accounts2CoutryCodes_Map) {
        
        map<String, list<Id>> countryCodes2CAMandTAMList_Map = new map<String, list<Id>>();
        
        system.debug('accounts2CoutryCodes_Map : ' + accounts2CoutryCodes_Map);
         system.debug('accounts2CoutryCodes_Map.values() : ' + accounts2CoutryCodes_Map.values());
          
        list< Regions__c> regions_List  = [SELECT Id, Name, version1__c, CAM__c, TAM__c FROM Regions__c WHERE version1__c IN : accounts2CoutryCodes_Map.values()];        
         system.debug('regions_List : ' + regions_List);
        for (Regions__c region  : regions_List) {
            
            list<Id> usersId_List;
            
            if (region.CAM__c != null) {
                
                if (region.TAM__c != null) {
                    
                    if (region.CAM__c != region.TAM__c) {
                        
                        usersId_List = new list<Id>{region.CAM__c, region.TAM__c};
                    }
                    
                    else {
                    
                        usersId_List = new list<Id>{region.CAM__c};
                    }
                }
                
                else {
                    
                    usersId_List = new list<Id>{region.CAM__c};
                }
                
                countryCodes2CAMandTAMList_Map.put(region.version1__c, usersId_List);
            }
            
            system.debug('countryCodes2CAMandTAMList_Map : ' + countryCodes2CAMandTAMList_Map);
        }
        
        return countryCodes2CAMandTAMList_Map;
    }
    
    // This method returns a map of States as keys and list of Users Ids (to populate CAM__c and TAM__c)
    private map<String, list<Id>> getMapOfStatesAndCAMandTAM(map<Id, String> accounts2StatesCodes_Map) {
        
        map<String, list<Id>> statesCodes2CAMandTAMList_Map = new map<String, list<Id>>();
         
        list< Region_State__c> regionsStates_List  = [SELECT Id, Name, StateCode__c, CAM__c, TAM__c FROM Region_State__c WHERE StateCode__c IN : accounts2StatesCodes_Map.values()];        
        
        for (Region_State__c region  : regionsStates_List) {
            system.debug('regionsStates_List : ' + regionsStates_List);
            list<Id> usersId_List;
            
            if (region.CAM__c != null) {
                
               if (region.TAM__c != null) {
                   
                    if (region.CAM__c != region.TAM__c) {
                        
                        usersId_List = new list<Id>{region.CAM__c, region.TAM__c};
                    }
                    
                    else {
                    
                        usersId_List = new list<Id>{region.CAM__c};
                    }
               }
                
                else {
                    
                    usersId_List = new list<Id>{region.CAM__c};
                }
                
                statesCodes2CAMandTAMList_Map.put(region.StateCode__c, usersId_List);
            }
        }
        
        return statesCodes2CAMandTAMList_Map;
    }
    
    // This method will create a Sharing Object per each record of Fund Request created for others in the same Account to View/Edit
    public void createMDFShareForMDFRequet(list<SFDC_MDF__c> newMDFs_List) {
        
        Id userRoleId = UserInfo.getUserRoleId();
        list<SObject> objects2Create_List = new list<SObject>();
        
        list<Group> relatedGroups_List = [SELECT Id FROM Group WHERE RelatedId =: userRoleId and type =: 'Role'];
        
        if (!relatedGroups_List.isEmpty()) {
            
            for (SFDC_MDF__c newMDF : newMDFs_List)  {
                
                SFDC_MDF__Share MFDShare = new SFDC_MDF__Share();
                MFDShare.ParentId = newMDF.Id;
                MFDShare.UserOrGroupId = relatedGroups_List[0].Id;
                MFDShare.AccessLevel = 'Edit';
                objects2Create_List.add(MFDShare);
            }
            
            try {
                
                insert objects2Create_List;   
            }
            
            catch(Exception e) {
                
                system.debug('Error : ' + e.getMEssage());
            }
        }
    }
    
    // This method will update the fund request fields 'Approval_Comment__c' with the Approver Comment and will turn 'Approval_Handled__c' to false
    public void updateFundRequestApprovalRequest(list<SFDC_MDF__c> newFundRequests_List, map<Id, SFDC_MDF__c> oldFundRequests_Map) {
        
        if (newFundRequests_List.size() == 1) {
            
            if (newFundRequests_List[0].Approval_Handled__c && !oldFundRequests_Map.get(newFundRequests_List[0].Id).Approval_Handled__c) {
            
                Id newFundRequsetId = newFundRequests_List[0].Id;
                
                updateFundRequestCommentASync(newFundRequsetId);
            }
         }                                              
    }
    
    // This method will update the fund request fields 'Approval_Comment__c' and 'Approval_Handled__c' Async
    @future
    public static void updateFundRequestCommentASync(ID fundRequestId) {
        
        list<ProcessInstance> ProcessInstances_List = [Select Id, (Select Id, ProcessInstanceId, StepStatus,
                                                       OriginalActorId, ActorId, Comments, StepNodeId, CreatedDate From Steps
                                                       order by createdDate desc limit 1)
                                                       From ProcessInstance p WHERE p.TargetObjectId =: fundRequestId
                                                       order by lastModifiedDate desc limit 1];
                
            String theComment;    
            if (!ProcessInstances_List.isEmpty()) {
                
                for (ProcessInstance instance : ProcessInstances_List) {
                    
                    for (ProcessInstanceStep lastStep : instance.Steps) {
                    
                        system.debug('lastStep : ' + lastStep);
                        theComment = lastStep.Comments;
                    }
                }
            }
            
        SFDC_MDF__c newFundRequest = new SFDC_MDF__c(Id = fundRequestId);
        newFundRequest.Approval_Handled__c = false;
        newFundRequest.Approval_Comment__c = theComment;
        update newFundRequest;
    }
    
    // This method will send email with the Invoice Attachment included, when a Fund Request 'PO_Attachment_Id__c' is changed and is not null
    public void sendEmailAndInvoiceAttachmentWhenRequsetsApproved(list<SFDC_MDF__c> newMDFs_List, map<Id, SFDC_MDF__c> oldMDFs_Map) {
        
        
        list<SFDC_MDF__c> requestsToSendEmailto_List = getApprovedFundRequests(newMDFs_List, oldMDFs_Map);
        map<Id, Attachment> attachmentsIds2Attachments_Map = getMapOfInvoicesRelatedToRequests(requestsToSendEmailto_List);
        
        if(!requestsToSendEmailto_List.isEmpty()) {
            
            for(SFDC_MDF__c request : requestsToSendEmailto_List) {
                
                List<Messaging.EmailFileAttachment> emailAttachments_list = new List<Messaging.EmailFileAttachment>();
                
                system.debug('request.PO_Attachment_Id__c : ' +  request.PO_Attachment_Id__c);
                system.debug('attachmentsIds2Attachments_Map : ' +  attachmentsIds2Attachments_Map);
                
                if (request.PO_Attachment_Id__c != null && attachmentsIds2Attachments_Map.containsKey(request.PO_Attachment_Id__c)) {
                    
                    Attachment attachmentPO = new Attachment();
                    Attachment attachment = attachmentsIds2Attachments_Map.get(request.PO_Attachment_Id__c);
                    
                    
                    if (attachment.ParentId == request.Id) {    
                        
                        Messaging.EmailFileAttachment attachmentE = new Messaging.EmailFileAttachment();
                        attachmentE.setBody(attachment.Body);
                        attachmentE.setContentType(attachment.ContentType);
                        attachmentE.setFileName(attachment.Name);
                        emailAttachments_list.add(attachmentE);
                        
                         
                        list<String> ccAdresses_List = new list<String>{'innag@mellanox.com','kara@mellanox.com', 'patty@mellanox.com'};  // {'kara@mellanox.com', 'patty@mellanox.com'};
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        //mail.setSubject('Invoice Approved');
                            
                     //   if (request.Requestor_Name__c!= null) {
                            mail.setFileAttachments(emailAttachments_list);
                            mail.setTargetObjectId(request.Requestor_Contact__c);
                            
                            mail.setTemplateID('00X50000001sPyu'); // Production
                            //mail.setTemplateID('00XR0000000Qk1l'); //SandBox
                            mail.setCcAddresses(ccAdresses_List);
                            mail.saveAsActivity = false;
                            mail.setWhatId(request.id);
                            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {mail};
                            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                        
                            if (results[0].success) {
                               
                                System.debug('The email was sent successfully.');
                            }
                            
                             else {
                                
                                System.debug('The email failed to send: '
                                      + results[0].errors[0].message);
                            }
                       // } 
                    } 
                }
            }
        }
    }
    
    // This method retruns a map of attachments Ids and Attachments as their values, representing all the Invoices of the approved Fund Claims  
    private map<Id, Attachment> getMapOfInvoicesRelatedToRequests(list<SFDC_MDF__c> requestsToSendEmailto_List) {
        
        set<Id> invoicesIds_Set = new set<Id>();
        
         
            
        for (SFDC_MDF__c request : requestsToSendEmailto_List) {
            
            invoicesIds_Set.add(request.PO_Attachment_Id__c);
        }
           
        system.debug('invoicesIds_Set : ' + invoicesIds_Set);
        map<Id, Attachment> attachmentsIds2Attachments_Map;
        
        if (!invoicesIds_Set.isEmpty()) {
            
            attachmentsIds2Attachments_Map = new map<Id , Attachment>([SELECT ParentId, Name, Id, Description, ContentType, BodyLength, Body
                                                                       From Attachment WHERE Id IN : invoicesIds_Set]);
        }
        
         system.debug('attachmentsIds2Attachments_Map : ' + attachmentsIds2Attachments_Map);
        
        return attachmentsIds2Attachments_Map;
    }
    
    // This method will return a list of Fund Claims that their status has been changed to 'Approved'
    private list<SFDC_MDF__c> getApprovedFundRequests(list<SFDC_MDF__c> newMDFs_List, map<Id, SFDC_MDF__c> oldMDFs_Map) {
        
        list<SFDC_MDF__c> requestsToSendEmailto_List = new list<SFDC_MDF__c>();
        
        for (SFDC_MDF__c newMDF : newMDFs_List) {
            
            if (newMDF.PO_Attachment_Id__c != null) {
                 
                if (newMDF.PO_Attachment_Id__c != oldMDFs_Map.get(newMDF.Id).PO_Attachment_Id__c) {
                     
                    requestsToSendEmailto_List.add(newMDF);
                }
            }
        }
        
        return requestsToSendEmailto_List;
    }  
    
    */ 
}