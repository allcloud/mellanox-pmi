public with sharing class cls_trg_Product_Detail {
	
	// This method will update the field 'Image_URL__c' on the Product when Product Details 'StaticResourceImage__c' is Changed
	public void updateRelatedProductsImageURL(list<ProductDetails__c> newProductDeatils_List, map<Id, ProductDetails__c> oldProductDetials_Map) {
		
		list<ProductDetails__c> ProductDeatilIds2Update_List = getProductDetialsAndRelatedProducts(newProductDeatils_List, oldProductDetials_Map);
		list<Product2> products2Update_List = new list<Product2>();
		
		if (ProductDeatilIds2Update_List != null) {
			
			for (ProductDetails__c productDetail2Update : ProductDeatilIds2Update_List) {
				
				for (Product2 relatedProduct : productDetail2Update.Products__r) {
					
					relatedProduct.Image_URL__c = URL.getSalesforceBaseUrl().toExternalForm() + '/resource/' + productDetail2Update.StaticResourceImage__c;
					products2Update_List.add(relatedProduct);
				}
			}
		}
		
		if (!products2Update_List.isEmpty()) {
			
			try {
				
				update products2Update_List;
			}
			
			catch(Exception e) {
				
				newProductDeatils_List[0].addError('Cannot update the Product :' + e.getMessage());
			}
		}
	}
	
	
	
	// This method returns a list of Product Detials with their related Products (Product2) if any exists
	private list<ProductDetails__c> getProductDetialsAndRelatedProducts(list<ProductDetails__c> newProductDeatils_List, map<Id, ProductDetails__c> oldProductDetials_Map) {
		
		set<Id> ProductDeatilIds_Set = new set<Id>();
		list<ProductDetails__c> ProductDeatilIds2Update_List;
		
		for (ProductDetails__c newProductDeatil : newProductDeatils_List) {
			
			if (newProductDeatil.StaticResourceImage__c != null) {
				
				if (newProductDeatil.StaticResourceImage__c != oldProductDetials_Map.get(newProductDeatil.Id).StaticResourceImage__c) {
					
					ProductDeatilIds_Set.add(newProductDeatil.Id);
				}
			}
		}
		
		if (!ProductDeatilIds_Set.isEmpty()) {
			
			ProductDeatilIds2Update_List = [SELECT Id, StaticResourceImage__c, (SELECT Id, Image_URL__c FROM Products__r) FROM ProductDetails__c
											WHERE Id IN : ProductDeatilIds_Set];
		}
		
		return ProductDeatilIds2Update_List;
	}

}