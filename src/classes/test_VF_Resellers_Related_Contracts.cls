@isTest(seeAllData=true)
public with sharing class test_VF_Resellers_Related_Contracts {
	
	static testMethod void test_Resellers_Related_Contracts() {
		
		User testUser = [SELECT Id FROM User WHERE contactId != null and isActive = true limit 1];
		
		Contract2__c testContract = new Contract2__c();
		
		system.runAs(testUser) {
		
			//ApexPages.StandardController sc = new ApexPages.StandardController(testContract);
		
			VF_Resellers_Related_Contracts controller = new VF_Resellers_Related_Contracts();
			controller.getSelectContracts();
			controller.fillTheRelatedContactsList();
			controller.selectedView = 'My Contracts';
			controller.fillTheRelatedContactsList();
			controller.selectedView = 'My Customers Active Contracts';
			controller.fillTheRelatedContactsList();
			controller.selectedView = 'My Customers Expired Contracts';
			controller.fillTheRelatedContactsList();
			controller.redirectNonResellersUsersToStandardContractsPage();
			
		}
	}
}