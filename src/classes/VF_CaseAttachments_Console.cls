/*
Elad Kaplan
Blat-Lapidot
10-07-2013
the below controller does:
* Getting the Case ID of current case
* Extract all his related Attachment as per case id and as per the privateAttachments
* 
*/

public with sharing class VF_CaseAttachments_Console {
	
	/**************  INNER CLASS  ******************/
        public class CaseAttachments_cls
    {
        public Attachment att {get;set;}
        public String publicOrPrivate {get;set;}
        public String RMcase {get;set;}
        public String createdByFrmAttIntg {get;set;}          
        public String Source {get;set;}      
        public Date createdDate {get;set;}
        public Boolean isPublic {get; set;}
        public Double Size {get;set;}
        public double AttachmentNumber {get;set;}
        // Booleans representing the RM Case related Checkboxes
        public Boolean RM1 {get;set;}
        public Boolean RM2 {get;set;}
        public Boolean RM3 {get;set;}
        public Boolean RM4 {get;set;}
        public Boolean RM5 {get;set;}
        // Boolean representing if the related Checkboxes are disabled or not
        public Boolean RM1Disabled{get;set;}
    	public Boolean RM2Disabled{get;set;}
    	public Boolean RM3Disabled{get;set;}
    	public Boolean RM4Disabled{get;set;}
    	public Boolean RM5Disabled{get;set;}
     /**************  INNER CLASS  ******************/
       
        public CaseAttachments_cls(Attachment inAtt,String inPubOrPriv)
        //public CaseAttachments_cls(Attachment inAtt,String inPubOrPriv, string attIntgCreatedByname)
        {
                att = new Attachment();
                att = inAtt;
                publicOrPrivate = inPubOrPriv;          
                if(publicOrPrivate  == 'Public'){isPublic =True;}     
                else{isPublic =False;}     
                    
                createdByFrmAttIntg = '';              
                source ='';        
                createdDate = null;
                RMcase = '';
                size = 0;
                AttachmentNumber=null;
                
        }
        
        
    }
        //End of defining the inner class
        
        //the current case Id
        public Id caseId;
        public Case currentCaseInfo;
        public List<Attachment> lst_caseAtt ;
        public List<CaseAttachments_cls> lst_caseAtt_InnerCls {get;set;}
        public map<Id,Attachment> map_IdAttachment;
        public Boolean IsPrive {get;set;}
        public map<Id,CaseAttachments_cls>  map_ID_toAttCls;
        // List of related RM Cases ordered by Created Date
        public list<RmCase__c> rmCases_list{get;set;}
        // Booleans determine if to display the RM Checkboxes or not, pending of existence of the rleated RM Case records
        public Boolean visible1 {get;set;}
	    public Boolean visible2 {get;set;}
	    public Boolean visible3 {get;set;}
	    public Boolean visible4 {get;set;}
	    public Boolean visible5 {get;set;}
	    // Strings to display the RM Case Name to the user
        public String RM1_Name{get;set;}
		public String RM2_Name{get;set;}
		public String RM3_Name{get;set;}
		public String RM4_Name{get;set;}
		public String RM5_Name{get;set;}
        
    public VF_CaseAttachments_Console(ApexPages.StandardController controller) 
    {
     	   
        //get the current Case Id
        caseId = Apexpages.currentPage().getParameters().get('Id'); 
        
        system.debug('caseId : ' + caseId);
        
        init();
        
        
    }
    
    
    
    public void init()
    {
    	list<AttachmentIntegration__c> relatedAttInteg = new list<AttachmentIntegration__c>();
        //get the relevant info from the Case record as per the Id
        if(caseId != null)
        {
           currentCaseInfo = new Case();
           currentCaseInfo = [Select c.RM_cases_count__c,c.Private_Attachments__c, c.Id From Case c 
                              where c.Id =: caseId];
                              
                              system.debug('currentCaseInfo :' + currentCaseInfo);
           
           // List of Case related RM Cases ordered by Created Date   
           rmCases_list = new list<RmCase__c>();
           rmCases_list = [SELECT Id, Name FROM RmCase__c WHERE sfcase__c =: caseId and Exist_in_Redmine__c =: false order By createdDate];
           
           system.debug('rmCases_list :' + rmCases_list);
           // List of 'Attachment Integrations' related to the current Case's attachments
           relatedAttInteg = new list<AttachmentIntegration__c>();
          
           if (rmCases_list.size() > 0)
           {    
        	   visible1 = true;
        	   RM1_Name = 'RM1 - '+ rmCases_list[0].Name;      system.debug('rmCases_list[0] :' + rmCases_list[0]);
        	   if (rmCases_list.size() > 1)
        	   {        system.debug('rmCases_list[1] :' + rmCases_list[1]);
        		    visible2 = true;
        		    RM2_Name = 'RM2 - '+ rmCases_list[1].Name;
        		    if (rmCases_list.size() > 2)
        		    {      system.debug('rmCases_list[2] :' + rmCases_list[2]);
        		 	   visible3 = true;
        		 	   RM3_Name = 'RM3 - '+ rmCases_list[2].Name;   
        		 	   
        		 	    if (rmCases_list.size() > 3)
	        		    {      system.debug('rmCases_list[3] :' + rmCases_list[3]);
	        		 	   visible4 = true;
	        		 	   RM4_Name = 'RM4 - '+ rmCases_list[3].Name;  
	        		 	   
	        		 	   if (rmCases_list.size() > 4)
		        		    {      system.debug('rmCases_list[4] :' + rmCases_list[4]);
		        		 	   visible5 = true;
		        		 	   RM5_Name = 'RM5 - '+ rmCases_list[4].Name;     
		        		    }
	        		    }  
        		    }
        	   }
           }                                         
        }
        
        //get the relevant attachment of the Case
        if(currentCaseInfo != null && currentCaseInfo.Id != null)
        {
            //body not selected as it will cause a failer in the limit size of 135 k
            //Maximum view state size limit (135KB)  can be exceeded
             lst_caseAtt = new List<Attachment>([ Select a.CreatedDate,a.ParentId, a.Name, a.IsPrivate, a.Id, a.BodyLength, a.Description, a.ContentType 
                                                 From Attachment a where   a.ParentId =: currentCaseInfo.Id OR 
                                                 a.ParentId =: currentCaseInfo.Private_Attachments__c Order by a.CreatedDate DESC]);
                     
                     
            system.debug('lst_caseAtt :' + lst_caseAtt);
            // set of Attachment Ids as Strings                          
            set<String> attachmentsIds_Set = new set<String>();
            
            for (Attachment att : lst_caseAtt)
            {
            	String attId_Str = String.valueOf(att.Id);
            	attachmentsIds_Set.add(attId_Str);
            }
            
            relatedAttInteg = [SELECT Id, Name, Attachment_Id__c, RM_Case__c FROM AttachmentIntegration__c WHERE Attachment_Id__c IN :attachmentsIds_Set];
            
            system.debug('relatedAttInteg :' + relatedAttInteg);                                       
            
            system.debug('the current Case Id is : ' + currentCaseInfo.Id);
            map_IdAttachment = new map<Id,Attachment> ();
            
            if(lst_caseAtt != null && lst_caseAtt.size() > 0)
            {
                lst_caseAtt_InnerCls = new List<CaseAttachments_cls>();
                 map_ID_toAttCls = new map<Id,CaseAttachments_cls>();              
                //get the created by from the attachment integration object
                set<ID> set_attIds = new set<ID>();
                
                
                for(Attachment att : lst_caseAtt)
                {
                    set_attIds.add(att.Id);
                }
                list<AttachmentIntegration__c> lst_attIntegrations ;
                if(set_attIds != null && set_attIds.size() > 0)
                {
                    lst_attIntegrations = new List<AttachmentIntegration__c >([ Select a.Attachment_Number__c,a.CreatedDate,a.Created_date_final__c, a.RM_Case__r.name, a.Source__c, a.Is_public__c, a.Id, a.Creator_name__c,  a.CreatedById,a.Created_by__c,a.size__c, a.Attachment_Id__c 
                                                                                From AttachmentIntegration__c a 
                                                                                where a.Attachment_Id__c IN : set_attIds and a.Source__c != 'Copy to Redmine' order by a.Attachment_Number__c]);
                }
                
                system.debug('lst_attIntegrations :' + lst_attIntegrations);
                //has the attachment ID and the created by name of the attachment integration
                map<ID,AttachmentIntegration__c> map_ID_toAttIntegration;
                if(lst_attIntegrations != null && lst_attIntegrations.size() > 0)
                {
                    map_ID_toAttIntegration = new map<ID,AttachmentIntegration__c>();
                    for(AttachmentIntegration__c attInt :lst_attIntegrations)
                    {
                        //map_ID_toCreatedBy.put(attInt.Attachment_Id__c,attInt.CreatedBy.Name);
                        map_ID_toAttIntegration.put(attInt.Attachment_Id__c,attInt);
                        system.debug('===> attInt '+attInt);
                    }
                }
                
                //end getting the attachment integration created by
                for(Attachment att : lst_caseAtt)
                {
                    map_IdAttachment.put(att.id,att);
                    system.debug('Att Name :' + att.Name );
                    if(att.ParentId == currentCaseInfo.Id)
                    {
                        CaseAttachments_cls tmp =   new CaseAttachments_cls(att,'Public');
                        
                        
                        if(map_ID_toAttIntegration != null && map_ID_toAttIntegration.size() > 0 )
                        {
                            if(map_ID_toAttIntegration.containskey(att.Id))
                            {
                                tmp.createdByFrmAttIntg =  map_ID_toAttIntegration.get(att.Id).Created_by__c;
                                tmp.createdDate  =  map_ID_toAttIntegration.get(att.Id).Created_Date_final__c.date();            
                                tmp.Source  =  map_ID_toAttIntegration.get(att.Id).source__c; 
                                tmp.RMcase = map_ID_toAttIntegration.get(att.Id).RM_case__r.name;   
                                tmp.Size  =  map_ID_toAttIntegration.get(att.Id).size__c; 
                                tmp.AttachmentNumber = map_ID_toAttIntegration.get(att.Id).Attachment_Number__c;
                                                                    
                            }
                        }
                                
                        system.debug('===>tmp' + tmp );
                        
                        lst_caseAtt_InnerCls.add(tmp);
                        map_ID_toAttCls.put(tmp.att.id,tmp);               
                    }
                    
                    if(att.ParentId == currentCaseInfo.Private_Attachments__c)
                    {
                        CaseAttachments_cls tmp =   new CaseAttachments_cls(att,'Private');
                        
                        if(map_ID_toAttIntegration != null && map_ID_toAttIntegration.size() > 0 )
                        {
                            if(map_ID_toAttIntegration.containskey(att.Id))
                            {
                                tmp.createdByFrmAttIntg =  map_ID_toAttIntegration.get(att.Id).Created_by__c;
                                tmp.createdDate  =  map_ID_toAttIntegration.get(att.Id).Created_Date_final__c.date();        
                                tmp.source  =  map_ID_toAttIntegration.get(att.Id).source__c;
                                tmp.size  =  map_ID_toAttIntegration.get(att.Id).size__c;
                                tmp.RMcase = map_ID_toAttIntegration.get(att.Id).RM_case__r.name; 
                                tmp.AttachmentNumber = map_ID_toAttIntegration.get(att.Id).Attachment_Number__c;                         
                            }
                        }
                        system.debug('===>tmp' + tmp );
                        
                        lst_caseAtt_InnerCls.add(tmp);
                        
                        map_ID_toAttCls.put(tmp.att.id,tmp);                           
                    }
                }
                
                if (!lst_caseAtt_InnerCls.isEmpty() && !relatedAttInteg.isEmpty())
                {
	                for (CaseAttachments_cls caseAtt : lst_caseAtt_InnerCls)
	                {
	                	for (AttachmentIntegration__c attInt : relatedAttInteg)
	                	{
	                		if (attInt.Attachment_Id__c.EqualsIgnoreCase(caseAtt.att.Id))
	                		{  
	                			 system.debug('elad 238');
	                			 
	                			if (rmCases_list.size() > 0 && attInt.RM_Case__c == rmCases_list[0].Id)
	                			{
	                				caseAtt.RM1 = true;
	                				caseAtt.RM1Disabled = true;
	                			}
	                			
	                			else if (rmCases_list.size() > 1 && attInt.RM_Case__c == rmCases_list[1].Id)
	                			{
	                				caseAtt.RM2 = true;
	                				caseAtt.RM2Disabled = true;
	                			}
	                			
	                			else if (rmCases_list.size() > 2 && attInt.RM_Case__c == rmCases_list[2].Id)
	                			{
	                				caseAtt.RM3 = true;
	                				caseAtt.RM3Disabled = true;
	                			}
	                			
	                			else if (rmCases_list.size() > 3 && attInt.RM_Case__c == rmCases_list[3].Id)
	                			{
	                				caseAtt.RM4 = true;
	                				caseAtt.RM4Disabled = true;
	                			}
	                			
	                			else if (rmCases_list.size() > 4 && attInt.RM_Case__c == rmCases_list[4].Id)
	                			{
	                				caseAtt.RM5 = true;
	                				caseAtt.RM5Disabled = true;
	                			}
	                		}
	                	}
	                }
                }
            }
            else
            {
                system.debug('No related Attachment for this Case');
            }
        }
    }
    
    public pagereference  AddAttachmentPublic() 
    {
        
        
        String hostname = ApexPages.currentPage().getHeaders().get('Host');
        
        String absolutePath = 'https://' + hostname + '/apex/VF_UploadAttachment_Console?&parentId='+ currentCaseInfo.Id+'&caseId='+currentCaseInfo.Id ;
       
       
        //Pagereference p = new Pagereference('/apex/VF_uploadAttachment?&parentId='+ currentCaseInfo.Id+'&caseId='+currentCaseInfo.Id );
        Pagereference p = new Pagereference(absolutePath );
        p.setRedirect(true);
        return p;
        
        
    }
    
    public pagereference  AddAttachmentPrivate() 
    {
        
        
        system.debug('currentCaseInfo.Private_Attachments__c : ' + currentCaseInfo.Private_Attachments__c);
        if(currentCaseInfo.Private_Attachments__c != null )
        {
            Pagereference p = new Pagereference('/apex/VF_UploadAttachment_Console?&parentId='+ currentCaseInfo.Private_Attachments__c+'&caseId='+currentCaseInfo.Id );
            p.setRedirect(true);
            return p;
        }
        else
        {
            return null;
        }
        
    } 
    
    public pagereference  turnPublic() 
    {
        //Attachment toBeDeleted =  map_IdAttachment.get( ApexPages.currentPage().getParameters().get('attID'));
        
        Attachment toBeDeleted =  new Attachment();
        toBeDeleted = [ Select a.CreatedDate,a.Body ,a.ParentId, a.Name, a.IsPrivate, a.Id, a.Description, a.ContentType 
            From Attachment a 
            where   a.Id =: ApexPages.currentPage().getParameters().get('attID')];
        
        system.debug('a.Id :' + ApexPages.currentPage().getParameters().get('attID'));
        
        list<AttachmentIntegration__c>  lst_AttIntegration = new list<AttachmentIntegration__c> ([Select a.Id, a.Attachment_Id__c,a.Attachment_Number__c From AttachmentIntegration__c a where a.Attachment_Id__c =:toBeDeleted.Id and a.Source__c != 'Copy to Redmine']);   
        
            
            
        Attachment newAtt = new Attachment();
        newAtt.Body = toBeDeleted.Body;
        newAtt.ParentId =  currentCaseInfo.Id;
        newAtt.ContentType = toBeDeleted.ContentType;
        newAtt.Description = toBeDeleted.Description;
        newAtt.IsPrivate = false;
        newAtt.Name = toBeDeleted.Name;
        system.debug('Elad inside 340 turnPublic');
        insert newAtt;
        
        if(lst_AttIntegration != null && lst_AttIntegration.size() > 0)
        {
            for(AttachmentIntegration__c attInt :lst_AttIntegration)
            {
                attInt.Attachment_Id__c = newAtt.Id;
            }
            update lst_AttIntegration;
        }
        
        
        delete toBeDeleted;
        
        init();
        
        return null;
    }
    
    public pagereference  turnPrivate() 
    {
        //get the Attachment ID, using it get the related attachmnet integrations of it and update them to point to the new attachment
        
        //Attachment toBeDeleted =  map_IdAttachment.get( ApexPages.currentPage().getParameters().get('attID'));
        
   
        //need to get the body of the attachment as we did not get it in the constructor due to the limitation
        Attachment toBeDeleted =  new Attachment();
        
        system.debug('Test J what is the attID: ' +  ApexPages.currentPage().getParameters().get('attID'));
            
        
        toBeDeleted = [ Select a.CreatedDate,a.Body ,a.ParentId, a.Name, a.IsPrivate, a.Id, a.Description, a.ContentType 
            From Attachment a 
            where   a.Id =: ApexPages.currentPage().getParameters().get('attID')];
        
        
        list<AttachmentIntegration__c>  lst_AttIntegration = new list<AttachmentIntegration__c> ([Select a.Id, a.Attachment_Id__c From AttachmentIntegration__c a where a.Attachment_Id__c =:toBeDeleted.Id and a.Source__c != 'Copy to Redmine']);   
        
            
        Attachment newAtt = new Attachment();
        newAtt.Body = toBeDeleted.Body;
        newAtt.ParentId =  currentCaseInfo.Private_Attachments__c;
        newAtt.ContentType = toBeDeleted.ContentType;
        newAtt.Description = toBeDeleted.Description;
        newAtt.IsPrivate = true;
        newAtt.Name = toBeDeleted.Name;
         system.debug('Elad inside 340 turnPrivate');
        insert newAtt;
        
        if(lst_AttIntegration != null && lst_AttIntegration.size() > 0)
        {
            for(AttachmentIntegration__c attInt :lst_AttIntegration)
            {
                attInt.Attachment_Id__c = newAtt.Id;
            }
            update lst_AttIntegration;
        }
        delete toBeDeleted;
        
        init();
        
        return null;
    }           
       
    public  pagereference turnPublicPrivate()            
    {
        ID attId = ApexPages.currentPage().getParameters().get('attID');
        system.debug('attId :' + attId);
        
        if(map_ID_toAttCls.get(attId ).isPublic == true)             
        {
            return turnPublic();
        }
        else                      
        {
            return turnPrivate();
        }
                     
    }   
                                                        
    public pagereference  deleteAction() 
    {
        Attachment toDelete = new Attachment(Id = ApexPages.currentPage().getParameters().get('attID'));
       
		 try
        {
        	delete toDelete ;
        }
        
        catch (Exception e)
        {
        	e.getMessage();
        }
        
        init();
        return null;
    }
    
    
   // This method will call a method that will create a Case's Attachment related Attachment Integration with all The relevant fields
    public void createAttachmentIntegrationRM()    
    {
    	Integer rmNumber = Integer.valueOf(ApexPages.currentPage().getParameters().get('rmNumber'));
    	Id attId = createAttachmentIntegration(rmNumber);
    	
    	
    	for (CaseAttachments_cls caseAtt : lst_caseAtt_InnerCls)
    	{
    		if (caseAtt.att.Id == attId)
    		{
    			if (rmNumber == 0)
    				caseAtt.RM1Disabled = true;
    			else if (rmNumber == 1)
    				caseAtt.RM2Disabled = true;
    			else if (rmNumber == 2)
    				caseAtt.RM3Disabled = true;
				else if (rmNumber == 3)
    				caseAtt.RM4Disabled = true;
				else if (rmNumber == 4)
    				caseAtt.RM5Disabled = true;
    		}
    	}
    }
    
    // if the Attachment is private (Heads related to a Private Attachment record and not directly to the Case) 
    private Boolean isPrivateAttachment(Attachment att)
    {
 		String attIs_Str = String.valueOf(att.ParentId);
 		
 		if (attIs_Str.startsWith('500'))
 			return false;
 		
 		return true;
    }
    
    private AttachmentIntegration__c prepareAttachmentIntegration(Attachment att, Case c, Integer rmNumber)
    {
    	AttachmentIntegration__c attInteg = new AttachmentIntegration__c(); 

    	String attId_Str = String.valueOf(att.Id);
    	attInteg.Attachment_Id__c = attId_Str;
    	attInteg.Name = att.Name;
    	attInteg.Case_Id__c = c.Id;
    	attInteg.Source__c = 'copy to Redmine';
    	attInteg.sync_flag__c = true;
    	attInteg.Creator_name__c = string.valueOf(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
    	attInteg.size__c = att.BodyLength;
    	attInteg.Is_Public__c = !isPrivateAttachment(att); 
    	attInteg.RM_Case__c = rmCases_list[rmNumber].Id;
    	attInteg.Attachment_Number__c = c.Next_Attachment_Integration_Number__c;
    	
    	return attInteg;
    }

   
    // This method will create a Case related 'Attachment Integration' upon user selection, it will associate it to the relevant Attachment, 
    // and to the selected RM Case. 
    public Id createAttachmentIntegration(Integer rmNumber)    
    {
    	ID attId = ApexPages.currentPage().getParameters().get('attID');
    	   	
    	// The relevant Attachment chosen by the user, that the created Attachment Integration Record Will be associated with
    	Attachment att = new Attachment();
        att = [SELECT Id, Name, bodyLength, ParentId, IsPrivate FROM Attachment WHERE Id =: attId];    	
    	
    	Case c = [SELECT Id, Next_Attachment_Integration_Number__c FROM Case WHERE Id =: caseId];
    	c.Next_Attachment_Integration_Number__c = (c.Next_Attachment_Integration_Number__c != null ? c.Next_Attachment_Integration_Number__c + 1 : 1);
        update c; 
           	
    	try
    	{
    		AttachmentIntegration__c attInteg = prepareAttachmentIntegration(att, c, rmNumber);  system.debug('elad inside createAttachmentIntegration');
    		insert attInteg;
    	}
    	
    	 catch (DMLException e) 
        {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
              return null;
        } 
    	
    	return attId;	
    }

}