global class ScheduleMoveAttchFromProGPSToProject implements Schedulable
{
	global void execute(SchedulableContext SC) 
	{
		set<Id> set_GPSProjectId = new set<Id>();
		map<Id,Id> map_GPSProTOMProject = new map<Id,Id>();
		list<Attachment> lst_AtachmentsToUpdate = new list <Attachment>();
		
		for(Milestone1_Project__c mp: [Select m.Id, m.GPS_Project_ID__c From Milestone1_Project__c m])
		{
			if(mp.GPS_Project_ID__c != null)
			{
				set_GPSProjectId.add(mp.GPS_Project_ID__c);
				map_GPSProTOMProject.put(mp.GPS_Project_ID__c,mp.Id);
			}
		}
		system.debug('==>set_GPSProjectId : '+set_GPSProjectId);
		system.debug('==>map_GPSProTOMProject : '+map_GPSProTOMProject);
		for(Attachment att: [Select a.SystemModstamp, a.ParentId, a.Owner.IsActive, a.OwnerId, a.Name, a.LastModifiedDate, a.LastModifiedById, a.IsPrivate, a.IsDeleted, a.Id, a.Description, 
									a.CreatedDate, a.CreatedById, a.ContentType, a.BodyLength, a.Body From Attachment a where a.ParentId IN:set_GPSProjectId])
		{ 
			if(map_GPSProTOMProject.get(att.ParentId) != null)
			{
				//Attachment tempAtt = new Attachment(OwnerId= att.OwnerId,Name=att.Name,IsPrivate=att.IsPrivate,Description=att.Description,ContentType=att.ContentType,Body=att.Body);
				//tempAtt.ParentId = map_GPSProTOMProject.get(att.ParentId);
				system.debug('==>map_GPSProTOMProject.get(att.ParentId) '+map_GPSProTOMProject.get(att.ParentId));
				Attachment tempAtt = New Attachment(Name = att.name, body = att.body,IsPrivate = att.IsPrivate ,ContentType =att.ContentType);
				tempAtt.parentID = map_GPSProTOMProject.get(att.ParentId);
				if(att.Owner.IsActive == true)
					tempAtt.OwnerId = att.OwnerId ;
				lst_AtachmentsToUpdate.add(tempAtt);
			}
		}
		system.debug('==>lst_AtachmentsToUpdate : '+lst_AtachmentsToUpdate);
		if(!lst_AtachmentsToUpdate.isempty())
		{
			insert lst_AtachmentsToUpdate;
		}
	}
}