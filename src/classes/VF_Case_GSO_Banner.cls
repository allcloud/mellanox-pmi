public class VF_Case_GSO_Banner {
    
    public Case theCase {get;set;}
    public boolean is_SystemCase {get;set;}
    
    public VF_Case_GSO_Banner(ApexPages.standardController controller)
    {
        is_SystemCase = false;
        Id CaseId = controller.getId();
        
        theCase = [SELECT id, Case_Type__c FROM Case WHERE Id =: CaseId];
        
        if ( theCase.Case_Type__c == 'System Case' )  {
            
            is_SystemCase = true;
        }
    }
}