public with sharing class Category_Sync_Handler {
    
    private map<String, FAQ__kav> FAQs_Map = new map<String, FAQ__kav>();  
    private map<String, HowTo__kav> HowTos_Map  = new map<String, HowTo__kav>(); 
    private map<String, CRA__kav> CRAs_Map = new map<String, CRA__kav>(); 
    private map<String, Common_Errors__kav> CommonErrors_Map = new map<String, Common_Errors__kav>(); 
    
    
    // this method will create a 'FAQ__DataCategorySelection' with the relevant fields values 
    // pending on the SharePoint Id upon creation of new 'Category_Sync__c', it will then delete the created 'Category_Sync__c'
    public void assignCategoriesBySharePointId(list<Category_Sync__c> newCategorySync_List) {
        
        set<String> SPID2FAQ_Set = new set<String>();
        set<String> SPID2HowTo_Set = new set<String>();
        set<String> SPID2CRA_Set = new set<String>();
        set<String> SPID2CommonError_Set = new set<String>();
        list<SObject> objectsList_To_Insert = new list<SObject>();
                
        fillUpVariousSets(SPID2FAQ_Set, SPID2HowTo_Set, SPID2CRA_Set, SPID2CommonError_Set, newCategorySync_List);
        fillUpVariousLists(SPID2FAQ_Set, SPID2HowTo_Set, SPID2CRA_Set, SPID2CommonError_Set);
                
        for (Category_Sync__c catSync : newCategorySync_List) {
        
            if (FAQs_Map != null && !FAQs_Map.isEmpty()) {  
                
                FAQ__kav faq = FAQs_Map.get(catSync.SP_Article_Id__c);
                
                if (faq != null) {                   
                    
                    system.debug('faq : ' + faq);
                    FAQ__DataCategorySelection faqCategory = new FAQ__DataCategorySelection();
                    faqCategory.DataCategoryName = catSync.DATACATEGORYNAME__c;
                    faqCategory.DataCategoryGroupName = catSync.DATACATEGORYGROUPNAME__c;
                    faqCategory.ParentId = faq.Id;
                    objectsList_To_Insert.add(faqCategory);
                }
            } 
            
            
                if (HowTos_Map != null && !HowTos_Map.isEmpty()) { 
                
                    HowTo__kav howTo = HowTos_Map.get(catSync.SP_Article_Id__c);
                        
                    if (howTo != null) {    
                        
                        HowTo__DataCategorySelection  howToCategory = new HowTo__DataCategorySelection();
                        howToCategory.DataCategoryName = catSync.DATACATEGORYNAME__c;
                        howToCategory.DataCategoryGroupName = catSync.DATACATEGORYGROUPNAME__c;
                        howToCategory.ParentId = howTo.Id;
                        objectsList_To_Insert.add(howToCategory);
                        
                    } 
                }
            
            
            if (CRAs_Map != null && !CRAs_Map.isEmpty()) {
                
                CRA__kav cra = CRAs_Map.get(catSync.SP_Article_Id__c);
                    
                if (cra != null) {
                    
                    CRA__DataCategorySelection  CRACategory = new CRA__DataCategorySelection();
                    CRACategory.DataCategoryName = catSync.DATACATEGORYNAME__c;
                    CRACategory.DataCategoryGroupName = catSync.DATACATEGORYGROUPNAME__c;
                    CRACategory.ParentId = cra.Id;
                    objectsList_To_Insert.add(CRACategory);
                }
            }
            
            if (CommonErrors_Map != null && !CommonErrors_Map.isEmpty()) {
                
                Common_Errors__kav commonError = CommonErrors_Map.get(catSync.SP_Article_Id__c);
                    
                if (commonError != null) {
                    
                    Common_Errors__DataCategorySelection  CErrorsCategory = new Common_Errors__DataCategorySelection();
                    CErrorsCategory.DataCategoryName = catSync.DATACATEGORYNAME__c;
                    CErrorsCategory.DataCategoryGroupName = catSync.DATACATEGORYGROUPNAME__c;
                    CErrorsCategory.ParentId = commonError.Id;
                    objectsList_To_Insert.add(CErrorsCategory);
                }
            }
        }
        
        set<Category_Sync__c> category_SyncToUpdate_Set = new set<Category_Sync__c>();
        list<Category_Sync__c> category_SyncToDelete_List = new list<Category_Sync__c>();
        
        if (!objectsList_To_Insert.isEmpty()) {
            
            system.debug('objectsList_To_Insert : ' + objectsList_To_Insert);
            system.debug('objectsList_To_Insert size : ' + objectsList_To_Insert.size());
            
            for (SObject obj : objectsList_To_Insert) {
            
                Database.SaveResult sr = Database.insert(obj, false);
                
                if(!sr.isSuccess()) {
                    
                    system.debug('Error inserting  : ' + sr.getErrors());
                    system.debug('Error inserting Id : ' + sr.getId());
                }
                
                else {
                    
                    system.debug('obj  : ' + obj);
                    
                    String parentId = (String)obj.get('ParentId');
                    
                    for (Category_Sync__c catSync : newCategorySync_List) {  
                    
                        FAQ__kav faq = FAQs_Map.get(catSync.SP_Article_Id__c);
                        
                        if (faq != null && faq.SharePoint_ID__c != null && faq.SharePoint_ID__c.EqualsIgnoreCase(catSync.SP_Article_Id__c)
                            && faq.Id == parentId) {
                            
                            Category_Sync__c catSyncUpdate = new Category_Sync__c();
                            catSyncUpdate.Id = catSync.Id;
                            catSyncUpdate.sync__c = true;
                            category_SyncToUpdate_Set.add(catSyncUpdate);
                        }
                    
                    
                        HowTo__kav howTo = HowTos_Map.get(catSync.SP_Article_Id__c);
                        
                        if (howTo != null && howTo.SharePoint_ID__c != null && howTo.SharePoint_ID__c.EqualsIgnoreCase(catSync.SP_Article_Id__c)
                            && howTo.Id == parentId) {
                            
                            Category_Sync__c catSyncUpdate = new Category_Sync__c();
                            catSyncUpdate.Id = catSync.Id;
                            catSyncUpdate.sync__c = true;
                            category_SyncToUpdate_Set.add(catSyncUpdate);
                        }
                    
                    
                        CRA__kav cra = CRAs_Map.get(catSync.SP_Article_Id__c);
                        
                        if (cra != null && cra.SharePoint_ID__c != null && cra.SharePoint_ID__c.EqualsIgnoreCase(catSync.SP_Article_Id__c)
                            && cra.Id == parentId) {
                            
                            Category_Sync__c catSyncUpdate = new Category_Sync__c();
                            catSyncUpdate.Id = catSync.Id;
                            catSyncUpdate.sync__c = true;
                            category_SyncToUpdate_Set.add(catSyncUpdate);
                        }
                    
                    
                        Common_Errors__kav error = CommonErrors_Map.get(catSync.SP_Article_Id__c);
                        
                        if (error != null && error.SharePoint_ID__c != null &&  error.SharePoint_ID__c.EqualsIgnoreCase(catSync.SP_Article_Id__c)
                            && error.Id == parentId) {
                            
                            Category_Sync__c catSyncUpdate = new Category_Sync__c();
                            catSyncUpdate.Id = catSync.Id;
                            catSyncUpdate.sync__c = true;
                            category_SyncToUpdate_Set.add(catSyncUpdate);
                        }
                    }
                }
            }
            
            if (!category_SyncToUpdate_Set.isEmpty()) {
                
                category_SyncToDelete_List.addAll(category_SyncToUpdate_Set);
                update category_SyncToDelete_List;
            }
        }
    }
        
    
    private FAQ__DataCategorySelection createFAQDataCategory(Sobject obj) {
                    
        FAQ__DataCategorySelection faqCategory = new FAQ__DataCategorySelection();
        faqCategory.ParentId = obj.Id;
        
        return faqCategory;
    }
    
    // This method will full up the different sets with the relevant SharePoint Id 
    private void fillUpVariousSets(set<String> SPID2FAQ_Set, set<String> SPID2HowTo_Set, set<String> SPID2CRA_Set, set<String> SPID2CommonError_Set, list<Category_Sync__c> newCategorySync_List) {
        
        for (Category_Sync__c catSync : newCategorySync_List) {
            
            if (catSync.Article_Type__c != null ) {
            
                if (catSync.Article_Type__c.EqualsIgnoreCase('FAQ') ) {
                    
                    SPID2FAQ_Set.add(catSync.SP_Article_Id__c);
                }
                
                else if ( catSync.Article_Type__c.EqualsIgnoreCase('HowTo') ) {
                    
                    SPID2HowTo_Set.add(catSync.SP_Article_Id__c);
                }
                
                else if ( catSync.Article_Type__c.EqualsIgnoreCase('CRA') ) {
                    
                    SPID2CRA_Set.add(catSync.SP_Article_Id__c);
                }
                
                else if ( catSync.Article_Type__c.EqualsIgnoreCase('Common Error') ) {
                    
                    SPID2CommonError_Set.add(catSync.SP_Article_Id__c);
                }
            } 
        }
    }
    
    // This method will full up the different lists with the relevant records pending on Sharepoint Ids
    private void fillUpVariousLists(set<String> SPID2FAQ_Set, set<String> SPID2HowTo_Set, set<String> SPID2CRA_Set, set<String> SPID2CommonError_Set) {
        
        if (!SPID2FAQ_Set.isEmpty()) {
            
            list<FAQ__kav> FAQs_List_Draft = [SELECT Id, SharePoint_ID__c FROM FAQ__kav WHERE SharePoint_ID__c IN : SPID2FAQ_Set and PublishStatus = 'Draft' and Language = 'en_US'];
            list<FAQ__kav> FAQs_List = [SELECT Id, SharePoint_ID__c FROM FAQ__kav WHERE SharePoint_ID__c IN : SPID2FAQ_Set and PublishStatus = 'Online' and Language = 'en_US'];
            
            if (!FAQs_List_Draft.isEmpty()) {
                
                FAQs_List.addAll(FAQs_List_Draft);
            }
            
            if (!FAQs_List.isEmpty()) {
                for (FAQ__kav faq : FAQs_List) {
                    
                    FAQs_Map.put(faq.SharePoint_ID__c, faq);
                }
            }
        }
        
        if (!SPID2HowTo_Set.isEmpty()) {
            
            list<HowTo__kav> HowTos_List_Draft = [SELECT Id, SharePoint_ID__c FROM HowTo__kav WHERE SharePoint_ID__c IN : SPID2HowTo_Set and PublishStatus = 'Draft' and Language = 'en_US'];
            list<HowTo__kav> HowTos_List = [SELECT Id, SharePoint_ID__c FROM HowTo__kav WHERE SharePoint_ID__c IN : SPID2HowTo_Set and PublishStatus = 'Online' and Language = 'en_US'];
            
            if (!HowTos_List_Draft.isEmpty()) {
                
                HowTos_List.addAll(HowTos_List_Draft);
            }
            
            if (!HowTos_List.isEmpty()) {
            
                for (HowTo__kav howTo : HowTos_List) {
                    
                    HowTos_Map.put(howTo.SharePoint_ID__c, howTo);
                }
            }
        }
        
        if (!SPID2CRA_Set.isEmpty()) {
            
            list<CRA__kav> CRAs_List_Draft = [SELECT Id, SharePoint_ID__c FROM CRA__kav WHERE SharePoint_ID__c IN : SPID2CRA_Set and PublishStatus = 'Draft' and Language = 'en_US'];
            list<CRA__kav> CRAs_List = [SELECT Id, SharePoint_ID__c FROM CRA__kav WHERE SharePoint_ID__c IN : SPID2CRA_Set and PublishStatus = 'Online' and Language = 'en_US'];
            
            if (!CRAs_List_Draft.isEmpty()) {
                
                CRAs_List.addAll(CRAs_List_Draft);
            }
            
            if (!CRAs_List.isEmpty()) {
            
                for (CRA__kav cra : CRAs_List) {
                    
                    CRAs_Map.put(cra.SharePoint_ID__c, cra);
                }
            }
        }
        
        if (!SPID2CommonError_Set.isEmpty()) {
            
            list<Common_Errors__kav> CommonErrors_List_Draft = [SELECT Id, SharePoint_ID__c FROM Common_Errors__kav WHERE SharePoint_ID__c IN : SPID2CommonError_Set and PublishStatus = 'Draft' and Language = 'en_US'];
            list<Common_Errors__kav> CommonErrors_List = [SELECT Id, SharePoint_ID__c FROM Common_Errors__kav WHERE SharePoint_ID__c IN : SPID2CommonError_Set and PublishStatus = 'Online' and Language = 'en_US'];
        
            if (!CommonErrors_List_Draft.isEmpty()) {
                
                CommonErrors_List.addAll(CommonErrors_List_Draft);
            }
            
            if (!CommonErrors_List.isEmpty()) {
            
                for (Common_Errors__kav error : CommonErrors_List) {
                    
                    CommonErrors_Map.put(error.SharePoint_ID__c, error);
                }
            }
        }
        
        system.debug('FAQs_Map : ' + FAQs_Map);
        system.debug('HowTos_Map : ' + HowTos_Map);
        system.debug('CRAs_Map : ' + CRAs_Map);
        system.debug('CommonErrors_Map : ' + CommonErrors_Map);     
    } 
}