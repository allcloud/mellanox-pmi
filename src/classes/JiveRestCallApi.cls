@RestResource(urlMapping = '/updateSFCaseStatus/*')
global with sharing class JiveRestCallApi {
    global Class JSONfromJive {
        Public String ContentID;
        Public String Status;
    }
@HttpPost
global static String doPost() {
     Map<String,list<String>> SFResponse = new Map<String,list<String>>();
    List<String> tempContentIdList=new List<String>();
    List<String> tempList=new List<String>();
    List < Case > updateCaseStatus = new List < Case > ();
    List < Case > caseObjList = new List < Case > ();
    Map<String,String> caseMap = new Map<String,String>();
    try {
    System.debug('In DOPOST Method--->');
      if(!Test.isRunningTest()){
    RestRequest request = RestContext.request;
    RestResponse response = RestContext.response;

    list < JSONfromJive > JSONJive = (list < JSONfromJive > ) JSON.deserialize(request.requestBody.toString(), list < JSONfromJive > .class);
      
   

    
        For(JSONfromJive J: JSONJive) {
            system.debug(J.ContentID);
            system.debug(J.Status);
            caseMap.put(J.ContentID,J.Status);
           tempContentIdList.add(J.ContentID);           
        }
         caseObjList = [select id, Content_ID__c, status from Case where Content_ID__c in: tempContentIdList];                
         for (Case caseObj: caseObjList) {
                caseObj.status = caseMap.get(caseObj.Content_ID__c);
                updateCaseStatus.add(caseObj);                   
                tempList.Add(caseObj.Content_ID__c);
            }
            update updateCaseStatus;  
        SFResponse.put('contentID',tempList);
    }
    } catch (Exception e) {
        System.debug('**************Exception**************' + e);
    }
    return JSON.serialize(SFResponse);
    }
}