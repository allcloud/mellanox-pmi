/*****       This class is a controller class that will determeine if the message 
                   that is on the related VF will be displayed on the Contact Layout    *****/

public with sharing class VF_Contact_already_has_System_access {
	public Contact con{get;set;}
	//Boolean that determines if the output will be displayed on tha page 
	public boolean showPage{get;set;}
	
	// Constructor
	public VF_Contact_already_has_System_access(ApexPages.StandardController controller)
	{
		con = (Contact)controller.getRecord();
		Contact DBCon = [SELECT Id, access_required__c, Portal_Access__c FROM Contact WHERE Id =: con.Id];
		
		if (DBCon.access_required__c != null &&  DBCon.Portal_Access__c != null)
		{
			if (DBCon.access_required__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer') 
			&& DBCon.Portal_Access__c.EqualsIgnoreCase('Mellanox System Customer'))
			{
				showPage = true;
			}
			
			else
			{
				showPage = false;
			}
		}
	}

}