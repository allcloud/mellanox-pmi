public class Quota_Calculation {
	public Quota_Calculation(){}
	
	public pagereference init(){
		runCalc();
		PageReference pg = new PageReference('/00O50000003Ja0d');
        pg.setRedirect(true);
        return pg;
	}
	public static void runCalc() {
		//List<Period> periods = new List<Period>([select StartDate, EndDate from Period where Type='Quarter' and EndDate >= :System.today() order by EndDate limit 1]);
		List<Quota_Period__c> quota_periods = new List<Quota_Period__c>([select Start_Date__c, End_Date__c from Quota_Period__c where Current_Quarter__c=true limit 1]);
		// Hold map all Sales Guys to their number Opty Actuals
		Map<ID,Double> map_Salesguy_OptyActuals = new Map<ID,Double>();
		// Map Region to number Opty Actuals
		Map	<String,Double> map_Region_OptyActuals = new Map <String,Double>();
		//Map Region to Sales Directors
		Map<String,ID> map_region_SalesDirector = new Map<String,ID>();
		map_region_SalesDirector.put('BIZ','005500000015ZVpAAM'); //Amir P
		map_region_SalesDirector.put('Americas','005500000015ZVrAAM'); //Peter W
		
		map_region_SalesDirector.put('ASI','00550000001t0V4AAI'); //Gil B
		map_region_SalesDirector.put('JPN','00550000001t0V4AAI'); //Gil B 
		map_region_SalesDirector.put('CTK','00550000001t0V4AAI'); // Gil B
		map_region_SalesDirector.put('SEA','00550000001t0V4AAI'); //= Gil B 
		
		map_region_SalesDirector.put('EUR','0055000000152snAAA'); // Yossi Avni
		
		map_region_SalesDirector.put('MFS','00550000001s3jfAAA'); // Dale D’Alessio
		map_region_SalesDirector.put('VAR','005500000011h1EAAQ'); // Darrin Chen
		map_region_SalesDirector.put('IPT','00550000001t25mAAA'); // Steen G
		map_region_SalesDirector.put('OTH','005500000011cs7AAA'); // Marc S
		//Chuck Tybur
		map_region_SalesDirector.put('DELL','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('HP','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('IBM','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('KOT','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('USC','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('USE','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('USF','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('USS','005500000013P66AAE'); // Chuck T
		map_region_SalesDirector.put('USW','005500000013P66AAE'); // Chuck T
		//Date used to run Previous Quarter
		
		List<Quota__c> lst_quota = new List<Quota__c>([Select id,Quota_Period_1__r.Current_Quarter__c,Opportunity_Actuals_Before_Adjustment__c,
														Opportunity_Committed__c,Opportunity_Actuals__c,Revenue_Actuals__c,
														Revenue_TOGO__c,Revenue_Actuals_Before_Adjustment__c,Revenue_Quota__c,Opportunity_Quota__c,
		 												Employee_Name__c,Opportunity_Adjustment__c,Revenue_Adjustment__c,
		 												Employee_Name_text__c
		 												From Quota__c where Quota_Period_1__r.Current_Quarter__c=true and Employee_Name__c != null ]);

		if(lst_quota==null || lst_quota.size()==0)
			return;
		if(quota_periods==null || quota_periods.size()==0)
			return;
			
		Map<ID,Quota__c> map_userID_quota = new Map<ID,Quota__c>();
		for (Quota__c q : lst_quota){
			map_userID_quota.put(q.Employee_Name__c,q);
		} 	
		for (Quota__c q : map_userID_quota.values()){
			q.Opportunity_Actuals_Before_Adjustment__c = 0;
			q.Revenue_Actuals_Before_Adjustment__c = 0;
			q.Opportunity_Committed__c = 0;	
			q.Revenue_TOGO__c = 0;
			q.Revenue_Adjustment__c = 0;
			q.Opportunity_Adjustment__c = 0;
		}	 
		
		List<Opportunity> lst_optys = new List<Opportunity>([select id,CloseDate,StageName,Sales_Credit_Amount__c,Amount,Sub_Channel__c,
												OwnerID,Owner.Name,BizDev_Opp__c,BizDev_Rep__c from Opportunity
												where 
												//Required_Ship_Date__c >= :prev_quarter_start and Required_Ship_Date__c <= :prev_quarter_end
												Record_type_ID__c !='01250000000DOn2'
												and Required_Ship_Date__c >= :quota_periods[0].Start_Date__c and Required_Ship_Date__c <= :quota_periods[0].End_Date__c
												and Sales_Credit_Amount__c!=null and Sales_Credit_Amount__c>0 
												and (StageName = 'Close Won' OR StageName = 'Closed Won' OR StageName='Commit') ]);
		for (Opportunity o : lst_optys){
			if(o.StageName=='Close Won' || o.StageName=='Closed Won'){
				//Sum Region
				if(map_region_SalesDirector.containsKey(o.Sub_Channel__c)){
					if(map_region_SalesDirector.get(o.Sub_Channel__c)!=null && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c))!=null)
					{		
						if(map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c>0)				
							map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Actuals_Before_Adjustment__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
					}						
				}
				//Take care BixDev case
				if (o.BizDev_Opp__c==true && o.Sub_Channel__c != 'BIZ'){
					if(map_userID_quota.get(map_region_SalesDirector.get('BIZ')).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get('BIZ')).Opportunity_Quota__c>0)				
							map_userID_quota.get(map_region_SalesDirector.get('BIZ')).Opportunity_Actuals_Before_Adjustment__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
				}
			}
			else { // Calculte Commit deals
				//Sum Region
				if(map_region_SalesDirector.containsKey(o.Sub_Channel__c)){
					if(map_region_SalesDirector.get(o.Sub_Channel__c)!=null && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c))!=null)
					{
						if(map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c>0)
							map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Committed__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
					}						
				}
				//Take care BixDev case ?
				if (o.BizDev_Opp__c==true && o.Sub_Channel__c != 'BIZ'){
					if(map_userID_quota.get(map_region_SalesDirector.get('BIZ')).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get('BIZ')).Opportunity_Quota__c>0)				
							map_userID_quota.get(map_region_SalesDirector.get('BIZ')).Opportunity_Committed__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
				}	
			}				
		}	
		
		//Update REV Actuals
		Double total_rev_TOGO = 0;
		Double total_bbb = 0;
		//Double total_adj = 0;
		List<Opportunity> lst_optys_TOGO = new List<Opportunity>([select id,Amount,Opportunity_VT_Region__c,VT__c
																from Opportunity
																where RecordTypeid='01250000000Dw1p'
																and 
																//CloseDate >= :prev_quarter_start and CloseDate <= :prev_quarter_end
																CloseDate >= :quota_periods[0].Start_Date__c and CloseDate <= :quota_periods[0].End_Date__c
																]);
		List<BBB_Orders__c> orders = new List<BBB_Orders__c>([select id,Ordered_Item__c,Selling_Amount__c,S__c,VT__c,VT_Map__c,VT_Map__r.Name,
																VT1_COMISSION__c,VT1_PERCENTAGE__c,VT2_COMISSION__c,VT2_PERCENTAGE__c,Customer_Parent__c 
																from BBB_Orders__c 
																where 
																//BBBSDT__c >= :prev_quarter_start and BBBSDT__c <= :prev_quarter_end
																BBBSDT__c >= :quota_periods[0].Start_Date__c and BBBSDT__c <= :quota_periods[0].End_Date__c
																and Dist_Order_type__c != 'Deferred' 
																and (S__c='BLS' or S__c='BxLog' or S__c='BxLOG')
																//test
																//and VT1_COMISSION__c='VAR-EAST'
																//and Customer_Parent__c = 'DELL'
																order by Customer_Parent__c,ID
																]);
		
		Map<String,ID> map_VT_SalesDirector = new Map<String,ID>();
		Map<String,String> map_VTMap_VT = new Map<String,String>(); 
		List<VT_Map__c> lst_vtmap = new List<VT_Map__c>([select Name,Sales_Director__c,VT__c from VT_Map__c]);
		for (VT_Map__c v : lst_vtmap){
			map_VT_SalesDirector.put(v.Name, v.Sales_Director__c);	
			//if (v.Name != v.VT__c){
				map_VTMap_VT.put(v.Name,v.VT__c);
			//}
		}															
		//Calculate TOGO Revenue
		for (Opportunity o : lst_optys_TOGO){
			total_rev_TOGO += o.Amount!=null ? o.Amount : 0;
			if(o.VT__c!=null){
				if(map_region_SalesDirector.get(o.VT__c)!=null
					&& map_userID_quota.get(map_region_SalesDirector.get(o.VT__c))!=null)	
				{
						if(map_userID_quota.get(map_region_SalesDirector.get(o.VT__c)).Revenue_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(o.VT__c)).Revenue_Quota__c>0)
							map_userID_quota.get(map_region_SalesDirector.get(o.VT__c)).Revenue_TOGO__c += o.Amount!=null ? o.Amount : 0;	
				}
			}
		}
		//	
		Double VT1_sales, VT2_sales;
		for (BBB_Orders__c bbb : orders){
//map_region_SalesDirector			
			VT1_sales = VT2_sales = 0;
			total_bbb += bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c : 0;
			if(bbb.VT1_PERCENTAGE__c==null && bbb.VT2_PERCENTAGE__c==null){
				if(bbb.VT__c != null && map_region_SalesDirector.get(bbb.VT__c) != null && map_userID_quota.get(map_region_SalesDirector.get(bbb.VT__c)) != null)
					map_userID_quota.get(map_region_SalesDirector.get(bbb.VT__c)).Revenue_Actuals_Before_Adjustment__c += bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c : 0;
			}	
			else {
				if(bbb.VT1_COMISSION__c!='' && bbb.VT1_PERCENTAGE__c!=null && bbb.VT1_PERCENTAGE__c>0){
					VT1_sales = bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c * bbb.VT1_PERCENTAGE__c : 0;
					if (map_VTMap_VT.get(bbb.VT1_COMISSION__c) != null){
						if(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))!=null 
								&& map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c)))!=null)
						{								
							if(map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))).Revenue_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))).Revenue_Quota__c>0)								
								map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))).Revenue_Actuals_Before_Adjustment__c += VT1_sales;
						}							
					}
					
				}
				if(bbb.VT2_COMISSION__c!='' && bbb.VT2_PERCENTAGE__c!=null && bbb.VT2_PERCENTAGE__c>0){
					VT2_sales = bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c * bbb.VT2_PERCENTAGE__c : 0;
					/*if(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)!=null && map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c))!=null)
					{
						if(map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)).Revenue_Quota__c>0)
							map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)).Revenue_Actuals_Before_Adjustment__c += VT2_sales;
					}						
					*/							
					if (map_VTMap_VT.get(bbb.VT2_COMISSION__c) != null){
						if(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))!=null 
								&& map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c)))!=null)
						{									
							if(map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))).Revenue_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))).Revenue_Quota__c>0)
								map_userID_quota.get(map_region_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))).Revenue_Actuals_Before_Adjustment__c += VT2_sales;
						}							
					}	
				}		
			}
		}	
		//Calculate Adjustment
		//Comment out for now
		//
		List<Quota_Adjustment__c> lst_adjustments = new List<Quota_Adjustment__c>([select id,Opportunity_Actuals_Adjustment__c,Revenue_Actuals_Adjustment__c,
																						Quota__r.Employee_Name__c,Region__c,VT_Map__c
																						from Quota_Adjustment__c
																						where Quota_Period__r.Current_Quarter__c=true]);
		for (Quota_Adjustment__c adj : lst_adjustments){
			//total_adj += adj.Revenue_Actuals_Adjustment__c!=null ? adj.Revenue_Actuals_Adjustment__c : 0;
			if(adj.Opportunity_Actuals_Adjustment__c!=null){
				if(map_region_SalesDirector.get(adj.VT_Map__c) != null && map_userID_quota.get(map_region_SalesDirector.get(adj.VT_Map__c)) != null)
					map_userID_quota.get(map_region_SalesDirector.get(adj.VT_Map__c)).Opportunity_Adjustment__c += adj.Opportunity_Actuals_Adjustment__c;
				//Old logic
				/*
				map_userID_quota.get(adj.Quota__r.Employee_Name__c).Opportunity_Adjustment__c += adj.Opportunity_Actuals_Adjustment__c;
				//roll up
				if(adj.Region__c!=null && map_region_SalesDirector.get(adj.Region__c)!=null 
						&& map_region_SalesDirector.get(adj.Region__c)!=adj.Quota__r.Employee_Name__c 
						&& map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c))!=null)
				{		
					if(map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c)).Opportunity_Quota__c>0)				
						map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c)).Opportunity_Adjustment__c += adj.Opportunity_Actuals_Adjustment__c;
				}
				*/ 	
			}
			if(adj.Revenue_Actuals_Adjustment__c!=null){
				if(map_region_SalesDirector.get(adj.VT_Map__c) != null && map_userID_quota.get(map_region_SalesDirector.get(adj.VT_Map__c)) != null)
					map_userID_quota.get(map_region_SalesDirector.get(adj.VT_Map__c)).Revenue_Adjustment__c += adj.Revenue_Actuals_Adjustment__c;
			}
		}
		//
		//Handle special cases
		//Set Marc number to grand total - 005500000011cs7AAA
		if(map_userID_quota.get('005500000011cs7AAA')!=null){
			map_userID_quota.get('005500000011cs7AAA').Revenue_Actuals_Before_Adjustment__c = total_bbb;
			map_userID_quota.get('005500000011cs7AAA').Revenue_TOGO__c = total_rev_TOGO;
			map_userID_quota.get('005500000011cs7AAA').Revenue_Adjustment__c = 0;
		}
		//Calc Actuals by sum up Actuals_before_Adjustment and Adjustment
		for (Quota__c q : map_userID_quota.values()){
			q.Opportunity_Actuals__c = q.Opportunity_Actuals_Before_Adjustment__c + q.Opportunity_Adjustment__c;
			q.Revenue_Actuals__c = q.Revenue_Actuals_Before_Adjustment__c + q.Revenue_Adjustment__c;
		}
		//End Rev Actuals Calc
		if (map_userID_quota!=null && map_userID_quota.size()>0)
			update map_userID_quota.values();																		
	}
}