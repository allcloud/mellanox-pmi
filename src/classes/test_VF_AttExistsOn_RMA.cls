@isTest
public with sharing class test_VF_AttExistsOn_RMA {
	
	static testMethod void test_attExistsON_RMA()
	{
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		
		RMA__c rma = creator.CreateRMA();
		insert rma;
		
		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(rma);
        VF_AttExistsOn_RMA vf = new VF_AttExistsOn_RMA(controller);
		
		Attachment att = new Attachment();
		att.ParentId = rma.Id;
		att.Name = 'Test att';
		Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	att.body=bodyBlob;
		insert att;
		
		
		Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(rma);
        VF_AttExistsOn_RMA vf1 = new VF_AttExistsOn_RMA(controller1);
        
	}

}