@isTest
public with sharing class test_VF_Satisfaction_Survey {
	
	static testMethod void test_updateTheCase() {	
		
		Case c = new Case(priority = 'Low');
		insert c;
		
		ApexPages.currentPage().getParameters().put('Id',c.Id);
		ApexPages.currentPage().getParameters().put('sat','1');
		 
		VF_Satisfaction_Survey controller = new VF_Satisfaction_Survey();
		controller.updateTheCase();
		ApexPages.currentPage().getParameters().put('sat','2');
		controller = new VF_Satisfaction_Survey();
		controller.updateTheCase();
		ApexPages.currentPage().getParameters().put('sat','3');
		controller = new VF_Satisfaction_Survey();
		controller.updateTheCase();
		ApexPages.currentPage().getParameters().put('sat','4');
	    controller = new VF_Satisfaction_Survey();
		controller.updateTheCase();
		controller.updateCustomerComment();
	}
	
}