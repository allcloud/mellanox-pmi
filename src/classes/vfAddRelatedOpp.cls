public class vfAddRelatedOpp {
/*    
    private final Opportunity opp;          // input Opportunity object
    public String byName {get; set;}        // Search input string
    public Boolean ResultExist {get; set;}  //flag for result list//    
    public List<OpportunityResult> lst_OppDisplay {get; set;}   // Search results
    
    public vfAddRelatedOpp(ApexPages.StandardController controller) 
    {
        this.opp = (Opportunity)controller.getRecord();
        ResultExist = false;
    } 
    
    // searchOpportunities - looks for all opportunities by Name or partial name with the input string which are not related yet to an opportunity
    public void searchOpportunities()
    {
        ResultExist = false;
        lst_OppDisplay = new List <OpportunityResult>();
        
        if (byName != null && byName !='')
        {
           List <Opportunity> LstOpportunity = [Select Name, Account.Name, Likely_Winner__c, StageName, CloseDate, Related_Opportunity__c
                                                 From Opportunity
                                                 Where Name like : '%' + byName + '%'
                                                 And Related_Opportunity__c = null
                                                 And Id != : opp.Id];
                                                 
            if (LstOpportunity.size() > 0)
            {
                ResultExist = true;             

                for(Opportunity objOpp : LstOpportunity)
                {
                    OpportunityResult oppRes = new OpportunityResult(objOpp);
                    lst_OppDisplay.add(oppRes);
                }
                
            }
        }
    }

    // this function is called by the 'select' button and updated the Related_Opportunity__c field of all selected opportunities with the input opportunity.
    // Then, returns to the opportunity page. If no opportunity was selected, returns to the search results.
    public PageReference selectRelatedOpp()
    {
        List <Opportunity> selectedOpps = new List <Opportunity>();
        
        for (OpportunityResult result : lst_OppDisplay) 
        {
            if (result.selected)
            {
                result.opp.Related_Opportunity__c = this.opp.Id;
                selectedOpps.add(result.opp);
            }
        }
        
        if (selectedOpps.size() == 0)
            return null;
        else
        {   
            update selectedOpps;
        }
        
        PageReference p =  new PageReference('/'+ opp.Id);
        return p;       
    }
    
    //go back to opportunity page
    public PageReference cancel()
    {
        PageReference p =  new PageReference('/'+ opp.Id);
        return p;
    }  


    //output object result// 
    public class OpportunityResult
    {
        public Opportunity opp {get; set;}
        public Boolean selected {get; set;}
        
        public OpportunityResult(Opportunity opp)
        {
                this.opp = opp;
                this.selected = false;
        }
    }*/
}