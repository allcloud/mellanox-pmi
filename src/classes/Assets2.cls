public class  Assets2{
    public list<Asset2__c> currAsset {get;set;}
    public String orderpassed{ get; set; }
    public String partpassed { get; set; }
    ApexPages.StandardController controller;
    public Assets2() 
        {
                orderpassed= Apexpages.currentPage().getParameters().get('ordernumber');
                partpassed = Apexpages.currentPage().getParameters().get('partnumber');             
                currAsset= [select serial_number__c, part_number__c, order__c from Asset2__c where  IsDeleted = false  and Order__c=:orderpassed and part_number__c=:partpassed and isdeleted=false  ];
        }  

}