global class Schedule_Update_ERI_Approve implements Schedulable{
	
	// This metohd executes the class 'Batch_Update_ERI_Approve' with a scheduler
    global void execute(SchedulableContext SC) {
    
        Batch_Update_ERI_Approve batch = new Batch_Update_ERI_Approve();
        Database.executeBatch(batch);
   }	
}