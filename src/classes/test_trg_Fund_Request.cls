@isTest(seeAllData=true)
public class test_trg_Fund_Request {
    
    static testMethod void  test_sendFundRequestToApproval() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        acc.Name = 'test Name';
        acc.billingStreet = 'test Street';
        acc.billingCountryCode = 'US';
        acc.BillingState = 'Texas';
        acc.BillingCity = 'Houston';
        insert acc; 
        
        Contact c = new Contact();
	  	c.LastName = 'Doe';
	  	c.FirstName = 'John';
	  	c.Email = 'surveyAppUser@hotmail.com';
	  	c.AccountId = acc.Id;
	  	insert c;
        
        SFDC_MDF__c mdf = new SFDC_MDF__c();
        mdf.Account__c = acc.Id;
        mdf.Requestor_Contact__c = c.Id;
        mdf.Name = 'Test MDF';
        mdf.Amount__c = 1200;       
        mdf.Activity_Start_Date__c = date.today().addDays(20);
        mdf.Activity_End_Date__c = mdf.Activity_Start_Date__c + 10;
        mdf.Fund_Request_Type__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1000;
        mdf.Activity_Description__c = 'Advertising';
        mdf.Purpose_of_Activity__c = 'Advertising';
        mdf.Activity_Product_Focus__c = 'Advertising';
        mdf.Activity_Vertical_Market_Focus__c = 'Advertising';
        mdf.Marketing_Dollars_Requested__c = 1000;
        mdf.Target_Audience__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1500;
        mdf.Status__c = 'Draft';
        mdf.CAM__c = UserInfo.getUserId();
        mdf.Requestor_Name__c = UserInfo.getUserID();
        insert mdf;
        
        Attachment att = new Attachment();
		att.ParentId = mdf.Id;
		att.Name = 'Test att';
		Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	att.body=bodyBlob;
		insert att;
        
        test.startTest();
        
        mdf.Status__c = 'Submitted';
        
        update mdf;
        
        mdf.PO_Attachment_Id__c = att.Id;
        
        update mdf;
        
        
        
        test.stopTest();
    }
}