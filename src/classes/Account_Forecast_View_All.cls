public with sharing class Account_Forecast_View_All {
	//ID AccountID;
	public Account acct {get; set; }
	public String OPN_name {get; set;}
	Map<ID,Opportunity> map_optys = new Map<ID,Opportunity>();
	List<Opportunity> optys_enduser = new List<Opportunity>();
	List<Opportunity> optys_actuals_togo = new List<Opportunity>();
	
	//for search by OPNs
	public List<OpportunityLineItem> olines_by_search {get; set; }
	Set<ID> opty_endusers_IDs = new Set<ID>();
	public boolean display_seacrh {get; set; }
	//
	List<OpportunityLineItemSchedule> lst_schedules = new List<OpportunityLineItemSchedule>();
	List<OpportunityLineItemSchedule> tmp_schedules = new List<OpportunityLineItemSchedule>();
	Map<ID,List<OpportunityLineItemSchedule>> map_optyID_listschedules = new Map<ID,List<OpportunityLineItemSchedule>>(); 
	
	List<Period> periods = new List<Period>([select id, StartDate,EndDate from Period where Type='Quarter' and EndDate >= :System.today() order by EndDate]);
	//Set<ID> set_optyIDs = new Set<ID>();
			
	public class AllRow {
		//public boolean isOEM {get;set;}
		public String optyID { get; set; }
        public String optyname { get; set; }
        public String stage {get; set;}
        public String sales_upside { get; set; }
        //public String disty_partner {get; set;}
        
        public Double Qty_P0 { get; set; }
        public Double Qty_P1 { get; set; }
        public Double Qty_P2 { get; set; }
        public Double Qty_P3 { get; set; }
        public Double Qty_P4 { get; set; }
        public Double Qty_P5 { get; set; }
        public Double Qty_P6 { get; set; }
        public Double Qty_P7 { get; set; }
        public Double Qty_P8 { get; set; }
        public Double Qty_P9 { get; set; }
        public Double Qty_P10 { get; set; }
        public Double Qty_P11 { get; set; }
        
        public AllRow(){
        	sales_upside = 'No';
        	Qty_P0 = Qty_P1 = Qty_P2 = Qty_P3 = 0;
        	Qty_P4 = Qty_P5 = Qty_P6 = Qty_P7 = 0;
        	Qty_P8 = Qty_P9 = Qty_P10 = Qty_P11 = 0;	
        }
        void zero_out(){
        	Qty_P1 = Qty_P2 = Qty_P3 = 0;
        	Qty_P4 = Qty_P5 = Qty_P6 = Qty_P7 = 0;
        	Qty_P8 = Qty_P9 = Qty_P10 = Qty_P11 = 0;
        }
	}
	
	AllRow tmp_row;
	AllRow totalOEM_row;
	AllRow Total_InFC_row; //sum up OEM and End Users rows in FC
	AllRow totalOEM_row_notFC;
	AllRow totalEndUser_NotFC_row;
	AllRow totalEndUser_InFC_row;
	
	AllRow totalActual_row;
	AllRow totalTOGO_row;
	AllRow grandtotal_row;
	
	public List<AllRow> OEM_allrows {get; set;}
	public List<AllRow> OEM_allrows_notFC {get; set;}
	public List<AllRow> EndUser_InFC_allrows {get; set;}
	public List<AllRow> EndUser_NotFC_allrows {get; set;}
	public List<AllRow> TOTAL_allrows {get; set;}
	public List<AllRow> ActualTOGO_allrows {get; set;}
	
	public Account_Forecast_View_All (ApexPages.StandardController controller){
		display_seacrh = false;
		//olines_by_search = new List<OpportunityLineItem>();
		
		//AccountID = controller.getRecord().id;
		acct = [select id, name,type,sales_rep__c,Sales_Owner__r.Name from Account where id = :controller.getRecord().id];
		//OEM recordtype = '01250000000DOn2'
		//End User Record Type = '01250000000Dtdw';
		//Actual Opportunity Record Type = '01250000000Dtdv'
		//To-Go Opportunity Record Type = '01250000000Dw1p'
		
		map_optys = new Map<ID, Opportunity>([select id,Name,RecordTypeID,StageName,Sales_Ops_Upside__c,Forecast__c
													from Opportunity 
													where RecordTypeID = '01250000000DOn2' 
													//AND Forecast__c = 1
													AND (StageName = 'Pipeline' OR StageName = 'Best Case' OR StageName='Commit' OR StageName='Design Win') 
													AND (AccountID = :controller.getRecord().id OR Primary_Partner_N__c = :controller.getRecord().id)
													]);
		
		optys_actuals_togo = [select id,Amount,Closedate,Name from Opportunity where CloseDate >= :periods[0].StartDate
									//AND (AccountID = :controller.getRecord().id OR Primary_Partner_N__c = :controller.getRecord().id)
									AND AccountID = :controller.getRecord().id
									AND Amount != null AND Amount > 0
									AND ((RecordTypeID = '01250000000Dtdv' and Forecast__c = 1) or RecordTypeID = '01250000000Dw1p') 
									];
		
		optys_enduser = [select id,Amount,Closedate,Name,StageName,New_Stage__c,Distribution_Partner__r.Name,Sales_Ops_Upside__c,Forecast__c,
									AccountID,Primary_Partner_N__c,Customer_Parent__c 
									from Opportunity where CloseDate >= :periods[0].StartDate
									//AND (AccountID = :controller.getRecord().id OR Primary_Partner_N__c = :controller.getRecord().id)
									AND (AccountID = :controller.getRecord().id OR Customer_Parent__c = :acct.name OR Primary_Partner_N__c = :acct.ID OR Distribution_Partner__c = :acct.ID)
									AND RecordTypeID = '01250000000Dtdw'
									AND Amount != null AND Amount > 0
									AND (New_Stage__c = '4 - Forecast' OR New_Stage__c = '3 - Commit' OR New_Stage__c = '2 - Best Case' OR New_Stage__c ='1 - Pipilene')
									AND StageName != 'Closed Won'
									order by Sales_Ops_Upside__c desc,StageName desc
									];
																			
		OEM_allrows = new List<AllRow>();
		OEM_allrows_notFC = new List<AllRow>();
		EndUser_NotFC_allrows = new List<AllRow>();
		EndUser_InFC_allrows = new List<AllRow>();
		TOTAL_allrows = new List<AllRow>();
		ActualTOGO_allrows = new List<AllRow>();
	}
	Map<Integer, Integer> month_To_Quarter = new Map<Integer, Integer> 
        {1=>1, 2=>1, 3=>1, 4=>2, 5=>2, 6=>2, 7=>3, 8=>3, 9=>3, 10=>4, 11=>4, 12=>4};
	public List<String> quarters {
        
        get{
                
            List<String> ans = new List<String>();
            ans = getNext8Quarters(month_To_Quarter.get(system.today().month()), system.today().year());
            return ans;
        }
    }
    public List<String> getNext8Quarters(integer q, integer year)
    {
        
        List<String> q_list;
        Integer next1 = year + 1;
        Integer next2 = year + 2;
        Integer next3 = year + 3;
        
        if (q == 1)
        {
            q_list = new List<String> {'Q1 - ' +year,'Q2 - ' +year, 'Q3 - ' +year, 'Q4 - ' +year,
                                       'Q1 - ' +next1, 'Q2 - ' +next1, 'Q3 - ' +next1, 'Q4 - ' +next1,
                                       'Q1 - ' +next2,'Q2 - ' +next2
                                       //KN added 5-2-13
                                       ,'Q3 - ' +next2, 'Q4 - ' +next2
                                       };
                                                                                                            
        }
        if (q == 2)
        {
            q_list = new List<String> {'Q2 - ' +year,'Q3 - ' +year, 'Q4 - ' +year, 'Q1 - ' +next1,
                                       'Q2 - ' +next1, 'Q3 - ' +next1, 'Q4 - ' +next1, 'Q1 - ' +next2,
                                       //KN added 5-2-13
                                       //'Q2 - ' +next2,  'Q3 - ' +next2};
                                       'Q2 - ' +next2,  'Q3 - ' +next2, 'Q4 - ' +next2, 'Q1 - ' +next3};
                                            
        }
        if (q == 3)
        {
            q_list = new List<String> {'Q3 - ' +year,'Q4 - ' +year, 'Q1 - ' +next1, 'Q2 - ' +next1,
                                       'Q3 - ' +next1, 'Q4 - ' +next1, 'Q1 - ' +next2, 'Q2 - ' +next2,  
                                       //'Q3 - ' +next2 , 'Q4 - ' +next2};
                                       'Q3 - ' +next2 , 'Q4 - ' +next2, 'Q1 - ' +next3,'Q2 - ' +next3};
                                                                                                            
        }
        if (q == 4)
        {
            q_list = new List<String> {'Q4 - ' +year,'Q1 - ' +next1, 'Q2 - ' +next1, 'Q3 - ' +next1,
                                       'Q4 - ' +next1, 'Q1 - ' +next2, 'Q2 - ' +next2, 'Q3 - ' +next2,
                                       //'Q4 - ' +next2,'Q1 - ' +next3};
                                       'Q4 - ' +next2,'Q1 - ' +next3, 'Q2 - ' +next3, 'Q3 - ' +next3};
                                                                    
                                                
        }
        return q_list;          
        
    }
    public pagereference showALL() {
    	display_seacrh = false;
    	OPN_name = '';
    	return null;
    }
    public pagereference OPN_search() {
    	display_seacrh = true;
    	olines_by_search = new List<OpportunityLineItem>();
    	if( !String.isBlank(OPN_name)) {
    		olines_by_search = new List<OpportunityLineItem>([select OpportunityId,Opportunity.Sales_Ops_Upside__c,Opportunity.StageName,
    																Opportunity.Name,Opportunity.Record_type_ID__c, Opportunity.Amount,
    																Opportunity.CloseDate,Opportunity.Customer_Parent__c,Opportunity.Owner.Name,
    																Opportunity.Primary_Partner_N__r.name
    													from OpportunityLineItem
    													where Product_Name__c = :OPN_name
    													and (OpportunityId in :map_optys.keyset() or OpportunityId in :opty_endusers_IDs) 
    													order by Opportunity.Record_type_ID__c]);
    	}
    	return null;
    }
	public void init(){
		//display_seacrh = false;
		lst_schedules = [Select OpportunityLineItem.OpportunityId,Revenue,ScheduleDate 
							From OpportunityLineItemSchedule
							where Revenue > 0 AND OpportunityLineItem.OpportunityId in :map_optys.keyset()];
		for (OpportunityLineItemSchedule ols : lst_schedules){
			if (map_optyID_listschedules.get(ols.OpportunityLineItem.OpportunityId) == null){
				tmp_schedules = new List<OpportunityLineItemSchedule>();
				tmp_schedules.add(ols);
				map_optyID_listschedules.put(ols.OpportunityLineItem.OpportunityId,tmp_schedules);
			}
			else {
				tmp_schedules = map_optyID_listschedules.get(ols.OpportunityLineItem.OpportunityId);
				tmp_schedules.add(ols);
				map_optyID_listschedules.put(ols.OpportunityLineItem.OpportunityId,tmp_schedules);
			}
		} //end building map
		
		totalOEM_row = new AllRow();
		Total_InFC_row = new AllRow();
		totalOEM_row_notFC = new AllRow();
		Total_InFC_row.optyname = 'Total-In Forecast';
		totalOEM_row.optyname = 'OEM-In Forecast';
		totalOEM_row_notFC.optyname = 'OEM -Not Forecast'; 
		totalEndUser_NotFC_row = new AllROW();
		totalEndUser_InFC_row = new AllROW();
		totalEndUser_NotFC_row.optyname = 'End User-Not Forecast';
		totalEndUser_InFC_row.optyname = 'End User-In Forecast';
		totalActual_row = new AllRow();
		totalTOGO_row = new AllRow();
		totalActual_row.optyname = 'Actuals';
		totalTOGO_row.optyname = 'TOGO (Calculated)'; 
		
		grandtotal_row = new AllROW();
		grandtotal_row.optyname = 'Total All Pipeline';
		
		for (ID oID : map_optyID_listschedules.keyset()){
			tmp_row = new AllRow();
			tmp_row.optyID = map_optys.get(oID) != null ? map_optys.get(oID).ID : null;
			tmp_row.optyname = map_optys.get(oID) != null ? map_optys.get(oID).Name : '';
			//tmp_row.optyname = map_optys.get(oID).Name.substring(0,map_optys.get(oID).Name.length()-17);
			tmp_row.stage = map_optys.get(oID) != null ? map_optys.get(oID).StageName : '';
			if(map_optys.get(oID) != null && map_optys.get(oID).Sales_Ops_Upside__c == true)
				tmp_row.sales_upside = 'Yes';
			
			for (OpportunityLineItemSchedule osch : map_optyID_listschedules.get(oID)){
				if(osch.ScheduleDate >= periods[0].StartDate && osch.ScheduleDate <= periods[0].EndDate){
					tmp_row.Qty_P0 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1){
						totalOEM_row.Qty_P0 += osch.Revenue;
						Total_InFC_row.Qty_P0 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P0 += osch.Revenue;
					grandtotal_row.Qty_P0 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[1].StartDate && osch.ScheduleDate <= periods[1].EndDate){
					tmp_row.Qty_P1 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P1 += osch.Revenue;
						Total_InFC_row.Qty_P1 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P1 += osch.Revenue;
					grandtotal_row.Qty_P1 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[2].StartDate && osch.ScheduleDate <= periods[2].EndDate){
					tmp_row.Qty_P2 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P2 += osch.Revenue;
						Total_InFC_row.Qty_P2 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P2 += osch.Revenue;
					grandtotal_row.Qty_P2 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[3].StartDate && osch.ScheduleDate <= periods[3].EndDate){
					tmp_row.Qty_P3 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P3 += osch.Revenue;
						Total_InFC_row.Qty_P3 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P3 += osch.Revenue;
					grandtotal_row.Qty_P3 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[4].StartDate && osch.ScheduleDate <= periods[4].EndDate){
					tmp_row.Qty_P4 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P4 += osch.Revenue;
						Total_InFC_row.Qty_P4 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P4 += osch.Revenue;
					grandtotal_row.Qty_P4 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[5].StartDate && osch.ScheduleDate <= periods[5].EndDate){
					tmp_row.Qty_P5 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P5 += osch.Revenue;
						Total_InFC_row.Qty_P5 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P5 += osch.Revenue;
					grandtotal_row.Qty_P5 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[6].StartDate && osch.ScheduleDate <= periods[6].EndDate){
					tmp_row.Qty_P6 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P6 += osch.Revenue;
						Total_InFC_row.Qty_P6 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P6 += osch.Revenue;
					grandtotal_row.Qty_P6 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[7].StartDate && osch.ScheduleDate <= periods[7].EndDate){
					tmp_row.Qty_P7 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P7 += osch.Revenue;
						Total_InFC_row.Qty_P7 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P7 += osch.Revenue;
					grandtotal_row.Qty_P7 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[8].StartDate && osch.ScheduleDate <= periods[8].EndDate){
					tmp_row.Qty_P8 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P8 += osch.Revenue;
						Total_InFC_row.Qty_P8 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P8 += osch.Revenue;
					grandtotal_row.Qty_P8 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[9].StartDate && osch.ScheduleDate <= periods[9].EndDate){
					tmp_row.Qty_P9 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P9 += osch.Revenue;
						Total_InFC_row.Qty_P9 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P9 += osch.Revenue;
					grandtotal_row.Qty_P9 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[10].StartDate && osch.ScheduleDate <= periods[10].EndDate){
					tmp_row.Qty_P10 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P10 += osch.Revenue;
						Total_InFC_row.Qty_P10 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P10 += osch.Revenue;
					grandtotal_row.Qty_P10 += osch.Revenue;
				}
				else if (osch.ScheduleDate >= periods[11].StartDate && osch.ScheduleDate <= periods[11].EndDate){
					tmp_row.Qty_P11 += osch.Revenue;
					if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1) {
						totalOEM_row.Qty_P11 += osch.Revenue;
						Total_InFC_row.Qty_P11 += osch.Revenue;
					}
					else
						totalOEM_row_notFC.Qty_P11 += osch.Revenue;
					grandtotal_row.Qty_P11 += osch.Revenue;
				}
			}
			//group into the right bucket
			if (map_optys.get(oID) != null && map_optys.get(oID).Forecast__c == 1)
				OEM_allrows.add(tmp_row);
			else 
				OEM_allrows_notFC.add(tmp_row);
		}			
		
		TOTAL_allrows.add(totalOEM_row);
		TOTAL_allrows.add(totalOEM_row_notFC);
		//Sum up Actuals + TOGO
		for (Opportunity o : optys_actuals_togo){
			if(o.CloseDate >= periods[0].StartDate && o.CloseDate <= periods[0].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P0 += o.Amount;
				else
					totalTOGO_row.Qty_P0 += o.Amount;
			}
			else if(o.CloseDate >= periods[1].StartDate && o.CloseDate <= periods[1].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P1 += o.Amount;
				else
					totalTOGO_row.Qty_P1 += o.Amount;
			}
			else if(o.CloseDate >= periods[2].StartDate && o.CloseDate <= periods[2].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P2 += o.Amount;
				else
					totalTOGO_row.Qty_P2 += o.Amount;
			}
			else if(o.CloseDate >= periods[3].StartDate && o.CloseDate <= periods[3].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P3 += o.Amount;
				else
					totalTOGO_row.Qty_P3 += o.Amount;
			}
			else if(o.CloseDate >= periods[4].StartDate && o.CloseDate <= periods[4].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P4 += o.Amount;
				else
					totalTOGO_row.Qty_P4 += o.Amount;
			}
			else if(o.CloseDate >= periods[5].StartDate && o.CloseDate <= periods[5].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P5 += o.Amount;
				else
					totalTOGO_row.Qty_P5 += o.Amount;
			}
			else if(o.CloseDate >= periods[6].StartDate && o.CloseDate <= periods[6].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P6 += o.Amount;
				else
					totalTOGO_row.Qty_P6 += o.Amount;
			}
			else if(o.CloseDate >= periods[7].StartDate && o.CloseDate <= periods[7].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P7 += o.Amount;
				else
					totalTOGO_row.Qty_P7 += o.Amount;
			}
			else if(o.CloseDate >= periods[8].StartDate && o.CloseDate <= periods[8].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P8 += o.Amount;
				else
					totalTOGO_row.Qty_P8 += o.Amount;
			}
			else if(o.CloseDate >= periods[9].StartDate && o.CloseDate <= periods[9].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P9 += o.Amount;
				else
					totalTOGO_row.Qty_P9 += o.Amount;
			}
			else if(o.CloseDate >= periods[10].StartDate && o.CloseDate <= periods[10].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P10 += o.Amount;
				else
					totalTOGO_row.Qty_P10 += o.Amount;
			}
			else if(o.CloseDate >= periods[11].StartDate && o.CloseDate <= periods[11].EndDate){
				if(o.RecordTypeID == '01250000000Dtdv')
					totalActual_row.Qty_P11 += o.Amount;
				else
					totalTOGO_row.Qty_P11 += o.Amount;
			}
		}
		ActualTOGO_allrows.add(totalActual_row);
		ActualTOGO_allrows.add(totalTOGO_row);
		//
		//End User Sum
		for (Opportunity o : optys_enduser){
			//if Account and Primary Partner are different, IGNORE
			if(o.AccountID != o.Primary_Partner_N__c 
					&& acct.Name != o.Customer_Parent__c
					&& (o.Customer_Parent__c == 'MICROSOFT' || o.Customer_Parent__c == 'FACEBOOK INC' || o.Customer_Parent__c == 'AMAZON' || o.Customer_Parent__c == 'APPLE COMPUTER INC'))
				continue;
			//store IDs for later use in OPN search
			opty_endusers_IDs.add(o.ID);
			//				
			tmp_row = new AllRow();
			tmp_row.optyID = o.ID;
			if(o.Name.length() > 25)
				tmp_row.optyname = o.Name.substring(0,o.Name.length()-17);
			else
				tmp_row.optyname = o.Name;								
			tmp_row.stage = o.StageName;
			//tmp_row.disty_partner = o.Distribution_Partner__r.Name;
			
			//tmp_row.isOEM = false;
			if(o.Sales_Ops_Upside__c == true)
				tmp_row.sales_upside = 'Yes';
				
			if(o.CloseDate >= periods[0].StartDate && o.CloseDate <= periods[0].EndDate){
				tmp_row.Qty_P0 += o.Amount;
				grandtotal_row.Qty_P0 += o.Amount;	
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P0 += o.Amount;
					Total_InFC_row.Qty_P0 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P0 += o.Amount;
			}
			else if(o.CloseDate >= periods[1].StartDate && o.CloseDate <= periods[1].EndDate){
				tmp_row.Qty_P1 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P1 += o.Amount;
				grandtotal_row.Qty_P1 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P1 += o.Amount;
					Total_InFC_row.Qty_P1 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P1 += o.Amount;
						
			}
			else if(o.CloseDate >= periods[2].StartDate && o.CloseDate <= periods[2].EndDate){
				tmp_row.Qty_P2 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P2 += o.Amount;
				grandtotal_row.Qty_P2 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P2 += o.Amount;
					Total_InFC_row.Qty_P2 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P2 += o.Amount;	
			}
			else if(o.CloseDate >= periods[3].StartDate && o.CloseDate <= periods[3].EndDate){
				tmp_row.Qty_P3 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P3 += o.Amount;
				grandtotal_row.Qty_P3 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P3 += o.Amount;
					Total_InFC_row.Qty_P3 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P3 += o.Amount;	
			}
			else if(o.CloseDate >= periods[4].StartDate && o.CloseDate <= periods[4].EndDate){
				tmp_row.Qty_P4 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P4 += o.Amount;
				grandtotal_row.Qty_P4 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P4 += o.Amount;
					Total_InFC_row.Qty_P4 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P4 += o.Amount;	
			}
			else if(o.CloseDate >= periods[5].StartDate && o.CloseDate <= periods[5].EndDate){
				tmp_row.Qty_P5 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P5 += o.Amount;
				grandtotal_row.Qty_P5 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P5 += o.Amount;
					Total_InFC_row.Qty_P5 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P5 += o.Amount;	
			}
			else if(o.CloseDate >= periods[6].StartDate && o.CloseDate <= periods[6].EndDate){
				tmp_row.Qty_P6 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P6 += o.Amount;
				grandtotal_row.Qty_P6 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P6 += o.Amount;
					Total_InFC_row.Qty_P6 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P6 += o.Amount;	
			}
			else if(o.CloseDate >= periods[7].StartDate && o.CloseDate <= periods[7].EndDate){
				tmp_row.Qty_P7 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P7 += o.Amount;
				grandtotal_row.Qty_P7 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P7 += o.Amount;
					Total_InFC_row.Qty_P7 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P7 += o.Amount;	
			}
			else if(o.CloseDate >= periods[8].StartDate && o.CloseDate <= periods[8].EndDate){
				tmp_row.Qty_P8 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P8 += o.Amount;
				grandtotal_row.Qty_P8 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P8 += o.Amount;
					Total_InFC_row.Qty_P8 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P8 += o.Amount;	
			}
			else if(o.CloseDate >= periods[9].StartDate && o.CloseDate <= periods[9].EndDate){
				tmp_row.Qty_P9 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P9 += o.Amount;
				grandtotal_row.Qty_P9 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P9 += o.Amount;
					Total_InFC_row.Qty_P9 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P9 += o.Amount;	
			}
			else if(o.CloseDate >= periods[10].StartDate && o.CloseDate <= periods[10].EndDate){
				tmp_row.Qty_P10 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P10 += o.Amount;
				grandtotal_row.Qty_P10 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P10 += o.Amount;
					Total_InFC_row.Qty_P10 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P10 += o.Amount;	
			}
			else if(o.CloseDate >= periods[11].StartDate && o.CloseDate <= periods[11].EndDate){
				tmp_row.Qty_P11 += o.Amount;
				//totalEndUser_NotFC_row.Qty_P11 += o.Amount;
				grandtotal_row.Qty_P11 += o.Amount;
				if(o.Forecast__c == 1) {
					totalEndUser_InFC_row.Qty_P11 += o.Amount;
					Total_InFC_row.Qty_P11 += o.Amount;
				}
				else
					totalEndUser_NotFC_row.Qty_P11 += o.Amount;	
			}
			//Add opty row to the corresponding list -End User Deal in FC or Not in FC
			if(o.Forecast__c == 1)
				EndUser_InFC_allrows.add(tmp_row);
			else
				EndUser_NotFC_allrows.add(tmp_row);
		}
		TOTAL_allrows.add(totalEndUser_InFC_row);
		TOTAL_allrows.add(totalEndUser_NotFC_row);
		
		ActualTOGO_allrows.add(Total_InFC_row);
		
		TOTAL_allrows.add(grandtotal_row);
	}
	/*
	public pagereference Forecast_Mgr() {
        String optyID = ApexPages.CurrentPage().getParameters().get('optyID');
        PageReference pg;
            pg = new PageReference('/apex/ListAllOptysforPart');
            pg.getParameters().put('product_no',product_no);
            pg.getParameters().put('product_desc',product_desc);
            pg.getParameters().put('accountName',opty.Fulfillment_Account__r.Name);
            pg.getParameters().put('accountID',opty.Fulfillment_Account__c);
            pg.getParameters().put('ID',opty.ID);
            pg.getParameters().put('OptyType',opty.Type);       
            pg.getParameters().put('OwnerID',opty.OwnerID);
            pg.setRedirect(true);
        return pg;
    }
    */
}