@isTest(seeAllData=true)
public with sharing class test_cls_Project_Milestone {
	
	static testMethod void test_updateClonedProjectsWithRelatedChildrenRecords(){
		
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		
		Account acc = creator.createAccount();
		insert acc;
		
		Milestone1_Project__c project = creator.CreateMPproject(acc.Id);
		project.Total_Duration_Hours__c = 60;
		insert project;
		
		User user = creator.CreateUser();
		insert user;
		
		Project_Member__c member = creator.CreateProjectMember(project.Id, user.Id);
		insert member;
		
		project.Total_Duration_Hours__c = 70;
		update project;
		
		Milestone1_Project__c newProject = creator.CreateMPproject(acc.Id);
		newProject.Salesforce_ID__c = String.valueOf(project.Id).substring(0,15);
		
		system.test.startTest();
		insert newProject;
		system.test.stopTest();
		
	}
	
	static testMethod void test_create2MembersWhenDiagnosticPackProjectIsCreated(){
		
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		
		RecordType diagnosticPackRecordType = [SELECT Id FROM RecordType WHERE developerName =: 'Diagnostics_Pack' and SobjectType =: 'Milestone1_Project__c'];
		
		Account acc = creator.createAccount();
		insert acc;
		
		Milestone1_Project__c project = creator.CreateMPproject(acc.Id);
		project.RecordTypeId = diagnosticPackRecordType.Id;
		
		system.test.startTest();
		
		insert project;
		system.test.stopTest();
		
	}
}