global class AssetReassignment implements Database.Batchable<sObject>{

global string query;
global map<id,id> NewContract;
global map<string,id> OrderContract;
global id SNContract;
global string Origin;
global id NewTest;
global integer ContNum = 0;



global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query );
       
            }
 


global void execute(Database.BatchableContext BC, List<sObject> scope){
List<Asset2__c> NewAssets = new List<Asset2__c>();
List<Asset2__c> DelAssets = new List<Asset2__c>();
                    
for(sObject s : scope){ Asset2__c ast = (Asset2__c)s;
  Asset2__c ast11 = new Asset2__c();
 ContNum = +1;
   // ASSIGN ASSET    
   
 System.debug('Asset from Inna= '+ ast.id);  
   
if(((Origin != 'CM' && Origin != 'CM_SN' ) && ast.Contract2__c != NewContract.get(ast.Contract2__c))||   
   ((Origin == 'CM')&& ast.Contract2__c !=  OrderContract.get(ast.order__c)) || (Origin == 'CM_SN')  )                                                                                          
{
           
if(Origin != 'CM' && Origin != 'CM_SN')
{ast.Contract2__c =  NewContract.get(ast.Contract2__c) ;}

if(Origin == 'CM')
{ast.Contract2__c =  OrderContract.get(ast.order__c) ; }

if(Origin == 'CM_SN')
{ast.Contract2__c =  SNContract ; 
  System.debug('Asset contract from Inna= '+ SNContract);

}
  
  //ASSIGN ASSET
  
  
  //insert ast11;
    
NewAssets.add(ast);     
    
//  NewAssets.add(ast11); 
//  DelAssets.add(ast); 
 }   //if new contract != existing
} // for

//insert NewAssets;
//delete DelAssets;

update(NewAssets);  
}
global void finish(Database.BatchableContext BC){
/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.setToAddresses(new String[] {'innag@mellanox.co.il'});
mail.setReplyTo('batch@acme.com');
mail.setSenderDisplayName('Batch Processing');
mail.setSubject('Batch Process Completed');
mail.setPlainTextBody('Batch Process has completed moving :'+ ContNum + ' contracts');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

}
}