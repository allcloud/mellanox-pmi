global class asyncApex {  
 
@future  
public static void processPortalContact(String data) {  

List<Portal_Contact__c> contacts = new List<Portal_Contact__c>();
         
 for(Portal_Contact__c cnt: [select id,Notification_data__c, Send_note__c from Portal_contact__c])
 
  {  cnt.Send_Note__c = True;    
     cnt.Notification_data__c =data;
     Contacts.add(cnt);           
   }      

   update contacts;          
 }
}