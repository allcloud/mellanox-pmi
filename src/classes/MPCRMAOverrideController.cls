public with sharing class MPCRMAOverrideController {
  	private final RMA__c record;
     
  	public MPCRMAOverrideController(ApexPages.StandardController controller) {
		this.record = (RMA__c)controller.getRecord();
	}
  
	public boolean inCommunity() {
		return Network.getNetworkId() != null;
	}
	
	public PageReference redirect() {
		if (inCommunity()) 
    {
      PageReference customPage =  Page.MPCRMADetail;
      customPage.setRedirect(true);
      customPage.getParameters().put('id', record.Id);
      return customPage;
    } else {
      return null; //otherwise stay on the same page  
    }
  }
}