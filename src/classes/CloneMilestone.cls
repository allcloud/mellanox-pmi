public with sharing class CloneMilestone
{   
    public String idOfProj {get;set;}
    public Milestone1_Milestone__c ms {get;set;}
    public String idOfRec {get;set;}
    public ID MsId;
    
        
   
    public CloneMilestone(ApexPages.StandardController controller)
    {
     MsId = Apexpages.currentPage().getParameters().get('Id');
     ms = new Milestone1_Milestone__c();
     ms = [SELECT ID, Name,project__c, project__r.name FROM Milestone1_Milestone__c WHERE Id = : Msid];
    }
    
     public Pagereference Cancel()
     {           
       return new Pagereference('/'+ms.id);       
     }     
   
    public PageReference cloneRec()
    {
        List<Task> tasks = new List<Task>();
       
        Milestone1_Milestone__c MsCopy = ms.clone(false,true);
        MsCopy.project__c = ms.project__c; //idOfProj; 
        insert msCopy;
        
        List<Task> ms_tsks = [SELECT Id,subject,ActivityDate,IsVisibleInSelfService,Priority,ProjectId__c,ProjectName__c, whatId FROM task WHERE whatId = : ms.Id];
        for(Task ts : ms_tsks)
        {
            Task tsCopy = ts.clone(false,true);
            tsCopy.WhatId = msCopy.Id;
            tsCopy.projectId__c = string.valueOf(msCopy.project__c).left(15);
            tsCopy.projectName__c = ms.project__r.name; 
            tasks.add(tsCopy);
        }
        insert tasks;
        return new Pagereference('/'+msCopy.Id);
        
      /*  List<Contact> cons = new List<Contact>();
        Account acc = [SELECT ID, Name FROM Account WHERE Id = : idOfRec];
        Account accCopy = acc.clone(false,true);
        insert accCopy;
        List<Contact> con = [SELECT Id, LastName, AccountId FROM Contact WHERE AccountId = : acc.Id];
        for(Contact c : con)
        {
            Contact conCopy = c.clone(false,true);
            conCopy.AccountId = accCopy.Id;
            cons.add(conCopy);
        }
        insert cons;*/
    }
}