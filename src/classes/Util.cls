public class Util
{
    public static Boolean CaseCommentCreated=false;
    public static Map<String, String> caseStates = new Map<String, String>{'Open' => 'Open', 'Assigned' => 'Assigned', 'Open AE' => 'Open AE', 'Assigned AE' => 'Assigned AE', 'Waiting for Customer Info' => 'Waiting for Customer Info', 'Waiting for Customer Approval' => 'Waiting for Customer Approval', 'RMA Assigned' => 'RMA Assigned', 'Wait for Release' => 'Wait for Release','On Hold' => 'On Hold', 'Closed by Customer' => 'Closed by Customer', 'Closed' => 'Closed'};
    
    public static String refreshOpenerCloseMeSControlId = Env.refreshOpenerCloseMeSControlId;
    
    //public static ID ProfileDesignSupportCus = Env.ProfileDesignSupportCus;
    
    //public static ID ProfileSystemSupportCus = Env.ProfileSystemSupportCus;
    
    //public static ID ProfileSystemAdmin = Env.ProfileSystemAdmin;
    
    public static ID ContactForEmail = Env.ContactForEmail;
    
    public static ID TemplateInternalCaseComment = Env.TemplateInternalCaseComment;
    
    public static ID TemplatePublicCaseComment = Env.TemplatePublicCaseComment;
    
    public static set<String> removeCreatedBy(set <String> tmpAddress, String createdBy)
    {
        system.debug('BLAT removeCreatedBy');
        system.debug('BLAT createdBy: '+ createdBy);
        if(tmpAddress.contains(createdBy))
        {
            system.debug('BLAT Contains');
            system.debug('BLAT tmpAddress Before: '+ tmpAddress);
            tmpAddress.remove(createdBy);
            system.debug('BLAT tmpAddress After: '+ tmpAddress);
        }
        return tmpAddress;
    }
    
    public static void SendSingleEmail(List<String> toAddresses, List<String> ccAddresses, String ReplyToEmail, String SenderName, ID TemplateID, ID ObjectID, String subject, String html)
    {
        System.debug('BLAT SendSingleEmail');
        try
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            if(ccAddresses!=null)
            {
              mail.setCcAddresses(ccAddresses);
            }
            mail.setReplyTo('supportadmin@mellanox.com');
            //mail.setSenderDisplayName(SenderName);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            //mail.setOrgWideEmailAddressId('0D25000000000Aa');           
            mail.setOrgWideEmailAddressId(Env.SupportAdminAddress);
            if(TemplateID!=null || TemplateID!='')
            {
                System.debug('BLAT TemplateID: '+ TemplateID);
                mail.setTemplateId(TemplateID);
                mail.setTargetObjectId(ContactForEmail);
                mail.setWhatId(ObjectID);
                System.debug('BLAT ObjectID: '+ ObjectID);
                System.debug('BLAT TemplateID: '+ TemplateID);
                System.debug('BLAT ContactForEmail: '+ ContactForEmail);
            }
            else
            {
                mail.setSubject(subject);
                mail.setHtmlBody(html);
            }
            Messaging.SendEmailResult[] sendEmailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            System.debug('BLAt Success');
            for(Messaging.SendEmailResult sendEmailResult : sendEmailResults)
            {
              if(sendEmailResult.isSuccess())
              {
                    System.debug('SendSingleEmail --> email was sent to user.');
              } 
              else 
              {
                    System.debug('SendSingleEmail --> email was not sent to user.');
              }
            } 
        }
        catch (Exception ex)
        {
            System.debug(ex);
        }    
      }
      
    public static Id getInternalRecordTypeId()
    {
        return [Select r.Name, r.Id From RecordType r where r.Name = 'Internal_RMA'].Id ;
    }
}