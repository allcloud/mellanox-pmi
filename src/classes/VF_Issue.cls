public with sharing class VF_Issue
{

   
    public boolean isNew {get;set;} //0 is new not 0 is edit
    public Id EditId {get;set;}
    public SFDC_Issue__c editVFIssue {get;set;}
    
    public Case currCase {get;set;}
    
    public map<ID, Issue_Product__c> map_ProdIdToProd ;
       
    public map<ID, Release__c  > map_IdToRelease;
    public map<ID, Issue_Category__c  > map_IdToCategory;
    
                 
    public List<SelectOption> productsList {get;set;} 
    public String ProductSelected {get;set;}
    
      
    public List<SelectOption> releaseItems {get;set;}
    //public String releaseSelected {get;set;}
    public String FixReleaseSelected {get;set;}
     public String IssueName {get;set;}
    
    public List<SelectOption> categoryItems {get;set;}
    public String categorySelected {get;set;}
    public SFDC_issue__c newIssue  {get;set;} 
    public List<String> multiProfileSelected {get;set;} 
    public List<String> multiReleaseSelected {get;set;}                           
    public map<ID, Issue_Profile__c > map_profIdToProfile_all ;
    public List<SelectOption> allProfiles {get;set;} 
    public List<SelectOption> ProfileItems {get;set;}
    public List<SelectOption> allReleases {get;set;} 
    
                
       
    

    
    public ID theCaseId {get;set;}
    
    
    //Validations Flad
    public boolean prodFilled {get;set;}
    public boolean releaseFilled {get;set;}
    public boolean categoryFilled {get;set;}
    public boolean errMsg {get;set;}
    public boolean successSave {get;set;}
    public boolean existCase {get;set;}
    public boolean makeUnMakeAssign {get;set;}

    Set<string> leftvalues = new Set<string>();  
    Set<string> rightvalues = new Set<string>();  
    Public List<string> leftselected{get;set;}  
    Public List<string> rightselected{get;set;}  
 
 
    Set<string> leftvaluesRel = new Set<string>();  
    Set<string> rightvaluesRel = new Set<string>();  
    Public List<string> leftselectedRel{get;set;}  
    Public List<string> rightselectedRel{get;set;}  

        

    public VF_Issue(ApexPages.StandardController controller) 
    {   
        //as there are an issue with the IE9 and rerender in visual force this will make the IE 9 react as IE8
       Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
       leftselected = new List<String>();  
       rightselected = new List<String>();
       leftselectedRel = new List<String>();  
       rightselectedRel = new List<String>();   
                         
        makeUnMakeAssign = false;
        
        prodFilled  = true;
        releaseFilled = true;
        categoryFilled = true;
        errMsg = false;
        successSave = false ;
                                         
        ProductSelected   =   '--None--';
        
        categorySelected  =   '--None--';
        FixReleaseSelected =  '--None--';
    
    
        EditId = Apexpages.currentPage().getParameters().get('id');
        
        //String i  = Apexpages.currentPage().getParameters().get('mode');
        if(EditId != NULL)
            isNew = false;
        else
            isNew = true;
        
        if(EditId != null)
        {
            System.debug('******** EDIT MODE: ' );
            editVFIssue = new SFDC_Issue__c();
            editVFIssue = [  Select  r.id,Product__c,Profile_selected__c,Release__c, Type__c, Issue_Category__c, Fix_in_Release__c,
                            Comments__c,WorkAround__c,Issue_Description__c,Issue_Name__c, Status__c
                                    From SFDC_Issue__c r
                            WHERE r.Id =: EditId];
            
             
            isNew = false;
                             
            productsList = getproductsList();
            ProductSelected = editVFIssue.Product__c;
                       
           
                                        
            getReleaseAndCategoryBsedOnProd();
            //releaseSelected = editVFIssue.Issue_Release__c;
            
            
            getCategory();
            CategorySelected = editVFIssue.Issue_Category__c;
            FixReleaseSelected = editVFIssue.Fix_in_Release__c; 
            
            
            
             if(editVFIssue.Profile_selected__c != null){
             multiProfileSelected =editVFIssue.Profile_selected__c.split(',');    
            rightvalues.addAll(multiProfileSelected);
            selectclickRel();
           // leftvalues.addAll(multiProfileSelected);
             }
            
            if(editVFIssue.Release__c != null){
             multiReleaseSelected =editVFIssue.Release__c.split(',');    
            rightvaluesRel.addAll(multiReleaseSelected); }    
            
                     
            
        }else
        {
            System.debug('********************************* NEW  MODE: ' );
              
                productsList = getproductsList();
                newIssue = new SFDC_Issue__c();
          //      getProfiles();
                           
        }
        
        errMsg = allFieldsFilled();
                          
    }
    
    //get all the projects in the system
    public List<SelectOption> getproductsList() 
    {
                
                
        map_ProdIdToProd = new map<ID,Issue_Product__c>([Select  r.Id, r.Name From Issue_Product__c r order by r.Name ASC ]);
        
        if(map_ProdIdToProd != null && map_ProdIdToProd.size() > 0)
        {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--None--','--None--'));
            
            for( Issue_Product__c  prod : map_ProdIdToProd.values())
            {
                               
               options.add(new SelectOption(prod.id, prod.name));
            
            }
            system.debug('START: getprojectsList End with list full ');    
            
            errMsg =allFieldsFilled();
            return options;
            
        }else
        {
            errMsg=allFieldsFilled();
                      
            system.debug('START: getprojectsList End with Null');
            return null;
        }
    }
    
    //get the subproject as per the selected project
   
    
                      
    //get the subproject as per the selected project
    public void getReleaseAndCategoryBsedOnProd()
    {
       map_IdToCategory = new map<ID, Issue_Category__c >([  Select  r.id, r.name 
                                                                From  Issue_category__c r 
                                                                WHERE r.Product__c =:ProductSelected ]);
        
        if(map_IdToCategory != null && map_IdToCategory .size() > 0)
        {
            
            categoryItems = new List<SelectOption> ();
            categoryItems.add(new SelectOption('--None--','--None--'));
                      
            for(Issue_category__c  cat : map_IdToCategory.values())
            {
                categoryItems.add(new SelectOption(cat.Id, cat.name));
                        
            }
            
        }else
        {
            if(categoryItems != null && categoryItems.size() > 0)
            {
                categoryItems.clear();
                categoryItems.add(new SelectOption('--None--','--None--'));
            }
            
        }        
        
        map_IdToRelease = new map<ID, Release__c  >([    Select  r.id , r.name 
                                                                        From Release__c r
                                                                        WHERE r.Product__c =:ProductSelected ]);
        
        if(map_IdToRelease != null && map_IdToRelease.size() > 0)
        {
            ReleaseItems = new List<SelectOption> ();
           // AssigneeItems.add(new SelectOption('Default','Default'));
            ReleaseItems.add(new SelectOption('--None--','--None--'));   
            for(Release__c  rel : map_IdToRelease.values())
            {
                ReleaseItems.add(new SelectOption(rel.id, rel.Name));
                       
            }
          // ReleaseItems =SortOptionList(ReleaseItems);
          //  SelectOptionSorter.doSort(AssigneeItems, SelectOptionSorter.FieldToSort.Label); 
                
                           
        }else
        {
            
            if(ReleaseItems != null && ReleaseItems.size() > 0)
            {
                ReleaseItems.clear();
                ReleaseItems.add(new SelectOption('--None--','--None--'));
            }
            
            
        } 
       
        errMsg = allFieldsFilled();
        system.debug('END - getAssigneBsedOnProjAndSubProj');
      
       map_IdToCategory = new map<ID, Issue_Category__c >([  Select  r.id, r.name 
                                                                From  Issue_category__c r 
                                                                WHERE r.Product__c =:ProductSelected ]);
        
        if(map_IdToCategory != null && map_IdToCategory .size() > 0)
        {
            
            categoryItems = new List<SelectOption> ();
            categoryItems.add(new SelectOption('--None--','--None--'));
                      
            for(Issue_category__c  cat : map_IdToCategory.values())
            {
                categoryItems.add(new SelectOption(cat.Id, cat.name));
                        
            }
            
        }else
        {
            if(categoryItems != null && categoryItems.size() > 0)
            {
                categoryItems.clear();
                categoryItems.add(new SelectOption('--None--','--None--'));
            }
            
        }
        
        errMsg=  allFieldsFilled(); 
       map<ID, Issue_Profile__c > map_profIdToProfile_all = new map<ID, Issue_Profile__c  >([select  id, name
                                                                                  From Issue_Profile__c r where r.Product__c =:ProductSelected]);                                                                                          
       
        set<string> set_tmp = new set<String>();
        leftvalues.clear();
         
        if(map_profIdToProfile_all != null && map_profIdToProfile_all.size() > 0)
        {
                           
            allProfiles = new List<SelectOption> ();
            allProfiles.add(new SelectOption('--None--','--None--'));
            for(Issue_Profile__c  Prof : map_profIdToProfile_all.values())
            { 
                  allProfiles.add(new SelectOption(Prof.Name, Prof.Name));
                  leftvalues.add(Prof.Name);  
                                                      
                                                     
            }
                        
        }else
        {
            if(ProfileItems != null && ProfileItems.size() > 0)
            {
                allProfiles.clear();
                allProfiles.add(new SelectOption('--None--','--None--'));
            }
            
            
        } 
        
        
 
 
   map_IdToRelease = new map<ID, Release__c  >([    Select  r.id , r.name 
                                                                        From Release__c r
                                                                        WHERE r.Product__c =:ProductSelected ]);
                                                                                                                                                                            
       
        leftvaluesRel.clear();
         
        if(map_IdToRelease != null && map_IdToRelease.size() > 0)
        {
                           
            allReleases = new List<SelectOption> ();
            allReleases.add(new SelectOption('--None--','--None--'));
            for(Release__c  Rel : map_IdToRelease.values())
            { 
                  allReleases.add(new SelectOption(REl.Name, Rel.Name));
                  leftvaluesRel.add(Rel.Name);  
                                                      
                                                     
            }
                        
        }else
        {
            if(ReleaseItems != null && ReleaseItems.size() > 0)
            {
                allReleases.clear();
                allReleases.add(new SelectOption('--None--','--None--'));
            }
            
            
        } 
 
        
        
  
               
       
                                        
    }
   
   
  
            
    //-----------------Category---------------
    public void getCategory()
    {
       /*         
        
        map_IdToCategory = new map<ID, Issue_Category__c >([  Select  r.id, r.name 
                                                                From  Issue_category__c r 
                                                                WHERE r.Product__c =:ProductSelected ]);
        
        if(map_IdToCategory != null && map_IdToCategory .size() > 0)
        {
            
            categoryItems = new List<SelectOption> ();
            categoryItems.add(new SelectOption('--None--','--None--'));
                      
            for(Issue_category__c  cat : map_IdToCategory.values())
            {
                categoryItems.add(new SelectOption(cat.Id, cat.name));
                        
            }
            
        }else
        {
            if(categoryItems != null && categoryItems.size() > 0)
            {
                categoryItems.clear();
                categoryItems.add(new SelectOption('--None--','--None--'));
            }
            
        }
        
        errMsg=  allFieldsFilled(); */
   }
    
   
    
    public void CategorySelectedFunc() 
    {
        if(CategorySelected == '--None--')
        {
            CategorySelected = null;
             
        }
        errMsg = allFieldsFilled();
                     
    }
    
    public void IssueNameFunc() 
    {
        errMsg = allFieldsFilled();
                     
    }
    
    
    
    public Pagereference SaveIssue()
    {
        
        
       string str1= '';  
       string str2= '';    
         
            
            if(ProductSelected == '--None--')
            {
                ProductSelected = null;
            }
            
           if(CategorySelected == '--None--')
            {
                CategorySelected = null;
            }
            
                       
            if(FixReleaseSelected == '--None--')
            {
                FixReleaseSelected = null;
            }
            
            
            newIssue.Product__c = ProductSelected;
            newIssue.Issue_Category__c = CategorySelected;                                                               
                   
            newIssue.Fix_in_Release__c = FixReleaseSelected;
            
            
            if(rightvalues.size() ==1 && rightvalues.contains('--None--'))    
            {}        
            else{                      
           
              for(string AId:rightvalues)                                  
                { if(str1.length() ==0)          
                   {str1 = AId;}          
                    else {str1 = str1+','+AId;}        
                     
                                        
                 } //for               
               } //else     
                                                                       
             newIssue.Profile_selected__c = str1;
            
            
            
            if(rightvalues.size() ==1 && rightvalues.contains('--None--'))    
            {}        
            else{                      
           
              for(string AId:rightvalues)                                  
                { if(str1.length() ==0)          
                   {str1 = AId;}          
                    else {str1 = str1+','+AId;}        
                     
                                        
                 } //for               
               } //else     
                                                                       
             newIssue.Profile_selected__c = str1;
            
            
            if(rightvaluesRel.size() ==1 && rightvaluesRel.contains('--None--'))    
            {}        
            else{                      
           
              for(string Rel:rightvaluesRel)                                  
                { if(str2.length() ==0)          
                   {str2 = Rel;}          
                    else {str2 = str2+','+Rel;}        
                     
                                        
                 } //for               
               } //else     
                                                                       
             newIssue.Release__c = str2;
            
            
            
            
            
            
            
            
            
           // newIssue.Issue_Name__c = IssueName;
            
            /*if(NewIssue.Issue_Name__c == null )
            {
            errMsg = true;
            return NULL;
            }*/
            
           
            
            
             if(!allFieldsFilled())
             {
               
                   successSave = true;
                    errMsg = false;
                    system.debug('before save : ' + newIssue);
                    insert newIssue;
                    system.debug('/apex/VF_Issue?id=' + newIssue.Id);
                    return new Pagereference('/' + newIssue.id);
                  
    
             }else
             {
                errMsg = true;
                return NULL;
             }
    
            
        //}
        
        
    }
    
    public Pagereference CloseIssueUpdate()
    {
        return new Pagereference('/'+ EditId );
    }
    
        
    
    
    public Pagereference SaveIssueUpdate()
    {
         string str1= '';  
           string str2= '';  
       
       
          if(ProductSelected == '--None--')
            {
                ProductSelected = null;
            }
            
           if(CategorySelected == '--None--')
            {
                CategorySelected = null;
            }
            
            
            
            if(FixReleaseSelected == '--None--')
            {
                FixReleaseSelected = null;
            }
              
                
            editVFIssue.product__c = productSelected;     
            editVFIssue.Fix_in_Release__c = FixReleaseSelected;
            editVFIssue.Issue_Category__c = CategorySelected; 
            
            if(rightvalues.size() ==1 && rightvalues.contains('--None--'))    
            {}        
            else{                      
           
              for(string AId:rightvalues)                                  
                { if(str1.length() ==0)          
                   {str1 = AId;}          
                    else {str1 = str1+','+AId;}        
                     
                                        
                 } //for               
               } //else     
                                                                       
            editVFIssue.Profile_selected__c = str1;
            
            if(rightvaluesRel.size() ==1 && rightvaluesRel.contains('--None--'))    
            {}        
            else{                      
           
              for(string Rel:rightvaluesRel)                                  
                { if(str2.length() ==0)          
                   {str2 = Rel;}          
                    else {str2 = str2+','+Rel;}        
                     
                                        
                 } //for               
               } //else     
                                                                       
            editVFIssue.Release__c = str2;

            update  editVFIssue;                           
             return new Pagereference('/' + editVFIssue.id); 
            
            
           /* if(!allFieldsFilled())
            {
                errMsg = false;
                update editVFIssue;
                return new Pagereference('/' + editVFIssue.id);
            }else
            {
                errMsg = true;
                return null;
                
            }*/
            
    }
    
   
    public boolean allFieldsFilled()
    {
        
        
        if( 
            (productSelected == '--None--' || productSelected == Null) ||  
           // (releaseSelected == '--None--' || releaseSelected == null )|| 
            (categorySelected == '--None--' ||categorySelected == null)) 
           // (rightvaluesRel.size() ==1 ||  rightvaluesRel.contains('--None--') ))
            
        {
            system.debug('error');
            return true;
        }else
        {
            system.debug('Filled');
            return false;
        }
    } 
              
   
  
                        
   public PageReference selectclick(){  

         rightselected.clear();  

         for(String s : leftselected){  

             leftvalues.remove(s);  

             rightvalues.add(s);  

         }  

         return null;  

     }  

        

     public PageReference unselectclick(){  

         leftselected.clear();  

         for(String s : rightselected){  

             rightvalues.remove(s);  

             leftvalues.add(s);  

         }  

         return null;  

    }    
   public List<SelectOption> getunSelectedValues(){  

         List<SelectOption> options = new List<SelectOption>();  

         List<string> tempList = new List<String>();  

         tempList.addAll(leftvalues);  
         
         
          for(string s : tempList)  
         {
            options.add(new SelectOption(s,s));
         }

   
           return sortOptionList(options);
         //return options;  

     }  
 
   public List<SelectOption> getSelectedValues(){  

        List<SelectOption> options1 = new List<SelectOption>();  

         List<string> tempList = new List<String>();  

         tempList.addAll(rightvalues);  

         

         for(String s : tempList) 
         {                           
             
           options1.add(new SelectOption(s,s));
               
          }
         return sortOptionList(options1);
        // return options1;  

     }  
 
 // *************************
 
 public PageReference selectclickRel(){  

         rightselectedRel.clear();  

         for(String s : leftselectedRel){  

             leftvaluesRel.remove(s);  

             rightvaluesRel.add(s);  

         }  

         return null;  

     } 
 
 
 
 
    public PageReference unselectclickRel(){  

         leftselectedRel.clear();  

         for(String s : rightselectedRel){  

             rightvaluesRel.remove(s);  

             leftvaluesRel.add(s);  

         }  

         return null;  

    }    
   public List<SelectOption> getunSelectedValuesRel(){  

         List<SelectOption> options = new List<SelectOption>();  

         List<string> tempList = new List<String>();  

         tempList.addAll(leftvaluesRel);  
         
         
          for(string s : tempList)  
         {
            options.add(new SelectOption(s,s));
         }

   
           return sortOptionList(options);
         //return options;  

     }  
 
   public List<SelectOption> getSelectedValuesRel(){  

        List<SelectOption> options1 = new List<SelectOption>();  

         List<string> tempList = new List<String>();  

         tempList.addAll(rightvaluesRel);  

         

         for(String s : tempList) 
         {                           
             
           options1.add(new SelectOption(s,s));
               
          }
         return sortOptionList(options1);
      
     }  
 
   //**************************                                        
                                       
  public static List<SelectOption> SortOptionList(List<SelectOption> ListToSort)
    {
        if(ListToSort == null || ListToSort.size() <= 1)
            return ListToSort;
            
        List<SelectOption> Less = new List<SelectOption>();
        List<SelectOption> Greater = new List<SelectOption>();
        integer pivot = 0;
        
        // save the pivot and remove it from the list
        SelectOption pivotValue = ListToSort[pivot];
        ListToSort.remove(pivot);
        
        for(SelectOption x : ListToSort)
        {
            system.debug('x.getLabel() : ' + x.getLabel() + ' --- pivotValue.getLabel(): ' +pivotValue.getLabel());
            if(x.getLabel() <= pivotValue.getLabel())
            {
            
            system.debug('****1');
                Less.add(x);
            }
            else 
            {
                if(x.getLabel() > pivotValue.getLabel()) 
                {
                     system.debug('****2');
                    Greater.add(x);
                }
            }      
        }
        List<SelectOption> returnList = new List<SelectOption> ();
        returnList.addAll(SortOptionList(Less));
        returnList.add(pivotValue);
        returnList.addAll(SortOptionList(Greater));
        system.debug('returnList : ' + returnList);
        return returnList; 
    }  
    
    
    
    
    
    
    static testMethod void Test_VF_Issue() 
   {
                 //instance of object creator
                CLS_ObjectCreator obj = new CLS_ObjectCreator();
                Issue_Product__c prod = obj.createIssueProduct();
                prod.name ='MyProd';
                insert prod;
                
                Release__c rel = obj.CreateIssueRelease(prod);
                rel.Name = 'MyRel';
                insert rel;
                
                Issue_Profile__c prof = obj.CreateIssueProfile(prod);
                prof.name = 'MyProf';
                insert prof;
                
                Issue_Category__c cat = obj.CreateIssueCategory(prod);
                cat.name ='MyCon';
                insert cat;
                
                SFDC_issue__c issue = obj.createIssue(prod, cat, prof, rel); 
                insert issue;
                
             
                
              
               
                Test.setCurrentPageReference(new PageReference('Page.VF_RedMineCase')); 
                System.currentPageReference().getParameters().put('IssueId',issue.Id );            
                Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(prod);
                VF_Issue vf = new VF_Issue(teststandard);
                
                vf.getproductsList(); 
                vf.ProductSelected = prod.id;
                
                vf.getReleaseAndCategoryBsedOnProd();
                vf.CategorySelected = Cat.id;
               // vf.releaseSelected = Rel.id;
                vf.FixReleaseSelected = Rel.id;
                
                
                vf.SaveIssue();
                
                 
                 
                
                 
                
                SFDC_issue__c IssueUpd = [select Id from SFDC_issue__c where ID =:Issue.Id ];
                IssueUpd.Issue_Description__c = 'aaa';
                
                update IssueUpd;
                
                 Test.setCurrentPageReference(new PageReference('Page.VF_Issue')); 
                System.currentPageReference().getParameters().put('id',IssueUpd.Id );            
                Apexpages.Standardcontroller teststandard1 = new Apexpages.Standardcontroller(prod);
                VF_Issue vf1 = new VF_Issue(teststandard1);
               
               
                
                                
                pagereference p1 = vf1.selectclick();
                
                pagereference p11 = vf1.unselectclick();
                
                
                
                vf1.SaveIssueUpdate();
               
                 
                 
                 
                 vf1.CloseIssueUpdate();
                 
                 
                 
                 
            
                 
     }
    
    
                                  

}