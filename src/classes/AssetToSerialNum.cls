global class AssetToSerialNum implements Database.Batchable<sObject>
{

	global string query;
	global List<Serial_Number__c> UpdSNs;
	global map<String,Serial_Number__c> NewSNs;
	global integer ContNum = 0;
	// list of serial numbers ids passed from the trigger
	global List<id> NewSNs1;


	global database.querylocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(query );
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{  
		map<String,Serial_Number__c> NewSNs = new map<String,Serial_Number__c>(); 
		list<Serial_Number__c> serialNumbers_list = [select id, Dangerous_goods__c, Serial__c,Serial_upper__c, Asset__c,OEM_SN__c,RMA_type_new__c, Warranty_Status__c,
		Warranty_Date_Asset__c,Advanced_Replacement__c, Approval__c, Japan_Asset__c,reject_reason__c
		FROM Serial_Number__c WHERE Id in :NewSNs1 ];
		// set of Asset's Ids from the scope to retieve the related products
		set<String> assetsPartNumbers_set = new set<String>();
		// set of products names holding the products names of products 
		// with dangerous goods
		set<String> productsNames_set = new set<String>();
		
		system.debug('query : '+ query);
		for (sObject obj : scope){
			
			Asset2__c asset = (Asset2__c)obj;
			assetsPartNumbers_set.add(asset.Part_Number__c);
		}
		
		if(!assetsPartNumbers_set.isEmpty()){
		
			list<Product2> relatedProducts_List = DAL_Product.getProductsByPartNumbersForAssets(assetsPartNumbers_set);
		    
		    if (!relatedProducts_List.isEmpty()){
		    	
		    	for (Product2 product : relatedProducts_List){
		    		
		    		productsNames_set.add(product.Name);
		    	}
		    }
		}
		
		if (!serialNumbers_list.isEmpty()) {              
			
			for( Serial_Number__c SN : serialNumbers_list){
			
				NewSNs.put(SN.Serial_upper__c,SN);
			}   
			                                                        
			for(sObject s : scope)
			{ 
				Asset2__c ast = (Asset2__c)s;
				ContNum = +1;
	
				NewSNs.get(ast.Serial_Number__c).Asset__c = ast.id;
				NewSNs.get(ast.Serial_Number__c).Warranty_Date_Asset__c  = ast.SN_Warranty_Expire__c;    
				NewSNs.get(ast.Serial_Number__c).RMA_type_new__c = ast.RMA_type1__c;
				NewSNs.get(ast.Serial_Number__c).Japan_Asset__c = ast.Japan_Asset__c;
				if( ast.Asset_Status__c == 'Under Warranty') { NewSNs.get(ast.Serial_Number__c).Warranty_Status__c = 'Under Warranty';}                              
				if( ast.Asset_Status__c == 'Under Contract') { NewSNs.get(ast.Serial_Number__c).Warranty_Status__c = 'Under Warranty';}                              
	
				if( ast.Asset_Status__c == 'Out Of Warranty') 
				{ 
					NewSNs.get(ast.Serial_Number__c).Warranty_Status__c = 'Not under warranty';
					NewSNs.get(ast.Serial_Number__c).Approval__c = 'Not Approved';
					NewSNs.get(ast.Serial_Number__c).Reject_reason__c = 'Serial number is not under warranty';
				}                              
	
				if( ast.RMA_type1__c == 'Advanced Replacement') { NewSNs.get(ast.Serial_Number__c).Advanced_Replacement__c = TRUE;}
				if(ast.Is_OEM__c == 'Yes') {NewSNs.get(ast.Serial_Number__c).OEM_SN__c = TRUE;}
				
				if(productsNames_set.contains(ast.Part_Number__c)){
					
					NewSNs.get(ast.Serial_Number__c).Dangerous_goods__c = true;
				}
	
				//NewSNs.get(ast.Serial_Number__c).Warranty_Status__c = ast.Asset_Status__c;    
				//NewSNs.get(ast.Serial_Number__c).Warranty_Status__c = 'Under Warranty';                              
	
				/* NewSNs.get(ast.Serial_Number__c).RMA_type__c = ast.RMA_Type1__c;         
				NewSNs.get(ast.Serial_Number__c).Warranty_Valid_Date__c = ast.SN_Warranty_Expire__c;                                                                  
				NewSNs.get(ast.Serial_Number__c).Contract_Date__c = ast.Contract2_Date__c; 
				NewSNs.get(ast.Serial_Number__c).SLA_DATE__c = ast.SN_Warranty_Expire__c;*/
	
				system.debug('SN = ' + NewSNs.get(ast.Serial_Number__c));
			}
		}
		update(NewSNs.values());
		system.debug('VALUE = ' + NewSNs.values()[0]);

	}


	global void finish(Database.BatchableContext BC)
	{
		/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new String[] {'innag@mellanox.co.il'});
		mail.setReplyTo('batch@acme.com');
		mail.setSenderDisplayName('Batch Processing');
		mail.setSubject('Batch Process Completed');
		mail.setPlainTextBody('Query :'+query);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
	}
}