public with sharing class MPCProductFamilyOverrideController {
	String recordId;
	   
	public MPCProductFamilyOverrideController(ApexPages.StandardController 
		   controller) {recordId = controller.getId();}
	
	public boolean inCommunity() {
		return Network.getNetworkId() != null;
	}
	
	public PageReference redirect() {
		if (inCommunity()) 
		{
			PageReference customPage =  Page.SupportProductFamily;
			customPage.setRedirect(true);
			customPage.getParameters().put('pfid', recordId);
			return customPage;
		} else {
			return null; //otherwise stay on the same page  
		}
	}
}