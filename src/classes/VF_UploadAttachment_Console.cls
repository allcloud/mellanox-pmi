public with sharing class VF_UploadAttachment_Console {
	
	 public ID parentId ;
    public ID caseId;
    public String fName {get;set;}
    // Booleans representing if the RM Case Checkboxes should be visiable or not
	public Boolean visible1 {get;set;}
	public Boolean visible2 {get;set;}
	public Boolean visible3 {get;set;}
	public Boolean visible4 {get;set;}
	public Boolean visible5 {get;set;}
	 // Booleans representing the RM Case related Checkboxes  
	public Boolean RM1{get;set;}
	public Boolean RM2{get;set;}
	public Boolean RM3{get;set;}
	public Boolean RM4{get;set;}
	public Boolean RM5{get;set;}
	// Strings representing the RM Cases Names to display to the user
	public String RM1_Name{get;set;}
	public String RM2_Name{get;set;}
	public String RM3_Name{get;set;}
	public String RM4_Name{get;set;}
	public String RM5_Name{get;set;}
	public Boolean RM1Disabled{get;set;}
	public Boolean RM2Disabled{get;set;}
	public Boolean RM3Disabled{get;set;}
	public Boolean RM4Disabled{get;set;}
	public Boolean RM5Disabled{get;set;}
	
	public list<RmCase__c> rmCases_list{get;set;}
 	
    public Attachment attachment 
    {
        get 
        {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
    
  //Constructor
    public VF_UploadAttachment_Console()
    {
        parentId = Apexpages.currentPage().getParameters().get('parentId'); 
        caseId = Apexpages.currentPage().getParameters().get('caseId'); 
        
        // Booleans to determine if to display the RM checkboxes in the page or not
        visible1 = false;
        visible2 = false;
        visible3 = false;
        visible4 = false;
        visible5 = false;
        // Booleans to tedermine if the RM Checkboxes have been checked or not
        RM1 = false;
        RM2 = false;
        RM3 = false;
        RM4 = false;
        RM5 = false;
        RM1Disabled = false;
        RM2Disabled = false;
        RM3Disabled = false;
        RM4Disabled = false;
        RM5Disabled = false;
        
        // list of Case related RM Cases ordered by Craeted Date
        rmCases_list = new list<RmCase__c>();
        rmCases_list = [SELECT Id, RM_Subject__c, Name FROM RmCase__c WHERE sfcase__c =: caseId and Exist_in_Redmine__c =: false order By createdDate];
        
        if (rmCases_list.size() > 0)
        {
        	visible1 = true;
        	RM1_Name = 'RM1 - '+ rmCases_list[0].Name;
        	if (rmCases_list.size() > 1)
        	{
        		 visible2 = true;
        		 RM2_Name = 'RM2 - '+ rmCases_list[1].Name;
        		 if (rmCases_list.size() > 2)
        		 {
        		 	visible3 = true;
        		 	RM3_Name = 'RM3 - '+ rmCases_list[2].Name;
        		 	
        		 	if (rmCases_list.size() > 3)
	        		 {
	        		 	visible4 = true;
	        		 	RM4_Name = 'RM4 - '+ rmCases_list[3].Name;
	        		 	
	        		 	if (rmCases_list.size() > 4)
		        		 {
		        		 	visible5 = true;
		        		 	RM5_Name = 'RM5 - '+ rmCases_list[4].Name;
        		 
		        		 }
	        		 }
        		 }
        	}
        }
        
        system.debug('The sent Parent Id is :  ' + parentId); 
    }
   
    
    // This method creates new Case related Attachment, Private Attachment if needed and 'Attachment Integration' related to the Attachment on demand
    public PageReference upload() 
    {
    	String rmNumber = ApexPages.currentPage().getParameters().get('rmNumber');
        
        system.debug('rmNumber :' + rmNumber);
        
    	system.debug('RM1 81:'+ RM1);
    	system.debug('RM2 82:'+ RM2);
        Case c = new Case();
        Boolean NoRMCaseChosen = false;
        list<AttachmentIntegration__c> lst_AttIntegration = new list<AttachmentIntegration__c> ();
        if( Apexpages.currentPage().getParameters().get('caseId') != null)
         c = [Select c.RM_cases_count__c,c.Private_Attachments__c, c.Id,Next_Attachment_Integration_Number__c From Case c 
             where c.Id =: Apexpages.currentPage().getParameters().get('caseId')];
        
        if(c.Next_Attachment_Integration_Number__c==null)
        {
            c.Next_Attachment_Integration_Number__c =1;
        }                                                 
        
        if(c.RM_cases_count__c > 0)
        {
            system.debug('There is a RM Case');
            rmCases_list = [SELECT Id, RM_Subject__c FROM RmCase__c WHERE sfcase__c =: caseId order By createdDate];
            List<RmCase__c> rm_Case_Chosen_List = new list<RmCase__c>();
                        
            if (RM1)
            {
            	rm_Case_Chosen_List.add(rmCases_list[0]);
            }
            
            if (RM2)
            {
            	rm_Case_Chosen_List.add(rmCases_list[1]);
            }
            
            if (RM3)
            {
            	rm_Case_Chosen_List.add(rmCases_list[2]);
            }
            
            if (RM4)
            {
            	rm_Case_Chosen_List.add(rmCases_list[3]);
            }
            
            if (RM5)
            {
            	rm_Case_Chosen_List.add(rmCases_list[4]);
            }

            // At least one RM Case checkbox has been chosen
            if(rm_Case_Chosen_List != null && rm_Case_Chosen_List.size() > 0)
            {
            	Integer RMCounter = 0;
                for(RmCase__c rmc :rm_Case_Chosen_List)
                {     
              
                    AttachmentIntegration__c attInt = new AttachmentIntegration__c();
                    attInt.Case_Id__c = Apexpages.currentPage().getParameters().get('caseId');
                    
                    attInt.RM_Case__c = rmc.id;
                    if (RMCounter == 0)
                    {
                    	attInt.Source__c ='Salesforce';
                    }
                    
                    else
                    {
                    	attInt.Source__c ='Copy to Redmine';
                    }
                    RMCounter ++;
                    attInt.sync_flag__c = true;
                    attInt.Creator_name__c = string.valueOf(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
                    attInt.Attachment_Number__c =c.Next_Attachment_Integration_Number__c;
                    if(c.Private_Attachments__c == Apexpages.currentPage().getParameters().get('parentId'))
                        attInt.Is_Public__c = false;
                    else
                        attInt.Is_Public__c = true;
                    lst_AttIntegration.add(attInt);
                }   
            }
            
            // No RM Case checkbox has been chosen
            else
            {   system.debug('Elad inside 137');
            	AttachmentIntegration__c attInt = new AttachmentIntegration__c();
                attInt.Case_Id__c = Apexpages.currentPage().getParameters().get('caseId');
                
                attInt.Source__c ='Salesforce';
                attInt.sync_flag__c = false;
                attInt.Creator_name__c = string.valueOf(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
                attInt.Attachment_Number__c =c.Next_Attachment_Integration_Number__c;
                if(c.Private_Attachments__c == Apexpages.currentPage().getParameters().get('parentId'))
                    attInt.Is_Public__c = false;
                else
                    attInt.Is_Public__c = true;
                lst_AttIntegration.add(attInt);
                NoRMCaseChosen = true;
            }
        }
        
        else if(!NoRMCaseChosen)
        {
            system.debug('Elad inside 156');
            system.debug('There is NOOOOOOOOOO a RM Case');
            AttachmentIntegration__c attInt = new AttachmentIntegration__c();
            attInt.Case_Id__c = Apexpages.currentPage().getParameters().get('caseId');
            //attInt.RM_Case__c = rmc.id;
            attInt.Source__c ='Salesforce';
            attInt.sync_flag__c = false;
            attInt.Creator_name__c = string.valueOf(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
            attInt.Attachment_Number__c =c.Next_Attachment_Integration_Number__c;
            
            if(c.Private_Attachments__c == Apexpages.currentPage().getParameters().get('parentId'))
            {
                attInt.Is_Public__c = false;
            }
            
            else
            {
                attInt.Is_Public__c = true;
            }
            
            lst_AttIntegration.add(attInt);
        }
        
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = parentId; // the record the file is attached to
        //attachment.IsPrivate = true;
        
        system.debug('lst_AttIntegration : size :' + lst_AttIntegration.size());
 
        try 
        {
            attachment.Name = fName;
            system.debug('The name of the file is : ' + attachment.Name);
            insert attachment;
            if(lst_AttIntegration != null && lst_AttIntegration.size() > 0)
            {
             
             	list<attachment> size = [select bodyLength from attachment where id = :attachment.id];
               
                for(AttachmentIntegration__c attInt : lst_AttIntegration)
                {    system.debug('Elad inside 196 for loop');
                    attInt.Attachment_Id__c = attachment.Id;        
                    attInt.name = attachment.name;
                    attInt.size__c = size[0].bodyLength;                                                                                         
                }
                system.debug(' lst_AttIntegration  : ' + lst_AttIntegration);
                
                insert lst_AttIntegration;
                
                if(c.Next_Attachment_Integration_Number__c != null)
                {
                	c.Next_Attachment_Integration_Number__c++;
                }
                
                else
                {
                	c.Next_Attachment_Integration_Number__c =1 ;
                }
                
                update c;            
            }
            
            String hostname = ApexPages.currentPage().getHeaders().get('Host');
	        String caseIdStr = String.valueOf(Apexpages.currentPage().getParameters().get('caseId'));                       
            String absolutePath = 'https://' + hostname + '/apex/VF_CaseAttachments_Console?Id='+caseIdStr ;     
            
            //Pagereference p = new Pagereference('/'+Apexpages.currentPage().getParameters().get('caseId')+'?&inline=0');  Redirecting when VF is on the Case page
             Pagereference p = new Pagereference(absolutePath);   //Redirecting when VF is on the Console
                        
            p.setRedirect(true);
            system.debug('the page is  : ' + p);
            
               
            return p;
        }
        catch (DMLException e) 
        {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
              return null;
        } 
    }
    
    public PageReference Cancel() 
    {
        String hostname = ApexPages.currentPage().getHeaders().get('Host');
        String caseIdStr = String.valueOf(Apexpages.currentPage().getParameters().get('caseId'));
        String parentIdStr = String.valueOf(Apexpages.currentPage().getParameters().get('parentId'));
        
        //String absolutePath = 'https://' + hostname + '/'+Apexpages.currentPage().getParameters().get('caseId')+'?&inline=0';       Redirecting when VF is on the Case page 
        String absolutePath = 'https://' + hostname + '/apex/VF_CaseAttachments_Console?Id='+caseIdStr ;   //Redirecting when VF is on the Console
        
        Pagereference p = new Pagereference(absolutePath);
        
        p.setRedirect(true);
        system.debug('the page is  : ' + p);
        return p;
    }

}