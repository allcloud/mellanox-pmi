@isTest
public with sharing class test_VF_Internal_Comment_Email {
  
      static testMethod void Test_VF_Internal_Comment_Email() {
      
        //instance of object creator
          CLS_ObjectCreator obj = new CLS_ObjectCreator();
         
          Account acc = new Account(name = 'Elad test Account',support_center__c = 'Global', is_oracle_parent__c = 'Yes', type = 'OEM');
          insert acc;
          
          Contact con = obj.CreateContact(acc);
          insert con;
          
          Case cs = obj.Create_case(acc, con);
          insert cs;
          
          FM_Discussion__c discussion = new FM_Discussion__c();
          discussion.Case__c = cs.Id; 
          discussion.discussion__c = 'test discussion test discussion ';
          
          insert discussion;
          
          test.startTest();
          
          VF_CaseStatusChangedEmailTemplate vf = new VF_CaseStatusChangedEmailTemplate();
          VF_InternalCaseCommentsEmailTemplate vf1 = new VF_InternalCaseCommentsEmailTemplate();
          vf.caseId = cs.Id;
          vf1.caseId = cs.Id;
          vf.getAllBooleansSet();
          vf1.getAllBooleansSet();
          
          test.stopTest();
            
      }
}