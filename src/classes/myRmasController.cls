public class myRmasController{ 
 
  // the soql without the order and limit
  private String soql {get;set;}
  // the collection of contacts to display
  public List<BBB_ORDERS__C> orders {get;set;}
  String userID = UserInfo.getUserID();
  User activeUser = [Select Username,Rep_Access__c,Profile_text__c,Customer_Buyer__c,contactAccount__C,Exclude_Bill_to__c,UserType From User where ID= : userID limit 1]; 
  String userEmail = activeUser.Username;
  boolean superuser = activeUser.Customer_Buyer__c;
  string customerAccount= activeUser.contactAccount__C;
  string customerProfile=activeUser.Profile_text__c;
  string resellerAccess=activeUser.Rep_Access__c;
  string excludeBill=activeUser.Exclude_Bill_to__c;
  // the current sort direction. defaults to des
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to BBBSDT__C
  public String sortField {
    get  { if (sortField == null) {sortField = 'CUST_PO_NUMBER__C'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir; }
    set;
  }
 
  // init the controller and display some sample data when the page loads
  public myRmasController() {
    //KN added 05-29-13
    if (activeUser.UserType=='Standard'){
        soql = 'Select Line_type__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Transaction_QTY__c,Actual_Shipment_Date__c,ID,buyer_email__C,CSD__c from BBB_ORDERS__C where CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>700000 and S__C!=\'BKS\' and Order_type__c!=\'MTI Administrative RMA\' and (BBBSDT__C>=LAST_N_DAYS:30 or BBBSDT__C=NULL)';       
    }
    // end adding new condition
    else if (superuser!=true && customerProfile!='MyMellanox Distributor User' ) { 
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Country__c,Ship_to_Account__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>700000 and S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL) and  buyer_email__C <>\'\' and buyer_email__C=\'' + userEmail +'\'';
    }else if (customerProfile!='MyMellanox Distributor User') {
        if (excludeBill==''){
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\'';
        }else{
         soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\' and BILL_TO_ACCOUNT__c!=\'' + excludeBill  +'\'';
  
        }
    }else if (customerProfile=='MyMellanox Distributor User'){
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c=\'' + customerAccount  +'\'';
    }
    if (resellerAccess=='Paragon'){
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and REP_COMISSIONS__C like \'%PT%\'';
        
    }else if (resellerAccess=='Universal'){
        soql = 'Select Line_type__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and REP_COMISSIONS__C like \'%UT%\'';
    } 
    //KN added 10-23-14
    if (customerAccount=='BOSTON LTD'){
        customerAccount = 'BOSTON LIMITED';
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:90 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';    
    }
    //
    runQuery();
  }
 
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
   public PageReference export() 
   {
        return page.MyOrdersCSV;
    }
  // runs the actual query
  public void runQuery() {
 
    try {
      orders = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 1000');
    } catch (Exception e) {
            ApexPages.addMessages(e);
     // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.message));
    }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String CUST_PO_NUMBER = Apexpages.currentPage().getParameters().get('CUST_PO_NUMBER');
    String SO_NUMBER_TEXT = Apexpages.currentPage().getParameters().get('SO_NUMBER_TEXT');
    String Ref_only_PN = Apexpages.currentPage().getParameters().get('Ref_only_PN');
    String Ordered_item = Apexpages.currentPage().getParameters().get('Ordered_item');
    //KN added 05-29-13
    if (activeUser.UserType=='Standard'){
        //soql = 'Select End_Customer_Name__c,Ship_to_Country__c,Ship_to_Account__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>700000 and S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)';
        soql = 'Select Line_type__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Transaction_QTY__c,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>700000 and S__C!=\'BKS\' and Order_type__c!=\'MTI Administrative RMA\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)';              
    }
    // end adding new condition
    else if (superuser!=true && customerProfile!='MyMellanox Distributor User' ) { 
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Country__c,Ship_to_Account__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and  SALES_ORDER_NUMBER__C>900000 and S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL) and  buyer_email__C <>\'\' and buyer_email__C=\'' + userEmail +'\'';
    }else if (customerProfile!='MyMellanox Distributor User') {
 if (excludeBill==''){
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\'';
        }else{
         soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\' and BILL_TO_ACCOUNT__c!=\'' + excludeBill  +'\'';
  
        }    }else if (customerProfile=='MyMellanox Distributor User'){
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL) and BILL_TO_ACCOUNT__c=\'' + customerAccount  +'\'';
    }
    if (resellerAccess=='Paragon'){
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL) and REP_COMISSIONS__C like \'%PT%\'';
        
    }else if (resellerAccess=='Universal'){
        soql = 'Select Line_type__c,End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Transaction_QTY__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,ID,buyer_email__C from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C>900000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL) and REP_COMISSIONS__C like \'%UT%\'';
    } 
   
      
    if (!CUST_PO_NUMBER.equals(''))
      soql += ' and CUST_PO_NUMBER__C LIKE \''+String.escapeSingleQuotes(CUST_PO_NUMBER)+'%\'';
    if (!SO_NUMBER_TEXT.equals(''))
      soql += ' and SO_NUMBER_TEXT__C LIKE \''+String.escapeSingleQuotes(SO_NUMBER_TEXT)+'%\'';
    if (!Ref_only_PN.equals(''))
      soql += ' and Ref_only_PN__c LIKE \''+String.escapeSingleQuotes(Ref_only_PN)+'%\'';  
    if (!Ordered_item.equals(''))
      soql += ' and Ordered_item__C LIKE \''+String.escapeSingleQuotes(Ordered_item)+'%\'';  
 
    // run the query again
    runQuery();
 
    return null;
  }
 
  // use apex describe to build the picklist values

 
}