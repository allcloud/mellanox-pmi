public class VF_Deal_Flow_Scoring {
    
    public String theScoreStr {get;set;}
    public list<SelectOption> scoring_List {get;set;}
    public Deal_Score__c therScore {get;set;}
    private Id parentRoundId;
    private String userId;
    private User currentUser;
    public boolean displayErrorSection {get;set;}
    
    public VF_Deal_Flow_Scoring(ApexPages.standardController controller) {
        
        parentRoundId = controller.getId();
        userId = Apexpages.currentPage().getParameters().get('userName');
        system.debug('userId : ' + userId );
        therScore = new Deal_Score__c();
        currentUser = [SELECT Id, Name, Email FROM User WHERE Id =: userId];
        
        Deal_Round__c relatedRound = [SELECT Id, (SELECT Id FROM Scores__r WHERE Rated_by__c =: userId) FROM Deal_Round__c WHERE Id =: parentRoundId];
        
        if (!relatedRound.Scores__r.isEmpty()) {
            
            displayErrorSection = true;
        }
    }
    
    public list<SelectOption> getScores() {
        
        scoring_List = new list<SelectOption>();
        
        for (Integer i = 0 ; i < 5 ; i ++) {
            
            scoring_List.add(new SelectOption (String.valueOf(i+1), String.valueOf(i+1)));
        }
        
        return scoring_List;
    }
    
    public PageReference submitForm() {
        
         
        therScore.Deal_Rounds__c = parentRoundId;
        therScore.Name = currentUser.Name;
        therScore.Rated_by__c = userId;
        therScore.Score__c = integer.valueOf(theScoreStr);
        therScore.User_Mail__c = currentUser.Email;
        system.debug('therScore : ' + therScore);
        
        try {   
            insert therScore;
        }
        
        catch(Exception e) {
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'We cannot create a new Deal score : ' + e.getmessage()));
            return null;
        }
        return new PageReference('/apex/VF_CommunitySelfReg_success_page');
    }

}