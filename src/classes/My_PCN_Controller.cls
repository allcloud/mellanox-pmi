public class My_PCN_Controller {
	User curr_user;
	public boolean run_once {get; set;}
	public boolean is_submitted {get;set;}
	public boolean exist_previous_comments {get;set;}
	public boolean show_comments {get;set;}
	public boolean exist_pcns {get;set;}
	Set<ID> set_PCN_IDs = new Set<ID>();
	public List<PCN_Comment__c> pcncomments {get;set;}
	public List<PCN_Contacts__c> pcn_contacts {get;set;}
	public List<PCN_Contacts__c> pcn_contacts_distinct {get;set;}
	public Map<ID,List<PCN_Contacts__c>> map_PCN_lst_pcncontacts = new Map<ID,List<PCN_Contacts__c>>();
	List<PCN_Contacts__c> tmp_lst_pcncontacts = new List<PCN_Contacts__c>();
	List<PCN_Contacts__c> pcn_contacts_updates = new List<PCN_Contacts__c>();
	List<PCN_Comment__c> pcncomments_inserts = new List<PCN_Comment__c>();
	List<Attachment> lst_attachments = new List<Attachment>();
	public String pcn_search {get; set;}
	public String pcn_header {get; set;}
	public String filter_by {get; set;}
	//public boolean show_approvedPCN {get; set;}
	public boolean show_all_company_PCN {get; set;}
	Set<ID> PCN_IDs = new Set<ID>();
	
	Map<ID,String> map_PCN_to_comments = new Map<ID,String>();
		
	public class PCN_row {
		public ID attachmentID {get; set;}
		public String PCN_Name {get;set;}
		public String PCN_ID {get;set;}
		public String PCN_ContactID {get;set;}
		public String PCN_Desc {get;set;}
		public String PCN_Type {get;set;}
		public String PCN_Status {get;set;}
		public Date PCN_PublishDate {get;set;}
		//public Date PCN_ApprovalDate {get;set;}
		public boolean isApproved_beforeSubmit {get;set;}
		public boolean isApproved {get;set;}
		public boolean isRejected_beforeSubmit {get;set;}
		public boolean isRejected {get;set;}
		public boolean show_Checkbox {get;set;}
		public String comments {get;set;}
		public String prevcomments {get;set;}
		public PCN_row(){
			isApproved = false;
			isApproved_beforeSubmit = false;
			//show_all_company_PCN = false;
		}
	}
	public List<PCN_row> allrows {get;set;} 
	//public List<PCN_row> release_rows {get;set;}
	public My_PCN_Controller(){
		filter_by = 'Approval Required';
		pcn_search = '';
		pcn_header = 'Approval Required PCNs: ';
		run_once = false;
		curr_user = [select id,Email,Name,PCN_Approver__c,contactAccount__c from User where ID=:UserInfo.getUserID()];
		show_all_company_PCN = false;	
	}
	public void init(){
		//if(run_once == false)
		//	add_sharingrules();
		//run_once = true;
		
		is_submitted = false;
		exist_previous_comments = false;
		show_comments = false;
		exist_pcns = false;
		set_PCN_IDs.clear();
		map_PCN_lst_pcncontacts.clear();
		map_PCN_to_comments.clear();
		//map_PCNID_AttachmentID.clear();
		allrows = new List<PCN_row>();
		//release_rows = new List<PCN_row>();
		pcn_contacts = new List<PCN_Contacts__c>();
		pcn_contacts_distinct = new List<PCN_Contacts__c>();
		PCN_IDs = new Set<ID>();
		//curr_user = [select id,Email,Name,PCN_Approver__c from User where ID=:UserInfo.getUserID()];
		//Only display comments from the log-in user or Matt's comments that were replied to the log-in user
		//05-13-14
		pcncomments = [select PCN__c,PCN_Name__c,PCN_Description__c,Comments__c,Create_Date__c 
						From PCN_Comment__c 
						where Contact_Email__c = :curr_user.Email 
								//and (OwnerID=:UserInfo.getUserID() OR OwnerID='005500000011tXL') 
						      //order by PCN__c asc, CreatedDate desc limit 20];
						      order by CreatedDate desc limit 50];
		String s;
		if (pcncomments!=null && pcncomments.size()>0) {
			exist_previous_comments = true;
			for (PCN_Comment__c p : pcncomments) {
				s='';
				if(map_PCN_to_comments.get(p.PCN__c) == null) {
					s = p.Create_Date__c.year() + '-' + p.Create_Date__c.month() + '-' + p.Create_Date__c.day() + ': '+ p.Comments__c;
					map_PCN_to_comments.put(p.PCN__c,s);
				}
				else {
					s = map_PCN_to_comments.get(p.PCN__c) + '; ' + p.Create_Date__c.year() + '-' + p.Create_Date__c.month() + '-' + p.Create_Date__c.day() + ': '+ p.Comments__c;
					map_PCN_to_comments.put(p.PCN__c,s);
				}
			}				
		}			      
		//
		//KN - 07-08-14 - Create sharing rules since PCN object is Private
		
		//end KN 07-08-14
				
		//one contact might have multiple PCN_Contacts__c records because 1 contact might be responsible for multiple products within the same PCN			
		//
		if(pcn_search != null && pcn_search != ''){
			String search_criteria = '%' + pcn_search + '%';
			pcn_contacts = [Select ID,Product__c,PCN_Name__c,PCNType__c,PCNStatus__c,PCNPublishDate__c,PCN_Description__c,PCN__c,Contact__c,
							p.Approved__c,p.Comments__c,Customer_Approval_Date__c,Rejected__c
							From PCN_Contacts__c p 
						where Contact_Email__c = :curr_user.Email and PCN_Name__c like :search_criteria
						and PCNStatus__c = 'Pending'
						and PCN__r.Send_Emails__c = true
						and (PCNType__c = 'FYI' or PCNType__c = 'Approval Required')
						];
			//In case not found first search, search all PCNs for the company
			if(pcn_contacts == null || pcn_contacts.size()==0){
				filter_by = 'ALL';
				pcn_contacts = [Select ID,Product__c,PCN_Name__c,PCNType__c,PCNStatus__c,PCNPublishDate__c,PCN_Description__c,PCN__c,Contact__c,
							p.Approved__c,p.Comments__c,Customer_Approval_Date__c,Rejected__c
							From PCN_Contacts__c p 
						where PCN_Name__c like :search_criteria
						and PCNStatus__c = 'Release'
						and PCN__r.Send_Emails__c = true
						and (PCNType__c = 'FYI' or PCNType__c = 'Approval Required')
						];
			}
		}
		//View past approved PCNs
		else if(filter_by == 'Past Approval'){
			pcn_contacts = [Select ID,Product__c,PCN_Name__c,PCNType__c,PCNStatus__c,PCNPublishDate__c,PCN_Description__c,PCN__c,Contact__c,
							p.Approved__c,p.Comments__c,Customer_Approval_Date__c,Rejected__c 
							From PCN_Contacts__c p 
						where Contact_Email__c = :curr_user.Email and (Approved__c = true or Rejected__c = true)
						order by PCNPublishDate__c desc];
		}
		else if(filter_by == 'Approval Required') {
				pcn_contacts = [Select ID,Product__c,PCN_Name__c,PCNType__c,PCNStatus__c,PCNPublishDate__c,PCN_Description__c,PCN__c,Contact__c,
								p.Approved__c,p.Comments__c,Customer_Approval_Date__c,Rejected__c 
								From PCN_Contacts__c p 
							//where Contact_Email__c = :curr_user.Email and (PCNType__c = 'FYI' or PCNType__c = 'Approval Required')
							where Contact_Email__c = :curr_user.Email and PCNType__c = :filter_by
							and PCNStatus__c = 'Pending' and Approved__c = false and Rejected__c = false
							and PCN__r.Send_Emails__c = true
							order by PCNPublishDate__c desc];
		}
		//Only run first time, no approval-required PCNs, then search all FYI PCNs 
		//then display all company-wide PCNs
		if( (run_once == false && pcn_contacts.size() == 0) || filter_by == 'FYI') {
			pcn_header = 'FYI PCNs:';
			filter_by = 'FYI';
			pcn_contacts = [Select ID,Product__c,PCN_Name__c,PCNType__c,PCNStatus__c,PCNPublishDate__c,PCN_Description__c,PCN__c,Contact__c,
								p.Approved__c,p.Comments__c,Customer_Approval_Date__c,Rejected__c 
								From PCN_Contacts__c p 
							//where Contact_Email__c = :curr_user.Email and (PCNType__c = 'FYI' or PCNType__c = 'Approval Required')
							where Contact_Email__c = :curr_user.Email and PCNType__c = :filter_by
							and PCN__r.Send_Emails__c = true
							order by PCNPublishDate__c desc];
		}		
		if( (run_once == false && pcn_contacts.size() == 0) || filter_by=='ALL'){
			pcn_header = 'All My Company PCNs:';
			filter_by = 'ALL';
			//All PCNs across company
			if(pcn_contacts != null)
				pcn_contacts.clear();
			List<AggregateResult> aggregate_pcns = new List<AggregateResult>([select PCN__c from PCN_Contacts__c 
											where PCNStatus__c = 'Release' AND Company__c = :curr_user.contactAccount__c
											group by PCN__c
											//order by PCNPublishDate__c desc
											]);
			for (AggregateResult ag : aggregate_pcns) {
				PCN_IDs.add((ID)ag.get('PCN__c'));
			} 									
		}
		
		//
		for (PCN_Contacts__c p : pcn_contacts){
			if(!set_PCN_IDs.contains(p.PCN__c)){
				set_PCN_IDs.add(p.PCN__c);
				pcn_contacts_distinct.add(p);
			}
			if(map_PCN_lst_pcncontacts.get(p.PCN__c)==null){
				tmp_lst_pcncontacts = new List<PCN_Contacts__c>();
				tmp_lst_pcncontacts.add(p);
				map_PCN_lst_pcncontacts.put(p.PCN__c,tmp_lst_pcncontacts);											
			}
			else {
				tmp_lst_pcncontacts = map_PCN_lst_pcncontacts.get(p.PCN__c);
				tmp_lst_pcncontacts.add(p);
				map_PCN_lst_pcncontacts.put(p.PCN__c,tmp_lst_pcncontacts);
			}
		}
		//Get list of attachments
		lst_attachments = [select id, ContentType, ParentID from Attachment where ContentType = 'application/pdf' and ParentID in :set_PCN_IDs];
		Map<ID,ID> map_PCNID_AttachmentID = new Map<ID,ID>();
		for (Attachment a : lst_attachments){
			map_PCNID_AttachmentID.put(a.ParentID,a.ID);
		}
		//
		//Build row for display
		if(pcn_contacts_distinct != null && pcn_contacts_distinct.size() > 0){
			for (PCN_Contacts__c p : pcn_contacts_distinct){
				PCN_row row1 = new PCN_row();
				row1.PCN_Name = p.PCN_Name__c;
				row1.PCN_ID = p.PCN__c;
				//Assign to an old pdf PCN to avoid it breaks if no pdf found 
				row1.attachmentID = map_PCNID_AttachmentID.get(p.PCN__c) != null ? map_PCNID_AttachmentID.get(p.PCN__c) : '00P5000000Bt3c3';  
				row1.PCN_Desc = p.PCN_Description__c;
				row1.PCN_Type = p.PCNType__c;
				row1.PCN_Status = p.PCNStatus__c;
				row1.PCN_PublishDate = p.PCNPublishDate__c;
				//row1.PCN_ApprovalDate = p.Customer_Approval_Date__c;
				row1.isApproved = p.Approved__c;
				row1.isApproved_beforeSubmit = p.Approved__c;
				row1.isRejected = p.Rejected__c;
				row1.isRejected_beforeSubmit = p.Rejected__c;
				row1.comments = ''; 
				if(map_PCN_to_comments != null && map_PCN_to_comments.get(p.PCN__c) != null)
					row1.prevcomments = map_PCN_to_comments.get(p.PCN__c);
				row1.PCN_ContactID = p.ID;
				row1.show_Checkbox = (p.Approved__c || p.PCNType__c=='FYI' || curr_user.PCN_Approver__c!='Yes') ? true : false;
				
				allrows.add(row1);
			}
		}
		else if (PCN_IDs != null && PCN_IDs.size() > 0){
			for (PCN__c p : [select ID,Name,PCN_Description__c,Publish_Date__c,Status__c,Type__c 
								from PCN__c where ID in :PCN_IDs AND Status__c = 'Release' and Publish_Date__c >= :System.today().addmonths(-18)
								order by Type__c asc, Publish_Date__c desc]) {
				PCN_row row1 = new PCN_row();
				row1.PCN_Name = p.Name;
				row1.PCN_ID = p.ID;
				//Assign to an old pdf PCN to avoid it breaks if no pdf found 
				row1.attachmentID = map_PCNID_AttachmentID.get(p.ID) != null ? map_PCNID_AttachmentID.get(p.ID) : '00P5000000Bt3c3';  
				row1.PCN_Desc = p.PCN_Description__c;
				row1.PCN_Type = p.Type__c;
				row1.PCN_Status = p.Status__c;
				row1.PCN_PublishDate = p.Publish_Date__c;
				//row1.PCN_ApprovalDate = p.Customer_Approval_Date__c;
				allrows.add(row1);
			}	
		}		
System.debug('...KN allrows===' + allrows);		
		run_once = true;
		if (allrows != null && allrows.size()>0)
			exist_pcns = true;				
System.debug('...KN exist_pcns===' + exist_pcns);				
	}
	public pagereference view_comments(){
		show_comments = true;
		return null;
	}
	public pagereference search(){
		pcn_header = 'PCN Search Results: ';
		run_once = true;
		filter_by = 'searchbyPCN';
		init();
		return null;
	}
	public pagereference view_Approved_PCN(){
		pcn_header = 'Past Approved PCNs:';
		run_once = true;
		filter_by = 'Past Approval';
		pcn_search = '';
		init();
		return null;
	}
	public pagereference view_FYI_PCN(){
		pcn_header = 'FYI PCNs:';
		filter_by = 'FYI';
		pcn_search = '';
		run_once = true;
		init();
		return null;
	}
	//Allow view all PCNs in my company
	public pagereference view_AllCompany_PCN(){
		pcn_header = 'All My Company PCNs:';
		filter_by = 'ALL';
		pcn_search = '';
		run_once = true;
		init();
		return null;
	}
	//
	public pagereference view_ApprovalRequired_PCN(){
		pcn_header = 'Approval Required PCNs: ';
		filter_by = 'Approval Required';
		pcn_search = '';
		run_once = true;
		init();
		return null;
	}
	public pagereference save(){
		pcn_contacts_updates.clear();
		pcncomments_inserts.clear();
		Integer ii, jj;
		
		for (PCN_row pr : allrows){
			if(pr.isApproved == true && pr.isRejected == true){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'A PCN cannot have both Approved and Rejected checkboxes both checked. Please review.!');
				ApexPages.addMessage(myMsg);		
				return null;	
			}
			if(pr.isRejected_beforeSubmit==false && pr.isRejected == true && (pr.comments == null || pr.comments == '') ){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Please provide Comments when rejecting a PCN.!');
				ApexPages.addMessage(myMsg);		
				return null;	
			}
			if(pr.isApproved_beforeSubmit==false && pr.isApproved==true){
				if(map_PCN_lst_pcncontacts!=null && map_PCN_lst_pcncontacts.get(pr.PCN_ID)!=null){
					ii = 0;
					for (PCN_Contacts__c p2 : map_PCN_lst_pcncontacts.get(pr.PCN_ID)){
						if(ii == 0) p2.send_Notification__c = true;
						p2.Approved__c = true;
						if (pr.isRejected==false)
							p2.Rejected__c = false;	
						p2.Customer_Approval_Date__c = System.today();
						pcn_contacts_updates.add(p2);
						ii++;
					}
				}
			}
			//Rejected PCN
			if(pr.isRejected_beforeSubmit==false && pr.isRejected==true){
				if(map_PCN_lst_pcncontacts!=null && map_PCN_lst_pcncontacts.get(pr.PCN_ID)!=null){
					jj = 0;
					for (PCN_Contacts__c p2 : map_PCN_lst_pcncontacts.get(pr.PCN_ID)){
						if(jj == 0) p2.send_Notification__c = true;
						p2.Rejected__c = true;
						pcn_contacts_updates.add(p2);
						jj++;
					}
				}
			}
			//
			if(pr.comments!=null && pr.comments!=''){
				pcncomments_inserts.add(new PCN_Comment__c(PCN__c=pr.PCN_ID,
											//PCN_Product__c=p.Product__c,
											PCN_Contacts__c= pr.PCN_ContactID,
											Comments_from_Portal__c=true,
											Comments__c=pr.comments));	
			}
		}
		if(pcn_contacts_updates.size()>0)
			update pcn_contacts_updates;
		if(pcncomments_inserts.size()>0)
			insert pcncomments_inserts;		
		
		is_submitted = true;				
		return null;	
	}
	public pagereference view_PCN(){
		PageReference pg = new PageReference('/apex/My_PCN');
        pg.setRedirect(true);
        return pg;
	}
}