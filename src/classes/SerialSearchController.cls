public without sharing class SerialSearchController {
 
  private ApexPages.StandardController controller {get; set;}
  public List<Asset2__c> searchResults {get;set;}
  public string searchText {get;set;}
  public Integer UrlStatus {get;set;}
 
  // standard controller - could also just use custom controller
  public SerialSearchController(ApexPages.StandardController controller) { }
 
  // fired when the search button is clicked
  public PageReference search() {
   
   	 String qry = 'select id, name from Asset2__c where Serial_Number__c =  \'' + searchText + '\'';
     searchResults = Database.query(qry);
    
    system.debug('searchResults : ' + searchResults);
     if(searchResults.isempty() ) {
      
      	UrlStatus = 0;
     }
 
	 else {
		
		 String startUrl = System.currentPageReference().getParameters().get('startURL');
         //return Site.login('_c1@mellanox.com.test', 'mlnx3471', startUrl);   // Sandbox User
         return Site.login('support@mellanox.com', 'mlnx3471', startUrl);   // Production User
	 }
	  
	  return null;
  }
 
 /* 
 
  // takes user back to main record
  public PageReference cancel() {
    return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
  }
 */
}