global class Product_Populate_DB_CR_Price implements Schedulable{
	global void execute(SchedulableContext SC) {
		Set<ID> PB_IDs = new Set<ID>();
		PB_IDs.add('01s500000006J7K'); //Certified RS
		PB_IDs.add('01s500000006J09'); //DB
		PB_IDs.add('01s500000006J7P'); //DB1
		PB_IDs.add('01s500000006J7F'); //OEM
		PB_IDs.add('01s5000000062WY'); //Standard PB
		//
		Set<ID> prodIDs = new Set<ID>();
    	List<PricebookEntry> all_pbes = new List<PricebookEntry>([select id,Name, Product2ID, Pricebook2ID,UnitPrice 
    																from PricebookEntry 
																	where Product2.IsActive = true 
																	and Pricebook2ID = :PB_IDs]);
		for (PricebookEntry p : all_pbes){
			prodIDs.add(p.Product2ID);
		}
		
		Map<ID,Product2> map_prods = new Map<ID,Product2>([select id,DB_Price__c,DB1_Price__c,Certified_RSL_Price__c,OEM_Price__c,Std_Price__c,Date_Refresh_Pricing__c,
																Prev_DB_Price__c,Prev_DB1_Price__c,Prev_CR_Price__c,Prev_OEM_Price__c,Prev_STD_Price__c 
																from Product2 where id in :prodIDs]);
		boolean ischanged = false;
		Set<ID> prod_IDs = new Set<ID>();
		List<Product2> products_updates = new List<Product2>();
																			
		for (PricebookEntry p : all_pbes){
			ischanged = false;
			if(p.Pricebook2id == '01s500000006J7K'){ 	//Certified RS
				if(map_prods.get(p.Product2ID) != null && map_prods.get(p.Product2ID).Certified_RSL_Price__c != p.UnitPrice){
					map_prods.get(p.Product2ID).Prev_CR_Price__c = map_prods.get(p.Product2ID).Certified_RSL_Price__c;
					map_prods.get(p.Product2ID).Certified_RSL_Price__c = p.UnitPrice;
					ischanged = true;
				}
			}
			else if(p.Pricebook2id == '01s500000006J09'){ 	//DB
				if(map_prods.get(p.Product2ID) != null && map_prods.get(p.Product2ID).DB_Price__c != p.UnitPrice){
					map_prods.get(p.Product2ID).Prev_DB_Price__c = map_prods.get(p.Product2ID).DB_Price__c;
					map_prods.get(p.Product2ID).DB_Price__c = p.UnitPrice;
					ischanged = true;
				}
			}	
			else if(p.Pricebook2id == '01s500000006J7P'){ 	//DB1
				if(map_prods.get(p.Product2ID) != null && map_prods.get(p.Product2ID).DB1_Price__c != p.UnitPrice){
					map_prods.get(p.Product2ID).Prev_DB1_Price__c = map_prods.get(p.Product2ID).DB1_Price__c;
					map_prods.get(p.Product2ID).DB1_Price__c = p.UnitPrice;
					ischanged = true;
				}
			}
			else if(p.Pricebook2id == '01s500000006J7F'){ 	//OEM
				if(map_prods.get(p.Product2ID) != null && map_prods.get(p.Product2ID).OEM_Price__c != p.UnitPrice){
					map_prods.get(p.Product2ID).Prev_OEM_Price__c = map_prods.get(p.Product2ID).OEM_Price__c;
					map_prods.get(p.Product2ID).OEM_Price__c = p.UnitPrice;
					ischanged = true;
				}
			}
			else if(p.Pricebook2id == '01s5000000062WY'){ 	//Std Price
				if(map_prods.get(p.Product2ID) != null && map_prods.get(p.Product2ID).Std_Price__c != p.UnitPrice){
					map_prods.get(p.Product2ID).Prev_STD_Price__c = map_prods.get(p.Product2ID).Std_Price__c;
					map_prods.get(p.Product2ID).Std_Price__c = p.UnitPrice;
					ischanged = true;
				}
			}
			if(ischanged){
				if( !prod_IDs.contains(p.Product2ID) ) {
					prod_IDs.add(p.Product2ID);
					map_prods.get(p.Product2ID).Date_Refresh_Pricing__c = System.today();
					products_updates.add(map_prods.get(p.Product2ID));
				}
			}				
		}
		if(products_updates!=null && products_updates.size()>0)
			update products_updates;
	}
}