public class Disty_Pricebook_Export {
	List<Product2> prods;
	public List<Product2> adapter_cards {get;set;}
	public List<Product2> appliance_systems {get;set;}
	public List<Product2> cables {get;set;}
	public List<Product2> EDR_Chassis {get;set;}
	public List<Product2> EDR_Edge {get;set;}
	public List<Product2> ETH_Switch {get;set;}
	public List<Product2> FDR_Chassis {get;set;}
	public List<Product2> FDR_Edge {get;set;}
	public List<Product2> Gateway_system {get;set;}
	public List<Product2> HW_Support_1 {get;set;}
	public List<Product2> HW_Support_2 {get;set;}
	public List<Product2> HW_Extended_Warranty {get;set;}
	public List<Product2> Last_Time_Buy {get;set;}
	public List<Product2> Professional_Services {get;set;}
	public List<Product2> QDR_DDR_Edge {get;set;}
	public List<Product2> QDR_Chassis {get;set;}
	public List<Product2> SW_SWSupport {get;set;}
	
	public String xlsHeader {
	        get {
	            String strHeader = '';
	            strHeader += '<?xml version="1.0"?>';
	            strHeader += '<?mso-application progid="Excel.Sheet"?>';
	            return strHeader;
	        }
	    }
	 
	public Disty_Pricebook_Export(){
		adapter_cards = new List<Product2>();
		appliance_systems = new List<Product2>();
		cables = new List<Product2>();
		EDR_Chassis = new List<Product2>();
		EDR_Edge = new List<Product2>();
		ETH_Switch = new List<Product2>();
		FDR_Chassis = new List<Product2>();
		FDR_Edge = new List<Product2>();
		Gateway_system = new List<Product2>();
		HW_Support_1 = new List<Product2>();
		HW_Support_2 = new List<Product2>();
		HW_Extended_Warranty = new List<Product2>();
		Last_Time_Buy = new List<Product2>();
		Professional_Services = new List<Product2>();
		QDR_DDR_Edge = new List<Product2>();
		QDR_Chassis = new List<Product2>();
		SW_SWSupport = new List<Product2>();
		
	}
	public void init_internalPB(){
		prods = [select Classification__c,Name,Description,MSRP__c,DB_Price__c,DB1_Price__c,Certified_RSL_Price__c,Std_Price__c,OEM_Price__c,
					Disty_Status__c,License_Symbol__c,ECCN__c,EAN_Code__c,Availability__c,Harmonized_Code__c,Master_Box_Weight_kg__c,
					Master_Box_Dimension__c,Single_Box_Weight__c,Single_Box_Dimension__c,Disty_MOQ__c,Extended_Warranty_Support_X_Ref__c
	    			from Product2 where isactive = true
	    								and Do_Not_Release_to_Published_Price_List__c = false
	    								and ( Customer__c = '' or Customer__c = null) 
	    								and ( OEM_PARTNER__c = '' or OEM_PARTNER__c = null) 
	    							order by Classification__c,Name];
		for(Product2 p : prods) {
			if(p.Classification__c == 'Adapter Cards')
				adapter_cards.add(p);
			else if (p.Classification__c == 'Appliance Systems')
				appliance_systems.add(p);
			else if (p.Classification__c == 'Cables')
				cables.add(p);
			else if (p.Classification__c == 'EDR Chassis Switches')
				EDR_Chassis.add(p);
			else if (p.Classification__c == 'EDR Edge Switches')
				EDR_Edge.add(p);
			else if (p.Classification__c == 'ETH Switches')
				ETH_Switch.add(p);
			else if (p.Classification__c == 'FDR Chassis Switches')
				FDR_Chassis.add(p);
			else if (p.Classification__c == 'FDR Edge Switches')
				FDR_Edge.add(p);
			else if (p.Classification__c == 'Gateway Systems')
				Gateway_system.add(p);
			else if (p.Classification__c == 'Hardware Support') {
				if(HW_Support_1.size() < 1000)
					HW_Support_1.add(p);
				else
					HW_Support_2.add(p);
			}
			else if (p.Classification__c == 'HW Extended Warranty')
				HW_Extended_Warranty.add(p);
			else if (p.Classification__c == 'Last Time Buy')
				Last_Time_Buy.add(p);
			else if (p.Classification__c == 'Professional Services')
				Professional_Services.add(p);
			else if (p.Classification__c == 'QDR / DDR Edge Switches')
				QDR_DDR_Edge.add(p);
			else if (p.Classification__c == 'QDR Chassis Switches')
				QDR_Chassis.add(p);
			else if (p.Classification__c == 'Software and SW Support')
				SW_SWSupport.add(p);
		}
	}
	public void init(){
		prods = [select Classification__c,Name,Description,MSRP__c,DB_Price__c,DB1_Price__c,Certified_RSL_Price__c,Disty_Status__c,
	    			License_Symbol__c,ECCN__c,EAN_Code__c,Availability__c,Harmonized_Code__c,Master_Box_Weight_kg__c,
	    			Master_Box_Dimension__c,Single_Box_Weight__c,Single_Box_Dimension__c,Disty_MOQ__c,Extended_Warranty_Support_X_Ref__c
	    			from Product2 where isactive = true 
	    								and Do_Not_Release_to_Published_Price_List__c = false
	    								//and ( Customer__c = '' or Customer__c = null) 
	    								//and ( OEM_PARTNER__c = '' or OEM_PARTNER__c = null)
	    								and (Disty_Status__c = 'Released' or Disty_Status__c = 'Released - Dell Excluded' or 
	    								Disty_Status__c = 'DELL Distribution ONLY' or Disty_Status__c='JAPAN Distribution ONLY')
	    							order by Classification__c,Name];
		for(Product2 p : prods) {
			if(p.Classification__c == 'Adapter Cards')
				adapter_cards.add(p);
			else if (p.Classification__c == 'Appliance Systems')
				appliance_systems.add(p);
			else if (p.Classification__c == 'Cables')
				cables.add(p);
			else if (p.Classification__c == 'EDR Chassis Switches')
				EDR_Chassis.add(p);
			else if (p.Classification__c == 'EDR Edge Switches')
				EDR_Edge.add(p);
			else if (p.Classification__c == 'ETH Switches')
				ETH_Switch.add(p);
			else if (p.Classification__c == 'FDR Chassis Switches')
				FDR_Chassis.add(p);
			else if (p.Classification__c == 'FDR Edge Switches')
				FDR_Edge.add(p);
			else if (p.Classification__c == 'Gateway Systems')
				Gateway_system.add(p);
			else if (p.Classification__c == 'Hardware Support') {
				if(HW_Support_1.size() < 1000)
					HW_Support_1.add(p);
				else
					HW_Support_2.add(p);
			}
			else if (p.Classification__c == 'HW Extended Warranty')
				HW_Extended_Warranty.add(p);
			else if (p.Classification__c == 'Last Time Buy')
				Last_Time_Buy.add(p);
			else if (p.Classification__c == 'Professional Services')
				Professional_Services.add(p);
			else if (p.Classification__c == 'QDR / DDR Edge Switches')
				QDR_DDR_Edge.add(p);
			else if (p.Classification__c == 'QDR Chassis Switches')
				QDR_Chassis.add(p);
			else if (p.Classification__c == 'Software and SW Support')
				SW_SWSupport.add(p);
		}	
/*
System.debug('... KN adapter_cards.size()===' + adapter_cards.size());
System.debug('... KN appliance_systems.size()===' + appliance_systems.size());
System.debug('... KN cables.size()===' + cables.size());
System.debug('... KN EDR_Chassis.size()===' + EDR_Chassis.size());
System.debug('... KN EDR_Edge.size()===' + EDR_Edge.size());
System.debug('... KN ETH_Switch.size()===' + ETH_Switch.size());
System.debug('... KN FDR_Chassis.size()===' + FDR_Chassis.size());
System.debug('... KN FDR_Edge.size()===' + FDR_Edge.size());
System.debug('... KN Gateway_system.size()===' + Gateway_system.size());
System.debug('... KN HW_Support.size()===' + HW_Support.size());
System.debug('... KN HW_Extended_Warranty.size()===' + HW_Extended_Warranty.size());
System.debug('... KN Last_Time_Buy.size()===' + Last_Time_Buy.size());
System.debug('... KN Professional_Services.size()===' + Professional_Services.size());
System.debug('... KN QDR_DDR_Edge.size()===' + QDR_DDR_Edge.size());
System.debug('... KN QDR_Chassis.size()===' + QDR_Chassis.size());
System.debug('... KN SW_SWSupport.size()===' + SW_SWSupport.size());
*/		
	}  //end public void init(){
}