@isTest(seeAllData=true)
public class test_VF_Deal_Flow_Scoring {
    
    static testMethod void test_theVF() {
        
        Deal_Flow__c flow = new Deal_Flow__c();
        flow.Name = 'test flow';
        
        insert flow;
        
        Deal_Round__c round = new Deal_Round__c();
        round.Deal_Flow__c = flow.Id;
        round.Name = 'test round';
        insert round;
        
        test.startTest();
        
        Id userId = UserInfo.getUserId(); 
        ApexPages.standardController thePage = new  ApexPages.standardController(round);
        System.currentPageReference().getParameters().put('userName',userId);
        VF_Deal_Flow_Scoring controller = new VF_Deal_Flow_Scoring(thePage);
        
        
        controller.theScoreStr = '3';
        controller.getScores();
        controller.submitForm();
        test.stopTest();
        
    }
}