/* 

Author: David Rennert
Date: 22-Dec-2009
Description: Case extension controller. 

*/


public class CaseController
{

    //private final Case myCase;
    public Case myCase {get; set;}
    private String OpenerUrl;
    public Boolean IsPublished {get;set;}
    public String Comment {get;set;}
    public Boolean syncFM {get;set;}
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public CaseController(ApexPages.StandardController stdController)
    {
        //this.myCase = (Case)stdController.getRecord();
        this.myCase = [select id, State__c, AE_Engineer__c, Assignee__c,AE_PM_Escalated__c,Account.PM_Owner__c from Case where id = :stdController.getRecord().id];
    }
    
    public List<SelectOption> GetStatePossibleValues()
    {
        List<SelectOption> possibleValues = new List<SelectOption>();

        SelectOption open                       = new SelectOption(Util.caseStates.get('Open'),                          Util.caseStates.get('Open'));
        SelectOption assigned                   = new SelectOption(Util.caseStates.get('Assigned'),                      Util.caseStates.get('Assigned'));
        SelectOption onHold                     = new SelectOption(Util.caseStates.get('On Hold'),                       Util.caseStates.get('On Hold'));
        
        SelectOption openAe                     = new SelectOption(Util.caseStates.get('Open AE'),                       Util.caseStates.get('Open AE'));
        SelectOption assignedAe                 = new SelectOption(Util.caseStates.get('Assigned AE'),                   Util.caseStates.get('Assigned AE'));
        SelectOption waitingForCustomerInfo     = new SelectOption(Util.caseStates.get('Waiting for Customer Info'),     Util.caseStates.get('Waiting for Customer Info'));
        
        SelectOption waitingForCustomerApproval = new SelectOption(Util.caseStates.get('Waiting for Customer Approval'), Util.caseStates.get('Waiting for Customer Approval'));
        SelectOption rmaAssigned                = new SelectOption(Util.caseStates.get('RMA Assigned'),                  Util.caseStates.get('RMA Assigned'));
        SelectOption waitForRelease             = new SelectOption(Util.caseStates.get('Wait for Release'),              Util.caseStates.get('Wait for Release'));
       // SelectOption closedByCustomer           = new SelectOption(Util.caseStates.get('Closed by Customer'),            Util.caseStates.get('Closed by Customer'));
        SelectOption closed                     = new SelectOption(Util.caseStates.get('Closed'),                        Util.caseStates.get('Closed'));
        
        if(myCase.State__c == Util.caseStates.get('Open'))
        {
            possibleValues.add(open);
            possibleValues.add(assigned);     
            possibleValues.add(OpenAE);                                               
            possibleValues.add(Closed);                                            
        }
        else if(myCase.State__c == Util.caseStates.get('Assigned'))
        {
            possibleValues.add(assigned);
            possibleValues.add(openAe);
            possibleValues.add(waitingForCustomerApproval);
            possibleValues.add(waitingForCustomerInfo);
            possibleValues.add(onHold);
            possibleValues.add(rmaAssigned);
            possibleValues.add(Closed);                                                                
        }   
        else if(myCase.State__c== Util.caseStates.get('Open AE'))
        {
            possibleValues.add(openAe);
            possibleValues.add(assignedAe);      
            possibleValues.add(assigned);           
            possibleValues.add(Closed);                                                           
        }
        else if(myCase.State__c == Util.caseStates.get('Assigned AE'))
        {
      
            possibleValues.add(assignedAe);
            possibleValues.add(assigned);
            possibleValues.add(waitForRelease);         
            possibleValues.add(waitingForCustomerApproval);
            possibleValues.add(waitingForCustomerInfo);                    
            possibleValues.add(onHold);              
            possibleValues.add(Closed);                                                                                                              
        }
        else if(myCase.State__c == Util.caseStates.get('Waiting for Customer Info'))
        {
            possibleValues.add(waitingForCustomerInfo);       
            possibleValues.add(waitingForCustomerApproval);                                  
            possibleValues.add(assigned);             
            possibleValues.add(onHold);
            possibleValues.add(openAe);
            possibleValues.add(Closed);                                                      
        }
        else if(myCase.State__c == Util.caseStates.get('Waiting for Customer Approval'))
        {
            possibleValues.add(waitingForCustomerApproval);
            possibleValues.add(waitingForCustomerInfo);             
            possibleValues.add(assigned);             
            possibleValues.add(onHold);
            possibleValues.add(closed);
            
        }
        else if(myCase.State__c == Util.caseStates.get('RMA Assigned'))
        {
            possibleValues.add(rmaAssigned);
            possibleValues.add(assigned);         
            possibleValues.add(onHold);
            possibleValues.add(Closed);                                                   
        }
        else if(myCase.State__c == Util.caseStates.get('Wait for Release'))
        {
            possibleValues.add(waitForRelease);
            possibleValues.add(waitingForCustomerApproval);         
            possibleValues.add(onHold);                            
            possibleValues.add(Closed);                                                     
        }
        
                
        else if(myCase.State__c == Util.caseStates.get('Closed'))
        {
            possibleValues.add(closed);
            possibleValues.add(assigned);
            possibleValues.add(open);
        } 
      
       else if(myCase.State__c == Util.caseStates.get('On Hold'))
        {
            
            possibleValues.add(assigned);
        }
                                
        return possibleValues;
    }
    
    public String GetOpenerUrl()
    {
        return ApexPages.currentPage().getParameters().get('openerUrl');
    }
    
    public void SetOpenerUrl(string s)
    {
        OpenerUrl = s;
    }
    
    public PageReference save()
    {
        system.debug('BLAT Save');
        try
        {
            
            if(Comment!=null && Comment!='')
            {
                 
                FM_Discussion__c c = new FM_Discussion__c();
                c.Sync_With_FM__c = this.syncFM;
                c.Case__c = myCase.Id;
                c.Public__c = this.IsPublished;
                c.Discussion__c = this.Comment;
                insert c;
            }
            //If users change state to 'Open AE', and AE_Engineer is filled, automatically assigned to 'Assigned AE'
            if (myCase.state__c=='Open AE' && myCase.AE_Engineer__c!=null && myCase.AE_PM_Escalated__c != 'PM'){
            	myCase.state__c = 'Assigned AE';
            	myCase.Assignee__c = myCase.AE_Engineer__c;
            }
            //If State is changed to 'Open AE', Assignee is assigned to AE_Queue
            if (myCase.state__c=='Open AE' && myCase.AE_Engineer__c==null && myCase.AE_PM_Escalated__c != 'PM'){
            	myCase.Assignee__c = ENV.AEQueueCP;
            }
            //
			//If users change state to 'Open AE', and did not fill AE_Engineer, and AE/PM flag is set to PM Escalation, auto set to 'Assigned AE'
			if (myCase.state__c=='Open AE' && myCase.AE_PM_Escalated__c=='PM'){
				if(myCase.Account.PM_Owner__c!=null){
					myCase.state__c = 'Assigned AE';
            		myCase.AE_Engineer__c = myCase.Account.PM_Owner__c;
            		myCase.Assignee__c = myCase.Account.PM_Owner__c;	
            		myCase.Escalation_Q_Owner__c = myCase.Account.PM_Owner__c;
                    myCase.Escalation_Q_Owner2__c = null;
                    myCase.Optional_AE_Owners__c = '';
				}
          		else {
          			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The case escalation type is PM, but there is no PM Owner assigned for this Account.'));
          			return null;
          		}
            }
			//
            update myCase;
        }
        catch(System.DMLException e)
        {
            ApexPages.addMessages(e);
            return null;
        }

        /* We can't refresh the opener page from a VF page because they are not on the same domains (VF pages reside on c.csN.salesforce.com domain).
         for this reason we redirect to a s-control which resides on the same domain.
         The domain is passed through the VF pages and used in the page reference below.
         */

        String openerUrl = ApexPages.currentPage().getParameters().get('openerUrl');
        PageReference nextPage = new PageReference(openerUrl + '/servlet/servlet.Integration?lid=' + Util.refreshOpenerCloseMeSControlId);        
        
        nextPage.setRedirect(true);
        return nextPage;
    }
}