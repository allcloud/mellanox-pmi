public with sharing class MPCCaseOverrideController {
  private My_Mellanox_Setting__c settings = My_Mellanox_Setting__c.getInstance();
  
  private final Case record;
     
  public MPCCaseOverrideController(ApexPages.StandardController 
       controller) {
           this.record = (Case)controller.getRecord();
       }
  
  public PageReference redirect() {
    if (UserInfo.getProfileId().substring(0, 15)  == settings.System_Support_Profile_ID__c 
      || UserInfo.getProfileId().substring(0, 15) == settings.Design_In_Profile_ID__c) 
    {
      PageReference customPage =  Page.MPCCaseDetail;
      customPage.setRedirect(true);
      customPage.getParameters().put('id', record.Id);
      return customPage;
    } else {
      return null; //otherwise stay on the same page  
    }
  }
}