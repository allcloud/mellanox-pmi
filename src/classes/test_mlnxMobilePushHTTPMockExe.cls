@isTest(seeAllData=true)
public class test_mlnxMobilePushHTTPMockExe{
	public static testmethod void testmlnxMobilePushHTTPCallout() {
        test_mlnxMobilePushHTTP fakeResponse = new test_mlnxMobilePushHTTP(200,
                                                 'Complete',
                                                 '[{"Name": "sForceTest1"}]',
                                                 null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		
		list<String> messages_List = new list<String>{'test Message'};
		
		String userEmail =  UserInfo.getUserEmail();
		list<String> users_List = new list<String>{userEmail};
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
     	
     	Account acc = obj.createAccount();
     	acc.Type = 'OEM';
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
    	
    	 
    	Case cs = obj.Create_case(acc, con);
        cs.Business_impact__c = 'Critical';
        cs.AE_PM_Escalated__c = 'PM';
        cs.AE_Class__c = 'Host';
        cs.AE_Sub_Class__c = 'UFM';
        cs.AE_Engineer__c = UserInfo.getUserId();
         
        cs.ISFATAL__c = true;
        cs.priority = 'Fatal';
        
        Test.StartTest();
        cs.RecordTypeId = ENV.CaseTypeSupport;
        cs.Origin = 'Mobile';
        insert cs;
        //mlnxMobilePushHTTP.createHTTPRequestForPushNotif(users_List,messages_List);
        
    }
    
     
    
}