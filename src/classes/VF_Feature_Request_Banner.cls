public class VF_Feature_Request_Banner {
	
	public boolean isCaseTypeFR{get;set;}
	
	public VF_Feature_Request_Banner(ApexPages.StandardController controller) {
		
		isCaseTypeFR = false;
		Id TheCaseId = controller.getId();
		
		Case theCAse = [SELECT Id, Type FROM Case WHERE Id =: TheCaseId];
		if (theCAse.Type != null && theCAse.Type.EqualsIgnoreCase('Feature Request')) {
			
			isCaseTypeFR = true;
		}
	}

}