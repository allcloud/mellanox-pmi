public with sharing class VF_Campaign_Send_Form
{   
      
    public ID CampaignId;
    
    public VF_Campaign_Send_Form (ApexPages.StandardController controller) 
         {    
             //id = Apexpages.currentPage().getParameters().get('Id');    
         }

    
    
    
    public PageReference Send_Long_Form() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         CampaignId = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         system.debug('id is : ' + CampaignId);
         campaign cm = [Select id,Send_Long_form__c From campaign WHERE id =: CampaignId];
        
        /*if(cs.nc_reject_reason__c == NULL)

            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ' Please fill in the \'No Contract Reject Reason\' '));
              return  Apexpages.currentPage();
  
            } */

      
         cm.Send_Long_form__c = true; 
         update cm;

         return new Pagereference('/'+CampaignId);
     }

     public PageReference Send_Short_Form() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         CampaignId = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         system.debug('id is : ' + CampaignId);
         campaign cm = [Select id,Send_Short_form__c  From campaign WHERE id =: CampaignId];
        
        /*if(cs.nc_reject_reason__c == NULL)

            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ' Please fill in the \'No Contract Reject Reason\' '));
              return  Apexpages.currentPage();
  
            } */

      
         cm.Send_Short_form__c = true; 
         update cm;

         return new Pagereference('/'+CampaignId);
     }



}

/*static testMethod void Test_VF_RMA_Reject() 
    {
                //instance of object creator
                CLS_ObjectCreator obj = new CLS_ObjectCreator();

                Account acc = obj.createAccount();
                insert acc;
        
               Contact con = obj.CreateContact(acc);
               con.email = 'hgft56@ibm.com';
               insert con;
                
               Case cs = obj.Create_case(acc, con);
               insert cs;
                  
               cs.accountId = ENV.UnknownAccount;            
               update cs;                                
                                         
                                
                Test.setCurrentPageReference(new PageReference('Page.VF_Case_Reject_no_contract')); 
                System.currentPageReference().getParameters().put('id',cs.Id );       
                System.currentPageReference().getParameters().put('isTest','Testing' );  
              
               Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(cs);
                VF_Case_Reject_no_contract vf = new VF_Case_Reject_no_contract(teststandard);
                vf.reject_case();
    
    }

}*/