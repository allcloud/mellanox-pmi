@isTest
private class SupportNewsTickerControllerTest {
	
    static testMethod void myUnitTest() {
        Customer_News__c cusNews = new Customer_News__c( Name = 'Test Customer News',
                                                         Active__c = true
                                                       );
        insert cusNews;
        Test.startTest();
        	SupportNewsTickerController cont = new SupportNewsTickerController();
        Test.stopTest();
        System.assertEquals(1, cont.custNews.size());
    }
}