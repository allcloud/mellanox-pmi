global class batchUpdateAssets2Contract implements Database.Batchable<sObject>{
	global string query;
	
	global database.querylocator start(Database.BatchableContext BC){
           
            return Database.getQueryLocator(query );
   }
	
	global void execute(Database.BatchableContext BC, List<Asset2__c> scope) {
     	
     	  List<Asset2__c> assetsToBeUpdated_List = scope;
     	  
     	  system.debug('assetsToBeUpdated_List : '+ assetsToBeUpdated_List);
											       
		 if (!assetsToBeUpdated_List.isEmpty()) {
		 	
		 	for (Asset2__c theAsset : assetsToBeUpdated_List) {
		 		
		 		theAsset.MyMellanox_Contract__c = theAsset.Contract2__c;
		 	}
		 	
		 	Database.update(assetsToBeUpdated_List, false);
		 }									       
     }
     
     global void finish(Database.BatchableContext BC) {
     	   
     }
	

}