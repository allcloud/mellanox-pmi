global without sharing class VF_DealFlow_ContactUS {
        
        public Deal_Flow__c newDealFlow {get;set;}
        public String FirstName {get;set;}
        public String LastName {get;set;}
        public Attachment attachment {get;set;}
        public boolean displaySuccessMasage {get;set;} 
        
    public VF_DealFlow_ContactUS() {
        
        newDealFlow = new Deal_Flow__c();
        attachment = new Attachment();
        displaySuccessMasage = false;
        
        String message = Apexpages.currentPage().getParameters().get('Success');
        
        if (message != null) {
            
            displaySuccessMasage = true;
        }  
    }
    
    public PageReference createDeal() {
        
         
        if (FirstName != null && LastName != null && !FirstName.StartsWith(' ') && !LastName.StartsWith(' ') && FirstName != '' && LastName != ''
            && newDealFlow.Name != null && newDealFlow.Subject__c != null) {
            newDealFlow.Submitter_Name__c= FirstName + ' ' + LastName;
        }
        
        else {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill up First Name and Last Name'));
            return null;
        }
        
        newDealFlow.Deal_Source__c = 'Web';
        system.debug('FirstName : ' + FirstName);
        system.debug('LastName : ' + LastName);
        
        try {
            
            insert newDealFlow;
        }
        
        catch(Exception e) {
            
            
        }
        
        if (attachment.Name != null && attachment.body != null) {
            
            try {
                
                Deal_Flow__c newDealFlow = [SELECT Id, OwnerId FROM Deal_Flow__c WHERE Id =: newDealFlow.Id];
                attachment.OwnerId  = newDealFlow.OwnerId;
                attachment.ParentId = newDealFlow.Id;
                
                insert attachment;
                return new PageReference('/apex/VF_CommunitySelfReg_success_page');
            }
            
            catch(Exception me) {
            
                system.debug('Error : ' + me.getMessage());
            }
        }
        
        FirstName = null;
        LastName = null;
        newDealFlow = new Deal_Flow__c();
        displaySuccessMasage = true;
        return new PageReference('/apex/VF_CommunitySelfReg_success_page');
    }
    
     

}