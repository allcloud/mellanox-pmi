/**************************************************************************************
* Description:
*   This is a batch class that creates Knowledge Articles using content from Solutions.
*
***************************************************************************************/
global class S2KBatchJob implements Database.batchable<sObject>, Database.Stateful{
        
    global final Batch_Job_Invoker__c bji;
    global String solutionQuery;
    global String message = 'SUCCESS!';
    global Integer solutionsProcessed = 0;
    global Integer articlesCreated = 0;
            
    global S2KBatchJob(Batch_Job_Invoker__c bji){
        this.bji = bji;        
        //Query to fetch Solution Records
        //Get 'SolutionName' always.  Needed for computing URLName.
        //Get 'isPublished' always.
        String solutionFields = bji.Solution_Fields__c;
        if (bji.Solution_Fields__c.indexOf('SolutionName') == -1)
        	solutionFields += ', SolutionName';
        if (bji.Solution_Fields__c.indexOf('isPublishedInPublicKb') == -1)
        	solutionFields += ', isPublishedInPublicKb';
        if (bji.Solution_Fields__c.indexOf('isPublished') == -1)
        	solutionFields += ', isPublished';
        
        solutionQuery = 'Select ' + solutionFields + ' from Solution';         
        
        if (bji.WHERE_Clause__c != null && bji.WHERE_Clause__c.trim() != '')
            solutionQuery += ' WHERE ' + bji.WHERE_Clause__c.trim() + ' AND isMigrated__c = false';
        else
        	solutionQuery += ' WHERE ' + 'isMigrated__c = false';
            
        if (bji.LIMIT_Clause__c != null && bji.LIMIT_Clause__c.trim() != '')
            solutionQuery += ' LIMIT ' + bji.LIMIT_Clause__c.trim();
            
        Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job In Progress');
        if (Database.getQueryLocator(solutionQuery) == null)
        	bjic =  new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job Completed');
        	
        update bjic;
    }
        
    //START
    global Database.queryLocator start(Database.BatchableContext ctx){           
        return Database.getQueryLocator(solutionQuery);
    }
    
    //EXECUTE
    global void execute(Database.BatchableContext ctx, List<Solution> solutionList){    	
    	Savepoint sp = null;
        try{
        	sp = Database.setSavepoint();
            String articleType = bji.Article_Type__c;
            Boolean publishAfterImport = bji.Publish_after_Import__c;
            String solutionFields = bji.Solution_Fields__c;
            String articleFields = bji.Article_Fields__c;
            String attachmentFields = bji.Attachment_Fields__c;
            
            //TODO: Remove magic numbers below
            String[] solutionFieldTokens = solutionFields.split(',', 50); 
            String[] articleFieldTokens = articleFields.split(',', 50);
            String[] attachmentFieldTokensWithCSuffix = attachmentFields.split(',', 15); //We can have at most 15 File type fields on Article Type.
            
            //Collect all Solution IDs
            Set<ID> solutionIDs = new Set<ID>();
            for (Solution s : solutionList){
                solutionIDs.add(s.Id);
            }       
            
            Map<String, String> solution2ArticleFieldMapping = new Map<String, String>();       
            //TODO: Check length of solution and article tokens.  If not equal, throw error.
            for (Integer i=0; i<solutionFieldTokens.size(); i++){
                solution2ArticleFieldMapping.put(solutionFieldTokens[i].trim(), articleFieldTokens[i].trim());
            }         
            
            //Remove the __c from the Attachment field names AND save it in attachmentFieldTokens
            String[] attachmentFieldTokens = new List<String>();
            for (String attachmentField : attachmentFieldTokensWithCSuffix){
                String s = attachmentField.trim();
                attachmentFieldTokens.add(s.substring(0, s.indexOf('__c')));
            }
            
            //Fetch Attachments for all Solutions
            Map<ID, Attachment> attachmentMap = new Map<ID, Attachment>([SELECT Body, Name, ContentType, ParentId from Attachment where ParentId in :solutionIDs]);
            List<Attachment> attachmentList = attachmentMap.values();
            Map<ID, String> solution2AttachmentMapping = new Map<ID, String>();
            
            for (Attachment a : attachmentList){
                Boolean solutionHasEntry = (solution2AttachmentMapping.get(a.ParentId) != null);
                String newValue = '';
                if (solutionHasEntry)
                    newValue = solution2AttachmentMapping.get(a.ParentId) + ',' + a.Id;
                else
                    newValue = a.Id;
                    
                solution2AttachmentMapping.put(a.ParentId, newValue);
            }  
            
            //Fetch Data Categories for all Solutions
            Map<ID, CategoryData> categoriesMap = new Map<ID, CategoryData>([Select RelatedSObjectId, CategoryNodeId from CategoryData where RelatedSObjectId in :solutionIDs]) ;
            List<CategoryData> categoryList = categoriesMap.values();
            Set<ID> categoryNodeIDs = new Set<ID>();
            Map<ID, String> solution2CategoryMapping = new Map<ID, String>();
            
            for (CategoryData cd : categoryList){
            	categoryNodeIDs.add(cd.CategoryNodeId);
                Boolean solutionHasEntry = (solution2CategoryMapping.get(cd.RelatedSObjectId) != null);
                String newValue = '';
                if (solutionHasEntry)
                    newValue = solution2CategoryMapping.get(cd.RelatedSObjectId) + ',' + cd.CategoryNodeId;
                else
                    newValue = cd.CategoryNodeId;
                    
                solution2CategoryMapping.put(cd.RelatedSObjectId, newValue);
            } 
            
            System.debug('&*&*&* solution2CategoryMapping: ' + solution2CategoryMapping);    
            
            //Fetch MasterLabel for all Data Categories
            List<CategoryNode> categoryNodes = [Select ParentId, MasterLabel from CategoryNode where Id in :categoryNodeIDs];
            Set<ID> parentCategoryNodeIDs = new Set<ID>();
            Map<ID, ID> category2ParentCategory = new Map<ID, ID>();
            Map<ID, String> categoryMasterLabelsMap = new Map<ID, String>();
            
            //Insert Category Node Master Labels into HashMap
            for (CategoryNode cn : categoryNodes){
            	parentCategoryNodeIDs.add(cn.ParentId);
            	category2ParentCategory.put(cn.Id, cn.ParentId);
            	categoryMasterLabelsMap.put(cn.Id, cn.MasterLabel);
            }
            
            //Insert *Parent* Category Node Master Labels into HashMap
            List<CategoryNode> parentCategoryNodes = [Select MasterLabel from CategoryNode where Id in :parentCategoryNodeIDs];
            for (CategoryNode cn : parentCategoryNodes){            	
            	categoryMasterLabelsMap.put(cn.Id, cn.MasterLabel);
            }
                        
            System.debug('&*&*&*&* categoryMasterLabelsMap: ' + categoryMasterLabelsMap);
            
            //Insert Articles
            List<SObject> articles2Insert = new List<Sobject>();
            for (Solution s : solutionList){                        
                Schema.SObjectType targetType = Schema.getGlobalDescribe().get(articleType + '__kav');        
                SObject myObj = targetType.newSObject();        
                
                for (Integer i=0; i<solutionFieldTokens.size(); i++){               
                    myObj.put(((String)solution2ArticleFieldMapping.get(solutionFieldTokens[i].trim())).trim(), ((String)s.get(solutionFieldTokens[i].trim())).trim());
                }               
                                
                //Drive visibility of Article based on 'Published' statuses of Solution                
                myObj.put('IsVisibleInCsp', s.isPublished);
                myObj.put('IsVisibleInPkb', s.isPublishedInPublicKb);
                myObj.put('IsVisibleInPrm', s.isPublished);
                                
                myObj.put('UrlName', genURLName((String)s.get('SolutionName')));
                
                //Move attachments as well if they exist
                String attachmentIds = solution2AttachmentMapping.get(s.Id);            
                if (attachmentIds != null){             
                    //TODO: Remove magic number '15' below.  This is the max number of 'File' fields an Article can have
                    String[] attachIdTokens = attachmentIds.split(',', 15);
                    Integer index = 0;
                    for (String attachId : attachIdTokens){                 
                        myObj.put(attachmentFieldTokens[index]+'__Name__s', attachmentMap.get(attachId).Name);                  
                        myObj.put(attachmentFieldTokens[index]+'__ContentType__s', attachmentMap.get(attachId).ContentType);
                        myObj.put(attachmentFieldTokens[index]+'__Body__s', attachmentMap.get(attachId).Body);
                        index++;
                    }
                }
                
                articles2Insert.add(myObj);
                s.isMigrated__c = true;
            }   
            
            //Insert all Articles
            insert articles2Insert;    
            
            //Update Solutions w/ isMigrated = true.  You don't want Solutions from successful previous batch(es) to be migrated again.
            update solutionList;
            
            //Solution Id to Article Id Mapping
            Map<ID, ID> solution2Article = new Map<ID, ID>();
            Integer solutionIndex = 0;
            for (Solution s : solutionList){
            	solution2Article.put(s.Id, articles2Insert[solutionIndex].Id);
            	solutionIndex++;
            }
            
            //Assign Data Categories to Articles            
            List<SObject> dataCategorySelections2Insert = new List<Sobject>();
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(articleType + '__DataCategorySelection');
            
            for (Solution s : solutionList){
            	String categoryIds = solution2CategoryMapping.get(s.Id);    
            	System.debug('>>>> Solution ID: ' + s.Id + ' ---- categoryIds: ' + categoryIds);        	
            	if (categoryIds != null){
            		//TODO: Remove magic number '10' below.  This is the max number of Categories a Solution can have
                    String[] categoryIdTokens = categoryIds.split(',', 10);
                                		
            		for (String categoryId : categoryIdTokens){
            			SObject myObj = targetType.newSObject();                 
                        myObj.put('ParentId', solution2Article.get(s.Id));                                                
                        myObj.put('DataCategoryName', genCategorization(categoryMasterLabelsMap.get(categoryId)));
                        myObj.put('DataCategoryGroupName', genCategorization(categoryMasterLabelsMap.get(category2ParentCategory.get(categoryId))));
                        
                        dataCategorySelections2Insert.add(myObj);                       
                    }               	 
            	}           	
            }           
            
            System.debug('>>>Data Category Selection LIST: ' + dataCategorySelections2Insert);
            
            //TODO: Bulk Testing 
            insert dataCategorySelections2Insert;			
            
            //If publishAfterImport = TRUE, call the publish method.
            if (publishAfterImport){
                Set<ID> objectIds = new Set<ID>();
                for (SObject article : articles2Insert){
                    objectIds.add(article.Id);          
                }       
                
                String draftArticlesQuery = 'SELECT Id, KnowledgeArticleId FROM ' + articleType + '__kav WHERE PublishStatus=\'Draft\' and Id in :objectIds';
                List<SObject> draftArticles = Database.query(draftArticlesQuery);
                
                for (SObject myDraft : draftArticles){
                    KbManagement.PublishingService.publishArticle(((ID)myDraft.get('KnowledgeArticleId')), true);
                }       
            }
            
            solutionsProcessed += solutionList.size();
            articlesCreated += articles2Insert.size();
        }
        catch (Exception e){
            Database.rollback(sp);
            Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job Completed');
        	update bjic;         
            message = e.getTypeName() + ' ' + e.getMessage() + ' at: ' + e.getStackTraceString() + '.  This batch has been rolled back.';           
        }           
    }    
    
    //FINISH     
    global void finish(Database.BatchableContext ctx){      
        Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job Completed');
        update bjic;         
        
        AsyncApexJob a = [SELECT id, ApexClassId,
                        JobItemsProcessed, TotalJobItems,
                        NumberOfErrors, CreatedBy.Email FROM AsyncApexJob WHERE id = :ctx.getJobId()];      
        
        //TODO: Use an email template, don't HARDCODE message body
        String emailMessage = 'Your batch job Solutions to Knowledge (Advanced) has completed.'; 
                                
        emailMessage += '<br/><br/>Number of Solutions Processed: ' + solutionsProcessed + '.  Number of Articles created and/or published: ' + articlesCreated + '.';
        
        emailMessage += '<br/><br/>' + message;                     
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {a.createdBy.email};
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSenderDisplayName('S2K Batch Job');
        mail.setSubject('Solutions to Knowledge - batch job completed');
        mail.setPlainTextBody(emailMessage);
        mail.setHtmlBody(emailMessage);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ mail });   
    }    

	//Generate URL Name
	private String genURLName(String t) {
		t = t.replaceAll('[^a-zA-Z0-9]', '-').replaceAll('--', '-').replaceAll('--', '-').replaceAll('--', '-');
		t = t.removeEnd('-');
		t = t.removeStart('-');
		 
		return t;
	}
	
	//Generate Category API name from the Solution Category Master Label
	private String genCategorization(String t) {
        t = t.replaceAll('[ ,/,\\-,(,),;,\\.]', '_').replaceAll('___', '_').replaceAll('__', '_').replaceAll('__', '_');
        if (t.lastIndexOf('_') == (t.length() - 1)) {
            t = t.substring(0, t.length() - 1);
        }

        return t;
    }	
}