/** MUST be WITH SHARING */
public with sharing class mlnxMobileAssetSearchCtrl {
  
    public String barCode 	{get; set;}
    public List<Asset2__c> assetsToDisplay {get; set;}
    
    public Boolean isCommunity {get; set;}
    public Boolean isNoRecordFound {get; set;}
    public Boolean isDisplayOne {get; set;}
    public Boolean isDisplayAll {get; set;}
    public Id recordId {get; set;}

    public mlnxMobileAssetSearchCtrl(){
        isNoRecordFound= false;
        isDisplayOne = false;
		isDisplayAll = false;

        assetsToDisplay = new List<Asset2__c>();

        isCommunity = ( Network.getNetworkId() != null );
    }
    
    // Elad - chenged to redirect the user to 'mlnxMobileAssetView' page 
    public pageReference searchAsset(){
    	// Reset all
        assetsToDisplay.clear();
        isNoRecordFound = false;
		isDisplayOne = false;
		isDisplayAll = false;

        // Remove blanks arround search string.
        barCode = barCode.trim();
        System.debug('SCS: barCode = :' + barCode + ':');
		
		/*
        List<Asset2__c> assetsFound = [ SELECT Id, Name, Asset_Type__c, Asset_kind__c, Asset_Status__c, Part_Number__c
                              FROM  Asset2__c 
                              WHERE Name like barCode];
        */
                              
        String query =  ' SELECT Id, Name, Asset_Type__c, Asset_kind__c, Asset_Status__c, Part_Number__c FROM  Asset2__c ' +
                          'WHERE Name like \'%' + barCode + '%\'';             
		
		system.debug('query : ' + query);
		 List<Asset2__c> assetsFound = database.query(query);
		isNoRecordFound = (assetsFound.isEmpty()) ? true : false;
		
		system.debug('isNoRecordFound : ' + isNoRecordFound);
		//isDisplayOne = assetsCount == 1 ? true : false;
		//isDisplayAll = assetsFound.size() > 1 ? true : false;	// More than one found. 

       // System.debug('BA: isNoRecordFound = ' + isNoRecordFound);

        if (isNoRecordFound) {
        	
			return null;
        } 
        
        else {
        	
        	String openerUrl = '/apex/mlnxMobileAssetView?Id=' + assetsFound[0].Id ;
	        PageReference nextPage = new PageReference(openerUrl);
	        
	        nextPage.setRedirect(true);
	        return nextPage;
        }
    }
}