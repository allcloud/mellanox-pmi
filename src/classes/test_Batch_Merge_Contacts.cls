@isTest(seeAllData=true)
public with sharing class test_Batch_Merge_Contacts {
	
	static testMethod void test_theBatch() {
		
		CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        insert acc;
        
 		Contact  contactMaster = new Contact(Accountid = acc.Id, firstName = 'First', LastName = 'Last');
 		contactMaster.Email = 'TestEmail321@testmail.com';
   	    insert contactMaster;
   	    
   	    Contact  contact2Merge = new Contact(Accountid = acc.Id, firstName = 'First1', LastName = 'Last2');
 		contact2Merge.Email = 'TestEmail333@testmail.com';
 		contact2Merge.Contact_Need_to_be_merged__c = true;
 		contact2Merge.Email_for_merge__c = 'TestEmail321@testmail.com';
   	    insert contact2Merge;  
   	    
   	    Case Cs = obj.Create_case(acc, contact2Merge);
   	    insert Cs;
   	    
   	    Test.startTest();
        
        Schedule_Batch_Merge_Contacts sch = new Schedule_Batch_Merge_Contacts();

        String jobId = System.schedule('ScheduleContacts',
        '0 0 0 8 9 ? 2020', sch);
              
        Test.stopTest();      
 	}
}