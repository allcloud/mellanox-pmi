@isTest(seeAllData=true)
// This Class tests the functionalitiy of the classes 'VF_Feature_Request_Banner', 'VF_Case_Under_Recall_Banner'
// and 'VF_RMA_Domain_foxconn'
public with sharing class test_VF_Feature_Request_Banner {
	
	static testMethod void test_TheVF() {
		
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		 
		Account acc = creator.createAccount();
        insert acc;
        
        Contact con = creator.CreateContact(acc);
        insert con;
        
        Case c = creator.Create_case(acc, con);
        c.AccountId = acc.Id;
        c.ContactId = con.Id;
        c.Type = 'Feature Request';
        insert c;
        
        Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(c);
        VF_Feature_Request_Banner handler = new VF_Feature_Request_Banner(controller1);
	}
	
	static testMethod void test_VF_Case_Under_Recall_Banner() {
		
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		 
		Account acc = creator.createAccount();
		acc.Recall__c = 'test';
		
        insert acc;
        
        Contact con = creator.CreateContact(acc);
        insert con;
        
        Case c = creator.Create_case(acc, con);
        c.AccountId = acc.Id;
        c.ContactId = con.Id;
        c.Type = 'Feature Request';
         
        insert c;
        
        Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(c);
        VF_Case_Under_Recall_Banner handler = new VF_Case_Under_Recall_Banner(controller1);
	}
	
	static testMethod void test_VF_RMA_Domain_foxconn() {
    
 		CLS_ObjectCreator creator = new CLS_ObjectCreator(); 
		Account acc = creator.createAccount();
		acc.Recall__c = 'test';
		
        insert acc;
        
        Contact con = creator.CreateContact(acc);
        insert con;
	    
        RMA__c R = creator.createRMA();
        R.RMA_Support_Project__c = 'test';
        R.E_Mail__c = 'test@foxconn.com';
        insert R;
        
        Case c = creator.Create_case(acc, con);
        c.AccountId = acc.Id;
        c.ContactId = con.Id;
        c.Type = 'Feature Request';
        c.RMA_Request__c = R.Id;
         
        insert c;
        
        serial_number__c serial =  creator.CreateSN(R);
        serial.Asset_End_Customer_Account_Name__c = 'EMC';
        insert serial;
        
        Test.StartTest();
        ApexPages.standardController thePage = new  ApexPages.standardController(c);
        VF_RMA_Domain_foxconn controller = new VF_RMA_Domain_foxconn(thePage);
        Test.StopTest();
	}
}