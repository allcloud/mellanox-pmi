public class Opportunity_Reg_Upd 
{
public ID oppId;

      
      public Opportunity_Reg_Upd(ApexPages.StandardController controller) {

       oppId = Apexpages.currentPage().getParameters().get('Id');
 
    
    }



        public PageReference Upd_Send_Email()   
        { Distributor_oppy_registrations__c OppReg = new Distributor_oppy_registrations__c();
          OppReg = [select id, sent_approve_email__c from Distributor_oppy_registrations__c where id = :oppId];   
          OppReg.sent_approve_email__c = True;
          update(OppReg);
          PageReference p = new PageReference('/'+oppId);
          return p;    
            }
    
    }