public with sharing class VF_RMA_Letter_RequestInProcess_OEM 
{
    
    public ID id;
    
    public VF_RMA_Letter_RequestInProcess_OEM(ApexPages.StandardController controller) 
    {
        //id = Apexpages.currentPage().getParameters().get('Id');
        //getDeliverAsPDF();
    }

    public PageReference getDeliverAsPDF_InProcess() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         id = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         map<string,integer> PNcount = new map<string,integer>();

         system.debug('id is : ' + id);
         system.debug('isTest is : ' + isTest);
         
                  //String b = pdf.getContent().toString();
          RMA__c rm = [Select r.id, r.name,r.PN_List__c, r.approved_SNs__c,r.Approved_SN__c,r.RMA_approved__c, r.sup_case_assignee__r.email, r.RMA_Number__c,r.RMA_state__c, r.e_mail__c, r.placed_in_oracle__c From RMA__c r WHERE r.id =:Id];
          
          if(rm.RMA_state__c == 'Support Group Approval')
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'The RMA must be approved by System Support group from related case'));
              return  Apexpages.currentPage();
            }

          if(rm.RMA_approved__c == True)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'The RMA was already Approved, email notification was sent to customer'));
              return  Apexpages.currentPage();
  
            }

                   
         
          if(rm.Approved_SNs__c == 0)
           {Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'At least one Serial Number should be approved, in order to move to Approved status'));
            return  Apexpages.currentPage();
            }

          else{ 
          rm.RMA_State__c = 'Approved';  
          rm.RMA_approved__c = True;             
         /* for(Serial_Number__c S: [select id,approval__c, serial__c, Problem_Description__c,Reject_reason__c, Mellanox_PN__c, RMA__c from Serial_Number__c  SN where SN.RMA__c =:RM.Id])
           {       
            if(rm.Approved_SN__c == null){ rm.Approved_SN__c = S.serial__c  + '     -'+ s.Problem_Description__c; } 
           else{ rm.Approved_SN__c = rm.Approved_SN__c +'\n'+ s.serial__c +'     -'+ s.Problem_Description__c ;}               
                   
           if (!PNcount.containsKey(s.Mellanox_PN__c))            
            { PNcount.put(s.Mellanox_PN__c,1);
            }else
             {PNcount.put(s.Mellanox_PN__c, PNcount.get(s.Mellanox_PN__c)+1); } 
               
          }   
         for(string PN:PNcount.keyset()) 
          {                 
          if(rm.PN_List__c == NULL) {rm.PN_List__c = PN + '      Count:' +PNcount.get(PN); }
          else {rm.PN_List__c = rm.PN_List__c + '\n'+ PN + '      Count:' +PNcount.get(PN);}  
          } */
 
       }
         PageReference pdf =  Page.VF_RMA_Letter_RequestInProcess;  
         pdf.getParameters().put('id',id);
         
         system.debug('pdf.getParameters().get id:  ' + pdf.getParameters().get('id'));
         


 //-------------------------- Create an email to customer -----------------
    
         Messaging.SingleEmailMessage cust_email = new Messaging.SingleEmailMessage();
         Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

          String [] toCustAddresses = new String[] {rm.e_mail__c};
          cust_email.setToAddresses(toCustAddresses);
          cust_email.setOrgWideEmailAddressId(Env.RMASupportAddress);
    
          cust_email.setSubject('Mellanox RMA #' + rm.Name +' is in Process');
          cust_email.setPlainTextBody('Dear Valued Customer,\n' + '\nYour RMA request is verified and qualified. \n'+
                                 'You will receive a confirmation by email containing your RMA number. Note you MUST obtain an RMA number before returning your product.\n'+
                                 'Please see the attached information on your RMA status.\n' + 
                                 '\nTo learn more about Mellanox RMA:\n' +
                                 'http://www.mellanox.com/pdf/user_manuals/Mellanox_Support_and_Services_User_Guide.pdf \n\n' +
                                 'Thank you\n' +
                                 'Mellanox Support\n');

                     
            
         // Create an email attachment
         // email.setHtmlBody(b);
         if(isTest == 'Testing') 
         {
         }else
         {
            blob b = pdf.getContent();
            Messaging.EmailFileAttachment  efa = new Messaging.EmailFileAttachment();
        
            efa.setFileName('RMA_Request_In_Process'); // neat - set name of PDF
        
            efa.setBody(b); //attach the PDF
        
            cust_email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

        }
         // send it, ignoring any errors (bad!)
    
     //ek    Messaging.SendEmailResult [] cust_r =
    
         //ek        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {cust_email});
       
         
           

//-----------------------------Send notification to RMA Group ------------------------

String [] toAddresses = new String[] {};


Group rmaGroup = [Select g.Name, g.Id, g.Email From Group g WHERE Name ='RMA GROUP'];
List<GroupMember> lst_grpMembers = new List<GroupMember >([Select g.UserOrGroupId, g.Id, g.GroupId From GroupMember g where g.GroupId  =: rmaGroup.Id ]);
             system.debug('lst_grpMembers siae : ' +lst_grpMembers.size() );
             
             Set<ID> set_UsrIds = new Set<ID>();
             
             if(lst_grpMembers != null && lst_grpMembers.size() > 0)
             {
                for(GroupMember grpm :lst_grpMembers)
                {
                    set_UsrIds.add(grpm.UserOrGroupId);
                }           
             }
         
             system.debug('set_UsrIds Size : ' + set_UsrIds.size());
             
             List<User> lst_users = new List<User>([Select u.Id, u.Email From User u WHERE u.id IN: set_UsrIds]);
              system.debug('lst_users Size : ' + lst_users.size());
             
             set<String> set_usrEmails = new set<String>();
             if( lst_users != null && lst_users.size() > 0)
             {
                for(User usr :lst_users)
                {
                    if(usr.Email != null)
                    {
                        set_usrEmails.add(usr.Email);
                    }
                }
             }
             system.debug('set_usrEmails Size : ' + set_usrEmails.size());
             if(set_usrEmails != null && set_usrEmails.size() > 0)
             {
                for(String usrEmail :set_usrEmails)
                {
                    toAddresses.add(usrEmail);
                }
             }
//----------------


         email.setToAddresses(toAddresses);
         email.setOrgWideEmailAddressId(Env.RMASupportAddress);
         email.setSubject('Mellanox RMA #' + rm.Name +' is in Process');
         email.setPlainTextBody('**** OEM RMA REQUEST WAS APPROVED **** \n\n' +
                                     'RMA request #' + rm.Name + ' was approved. \n'+
                                     'See the attached information on  RMA \n' +
                                     'Please take care by placing an order in Oracle. \n' +
                                     'Once you receive RMA number from Oracle please enter it to Salesforce and change RMA state to Execution in Process.\n'+
                                     '\n Link to RMA: \n'+
                                     'https://mellanox.my.salesforce.com/'+rm.id);
                                     //'https://na3.salesforce.com/'+rm.id);

 
                   
             
        //ek Messaging.SendEmailResult [] r =
    
           //ek      Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
       
        update rm;
 
        return new Pagereference('/'+id);
    }


//---------------------------------------- TEST CLASS --------------------------------------------------------
    static testMethod void Test_VF_RMA_Letter_RequestInProcess() 
    {
        //list of serial numbers to be related to the RMA and to avoid many inserts
                List<Serial_Number__c> lst_snOfRMA = new List<Serial_Number__c>();
                
                List<Product2> lst_prods = New List<Product2>();
                //instance of object creator
                CLS_ObjectCreator obj = new CLS_ObjectCreator();
                
                Account acc = obj.createAccount();
                insert acc;
        
               Contact con = obj.CreateContact(acc);
               con.email = 'hgft56@ibm.com';
               insert con;
                
                MellanoxSite__c  site  = obj.createMellanoxSite();
                insert site;
                 
                RMA__c rma = obj.CreateRMA();
                rma.e_mail__c = 'dsfds@ibm.com';
                rma.RMA_state__c= 'Pending Sales Approval';
                rma.contact__c = con.id;
                insert rma;
                
                rma.RMA_state__c = 'Pending Sales Approval';
                update rma;
                                
                    
    
                Product2 p1 =  obj.createProduct();
                p1.Inventory_Item_id__c = '90989';
                lst_prods.add(p1);
                
                Product2 p2 =  obj.createProduct();
                p2.Name = 'Test Product 2';
                p2.Inventory_Item_id__c = '90980';
                lst_prods.add(p2);
                
                Serial_Number__c sn1 = obj.createSerialNumber(rma,p1);
                sn1.Approval__c = 'Approved';
                lst_snOfRMA.add(sn1);
                
                Serial_Number__c sn2 = obj.createSerialNumber(rma,p2);
                lst_snOfRMA.add(sn2);
                
                insert lst_prods;
                insert lst_snOfRMA; 
                
                Test.setCurrentPageReference(new PageReference('Page.VF_RMA_Letter_RequestInProcess_OEM')); 
                System.currentPageReference().getParameters().put('id',rma.Id );       
                System.currentPageReference().getParameters().put('isTest','Testing' );  
              
                Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rma);
                VF_RMA_Letter_RequestInProcess_OEM vf = new VF_RMA_Letter_RequestInProcess_OEM(teststandard);
                vf.getDeliverAsPDF_InProcess();
    
    }

}