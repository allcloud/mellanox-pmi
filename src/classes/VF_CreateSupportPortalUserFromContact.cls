public without sharing class VF_CreateSupportPortalUserFromContact {
    
    public Contact    theContact    {get;set;}
    public Account TheAccount {get;set;}
    public User    TheUser    {get;set;}
    public Id      ConId      {get;set;}
    public boolean IsLeadConverted {get;set;}
    
    public NewPortalUserForm NewUser      {get;set;}
    public boolean           displayPopup {get;set;}     
    public string            msg          {get;set;}
    // list of select options representing Contact Types picklist values
    public List<selectOption> contactTypestOptions_List {get;set;}
    // Boolean representing if the 2 fields relating to 'Design In customer' will be displayed or not 
    public boolean diaplayTecOwnerFields {get;set;}
    // Boolean relresenting if the whole page will be displayed or not (will not be displayed when a portal user already exists)
    public boolean displayWholePage{get;set;}
    // Boolean representing if the cnacel button under the error message 'This Contact already has a poratal user' will be displayed or not
    public boolean displayCancelButton{get;set;}
    private Account newCreateadAcc {get;set;}
    private boolean newAccCreated {get;set;}
    public String accExistsErrMessage{get;set;}
    public boolean displayTechOwnerCNA{get;set;}
    public String errMessage{get;set;}
    
    
    /****************  INNSER CLASS   ****************/
    public class NewPortalUserForm
    {
        public string TechnicalOwner          {get;set;}
        public String PortalAccessType        {get;set;}
        public string SelectedAccount         {get;set;}
        public Contact con                    {get;set;}
        public Id defaultTechUserId           {get;set;}
     /****************  INNSER CLASS   ****************/    
        
        public NewPortalUserForm()
        {
            con = new Contact();
        }
    }
  
    public VF_CreateSupportPortalUserFromContact(ApexPages.StandardController stdController) 
    {
        
        accExistsErrMessage = '';
        errMessage = ''; 
        newAccCreated = false;
        TheContact = [select Name, Id, Portal_Access__c, domain__c, Contact_Type__c, Technical_Owner__c,Email,FirstName,LastName, AccountId, Account.Name,
                      Case_to_techowner__c, Company_Name_from_Web__c From Contact Where Id =: stdController.getRecord().Id];
        system.debug('==>Lead: '+ TheContact);
        
        list<User> user_List = [SELECT Id, Username, IsPortalEnabled FROM user WHERE ContactId =: TheContact.Id];
        list<User> userWithSameUserNameAsContactEmail_List = [SELECT Id, Username FROM user WHERE userName =: TheContact.Email];
        
        if(!user_List.isEmpty() && user_List[0].IsPortalEnabled) {   
            
            ApexPages.Message PMsg = new ApexPages.Message(ApexPages.severity.error,'This Contact already has a potal user ');
            ApexPages.addMessage(PMsg);
            displayWholePage = false;
            displayCancelButton = true;
            return ;
        }
        
        else if (!userWithSameUserNameAsContactEmail_List.isEmpty()) {
            
            ApexPages.Message PMsg = new ApexPages.Message(ApexPages.severity.error,
                                     'There is an existing user with this user name, please search for this user : '+ userWithSameUserNameAsContactEmail_List[0].Username);
            ApexPages.addMessage(PMsg);
            displayWholePage = false;
            displayCancelButton = true;
            return ;
        }
        
        else {
            
            displayWholePage = true;
            displayCancelButton = false;
        }
        
        
        if (TheContact.Contact_Type__c == null 
            || TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer')
            || TheContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer') ){
            
            diaplayTecOwnerFields = true;
        }
        
        else{
            
            diaplayTecOwnerFields = false;
        }
        TheAccount      = new Account();
        NewUser = new NewPortalUserForm();
        system.debug('==> AvailableAccounts: '+AvailableAccounts.size());  
          
        displayPopup = false; 
        msg ='';  
    }
   
    //List of available Accounts
    public List<selectOption> AvailableAccounts 
    {
        get 
        {
             List<selectOption> options = new List<selectOption>();
             SelectOption tempOption; 
            if (newAccCreated) {
                
                options.add(new SelectOption(newCreateadAcc.Id,'Attach to Existing: '+ newCreateadAcc.Name));
            }
            
            
            else {
               
                            
                tempOption    = new SelectOption('','--None--');
                options.add(tempOption); 
                
                if (TheContact.Company_Name_from_Web__c != null && TheContact.Company_Name_from_Web__c != ''){
                
                }
                
                else {
                    
                    TheContact.Company_Name_from_Web__c = TheContact.domain__c;
                }
                
                
                options.add(new SelectOption ('New','Create New Account: '+ TheContact.Company_Name_from_Web__c));
            
            }
            
            tempOption = new SelectOption('','-----------------------------------');
            tempOption.SetDisabled(true);
            options.add(tempOption); 
           
            
            string ContactCompany = string.valueof(TheContact.Company_Name_from_Web__c).remove('Inc').remove('Ltd').remove('-').remove(',').remove('\'').remove('.').remove('_').trim().replaceAll(' ','%');
            ContactCompany='%'+ ContactCompany +'%';
            system.debug('==> LeadCompny: '+ ContactCompany);
                        
            list<Account> accounts_List = new list<Account>();
           
            accounts_List = createAccountsList(accounts_List, ContactCompany);
            
             if (!accounts_List.isEmpty()){
                
                    
                for (Account acc: accounts_List){
                    
                    options.add(new SelectOption(acc.Id,'Attach to Existing: '+acc.Name));
                }
            }
            
            system.debug('==> Options: '+ Options);
            system.debug('==> Options: '+ Options.size());
            return options;        
        }
        set;
        }
        
        private list<Account> createAccountsList(list<Account> accounts_List, string ContactCompany){
            
            Id parentAccRecType = ENV.getRecordTypeIdByName('Parent Account');
                    
            String soql;
           
                
            if (TheContact.Contact_Type__c != null 
                && (TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer')
                    || TheContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer') )){
                
                  soql = 'Select RecordTypeId, Name, Id, Domain2__c, Domain1__c,domain__c From Account' +
                         ' WHERE RecordTypeId = \'' + parentAccRecType  +'\'' +  ' and Technical_Owner__c != null and RW_Domain_multi__c includes '+  '(' + '\'' + TheContact.domain__c  + '\'' + ')' +
                         ' limit 990';
            }
            
            
            else{
                
                 soql = 'Select RecordTypeId, Name, Id, Domain2__c, Domain1__c,domain__c From Account' +
                         ' WHERE RecordTypeId = \'' + parentAccRecType  +'\'' +  ' and RW_Domain_multi__c includes '+  '(' + '\'' + TheContact.domain__c  + '\'' + ')' +
                         ' limit 990';
         
            }
            
            accounts_List = dataBase.Query(soql);
            return accounts_List;
        }
        
    // This method returns a list of all the Opportunity's 'Contact_Type__c'(picklist) values
    // To display on the VF
    public List<SelectOption> getContactTypes(){
        
        Schema.DescribeFieldResult contactTypesDescription = Contact.Contact_Type__c.getDescribe();
        contactTypestOptions_List = new list<SelectOption>();
        
        for (Schema.Picklistentry picklistEntry : contactTypesDescription.getPicklistValues())
        {
            contactTypestOptions_List.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        
        return contactTypestOptions_List;
    }
    
    // This method will update the Accounts list according to the chosen Contact Type and will rerender the page
    public pageReference rerenderTheTechnicalandDefaultOwner(){
        
        Database.SaveResult sr = Database.update(TheContact, false);
        if(!sr.isSuccess())
        {
            system.debug('==>error : '+sr.getErrors());
            
            errMessage = 'An error has occured  : \n'+string.valueof(sr.getErrors()).substringBetween('getMessage=',';');
            
            PageReference p = new PageReference('/apex/VF_CCPU_Error_Page?Id=' + theContact.Id);
            p.setRedirect(true);    
            return p;
             
        }
        
        
        if (TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer')
            || TheContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer') ){
            
            diaplayTecOwnerFields = true;
        }
        
        else{
            
            diaplayTecOwnerFields = false;
        }
        
        PageReference p = new PageReference('/apex/VF_CCPU?Id=' + theContact.Id);
        p.setRedirect(true);    
        return p;
    }
    
    public void CreatePUser()
    {  
          boolean foundManagerRole = false;
          String roleName_Str;
          string CRM_Content_Permissions ='';
          system.debug('TheAccount.Id ' + TheAccount.Id);
          system.debug('NewUser.SelectedAccount ' + NewUser.SelectedAccount);
          
        system.debug('inside CreatePUser');
        system.debug('TheContact : ' + TheContact);
        system.debug('TheContact.Technical_Owner__c : '+ TheContact.Technical_Owner__c);
        
        if(TheContact.Technical_Owner__c == null && 
        (TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer') ||
        TheContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer') )){
     
            errMessage = 'Please Fill All The Fields';
            diaplayTecOwnerFields = true;
        }
        
         
        else if (NewUser.con.CRM_Content_Permissions__c == null && !TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox System Customer')) {
            system.debug('inside 201');
            errMessage = 'Please Fill in content permissions';
        }
        
     
                
        else {    
                 
            if(TheAccount.Id != null && NewUser.SelectedAccount == 'new') {
             
                NewUser.SelectedAccount=TheAccount.Id;
            }
            
            if (TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox System Customer')) {
                    
                TheContact.CRM_Content_Permissions__c = 'System NDA';
                CRM_Content_Permissions = TheContact.CRM_Content_Permissions__c;
                NewUser.con.CRM_Content_Permissions__c = TheContact.CRM_Content_Permissions__c;  
           }
            
            system.debug('==> NewUser.SelectedAccount'+NewUser.SelectedAccount);
        
           
                system.debug('NewUser.con :' + NewUser.con);
                
                Id profileIdByContactType;
                
                TheUser = new User( userName=TheContact.Email, Email=TheContact.Email, LastName=TheContact.LastName, 
                                         
                                         Alias=( TheContact.LastName.length() > 1) ? TheContact.LastName.substring(0,3) : TheContact.LastName, 
                                     TimeZoneSidKey='America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', 
                                     LocaleSidKey='en_US', ContactId= TheContact.Id);
                                     
                if (TheContact.FirstName != null && !TheContact.FirstName.EqualsIgnoreCase('')) {
                    
                    TheUser.FirstName = TheContact.FirstName;  
                }                   
                
                system.debug('NewUser.SelectedAccount :' + NewUser.SelectedAccount);
                
                TheAccount = [SELECT Id, Name, OwnerId, Customer_Type__c FROM Account WHERE Id =: NewUser.SelectedAccount];
                
                if (TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer')) {
                    
                    profileIdByContactType =  MyMellanoxSettings.DesignInUserProfileId;
                    
                    theContact.Portal_Access__c = 'Mellanox Silicon Design-In customer';
                
                
                /*****************   REMOVED BY ELAD 24/11/14        ************************   
                    if (TheAccount != null && TheAccount.Name != null) {
            
                        roleName_Str = TheAccount.Name + ' Partner Manager';
                        UserRole userRole = new UserRole();
                        
                        try {   
                            
                            userRole = [SELECT Id FROM UserRole WHERE Name =: roleName_Str];
                            SYSTEM.DEBUG('userRole : ' + userRole);
                            TheUser.UserRoleId = userRole.Id;
                            foundManagerRole = true;
                        }
                        
                        catch(Exception ex) {
                            
                        }
                    }  
                    
                 /*****************   REMOVED BY ELAD 24/11/14      ************************/    
                }
                
                else if (TheContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer')) {
                    
                    profileIdByContactType =  MyMellanoxSettings.EzchipDesignInUserProfileId;
                    
                    theContact.Portal_Access__c = 'Ezchip Design-In customer';
                }
                
                else if ( TheAccount != null && TheAccount.Customer_Type__c != null && TheAccount.Customer_Type__c.EqualsIgnoreCase('OEM') ){
                    
                     profileIdByContactType =  MyMellanoxSettings.SystemOEMId;
                     theContact.Portal_Access__c = 'Mellanox System OEM Customer';
                }
                
              
                    
                
                else {
                    
                    profileIdByContactType =  MyMellanoxSettings.SystemSupportUserProfileId;
                    theContact.Portal_Access__c = 'Mellanox System Customer';        
                } 
                     
                 TheUser.ProfileId = profileIdByContactType;
                  
                if(TheContact.FirstName != null) {
                 
                    if (TheContact.LastName.length() < 3) {
                        
                        TheContact.LastName = (TheContact.LastName.length() > 1) ? TheContact.LastName.substring(0,2) : TheContact.LastName;
                        
                        TheUser.Alias = TheContact.FirstName.substring(0,0)+  TheContact.LastName;
                     }
                    
                    else {
                        
                        TheUser.Alias = TheContact.FirstName.substring(0,0)+TheContact.LastName.substring(0,3);
                    }
                }
                
                
                
                 system.debug('the User :' + TheUser);
                 // We're preventing the creation of a GroupMember from the 'Add_User_To_CRM_Content_group' trigger, as we create it with a future job
                 // This is in order to prevent the exception thrown as a result of mixed DML oporations 
                 ENV.createGroupMember = false;
                 
                 
                if (TheAccount.Id != null) {
                     system.debug('==> inside 290');
                    theContact.AccountId = TheAccount.Id;
                }
                
                else {
                     system.debug('==> inside 295 ');
                    theContact.AccountId = NewUser.SelectedAccount;
                }
                
                system.debug('theContact.Contact_Type__c :' + theContact.Contact_Type__c);
               
                theContact.access_required__c = null;
                theContact.Technical_Owner__c = theContact.Technical_Owner__c;
                
                system.debug('NewUser.con.CRM_Content_Permissions__c : ' + NewUser.con.CRM_Content_Permissions__c);
                system.debug('NewUser.con.Contact_Type__c : ' + NewUser.con.Contact_Type__c);
                
                if ( NewUser.con.CRM_Content_Permissions__c != null && NewUser.con.CRM_Content_Permissions__c != '' && 
                (theContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer') ||
                theContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer') )) {
                    
                    theContact.CRM_Content_Permissions__c = NewUser.con.CRM_Content_Permissions__c;
                }
                system.debug('theContact.AccountId ' + theContact.AccountId);
                
                boolean contactTypeDesignIn = false;
                
                if (TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer') ||
                theContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer') ) {
                    
                    contactTypeDesignIn = true;
                }
                
                
                try {
                    
                    if (theContact != null) {
                        
                        ENV.createCRMGroup = false;
                        String theContact_Str  = JSON.serialize(theContact);
                        // Update of the Contact will be done in a different thread using a @Future method
                        //futureUpdateTheContact(theContact_Str);
                        system.debug('theContact : ' + theContact.AccountId);
                        update theContact;
                    }
                }
                
                catch (Exception e) {
                    
                    system.debug('exception thrown :' + e.getMessage());
                    errMessage = 'An error has occured  : \n '+ e.getMessage();
                    return;
                }
                
                
                String theUserStr  = JSON.serialize(TheUser);
                CRM_Content_Permissions = NewUser.con.CRM_Content_Permissions__c;
            
                // Update of the Contact will be done in a different thread using a @Future method
                
                futureUpdateTheContact(theUserStr, roleName_Str, foundManagerRole, contactTypeDesignIn, CRM_Content_Permissions);
               
               
                //Database.SaveResult sr = Database.insert(TheUser, false);
                 
          /*      if (!foundManagerRole && TheContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer')) {
                 
                     list<UserRole> userRole = [SELECT Id FROM UserRole WHERE Name =: roleName_Str];
                     SYSTEM.DEBUG('userRole : ' + userRole);
                                
                     
                     if (!userRole.isEmpty()) {
                        
                        TheUser.UserRoleId = userRole[0].Id;
                     }
                 
                    update TheUser;
                 }
                            
         */           
                
                errMessage = 'A new user has been created';
                
                system.debug('==>TheUser.Id: '+ TheUser.Id);
                
                
                
                system.debug('CRM_Content_Permissions : ' + NewUser.con.CRM_Content_Permissions__c);
                
                
                if(NewUser.con.CRM_Content_Permissions__c !=null && NewUser.con.CRM_Content_Permissions__c != '' && NewUser.con.CRM_Content_Permissions__c != 'Other')
                {
                    addToPublicGroup(NewUser.con.CRM_Content_Permissions__c,TheUser.Id);
                }
                
                //ApexPages.Message PMsg = new ApexPages.Message(ApexPages.severity.INFO,'A new user has been succesfully created');
                //ApexPages.addMessage(PMsg);
                //displayWholePage = false;
                displayCancelButton = true;
                return ;
                
            
             
                
                
                //errMessage = 'An error has occured  : \n '+ e.getMessage();
                //ApexPages.Message PMsg = new ApexPages.Message(ApexPages.severity.error, 'An error has occured  : ' + ex2.getMessage().SubString(0,10));
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occured  : '));
                //ApexPages.addMessage(PMsg);
             
        }    
    }
    
    public Id getProfileId(string PName)
    {
        system.debug('==>PName'+PName);
        system.debug('==>Id Profile'+[select Id From Profile where Name =: PName ].Id);
        return [select Id From Profile where Name =: PName ].Id ;
    }
    
   
   
    @future
    public static void addToPublicGroup(string CRM_Content_Permissions , Id userId)
    {
        //system.debug('==>NewUser.con.CRM_Content_Permissions__c  '+ CRM_Content_Permissions);
       // system.debug('==>TheUser.Id  '+userId);
       // GroupMember temp_GM = new GroupMember (UserOrGroupId=userId, GroupId = [Select g.Name, g.Id From Group g where Name =: CRM_Content_Permissions limit 1].Id);
       // insert (temp_GM);
    }
    
    
   
    public void CreateAccount()
    {
        
        TheAccount.domain__c = theContact.domain__c;
        InsertRecord(TheAccount);
        system.debug('==> DONE: '+TheAccount.Id);
    
        if(TheAccount.Id!=null)
        {
                errMessage = TheAccount.Name+ ': Account was created';
        }
        
        else {
            
            accExistsErrMessage = 'This account already exists, Please enter a different account name';
            
            return;
        }
        
        newCreateadAcc = TheAccount;
        newAccCreated = true;
        
        redirectBackToCreatePortalUserPage();
        
                
    } 
    
    private pageReference redirectBackToCreatePortalUserPage() {
        
        closePopup();
        diaplayTecOwnerFields = true;
        displayWholePage = true;
        
        system.debug('diaplayTecOwnerFields 407 :' + diaplayTecOwnerFields);
        PageReference p = new PageReference('/apex/VF_CCPU?Id=' + theContact.Id);
        p.setRedirect(true);    
        return p;
    }
    
    
    public void closePopup() 
    {
        displayPopup = false;
    } 
        
    public void showPopup() 
    { 
        if(NewUser.SelectedAccount == 'New') 
        {      
            
            if (theContact.Contact_Type__c.EqualsIgnoreCase('Mellanox Silicon Design-In customer')
                || TheContact.Contact_Type__c.EqualsIgnoreCase('Ezchip Design-In customer')) {
                
                system.debug('displayTechOwnerCNA true');
                
                displayTechOwnerCNA = true;
            }
            
            else {
                
                system.debug('displayTechOwnerCNA false');
                displayTechOwnerCNA = false;
            }
            displayPopup    = true;
             
            TheAccount.Name = theContact.Company_Name_from_Web__c;
            
        }
        
    }
    
    public void InsertRecord (sobject sobj)
    {
        system.debug('start insert ');
        Database.SaveResult sr = Database.insert(sobj, false);
        
        if(!sr.isSuccess())
        {
            system.debug('==>error : '+sr.getErrors());
            
            return;
            
        }
       
    }
    
    public void getprintAccountPicked()
    {
        system.debug('inside 470 :' );
        system.debug('SelectedAccount :' + NewUser.SelectedAccount);
        system.debug('inside 470 :' );
        
        if (NewUser.SelectedAccount != null)
        {
            Account selectedAcc = [SELECT Technical_Owner__r.Id FROM Account Where Id =: NewUser.SelectedAccount];
            
            NewUser.defaultTechUserId =  selectedAcc.Technical_Owner__r.Id;
            TheContact.Technical_Owner__c = NewUser.defaultTechUserId;
            system.debug('TheContact.Technical_Owner__c : ' + TheContact.Technical_Owner__c);
            return;
        }
        
        else
        {
             errMessage = 'You need to have an Account before checking this checkbox';
        }
    }
    
    
    //This is an Async task that will insert the User Asynchronically as Mixed DML isn't permitted for cross objects
    @future
    public static void futureUpdateTheContact(String theUserStr,String roleName_Str, boolean foundManagerRole, boolean contactTypeDesignIn, String CRM_Content_Permissions) {
        
        ENV.createCRMGroup = false;
        
        User UserToCreate = cls_Create_User.insertUser(theUserStr, null, true, false); 
         set<Id> ContactIds_Set = new set<Id>();
        map<Id, Id> contactIds2UserIds_Map = new map<Id, Id>();
            
        if (UserToCreate.ContactId != null) {
            
            ContactIds_Set.add(UserToCreate.ContactId);
            contactIds2UserIds_Map.put(UserToCreate.ContactId, UserToCreate.Id);
        }
        
        
        if (!ContactIds_Set.isEmpty()) {
            
           list<RMA__c> rma2Update_List = new list<RMA__c>();
            
            map<Id, list<RMA__c>> contacts2ListsOfRMA_Map = getMapOfContacts2RMA(ContactIds_Set);
            
            if (!contacts2ListsOfRMA_Map.values().isEmpty()) {
                
                for (Id contactId : contacts2ListsOfRMA_Map.keySet()) {
                    
                    for (RMA__c theRma : contacts2ListsOfRMA_Map.get(contactId)) {
                        
                        if (contactIds2UserIds_Map.containsKey(theRma.Contact__c)) {
                            
                            theRma.OwnerId = contactIds2UserIds_Map.get(theRma.Contact__c);
                            rma2Update_List.add(theRma);
                        }
                    }
                }
            }
            
            if (!rma2Update_List.isEmpty()) {
                
                //update rma2Update_List;  // Removed and will be executed throghout Batch apex, as it causes Mixed DML Exception when creating GroupMembers and updating RMAs 
                                           // (Batch_UpadteNewUsers_RMAs_Owner)
            }
        }
        
        
        if (UserToCreate.Id != null) {
            GroupMember temp_GM = new GroupMember (UserOrGroupId = UserToCreate.Id, GroupId = [Select g.Name, g.Id From Group g where Name =: CRM_Content_Permissions limit 1].Id);
            insert (temp_GM);
        }
        
         /*****************   REMOVED BY ELAD 24/11/14        ************************    
           
            if (!foundManagerRole && contactTypeDesignIn) {
                 
             UserRole userRole = [SELECT Id FROM UserRole WHERE Name =: roleName_Str];
                        SYSTEM.DEBUG('userRole : ' + userRole);
                        
             UserToCreate.UserRoleId = userRole.Id;
         
            update UserToCreate;
         }
         
         /*****************   REMOVED BY ELAD 24/11/14        ************************/
       
    }
    
    
    
    // This methos will return a map of User's contact ids as keys and lists of RMAs as values
    private static map<Id, list<RMA__c>> getMapOfContacts2RMA(set<Id> ContactIds_Set) {
        
        map<Id, list<RMA__c>> contacts2ListsOfRMA_Map = new map<Id, list<RMA__c>>();
            
        list<RMA__c> relatedRMAs_List = [SELECT Id, OwnerId, Contact__c FROM RMA__c WHERE Contact__c IN : ContactIds_Set];
        
        if (!ContactIds_Set.isEmpty()) {
            
            for (RMA__c currentRMA : relatedRMAs_List) {
                
                if (contacts2ListsOfRMA_Map.containsKey(currentRMA.Contact__c)) {
                    
                    contacts2ListsOfRMA_Map.get(currentRMA.Contact__c).add(currentRMA);
                }
                
                else {
                    
                    contacts2ListsOfRMA_Map.put(currentRMA.Contact__c, new list<RMA__c>{currentRMA});
                }
            }
        }
        
        return contacts2ListsOfRMA_Map;
    }
    
    
}