public with sharing class cls_trg_PriceBookEntry {
	
	private static set<String> productsNames_Set = new set<String>();
	private	static set<String> pricebooksNames_Set = new set<String>();
	private static set<String> productsNamesOEMPB_set = new set<String>();
	
	public void executeFutureCreateStandardPBEntrieslist(list<Custom_Price_Book__c> newCustomPBE_List) {
		
		list<String> newCustomPBE_ListAsStrings = new list<String>();
		
		for (Custom_Price_Book__c custPBEntry : newCustomPBE_List) {
			
			String custPBEntryAsJSON_Str = JSON.serialize(custPBEntry);
	        newCustomPBE_ListAsStrings.add(custPBEntryAsJSON_Str); 
		}
		
		createStandardPBEntries(newCustomPBE_ListAsStrings);
	}
	
	@future
	public static void createStandardPBEntries(list<String> newCustomPBE_ListAsStrings) {
		
		
		list<Custom_Price_Book__c> newCustomPBE_List = new list<Custom_Price_Book__c>();
		
		for (String custPBEntryAsJSON_Str : newCustomPBE_ListAsStrings) {
			
			Custom_Price_Book__c custPBEntry = (Custom_Price_Book__c)JSON.deserialize(custPBEntryAsJSON_Str, Custom_Price_Book__c.class);
			newCustomPBE_List.add(custPBEntry);
		}
		
		// set of Pricebook names containing all the pricebooks that will have PBEntries relate to them created/updated upon insert of custom PBEntry
		// that relate to OEM Pricebook
		set<String> priceBooksForOEMNames_Set = new set<String>{'Certified Reseller Price Book', 'DB1 Price Book','Distributor Price Book', 'Standard Price Book'
		,'Dell Pricebook', 'MSRP', '1+', '50+', 'China Support'};
		
		
		// List of all the pricebooks that their PBEntry needs to be created/updated when a custom PBEntry is inserted and is relate to OEM Pricebook
		list<Pricebook2> priceBooksNeeds2havePBentriesCreatedfor = [SELECT Id, Name FROM Pricebook2 WHERE Name IN : priceBooksForOEMNames_Set and isActive = true];
		// map of Product Names and their related product in lists  
		map<String, list<PricebookEntry> > products2PBEntryLists_map = getPBEntriesMap(newCustomPBE_List);
		// map of custom PBEntry Ids and their related PBEntry if exist
		map<Id, PricebookEntry> custpPbEntriyIds2PBEntry_Map = getPbEntriyIds2CustomPBEntrymap(newCustomPBE_List, products2PBEntryLists_map);
		// map of Product Names and their related Products
		map<String, Product2> productNames2Products_Map = getProductByProductNames();
		// map of PriceBooks Names and their related PriceBooks
		map<String, Pricebook2> priceBooksNames2priceBooks_Map = getPriceBooksByPricebBooksNames();
		// map of PriceBook Names and their related calculated multipliers
		map<String, decimal> PBNames2UnitPrices_Map = populateUnitPricesForPBs();
		// list of PBEntries if exist under the Pricebooks where update is needed when a custom PBEntry is inserted with OEM Pricebook
		list<PricebookEntry> OEMRelatedPBEntries_List = getPBEntriesRelatedToOEMrelatedPriceBooks(newCustomPBE_List, PBNames2UnitPrices_Map);
		
		// list of PBEntry to create
		list<PricebookEntry> pbEntries2Insert_List = new list<PricebookEntry>();
		// list of PBEntry to update
		list<PricebookEntry> pbEntries2Update_List = new list<PricebookEntry>();
		// boolean indicating if a PBEntry relate to each OEM related pricebooks is founded
		boolean foundPBEntryOEMRelatedPB = false;
		for (Custom_Price_Book__c custPBEntry : newCustomPBE_List) {
			
			if (custPBEntry.PriceBook_Name__c.EqualsIgnoreCase('OEM Price Book')) {
				
				for (Pricebook2 PB : priceBooksNeeds2havePBentriesCreatedfor) {
					
					for (PricebookEntry OEMRealtedPBEntry : OEMRelatedPBEntries_List) {
						
						if (OEMRealtedPBEntry.Pricebook2.Name.EqualsIgnoreCase(PB.Name)
							&& OEMRealtedPBEntry.Product2.Name.EqualsIgnoreCase(custPBEntry.Product__c)) {
								//KN added
								OEMRealtedPBEntry.Old_Unit_Price__c = OEMRealtedPBEntry.UnitPrice;
								//
								OEMRealtedPBEntry.UnitPrice = custPBEntry.Unit_Price__c  * PBNames2UnitPrices_Map.get(PB.Name);
								pbEntries2Update_List.add(OEMRealtedPBEntry);
								foundPBEntryOEMRelatedPB = true;
							}
					}
					
					if (!foundPBEntryOEMRelatedPB) {
						
						PricebookEntry pbEntry = new PricebookEntry();
						pbEntry.Pricebook2Id = priceBooksNames2priceBooks_Map.get(PB.Name).Id;
						pbEntry.Product2Id = productNames2Products_Map.get(custPBEntry.Product__c).Id;
						pbEntry.UnitPrice = custPBEntry.Unit_Price__c  * PBNames2UnitPrices_Map.get(PB.Name);
						pbEntry.UseStandardPrice = false;
						pbEntry.IsActive = true;
						pbEntries2Insert_List.add(pbEntry);
					}
					
					foundPBEntryOEMRelatedPB = false;
				}
				
				if (custpPbEntriyIds2PBEntry_Map.containsKey(custPBEntry.Id)) {
				
					PricebookEntry pbEntry = custpPbEntriyIds2PBEntry_Map.get(custPBEntry.Id);
					//KN added
					pbEntry.Old_Unit_Price__c = pbEntry.UnitPrice; 
					pbEntry.UnitPrice = custPBEntry.Unit_Price__c;
					pbEntries2Update_List.add(pbEntry);
				}
			}
			
			else if (custpPbEntriyIds2PBEntry_Map.containsKey(custPBEntry.Id)) {
				
					PricebookEntry pbEntry = custpPbEntriyIds2PBEntry_Map.get(custPBEntry.Id);
					pbEntry.Old_Unit_Price__c = pbEntry.UnitPrice;
					pbEntry.UnitPrice = custPBEntry.Unit_Price__c;
					pbEntries2Update_List.add(pbEntry);
			}
			
			else {
				
				 PricebookEntry pbEntry = new PricebookEntry();
				 pbEntry.Product2Id = productNames2Products_Map.get(custPBEntry.Product__c).Id;
				 pbEntry.Pricebook2Id = priceBooksNames2priceBooks_Map.get(custPBEntry.PriceBook_Name__c).Id;
				 pbEntry.UnitPrice = custPBEntry.Unit_Price__c;
				 pbEntry.UseStandardPrice = false;
				 pbEntry.IsActive = true;
				 pbEntries2Insert_List.add(pbEntry);
			}
		}
		
		if (!pbEntries2Insert_List.isEmpty()) {
			
			Database.SaveResult[] lst_SR = Database.insert(pbEntries2Insert_List, false);
								 	
		    for(Database.SaveResult sr: lst_SR) {
		     
		        if(!sr.isSuccess()) {
		         
		        	system.debug('==>Insert sr.getMessage(): ' + sr.getErrors() + 'Id is :' + sr.getId());
		        }
		    }
		}
		
		if (!pbEntries2Update_List.isEmpty()) {
			
			Database.SaveResult[] lst_SR = Database.update(pbEntries2Update_List, false);
								 	
		    for(Database.SaveResult sr: lst_SR) {
		     
		        if(!sr.isSuccess()) {
		         
		        	system.debug('==>update sr.getMessage(): ' + sr.getErrors() + 'Id is :' + sr.getId());
		        }
		    }
		}
		
	}
	
	private static map<Id, PricebookEntry> getPbEntriyIds2CustomPBEntrymap(list<Custom_Price_Book__c> newCustomPBE_List, map<String, list<PricebookEntry> > products2PBEntryLists_map) {
		
		map<Id, PricebookEntry> custpPbEntriyIds2PBEntry_Map = new map<Id, PricebookEntry>();
		
		if (!products2PBEntryLists_map.values().isEmpty()) {
			
			for (Custom_Price_Book__c custPBEntry : newCustomPBE_List) {
				
				list<PricebookEntry> pbEntries_List = products2PBEntryLists_map.get(custPBEntry.Product__c);
				
				if (!pbEntries_List.isEmpty()) {
					
					for (PricebookEntry pbEntry : pbEntries_List) {
						
						if (pbEntry.Pricebook2.Name.EqualsIgnoreCase(custPBEntry.PriceBook_Name__c)) {
							
							custpPbEntriyIds2PBEntry_Map.put(custPBEntry.Id, pbEntry);
						}
					}
				}
			}
		}
		
		return custpPbEntriyIds2PBEntry_Map;
	}
	
	
	private static map<String, list<PricebookEntry> > getPBEntriesMap(list<Custom_Price_Book__c> newCustomPBE_List) {
		
		
		map<Id, Product2> productIds2Products_Map = new map<Id, Product2>();
		
		list<Pricebook2> pricebooks_List = [SELECT Id, Name FROM Pricebook2 WHERE isActive = true];
		
		for (Custom_Price_Book__c custPBE : newCustomPBE_List) {
			
			productsNames_Set.add(custPBE.Product__c);
			pricebooksNames_Set.add(custPBE.PriceBook_Name__c);
			
			if (custPBE.PriceBook_Name__c.EqualsIgnoreCase('OEM Price Book')) {
				
				productsNamesOEMPB_set.add(custPBE.Product__c);
			}
		}
		
	    list<PricebookEntry> priceBookEntries_List = [SELECT Id,Old_Unit_Price__c,UnitPrice, Product2Id, Pricebook2Id, Name, Product2.Name, Pricebook2.Name
													  FROM  PricebookEntry WHERE Product2.Name IN : productsNames_Set and Pricebook2.Name IN : pricebooksNames_Set
													  and Product2.isActive = true and Pricebook2.isActive = true];
		
		map<String, list<PricebookEntry> > products2Names2PBEntryLists_map = new map<String, list<PricebookEntry> >();
		
		if (!priceBookEntries_List.isEmpty()) {	
			
			for (PricebookEntry entry : priceBookEntries_List) {
				
				if (products2Names2PBEntryLists_map.containsKey(entry.Product2.Name)) {
					
					products2Names2PBEntryLists_map.get(entry.Product2.Name).add(entry);
				}
				
				else {
					
					products2Names2PBEntryLists_map.put(entry.Product2.Name, new list<PricebookEntry>{entry});
				}
			}
		}
		
		return products2Names2PBEntryLists_map;
	}
	
	private static map<String, Product2> getProductByProductNames() {
		
		map<String, Product2> productNames2Products_Map = new map<String, Product2>();
		list<Product2> products_List = [SELECT Id, Name FROM Product2 WHERE Name IN : productsNames_Set and isActive = true];
		
		for (Product2 product : products_List) {
			
			productNames2Products_Map.put(product.Name, product);
		}
		
		return productNames2Products_Map;
	}
	
	private static map<String, Pricebook2> getPriceBooksByPricebBooksNames() {
		
		map<String, Pricebook2> priceBooksNames2priceBooks_Map = new map<String, Pricebook2>();
		list<Pricebook2> priceBooks_List = [SELECT Id, Name FROM Pricebook2 WHERE isActive = true];
		
		for (Pricebook2 priceBook : priceBooks_List) {
			
			priceBooksNames2priceBooks_Map.put(priceBook.Name, priceBook);
		}
		
		return priceBooksNames2priceBooks_Map;
	}
	
	private static map<String, decimal> populateUnitPricesForPBs() {
		
		map<String, decimal> PBNames2UnitPrices_Map = new map<String, decimal>();
		
		PBNames2UnitPrices_Map.put('Certified Reseller Price Book', 1.02);
		PBNames2UnitPrices_Map.put('DB1 Price Book', 1.14);
		PBNames2UnitPrices_Map.put('Distributor Price Book', 1.2);
		PBNames2UnitPrices_Map.put('Standard Price Book', 1);
		PBNames2UnitPrices_Map.put('Dell Pricebook', 1.53);
		PBNames2UnitPrices_Map.put('MSRP', 2.46);
		PBNames2UnitPrices_Map.put('1+', 1.32);
		PBNames2UnitPrices_Map.put('50+', 1.26);
		PBNames2UnitPrices_Map.put('China Support', 1.03);
		
		return PBNames2UnitPrices_Map;
	}
	
	private static list<PricebookEntry> getPBEntriesRelatedToOEMrelatedPriceBooks(list<Custom_Price_Book__c> newCustomPBE_List, map<String, decimal> PBNames2UnitPrices_Map) {
		
		list<PricebookEntry> OEMRelatedPBEntries_List =  [SELECT Id,Old_Unit_Price__c,UnitPrice, Product2Id, Pricebook2Id, Name, Product2.Name, Pricebook2.Name
													 FROM  PricebookEntry WHERE Product2.Name IN : productsNamesOEMPB_set and Pricebook2.Name IN : PBNames2UnitPrices_Map.keySet()
													 and Product2.isActive = true and Pricebook2.isActive = true];
	
		return OEMRelatedPBEntries_List;
	}

}