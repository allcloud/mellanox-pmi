public with sharing class cls_trg_Asset2 {
	
	
	public void assignAssetsDateToRelatedLicenses(list<Asset2__c> newAssets_List) {
	/*	
		map<Id, list<License__c>> relatedLicenses_Map = getRelatedAssets(newAssets_List);
		list<License__c> licenses2Update_List = new list<License__c>();
		
		for (Asset2__c newAsset : newAssets_List) {
			
			list<License__c> relatedLicenses_List = new list<License__c>();
			if (relatedLicenses_Map.containsKey(newAsset.Order__c)) {
				
				relatedLicenses_List = relatedLicenses_Map.get(newAsset.Order__c);
			}
			
			else if (relatedLicenses_Map.containsKey(newAsset.Moved_from_contract__r.Order__c)) {
				
				relatedLicenses_List = relatedLicenses_Map.get(newAsset.Moved_from_contract__r.Order__c);
			}
			
			if (!relatedLicenses_List.isEmpty()) {
				
				for (License__c relatedLicense : relatedLicenses_List) {
				
					relatedLicense.Contract_End_Date__c = newAsset.Date__c;
					licenses2Update_List.add(relatedLicense);
				}
			}
		}
		
		if (!licenses2Update_List.isEmpty()) {
			
			update licenses2Update_List;
		}
		
		*/
	}
	
	
	
	// This method will return a list of License__c, Where the Asset's Order__c or the Asset's  Moved_from_contract__r.Order__c
	// equals to the License Sales_Order__c 
	private map<Id, list<License__c>> getRelatedAssets(list<Asset2__c> newAssets_List) {
		
		set<Id> AssetsOrdersIds_Set = new set<Id>();
		map<Id, list<License__c>> relatedLicenses_Map = new map<Id, list<License__c>>();
		
		for (Asset2__c newAsset : newAssets_List) {
			
			if (newAsset.Asset_Family__c != null && newAsset.Asset_Family__c.EqualsIgnoreCase('SW')) {
				
				if (newAsset.Order__c != null) {
					
					AssetsOrdersIds_Set.add(newAsset.Order__c);
				}
				
				if (newAsset.Moved_from_contract__r.Order__c != null) {
					
					AssetsOrdersIds_Set.add(newAsset.Moved_from_contract__r.Order__c);
				}
			}
		}
		
		if (!AssetsOrdersIds_Set.isEmpty()) {
			
			list<License__c> relatedLicenses_List = [SELECT Id, Contract_End_Date__c, Sales_Order__c  FROM License__c
								  WHERE Sales_Order__c IN : AssetsOrdersIds_Set];
								  
		
			for (License__c currentLicense : relatedLicenses_List) {
				
				if (relatedLicenses_Map.containsKey(currentLicense.Sales_Order__c)) {
					
					relatedLicenses_Map.get(currentLicense.Sales_Order__c).add(currentLicense);
				}
				
				
				relatedLicenses_Map.put(currentLicense.Sales_Order__c, new list<License__c> {currentLicense});
			}					  
		}
		
		return relatedLicenses_Map;
	} 
	
	// This method will relate new Assets with related Products (where Asset Part Number represents the Product Name)
	public void relateNewAssetsWithProducts(list<Asset2__c> newAssets_List) {
		
		map<String, Id> partNumbers2ProductsIds_Map = getProductsByAssets(newAssets_List);
		
		for (Asset2__c newAsset : newAssets_List) {
			 
			if (newAsset.Part_Number__c != null) {
				 
				if (partNumbers2ProductsIds_Map.containsKey(newAsset.Part_Number__c)) {
					 
					newAsset.Product__c = partNumbers2ProductsIds_Map.get(newAsset.Part_Number__c);
				}
			}
		}
	}
	
	
	// This method returns a map of Assets Part Numbers as Keys and Product Ids as values
	private map<String, Id> getProductsByAssets(list<Asset2__c> newAssets_List) {
		
		set<String> partNumbersNames_Set = new set<String>();
		map<String, Id> partNumbers2ProductsIds_Map = new map<String, Id>();
		
		for (Asset2__c newAsset : newAssets_List) {
			
			if (newAsset.Part_Number__c != null) {
				 
				partNumbersNames_Set.add(newAsset.Part_Number__c);
			}
		}
		
		if (!partNumbersNames_Set.isEmpty()) {
			 
			for (Product2 product : [SELECT Id, Name FROM Product2 WHERE Name IN : partNumbersNames_Set]) {
				 
				partNumbers2ProductsIds_Map.put(product.Name, product.Id);
			}
		}
 		return partNumbers2ProductsIds_Map;
	}

}