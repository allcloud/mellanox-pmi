public class User_trg {

	public void deleteUsersRelatedContentUpdates(list<User> newUsers_List, map<Id, User> oldUsers_Map) {
		
		set<Id> usersIds_Set = new set<Id>();
		
		for (User newUser : newUsers_List) {
			
			if ( newUser.Delete_Content_Update__c != false || newUser.Delete_Content_Update__c != oldUsers_Map.get(newUser.Id).Delete_Content_Update__c ) {
			
				newUser.Delete_Content_Update__c = false;
				usersIds_Set.add(newUser.Id);
			}
		}
		
		list<Content_Update__c> relatedContentUpdates_list = [SELECT Id FROM Content_Update__c WHERE User__r.Id IN : usersIds_Set];
		
		if (!relatedContentUpdates_list.isEmpty()) {
			
			 delete relatedContentUpdates_list;
		}
	}
}