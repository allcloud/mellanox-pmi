global class DeleteFRU implements Database.Batchable<sObject>{

global string query;
global list<id> Assets; 

global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query );
                   }
 


global void execute(Database.BatchableContext BC, List<sObject> scope){  
          
List<Asset2__c> DelAsst = new List<Asset2__c>();

                                                            
for(sObject s : scope){ Asset2__c ast = (Asset2__c)s;
     
system.debug('Retrieved asset ='+ ast);
ast.to_delete__c = true;
DelAsst.add(ast);
                             
}
system.debug('Del asset ='+ DelAsst[0]);

  update  DelAsst;     
}
          
               
global void finish(Database.BatchableContext BC){
/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.setToAddresses(new String[] {'innag@mellanox.co.il'});
mail.setReplyTo('batch@acme.com');
mail.setSenderDisplayName('Batch Processing');
mail.setSubject('Batch Process Completed');
mail.setPlainTextBody('Query :'+query);
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

}
}