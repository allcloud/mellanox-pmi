public class myOrdersController { 
 
  // the soql without the order and limit
  private String soql {get;set;}
  // the collection of contacts to display
  public List<BBB_ORDERS__C> orders {get;set;}
  String userID = UserInfo.getUserID();
  User activeUser = [Select Username,Rep_Access__c,Profile_text__c,Customer_Buyer__c,contactAccount__C,Exclude_Bill_to__c From User where ID= : userID limit 1]; 
  public String userEmail {get; set; }
  boolean superuser = activeUser.Customer_Buyer__c;
  string customerAccount= activeUser.contactAccount__C;
  string customerProfile=activeUser.Profile_text__c;
  string resellerAccess=activeUser.Rep_Access__c;
  string excludeBill=activeUser.Exclude_Bill_to__c;
  private String isExportALL;
  
  // the current sort direction. defaults to des
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to BBBSDT__C
  public String sortField {
    get  { if (sortField == null) {sortField = 'CUST_PO_NUMBER__C'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir; }
    set;
  }
 
  // init the controller and display some sample data when the page loads
  public myOrdersController() {
    userEmail = activeUser.Username;
	//For Denny Gonzalez @Seagate where his email and user name is different
	if(userEmail == 'denny.gonz@seagate.com')
		userEmail = 'denny.gonzalez@seagate.com';
    //KN handle Elain Carrol @ HP
    if (customerAccount=='HEWLETT-PACKARD'){
    	soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:30 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\'';
    }
    //
    else if (superuser!=true && customerProfile!='MyMellanox Distributor User' ) { 
        soql = 'Select End_Customer_Name__c,Ship_to_Country__c,Ship_to_Account__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:25 or BBBSDT__C=NULL) and  buyer_email__C <>\'\' and buyer_email__C=\'' + userEmail +'\'';
        //soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:25 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\'';
    }else if (customerProfile!='MyMellanox Distributor User') {
        if (excludeBill==''){
        	soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:25 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\'';
        }else{
         	soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:25 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\' and BILL_TO_ACCOUNT__c!=\'' + excludeBill  +'\'';
  
        }
    }else if (customerProfile=='MyMellanox Distributor User'){
        //KN added 07-01-14 -Governor limit
        if (customerAccount=='INGRAM MICRO (US)')
            soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:30 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c=\'' + customerAccount  +'\'';
        else
            soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:45 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c=\'' + customerAccount  +'\'';
    }
    //04-10-15 Avnet Asia needs to see Avnet TW HK SG sites
    //KN added 06-13-14 handling AVNET TAIWAN account
    if (customerAccount=='AVNET ASIA PTE. LTD.' || userEmail=='man.cheung@avnet.com' ){
        customerAccount = 'AVNET%';
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and (Bill_to_Country__c=\'TW\' or Bill_to_Country__c=\'HK\' or Bill_to_Country__c=\'SG\') and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';
    }
    //KN added 06-13-14 handling AVNET TAIWAN account
    if (customerAccount=='AVNET TAIWAN'){
        customerAccount = 'AVNET%';
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:60 or BBBSDT__C=NULL)  and Bill_to_Country__c=\'TW\' and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';
    }
    if (customerAccount=='INGRAM CHINA'){
        customerAccount = 'INGRAM MICRO TRADING (SHANGHAI)%'; //INGRAM MICRO TRADING (SHANGHAI) CO,. LTD.,
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:90 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';
    }
    //KN added 8-25-14 handling OCS/Isolin
    if (customerAccount=='OCS'){
        customerAccount = 'Isolin%';
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:90 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';
    }
    if (customerAccount=='ZYCKO LTD.'){
        customerAccount = 'ZYCKO%';
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:90 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';
    }
    //
    if (customerAccount=='BOSTON LTD'){
        customerAccount = 'BOSTON LIMITED';
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:90 or BBBSDT__C=NULL)  and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';
    }
    //
    if (resellerAccess=='Paragon'){
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:45 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and REP_COMISSIONS__C like \'%PT%\'';
        
    }else if (resellerAccess=='Universal'){
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:45 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and REP_COMISSIONS__C like \'%UT%\'';
    } 
    //runQuery();
  }
  public void init() {
  	isExportALL = 'No';
  	runQuery();
  } 
  public void init_exportALL() {
		soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\'';
		isExportALL = 'Yes';
		runQuery();  	
  }
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
   public PageReference export() 
   {
        return page.MyOrdersCSV;
    }
   public PageReference cust_inquiry() 
   {
        Pagereference pg = page.Customer_Order_Inquiry;
      	return pg;
   }
   public PageReference exportALL() 
   {
        Pagereference pg = page.MyOrdersCSV_ExportAll;
        //pg.setRedirect(true);
      	return pg;
   }
  // runs the actual query
  public void runQuery() {
 
    try {
      //orders = Database.query(soql + ' order by ' + sortField + ' ' + sortDir);
      //if (customerAccount=='HEWLETT-PACKARD'){
      if(isExportALL == 'Yes'){
      		soql = soql + ' and ( (Released_status_name__c!=\'Shipped\' and (BBBSDT__C>=LAST_N_DAYS:300 or BBBSDT__C=NULL) ) or (Released_status_name__c=\'Shipped\' and Actual_Shipment_Date__c>=LAST_N_DAYS:45) )';
      		orders = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' ' + 'limit 1000');
      }
      else
      	orders = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' ' + 'limit 400');
    } catch (Exception e) {
            ApexPages.addMessages(e);
     // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.message));
    }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String CUST_PO_NUMBER = Apexpages.currentPage().getParameters().get('CUST_PO_NUMBER');
    String SO_NUMBER_TEXT = Apexpages.currentPage().getParameters().get('SO_NUMBER_TEXT');
    String Ref_only_PN = Apexpages.currentPage().getParameters().get('Ref_only_PN');
    String Ordered_item = Apexpages.currentPage().getParameters().get('Ordered_item');
     if (superuser!=true && customerProfile!='MyMellanox Distributor User' ) { 
        soql = 'Select End_Customer_Name__c,Ship_to_Country__c,Ship_to_Account__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and  SALES_ORDER_NUMBER__C<700000 and S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:180 or BBBSDT__C=NULL) and  buyer_email__C <>\'\' and buyer_email__C=\'' + userEmail +'\'';
    }else if (customerProfile!='MyMellanox Distributor User') {
 		if (excludeBill==''){
        	soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:365 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\'';
        }else{
         	soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:365 or BBBSDT__C=NULL)  and DIST_ORDER_TYPE__C=\'Regular\' and customer_parent__C=\'' + customerAccount  +'\' and BILL_TO_ACCOUNT__c!=\'' + excludeBill  +'\'';
  		}    
 	}else if (customerProfile=='MyMellanox Distributor User'){
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:180 or BBBSDT__C=NULL) and BILL_TO_ACCOUNT__c like \'' + customerAccount  +'\'';
    }
    if (resellerAccess=='Paragon'){
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:180 or BBBSDT__C=NULL) and REP_COMISSIONS__C like \'%PT%\'';
        
    }else if (resellerAccess=='Universal'){
        soql = 'Select End_Customer_Name__c,Ship_to_Account__c,Ship_to_Country__c,Ship_method_meaning__c, Waybill__c, SO_NUMBER_TEXT__C,CUST_PO_NUMBER__C,Ordered_item__C,Ref_only_PN__c,Quantitycalc__c,Customer_Requested_Date__c,CSD__C,Actual_Shipment_Date__c,Booked_Date__c,Order_Create_Date__c,ID,buyer_email__C,Delivery_Number__c from BBB_ORDERS__C where  CUST_PO_NUMBER__C!=NULL and SALES_ORDER_NUMBER__C<700000 and  S__C!=\'BKS\' and (BBBSDT__C>=LAST_N_DAYS:180 or BBBSDT__C=NULL) and REP_COMISSIONS__C like \'%UT%\'';
    } 
   
      
    if (!CUST_PO_NUMBER.equals(''))
      soql += ' and CUST_PO_NUMBER__C LIKE \''+String.escapeSingleQuotes(CUST_PO_NUMBER)+'%\'';
    if (!SO_NUMBER_TEXT.equals(''))
      soql += ' and SO_NUMBER_TEXT__C LIKE \''+String.escapeSingleQuotes(SO_NUMBER_TEXT)+'%\'';
    if (!Ref_only_PN.equals(''))
      soql += ' and Ref_only_PN__c LIKE \''+String.escapeSingleQuotes(Ref_only_PN)+'%\'';  
    if (!Ordered_item.equals(''))
      soql += ' and Ordered_item__C LIKE \''+String.escapeSingleQuotes(Ordered_item)+'%\'';  
 
    // run the query again
    runQuery();
 
    return null;
  }
 
  // use apex describe to build the picklist values

 
}