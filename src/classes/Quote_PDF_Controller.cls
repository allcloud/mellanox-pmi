global class Quote_PDF_Controller {
	
	@Future(callout=true)
    public static void addPDFAttach(string sessionId, list<id> quoteIds){
        Quote_addPDFStub.SessionHeader_element sessionIdElement= new Quote_addPDFStub.SessionHeader_element();
        sessionIdElement.sessionId = sessionId;
 
        Quote_addPDFStub.Quote_GeneratePDF_webservice stub= new Quote_addPDFStub.Quote_GeneratePDF_webservice();
        stub.SessionHeader= sessionIdElement;
        stub.addPDF(quoteIds);
    }
    
}