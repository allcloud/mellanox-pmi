public with sharing class cls_trg_CSI_Assets {
	
    // This metohd will create a CSI_assets_log__c record which contains data about CSI_Asset__c.
    public void insertCSIAssetsLog(list<CSI_Asset__c> newCSIAsset_List) {
    	
    	list<CSI_assets_log__c> CSIAssetsLog2Create_List = new list<CSI_assets_log__c>();
    	
    	for (CSI_Asset__c newCSIAsset : newCSIAsset_List) {
    		
    		if(newCSIAsset.CSI_Ticket_Number__c != null){
    			
	    		CSI_assets_log__c newCSIAssetsLog = new CSI_assets_log__c();
	    		newCSIAssetsLog.Case_assignment_date__c= DateTime.now();
	    		newCSIAssetsLog.Case__c = newCSIAsset.CSI_Ticket_Number__c;
	    		newCSIAssetsLog.CSI_asset__c = newCSIAsset.id;
	    		newCSIAssetsLog.CSI_asset_status__c = 'Assigned';
	    		
	    		CSIAssetsLog2Create_List.add(newCSIAssetsLog);
    		}
    	}
    	
    	System.debug(Logginglevel.INFO,'print CSIAssetsLog2Create_List : '+ CSIAssetsLog2Create_List);
    	
    	if (!CSIAssetsLog2Create_List.isEmpty()) {
            try {
                insert CSIAssetsLog2Create_List;
            }
            catch (Exception E) {
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Failed to inset CSI assets log'));
				system.debug(LoggingLevel.Error, 'EXCEPTION in class cls_trg_CSI_Assets. '+e.getMessage()+' Line '+e.getLineNumber());
			}
        }
    }
    
  	// The system will update the field Case_release_date__c in CSI_assets_log__c.
    // if the Lookup to the case The deleted
    public void updateCSIAssetsLog(list<CSI_Asset__c> newCSIAsset_List, map<Id, CSI_Asset__c> oldCSIAsset_Map) {
    	list<CSI_assets_log__c> CSIAssetsLog2update_List;
    	set<Id> CSIAsset_IDs = new set<Id>();
    	list<CSI_Asset__c> CSIAssetsLog2Create_List = new list<CSI_Asset__c>();
    	
    	for (CSI_Asset__c newCSIAsset : newCSIAsset_List) {
    		
    		if(newCSIAsset.CSI_Ticket_Number__c != oldCSIAsset_Map.get(newCSIAsset.Id).CSI_Ticket_Number__c){
    			
    			CSIAssetsLog2Create_List.add(newCSIAsset);
    			
    			if(oldCSIAsset_Map.get(newCSIAsset.Id).CSI_Ticket_Number__c != null){
    				
    				CSIAsset_IDs.add(newCSIAsset.Id);
    			}
    		}
    	}

    	System.debug(Logginglevel.INFO,'print CSIAssetsLog2Create_List : '+ CSIAssetsLog2Create_List);
    	System.debug(Logginglevel.INFO,'print CSIAsset_IDs : '+ CSIAsset_IDs);
    	
    	if (!CSIAsset_IDs.isEmpty()) {
    		CSIAssetsLog2update_List = [Select Case_release_date__c, CSI_asset__c From CSI_assets_log__c where CSI_asset__c in: CSIAsset_IDs]; 
    		
    		if (!CSIAssetsLog2update_List.isEmpty()) {
	    		for (CSI_assets_log__c CSIAssetsLog : CSIAssetsLog2update_List) {
	    			CSIAssetsLog.Case_release_date__c = DateTime.now();
	    			CSIAssetsLog.CSI_asset_status__c = 'Available';
	    		}
	    		
	    		try {
	                update CSIAssetsLog2update_List;
	            }
	            catch (Exception E) {
	            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Failed to update CSI assets log'));
					system.debug(LoggingLevel.Error, 'EXCEPTION in class cls_trg_CSI_Assets. '+e.getMessage()+' Line '+e.getLineNumber());
				}
    		}
    	}
    	
    	if (!CSIAssetsLog2Create_List.isEmpty()) {
    		insertCSIAssetsLog(CSIAssetsLog2Create_List);
    	}
    }
}