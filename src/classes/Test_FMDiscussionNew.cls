@isTest(seeAllData=true)
private class Test_FMDiscussionNew {
/*
    static testMethod void myUnitTest() {
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc =obj.createAccount();
        acc.Name = 'unique test account for testing';
        insert acc;
         
        Contact con = obj.CreateContact(acc);
        insert con;
  
        User u = obj.CreateUser();
        insert u;
        
        Case cas = obj.Create_case(acc, con);
        cas.AE_PM_Escalated__c = 'PM';
        insert cas;
    
        
      //   Case cas = [SELECT Id, AE_PM_Escalated__c FROM Case WHERE Status = 'Open' and AE_PM_Escalated__c =: 'PM' limit 1];
        
        
        List<FM_Discussion__c> lst_fms = new List<FM_Discussion__c>();
        
        FM_Discussion__c fm1 = obj.createFMDiscussion(cas);
        fm1.Discussion__c = 'Test discussion test test test';
        lst_fms.add(fm1);
        
     //   FM_Discussion__c fm2 = obj.createFMDiscussion(cas);
     //   lst_fms.add(fm2);
        
        RMA__c rma = obj.CreateRMA();
        insert rma;
        
        
        
        RmCase__c rmCase = new RmCase__c();
        rmCase.Name = 'Test RM case';
        rmCase.sfcase__c = cas.Id;
        insert rmCase;
        
        RmCase__c rmCase1 = new RmCase__c();
        rmCase1.Name = 'Test RM case1';
        rmCase1.sfcase__c = cas.Id;
        insert rmCase1;
        
        RmCase__c rmCase2 = new RmCase__c();
        rmCase2.Name = 'Test RM case2';
        rmCase2.sfcase__c = cas.Id;
        insert rmCase2;
        
        insert lst_fms;
              
        ApexPages.StandardController sc = new ApexPages.standardController(cas);
        ApexPages.currentPage().getParameters().put('Id', cas.Id);     
        VF_FMDiscussionNew controller =new VF_FMDiscussionNew(sc);
        
        VF_FMDiscussionNew.FMDiscussionWrapper wrapper = new VF_FMDiscussionNew.FMDiscussionWrapper(fm1);
        wrapper.IsEditMode = true;
        wrapper.CommentText = 'test';
        wrapper.ISPublic = true;
        wrapper.ISPublicRM1 = true;
        wrapper.ISPublicRM2 = true;
        wrapper.ISPublicRM3 = true;
        
        controller.searchFolderTemplates();
        controller.assignToAssigned();
        controller.AddTemplate();
        controller.SelectedTab = 'Public';
        controller.searchComments();
        controller.SelectedTab = 'Private';
        controller.searchComments();
        controller.SelectedTab = 'RM1';
        controller.searchComments();
        controller.SelectedTab = 'RM2';
        controller.searchComments();
        controller.SelectedTab = 'RM3';
        controller.searchComments();
        controller.selectedTemplate = 'template';
        controller.selectedTemplate();
        controller.SelectedCommentId = fm1.Id;
        controller.map_FMDiscussionWrapper.put(fm1.Id,wrapper );
        controller.UpdatePublicORPrivate();
        controller.UpdatePublicORPrivateRM1();
        controller.UpdatePublicORPrivateRM2();
        controller.UpdatePublicORPrivateRM3();
        controller.UpdatePublicORPrivatePrivate();
        controller.UpdatePublicORPrivatePublic();
        controller.UpdatePrivate();
        controller.SelectedCommentId = fm1.Id;
        controller.map_FMDiscussionWrapper.put(fm1.Id,wrapper );
        controller.UpdatePublic();
        controller.DeleteComment();
       // controller.EditComment();
        controller.SelectedCommentId = fm1.Id;
        controller.map_FMDiscussionWrapper.put(fm1.Id,wrapper );
        controller.ChangeRM();
        controller.SaveChangeCom();
        controller.SelectedCommentId = fm1.Id;
        controller.map_FMDiscussionWrapper.put(fm1.Id,wrapper );
        controller.SaveChanges();
        controller.SelectedCommentId = fm1.Id;
        controller.map_FMDiscussionWrapper.put(fm1.Id,wrapper );
        controller.CancelChanges();
        controller.NextPage();
        controller.PreviousPage();
        controller.FirstPage();
        controller.LastPage();
        controller.FMDiscussion = 'test discussion test test';
        controller.assignToOnHold();
        controller.assignToOpen();
        controller.assignToOpenAE();
        controller.assignToWaitForRelease();
        controller.assignToWaitingForCustomerApproval();
        controller.assignToWaitForImplementation();
        controller.assignToWaitingForCustomerInfo();
        controller.assignToAssignedAE();
        
        controller.getPage();
        
    }
*/    
}