public with sharing class VF_Case_Origin_Jive_Banner {
	
	public Case theCase {get;set;}
    public boolean is_JiveCase {get;set;}
    private ApexPages.StandardController stdController;
    public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}

    public VF_Case_Origin_Jive_Banner(ApexPages.standardController stdController) 
    { 
        is_JiveCase = false;
        Id theCaseId = stdController.getId();
        this.stdController = stdController;
        shouldRedirect = false;
        
        theCase = [SELECT id, Origin FROM Case WHERE Id =: theCaseId];
        
        if ( theCase.Origin != null && theCase.Origin.EqualsIgnoreCase('Jive'))  { 
            
            is_JiveCase = true;
        }
    }
    
    public PageReference doStuffAndRedirect() {
        shouldRedirect = true;
        redirectUrl = stdController.view().getUrl();
        return null;
    }
     
}