@istest
public with sharing class test_VF_RMA_INSA_Project_Banner {
	
	static testMethod void test_theVf() {
		
		CLS_ObjectCreator obj = new CLS_ObjectCreator();
        RMA__c R = obj.createRMA();
        R.RMA_Support_Project__c = 'test';
        insert R;
        
        Test.StartTest();
        ApexPages.standardController thePage = new  ApexPages.standardController(R);
        VF_RMA_INSA_Project_Banner controller = new VF_RMA_INSA_Project_Banner(thePage);
        Test.StopTest();
	}

}