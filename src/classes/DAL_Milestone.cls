public with sharing class DAL_Milestone {
	
	public static list<Milestone1_Milestone__c> getMilestonesByProjectsIds(set<Id> ProjectsIds_Set)
	{
		return [SELECT Id, Project__c, Complete__c, Deadline__c FROM Milestone1_Milestone__c WHERE Complete__c != true and Project__c IN : ProjectsIds_Set];
	}

}