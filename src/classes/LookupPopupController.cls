//
// Custom controller for lookup example popup page
//
public with sharing class LookupPopupController 
{
    public String query {get; set;}
    public List<Account> accounts {get; set;}
     public List<Product2> products {get; set;}
     public boolean err {get;set;}
     
     public LookupPopupController()
     {
     	err = false;
     	query = '';
     	
     }
    public PageReference runQuery()
    {
    	query = query.trim(); 
    	if(query == null || query.trim() =='' || query.length() < 2)
    	{
    		err = true;
    	}else
    	{
    		err = false;
	        //List<List<Account>> searchResults=[FIND :query IN ALL FIELDS RETURNING Account (id, name, billingstreet, billingcity, billingpostalcode)];
	        List<List<Product2>> searchResults=[FIND :query IN ALL FIELDS RETURNING Product2 (id, name,Family, Name_Code__c  where Is_Registerable__c ='yes')];
	        
	        //[Select p.Name_Code__c, p.Name, p.Is_Registerable__c, p.Id, p.Family From Product2 p where p.Is_Registerable__c ='yes']
	        products=searchResults[0];
    	}
	    return null;
    }
}