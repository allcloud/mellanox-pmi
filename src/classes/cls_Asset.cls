public class cls_Asset {
	
	// This method will go over all the Asset's associated Contract's Assets and will update the Contract's field ERI_Approved__c accordinally
    public void updateAssetsRelatedContractERIApproved(list<Asset2__c> newAssets_List) {
    	
    	set<String> relatedPartNumbersNames_Set = new set<String>();
    	list<Asset2__c> assets2Update_List = new list<Asset2__c>();
    	
    	
    	for (Asset2__c newAsset : newAssets_List) {
    		
    		if (newAsset.Part_Number__c != null && newAsset.ERI_Approve__c && newAsset.Asset_Family__c != 'HCA' && newAsset.Asset_Family__c != 'CABLES') {
    			
    			newAsset.Not_In_ERI_List__c = true;
    			relatedPartNumbersNames_Set.add(newAsset.Part_Number__c);
    		}
    	}
    	
    	/*
    	if (!relatedPartNumbersNames_Set.isEmpty()) {
    		
    		list<Quote_Support_OPN__c> relatedPartNumbersNames_List = [SELECT Id, Product_OPN__c FROM Quote_Support_OPN__c WHERE Product_OPN__c IN : relatedPartNumbersNames_Set];
    	
    		if (!relatedPartNumbersNames_List.isEmpty()) {
    			
    			for (Asset2__c newAsset : newAssets_List) {
    				
    				for (Quote_Support_OPN__c relatedOPN : relatedPartNumbersNames_List) {
    					
    					if(relatedOPN.Product_OPN__c == newAsset.Part_Number__c) {
    						
    						newAsset.Not_In_ERI_List__c = false;
    					}
    				}
    			}
    		}
    	}
    	
    	*/
    }
}