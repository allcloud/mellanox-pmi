public with sharing class VF_MDF_Claim {
    
    public SFDC_MDF_Claim__c theMDF {get;set;}
    public Attachment parentMDFRequest {get;set;}
    public Attachment attachment1 {get;set;}
    public Attachment invoiceAttachment {get;set;}
    private User currentUser;
    public boolean displayEditMode {get;set;}
    public boolean displayEditButton {get;set;}
    public boolean displayEditSection {get;set;}
    private Id mdfId;
    private Id parentMDFRequestId;
    private Id mfdClaimId;
    public String poNumber {get;set;}
    //Picklist of tnteger values to hold file count
    public List<SelectOption> filesCountList {get; set;}
    //Selected count
    public String FileCount {get; set;}
    public List<Attachment> allFileList {get; set;}
     
    // Constructor where the rendering of the page is determines
    public VF_MDF_Claim(ApexPages.StandardController controller) {
        
         parentMDFRequestId = Apexpages.currentPage().getParameters().get('parentId');
        mfdClaimId = Apexpages.currentPage().getParameters().get('claimId');
        system.debug('mfdClaimId constructor: ' + mfdClaimId);
        mdfId = controller.getId();
        displayEditMode = true;
        Id currentUserId = UserInfo.getUserId();
        invoiceAttachment = new Attachment();
        
        currentUser = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id =: currentUserId];
        
        if (mdfId != null) {
            
            displayEditMode = false;
            
            theMDF = [Select s.Status__c,   s.Fund_Request__r.PO_Number__c,  PONumber__c ,
            s.Payment_Method__c,   s.PO_Number__c,  s.Units_Sold__c, s.Leads__c,  
            s.Name, s.Invoice_Number_to_Mellanox__c, s.Invoice_Date__c,
            s.Id, s.Fund_Request__c, s.CreatedDate, s.CreatedById, s.Claim_Paid_Date__c,
            s.Claim_ID__c, s.Budget__c, s.Approved__c, s.Approved_Date__c, s.Amount__c,
            s.Age_Days__c, s.Activity_Result__c, s.Activity_Result_Description__c, s.Account__c, s.Attendees__c
            From SFDC_MDF_Claim__c s WHERE Id =: mdfId];
            
            system.debug('theMDF Fund_Request__r.PO_Number__c : ' + theMDF.Fund_Request__r.PO_Number__c);
            
            if (theMDF.Status__c != 'Approved' && theMDF.Status__c != 'Rejected' && theMDF.Status__c != 'Paid') {
                
                displayEditButton = true;
            }
            
            system.debug('theMDF : ' + theMDF);
        }
        
        else {
            displayEditSection = false;
            filesCountList = new List<SelectOption>() ;
            FileCount = '' ;
            allFileList = new List<Attachment>() ;
            
            
            //Adding values count list - you can change this according to your need
            for(Integer i = 1 ; i < 11 ; i++)
            filesCountList.add(new SelectOption(''+i , ''+i)) ;
            
            theMDF = new SFDC_MDF_Claim__c();
            attachment1 = new Attachment();
            
            if (parentMDFRequestId != null) {
                
               poNumber = [SELECT Id, PO_Number__c FROM SFDC_MDF__c WHERE Id =: parentMDFRequestId].PO_Number__c;
            }
            theMDF.Account__c = (currentUser.ContactId != null && currentUser.Contact.AccountId != null) ? currentUser.Contact.AccountId : '001R000000t4EeP';
        }
    }
    
    // This method initiates upon loading/refreshing the 'VF_MDF_Request' page. it will look for existing 'Attachment_Holder__c' if exists and if it has attachments
    // they will be deleted, if it doesn't exist, it will create new 'Attachment_Holder__c' with the current user's Id as it's Name
    public void createAttachmentHolder() {
        
        list<Attachment_Holder__c> holder_List = [SELECT ID, Invoice_ID__c, (SELECT Id FROM Attachments) FROM Attachment_Holder__c WHERE Name =: currentUser.Id];
        
        Attachment_Holder__c holder;
        
        if (holder_List.isEmpty()) {
            
            holder = new Attachment_Holder__c();
            holder.Name = String.valueOf(currentUser.Id);
            insert holder;
        }
        
        else {
            
            
            holder = holder_List[0];
            if (!holder.Attachments.isEmpty()) {
                
                delete holder.Attachments;
            }
            
            holder.Invoice_ID__c = null;
            holder.Attachment_Uploaded__c = false;
            update holder;
        }
    }
    
    // This method will save the MDF Record and will set it's status 'Submitted'
    public pageReference submitForApproval() {
        
        PageReference nextPage = saveTheMDF(true, true);
        return nextPage;
    }
    
    // This method will save the MDF Record and will set it's status 'Submitted' without requesting Attachment to be added
    public pageReference submitForApprovalNoattNedded() {
        
        PageReference nextPage = saveTheMDF(false, false);
        return nextPage;
    }
    
    // This method will save the MDF Record and will set it's status 'Draft'
    public pageReference saveAsDraft() {
        
        PageReference nextPage = saveTheMDF(false, true);
        return nextPage;
    }
    
    // This method will save the new MDF and will assign it all the uploaded ataachments
    public pageReference saveTheMDF(boolean submitForApproval, boolean attNedded) {
        
        
        list<Attachment_Holder__c> holder_List = [SELECT ID, Invoice_ID__c, Attachment_Uploaded__c, (SELECT Id FROM Attachments) FROM Attachment_Holder__c WHERE Name =: currentUser.Id
        										  order by createdDate desc];
        
        system.debug('holder_List : ' + holder_List);
        
        if (attNedded) {
        	        	
	        if (!holder_List.isEmpty() && !holder_List[0].Attachments.isEmpty() && holder_List[0].Invoice_ID__c != null && holder_List[0].Attachment_Uploaded__c) {
	            
	            system.debug('theMDF.Invoice_Date__c : ' + theMDF.Invoice_Date__c);
	            system.debug('theMDF.Amount__c : ' + theMDF.Amount__c);
	            system.debug('theMDF.Invoice_Number_to_Mellanox__c : ' + theMDF.Invoice_Number_to_Mellanox__c);
	            system.debug('theMDF.Activity_Result_Description__c : ' + theMDF.Activity_Result_Description__c);
	            system.debug('theMDF.Activity_Result__c : ' + theMDF.Activity_Result__c);
 	            
	            if (theMDF.Invoice_Date__c != null && theMDF.Amount__c != null
	            && theMDF.Invoice_Number_to_Mellanox__c != null && theMDF.Activity_Result_Description__c != null && theMDF.Activity_Result__c != null) {
	                
	                if (theMDF.Account__c == null) {
	                    
	                    theMDF.Account__c ='001R000000t4EeP';
	                }
	                
	                try {
	                   
	                   if (parentMDFRequestId != null) { 
		                    theMDF.Status__c = (submitForApproval) ? 'Submitted' : 'Draft';
		                    theMDF.Fund_Request__c = parentMDFRequestId;
		                    theMDF.Invoice_ID__c = holder_List[0].Invoice_ID__c.SubString(0, 14);
		                    theMDF.PO_Number__c = theMDF.Fund_Request__r.PO_Number__c;
		                    insert theMDF;
	                   }
	                   
	                   else {
	                   	
	                   		ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'The Claim has no Fund Request associated to it'));
	                   		return null;
	                   }
	                }
	                
	                catch(Exception e) {
	                    
	                    String message = e.getMessage();
	                    //String specificMessage = message.substringAfter('EXCEPTION,');
	                    //specificMessage = specificMessage.substring(0, specificMessage.length() -2);
	                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, message));
	                    return null;
	                }
	                
	                Id InvoiceID =  holder_List[0].Invoice_ID__c;
	                Id AttHolderId = holder_List[0].Id;
	                system.debug('theMDF.Id : ' + theMDF.Id);
	                system.debug('AttHolderId : ' + AttHolderId);
	                transferAttachmentsToMDFAsync(theMDF.Id, AttHolderId, InvoiceID);
	                theMDF = new SFDC_MDF_Claim__c();
	                
	                system.debug('Inside 129');
	                String openerUrl = '/apex/VF_MDF_Claim?submitted=true';
	                PageReference nextPage = new PageReference(openerUrl);
	                
	                nextPage.setRedirect(true);
	                return nextPage;
	                
	            }
	            
	            else {
	                
	                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'All mandatory fields need to be filled up'));
	            }
	        }
	        
	        else if (!holder_List.isEmpty() && holder_List[0].Invoice_ID__c == null) {
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please upload your invoice'));
	        }
	        
	        else {
	            
	            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'At least 1 Attachments needs to be uploaded'));
	        }
        }
        
        else {
        	
        	theMDF.Status__c = 'Submitted';
        	update theMDF;
        	String openerUrl = '/apex/VF_MDF_Claim?submitted=true';
            PageReference nextPage = new PageReference(openerUrl);
            
            nextPage.setRedirect(true);
            return nextPage;
        }
        
        return null;
    }
    
    // This method will save all uploaded Attachments under the Attachment Holder
    public Pagereference SaveAttachments() {
        
         list<Attachment_Holder__c> holder_List = [SELECT ID, Invoice_ID__c, Attachment_Uploaded__c FROM Attachment_Holder__c WHERE Name like : currentUser.Id + '%'];
        
        Attachment_Holder__c holder;
        if (!holder_List.isEmpty()) {
        	
        	holder = holder_List[0];
        }
         String holderId = holder.Id;
        if(holderId == null || holderId == '')
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'No record is associated. Please pass record Id in parameter.'));
        if(FileCount == null || FileCount == '')
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select how many files you want to upload.'));
         List<Attachment> listToInsert = new List<Attachment>() ;
        
        //Attachment a = new Attachment(parentId = accid, name=myfile.name, body = myfile.body);
        for(Attachment a: allFileList)
        {
            if(a.name != '' && a.name != '' && a.body != null)
            listToInsert.add(new Attachment(parentId = holderId, name = a.name, body = a.body)) ;
        }
         //Inserting attachments
        Attachment_Holder__c holderClone = new Attachment_Holder__c(Id = holder.Id);
        
        holderClone.Attachment_Uploaded__c = true;
        update holderClone;
        if(listToInsert.size() > 0)
        {	
        	
            insert listToInsert ;
              
            String openerUrl = '/apex/VF_Contract_Renewal_PO_success_page';
            PageReference nextPage = new PageReference(openerUrl);
            
            nextPage.setRedirect(true);
            return nextPage;
        }
        else
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select at-least one file'));
        
        return null;
    }
    
    // This method will save the uploaded Invoice attachment and will update the Attachment holder field 'Invoice_ID__c' with the record Id
    // as an indication that this is the invoice 
    public Pagereference SaveInvoiceAttachment() {
       
        mfdClaimId = Apexpages.currentPage().getParameters().get('claimId');
         if (mfdClaimId != null && invoiceAttachment.Name != null && invoiceAttachment.Body != null) {
        
             
                invoiceAttachment.ParentId = mfdClaimId;
                insert invoiceAttachment ;
                
                String openerUrl = '/apex/VF_Contract_Renewal_PO_success_page';
                PageReference nextPage = new PageReference(openerUrl);
                SFDC_MDF_Claim__c theMdf = new SFDC_MDF_Claim__c(Id = mfdClaimId);
                theMdf.Invoice_ID__c = invoiceAttachment.Id;
                theMdf.Invoice_ID__c = theMdf.Invoice_ID__c.subString(0, 15);
                theMDF.PO_Number__c = theMDF.Fund_Request__r.PO_Number__c;
                update theMdf;
                nextPage.setRedirect(true);
                return nextPage;
             
        }
        
        else if (invoiceAttachment.Name != null && invoiceAttachment.Body != null) {
           
            Attachment_Holder__c holder = [SELECT ID, Invoice_ID__c FROM Attachment_Holder__c WHERE Name =: currentUser.Id];
            String holderId = holder.Id;
            
            if(holderId == null || holderId == '')
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'No record is associated. Please pass record Id in parameter.'));
            
            invoiceAttachment.ParentId = holderId;
            insert invoiceAttachment ;
            
            String openerUrl = '/apex/VF_Contract_Renewal_PO_success_page';
            PageReference nextPage = new PageReference(openerUrl);
            holder.Invoice_ID__c = invoiceAttachment.Id;
            update holder;
            nextPage.setRedirect(true);
            return nextPage;
        }
        
        else {
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select at-least one file'));
            return null;
        }
    }
    
    
    
    // This method will add/substract places for uploading attachments on the Attachments upload page
    public void ChangeCount()
    {
        allFileList.clear() ;
        //Adding multiple attachments instance
        for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
        allFileList.add(new Attachment()) ;
        
    }
    
    // This method will Asynchronically tranfer all uploaded attachments to the new 'Fund Request' and will delete the Attachment holder record
    @future
    private static void transferAttachmentsToMDFAsync(Id theMDFId, Id AttHolderId, Id invoiceId) {
        
        list<Attachment> attachments_List = [SELECT Id, Body, Name FROM Attachment WHERE ParentId =: AttHolderId];
        
        String invoiceName;
        list<Attachment> attachments2Create_List = new list<Attachment>();
        
        system.debug('attachments_List : ' + attachments_List);
        
        if (!attachments_List.isEmpty()) {
            
            for (Attachment att : attachments_List) {
                
                if (att.Id == invoiceId) {
                    
                    invoiceName = att.Name;
                }
                Attachment clonedAtt = att.clone();
                clonedAtt.ParentId = theMDFId;
                attachments2Create_List.add(clonedAtt);
            }
            
            insert attachments2Create_List;
            
            SFDC_MDF_Claim__c claim;
            for (Attachment att : attachments2Create_List) {
                
                if (att.Name == invoiceName) {
                    
                    claim = new SFDC_MDF_Claim__c (Id = theMDFId);
                    claim.Invoice_ID__c = att.Id;
                    claim.Invoice_ID__c = claim.Invoice_ID__c.SubString(0, 15);
                }
            }
            
            update claim;
            //Attachment_Holder__c attHolder2Delete = new Attachment_Holder__c(Id = AttHolderId);
            //delete attHolder2Delete;
        }
    }
    
    // This method will redirect the user to the Editable section in the page where he can edit the MDF record
    public void turnExitingRecordEditable() {
        
        
        theMDF.Id = mdfId;
        system.debug('theMDF : ' + theMDF);
        displayEditSection = true;
    }
    
    // This method will redirect the user back to the Display section in the page after saving his changes Or display an error message
    // if a validation rule has blocked the upldate
    public void turnExitingRecordNonEditable() {
        
        system.debug('theMDF : ' + theMDF);
        
        if (theMDF.Activity_Result__c != null && theMDF.Invoice_Date__c != null && theMDF.Amount__c != null
        && theMDF.Invoice_Number_to_Mellanox__c != null && theMDF.Activity_Result_Description__c != null) {
            
            try {
                
                update theMDF;
            }
            
            catch(Exception e) {
                
                String message = e.getMessage();
                String specificMessage = message.substringAfter('EXCEPTION,');
                specificMessage = specificMessage.substring(0, specificMessage.length() -2);
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, specificMessage));
                return;
            }
            displayEditSection = false;
        }
        
        else {
            
            String specificMessage = 'All Mandatory fields should be filled up';
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, specificMessage));
        }
    }
    
    // This method will redirect the user back to the Display section in the page without any changes
    public void cancelRequest() {
        
        theMDF = [Select s.SystemModstamp, s.Status__c,  s.Units_Sold__c, 
        s.Payment_Method__c,  s.PO_Number__c, s.Attendees__c,
         s.Name, s.Invoice_Number_to_Mellanox__c, s.Invoice_Date__c,
        s.Id, s.Fund_Request__c,  s.CreatedDate, s.CreatedById, s.Claim_Paid_Date__c,
        s.Claim_ID__c,  s.Budget__c, s.Approved__c, s.Approved_Date__c, s.Amount__c,
        s.Age_Days__c, s.Activity_Result__c, s.Activity_Result_Description__c, s.Account__c ,s.Leads__c
        From SFDC_MDF_Claim__c s WHERE Id =: mdfId];
        
        displayEditSection = false;
    }
}