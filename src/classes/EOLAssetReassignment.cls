global class EOLAssetReassignment implements Database.Batchable<sObject>{

global string query;
global  integer isinsert;
global map<string, date> EOS = new map<string, date> ();

global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query );
       
            }
 


global void execute(Database.BatchableContext BC, List<sObject> scope){
List<Asset2__c> UpdAssets = new List<Asset2__c>();
                    
for(sObject s : scope)         
    { Asset2__c ast = (Asset2__c)s;
  
      if(isinsert == 1)
     { Ast.EOL_check__c = True;     
      if( EOS.containsKey(Ast.Part_Number__c)) Ast.EOS_date__c = EOS.get(Ast.Part_Number__c);
     }
     if(isinsert == 0)
      {Ast.EOL_check__c = False;}
       UpdAssets.add(ast);
   
   }
                                
update UpdAssets;
  
}         
        
global void finish(Database.BatchableContext BC){
/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.setToAddresses(new String[] {'innag@mellanox.co.il'});
mail.setReplyTo('batch@acme.com');
mail.setSenderDisplayName('Batch Processing');
mail.setSubject('Batch Process Completed');
mail.setPlainTextBody('Batch Process has completed moving EOLs');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

}
}