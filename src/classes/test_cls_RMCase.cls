@isTest
public with sharing class test_cls_RMCase {
	
	static testMethod void test_createFMDiscussionOnRmCaseIsClosed() {
		
		CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        Case cs = obj.Create_case(acc, con);
        insert cs;
        
        RMCase__c newFRCase = new RMCase__c();
        newFRCase.sfcase__c = cs.Id;
        newFRCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: 'RM_case'].Id;
        insert newFRCase;
     
        newFRCase.Is_Closed__c = true;
        
        update newFRCase;
      
	}
}