public class clsCase 
{
    
    public static String updateLatestCaseComment(List <ID> lst_lastModifiedBy, Case c, List <CaseComment> lst_caseComment)
    {
        String str_LastModifiedBy;
        String str_createdBy= 'Created by: ';
        Map <ID, User> map_idToUser = new Map <ID, User>();
        Map <ID, SelfServiceUser> map_idToSelfServiseUser = new Map <ID, SelfServiceUser>();
        /*for(SelfServiceUser ssu:[select Id,name from SelfServiceUser where Id in :lst_lastModifiedBy])
        {
            map_idToSelfServiseUser.put(ssu.id,ssu);
        }
      
        for(User user:[Select Id, name FROM User Where id in :lst_lastModifiedBy])
        {
            map_idToUser.put(user.id,user);
        }*/
        Integer i = 0;
        String allCommentsCurr = '';
        String allCommentsPrev = allCommentsCurr; 
        for(CaseComment cc: lst_caseComment)
        {
            if(cc.IsPublished)
            {
                i++;
                if(i==11)
                {
                    break;
                }
                /*if(map_idToUser.containsKey(cc.LastModifiedById))   
                {    
                    if(map_idToUser.get(cc.LastModifiedById).Name=='Evgeny Kagan' ||map_idToUser.get(cc.LastModifiedById).Name=='INNA GOLDIN')
                    { //Inserted by external user which is not a contact
                        str_LastModifiedBy ='';
                        str_createdBy = ''; 
                    } 
                    else
                    { 
                        str_LastModifiedBy =map_idToUser.get(cc.LastModifiedById).Name;
                    } 
                           
                }         
                if(str_LastModifiedBy ==null)  // comment was added by Self Servise User         
                {        
                    if(map_idToSelfServiseUser.containsKey(cc.LastModifiedById))
                    { 
                        str_LastModifiedBy = map_idToSelfServiseUser.get(cc.LastModifiedById).name; 
                    }
                }
                allCommentsPrev = allCommentsCurr;
                allCommentsCurr = allCommentsCurr+'\n' + '-------------------------------------------------' + '\n '+ 
                + str_createdBy + str_LastModifiedBy+ ' ' + cc.CreatedDate+ '\n' + cc.CommentBody + '\n' ;
                if(allCommentsCurr.length()>32000)
                {
                    return allCommentsPrev;
                } */
                
                allCommentsPrev = allCommentsCurr;
                if(cc.LastModifiedBy.Name=='Evgeny Kagan' ||cc.LastModifiedBy.Name=='INNA GOLDIN')
                {
                    allCommentsCurr = allCommentsCurr+'\n' + '-------------------------------------------------' + '\n '+ 
                    cc.CreatedDate + '\n' + cc.CommentBody + '\n' ;
                    if(allCommentsCurr.length()>32000)
                    {
                        return allCommentsPrev;
                    } 
                }
                else
                {
                    allCommentsCurr = allCommentsCurr+'\n' + '-------------------------------------------------' + '\n '+  
                    + ' ' + str_createdBy + cc.LastModifiedBy.Name+' '+cc.CreatedDate+ '\n'+ cc.CommentBody + '\n' ;
                    if(allCommentsCurr.length()>32000)
                    {
                        return allCommentsPrev;
                    } 
                }
            }
            if(allCommentsCurr.length()>32000)
            {
                return allCommentsPrev;
            } 
        }
        return allCommentsCurr;
    }    
    /*
    // this method will create a new FM Discussion when the Case's Resolution is polupated
    public void createNewFMDiscussionWhenCaseReolutionIsPopulated(list<Case> newCase_List, map<Id, Case> oldCase_Map) {
        
        list<FM_Discussion__c> discussionsToCreate_List = new list<FM_Discussion__c>();
        
        Id userId = system.UserInfo.getUserId();
                
        User currentUser = [SELECT Id, Name FROM User Where Id =: userId];
        
        for (Case newCase : newCase_List) {
            
            system.debug('newCase.Resolution_Description__c : ' + newCase.Resolution_Description__c);
            system.debug('oldCase_Map.get(newCase.Id).Resolution_Description__c : ' + oldCase_Map.get(newCase.Id).Resolution_Description__c);
            system.debug('newCase.Status : ' + newCase.Status);
            system.debug('oldCase_Map.get(newCase.Id).Status : ' + oldCase_Map.get(newCase.Id).Status);
            
            if (newCase.Resolution_Description__c != null && newCase.Resolution_Description__c != oldCase_Map.get(newCase.Id).Resolution_Description__c) {
                
                FM_Discussion__c newDiscussion = new FM_Discussion__c();
                newDiscussion.Public__c = false;
                newDiscussion.Email_CreatedBy__c = currentUser.Name;
                newDiscussion.Discussion__c = 'The case was closed with following resolution: ' +  newCase.Resolution_Description__c;
                newDiscussion.Case__c = newCase.Id;
                discussionsToCreate_List.add(newDiscussion);
                
            }
        }
        
        if (!discussionsToCreate_List.isEmpty()) {
            
            try {
                
                insert discussionsToCreate_List;
            }
            
            catch(Exception e) {
                
                system.debug('ERROR Inserting Record : ' + e.getMessage());
            }
        }
    }
    */
    
    // This method will add a new team member(the Case's AE assignee) under the Case's associated Project
    public void addTeamMemberToCaseRelatedProject(list<Case> newCases_List, Map<Id, Case> oldCases_Map) {   
        
        list<Project_Member__c> projectMembers2Create_List = new list<Project_Member__c>();
        set<Id> relatedProjectIds_Set = new set<Id>();
        set<Case> changedCases_Set = new set<Case>();
        
        for (Case newCase : newCases_List) {
        
            if (newCase.AE_Engineer__c != null && newCase.AE_Engineer__c != oldCases_Map.get(newCase.Id).AE_Engineer__c) {
                
                relatedProjectIds_Set.add(newCase.Related_Project__c);
                changedCases_Set.add(newCase);
            }
        }
        
        id recordTypePOCId = [SELECT Id FROM RecordType WHERE developerName =: 'Project_POC'].Id;

        map<Id, Milestone1_Project__c> relatedProjects_Map;
        if (!relatedProjectIds_Set.isEmpty()) { 
        
            relatedProjects_Map = new map<Id, Milestone1_Project__c>([SELECT Id, RecordTypeId FROM Milestone1_Project__c
                                                                                                  WHERE Id IN : relatedProjectIds_Set]);
        }
        
        if (!changedCases_Set.isEmpty() && relatedProjects_Map != null && !relatedProjects_Map.values().isEmpty()) {
        
            for (Case newCase : changedCases_Set) {
                
                if (relatedProjects_Map.containsKey(newCase.Related_Project__c) && recordTypePOCId != null) {
                
                    if (relatedProjects_Map.get(newCase.Related_Project__c).RecordTypeId == recordTypePOCId) {
                        
                        system.debug('newCase : ' + newCase);
                        Project_Member__c newMember = new Project_Member__c();
                        newMember.Project__c = newCase.Related_Project__c;
                        newMember.Project_Member__c = newCase.Assignee__c;
                        projectMembers2Create_List.add(newMember);
                    }
                }
            }
        }
        
        if (!projectMembers2Create_List.isEmpty()) {
            
            insert projectMembers2Create_List;
        }
    }
    
    // This method will create a new Case Comment under the new Case being created and assign it's associated Project's fields
    // to the newly created Case Comments Description field, only if the Case's associated Project is of Record Type : POC 
    public void createCaseCommentForCaseRelatedToPOCProject(list<Case> newCases_List) {
                
        map<Id, Milestone1_Project__c> CasesRelatedProjects_Map = fillUpCaseRelatedProjectsMap(newCases_List);
        
        if (CasesRelatedProjects_Map != null && !CasesRelatedProjects_Map.values().isEmpty()) {  
            
            list<CaseComment> comment2Create_List = new list<CaseComment>();
            for (Case newCase : newCases_List) {
                
                if (newCase.Related_Project__c != null && CasesRelatedProjects_Map.containsKey(newCase.Related_Project__c)) {
                    
                    Milestone1_Project__c relatedProject = CasesRelatedProjects_Map.get(newCase.Related_Project__c);
                    
                    CaseComment newComment = new CaseComment();
                    newComment.ParentId = newCase.Id;
                    newComment.CommentBody = buildTheCaseCommentDescription(relatedProject); 
                    comment2Create_List.add(newComment);
                }
            }
            
            if (!comment2Create_List.isEmpty()) {
                
                insert comment2Create_List;
            }
        }
    }
    
    // This method will build the Comment Body from the Project's fields values and will return it to the 'createCaseCommentForCaseRelatedToPOCProject' metohd
    private String buildTheCaseCommentDescription(Milestone1_Project__c relatedProject) {
        
        String commentDescription = 'Related POC Project Details : \n';
        commentDescription += 'Name : ' + relatedProject.Name + '\n';
        commentDescription += 'Account : ' + relatedProject.c_Account__r.Name + '\n';
        commentDescription += 'Description : ' + relatedProject.Description__c + '\n';
        commentDescription +=  'Success Criteria : ' + relatedProject.Success_Criteria__c + '\n';
        commentDescription +=  'Owner : ' + relatedProject.Owner_Name__c + '\n';
        
        if (relatedProject.Deadline__c != null) {
            
            try {
                string datetimestr =    relatedProject.Deadline__c.format();  
                commentDescription += 'Due Date : ' + datetimestr + '\n';
            }
            
            catch (Exception e) {
                
                System.debug('The Project Due Date is not in a correct Date format, please fix and try again');
            }
        }
        
        return commentDescription;
    }
    
    // This method will get the Cases associated Projects if their Record Type is POC and in the event where each of the Cases has a Project associated to it
    // and return it to the 'createCaseCommentForCaseRelatedToPOCProject' metohd
    private map<Id, Milestone1_Project__c> fillUpCaseRelatedProjectsMap(list<Case> newCases_List) {
        
        Id recordTypePOCId = [SELECT Id FROM RecordType WHERE Name =: 'POC' and SobjectType =: 'Milestone1_Project__c'].Id;
        map<Id, Milestone1_Project__c> CasesRelatedProjects_Map;
        
        set<Id> relatedProjectsIds_Set = new set<Id>();
        
        for (Case newCase : newCases_List) {
            
            if (newCase.Related_Project__c != null) {
                
                relatedProjectsIds_Set.add(newCase.Related_Project__c);
            }
        }
        
        if (!relatedProjectsIds_Set.isEmpty()) {
            
            CasesRelatedProjects_Map = new map<Id, Milestone1_Project__c>([SELECT Id, Name, c_Account__r.Name, 
                                                                                                          Description__c, Success_Criteria__c, Owner_Name__c, Deadline__c
                                                                                                          FROM Milestone1_Project__c WHERE Id IN : relatedProjectsIds_Set
                                                                                                          and RecordTypeId =: recordTypePOCId]);
        }
        
        return CasesRelatedProjects_Map;
    }
    
    // This method updates the Case created from Live Agent with the relevant Contact
    public void updateLiveAgentCasesWithContacts(list<Case> NewCases_List) {
        
        Id liveAgentRecordType = ENV.LiveAgentCaseRecordType;
        
        set<String> contactsEmails_Set = new set<String>();
        
        set<Id> mellanox_Group_Set = new set<Id>();
        
        for (Case newCase : NewCases_List) {
            
            mellanox_Group_Set.add(newCase.Escalation_Q_Owner__c);
            
            if (newCase.Live_agent_Contact_Email__c != null) {  
                
                contactsEmails_Set.add(newCase.Live_agent_Contact_Email__c);
            }
            
            
            map<String, Id> emails2ContactsIds_Map = fillUpContactsIdsByEmail(contactsEmails_Set);
            
            if (!emails2ContactsIds_Map.values().IsEmpty()) {
                
                for (Case currentCase : NewCases_List) {
                    
                    if (currentCase.RecordTypeId == liveAgentRecordType) {
                        
                        if (emails2ContactsIds_Map.containsKey(currentCase.Live_agent_Contact_Email__c)) {  
                        
                            currentCase.ContactId = emails2ContactsIds_Map.get(currentCase.Live_agent_Contact_Email__c);
                        }
                    }
                }
            }
        }
        
        updateMellanoxGroupANDEscalationQueue(mellanox_Group_Set, NewCases_List);
    }
    
    // This method updates the Optional_Escalation_Queue_Owner_Lookup__c and Mellanox_Group__c fields according to user values
    private void updateMellanoxGroupANDEscalationQueue(set<Id> mellanox_Group_Set, list<Case> NewCases_List){
        
        map<Id, User> users_Map = new Map<Id,User>([Select id, Name, Mellanox_Group__c, Manager_Email__c  From User where id in: mellanox_Group_Set]);
        map<String, Id> optionalAEOwners_Map = new map<String, Id>();
        list <BusinessHours> businessHoursUS = [SELECT Id, Name FROM BusinessHours WHERE Name =: 'Default'];
        list <BusinessHours> businessHoursIL = [SELECT Id, Name FROM BusinessHours WHERE Name =: 'IL'];
                
        Decimal escalatedAETimeSLA_Label =  Label.Escalated_AE_Time_SLA != null ? Decimal.valueOf(Label.Escalated_AE_Time_SLA) : 0;
        Integer time2add =  Integer.valueOf(escalatedAETimeSLA_Label * 3600000);
        
        for (User user : users_Map.values()) {
            optionalAEOwners_Map.put(user.Name, user.id);
        }
        
        
        for (Case newCase : NewCases_List) {
            if(users_Map.containsKey(newCase.Escalation_Q_Owner__c)){
                
                if(users_Map.get(newCase.Escalation_Q_Owner__c).Mellanox_Group__c != NULL){
                    
                    newCase.Mellanox_Group__c = users_Map.get(newCase.Escalation_Q_Owner__c).Mellanox_Group__c;
                    
                    string mellanoxGroup = newCase.Mellanox_Group__c;
                    
                    Datetime escalatedAESLATime = Datetime.now();
    
                    if(mellanoxGroup.contains('IL') && !businessHoursIL.isEmpty() ){
                        
                        escalatedAESLATime = BusinessHours.add(businessHoursIL[0].id, escalatedAESLATime, time2add);
                        
                        newCase.Escalated_AE_SLA_Breach_Time__c = escalatedAESLATime;
                        
                        system.debug('Escalated_AE_SLA_Breach_Time__c ' + newCase.Escalated_AE_SLA_Breach_Time__c);
                    }
                    else if(mellanoxGroup.contains('US') && !businessHoursUS.isEmpty()){
                        
                        escalatedAESLATime = BusinessHours.addGmt(businessHoursUS[0].id, escalatedAESLATime, time2add);
                        
                        newCase.Escalated_AE_SLA_Breach_Time__c = escalatedAESLATime;
                        
                        system.debug('Escalated_AE_SLA_Breach_Time__c ' + newCase.Escalated_AE_SLA_Breach_Time__c);
                    }
                }
                if(users_Map.get(newCase.Escalation_Q_Owner__c).Manager_Email__c != NULL){
                    
                    newCase.Escalation_Q_Owner_Manager_Email__c = users_Map.get(newCase.Escalation_Q_Owner__c).Manager_Email__c;    
                }
            }
        }
    }
    
    // This method returns a map of COntact Emails as Keys and Contact IDs As Values
    private map<String, Id> fillUpContactsIdsByEmail(set<String> contactsEmails_Set) {
        
         map<String, Id> emails2ContactsIds_Map = new map<String, Id>();
         
         if (!contactsEmails_Set.isEmpty()) {
            
            list<Contact> contacts_List = [SELECT Id ,Email FROM Contact WHERE Email IN : contactsEmails_Set];
            
            for (Contact con : contacts_List) {
                
                emails2ContactsIds_Map.put(con.Email, con.Id);
            }
         }
         
         return emails2ContactsIds_Map;
    }
    
    // This method will create a new FM Discussion For every Case that checked with Critical Bug
    // and will populate it's RM Cases fields with the existing Rm cases Ids
    public void caseCriticalBug(list<Case> newCases_List, Map<Id, Case> oldCases_Map) {   
        
        list<Case> casesFromDB_List = [SELECT Id, Account.Type, Business_impact__c, Critical_Bug__c, AE_PM_Escalated__c,
        (SELECT Id FROM Redmine_Case__r) FROM Case WHERE Id IN : oldCases_Map.KeySet()];
        
        list<FM_Discussion__c> fmDiscussions2Create_List = new list<FM_Discussion__c>();
        
        for (Case newCase : casesFromDB_List) {
            
            if (newCase.Business_impact__c != null && newCase.Business_impact__c.EqualsIgnoreCase('Critical')
                && newCase.Account.Type != null && newCase.Account.Type.EqualsIgnoreCase('OEM')
                && newCase.AE_PM_Escalated__c != null && newCase.AE_PM_Escalated__c.EqualsIgnoreCase('PM')) {
                    
                if (newCase.Critical_Bug__c && !oldCases_Map.get(newCase.Id).Critical_Bug__c) {
                    
                    FM_Discussion__c discussion = new FM_Discussion__c();
                    discussion.Case__c = newCase.Id;
                    discussion.Discussion__c = 'Please note this is a critical bug.';
                    fmDiscussions2Create_List.add(discussion);
                    
                    integer counter = 1;
                    for (RmCase__c rmCase : newCase.Redmine_Case__r) {
                        
                        if (counter == 1) {
                            
                            discussion.RM_Case1__c = rmCase.Id;
                        }
                        
                        else if (counter == 2) {
                            
                            discussion.RM_Case2__c = rmCase.Id;
                        }
                        
                        else if (counter == 3) {
                            
                            discussion.RM_Case3__c = rmCase.Id;
                        }
                        
                        else if (counter == 4) {
                            
                            discussion.RM_Case4__c = rmCase.Id;
                        }
                        
                        else {
                            
                            discussion.RM_Case5__c = rmCase.Id;
                        }
                        
                        counter ++;
                    }
                }
            }
        }
        
        try {
            
            insert fmDiscussions2Create_List;
        }
        
        catch(Exception e) {
            
            if (newCases_List.size() == 1) {
                
                newCases_List[0].addError('cannot create new FM Discussion : ' + e.getMessage());
            }
        }
    }
    
    //This method will update the Academy Live Agent Cases Subject upon creation
    public void updateAcademyCaseSubject(list<Case> newCases_List) {
        
        set<Id> contactIds_Set = new set<Id>();
        
        
        for (Case newCase : newCases_List) {
            
            if (newCase.Live_Agent_Case_Source__c != null && newCase.Live_Agent_Case_Source__c.EqualsIgnoreCase('Academy')
                && newCase.ContactId != null) {
                
                contactIds_Set.add(newCase.ContactId);
            }
        }
        
        if (!contactIds_Set.isEmpty()) {
            
            map<Id, Contact> contactsIds2Contacts_Map = new map<Id, Contact>([SELECT Id, FirstName, LastName FROM Contact WHERE Id IN : contactIds_Set]);
            
            if (!contactsIds2Contacts_Map.values().isEmpty()) {
                
                for (Case newCase : newCases_List) {
                    
                    if (newCase.ContactId != null && contactsIds2Contacts_Map.containsKey(newCase.ContactId)) {
                        
                        String firstName = (contactsIds2Contacts_Map.get(newCase.ContactId).FirstName != null)? contactsIds2Contacts_Map.get(newCase.ContactId).FirstName : '';
                        
                        newCase.Subject = 'Academy Request from ' + firstName + ' ' + contactsIds2Contacts_Map.get(newCase.ContactId).LastName;
                    }
                }
            }
        }
    }
    
    // This method will update the Case Previous Assignee with the the last Assingee when the Assignee__c field is updated with new Assignee
    // In addition, the method calculates the date after a few days when new FM_Discussions is created
    public void updateCasePreviousAssigneeWhenAssigneeChanged(list<Case> newCases_List, Map<Id, Case> oldCases_Map) {
        
        set<Id> mellanox_Group_Set = new set<Id>();
        Case oldCase;
        list<Profile> AEProfileID = [Select Id,Name from Profile where Name = 'AE Profile'];
        Integer numberOfDaysToTheLastFMDiscussions_Label =  Label.Number_of_days_to_the_last_FM_Discussions != null ? Integer.valueOf(Label.Number_of_days_to_the_last_FM_Discussions) : 0;
        list <BusinessHours> businessHoursFMDiscussions = [SELECT Id, Name FROM BusinessHours WHERE Name =: 'FM Discussions'];

        Datetime AECommentEndDate = Datetime.now();
        
        for(Case newCase : newCases_List) {
            
            mellanox_Group_Set.add(newCase.Escalation_Q_Owner__c);
            
            oldCase = oldCases_Map.get(newCase.Id);
            
            if (oldCase.Assignee__c != null && newCase.Assignee__c != oldCase.Assignee__c) {
                newCase.Prev_Assignee__c = oldCases_Map.get(newCase.Id).Assignee__c;
            }
            
            if(!businessHoursFMDiscussions.isEmpty()){
                if ( (newCase.State__c == 'Assigned AE' && newCase.State__c != oldCase.State__c) ||
                    (!AEProfileID.isEmpty() && UserInfo.getProfileId() == AEProfileID[0].id && newCase.FM_Discussion_Field__c != oldCase.FM_Discussion_Field__c) ){
                    
                    AECommentEndDate = BusinessHours.add(businessHoursFMDiscussions[0].id, AECommentEndDate, numberOfDaysToTheLastFMDiscussions_Label * 24 * 3600000);
                    newCase.AE_Comment_end_date__c = AECommentEndDate;
                    system.debug('AE Comment end date: ' + newCase.AE_Comment_end_date__c);
                    
                }
            }
        }
        
        updateMellanoxGroupANDEscalationQueue(mellanox_Group_Set, NewCases_List);
    }
    
    // This method will add Case Team Members to the updated Case, every time the Case becomes fatal (Set Automatic priority to FATAL 911 on create)
    public void addCaseTeamMembersOnCaseUpdateToFatal(list<Case> newCases_List, Map<Id, Case> oldCases_Map) {
        
        Id theCaseTeamRoleId  = [SELECT Id FROM CaseTeamRole WHERE Name =: 'Follow Up'].Id;
        
        list<User> users2Assign_List = [SELECT Id FROM User WHERE Name =: 'Ofer Muki' OR Name =: 'Ron Assulin'];
        
        list<CaseTeamMember> caseTeamMebers2Create_List = new list<CaseTeamMember>();
        
        for (Case newCase : newCases_List) {
            
            if(newCase.ISFATAL__c && !oldCases_Map.get(newCase.Id).ISFATAL__c
               && newCase.Priority != null && (newCase.Priority.EqualsIgnoreCase('Fatal'))) {
                
                for (User currentUser : users2Assign_List) {
                    
                    CaseTeamMember teamMember = new CaseTeamMember();
                    teamMember.TeamRoleId = theCaseTeamRoleId;
                    teamMember.ParentId = newCase.Id;
                    teamMember.MemberId = currentUser.Id;
                    caseTeamMebers2Create_List.add(teamMember);
                }
            }
        }
        
        if (!caseTeamMebers2Create_List.isEmpty()) {
            
            try {
                
                database.saveResult[] rs = database.insert(caseTeamMebers2Create_List, false);
            }
            
            catch(Exception e) {
                
                newCases_List[0].addError('Error in creating Case Team Members : ' + e.getMessage());
            }
        }
    }
    
    // This method will update the Case related Cases Priortity to 'Fatal' when a new Case is created or updated with priority fatal
    public void updateCaseRelatedCasesPriority(list<Case> newCases_List, map<Id, Case> oldCases_Map) {
        
        list<Case> Cases2Update_List = fillUpRelatedCasesList(newCases_List, oldCases_Map);
        list<Case> childCases2Update_List = new list<Case>();
        
        for (Case currentCase : Cases2Update_List) {
            
            for (Case childCase : currentCase.Cases__r) {
                
                childCase.Priority = 'Fatal';
                childCases2Update_List.add(childCase);
            }
        }
        
        if (!childCases2Update_List.isEmpty()) {
            
            try {   
                update childCases2Update_List;
            }
            
            catch(Exception e) {
                
                if (newCases_List.size() == 1) {
                    
                    newCases_List[0].addError(' Related Cases Could not be updated : ' + e.getMessage());
                }
            }
        }
    }
    
    private list<Case> fillUpRelatedCasesList(list<Case> newCases_List, map<Id, Case> oldCases_Map) {
        
        list<Case> Cases2Update_List = new list<Case>();
        set<Id> newCasesIds_Set = new set<Id>();
        
        for (Case newCase : newCases_List) {
            
            if (newCase.Priority != null && newCase.RecordTypeId == ENV.CaseTypeSupport) {
                
                if (oldCases_Map != null && newCase.Priority.EqualsIgnoreCase('Fatal') && !oldCases_Map.get(newCase.Id).Priority.EqualsIgnoreCase('Fatal')) {
                    
                    newCasesIds_Set.add(newCase.Id);
                } 
                
                else if (newCase.Priority.EqualsIgnoreCase('Fatal')) {
                    
                    newCasesIds_Set.add(newCase.Id);
                }
            }
        }
        
        if (!newCasesIds_Set.isEmpty()) {
            
            Cases2Update_List = [SELECT Id, (SELECT Id FROM Cases__r) FROM Case WHERE ID IN : newCasesIds_Set];
        }
        
        return Cases2Update_List;
    }
    
    // This method will update the new Case created priority to 'Fatal' if the New Case Parent Case's prioity is 'Fatal' as well
    public void updateNewCasesPriorityToFatalFromParentCases(list<Case> newCases_List) {
        
        set<Id> parentCasesIds_Set = new set<Id>();
        
        for (Case newCase : newCases_List) {
            
            if (newCase.RMA_Related_case__c != null) {
                
                parentCasesIds_Set.add(newCase.RMA_Related_case__c);
            }
        }
        
        if (!parentCasesIds_Set.isEmpty()) {
        
            map<Id, Case> ParentCasesIds2ParentCases_Map = new map<Id, Case>([SELECT Id FROM Case WHERE Priority = 'Fatal' 
            and RecordTypeId =: ENV.CaseTypeSupport and Id IN : parentCasesIds_Set]);
            
            if (!ParentCasesIds2ParentCases_Map.values().isEmpty()) {
                
                for (Case newCase : newCases_List) {
                    
                    if (ParentCasesIds2ParentCases_Map.containsKey(newCase.RMA_Related_case__c)) {
                        
                        newCase.priority = 'Fatal';
                    }
                }
            }
        }
    }
    
    // This method will update the Case field 'Owner_Text_Field__c' with the Case Owner Name
    public void populateCaseOwnerTextOnCaseOwnerChange(list<Case> newCases_List, map<Id, Case> oldCases_Map) {
        
        set<Id> casesIds_Set = new set<Id>();
        
        for (Case newCase : newCases_List) {
            
            if (newCase.Case_Type__c != null && newCase.State__c != 'Closed') {
                
                if (newCase.Case_Type__c.EqualsIgnoreCase('System Case')) {
                    
                    if (oldCases_Map == null) {
                        
                        casesIds_Set.add(newCase.Id);
                    }
                    
                    else if (oldCases_Map != null && newCase.OwnerId != oldCases_Map.get(newCase.Id).OwnerId) {
                        
                        casesIds_Set.add(newCase.Id);
                    }
                }
            }
        }
        
        if (!casesIds_Set.isEmpty()) {
            
            list<Case> cases2Update_List = [SELECT Id, Owner.Name FROM Case WHERE Id IN : casesIds_Set]; 
            
            for (Case case2Update : cases2Update_List) {
                
                case2Update.Owner_Text_Field__c = case2Update.Owner.Name;
            }
            
            update cases2Update_List;
        }
    }
    
    //This method will update the Academy Live Agent Cases related Contacts field 'Interested In Training'
    public void updateAcademyContactInterestedInTraining(list<Case> newCases_List) {
                
        Id generalAccademyContactId = '003R000000rX1eJ';
        list<Contact> contacts2Update_List = new list<Contact>();
        
        for (Case newCase : newCases_List) {
            
            if (newCase.Live_Agent_Case_Source__c != null && newCase.Live_Agent_Case_Source__c.EqualsIgnoreCase('Academy')
                && newCase.ContactId != null && newCase.ContactId != generalAccademyContactId) {
                
                Contact relatedContact = new Contact(Id = newCase.ContactId);
                relatedContact.Interested_in_training__c = true;
                contacts2Update_List.add(relatedContact);
            }
        }
        
        if (!contacts2Update_List.isEmpty()) {
            
            update contacts2Update_List;
        }
    }
    
    // This method adds the User - Peter Zhou as Case Team Member if it's not exists on the Case and the Case field Is_Kotura__c is checked true
    public void addCaseTemMemberWhenKoturaIsChecked(list<Case> newCases_List, map<Id, Case> oldCases_Map) {
        
        list<Case> cases2Update_List = getListOfCasesAndCaseTeamMembers(newCases_List, oldCases_Map);
        list<CaseTeamMember> teamMembers2Create_List = new list<CaseTeamMember>();
        
        for (Case currentCase : cases2Update_List) {
            
            if (currentCase.TeamMembers.isEmpty()) {
                
                CaseTeamMember member = new CaseTeamMember();
                member.ParentId = currentCase.Id;
                member.MemberId = '005500000035DG0';
                member.TeamRoleId = '0B75000000000mH';
                teamMembers2Create_List.add(member);
            }
        }
        
        if (!teamMembers2Create_List.isEmpty()) {
            
            insert teamMembers2Create_List;
        }
    }
    
    // This method returns a list of Cases needs to be update
    private list<Case> getListOfCasesAndCaseTeamMembers(list<Case> newCases_List, map<Id, Case> oldCases_Map) {
        
        set<Id> relatedCasesIds_Set = new set<Id>();
        list<Case> cases2Update_List = new list<Case>();
        
        
        for (Case newCase : newCases_List) {
            
            if (newCase.Is_Kotura__c) {
                
                if (oldCases_Map != null) {
                    
                    if (!oldCases_Map.get(newCase.Id).Is_Kotura__c) {
                        
                        relatedCasesIds_Set.add(newCase.Id);
                    }
                }
                
                else {
                    
                    relatedCasesIds_Set.add(newCase.Id);
                }
            }
        } 
        
        if (!relatedCasesIds_Set.isEmpty()) {
            
            cases2Update_List = [SELECT Id, (SELECT Id, MemberId, ParentId From TeamMembers WHERE MemberId = '005500000035DG0') FROM Case WHERE Id IN : relatedCasesIds_Set];
        }
        
        return cases2Update_List;
    }
    
    // This method will delete cases where the field 'Delete_This_Case__c' is chang
    public void deleteMCareCases(list<Case> newCases_List, map<Id, Case> oldCases_Map) {
        
        list<Case> cases2Delete_List = new list<Case>();
        
        for (Case newCase : newCases_List) {
            
            if (!newCase.Delete_This_Case__c && (newCase.Delete_This_Case__c != oldCases_Map.get(newCase.Id).Delete_This_Case__c)) {
                
                Case case2Delete = new Case(Id = newCase.Id);
                cases2Delete_List.add(case2Delete);
            }
        }
        
        if(!cases2Delete_List.isEmpty()) {
            
            delete cases2Delete_List;
        }
    }
    
    
    // This method will call the 'mlnxMobilePushHTTP.createHTTPRequestForPushNotif' method that will send push notifications 
    // every time a Case is created from Mobile device
    public void sendPushNotificationOnNewMobileCase(list<Case> newCases_list) {
        
        
        list<String> usersEmails_List = new list<String>();
        
        for (Case newCase : newCases_list) {
            
            if (newCase.origin != null && newCase.origin.EqualsIgnoreCase('Mobile')) {
                
                usersEmails_List.add(UserInfo.getUserName());
            }
        }
        
        list<String> messages_List = new list<String>{'Thank you for contacting Mellanox Support, your Case ' + newCases_list[0].CaseNumber + ', has been submitted'};
        
        if (!usersEmails_List.isEmpty()) {
            
            mlnxMobilePushHTTP.createHTTPRequestForPushNotif(usersEmails_List,messages_List);
        }
    }
    
    
    // This method will update the Cases related Contacts and Accounts if the Case is created via
    // Email to Case and it's origin is 'NPU' or 'MC'
    public void updateContactOnEmail2CaseEZChip(list<Case> newCases_List) {
        
        system.debug('inside 809');
        set<Id> relatedContactsIds_Set = new set<Id>();
        list<SObject> objects2Update_List = new list<SObject>();
        map<Id, Contact> contactIds2Contacts_Map = new map<Id, Contact>();
        
        for(Case newCase : newCases_List) {
            
            if (newCase.Origin != null && ( newCase.Origin.containsIgnoreCase('NPU') || newCase.Origin.containsIgnoreCase('MC') ) && newCase.ContactId != null) {
                
                if (newCase.CreatedDate > date.today().addDays(-2)) {
                    
                    Contact relatedCon = new Contact(Id = newCase.ContactId);
                    relatedCon.Contact_Type__c = 'Ezchip Design-In customer';
                    
                    relatedCon.Contact_Source__c = (newCase.Origin.containsIgnoreCase('NPU')) ? 'NPU' : 'MC';
                    objects2Update_List.add(relatedCon);
                    contactIds2Contacts_Map.put(relatedCon.Id, relatedCon);
                    
                }
            }
        }
        
        if (!relatedContactsIds_Set.isEmpty()) {
            for (Contact relatedCon : [SELECT Id, Contact_Type__c, AccountId, Account.CreatedDate FROM Contact WHERE Id IN : relatedContactsIds_Set
                                       and AccountId !=  null]) {
                
                if (relatedCon.Account.CreatedDate > date.today().addDays(-2)) {
                    
                    Account relatedAcc = new Account(Id = relatedCon.AccountId);
                    relatedAcc.AccountSource = (contactIds2Contacts_Map.get(relatedCon.Id).Contact_Source__c.EqualsIgnoreCase('NPU')) ? 'NPU' : 'MC';
                    relatedAcc.Support_Center__c = (relatedAcc.AccountSource.EqualsIgnoreCase('NPU')) ? 'NPU' : 'MC';
                    objects2Update_List.add(relatedAcc);
                }
            }
        }
        
        if (!objects2Update_List.isEmpty()) {
            
            try {
                
                update objects2Update_List;
            }
            
            catch(Exception e) {
            
                system.debug('Error in updating objects : ' + e.getMessage());
            }
        }
    }
}