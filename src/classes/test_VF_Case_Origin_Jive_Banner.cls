@isTest(seeAllData=true)
public with sharing class test_VF_Case_Origin_Jive_Banner {
	
	static testMethod void test_theVf() {
		
		Contact  currentContact = new Contact(Accountid = ENV.UnknownAccount, firstName = 'First', LastName = 'Last', Phone = '050-5555555', Email ='Test@someService.com', Company_Name_from_Web__c = 'test Company');
	    insert currentContact;
	    
	    Id currentUserId = system.UserInfo.getUserId();
	    
	    Case currentCase = new case(accountid = ENV.UnknownAccount, contactid = currentContact.id);
	    currentCase.Assignee__c = currentUserId;
	    currentCase.Application_Reason__c = 'Sales/POC related inquiry';
	    currentCase.Origin = 'Jive';
	    insert currentCase; 
        
        Test.StartTest();
        ApexPages.standardController thePage = new  ApexPages.standardController(currentCase);
        VF_Case_Origin_Jive_Banner controller = new VF_Case_Origin_Jive_Banner(thePage);
        controller.doStuffAndRedirect();
        Test.StopTest();
	}
}