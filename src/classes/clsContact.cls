global class clsContact 
{
	
		
	@future
	public static void UpdateContact (List <ID> lst_contactsId, List <ID> lst_usersId)
	{
		Map <ID,Contact> RelCon= new Map <ID,Contact>();
		List <User> lst_user = [Select u.ProfileId, u.IsPortalEnabled, u.IsActive, u.ContactId From User u Where Id IN: lst_usersId];
		for(Contact C: [select Deny_Access__c, id, contact_type__c, Portal_Access__c, access_required__c  from Contact where id in :lst_contactsId ])
    	{
    		RelCon.put(C.id, C);
    	}
 	
	 	for(user CPU:lst_user)
	 	{ 
	    	if(RelCon.ContainsKey(CPU.ContactId))
	   		{
		   		
	     		if(CPU.ProfileId ==  MyMellanoxSettings.DesignInUserProfileId) 
	     		{
	     			RelCon.get(CPU.ContactId).contact_type__c = 'Mellanox Silicon Design-In customer';
	     			RelCon.get(CPU.ContactId).Portal_Access__c = 'Mellanox Silicon Design-In customer';
	     			RelCon.get(CPU.ContactId).access_required__c = null;
	     			
	 			}
	     		else if(CPU.ProfileId == MyMellanoxSettings.SystemSupportUserProfileId) 
	            {
	            	
	            	RelCon.get(CPU.ContactId).contact_type__c = 'Mellanox System Customer';
	            	RelCon.get(CPU.ContactId).Portal_Access__c = 'Mellanox System Customer';
	     			RelCon.get(CPU.ContactId).access_required__c = null;
	            }  
	            
	            else {
	            	
	            	RelCon.get(CPU.ContactId).contact_type__c = 'Mellanox System Customer';
	            	RelCon.get(CPU.ContactId).Portal_Access__c = 'Mellanox System OEM Customer';
	     			RelCon.get(CPU.ContactId).access_required__c = null;
	            }
	        }
	    }
		if(!RelCon.isempty())
		{ 
	    	update RelCon.values();
		}
	}
	
	public void createCommunityUserHPAccount(list<Contact> newContacts_List) {
		
		Id HPAccountId = ENV.HPAccountId;
		
		list<User> hpUsers2Create_List = new list<User>();
		
		for (Contact con : newContacts_List) {
			
			if (con.AccountId == HPAccountId) {
				
				User user2Insert = new User();
			    user2Insert.LocaleSidKey = 'en_US';
		    	user2Insert.EmailEncodingKey = 'ISO-8859-1';
		    	user2Insert.LanguageLocaleKey = 'en_US';
		    	user2Insert.UserName = con.HP_Username__c;
		    	user2Insert.contactId = con.Id;
		    	user2Insert.Email = con.Email;
		    	user2Insert.Alias = con.LastName.SubString(0,3);
		    	user2Insert.LastName = con.LastName;
		    	user2Insert.TimeZoneSidKey = 'America/New_York'; 
		    	user2Insert.ProfileId = MyMellanoxSettings.MyMellanoxLicenseUser;
		    	user2Insert.CommunityNickname = 'HP' + user2Insert.UserName;
		    	String Password = con.HP_Password__c;
		    	
		    	 try {	
			   		insert user2Insert;
			   	}
			   	
			   	catch(Exception e) {
	   		
			   		if ( e.getMessage().contains('DUPLICATE_USERNAME') ) {
			   			
			   			user2Insert.userName = '_' + user2Insert.userName;
			   		}
			   		
			   		else if (e.getMessage().contains('DUPLICATE_COMM_NICKNAME') ) {
			   			
			   			user2Insert.CommunityNickname = user2Insert.CommunityNickname + 'Mel';
			   		}
			   			
		   			try {	
		   				
		   				system.debug('user2Insert.userName : ' + user2Insert.userName);
		   				insert user2Insert;
		   			}
		   			
		   			catch(Exception me) {
		   				
		   				if ( me.getMessage().contains('DUPLICATE_USERNAME') ) {
		   			
		   					user2Insert.userName = 'm' + user2Insert.userName;
		   				}
		   				
		   				else if (e.getMessage().contains('DUPLICATE_COMM_NICKNAME') ) {
			   			
			   				user2Insert.CommunityNickname = user2Insert.CommunityNickname + 'Mel';
			   			}
			   			
			   			insert user2Insert;
		   			}
		    	
			   	}
			   	
			   	system.setPassword(user2Insert.Id, password);
			}
		}
	}
		
	
}