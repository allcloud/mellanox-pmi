global class EmailReceiveClass implements Messaging.InboundEmailHandler {

global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,Messaging.InboundEnvelope env){

// Create an inboundEmailResult object for returning 
// the result of the Force.com Email Service
	Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
	
	system.debug('==> email '+email);
// Save attachments, if any
	for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
	  
	  system.debug('==> tAttachment.fileName '+tAttachment.fileName);
	  system.debug('==> Blob.valueOf(tAttachment.body) '+Blob.valueOf(tAttachment.body));
	  	
	  Attachment attachment = new Attachment();
	  attachment.Name = tAttachment.fileName;
	  attachment.Body = Blob.valueOf(tAttachment.body);
	  //insert attachment;
	  system.debug('==> attachment'+attachment);
	}
	for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
	  Attachment attachment = new Attachment();
	  
	  system.debug('==> bAttachment.fileName '+bAttachment.fileName);
	  system.debug('==> bAttachment.body '+bAttachment.body);
	  
	  attachment.Name = bAttachment.fileName;
	  attachment.Body = bAttachment.body;
	  //insert attachment;
	  system.debug('==> attachment'+attachment);
	}

// Set the result to true, no need to send an email back to the user
// with an error message
	result.success = true;

// Return the result for the Force.com Email Service
	return result;
}

static testMethod void testES() {

// Create a new email and envelope object
   Messaging.InboundEmail email = new Messaging.InboundEmail();
   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

// Create the plainTextBody and fromAddres for the test
	email.plainTextBody = 'Here is my plainText body of the email';
	email.fromAddress ='rmencke@salesforce.com';

} 

}