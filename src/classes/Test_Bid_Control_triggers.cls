/**
 */
@isTest
private class Test_Bid_Control_triggers {

    static testMethod void myUnitTest() {
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        Product2 prod4 = obj.createProduct();
        prod4.Name ='HARDWARE';
        prod4.Type__c = 'HW';
        prod4.ProductCode = 'FGTTFSS';
        prod4.Product_Type1__c = 'CABLES';
        prod4.Inventory_Item_id__c = '12121112';
        prod4.partNumber__c = 'dhfgeryte';
        prod4.OEM_PL__C=100;
        insert prod4;
        
        Account Acc = new Account (name = 'NewAcc123');
    	Insert Acc;
    	
    	//Pricebook2 pricebookStandard = [Select p.Name, p.Id From Pricebook2 p where p.Name like 'Standard%' limit 1][0];
    	
        Bid_Control__c bcn = new Bid_Control__c(End_Customer__c=Acc.ID,Customer_Parent__c=Acc.ID,From_Date__c=System.today(),
        											To_Date__c=System.today().adddays(365));
		insert bcn;
		
		Bid_Control_Lines__c bline = new Bid_Control_Lines__c(Bid_Control__c=bcn.id,Product__c=prod4.id,Requested_Price__c=90,
																Qty__c=1000,End_Qty__c=5000);
		insert bline;
																		        											
    }
}