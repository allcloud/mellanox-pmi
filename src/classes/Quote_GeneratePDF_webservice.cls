global class Quote_GeneratePDF_webservice {
	webservice static void addPDF(List<ID> quoteIDs){
        //Instantiate a list of attachment object
        list<attachment> insertAttachment = new list<attachment>();
        Map<ID,Quote> map_quotes = new Map<ID, Quote>([select id, name,quotenumber from Quote where id in :quoteIDs]);
        for(Id qId: quoteIDs){
            //create a pageReference instance of the VF page.
            pageReference pdf = Page.quote_pdf_no_msrp;
            //pass the Account Id parameter to the class.
            pdf.getParameters().put('id',qId);
            Attachment attach = new Attachment();
            Blob body;
            if(!test.isRunningTest()){
                body = pdf.getContent();
            }else{
                body=blob.valueOf('TestString');
            }
            attach.Body = body;
            if(map_quotes != null && map_quotes.get(qid).name != null && map_quotes.get(qid).quotenumber !=null)
            	attach.Name = map_quotes.get(qid).name + '-' + map_quotes.get(qid).quotenumber + '-' + System.now() +'.pdf';
            else
            	attach.Name = 'QuoteID--'+qId+'.pdf';
            attach.IsPrivate = false;
            attach.ParentId = qId;//This is the record to which the pdf will be attached
            insertAttachment.add(attach);
         }
         //insert the list
         insert insertAttachment;
    }
}