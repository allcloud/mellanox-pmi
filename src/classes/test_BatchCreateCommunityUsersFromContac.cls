@isTest
public with sharing class test_BatchCreateCommunityUsersFromContac {

	static testmethod void testTheBatch() {
		
		
		RecordType recType = [SELECT ID FROM RecordType WHERE Name =: 'Parent Account'];
		
		Account acc = new Account();
		acc.Name = 'Elad Test Account123';
		acc.RecordTypeId = recType.Id;
		acc.domain__c = 'testAcc';
		insert acc;
		
		
		Contact c = new Contact();
        c.AccountId = acc.Id;
        c.FirstName = 'Eladtest123';
        c.LastName = 'Test';
        c.Title = 'Test';
        c.Email = 'Test321123@Somewhere123.com';
        c.CRM_Content_Permissions__c = 'Other';
        c.Contact_Type__c = 'standard';
        c.user_type_temp__c = 'System';
        c.test__c = false;
        c.user_craeted__c = false;
        insert c;
		
		
		test.startTest();
                
		ScheduleCreateCommunityUsersFromContacts sch = new ScheduleCreateCommunityUsersFromContacts();

        String jobId = System.schedule('ScheduleContacts',
        '0 0 0 8 9 ? 2020', sch);
        
        test.stopTest();
	}
}