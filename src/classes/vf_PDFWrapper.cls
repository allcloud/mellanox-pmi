public class vf_PDFWrapper {

    public String quoteId { get; set; }
    public String returnURL { get; set; }
    public Quote currQuote { get; set; }
        
        public vf_PDFWrapper()
        {
                quoteId=Apexpages.currentPage().getParameters().get('quoteid');
                returnURL = Apexpages.currentPage().getParameters().get('retURL');
                //currQuote = [Select (Select PricebookEntry.Product2.MSRP__c, PricebookEntry.Product2.Description, PricebookEntry.Product2Id, PricebookEntryId, Quantity, UnitPrice, TotalPrice, Product__c, Discount_MSRP__c, Customer_Ref_Part_Number__c, Order__c From QuoteLineItems Order By Order__c) From Quote q Where Id=: quoteId and IsDeleted = false limit 1][0];
                currQuote= [Select q.Id From Quote q Where q.Id = : quoteId];
        }
        
    public PageReference cancel() {
       
        
        Pagereference pr = new Pagereference(returnURL);
        return pr;
    }


    public PageReference save() {
        
            PageReference pdfPage = Page.quote_pdf_no_msrp;
        pdfPage.getParameters().put('id', quoteId);
        Blob pdfBlob = !Test.isRunningTest() ? pdfPage.getContent() : Blob.ValueOf('dummy text');
        
        QuoteDocument a = new QuoteDocument(QuoteId = currQuote.ID, Document= pdfBlob);
        //a.Name = currQuote.Name+'_'+System.Today().day()+System.Today().month()+System.today().year();
        insert a;
        return new PageReference (returnURL);
    }

}