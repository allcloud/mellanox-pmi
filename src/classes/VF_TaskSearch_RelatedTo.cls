public with sharing class VF_TaskSearch_RelatedTo {
	
	// A List of Milestones related to the Task related Project to display on the page
	public list<Milestone1_Milestone__c> project_Related_Milstones_List {get;set;}
	public  SObject obj;
	public Task task;
	// The Current Task retrieved from the DB with all relevant fields values
	private Task thetTask {get;set;}
	
	private integer totalRecs = 0;
	private integer OffsetSize = 0;
	// Amount of Records to be displayed on the page
	private integer LimitSize= 10;
	
	// Controller
	public VF_TaskSearch_RelatedTo(ApexPages.StandardController controller)
	{
		obj = controller.getRecord();  
	    task = (Task)obj;
	    
	    thetTask = [SELECT Id, WhatId FROM Task WHERE Id =: task.Id]; 
	    // The current Task related Milestone
	    Milestone1_Milestone__c relatedMilestone = [SELECT id, Name, Project__c FROM Milestone1_Milestone__c WHERE Id =: thetTask.WhatId];
	    // List of all the Miestones related to the project that the task is related to
	    project_Related_Milstones_List = [SELECT id, Name FROM Milestone1_Milestone__c WHERE Project__c =: relatedMilestone.Project__c and Complete__c != true];
	    
	     
	}
	
	//This method will save the selected Milestone as the Task's Milestone
	public void save()
	{
		Id milstoneId = ApexPages.currentPage().getParameters().get('milestoneId');
		thetTask.WhatId = milstoneId;
		update thetTask;
	}
	
	// This method will take the user to the page displaying previous table of records
	public void previous()
	{
		OffsetSize = OffsetSize - LimitSize;
	}
	// This method will take the user to the page displaying next table of records
	public void next()
	{
		OffsetSize = OffsetSize + LimitSize;
	}
	 
	// This method will Disable/Enable the 'Previous' Button if there are No records to be displayed 
	public boolean getprev()
	{
		if(OffsetSize == 0)
		return true;
		else
		return false;
	}
	// This method will Disable/Enable the 'Next' Button if there are No records to be displayed 
	public boolean getnxt()
	{
	if((OffsetSize + LimitSize) > totalRecs)
	return true;
	else
	return false;
	}

}