public with sharing class SupportProductFamilyController {

    private My_Mellanox_Setting__c settings = My_Mellanox_Setting__c.getInstance();
    Private List<ProductFamily__c> productFamily {get; set;}
    Private List<Product_Category__c> productCategory {get; set;}
    Private List<ProductDetails__c> productDetailsList {get; set;}
    public boolean isDiterbutorProfile{get;set;}
    public boolean isDesignInProfile{get;set;}
    public boolean isTestDesignInProfile{get;set;}
    public boolean isLicenseProfile {get;set;}
    
    private User currentUser{get;set;}
    //Private list<Product_Category__c> productCategoryList;
   
    public List<productFamilyWrap> pfwList { get; set; }
   // public List<productCategoryWrap> pcwList { get; set; }
    public List<productDetailWrap> pdwList { get; set; }
    
    public SupportProductFamilyController() {
        System.debug( 'START: SupportProductFamilyController.constructor()' );
        
        isDiterbutorProfile = false;
        isDesignInProfile = false;
        isTestDesignInProfile = false;
        isLicenseProfile = false;
        
        Id currentUserProfileId = system.UserInfo.getProfileId();
        
        if (currentUserProfileId != settings.MyMellanox_Distributor_User__c && currentUserProfileId != settings.MyMellanox_Sales_User__c
        
        && currentUserProfileId != settings.MyMellanox_License_User__c) {
            
            if (currentUserProfileId != settings.MyMellanox_Documentation_Only_User__c) {     
                
                isDesignInProfile = true;
            }
            
            else {
                
                isTestDesignInProfile = true;
            }
        }
        
        else if (currentUserProfileId == settings.MyMellanox_License_User__c) {
            
            isLicenseProfile = true;            
        }
        
        else {
            
            isDiterbutorProfile = true;
        }
        
        loadProductFamily();
        //loadProductCategory();
        //loadProductDetails();
        
        loadProducts();
        
        System.debug( 'END: SupportProductFamilyController.constructor()' );
    }
    
    public void loadProductFamily() {
        
        
        List<productCategoryWrap> pcw;
        List<productDetailWrap> pdw;
        pfwList = new List<productFamilyWrap>();
        //pcwList = new List<productCategoryWrap>();
        pdwList = new List<productDetailWrap>();
        Id userId = system.UserInfo.getUserId();
        
        currentUser = [SELECT Id, ProfileId FROM User WHERE Id =: userId];
        
        
            String pfQuery = '';
            
            list<ProductFamily__c> productFamily_List = new list<ProductFamily__c>();
            
            map<Id, ProductFamily__c> productFamilyIds2ProductFamilies;
            
            system.debug('UserInfo.getUserType()' + UserInfo.getUserType());
            system.debug('UserInfo.getProfileId ' + UserInfo.getProfileId());
            
            if ( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && UserInfo.getProfileId() == settings.System_Support_Profile_ID__c ) {
                
                productFamilyIds2ProductFamilies = new map<Id, ProductFamily__c>([select Id, Name, StaticResourceImage__c, DisplayOrder__c, IsEzchip__c, Private_Checkbox__c
                                       from ProductFamily__c where ActiveCategory__c = true and IsEzchip__c = false and Private_Checkbox__c = false order By DisplayOrder__c asc Limit 8]);
            }
            
            else if( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && UserInfo.getProfileId() == settings.Ezchip_Design_In_Profile_ID__c ) {
                
                productFamilyIds2ProductFamilies = new map<Id, ProductFamily__c>([select Id, Name, StaticResourceImage__c, DisplayOrder__c, IsEzchip__c, Private_Checkbox__c
                                       from ProductFamily__c where ActiveCategory__c = true and Private_Checkbox__c = false Order By DisplayOrder__c asc Limit 8]);
            }
            else if (UserInfo.getProfileId() == settings.Design_In_Profile_ID__c ) {
                
                productFamilyIds2ProductFamilies = new map<Id, ProductFamily__c>([select Id, Name, StaticResourceImage__c, DisplayOrder__c, IsEzchip__c, Private_Checkbox__c
                                       from ProductFamily__c where ActiveCategory__c = true and IsEzchip__c = false Order By DisplayOrder__c asc Limit 8]);
            }
            else {
               	productFamilyIds2ProductFamilies = new map<Id, ProductFamily__c>([select Id, Name, StaticResourceImage__c, DisplayOrder__c, IsEzchip__c, Private_Checkbox__c
                                       from ProductFamily__c where ActiveCategory__c = true Order By DisplayOrder__c asc Limit 8]);         	
            }
            
            list<decimal> prFamilyDispOrder_List = new list<decimal>();
            
            for (ProductFamily__c family : productFamilyIds2ProductFamilies.values()) {
                
                prFamilyDispOrder_List.add(family.DisplayOrder__c);
            }
            
            prFamilyDispOrder_List.sort();
            
          
            String pfcQuery = '';
            
            pfcQuery += 'select id, Name, Product_Family__c, Display_Order__c, Display_Order_Combined__c, IsEzchip__c, Is_Account_Specific__c, Private_Checkbox__c,  ' +
                '(select id, Name, StaticResourceImage__c, Product_Category__c, Display_Order__c from Product_Details__r where Active__c = true ';
            
            
            pfcQuery += 'Order By Display_Order__c asc, Name asc) from Product_Category__c where Active__c = true ';
            
            if ( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && UserInfo.getProfileId() == settings.System_Support_Profile_ID__c ) {
                pfcQuery += 'and IsEzchip__c = false and Private_Checkbox__c = false ';
                system.debug('-- Support Profile --');
            }
            
            else if( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && UserInfo.getProfileId() == settings.Ezchip_Design_In_Profile_ID__c ) {
                pfcQuery += 'and Private_Checkbox__c = false ';
                system.debug('-- Ezchip Design In Profile --');
            }
            
            else if (UserInfo.getProfileId() == settings.Design_In_Profile_ID__c ) {
                pfcQuery += 'and IsEzchip__c = false ';
                system.debug('-- Design In Profile --');
            }
            else{
            	system.debug('-- Internal user --');
            }
            
            pfcQuery += 'Order By Display_Order_Combined__c asc';
            
            system.debug('pfcQuery :' + pfcQuery );
            productCategory = Database.query(pfcQuery);
            system.debug('productCategory :' + productCategory );
            map<decimal, list<productCategoryWrap>> productFamily2ProductCategoryWrapLists_Map = new map<decimal, list<productCategoryWrap>>();
            map< decimal, Id> sortOrder2FamilyIds_Map = new map< decimal, Id>();
            list<productCategoryWrap> prodcatWrap_List;
        
            for(Product_Category__c pc : productCategory) {
                system.debug('pc Name:' + pc.Name);
                system.debug('inside 145 loop');
                pdwList = new list<productDetailWrap>();
                        
                for(ProductDetails__c pd : pc.Product_Details__r){
                    
                    pdwList.add(new productDetailWrap(pd));
                }

                if (productFamilyIds2ProductFamilies.containsKey(pc.Product_Family__c)) {
                    
                    if (productFamily2ProductCategoryWrapLists_Map.containsKey(productFamilyIds2ProductFamilies.get(pc.Product_Family__c).DisplayOrder__c)) {
                            
                         system.debug('inside 156 loop');       
                        productFamily2ProductCategoryWrapLists_Map.get(productFamilyIds2ProductFamilies.get(pc.Product_Family__c).DisplayOrder__c).add(new productCategoryWrap(pc,pdwList));
                    }
                        
                    else {
                             
                        system.debug('inside 162 loop');        
                        prodcatWrap_List = new list<productCategoryWrap>{new productCategoryWrap(pc,pdwList)};
                                
                        productFamily2ProductCategoryWrapLists_Map.put(productFamilyIds2ProductFamilies.get(pc.Product_Family__c).DisplayOrder__c, prodcatWrap_List);
                        sortOrder2FamilyIds_Map.put(productFamilyIds2ProductFamilies.get(pc.Product_Family__c).DisplayOrder__c, pc.Product_Family__c );
                        pfwList.add(new productFamilyWrap(productFamilyIds2ProductFamilies.get(pc.Product_Family__c),prodcatWrap_List));
                    }
                }
                
            }
            
        system.debug('pfwList : ' + pfwList);
        system.debug('pfwList size: ' + pfwList.size());
    }
    
    public class productFamilyWrap{
        public ProductFamily__c productFamily{get;set;}
        public List<productCategoryWrap> productCategory{get;set;}
        public productFamilyWrap(ProductFamily__c a,List<productCategoryWrap> b){
            productFamily = a;
            productCategory = b;
        }
    }
   
    public class productCategoryWrap{
        public Product_Category__c productCategory{get;set;}
        public List<productDetailWrap> productDetail{get;set;}
        public productCategoryWrap(Product_Category__c a, List<productDetailWrap> b){
            productCategory = a;
            productDetail = b;
        }
    }
    
    public class productDetailWrap{
        public ProductDetails__c productDetail{get;set;}
        public productDetailWrap(ProductDetails__c a){
            productDetail = a;
        }
    }
    
    private Map<ProductFamily__c,List<Product_Category__c>> productCatMap = new Map<ProductFamily__c,List<Product_Category__c>>();

    private Map<Product_Category__c,List<ProductDetails__c>> productMap = new Map<Product_Category__c,List<ProductDetails__c>>();


    public void loadProducts() {
        
    
        String query = '';
        
        query += 'select Id, Name, StaticResourceImage__c, DisplayOrder__c from ProductFamily__c where ActiveCategory__c = true ';
        
        if( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && currentUser.ProfileId != settings.Design_In_Profile_ID__c && currentUser.ProfileId != settings.System_OEM_Profile_ID__c) {
            query += 'and Private_Checkbox__c = false ';
        }
        
        query += 'Order By DisplayOrder__c asc';
        
         
        List<ProductFamily__c> productFamily = Database.query(query);
        
        system.debug('productFamily: ' + productFamily);
        system.debug('productFamily size : ' + productFamily.size());
        
        List<Product_Category__c> productCats = new List<Product_Category__c>();
            
        String pfcQuery = '';
        
        pfcQuery += 'select id, Name, Product_Family__c, Display_Order__c, IsEzchip__c, Private_Checkbox__c from Product_Category__c Where Active__c = true ';
        
        if ( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && UserInfo.getProfileId() == settings.System_Support_Profile_ID__c ) {
            pfcQuery += 'and IsEzchip__c = false and Private_Checkbox__c = false ';
            system.debug('-- Support Profile --');
        }
        
        else if( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && UserInfo.getProfileId() == settings.Ezchip_Design_In_Profile_ID__c ) {
            pfcQuery += 'and Private_Checkbox__c = false ';
            system.debug('-- Ezchip Design In Profile --');
        }
        
        else if (UserInfo.getProfileId() == settings.Design_In_Profile_ID__c ) {
            pfcQuery += 'and IsEzchip__c = false ';
            system.debug('-- Design In Profile --');
        }
        else{
        	system.debug('-- Internal user --');
        }
        
        pfcQuery += 'Order By Display_Order__c asc';
        
        List<Product_Category__c> productCatList = Database.query(pfcQuery);
        
        map<Id, list<Product_Category__c>> productFamily2Cat_Map = new map< Id, list<Product_Category__c> >();
        
        list<Product_Category__c> cat_List;
        
        for (Product_Category__c cat : productCatList){
            
            if (productFamily2Cat_Map.containsKey(cat.Product_Family__c)) {
                
                productFamily2Cat_Map.get(cat.Product_Family__c).add(cat);
            }
            
             else {
                
                cat_List = new list<Product_Category__c>();
                cat_List.add(cat);
                productFamily2Cat_Map.put(cat.Product_Family__c, cat_List);
             }
            
         }
        
        
        String pdQuery = '';
                
        pdQuery += 'select id, Name, StaticResourceImage__c, Product_Category__c, Display_Order__c from ProductDetails__c Where  Active__c = true ';
        
        if( !UserInfo.getUserType().EqualsIgnoreCase('Standard') && UserInfo.getProfileId() != settings.Design_In_Profile_ID__c && UserInfo.getProfileId() != settings.System_OEM_Profile_ID__c) {
            pdQuery += 'and Private_Checkbox__c = false ';
        }
        
        pdQuery += 'Order By Display_Order__c asc';
        
        List<ProductDetails__c> productList = Database.query(pdQuery);
        
        system.debug('productList :' + productList);
        
         map<Id, list<ProductDetails__c>> productCat2Details_Map = new map< Id, list<ProductDetails__c> >();
         
         list<ProductDetails__c> details_List;
        
        for (ProductDetails__c detail : productList) {
            
            if (productCat2Details_Map.containsKey(detail.Product_Category__c)) {
                
                productCat2Details_Map.get(detail.Product_Category__c).add(detail);
            }
            
            else {
                
                details_List = new list<ProductDetails__c>();
                details_List.add(detail);
                productCat2Details_Map.put(detail.Product_Category__c, details_List);
            }
        }
        
        
        system.debug('232: '  );
        
        for (ProductFamily__c pf : productFamily){

            string pfId = pf.Id;
            
          
            
            if (productFamily2Cat_Map.containsKey(pf.Id)) {
    
                for (Product_Category__c pfc : productFamily2Cat_Map.get(pf.Id)) {
        
                    system.debug('242: ' );
                    string productCatId = pfc.Id;
        
                    productCats.add(pfc);
                    
                
                    
                    system.debug('productList: ' + productList);
        
                    List<ProductDetails__c> products = new List<ProductDetails__c>();
        
                    if (productCat2Details_Map.containsKey(pfc.Id)) {
        
                        for (ProductDetails__c pd : productCat2Details_Map.get(pfc.Id)) {
                            products.add(pd);
                        }
                    }
                    productMap.put(pfc, products);
        
                }
            }
            productCatMap.put(pf, productCats);
        }
        
        system.debug('productMap: ' + productMap);
        
        system.debug('productCatMap: ' + productCatMap);
    }

    public Map<ProductFamily__c,List<Product_Category__c>> getProductMap(){
        return productCatMap;
    }

    public Map<Product_Category__c,List<ProductDetails__c>> getProductCatMap(){
        return productMap;
    }
    
}