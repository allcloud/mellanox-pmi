@isTest
    private class Test_Controllers {
    
       
        
        
        /*
        static testMethod void Test_VFquote_pdf() 
        {    
           US_Territory_Map__c tm = new US_Territory_Map__c( SD_Region__c = 'EMEA', Region__c = '0000'); 
           tm.Sales_Director__c = '00550000000wuVp';
           insert tm;

            
            Opportunity opp = new Opportunity(ownerid = '00550000000wuVp',Name='Test Opp', StageName='Open', type = 'Direct',CloseDate=Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
            
            insert opp;
            
            Quote q = new Quote(OpportunityId = opp.Id,Name='Test Quate');
            
            insert q;
            
            ApexPages.StandardController sController = new Apexpages.Standardcontroller(q);
            VFquote_pdf controller = new VFquote_pdf(sController);
            
            
            
        }*/
        
        static testMethod void Test_CaseController() 
        {
            Case c = new Case();
            
            insert c;
            
            Apexpages.Standardcontroller sController = new Apexpages.Standardcontroller(c);
            CaseController caseController = new CaseController(sController);
            
            c.State__c =  Util.caseStates.get('Open');
            update c;
            CaseController.GetStatePossibleValues();
            
            c.Assignee__c = Userinfo.getUserId();
            c.State__c =  Util.caseStates.get('Assigned');
            update c;
            CaseController.GetStatePossibleValues();
            
            c.State__c =  Util.caseStates.get('RMA Assigned');
            update c;
            CaseController.GetStatePossibleValues();
            
            c.Silicon_Board__c = 'PhyX';
            c.Requested_Delivery_Date__c = Date.today()+3;
            //c.State__c =  Util.caseStates.get('Open AE');
            update c;
            CaseController.GetStatePossibleValues();
            
            User dan = [Select u.Id from User u Where u.Name = 'Dan Waxman' Limit 1];
            c.AE_PM_Escalated__c = 'AE';
            c.AE_Class__c = 'AE class';
            c.AE_Sub_Class__c = 'AE sub class';      
            c.Assignee__c = dan.Id;
            //c.State__c =  Util.caseStates.get('Assigned AE');
            update c;
            CaseController.GetStatePossibleValues();
            
            Test.StartTest();
            c.Fix_in_the_release_version__c = 'Fix';
            update c;
            
            
            c.State__c = Util.caseStates.get('Waiting for Customer Info');
            update c;
            CaseController.GetStatePossibleValues();
            
            c.State__c =  Util.caseStates.get('On Hold');         
            c.Hold_End_Date__c = system.today();        
            update c;
            CaseController.GetStatePossibleValues();

            c.State__c = Util.caseStates.get('Closed');
            update c;
            CaseController.GetStatePossibleValues();
            
            CaseController.GetOpenerUrl();
            
            CaseController.IsPublished = true;
            CaseController.Comment = 'Test';
            CaseController.syncFM = true;
            
            CaseController.save();
            Test.StopTest();
        }
        
        
        /*
        static testMethod void Test_CaseController2() 
        {
            Case c = new Case();
            
            insert c;
            
            Apexpages.Standardcontroller sController = new Apexpages.Standardcontroller(c);
            CaseController caseController = new CaseController(sController);
            
            c.State__c =  Util.caseStates.get('Open');
            update c;
            CaseController.GetStatePossibleValues();
            
            c.Assignee__c = Userinfo.getUserId();
            c.State__c =  Util.caseStates.get('Assigned');
            update c;
            CaseController.GetStatePossibleValues();
            
            c.State__c =  Util.caseStates.get('RMA Assigned');
            update c;
            CaseController.GetStatePossibleValues();
            
            c.Silicon_Board__c = 'PhyX';
            c.Requested_Delivery_Date__c = Date.today()+3;
            //c.State__c =  Util.caseStates.get('Open AE');
            update c;
            CaseController.GetStatePossibleValues();
            
            User dan = [Select u.Id from User u Where u.Name = 'Dan Waxman' Limit 1];
            c.AE_PM_Escalated__c = 'AE';
            c.AE_Class__c = 'AE class';
            c.AE_Sub_Class__c = 'AE sub class';      
            c.Assignee__c = dan.Id;
            //c.State__c =  Util.caseStates.get('Assigned AE');
            update c;
            CaseController.GetStatePossibleValues();
            
            Test.StartTest();
            c.Fix_in_the_release_version__c = 'Fix';
            update c;
            
            c.State__c =  Util.caseStates.get('Wait for Release');
            c.Waiting_for_Release_End_Date__c = system.now();
            update c;
            CaseController.GetStatePossibleValues();
            
            c.State__c =  Util.caseStates.get('Waiting for Customer Approval');
            update c;
            CaseController.GetStatePossibleValues();
            
           
            Test.StopTest();
        }
        
        */
        
        static testMethod void OppRegFormController(){
            
            Case c = new Case();
            
            insert c;
            
            Apexpages.Standardcontroller sController = new Apexpages.Standardcontroller(c);
            OppRegFormController oppController = new OppRegFormController(sController);
            
            oppController.getAllCountries();
            
            oppController.odisioppreg.Disty_Country__c = 'USA';
            oppController.odisioppreg.Partner_Country__c = 'USA';
            oppController.odisioppreg.End_User_Country__c = 'USA';
            
            
            oppController.GetDistiList();
            oppController.getDistStates();
            oppController.getEndUserStates();
            oppController.getPartnerStates();
            
            oppController.Save();
            
            oppController.odisioppreg.Requestor_Name__c= 'a'; 
            oppController.odisioppreg.Requestor_Email__c= 'Abs59il@gmail.com';
            oppController.odisioppreg.Distributor_Name__c='BELL MICRO';
            oppController.odisioppreg.Disty_Contact_Name__c= 'Hello World'; 
            oppController.odisioppreg.Disty_State__c= 'USA'; 
            oppController.odisioppreg.Disty_Contact_Email__c= 'Abs59il@gmail.com';
            oppController.odisioppreg.Disty_Country__c= 'Israel';
            oppController.odisioppreg.Disty_Phone__c= '065226272' ;
            oppController.odisioppreg.Partner_Reseller_Name__c='sdfsf'; 
            oppController.odisioppreg.Partner_Contact_Name__c='sfsfsffsf';
            oppController.odisioppreg.Partner_State__c='USA'; 
            oppController.odisioppreg.Partner_Contact_Email__c='Abs59il@gmail.com';
            oppController.odisioppreg.Partner_Country__c='Israel';
            oppController.odisioppreg.Partner_Contact_Phone__c='812762372'; 
            oppController.odisioppreg.End_User__c='sdfsdfsf'; 
            oppController.odisioppreg.End_User_Contact_Name__c='ssdfsf' ;
            oppController.odisioppreg.End_User_State__c='USA' ;
            oppController.odisioppreg.End_User_Contact_Email__c='Abs59il@gmail.com';
            oppController.odisioppreg.End_User_Country__c='Israel'; 
            oppController.odisioppreg.End_User_Contact_Phone__c='812762372'; 
            
        
            
            oppController.odisioppreg.Name='sfsfsf';
            oppController.odisioppreg.Opportunity_Close_Date__c= Date.Today()+10;
            oppController.odisioppreg.Opportunity_Ship_Date__c=Date.Today()+10;
            
            
            oppController.Save(); 
        
            oppController.Reset();

            
        }
        /*
        static testMethod void VFEditAllQuoteLineItem() 
        {
             US_Territory_Map__c tm = new US_Territory_Map__c( SD_Region__c = 'EMEA', Region__c = '0000'); 
             tm.Sales_Director__c = '00550000000wuVp';
             insert tm;

            Opportunity opp = new Opportunity(ownerid ='00550000000wuVp', Name='Test Opp', StageName='Open',type = 'Direct',CloseDate=Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
            
            insert opp;
            
            Quote q = new Quote(OpportunityId = opp.Id,Name='Test Quate');
            
            insert q;
            
            ApexPages.StandardController sController = new Apexpages.Standardcontroller(q);
            VFEditAllQuoteLineItem controller = new VFEditAllQuoteLineItem(sController);
            
            controller.Save();
            controller.Cancel();
            
            
            
        }
        */
        static testMethod void Opportunity_Reg_Upd() 
        {

            //Distributor_Oppy_Registrations__c Dor = [Select Partner_State__c,Partner_Country__c,Requestor_Email__c,Opportunity_Close_Date__c,Opportunity_Ship_Date__c,OPN_1_Quantity__c,Discount_Granted__c From Distributor_Oppy_Registrations__c Where Discount_Granted__c != :null Limit 1];
            
            Distributor_Oppy_Registrations__c Dor = new Distributor_Oppy_Registrations__c(Discount_Granted__c=0.5, Partner_State__c='test', Partner_Country__c='US', Requestor_Email__c='test@test.com', 
                                                                                            Opportunity_Close_Date__c = System.today()+50,End_User_Country__c='Italy', Opportunity_Ship_Date__c = System.today()+50);
            insert Dor;
            
            System.currentPageReference().getParameters().put('Id',dor.Id);
            
            Apexpages.Standardcontroller sController = new Apexpages.Standardcontroller(Dor);
            Opportunity_Reg_Upd oppController = new Opportunity_Reg_Upd(sController);
            
        //  oppController.Upd_Approve_OpReg();
            oppController.Upd_Send_Email();
        
        }
        
        /*
        static testMethod void CaseFM_ComponentController()
        {
            
            Case c = new Case();
            
            insert c;
            
            CaseComment cc = new CaseComment(ParentId = c.Id);
            cc.CommentBody ='Comment By Mellanox User :';
            insert cc;
          
            CaseComment cc1 = new CaseComment(ParentId = c.Id);
            cc1.CommentBody ='A mail from User ';
            insert cc1;       
                   
            Folder f = [Select f.Id From Folder f Limit 1];
    
            Document doc = new Document(Name='Test',FolderId=f.Id);
            insert doc;
            
            FM_Discussion__c fm = new FM_Discussion__c();
            fm.Public__c = true;
            fm.Case__c = c.Id;
            fm.Case_Comment_ID__c = cc.Id; 
            fm.Attachment_Link__c = 'www.Walla.co.il';
            fm.DOC_ID__c = doc.Id;
            insert fm;
            
    
            
           
            
            System.currentPageReference().getParameters().put('CommentId_d',fm.Id);  
                        
            CaseFM_ComponentController controller = new CaseFM_ComponentController();
            controller.caseId = c.Id;
            CaseFM_ComponentController.cFMComments[] o = controller.comments;
            
            controller.publishComment(); 
        
            
        }
        */

        
    }