@isTest(seeAllData=true)
public with sharing class test_VF_Reseller_Dell_Partner_Banner {
	
	static testMethod void test_theVF() {
		
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		 
		Account acc = creator.createAccount();
        insert acc;
        
        Contact con = creator.CreateContact(acc);
        insert con;
        
        Case c = creator.Create_case(acc, con);
        c.AccountId = acc.Id;
        c.ContactId = con.Id;
        c.Type = 'Feature Request';
        c.Reseller_Dell_Partner__c = 'Test';
        insert c;
        
        Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(c);
        VF_Reseller_Dell_Partner_Banner handler = new VF_Reseller_Dell_Partner_Banner(controller1);
	}
}