@isTest
public with sharing class Test_VF_ProjectMilestone_RelatedList {
	
	static testMethod void test_VFProjectMilestone_RelatedList(){
		
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		
		Account acc = creator.createAccount();
	 	insert acc;
	 	
	 	Milestone1_project__c project = creator.CreateMPproject(acc.id);
		insert project;
		
		Milestone1_Milestone__c milestone = creator.CreateMilestone(project.Id);
		insert milestone;
		
		 Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(project);
		 VF_ProjectMilestone_RelatedList pjMilestone = new VF_ProjectMilestone_RelatedList(controller);
		 
		 system.test.startTest();
		 
		 pjMilestone.getPJMilestones();
		 pjMilestone.turnToEditMode();
		 milestone.Name = 'test Milestone';
		 pjMilestone.saveMilestones();
		 
		 system.test.stopTest();
		
		
	}

}