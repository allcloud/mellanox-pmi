/********************************************************************
 * Controller class for mlnxMobileCaseEdit VF page on mobile devices
 * 
 * @Author: Abdul Sattar (Magnet 360)
 * @Date:	08.26.2015
 *
 * @Updates:
 * 09.01.2015   Abdul Sattar (Magnet 360)
 *              Updated init() to pre-populate Product fields on new Case when
 *              creating from Asset
 * 
 ********************************************************************/
public with sharing class mlnxMobileCaseEditCtrl {
    private ApexPages.StandardController sc;    // Reference to standard controller
    private Boolean isCommunity;                // In Community flag
    
    public Case currentCase {get; set;}         // Reference to current case
    public String astId {get; set;}             // Asset ID passed in query parameters
    public CaseComment comment {get; set;}      // Reference to case comment
    public boolean isEditMode {get;set;}
    public list<SelectOption> asetsSerials {get;set;}
     public String selectedSN {get;set;}
   
    // Default constructor
    public mlnxMobileCaseEditCtrl(ApexPages.StandardController sc){
        try {
            // Initialize reference to standard controller
            this.sc = sc;   

            // Initialize current case with current SC record
            currentCase = (Case)sc.getRecord();

            // Get community status
            isCommunity = ( Network.getNetworkId() != null );

            // Receive assetId query parameter
            astId = ApexPages.currentPage().getParameters().get('assetId');
             
           isEditMode = (astId != null) ? true : false ;
             
			currentCase.SN__c = ApexPages.currentPage().getParameters().get('assetName');
            // Only initialize case fields while creating a new case
            if(currentCase.Id == null) {
                init();
            }
            
            else {
            	
            	currentCase = [SELECT Id, Subject, Product_Family__c, Priority, Description, Product_Category__c, Board_Part_Number__c, SN__c FROM Case WHERE Id =: currentCase.Id]; 
            
            	if (currentCase.Board_Part_Number__c != null) {
	    		
	    		asetsSerials = new List<SelectOption>();
    			set<String> Assetsnames_Set = new set<String>();
		  
		    	for (Asset2__c relatedAsset : [SELECT Id, Name FROM Asset2__c WHERE Product_Model_Number__c =: currentCase.Board_Part_Number__c] ) {
		    		
		    		Assetsnames_Set.add(relatedAsset.Name);
		    	}
		    	
		 	    for (String assetName : Assetsnames_Set) {
		 	    	
		 	    	asetsSerials.add(new selectOption (assetName, assetName));
		 	    }
		 	    
		 	   
		    }
            }
        } 
        
        catch (Exception e) {
            System.debug('General VF Exception: ' + e.getMessage());
        }
        
        
    }
    
    public void getAssetSeialsOptions() {
    	
    	asetsSerials = new List<SelectOption>();
    	set<String> Assetsnames_Set = new set<String>();
    	
	    if (currentCase.Board_Part_Number__c != null) {
	    
	    	for (Asset2__c relatedAsset : [SELECT Id, Name FROM Asset2__c WHERE Product_Model_Number__c =: currentCase.Board_Part_Number__c] ) {
	    		
	    		Assetsnames_Set.add(relatedAsset.Name);
	    	}
	    	
		 	if (!Assetsnames_Set.isEmpty()) {
		 	   
		 	    for (String assetName : Assetsnames_Set) {
		 	    	
		 	    	asetsSerials.add(new selectOption (assetName, assetName));
		 	    }
		 	}
		 	
		 	else {
		 		
		 		asetsSerials.add(new selectOption ('--None--', '--None--'));
		 		asetsSerials.add(new selectOption ('I dont have my serial number', 'I dont have my serial number'));
		 	}
	    }
    }
    
    // Overridden save method to navigate to mlnxMobileCaseView page
    public PageReference save() {
    	
    	currentCase.Origin = 'Mobile';
    	 
        if (currentCase.Product_Family__c == null || currentCase.Board_Part_Number__c == null
            || currentCase.Product_Category__c == null
            || currentCase.Subject == null || currentCase.Description == null
            || currentCase.Priority == null) {
            	 
            	 system.debug('currentCase.Product_Family__c : ' + currentCase.Product_Family__c);
            	 system.debug('currentCase.Board_Part_Number__c : ' + currentCase.Board_Part_Number__c);
            	 system.debug('currentCase.SN__c : ' + currentCase.SN__c);
            	 system.debug('currentCase.Product_Category__c : ' + currentCase.Product_Category__c);
            	 system.debug('currentCase.Subject : ' + currentCase.Subject);
            	 system.debug('currentCase.Description : ' + currentCase.Description);
            	 system.debug('currentCase.Priority : ' + currentCase.Priority);
            	 
            	 
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'All fields are mandatory'));
            	return null;
            }
        upsert currentCase;
        astId = ApexPages.currentPage().getParameters().get('assetId');
	     
	    String url = (astId != null) ? '/mlnxMobileAssetView?id=' + astId : '/mlnxMobileCaseView?Id=' + currentCase.Id;
	   
	    PageReference targetPage = new PageReference( url );
	    return targetPage;
       // return currentPage;
    }
    

    // Overridden cancel method
    // Navigates to mlnxMobileCaseView (Case View) page in case of editing an existing case
    // Else navigates to mlnxMobileCases (My Cases) page
    public PageReference cancel() {
        /*
        PageReference currentPage = sc.cancel();
        currentCase = (Case) sc.getRecord();

        if (currentPage != null) {
            PageReference targetPage = new PageReference( buildTargetURL() );
            return targetPage;
        }
        */
        
        PageReference returnPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));  

        return returnPage;
    }

    // Initialize new case fields
    public void init(){
    	if ( currentCase != null ) {
	        Id tsRTId;	// Variable to hold Case record type id.
	        List<RecordType> caseRts = [SELECT Id FROM RecordType WHERE DeveloperName = 'TechnicalSupport' LIMIT 1];
	        
	        system.debug('caseRts : ' + caseRts);
	        
	        if (!caseRts.isEmpty()) { 
	        	
	        	tsRTId = caseRts[0].Id; 
	        }

	        if(tsRTId != null) {
	        	// Initialize Record Type Id on Case
	        	currentCase.RecordTypeId = tsRTId;
	        }
	        
	        system.debug('currentCase.RecordTypeId : ' + currentCase.RecordTypeId);
	         system.debug('astId : ' + astId);

            if (!String.isBlank(astId)) {
                // Get Asset Details
                Asset2__c asset = selectAsset();

                if(asset != null) {
                    // Variable to hold details for pre-populating Case fields
                    String lsBoardPartNo, lsProductCategory, lsProductFamily;

                    // Get Board Part No.
                    List<Product2> products = [ SELECT  Product_Detail_Name__c  
                                                FROM    Product2 AS P 
                                                WHERE   P.Name = :asset.Part_Number__c 
                                                LIMIT 1];
                    if(!products.isEmpty()) {
                        // Get Board Part No
                        lsBoardPartNo = ( String.isBlank(products[0].Product_Detail_Name__c) ? '' : products[0].Product_Detail_Name__c );
                        
                        // Select others from database
                        List<ProductDetails__c> prdDetails = [  SELECT   Product_Category__c
                                                                        ,Product_Family__c 
                                                                FROM    ProductDetails__c  AS  PD 
                                                                WHERE PD.name = :lsBoardPartNo LIMIT 1];
                        
                        if(!prdDetails.isEmpty()) {
                            // Get Product Category & Family
                            lsProductCategory = ( String.isBlank(prdDetails[0].Product_Category__c) ? '' : prdDetails[0].Product_Category__c );
                            lsProductFamily = ( String.isBlank(prdDetails[0].Product_Family__c) ? '' : prdDetails[0].Product_Family__c );
                        }    
                    }
                    
                    
                    // Set initial values on Case
                    currentCase.Product_Family__c = asset.Asset_Family__c;
                    currentCase.Product_Category__c =  asset.Product__r.Product_Type1__c ;   // lsProductCategory;
                    currentCase.Board_Part_Number__c =  asset.Part_Number__c ;    // lsBoardPartNo;
                     
                }
            }
    	}
    }

    // Prepare target URL
    private String buildTargetURL () {
        // Construct URL of view page
        String targetURL;

        targetURL = ( isCommunity ? Site.getPathPrefix() + '/' : '/' );

        if(currentCase.Id == null) {
            targetURL = targetURL + 'apex/mlnxMobileCases';
        } else {
            targetURL = targetURL + 'apex/mlnxMobileCaseView?Id=' + currentCase.Id;
        }

        System.debug('SCS: targetURL = ' + targetURL);
        return targetURL;
    }

    // Select and return the asset fields that are needed for
    // initializing a Case when created from an Asset
    private Asset2__c selectAsset() {
        List<Asset2__c> assets = [SELECT Id, Asset_Family__c, Product__r.Product_Type1__c, Part_Number__c FROM Asset2__c WHERE Id = :astId LIMIT 1];

        if (!assets.isEmpty()) {
            return assets[0];
        }

        return null;
    }
}