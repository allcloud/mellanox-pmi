@isTest
public with sharing class test_Case_attachment_afterInsert {
	
	static testMethod void test_attExistsOnRMA()
	{
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		
		
		RMA__c rma = creator.CreateRMA();
		insert rma;
		
		Attachment att = new Attachment();
		att.ParentId = rma.Id;
		att.Name = 'Test att';
		Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	att.body=bodyBlob;
		insert att;
		
		Attachment att1 = new Attachment();
		att1.ParentId = rma.Id;
		att1.Name = 'Test att1';
		Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
    	att1.body=bodyBlob1;
    	
    	Attachment att2 = new Attachment();
		att2.ParentId = rma.Id;
		att2.Name = 'Test att1';
		Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body');
    	att2.body=bodyBlob1;
		
		system.test.startTest();
		
		delete att;
		
		insert att1;
	    insert att2;
	    
		delete att1;
		system.test.stopTest();
		
		
	}

}