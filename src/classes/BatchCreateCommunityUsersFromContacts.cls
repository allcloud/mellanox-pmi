global class BatchCreateCommunityUsersFromContacts implements Database.Batchable<Contact>{
    
     global Iterable<Contact> start(Database.BatchableContext BC) {
    
        list<Contact> theContact_List = [SELECT Id, Academy_Pass__c, Email, user_type_temp__c, LastName FROM Contact 
                                         WHERE Contact_Type__c =: 'Ezchip Design-In customer' and test__C = true and user_craeted__c = false limit 100];

                                                  
        return  theContact_List;                                   
     }
     
     global void execute(Database.BatchableContext BC, List<Contact> scope) {
        
         Profile thePro = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox Ezchip Design In Community User'];    
         
         system.debug('theContact_List ' + scope);
            
        list<User> users2Create_List = new list<User>();
        
        for (Contact TheContact : scope) {
            
            User newUser = new User( userName = TheContact.Email, Email = TheContact.Email, LastName=TheContact.LastName, 
                                    Alias=TheContact.LastName.substring(0,1), 
                                    TimeZoneSidKey='America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', 
                                    LocaleSidKey='en_US', ContactId= TheContact.Id);
            
             newUser.ProfileId = thePro.id;         
                        
            String User_Str = (String)JSON.serialize(newUser);          
                        
             cls_Create_User.insertUser(User_Str, null, true, false);    
              //  newUser.ProfileId = theProOEM.id;
            
            theContact.user_craeted__c = true;
         }
         
         update scope;
     }
     
     global void finish(Database.BatchableContext BC) {
           
     }

}