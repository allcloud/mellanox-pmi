public class VF_FinisarEventTrue_Banner {
	
	public RMA__c theRMA {get;set;}
	public boolean isFinisarEventExists {get;set;}
	
	public VF_FinisarEventTrue_Banner(ApexPages.standardController controller) {
		
		isFinisarEventExists = false;
		Id rmaId = controller.getId();
		
		theRMA = [SELECT Id, (SELECT Id, Finisar_Event__c FROM Serial_Numbers__r WHERE Finisar_Event__c = true) FROM RMA__c WHERE Id =: rmaId];
		
		if (!theRMA.Serial_Numbers__r.isEmpty()) {
			
			isFinisarEventExists = true;
		}
	}
}