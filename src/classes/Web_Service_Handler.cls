global class Web_Service_Handler {
	
	global class ContractRequest {
	  	
	  	webservice String ContractId;  // Actually represents the Contract's Name
	  	webservice String paymentStatus;
	  	webservice String transactionId;
	}
	 
	webservice static String updateContractUponRenewal(ContractRequest request) {
	 	
	 	list<SObject> objects2Update_List = new list<SObject>();
	 	
	 	system.debug('request.ContractId : ' + request.ContractId);
	 	
	   // Contract2__c contract2Update = new Contract2__c(Id = request.ContractId);
	    Contract2__c contract2Update = [SELECT Id FROM Contract2__c WHERE Name =: request.ContractId];
	    contract2Update.Payment_Status__c = request.paymentStatus;
	    contract2Update.Transaction_Id__c = request.transactionId;
	    contract2UPdate.Pending_PO__c = false;
	    contract2UPdate.Pending_Paypal_Approval__c = false;
	    contract2UPdate.Stage__c = 'Closed Won';
	    contract2UPdate.Close_date__c = date.today();
	    contract2UPdate.Renewal_Stat__c = 'Renewed';
	    
	    objects2Update_List.add(contract2Update);
	    
	    try {
	    	
	    	update objects2Update_List;
	    }
	    
	    catch(Exception e) {
	    	
	    	return 'Failure';
	    }
	    
	    return 'Success';
	}
}