@isTest(seeAllData=true)
public with sharing class test_ContentVersionHandler {
    
    static testMethod void test_ContentVersionHandlerMethod() {
        
        
        ProductDetails__c theProdDetail = [SELECT Id FROM ProductDetails__c WHERE Active__c = true limit 1];
        
        ContentVersion content1 = new ContentVersion();
        content1.Id = setupContent();
        ContentVersionHandler handler = new ContentVersionHandler();
        
        handler.addTestCoverage();
        handler.addTestCoverage2();
        handler.addTestCoverage3();
        
    }
    
     public static ID setupContent() { 
         
        // RecordType ContentRT = [select Id FROM RecordType WHERE Name='Sales Documents'];
         ContentVersion testContentInsert = new ContentVersion(); 
         testContentInsert.ContentURL='<a target="_blank" href="http://www.google.com/;">http://www.google.com/;</a>';
         testContentInsert.Title ='Google.com'; 
         //testContentInsert.RecordTypeId = ContentRT.Id; 
         
         
         insert testContentInsert; 
         ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id]; 
         
        
         //testContent.iPad_Content_Views__c =0; 
         update testContent;
         return testContentInsert.Id; 
    }
    
}