public with sharing class VF_Attach_existing_RM {
	
	public String rmCaseNum_Str {get;set;}
	public boolean showSaveButton{get;set;}
	
	public VF_Attach_existing_RM() {
		
		showSaveButton = true;
	}
	
	// This method will create a new RM Case under the related Case and sets the Exist in Redmine to TRUE - to hide the RM Case tabs and CB's for these records
	public void save() {
		
		Id caseId = ApexPages.CurrentPage().getParameters().get('caseid');
		RmCase__c theRmaCase = new RmCase__c();
		theRmaCase.Exist_in_Redmine__c = true;  
		theRmaCase.sfcase__c = CaseId;
		
		String caseNumUrl = 'http://redmine.mtl.com/issues/' + rmCaseNum_Str;
		theRmaCase.RM_Case__c = caseNumUrl;
		theRmaCase.Name = rmCaseNum_Str;
		
		Database.SaveResult sr = Database.insert(theRmaCase, false);      
       
	}
}