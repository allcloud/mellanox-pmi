global class VF_Satisfaction_Survey {
	
	public Id caseId {get;set;}
	public String  SatisfactionResult_Str {get;set;}
	public Case theCase {get;set;}
	public String customerComment {get;set;}
	
	public VF_Satisfaction_Survey() {
		
		caseId = ApexPages.currentPage().getParameters().get('Id');
		SatisfactionResult_Str = ApexPages.currentPage().getParameters().get('sat');
	
	}
	
	public void updateTheCase() {
		
		theCase = new Case(Id = caseId);   
		
		if (SatisfactionResult_Str != null) {
			
			if (SatisfactionResult_Str.EqualsIgnoreCase('1')) {
				
				theCase.Satisfaction_Result__c = 'Extremely satisfied';
			}
			
			else if (SatisfactionResult_Str.EqualsIgnoreCase('2')) {
				
				theCase.Satisfaction_Result__c = 'Satisfied';
			}
			
			else if (SatisfactionResult_Str.EqualsIgnoreCase('3')) {
				
				theCase.Satisfaction_Result__c = 'Moderately satisfied';
			}
			
			else {
				
				theCase.Satisfaction_Result__c = 'Not satisfied';
			}
		}
		update theCase;
	}
	
	public PageReference updateCustomerComment() {
		
		theCase.Customer_Comment__c = customerComment;
		update theCase;
		Pagereference thePage = new Pagereference('https://mymellanox.force.com/support/SupportLogin');  // TBD later on
   		thePage.setRedirect(true);
    	return thePage;
	}

}