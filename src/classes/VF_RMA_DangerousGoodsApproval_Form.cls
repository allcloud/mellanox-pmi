public with sharing class VF_RMA_DangerousGoodsApproval_Form {
	
	public RMA__c MainRMA {get;set;}
	// Boolean to determine if the main form section will be displayed
	public boolean displayform{get;set;}
	// Boolean to determine if the thanks you section will be displayed
	public boolean displayThankYou{get;set;}
	// String that will be displayed if an error accured
	public String ErroMessage{get;set;}
	
	
	// This is a Controller for the Pgae
	public VF_RMA_DangerousGoodsApproval_Form(){    
		ErroMessage = '';  
		MainRMA = new RMA__c();   
		displayform = true;
		displayThankYou = false;  
		//Id rmaId = ApexPages.currentPage().getParameters().get('RMAId');
		//RMA__c MainRMA = new RMA__c(Id = rmaId);
	}
	
	// This method will update the RMA Object with the relevant fields inserted by the user
	// abd will display an error message to the user if the update has failed
	public void submitConsent(){
		
		    // We're fetching the RMA Id from the URL so we can update this record
			Id rmaId = ApexPages.currentPage().getParameters().get('RMAId');
		    MainRMA.Id = rmaId;
		   
		    try{
		    	
				update MainRMA;
				displayform = false;
				displayThankYou = true;
		    }
		    
		    catch(Exception e){
		    	
		    	ErroMessage = 'This RMA no longer exists, please contact Mellanox support for more information';
		    }
	}

}