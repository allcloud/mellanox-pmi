public class VF_AdminQ_Cases {
    
    public Case currentCase {get;set;}
    public Contact currentContact {get;set;}
    public Distributor_Oppy_Registrations__c oppy {get;set;}
    public boolean displayTerritoryPickList {get;set;}
    public boolean displayContactPanel {get;set;}
    public list<SelectOption> regions_List {get;set;}
    public list<SelectOption> ApplicationReasonSelection_List {get;set;}
    public String selectedAppReason {get;set;}
    public String selectedRegion {get;set;}
    public boolean displayFirstSection {get;set;}
    public boolean displayContactDeletedSection {get;set;}
    public boolean displayWholePage {get;set;}
    public String iframeSrc {get;set;}
    
    // Contacts fields as Variables as Site user doesn't have permission for Edit
    public String FirstName {get;set;}
     public String LastName {get;set;}
    public String ContactCompanyEmail {get;set;}
    public String ContactPhone {get;set;}
    public String ContactMobilePhone {get;set;}
    public String AccountName {get;set;}
     
    public list<SelectOption> PurchasingPartnerDetails_List {get;set;}
    public String SlectedPurchasingPartnerDetails {get;set;}
    public String RelatedProductSN {get;set;}
    public String AdditionalComments {get;set;}
    
   
    public VF_AdminQ_Cases() {   
        
     try {
        oppy = new Distributor_Oppy_Registrations__c();
        String thankYouMessage = Apexpages.currentPage().getParameters().get('Message');
        if (String.isNotBlank(thankYouMessage)) {
            
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Thank you for providing your details, Mellanox support team will contact you shortly'));
            return;
        }
        
        displayWholePage = true;
        displayContactPanel = false;
        displayFirstSection = true;
        displayContactDeletedSection = false;
        Id CaseId = Apexpages.currentPage().getParameters().get('caseId');
        Id contactId = Apexpages.currentPage().getParameters().get('contactId');
        currentCase = new Case(Id = CaseId);
        system.debug('contactId : ' + contactId);
        try {   
            currentContact = [SELECT Id FROM Contact WHERE Id =: contactId];
             system.debug('currentContact : ' + currentContact);
        }
        
        catch(Exception e) {
            
            displayFirstSection = false;
            displayContactDeletedSection = true;
        }
        currentContact = new Contact(Id = contactId);
 
        //iframeSrc = 'http://test-mellanox.cs2.force.com/' + 'VF_AdminQ_Contacts?caseId=' + currentCase.Id + '&contactId=' + currentContact.Id; // Sandbox
        iframeSrc = 'http://mellanox.force.com/' + 'VF_AdminQ_Contacts?caseId=' + currentCase.Id + '&contactId=' + currentContact.Id; // Production
        
        system.debug('iframeSrc : ' + iframeSrc);
        
        displayTerritoryPickList = false;
     }
     
     catch(Exception e) {
     	
     	
     }
    }
    
    
   
    public pageReference chooseFromMainPickList() {
        
        pageReference newPage;
        system.debug('selectedAppReason : ' + selectedAppReason);
        if (selectedAppReason != null && !selectedAppReason.EqualsIgnoreCase('None')) {
            
            currentCase.Application_Reason__c = selectedAppReason;
            if (selectedAppReason.EqualsIgnoreCase('Sales/POC related inquiry')) {
                
                update currentCase;
                displayTerritoryPickList = true;
                displayContactPanel = false;
            }
            
            else if (selectedAppReason.EqualsIgnoreCase('RMA for products hardware malfunction')) {
                selectedAppReason = 'None';
                displayTerritoryPickList = false;
                currentCase.State__c = 'Closed';
                currentCase.Redirect_Case__c = null;
                String openerUrl = 'http://www.mellanox.com/page/rma_form';
                PageReference nextPage = new PageReference(openerUrl);        
                nextPage.setRedirect(true);
                displayContactPanel = false;
                update currentCase;
                return nextPage;
            }
            
            else if (selectedAppReason.EqualsIgnoreCase('Missing product Documentation')) {
                selectedAppReason = 'None';
                displayTerritoryPickList = false;
                currentCase.State__c = 'Closed';
                currentCase.Redirect_Case__c = null;
                String openerUrl = 'https://mymellanox.force.com/support/VF_SerialSearch';
                PageReference nextPage = new PageReference(openerUrl);        
                nextPage.setRedirect(true);
                displayContactPanel = false;
                update currentCase;
                return nextPage;
            }
            
            else {
                
                displayTerritoryPickList = false;
                displayContactPanel = true;
                currentCase.Redirect_Case__c = null;
                currentCase.Application_Reason__c = null;
                try {	
                	update currentCase;
                }
                
                catch(Exception e) {
                	
                	displayContactPanel = false;
                	Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Case cannot be updated 128 : ' + e.getMessage()));
                	return null;
                }
            }
        }
        
         else {
            
            displayTerritoryPickList = false;
            displayContactPanel = false;
        }   
        return null;
    }
    
    public list<selectOption> getRegionList() {
        
        regions_List = new list<SelectOption>();
        regions_List.add( new SelectOption('US', 'US') );
        regions_List.add( new SelectOption('EMEA', 'EMEA') );
        regions_List.add( new SelectOption('APAC', 'APAC') );
        
        return regions_List;    
    }
    
     
    public list<selectOption> getAppReasonList() {
        
        ApplicationReasonSelection_List = new list<SelectOption>();
        ApplicationReasonSelection_List.add( new SelectOption('None', 'None') );
        ApplicationReasonSelection_List.add( new SelectOption('Sales/POC related inquiry', 'Sales/POC related inquiry') );
        ApplicationReasonSelection_List.add( new SelectOption('RMA for products hardware malfunction', 'RMA for products hardware malfunction') );
        ApplicationReasonSelection_List.add( new SelectOption('Missing product Documentation', 'Missing product Documentation') );
        ApplicationReasonSelection_List.add( new SelectOption('Technical support inquiry', 'Technical support inquiry') );
          
        return ApplicationReasonSelection_List;    
    }
    
    public list<selectOption> getThePurchasingPartnerDetails() {
        
        PurchasingPartnerDetails_List = new list<SelectOption>();
        PurchasingPartnerDetails_List.add( new SelectOption('None', 'None') );
        PurchasingPartnerDetails_List.add( new SelectOption('Dell', 'Dell') );
        PurchasingPartnerDetails_List.add( new SelectOption('IBM', 'IBM') );
        PurchasingPartnerDetails_List.add( new SelectOption('Intel', 'Intel') );
        PurchasingPartnerDetails_List.add( new SelectOption('Oracle', 'Oracle') );
        PurchasingPartnerDetails_List.add( new SelectOption('HP', 'HP') );
        PurchasingPartnerDetails_List.add( new SelectOption('Supermicro', 'Supermicro') );
        PurchasingPartnerDetails_List.add( new SelectOption('Reseller/distributer', 'Reseller/distributer') );
        PurchasingPartnerDetails_List.add( new SelectOption('Other', 'Other') );        
        
        return PurchasingPartnerDetails_List;    
    }
    
    public void selectFromRegionList() {
        
        Schema.sObjectType objType = Case.getSObjectType(); 
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
        // Get a map of fields for the SObject
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values =
         fieldMap.get('Redirect_Case__c').getDescribe().getPickListValues();
        // Add these values to the selectoption list.
      
       system.debug('values :'  + values);
       
        for (Schema.PicklistEntry value  : values) {
            
            if ( !selectedRegion.EqualsIgnoreCase('US') && value.getLabel().containsIgnoreCase(selectedRegion)) {
            
                currentCase.Redirect_Case__c = value.getLabel();
            }
            
            else if (selectedRegion.EqualsIgnoreCase('US')) {
                
                currentCase.Redirect_Case__c = 'Inside Sales (gerryh@mellanox.com)';
            }
        }
        
        
        currentCase.State__c = 'Closed';
        displayTerritoryPickList = true;
        
        try {
            update currentCase;
        }
        
        catch (Exception e) {
            
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Case cannot be updated 221 : ' + e.getMessage()));
            return;
        }
        displayWholePage = false;
        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Case has been updated'));
        return;
    }
    
    
    public pageReference updateContactInfo() {
      	
      	if (String.isEmpty(FirstName) || String.isEmpty(LastName) || String.isEmpty(ContactCompanyEmail) || String.isEmpty(ContactPhone) ||
      		String.isEmpty(AccountName)) {
      			
      			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'All the mandatory fields must be filled in'));
                return null;
  		}
      	
      	
      	
        currentContact.FirstName = FirstName;
        currentContact.LastName = LastName;
        currentContact.Email = ContactCompanyEmail;
        currentContact.Phone = ContactPhone;
        currentContact.MobilePhone = ContactMobilePhone;
        currentContact.Company_Name_from_Web__c = AccountName;
        currentContact.Product_Purchase_Date__c = oppy.Approval_Date__c;
        currentContact.purchase_Mellanox_equipment_from__c = SlectedPurchasingPartnerDetails;
        currentContact.Serial_Number__c = RelatedProductSN;
        currentContact.Comment__c = AdditionalComments;
        
        
         Account accountByDomain; 
         boolean foundAccountByDomain = true;
        
        if (!String.isEmpty(SlectedPurchasingPartnerDetails) && SlectedPurchasingPartnerDetails.EqualsIgnoreCase('Dell')) {
            
            if (String.isEmpty(RelatedProductSN)) {
                
                 Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Serial Number must be filled in'));
                 return null;
            }
           
            currentCase.Reseller_Dell_Partner__c = '***Technical form for DELL product submitted***';   
        }
         
        AssignmentRule ar = new AssignmentRule();
        ar = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= ar.id;
        
        cls_Contact_Trigger handler = new cls_Contact_Trigger();
        
        system.debug('currentContact : ' + currentContact);
        if ( String.isNotBlank(currentContact.FirstName) && String.isNotBlank(currentContact.LastName) && String.isNotBlank(currentContact.Email)
            && String.isNotBlank(currentContact.Phone) && String.isNotBlank(currentContact.Company_Name_from_Web__c) ) {
                
                Public_Email_Domains__c globalDomain = Public_Email_Domains__c.getValues('Global Emails');
                
                 
                String domainName = currentContact.Email.substring(currentContact.Email.indexOf('@') + 1);
                String domainNameNoDot = '';
                String domainNameOneDot = '';
                set<String> domainNames_Set = new set<String>();
                
                String[] parts = domainName.split('[.]');
                domainNameNoDot = parts[0];
                domainNames_Set.add(domainNameNoDot);
                
                if (parts.size() > 2)
                {
                    String secondtSection = parts[1];
                    domainNameOneDot = domainNameNoDot + '.' + secondtSection;
                    domainNames_Set.add(domainNameOneDot);
                }
                
                
                for (String domain : domainNames_Set) {
                
                    if (globalDomain != null && globalDomain.Value__c != null && globalDomain.Value__c.contains(domain)) {
                    
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Please provide your company Email address'));
                        return null;
                    }
                }
                
                Contact foundContact;   
                try {       
                    
                    foundContact = [SELECT Id, Serial_Number__c, MobilePhone, Phone, Email FROM Contact WHERE Email =: ContactCompanyEmail];
                }
                
                catch(exception ex) {
                    
                    foundAccountByDomain = true;
                                  
                    try {
                        
                        accountByDomain = handler.getAccountByEmailDomain(currentContact.Email, null); 
                    }
                    
                    catch (Exception e){
                        
                        foundAccountByDomain = false;
                    }
                    
                    if (accountByDomain != null && accountByDomain.id != null) {  
                        
                        currentContact.AccountId = accountByDomain.Id;
                    }
                     
                     
                     
                    try {   
                        update currentContact;
                    }
                    
                    catch (Exception ex2) {
                        
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Contact cannot be updated : ' + ex2.getMessage()));
                        return null;
                    }
                    
                    currentCase.State__c = 'Open';
                    currentCase.Application_Reason__c = 'Technical support inquiry';
                    currentCase.Contact_Details_Provided__c = true;
                    currentCase.AccountID = currentContact.AccountId;
                    currentCase.setOptions(dmlOpts);

                    try {
                        update currentCase;
                    }
                    
                    catch (Exception ex1) {
                        
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Case cannot be updated 357: ' + ex1.getMessage()));
                        return null;
                    }
                    
                    
                    displayWholePage = false;
                    PageReference currentPage = System.currentPageReference();
                    String theUrl = '/apex/VF_AdminQ_Cases?caseId=' + currentCase.Id + '&message=ThankYou';
                    system.debug('theUrl : ' + theUrl);
                    PageReference nextPage = new Pagereference(theUrl);
                    nextPage.setRedirect(true);
                    return nextPage;
                }
                
                
               displayWholePage = false;
                
               system.debug('currentContact : ' + currentContact);
               
               try {
                    
                    accountByDomain = handler.getAccountByEmailDomain(foundContact.Email, null); 
                  }
                  
                  catch (Exception e){
                    
                    foundAccountByDomain = false;
                  }
                  
                  if (foundAccountByDomain && accountByDomain != null && accountByDomain.Id != null) {  
                    
                    foundContact.AccountId = accountByDomain.Id;
                  }
             
             try {
                
                 if (currentContact.Id == foundContact.Id) {
                 	
                 	foundContact.FirstName = currentContact.FirstName;
	                 foundContact.LastName = currentContact.LastName;
	                 foundContact.Email = currentContact.Email;
	                 foundContact.Phone = currentContact.Phone;
	                 foundContact.MobilePhone = currentContact.MobilePhone;
	                 foundContact.Company_Name_from_Web__c = currentContact.Company_Name_from_Web__c;
	                 foundContact.Product_Purchase_Date__c = currentContact.Product_Purchase_Date__c;
	                 foundContact.purchase_Mellanox_equipment_from__c = currentContact.purchase_Mellanox_equipment_from__c;
	                 foundContact.Serial_Number__c = currentContact.Serial_Number__c;
	                 foundContact.Comment__c = currentContact.Comment__c;
	                 system.debug('foundContact 407 : ' + foundContact);
	                 update  foundContact;
                 }
                 
                 else {
	                
	                 currentContact.Email_for_merge__c = currentContact.Email;
	                 currentContact.Contact_Need_to_be_merged__c = true;   
	                 currentContact.Email = currentContact.Email + 'Temp';
	                 system.debug('currentContact.Email_for_merge__c : ' + currentContact.Email_for_merge__c);
	                 update currentContact;
	                
	                
	                 foundContact.FirstName = currentContact.FirstName;
	                 foundContact.LastName = currentContact.LastName;
	                 foundContact.Email = currentContact.Email;
	                 foundContact.Phone = currentContact.Phone;
	                 foundContact.MobilePhone = currentContact.MobilePhone;
	                 foundContact.Company_Name_from_Web__c = currentContact.Company_Name_from_Web__c;
	                 foundContact.Product_Purchase_Date__c = currentContact.Product_Purchase_Date__c;
	                 foundContact.purchase_Mellanox_equipment_from__c = currentContact.purchase_Mellanox_equipment_from__c;
	                 foundContact.Serial_Number__c = currentContact.Serial_Number__c;
	                 foundContact.Comment__c = currentContact.Comment__c;
	                  
	                 update  foundContact;
                 }
             }
             
             catch(Exception e) {
                
             }
             
             
                currentCase.State__c = 'Open';
                currentCase.Application_Reason__c = 'Technical support inquiry';
                currentCase.Contact_Details_Provided__c = true;
                currentCase.AccountID = foundContact.AccountId;
                    
                try {   
                    currentCase.setOptions(dmlOpts);
                     update currentCase;
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Your details have been updated sucessfully'));
                    PageReference currentPage = System.currentPageReference();
                    String theUrl = '/apex/VF_AdminQ_Cases?caseId=' + currentCase.Id + '&message=ThankYou';
                    system.debug('theUrl : ' + theUrl);
                    PageReference nextPage = new Pagereference(theUrl );
                    nextPage.setRedirect(true);
                    return nextPage;
                }
                
                catch(Exception e) {
                    
                    system.debug('ERROR : ' + e.getMessage());
                    String theUrl = '/apex/VF_Error_Message_Page';
                    PageReference nextPage = new Pagereference(theUrl );
                    nextPage.setRedirect(true);
                    return nextPage;
                }
            }
            
        else {
            
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'All the required fields should be filled up'));
            return null;
        }
        
        return null;
    }
}