@isTest(seeAllData=false)
private class TestFederatedSearch{
    private static JiveCommunitySettings__c jiveSetting;
    private static DefaultFederatedSearchSettings__c federatedSearchSetting;
    
    private static ApexPages.StandardController sc;
    private static Case testCase;
    private static Case testCase1;
    private static Solution testSolution;
    
    //Populate predefined SOSL search resultset.
    private static List<Id> staticIdResultSetForSOSL = new List<Id>();
    
     public static void initCustomSettings() {
     
      // make sure custom setting record exists
      jiveSetting = JiveCommunitySettings__c.getValues('DefaultSetting');
        
        //put dummy values in the custom setting if jiveSetting == NULL
        if(jiveSetting == NULL) {
            jiveSetting = new JiveCommunitySettings__c();
            jiveSetting.Name = 'DefaultSetting';
            jiveSetting.Community_Base_URL__c ='http://cs-jive-sfdc-connector.com:5000';
            jiveSetting.Create_Contact__c =true;
            jiveSetting.Jive_URL__c='http://jivedemo-cs.jivecustom.com';
            insert jiveSetting;
       
        }
        else{
              // to cover user synchronization feature
             jiveSetting.Create_Contact__c =true;
             update jiveSetting;
        
        }
       
     federatedSearchSetting=DefaultFederatedSearchSettings__c.getValues('DefaultSetting');
        if(federatedSearchSetting == NULL){ 
            federatedSearchSetting= new DefaultFederatedSearchSettings__c();
            federatedSearchSetting.Name='Default Federated Search Filters' ;
            federatedSearchSetting.Solution__c=true;
            federatedSearchSetting.Document__c=true;
            federatedSearchSetting.Discussion__c=true;
            federatedSearchSetting.Case__c=true;
            federatedSearchSetting.Blog__c=true;
            insert federatedSearchSetting;
         
          }
          else{
          
            federatedSearchSetting= new DefaultFederatedSearchSettings__c();
            
            federatedSearchSetting.Solution__c=true;
            federatedSearchSetting.Document__c=true;
            federatedSearchSetting.Discussion__c=true;
            federatedSearchSetting.Case__c=true;
            federatedSearchSetting.Blog__c=true;
            update federatedSearchSetting;
          }
          
     
     
    }
    
    
    //insert RelatedContent__c
    public static void RelatedContent(){
    
      List<RelatedContent__c> insertRelatedContent = new List<RelatedContent__c>(); 
       
    
    
    
    }
    
    
    //insert cases
    public static void insertCase(){
    
        List<Case> insertCases_list = new List<Case>(); 
        insertCases_list.add(new Case(Subject='testCaseSubject1',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName1',Jive_Author_Email__c='jiveAuthor1@cs.com',JiveURL__c='https://demojive-community1.jiveon.com/thread/1',Description='caseSummary1'));
        insertCases_list.add(new Case(Subject='testCaseSubject2',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName2',Jive_Author_Email__c='jiveAuthor2@cs.com',JiveURL__c='https://demojive-community2.jiveon.com/thread/2',Description='caseSummary2'));
        insertCases_list.add(new Case(Subject='testCaseSubject3',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName3',Jive_Author_Email__c='jiveAuthor3@cs.com',JiveURL__c='https://demojive-community3.jiveon.com/thread/3',Description='caseSummary3'));
        insertCases_list.add(new Case(Subject='testCaseSubject4',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName4',Jive_Author_Email__c='jiveAuthor4@cs.com',JiveURL__c='https://demojive-community4.jiveon.com/thread/4',Description='caseSummary4'));
        insertCases_list.add(new Case(Subject='testCaseSubject5',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName5',Jive_Author_Email__c='jiveAuthor5@cs.com',JiveURL__c='https://demojive-community5.jiveon.com/thread/5',Description='caseSummary5'));
        insertCases_list.add(new Case(Subject='testCaseSubject6',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName6',Jive_Author_Email__c='jiveAuthor6@cs.com',JiveURL__c='https://demojive-community6.jiveon.com/thread/6',Description='caseSummary6'));
       
        insert insertCases_list;
        for(Case c: insertCases_list){
            if(String.isNotBlank(c.id)){
                staticIdResultSetForSOSL.add(c.id);
            }
        }
      
        
    }

    public static void insertSolution(){
    
    
    //Id,SolutionName,LastModifiedDate,SolutionNote,CreatedById
    List<Solution> insertSolution_list = new List<Solution>();
    insertSolution_list.add(new Solution(SolutionName = 'testSolutionName1 searchText',SolutionNote= 'testSolutionNote1'));
    insertSolution_list.add(new Solution(SolutionName = 'testSolutionName2 searchText', SolutionNote= 'testSolutionNote2'));
    insertSolution_list.add(new Solution(SolutionName = 'testSolutionName3 searchText',SolutionNote= 'testSolutionNote3'));
    insert insertSolution_list;
    
     for(Solution  sol: insertSolution_list){
        if(String.isNotBlank(sol.id)){
                staticIdResultSetForSOSL.add(sol.id);
            }
          }
     }
    
    public static void insertTestData(){
       //insert Cases
       insertCase();
       //Insert Solutions
       insertSolution();
    }
    public static testMethod void testFederatedSearchFeature() {
    
      //initailize custom settings
      initCustomSettings();
      insertTestData(); 
      testCase=new Case(Subject='testCaseSubject11',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName11',Jive_Author_Email__c='jiveAuthor1@cs.com',JiveURL__c='https://demojive-community1.jiveon.com/thread/11');
      testCase1=new Case(Subject='testCaseSubject22',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName22',Jive_Author_Email__c='jiveAuthor1@cs.com',JiveURL__c='https://demojive-community1.jiveon.com/thread/22');
      insert testCase; 
      insert testCase1;  
      
      system.debug('testCase1.Id'+testCase1.Id);
      sc = new ApexPages.StandardController(testCase);
       JiveFederatedSearchCtrl controllerObj= new  JiveFederatedSearchCtrl(sc);
      
      controllerObj.pageCountCon = 10;  
      //select filters 
      controllerObj.dateFilter='all';  
      controllerObj.author='';  
      controllerObj.dateFilter='all';  
      controllerObj.typeOfDiss='discussion';
      controllerObj.typeOfDiss='queWithAns';
      controllerObj.blog=true;
      controllerObj.document=true;
      //controllerObj.kA=true;
      controllerObj.cs=true;
      controllerObj.sol=true;
      controllerObj.space=true;
      controllerObj.grp=true;
      controllerObj.searchText='searchText';
      controllerObj.isSearchFromCasePage=true;
      //This std SF method returns either all records specified by ids in list or subset of them(which satisfy the where and limit clauses if exist in SOSL)
      Test.setFixedSearchResults(staticIdResultSetForSOSL);
      controllerObj.performSearch();
      controllerObj.linkContentToCase();
      controllerObj.getFilter();
      controllerObj.getItems();
      controllerObj.getConPageNumbers();
      controllerObj.pageSize=5;
      controllerObj.pageChangedCon();
      controllerObj.getPageSize();
      controllerObj.getPageLst();
      controllerObj.checkIfPageSizeChanged();
    
       //----------------------------------------------------------------------------------------------------------
      //set author filter here 
      JiveFederatedSearchCtrl controllerObj1= new  JiveFederatedSearchCtrl(sc);
      controllerObj1.pageCountCon = 10;  
      //select filters 
      //query from users object ...and put some/current user here
      controllerObj.author='CsPhaseTwo Dev';
      controllerObj1.dateFilter='week';  
      controllerObj1.typeOfDiss='discussion';
      controllerObj1.typeOfDiss='queWithAns';
      controllerObj1.blog=true;
      controllerObj1.document=true;
      controllerObj1.cs=true;
      controllerObj1.sol=true;
      controllerObj1.space=true;
      controllerObj1.grp=true;
      controllerObj1.searchText='search';
       controllerObj1.cse.parentId =  testCase1.id;
      //This std SF method returns either all records specified by ids in list or subset of them(which satisfy the where and limit clauses if exist in SOSL)
      Test.setFixedSearchResults(staticIdResultSetForSOSL);
      controllerObj1.performSearch();
      controllerObj1.linkContentToCase();
       
      //When no filters are selected
        JiveFederatedSearchCtrl controllerObj2= new  JiveFederatedSearchCtrl(sc);
      
      controllerObj2.pageCountCon = 10;  
      //select filters 
      //query from users object ...and put some/current user here
       controllerObj2.typeOfDiss=NULL;
      controllerObj2.isAtleastOneFilterSelected=false;
      //This std SF method returns either all records specified by ids in list or subset of them(which satisfy the where and limit clauses if exist in SOSL)
      Test.setFixedSearchResults(staticIdResultSetForSOSL);
      controllerObj2.performSearch();
      controllerObj2.linkContentToCase();
     
     
        JiveFederatedSearchCtrl controllerObj3= new  JiveFederatedSearchCtrl(sc);
     
      controllerObj3.pageCountCon = 10;  
      
      controllerObj3.dateFilter='month';
      Test.setFixedSearchResults(staticIdResultSetForSOSL);
      controllerObj3.performSearch();
      
     //to cover Wrapper Class
     JiveFederatedSearchCtrl.JiveAddonResponseWrapper resposeWrp=new JiveFederatedSearchCtrl.JiveAddonResponseWrapper();
     controllerObj3.clearAllFilters();
    
      JiveFederatedSearchCtrl controllerObj4= new  JiveFederatedSearchCtrl(sc);
     
      controllerObj4.pageCountCon = 10;  
      controllerObj4.dateFilter='year';
      controllerObj4.performSearch();
      controllerObj4.pageAction();
    }
}