@isTest(seeAllData=true)
public class test_FMDiscussionNew_Project {
	
	static testMethod void test_theVF() {
		 CLS_ObjectCreator obj = new CLS_ObjectCreator();
	         
	        Account acc = obj.createAccount();
	        acc.Name = 'unique test account for testing';
	        insert acc;
	         
	        Milestone1_Project__c proj = obj.CreateMPproject(acc.Id);
	        insert proj;
	        
	        Project_FM_Discussions__c discussion = new Project_FM_Discussions__c();
	        
	        discussion.Comment__c = 'test Comment';
	        discussion.Project__c = proj.Id;
	        discussion.Public__c = true;
	        discussion.Discussion__c = 'test Discussion';    
	        insert discussion;
	          
	        ApexPages.StandardController sc = new ApexPages.standardController(proj);
	        ApexPages.currentPage().getParameters().put('Id', proj.Id);     
	        FMDiscussionNew_Project controller =new FMDiscussionNew_Project(sc);
	        
	        FMDiscussionNew_Project.FMDiscussionWrapper wrapper = new FMDiscussionNew_Project.FMDiscussionWrapper(discussion);
	        wrapper.IsEditMode = true;
	        wrapper.CommentText = 'test';
	        wrapper.ISPublic = true;
	        wrapper.ISPublicRM1 = true;
	        wrapper.ISPublicRM2 = true;
	        wrapper.ISPublicRM3 = true;
	        
	        controller.searchFolderTemplates();
	       
	        controller.SelectedTab = 'Public';
	        controller.searchComments();
	        controller.SelectedTab = 'Private';
	        controller.searchComments();
	        controller.SelectedTab = 'RM1';
	        controller.searchComments();
	        controller.SelectedTab = 'RM2';
	        controller.searchComments();
	        controller.SelectedTab = 'RM3';
	        controller.searchComments();
	        controller.selectedTemplate = 'template';
	        controller.selectedTemplate();
	        controller.SelectedCommentId = discussion.Id;
	        controller.map_FMDiscussionWrapper.put(discussion.Id,wrapper );
	        controller.UpdatePublicORPrivate();
	        controller.UpdatePublicORPrivatePrivate();
	        controller.UpdatePublicORPrivatePublic();
	        controller.UpdatePrivate();
	        controller.SelectedCommentId = discussion.Id;
	        controller.map_FMDiscussionWrapper.put(discussion.Id,wrapper );
	        controller.UpdatePublic();
	        controller.DeleteComment();
	        controller.SelectedCommentId = discussion.Id;
	        controller.map_FMDiscussionWrapper.put(discussion.Id,wrapper );
	        controller.SaveChangeCom();
	        controller.SelectedCommentId = discussion.Id;
	        controller.map_FMDiscussionWrapper.put(discussion.Id,wrapper );
	        controller.SaveChanges();
	        controller.SelectedCommentId = discussion.Id;
	        controller.map_FMDiscussionWrapper.put(discussion.Id,wrapper );
	        controller.CancelChanges();
	        controller.NextPage();
	        controller.PreviousPage();
	        controller.FirstPage();
	        controller.LastPage();
	        controller.FMDiscussion = 'test discussion test test';
	        controller.getPage();
	}

}