public with sharing class CaseFM_ComponentController{
    
public Id caseId {get; set;}
public string CaseNumber {get; set;}

public cFMComments[] comments{
        get{
            List<cFMComments> comments = new List<cFMComments>();
            for(FM_Discussion__c comment : [Select LastModifiedDate, LastModifiedBy.Id, LastModifiedBy.Name, DOC_ID__c, CreatedDate, CreatedBy.Id, CreatedBy.Name, Discussion__C, Attachment_Link__c, Public__c, Name From FM_Discussion__c c where Case__c= :caseId order by c.LastModifiedDate desc])
            { 
                cFMComments tempcComment = new cFMComments();
                tempcComment.cFMComment = comment;
                
                // Build String to display.
                tempcComment.commentText = '<b>Created By: <a href=\'/' + comment.CreatedBy.Id + '\'>' + comment.CreatedBy.Name + '</a> (' + comment.CreatedDate.format() + ')</b><br>';
                //tempcComment.commentText += 'Last Modified By: <a href=\'/' + comment.LastModifiedBy.Id + '\'>' +  comment.LastModifiedBy.Name + '</a> (' + comment.LastModifiedDate.format() + ')</b><br>';
                tempcComment.commentText += comment.Discussion__c;
                tempcComment.commentText = tempcComment.commentText.replace('\n', '<br/>');
                tempcComment.commentDisc = comment.Discussion__c;
                tempcComment.commentAttachment=comment.Attachment_Link__c;
                tempcComment.commentPublic=comment.Public__c;
                tempcComment.commentDocId=comment.DOC_ID__c;
                tempcComment.commentName= '<b>#'+ comment.Name + '</b>';
                
                //if(comment.IsPublished)
                 //   tempcComment.PublicPrivateAction = 'Make Private';
               // else
                //    tempcComment.PublicPrivateAction = 'Make Public';
                //Add to list
                comments.add(tempcComment); 
            }
            
            return comments;
        }
        
        set;
    }
  public PageReference NewComment()
    {
        PageReference pr = new PageReference('/a09/e?CF00NS0000000aQpB='+caseId+'&CF00NS0000000aQpB_lkid='+caseId+'&retURL=%2F'+caseId);
        pr.setRedirect(true);
        return pr;
        
    }
    


public PageReference publishComment(){


        // If attachment link not empty publish attachment
 Id FMid = ApexPages.currentPage().getParameters().get('CommentId_d');
 
 
 
 for(FM_discussion__c FMComment :  [Select LastModifiedDate, LastModifiedBy.Id, LastModifiedBy.Name, DOC_ID__c, CreatedDate, CreatedBy.Id, CreatedBy.Name, Discussion__C, Attachment_Link__c, Public__c From FM_Discussion__c c where id= :FMId]) 
 { 
    FMComment.Public__c =true;
    update(FMComment);   
                     
    if(FMComment.Attachment_Link__c!=NULL)
    {
     list<Document> Docs = [select  Id, Name, body from Document where id=:FMComment.DOC_ID__c];
          if( !Docs.isempty())
          { FMComment.Public__c =true;
            update(FMComment);
            Attachment AttNew = new Attachment();
            AttNew.Name=Docs[0].Name;
            AttNew.Body = Docs[0].Body;
            AttNew.IsPrivate = FALSE;
            AttNew.ParentId= CaseId;
            insert AttNew;
          }
    }
    // If empty publish comment
       if(FMComment.Attachment_Link__c==NULL)
        {           
            CaseComment CComNew = new CaseComment();
            CComNew.CommentBody= FMComment.Discussion__C; 
            CComNew.IsPublished = FALSE;
            CComNew.ParentId = CaseId;
            insert CComNew;
            
       }
 }
   
        
        
    
        PageReference pg = System.currentPageReference();
        pg.setRedirect(false);
        return pg;
}
public class cFMComments {
    
        public FM_Discussion__c cFMComment {get; set;}
        public String commentText {get; set;}            
         public String commentDisc {get; set;}     
         public String commentAttachment {get; set;}  
         public Boolean commentPublic {get; set;} 
         public String commentName {get; set;}  
        public String PublicPrivateAction {get; set;}
        public ID commentDocId {get; set;}
    }

}