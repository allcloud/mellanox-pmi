public with sharing class My_Pricebook_Documents {
	public List<Document> docs = new List<Document>();
	public boolean show_PB {get;set;}
	String userID = UserInfo.getUserID();
  	User activeUser = [Select Username,UserType,Show_Pricebook__c From User where ID= : userID limit 1];
  	//public List<String> url_docs {get;set;}
  	String temp_url;
  	public class URL_row {
  		public String name {get; set;}
  		public String link {get; set;}
  	}
  	public List<URL_row> allrows {get;set;}
	public My_Pricebook_Documents(){
		allrows = new List<URL_row>();
		//url_docs = new List<String>();
		docs = [select id,name from Document where Folderid='00l50000001bYo2' order by LastModifiedDate desc];
		show_PB = activeUser.Show_Pricebook__c;
		for (Document d : docs){
			URL_row r = new URL_row();
			r.name = d.Name;
			r.link = '/servlet/servlet.FileDownload?file='+d.Id;
			allrows.add(r);
		}
System.debug('... KN allrows==='+allrows);		
	}
}