public class cls_trg_Fund_Claim {
    
    
    public void sendFundClaimsToApproval(list<SFDC_MDF_Claim__c> newMDFs_List, map<Id, SFDC_MDF_Claim__c> oldMDFs_Map) {
        
        list<SFDC_MDF_Claim__c> MDFsForApproval_List = getListOfMDFsForApproval(newMDFs_List, oldMDFs_Map);
        system.debug('inside 7');
        for (SFDC_MDF_Claim__c mdfForApproval : MDFsForApproval_List) {
            
            // Create an approval request for the Fund Requst
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Sending the fund Requst for approval');
            req1.setObjectId(mdfForApproval.id);
            //req1.setNextApproverIds(new Id[] {mdfForApproval.Next_Approver__c});
            Approval.ProcessResult result;
            // Submit the approval request for the Opportunity
            try {   
                
                result = Approval.process(req1);
            }
            
            catch(Exception e) {
                
                system.debug('Error :' + e.getMessage());
            }
        }
    }
    
    // This method will return a list of fund requests for approval
    private list<SFDC_MDF_Claim__c> getListOfMDFsForApproval(list<SFDC_MDF_Claim__c> newMDFs_List, map<Id, SFDC_MDF_Claim__c> oldMDFs_Map) {
        
        list<SFDC_MDF_Claim__c> MDFsForApproval_List = new list<SFDC_MDF_Claim__c>();
        system.debug('inside 33');
        for (SFDC_MDF_Claim__c newMDF : newMDFs_List) {
            
            if (newMDF.Status__c != null) {
                system.debug('inside 37');
                if (oldMDFs_Map != null) {  
                    if (newMDF.Status__c.EqualsIgnoreCase('Submitted') && newMDF.Status__c != oldMDFs_Map.get(newMDF.Id).Status__c) {
                        system.debug('inside 40');
                        MDFsForApproval_List.add(newMDF);
                    }
                }
                
                else if (newMDF.Status__c.EqualsIgnoreCase('Submitted')) {
                    
                    MDFsForApproval_List.add(newMDF);
                }
            }
        }
        
        return MDFsForApproval_List;
    }
    
    // This method will fill up the 'CAM' and 'TAM' fields when a new Fund Request record is created, according to their Account's country region
    public void updateFundClaimsTAM (list<SFDC_MDF_Claim__c> newMDFs_List) {
        
        map<String, map<Id, String>> countriesAndStates2AccountsMap_Map = getMapOfAccountsAndTheirBillingCountries(newMDFs_List);
        //map<Id, String> accounts2CoutryCodes_Map = getMapOfAccountsAndTheirBillingCountries(newMDFs_List);
         
        
        map<Id, String> accounts2Countries_Map = new map<Id, String>();
        map<Id, String> accounts2StatesCodes_Map = new map<Id, String>();
        
        if (countriesAndStates2AccountsMap_Map.containsKey('BillingCountryCode')) {
            
            accounts2Countries_Map = countriesAndStates2AccountsMap_Map.get('BillingCountryCode');
        }
        
        if (countriesAndStates2AccountsMap_Map.containsKey('BillingStateCode')) {
            
            accounts2StatesCodes_Map = countriesAndStates2AccountsMap_Map.get('BillingStateCode');
        }
        
        map<String, Id> countryCodes2TAM_Map = getMapOfCountriesAndTAM(accounts2Countries_Map);
        map<String, Id> stateCodes2TAM_Map = getMapOfStatesAndTAM(accounts2StatesCodes_Map);
        map<String, Id> countryCodes2CAM_Map = getMapOfCountriesAndCAM(accounts2Countries_Map);
        map<String, Id> stateCodes2CAM_Map = getMapOfStatesAndCAM(accounts2StatesCodes_Map);
         
        
        for (SFDC_MDF_Claim__c newMDF : newMDFs_List) {
            
            if ( accounts2Countries_Map.containsKey(newMDF.Account__c) ) {  
                
                String countryCode_Str = accounts2Countries_Map.get(newMDF.Account__c);
                
                if (countryCodes2TAM_Map.containsKey(countryCode_Str)) {
                     
                    newMDF.TAM__c = countryCodes2TAM_Map.get(countryCode_Str);
                }
                
                if (countryCodes2CAM_Map.containsKey(countryCode_Str)) {
                     
                    newMDF.CAM__c = countryCodes2CAM_Map.get(countryCode_Str);
                }
            }
            
            else if ( accounts2StatesCodes_Map.containsKey(newMDF.Account__c) ) {   
                
                String countryCode_Str = accounts2StatesCodes_Map.get(newMDF.Account__c);
                
                if (stateCodes2TAM_Map.containsKey(countryCode_Str)) {
                     
                    newMDF.TAM__c = stateCodes2TAM_Map.get(countryCode_Str);
                }
                
                if (stateCodes2CAM_Map.containsKey(countryCode_Str)) {
                     
                    newMDF.CAM__c = stateCodes2CAM_Map.get(countryCode_Str);
                }
            }
        }
    }
    
    // This method returns a map of ther Fund Request related Account IDs as keys and Account Billing Country as values
    private map<String, map<Id, String>> getMapOfAccountsAndTheirBillingCountries(list<SFDC_MDF_Claim__c> newMDFs_List) {
        
        map<Id, String> accounts2CoutryCodes_Map = new map<Id, String>();
        map<Id, String> accounts2StateCodes_Map = new map<Id, String>();
        map<String, map<Id, String>> countriesAndStates2AccountsMap_Map = new map<String, map<Id, String>>();
        
        set<Id> relatedAccountsIds_Set = new set<Id>();
                
        for (SFDC_MDF_Claim__c newMDF : newMDFs_List) {
            
            relatedAccountsIds_Set.add(newMDF.Account__c);
        }
        
        if (!relatedAccountsIds_Set.isEmpty()) {
            
            for (Account acc : [SELECT Id, BillingCountryCode, BillingStateCode FROM Account WHERE Id IN : relatedAccountsIds_Set]) {
                
                if (acc.BillingCountryCode != null) {   
                    
                    if (!acc.BillingCountryCode.EqualsIgnoreCase('US')) {
                        
                        accounts2CoutryCodes_Map.put(acc.Id, acc.BillingCountryCode);
                    }
                    
                    else if (acc.BillingStateCode != null) {
                        
                        accounts2StateCodes_Map.put(acc.Id, acc.BillingStateCode);
                    }
                }
            }
        }
        
        countriesAndStates2AccountsMap_Map.put('BillingStateCode', accounts2StateCodes_Map);
        countriesAndStates2AccountsMap_Map.put('BillingCountryCode', accounts2CoutryCodes_Map);
        
        return countriesAndStates2AccountsMap_Map;
    }
    
    // This method returns a map of countryCodes as keys and list of Users Ids (to populate CAM__c and TAM__c)
    private map<String, Id> getMapOfCountriesAndTAM(map<Id, String> accounts2CoutryCodes_Map) {
        
        map<String, Id> countryCodes2TAMMap = new map<String, Id>();
         
        list< Regions__c> regions_List  = [SELECT Id, Name, TAM__c, version1__c FROM Regions__c WHERE version1__c IN : accounts2CoutryCodes_Map.values()];      
        
        for (Regions__c region  : regions_List) {
             
            if (region.TAM__c != null) {
                
                countryCodes2TAMMap.put(region.version1__c, region.TAM__c);
            }
        }
        
        return countryCodes2TAMMap;
    }
    
    // This method returns a map of countryCodes as keys and list of Users Ids (to populate CAM__c and TAM__c)
    private map<String, Id> getMapOfStatesAndTAM(map<Id, String> accounts2StateCodes_Map) {
        
        map<String, Id> statesCodes2TAMMap = new map<String, Id>();
         
        list< Region_State__c> regionsStates_List  = [SELECT Id, Name, StateCode__c, TAM__c FROM Region_State__c WHERE StateCode__c IN : accounts2StateCodes_Map.values()];        
        
        for (Region_State__c region  : regionsStates_List) {
             
            if (region.TAM__c != null) {
                
                statesCodes2TAMMap.put(region.StateCode__c, region.TAM__c);
            }
        }
        
        return statesCodes2TAMMap;
    }
    
    // This method returns a map of countryCodes as keys and list of Users Ids (to populate CAM__c)
    private map<String, Id> getMapOfCountriesAndCAM(map<Id, String> accounts2CoutryCodes_Map) {
        
        map<String, Id> countryCodes2CAMMap = new map<String, Id>();
         
        list< Regions__c> regions_List  = [SELECT Id, Name, CAM__c, version1__c FROM Regions__c WHERE version1__c IN : accounts2CoutryCodes_Map.values()];      
        
        for (Regions__c region  : regions_List) {
             
            if (region.CAM__c != null) {
                
                countryCodes2CAMMap.put(region.version1__c, region.CAM__c);
            }
        }
        
        return countryCodes2CAMMap;
    }
    
    // This method returns a map of countryCodes as keys and list of Users Ids (to populate CAM__c)
    private map<String, Id> getMapOfStatesAndCAM(map<Id, String> accounts2StateCodes_Map) {
        
        map<String, Id> statesCodes2CAMMap = new map<String, Id>();
         
        list< Region_State__c> regionsStates_List  = [SELECT Id, Name, StateCode__c, CAM__c FROM Region_State__c WHERE StateCode__c IN : accounts2StateCodes_Map.values()];        
        
        for (Region_State__c region  : regionsStates_List) {
             
            if (region.CAM__c != null) {
                
                statesCodes2CAMMap.put(region.StateCode__c, region.CAM__c);
            }
        }
        
        return statesCodes2CAMMap;
    }
    
    // This method will send email with the Invoice Attachment included, when a Fund Claim is approved( Status Approved)
    public void sendEmailAndInvoiceAttachmentWhenClaimIsApproved(list<SFDC_MDF_Claim__c> newMDFs_List, map<Id, SFDC_MDF_Claim__c> oldMDFs_Map) {
        
        //EmailTemplate template = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE Id =: '00X500000017WEX'];       
        
        list<SFDC_MDF_Claim__c> claimsToSendEmailto_List = getApprovedFundClaims(newMDFs_List, oldMDFs_Map);
        map<Id, Attachment> attachmentsIds2Attachments_Map = getMapOfInvoicesRelatedToClaims(claimsToSendEmailto_List);
        
        if(!claimsToSendEmailto_List.isEmpty()) {
            
            for(SFDC_MDF_Claim__c claim : claimsToSendEmailto_List) {
                
                List<Messaging.EmailFileAttachment> emailAttachments_list = new List<Messaging.EmailFileAttachment>();
                if (claim.Invoice_ID__c != null && attachmentsIds2Attachments_Map.containsKey(claim.Invoice_ID__c)) {
                    
                    Attachment attachmentPO = new Attachment();
                    Attachment attachment = attachmentsIds2Attachments_Map.get(claim.Invoice_ID__c);
                    
                    if (attachmentsIds2Attachments_Map.containsKey(claim.PO_ID__c)) {
                        
                        attachmentPO = attachmentsIds2Attachments_Map.get(claim.PO_ID__c);
                    }
                    
                    if (attachment.ParentId == claim.Id) {  
                        
                        Messaging.EmailFileAttachment attachmentE = new Messaging.EmailFileAttachment();
                        attachmentE.setBody(attachment.Body);
                        attachmentE.setContentType(attachment.ContentType);
                        attachmentE.setFileName(attachment.Name);
                        emailAttachments_list.add(attachmentE);
                        
                        if (attachmentPO != null && attachmentPO.Id != null) {
                            
                            Messaging.EmailFileAttachment attachmentEmailPO = new Messaging.EmailFileAttachment();
                            attachmentEmailPO.setBody(attachmentPO.Body);
                            attachmentEmailPO.setContentType(attachmentPO.ContentType);
                            attachmentEmailPO.setFileName(attachmentPO.Name);
                            emailAttachments_list.add(attachmentEmailPO);
                        }
                        
                        list<String> ccAdresses_List = new list<String>{'innag@mellanox.com', 'kara@mellanox.com', 'patty@mellanox.com'};  // {'kara@mellanox.com', 'patty@mellanox.com'};
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        //mail.setSubject('Invoice Approved');
                        mail.setFileAttachments(emailAttachments_list);
                        
                         mail.setTargetObjectId('0035000002barVp'); // Production
                        //mail.setTargetObjectId('003R000000rXBet'); //SandBox
                        mail.setTemplateID('00X50000001sPyt'); // Production
                        //mail.setTemplateID('00XR0000000Qk4X'); // SandBox
                        mail.setCcAddresses(ccAdresses_List);
                        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {mail};
                        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                        
                        if (results[0].success) {
                           
                            System.debug('The email was sent successfully.');
                        }
                        
                         else {
                            
                            System.debug('The email failed to send: '
                                  + results[0].errors[0].message);
                        } 
                    } 
                }
            }
        }
    }
    
    // This method retruns a map of attachments Ids and Attachments as their values, representing all the Invoices of the approved Fund Claims  
    private map<Id, Attachment> getMapOfInvoicesRelatedToClaims(list<SFDC_MDF_Claim__c> claimsToSendEmailto_List) {
        
        set<Id> invoicesIds_Set = new set<Id>();
        
        for (SFDC_MDF_Claim__c claim : claimsToSendEmailto_List) {
            
            invoicesIds_Set.add(claim.Invoice_ID__c);
        }
        
        set<Id> POIds_Set = new set<Id>();
        
        for (SFDC_MDF_Claim__c claim : claimsToSendEmailto_List) {
            
            invoicesIds_Set.add(claim.PO_ID__c);
        }
         
        map<Id, Attachment> attachmentsIds2Attachments_Map;
        invoicesIds_Set.addAll(POIds_Set);
        
        if (!invoicesIds_Set.isEmpty()) {
            
            attachmentsIds2Attachments_Map = new map<Id , Attachment>([SELECT ParentId, Name, Id, Description, ContentType, BodyLength, Body
                                                                       From Attachment WHERE Id IN : invoicesIds_Set]);
        }
        
        return attachmentsIds2Attachments_Map;
    }
    
    // This method will return a list of Fund Claims that their status has been changed to 'Approved'
    private list<SFDC_MDF_Claim__c> getApprovedFundClaims(list<SFDC_MDF_Claim__c> newMDFs_List, map<Id, SFDC_MDF_Claim__c> oldMDFs_Map) {
        
        list<SFDC_MDF_Claim__c> claimsToSendEmailto_List = new list<SFDC_MDF_Claim__c>();
        
        for (SFDC_MDF_Claim__c newMDF : newMDFs_List) {
            
            if (newMDF.Status__c != null) {
                 
                if (newMDF.Status__c.EqualsIgnoreCase('Approved') && newMDF.Status__c != oldMDFs_Map.get(newMDF.Id).Status__c) {
                     
                    claimsToSendEmailto_List.add(newMDF);
                }
            }
        }
        
        return claimsToSendEmailto_List;
    }
    
    // This method will update the Fund Claim fields 'Approval_Comment__c' with the Approver Comment and will turn 'Approval_Handled__c' to false
    public void updateFundClaimsApprovalRequest(list<SFDC_MDF_Claim__c> newFundClaims_List, map<Id, SFDC_MDF_Claim__c> oldFundClaims_Map) {
        
        if (newFundClaims_List.size() == 1) {
            
            if (newFundClaims_List[0].Approval_Handled__c && !oldFundClaims_Map.get(newFundClaims_List[0].Id).Approval_Handled__c) {
            
                Id newFundClaimId = newFundClaims_List[0].Id;
                
                updateFundClamisCommentASync(newFundClaimId);
            }
         }                                              
    }
    
    // This method will update the Fund Claim fields 'Approval_Comment__c' and 'Approval_Handled__c' Async
    @future
    public static void updateFundClamisCommentASync(ID newFundClaimId) {
        
        list<ProcessInstance> ProcessInstances_List = [Select Id, (Select Id, ProcessInstanceId, StepStatus,
                                                       OriginalActorId, ActorId, Comments, StepNodeId, CreatedDate From Steps
                                                       order by createdDate desc limit 1)
                                                       From ProcessInstance p WHERE p.TargetObjectId =: newFundClaimId
                                                       order by lastModifiedDate desc limit 1];
                
            String theComment;    
            if (!ProcessInstances_List.isEmpty()) {
                
                for (ProcessInstance instance : ProcessInstances_List) {
                    
                    for (ProcessInstanceStep lastStep : instance.Steps) {
                    
                        system.debug('lastStep : ' + lastStep);
                        theComment = lastStep.Comments;
                    }
                }
            }
            
        SFDC_MDF_Claim__c newFundClaim = new SFDC_MDF_Claim__c(Id = newFundClaimId);
        newFundClaim.Approval_Handled__c = false;
        newFundClaim.Approval_Comment__c = theComment;
        update newFundClaim;
    }
}