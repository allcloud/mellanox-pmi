global class Opportunity_30_60_90_days_Notifications implements Schedulable{
	global void execute(SchedulableContext SC) {
		Date days_30_ago = System.today().addmonths(-1);
		Date days_60_ago = System.today().addmonths(-2);
		Date days_90_ago = System.today().addmonths(-3);
		Date days_180_ago = System.today().addmonths(-6);
		Date days_7_ago = System.today().adddays(-7);
		Date days_90_next = System.today().adddays(90);
		Date Three_Qs_out;
		
		List<Period> pers = [select StartDate, EndDate from Period where Type='Quarter' and EndDate >= :System.today() order by StartDate limit 3 ];
		if(pers != null && pers[2] != null)			
			Three_Qs_out = pers[2].EndDate;
		else
			Three_Qs_out = System.today().adddays(270);
		
		//Opportunity_zero_amt_7_days__c, Original_Book_Date__c, Opportunity_6month_from_OrigBookDate__c
		List<Opportunity> optys_7_days = new List<Opportunity>();				
		List<Opportunity> optys = new List<Opportunity>();
		List<Opportunity> optys_6_months_fromOrigBookDate = new List<Opportunity>();
		//List<Opportunity> optys_90_days = new List<Opportunity>();
		
		optys = [select id,X30_days_notification__c,X60_days_notification__c,X90_days_notification__c,Required_Ship_Date__c,
											Last_Updated_Date__c,Amount,Bookings_Revenue_Credit__c from Opportunity 
											where Record_type_ID__c = '01250000000Dtdw'
													and Sub_Channel__c != 'BIZ'
													and Last_Updated_Date__c != null
													and Required_Ship_Date__c <= :Three_Qs_out
													and (Last_Updated_Date__c = :days_30_ago or Last_Updated_Date__c = :days_60_ago or Last_Updated_Date__c = :days_90_ago) 
													and (StageName='Commit' or StageName='Best Case' or StageName='Pipeline')
											order by Last_Updated_Date__c];
		optys_7_days = [select id,Closed_Lost_By__c,StageName,Last_Updated_Date__c, Name
									from Opportunity where Record_type_ID__c = '01250000000Dtdw' 
															//and Sub_Channel__c != 'BIZ'
															and (Amount=0 or Amount=null)
															and of_Quotes__c = 0
															and (StageName='Commit' or StageName='Best Case' or StageName='Pipeline')
															and Last_Updated_Date__c != null
															and Last_Updated_Date__c <= :days_7_ago];
		
		optys_6_months_fromOrigBookDate = [select id,Closed_Lost_By__c,StageName,Original_Book_Date__c 
									from Opportunity where Record_type_ID__c = '01250000000Dtdw' 
															and Sub_Channel__c != 'BIZ'
															and (StageName='Commit' or StageName='Best Case' or StageName='Pipeline')
															and Amount != null and Amount <=50000
															and Original_Book_Date__c != null
															and Original_Book_Date__c = :days_180_ago]; 
		
		if((optys==null || optys.size()==0) && (optys_7_days==null || optys_7_days.size()==0) && (optys_6_months_fromOrigBookDate==null || optys_6_months_fromOrigBookDate.size()==0))
			return;
		
		for (Opportunity oo : optys_6_months_fromOrigBookDate){
			oo.StageName = 'Closed Lost';
			oo.Closed_Lost_By__c = 'Christina Quinto' + '-' + String.valueOf(System.today() + '- deals less than 50k and Original Book Date 6 months ago.');
		}	
		
		for (Opportunity oo : optys_7_days){
			oo.StageName = 'Closed Lost';
			oo.Closed_Lost_By__c = 'Christina Quinto' + '-' + String.valueOf(System.today() + '- deals create more than 7 days and no Quotes or zero Amount.');
		}	
		
		for (Opportunity o : optys){
			//Kn added 10-14-15
			if(o.Bookings_Revenue_Credit__c == 'Revenue' && o.Amount < 100000)
				continue;
			else if (o.Last_Updated_Date__c == days_90_ago){
				o.X90_days_notification__c = true;	
			}	
			else if (o.Last_Updated_Date__c == days_60_ago){
				o.X60_days_notification__c = true;
			}
			else if(o.Last_Updated_Date__c == days_30_ago && o.Required_Ship_Date__c <= days_90_next){
				o.X30_days_notification__c = true;
			}
		}
		
		if(optys != null && optys.size()>0)	
			Database.update(optys,false);
		if(optys_7_days != null && optys_7_days.size()>0)	
			Database.update(optys_7_days,false);
		if(optys_6_months_fromOrigBookDate != null && optys_6_months_fromOrigBookDate.size()>0)	
			Database.update(optys_6_months_fromOrigBookDate,false);
		
		for (Opportunity o : optys){
			o.X30_days_notification__c = false;
			o.X60_days_notification__c = false;
			o.X90_days_notification__c = false;
		}
		if(optys != null && optys.size()>0)	
			Database.update(optys,false);
	}
}