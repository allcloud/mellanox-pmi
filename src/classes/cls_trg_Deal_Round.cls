public with sharing class cls_trg_Deal_Round {
	
	// This method will send an Email message to specific users to rate the Deal
	public void sendEmailToRateDeals(list<Deal_Round__c> newDealRounds_List, map<Id, Deal_Round__c> oldDealRounds_Map) {
		
		set<Deal_Round__c> deals2SendEmailsTo_set = new set<Deal_Round__c>();
 		
		for (Deal_Round__c newDealRound : newDealRounds_List) {
			
			if (newDealRound.Stage__c != null && newDealRound.Stage__c.EqualsIgnoreCase('Staff Review')) {
				
				if (newDealRound.Stage__c != oldDealRounds_Map.get(newDealRound.Id).Stage__c) {
					
					deals2SendEmailsTo_set.add(newDealRound);
				}
			}
			
			if (newDealRound.Send_Reminders_to_vote__c && !oldDealRounds_Map.get(newDealRound.Id).Send_Reminders_to_vote__c) {
				
				deals2SendEmailsTo_set.add(newDealRound);
			}
		}
		
		if (!deals2SendEmailsTo_set.isEmpty()) {
			
			list<User> users2SendEmailsTo_List = [SELECT Id, UserName, ContactId, Email FROM User WHERE Deal_Approver__c = true];
			
			OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'deal_flow_email@34tzj2g5n7m2z5pxtfdj4koou4fxyugk9t5fi2lvrjpnf64n9g.5-7hegeaa.na3.apex.salesforce.com'];
			
			for (Deal_Round__c newDealRound : deals2SendEmailsTo_set) {
				
				for (User currentUser : users2SendEmailsTo_List) {
					
					if (newDealRound.UserNames_not_voted__c != null) {
						
						if (newDealRound.UserNames_not_voted__c.contains(currentUser.UserName)) {
							
							 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            
                            
                            mail.setTargetObjectId(currentUser.ContactId);
                            mail.setTemplateID('00X50000001sO0N');
                           // mail.setCcAddresses(ccAdresses_List);
                            mail.saveAsActivity = false;
                            if ( owea.size() > 0 ) {
						    	mail.setOrgWideEmailAddressId(owea.get(0).Id);
							}
                            mail.setWhatId(newDealRound.id);
                            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {mail};
                            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
						}
					}
				}
			}
		}
	}
}