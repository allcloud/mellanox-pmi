public class myRegistrationController {
 
  // the soql without the order and limit
  private String soql {get;set;}
  // the collection of contacts to display
  public List<Opp_Reg_OPN__c> registrations {get;set;}
  String userID = UserInfo.getUserID();
  User activeUser = [Select Username,Customer_Buyer__c,contactAccount__C From User where ID= : userID limit 1];
  String userEmail = activeUser.Username;
  //boolean superuser = activeUser.Customer_Buyer__c;
  string customerAccount= activeUser.contactAccount__C;
  String exp_regis = 'Expired';
  // the current sort direction. defaults to des
  String tmp_Account,tmp2,tmp3;
  
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to BBBSDT__C
  public String sortField {
    get  { if (sortField == null) {sortField = 'Opp_Reg__r.Registration_Nbr__c'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir; }
    set;
  }
 
  // init the controller and display some sample data when the page loads
  public myRegistrationController() {
	   if (customerAccount == 'AVNET ASIA PTE. LTD.'){
       		tmp_Account = 'AVNET ASIA PTE. LTD.';
       		tmp2 = 'AVNET TAIWAN';
       		tmp3 = 'AVNET MEMEC CHINA';
       		soql = 'SELECT Opp_Reg__r.Registration_Region_Calc__c,Opp_Reg__r.Registration_Expiration_Date__c,Opp_Reg__r.Approval_Date__c,Opp_Reg__r.Registration_Status_Calc__c,Opp_Reg__r.End_User__c,Opp_Reg__r.Partner_Reseller_Name__c,Opp_Reg__r.Discount_Granted__c, Opp_Reg__r.Distributor_Name__c, Opp_Reg__r.Disty_Contact_Name__c, OPN__r.Part_Number__c, OPN_Quantity__c, DB_Cost__c,Approved_Cost__c, Total_Value__c, OPN_Requested_Discount__c, Opp_Reg__r.Registration_Nbr__c,Opp_Reg__r.Registration_Number_New_Format__c FROM Opp_Reg_OPN__c where Opp_Reg__r.Registration_Status_Calc__c!=:exp_regis and (Opp_Reg__r.createddate>=last_n_days:120 or (Opp_Reg__r.Approval_Date__c!=NULL and Opp_Reg__r.Approval_Date__c>=last_n_days:120)) and (Opp_Reg__r.Distributor_Name__c = \'' + tmp_Account + '\'' + ' or Opp_Reg__r.Distributor_Name__c = \'' + tmp2 + '\'' + ' or Opp_Reg__r.Distributor_Name__c = \'' + tmp3 + '\')' ;
	   }
	   else if (customerAccount == 'BOSTON LTD' || customerAccount == 'BOSTON LIMITED'){
       		tmp_Account = 'BOSTON%';
       		soql = 'SELECT Opp_Reg__r.Registration_Region_Calc__c,Opp_Reg__r.Registration_Expiration_Date__c,Opp_Reg__r.Approval_Date__c,Opp_Reg__r.Registration_Status_Calc__c,Opp_Reg__r.End_User__c,Opp_Reg__r.Partner_Reseller_Name__c,Opp_Reg__r.Discount_Granted__c, Opp_Reg__r.Distributor_Name__c, Opp_Reg__r.Disty_Contact_Name__c, OPN__r.Part_Number__c, OPN_Quantity__c, DB_Cost__c,Approved_Cost__c, Total_Value__c, OPN_Requested_Discount__c, Opp_Reg__r.Registration_Nbr__c,Opp_Reg__r.Registration_Number_New_Format__c FROM Opp_Reg_OPN__c where Opp_Reg__r.Registration_Status_Calc__c!=:exp_regis and (Opp_Reg__r.createddate>=last_n_days:120 or (Opp_Reg__r.Approval_Date__c!=NULL and Opp_Reg__r.Approval_Date__c>=last_n_days:120)) and Opp_Reg__r.Distributor_Name__c like \'' + tmp_Account  +'\'';
	   } 
	   else
			soql = 'SELECT Opp_Reg__r.Registration_Region_Calc__c,Opp_Reg__r.Registration_Expiration_Date__c,Opp_Reg__r.Approval_Date__c,Opp_Reg__r.Registration_Status_Calc__c,Opp_Reg__r.End_User__c,Opp_Reg__r.Partner_Reseller_Name__c,Opp_Reg__r.Discount_Granted__c, Opp_Reg__r.Distributor_Name__c, Opp_Reg__r.Disty_Contact_Name__c, OPN__r.Part_Number__c, OPN_Quantity__c, DB_Cost__c,Approved_Cost__c, Total_Value__c, OPN_Requested_Discount__c, Opp_Reg__r.Registration_Nbr__c,Opp_Reg__r.Registration_Number_New_Format__c FROM Opp_Reg_OPN__c where Opp_Reg__r.Registration_Status_Calc__c!=:exp_regis and (Opp_Reg__r.createddate>=last_n_days:120 or (Opp_Reg__r.Approval_Date__c!=NULL and Opp_Reg__r.Approval_Date__c>=last_n_days:120)) and Opp_Reg__r.Distributor_Name__c=\'' + customerAccount  +'\'';
    
    runQuery();
  }
 
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
   public PageReference export() 
   {
    
        return page.MyRegistrationsCSV;
    }
  // runs the actual query
  public void runQuery() {
 
    try {
      registrations = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 700');
    } catch (Exception e) {
            ApexPages.addMessages(e);
     // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.message));
    }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String REG_NUMBER = Apexpages.currentPage().getParameters().get('REG_NUMBER');
 		//allow users to search registration rregardless of its status or Creation-Date
        if (customerAccount == 'BOSTON LTD' || customerAccount == 'BOSTON LIMITED'){
       		tmp_Account = 'BOSTON%';
       		soql = 'SELECT Opp_Reg__r.Registration_Region_Calc__c,Opp_Reg__r.Registration_Expiration_Date__c,Opp_Reg__r.Approval_Date__c,Opp_Reg__r.Registration_Status_Calc__c,Opp_Reg__r.End_User__c,Opp_Reg__r.Partner_Reseller_Name__c,Opp_Reg__r.Discount_Granted__c, Opp_Reg__r.Distributor_Name__c, Opp_Reg__r.Disty_Contact_Name__c, OPN__r.Part_Number__c, OPN_Quantity__c, DB_Cost__c,Approved_Cost__c, Total_Value__c, OPN_Requested_Discount__c, Opp_Reg__r.Registration_Nbr__c,Opp_Reg__r.Registration_Number_New_Format__c FROM Opp_Reg_OPN__c where Opp_Reg__r.Distributor_Name__c like \'' + tmp_Account  +'\'';
	   	} 
	   	else if (customerAccount == 'AVNET ASIA PTE. LTD.'){
			soql = 'SELECT Opp_Reg__r.Registration_Region_Calc__c,Opp_Reg__r.Registration_Expiration_Date__c,Opp_Reg__r.Approval_Date__c,Opp_Reg__r.Registration_Status_Calc__c,Opp_Reg__r.End_User__c,Opp_Reg__r.Partner_Reseller_Name__c,Opp_Reg__r.Discount_Granted__c, Opp_Reg__r.Distributor_Name__c, Opp_Reg__r.Disty_Contact_Name__c, OPN__r.Part_Number__c, OPN_Quantity__c, DB_Cost__c,Approved_Cost__c, Total_Value__c, OPN_Requested_Discount__c, Opp_Reg__r.Registration_Nbr__c,Opp_Reg__r.Registration_Number_New_Format__c FROM Opp_Reg_OPN__c where (Opp_Reg__r.Distributor_Name__c = \'' + tmp_Account + '\'' + ' or Opp_Reg__r.Distributor_Name__c = \'' + tmp2 + '\'' + ' or Opp_Reg__r.Distributor_Name__c = \'' + tmp3 + '\')';
System.debug('...KN soql===' + soql);				   		
	   	}
	   	else
			soql = 'SELECT Opp_Reg__r.Registration_Region_Calc__c,Opp_Reg__r.Registration_Expiration_Date__c,Opp_Reg__r.Approval_Date__c,Opp_Reg__r.Registration_Status_Calc__c,Opp_Reg__r.End_User__c,Opp_Reg__r.Partner_Reseller_Name__c,Opp_Reg__r.Discount_Granted__c, Opp_Reg__r.Distributor_Name__c, Opp_Reg__r.Disty_Contact_Name__c, OPN__r.Part_Number__c, OPN_Quantity__c, DB_Cost__c,Approved_Cost__c, Total_Value__c, OPN_Requested_Discount__c, Opp_Reg__r.Registration_Nbr__c,Opp_Reg__r.Registration_Number_New_Format__c FROM Opp_Reg_OPN__c where Opp_Reg__r.Distributor_Name__c=\'' + customerAccount  +'\'';      
    if (!REG_NUMBER.equals(''))
      soql += ' and Opp_Reg__r.Registration_Number_New_Format__c  LIKE \''+String.escapeSingleQuotes(REG_NUMBER)+'%\'';
    
    // run the query again
    runQuery();
 
    return null;
  }
 
  // use apex describe to build the picklist values

 
}