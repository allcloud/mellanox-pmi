public with sharing class VF_CCPU_Error_Page {
	
	 
	
	public VF_CCPU_Error_Page() {
		
		ApexPages.Message PMsg = new ApexPages.Message(ApexPages.severity.error,'THIS IS A SYSTEM SUPPORT CONTACT ON A SERVICE CONTRACTED ACCOUNT.'+ 
														'ALL SERVICE REQUEST WILL NOT GO TO GSO SUPPORT TEAM.' +
														'Please check the "Confirm case redirect to Tech Owner" checkbox');
        ApexPages.addMessage(PMsg);
         return ;
	}
	
	

}