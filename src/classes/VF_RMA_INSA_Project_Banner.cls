public class VF_RMA_INSA_Project_Banner {
    
    public RMA__c theRMA {get;set;}
    public boolean is_INSA {get;set;}

    public VF_RMA_INSA_Project_Banner(ApexPages.standardController controller) 
    { 
        is_INSA = false;
        Id rmaId = controller.getId();
        
        theRMA = [SELECT id, RMA_Support_Project__c FROM RMA__c WHERE Id =: rmaId];
        
        if ( theRMA.RMA_Support_Project__c != null)  { 
            
            is_INSA = true;
        }
    }
}