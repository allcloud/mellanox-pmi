public with sharing class VF_RMA_Letter_RequestInProcess 
{
    
    public ID id;
    
    public VF_RMA_Letter_RequestInProcess(ApexPages.StandardController controller) 
    {
        //id = Apexpages.currentPage().getParameters().get('Id');
        //getDeliverAsPDF();
    }

    public PageReference getDeliverAsPDF_InProcess() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         id = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         
         system.debug('id is : ' + id);
         system.debug('isTest is : ' + isTest);
         
                  
         //String b = pdf.getContent().toString();
          RMA__c rm = [Select r.id, r.contact__r.email, r.sup_case_assignee__r.email,r.RMA_Number__c, r.Approved_to_ship_from__c,
                        r.e_mail__c, r.Mellanox_Site__c, r.placed_in_oracle__c, r.RMA_state__c, r.Ship_Restrictions__c, r.Restricted_Country__c From RMA__c r WHERE r.id =:Id];
          
          if(rm.RMA_state__c != 'Approved')
          {   
           Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'RMA can move to Execution in Process only after it was Approved'));
           return  Apexpages.currentPage();

          }
                  
           
          
         if(rm.Mellanox_Site__c == NULL)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ' Please fill in the \'Mellanox Site\' field in order to move to Execution in Process'));
              return  Apexpages.currentPage();
  
            } 
            
            
          if(rm.Ship_Restrictions__c == 'non IL products' && rm.Restricted_Country__c == FALSE)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ' The RMA is shipped to restricted country. Please check \'Restricted Country\'  in order to move to Execution in Process'));
              return  Apexpages.currentPage();
  
            }   
            
            
            
         if(rm.RMA_Number__c == NULL)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ' Please fill in the \'RMA Number\' from Oracle in order to move to Execution in Process'));
              return  Apexpages.currentPage();
  
            } 

        if( rm.placed_in_oracle__c == True)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'The RMA was already moved to Execution in Process and RMA letter was sent to customer'));
              return  Apexpages.currentPage();
  
            } 
       if(rm.Approved_to_ship_from__c == NULL)
           {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select the subinventory location in \'Approved_to_ship_from__c \' field'));
              return  Apexpages.currentPage();
  
           }



         PageReference pdf =  Page.VF_RMA_Letter;
         pdf.getParameters().put('id',id);
         
         system.debug('pdf.getParameters().get id:  ' + pdf.getParameters().get('id'));


           
          rm.RMA_State__c = 'Execution in process';  
          rm.placed_in_oracle__c = True;    
           

                                

//-----------------------------------------------------

Group rmaGroup = [Select g.Name, g.Id, g.Email From Group g WHERE Name ='RMA GROUP'];
List<GroupMember> lst_grpMembers = new List<GroupMember >([Select g.UserOrGroupId, g.Id, g.GroupId From GroupMember g where g.GroupId  =: rmaGroup.Id ]);
             system.debug('lst_grpMembers siae : ' +lst_grpMembers.size() );
             
             Set<ID> set_UsrIds = new Set<ID>();
             
             if(lst_grpMembers != null && lst_grpMembers.size() > 0)
             {
                for(GroupMember grpm :lst_grpMembers)
                {
                    set_UsrIds.add(grpm.UserOrGroupId);
                }           
             }
         
             system.debug('set_UsrIds Size : ' + set_UsrIds.size());
             
             List<User> lst_users = new List<User>([Select u.Id, u.Email From User u WHERE u.id IN: set_UsrIds]);
              system.debug('lst_users Size : ' + lst_users.size());
             
             set<String> set_usrEmails = new set<String>();
             if( lst_users != null && lst_users.size() > 0)
             {
                for(User usr :lst_users)
                {
                    if(usr.Email != null)
                    {
                        set_usrEmails.add(usr.Email);
                    }
                }
             }
             system.debug('set_usrEmails Size : ' + set_usrEmails.size());



           // Create an email
     
         Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          String [] toAddresses = new String[] {rm.e_mail__c};
          if(rm.contact__r.email!=NULL) 
          {toAddresses.add(rm.contact__r.email); } 
          String [] BccAddresses = new String[] {};

          if(rm.sup_case_assignee__r.email!=NULL)
          {BccAddresses.add(rm.sup_case_assignee__r.email);}



          
             if(set_usrEmails != null && set_usrEmails.size() > 0)
             {
                for(String usrEmail :set_usrEmails)
                {
                    BccAddresses.add(usrEmail);
                }
             }
//-----------------------------------------------------------------


         email.setToAddresses(toAddresses);
         email.setBccAddresses(BccAddresses);
         email.setOrgWideEmailAddressId(Env.RMASupportAddress);
         email.setSubject('Mellanox RMA #'+ rm.RMA_Number__c +' Confirmation update');
         email.setPlainTextBody('Dear customer, \n \n' +   
                                 'Your RMA Request is approved and assigned to RMA number #' + rm.RMA_Number__c + '\n. Kindly use the attached for product return instructions.\n'+
                                 '\nTo learn more about Mellanox RMA:\n' +
                                 'http://www.mellanox.com/pdf/user_manuals/Mellanox_Support_and_Services_User_Guide.pdf \n\n' +
                                 'Thank you\n' +
                                 'Mellanox Support\n');


            
         // Create an email attachment
         // email.setHtmlBody(b);
         if(isTest == 'Testing') 
         {
         }else
         {
            blob b = pdf.getContent();
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        
            efa.setFileName('RMA_Acknowledgment_letter.pdf'); // neat - set name of PDF
        
            efa.setBody(b); //attach the PDF
        
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        }
         // send it, ignoring any errors (bad!)
    
         Messaging.SendEmailResult [] r =
    
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
       
         update rm;

        return new Pagereference('/'+id);
    }




//----------------------------------------------------









    static testMethod void Test_VF_RMA_Letter_RequestInProcess() 
    {
        //list of serial numbers to be related to the RMA and to avoid many inserts
                List<Serial_Number__c> lst_snOfRMA = new List<Serial_Number__c>();
                
                List<Product2> lst_prods = New List<Product2>();
                //instance of object creator
                CLS_ObjectCreator obj = new CLS_ObjectCreator();
                
                Account acc = obj.createAccount();
                insert acc;
        
               Contact con = obj.CreateContact(acc);
               con.email = 'hgft56@er.com';
               insert con;
                
                MellanoxSite__c  site  = obj.createMellanoxSite();
                insert site;
                 
                RMA__c rma = obj.CreateRMA();
                rma.e_mail__c = 'dsfds@fdfd.com';
                rma.Mellanox_Site__c = site.id;
                rma.contact__c = con.id;
                rma.sup_case_assignee__c = '005500000013ipV';
                insert rma;
                
                Case rmaCase = obj.CreateRMA_case(acc, con, rma);
                Insert rmaCase;
                
                /*rmaCase.assignee__c = '005500000013ipV';
                rmacase.customer_environment__c = 'USA, CANADA, LATAM' ;
                update rmaCase;*/
    
    
                Product2 p1 =  obj.createProduct();
                p1.Inventory_Item_id__c = '90989';
                lst_prods.add(p1);
                
                Product2 p2 =  obj.createProduct();
                p2.Name = 'Test Product 2';
                p2.Inventory_Item_id__c = '90980';
                lst_prods.add(p2);
                
                Serial_Number__c sn1 = obj.createSerialNumber(rma,p1);
                lst_snOfRMA.add(sn1);
                
                Serial_Number__c sn2 = obj.createSerialNumber(rma,p2);
                lst_snOfRMA.add(sn2);
                
                insert lst_prods;
                insert lst_snOfRMA; 
                
                Test.setCurrentPageReference(new PageReference('Page.VF_RMA_Letter_RequestInProcess')); 
                System.currentPageReference().getParameters().put('id',rma.Id );       
                System.currentPageReference().getParameters().put('isTest','Testing' );  
              
                Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rma);
                VF_RMA_Letter_RequestInProcess vf = new VF_RMA_Letter_RequestInProcess(teststandard);
                vf.getDeliverAsPDF_InProcess();
    
    }

}