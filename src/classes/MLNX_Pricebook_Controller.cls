public class MLNX_Pricebook_Controller {
	public String PricebookSelected {get;set;} 
	public List<SelectOption> PricebookList {get;set;}
	public List<MLNX_Price_Book__c> pb_rows {get; set;}
	//boolean rows_selected = false;
	
	//public List<MLNX_Price_Book__c> rows_updates = new List<MLNX_Price_Book__c>();
	public List<MLNX_Price_Book__c> rows_inserts = new List<MLNX_Price_Book__c>();
	
	public List<MLNX_Price_Book__c> add_MLNX_Price_Book_rows {get; set;}
	
	public MLNX_Pricebook_Controller (ApexPages.StandardController controller){
		//selected_all = false;
		PricebookSelected = 'OEM Price List';
		
		pb_rows = new List<MLNX_Price_Book__c>();
		add_MLNX_Price_Book_rows = new List<MLNX_Price_Book__c>();
		
		PricebookList = new List<SelectOption>();
		//PricebookList.add(new SelectOption('--None--','--None--'));
		PricebookList.add(new SelectOption('OEM Price List','OEM Price List'));
		//PricebookList.add(new SelectOption('Actual','Actual'));
		//PricebookList.add(new SelectOption('Bull Price List','Bull Price List'));
		//PricebookList.add(new SelectOption('Certified Reseller Price Book','Certified Reseller Price Book'));
		//PricebookList.add(new SelectOption('Distributor Price List','Distributor Price List'));
		//PricebookList.add(new SelectOption('DB1 Price Book','DB1 Price Book'));
		//PricebookList.add(new SelectOption('Standard Price Book','Standard Price Book'));
		PricebookList.add(new SelectOption('DELL','DELL'));
		PricebookList.add(new SelectOption('HP PRICE LIST','HP PRICE LIST'));
		PricebookList.add(new SelectOption('CRAY PRICE LIST','CRAY PRICE LIST'));
		PricebookList.add(new SelectOption('EMC PRICE LIST','EMC PRICE LIST'));
		PricebookList.add(new SelectOption('IBM PRICE LIST','IBM PRICE LIST'));
		PricebookList.add(new SelectOption('ORACLE','ORACLE'));
		PricebookList.add(new SelectOption('SGI PRICE LIST','SGI PRICE LIST'));
		PricebookList.add(new SelectOption('TERADATA PRICE LIST','TERADATA PRICE LIST'));
		PricebookList.add(new SelectOption('OEM CHINA','OEM CHINA'));
		PricebookList.add(new SelectOption('FACEBOOK','FACEBOOK'));
		PricebookList.add(new SelectOption('LENOVO','LENOVO'));
	}
	Map<Integer, Integer> month_To_Quarter = new Map<Integer, Integer> 
        {1=>1, 2=>1, 3=>1, 4=>2, 5=>2, 6=>2, 7=>3, 8=>3, 9=>3, 10=>4, 11=>4, 12=>4};
	public List<String> quarters {
        
        get{
                
            List<String> ans = new List<String>();
            //ans = getNext8Quarters(month_To_Quarter.get(system.today().month()), system.today().year());
            //display only next quarter and on-ward
            ans = getNext8Quarters(month_To_Quarter.get(system.today().month()), system.today().year());
System.debug('...KN ans===' + ans);            
            return ans;
        }
    }
	public List<String> getNext8Quarters(integer q, integer year)
    {
        
        List<String> q_list;
        Integer next1 = year + 1;
        Integer next2 = year + 2;
        Integer next3 = year + 3;
        
        if (q == 1)
        {
            q_list = new List<String> {'Q1 - ' +year,'Q2 - ' +year, 'Q3 - ' +year, 'Q4 - ' +year,
                                       'Q1 - ' +next1, 'Q2 - ' +next1, 'Q3 - ' +next1, 'Q4 - ' +next1,
                                       'Q1 - ' +next2,'Q2 - ' +next2
                                       //KN added 5-2-13
                                       ,'Q3 - ' +next2, 'Q4 - ' +next2
                                       };
                                                                                                            
        }
        if (q == 2)
        {
            q_list = new List<String> {'Q2 - ' +year,'Q3 - ' +year, 'Q4 - ' +year, 'Q1 - ' +next1,
                                       'Q2 - ' +next1, 'Q3 - ' +next1, 'Q4 - ' +next1, 'Q1 - ' +next2,
                                       //KN added 5-2-13
                                       //'Q2 - ' +next2,  'Q3 - ' +next2};
                                       'Q2 - ' +next2,  'Q3 - ' +next2, 'Q4 - ' +next2, 'Q1 - ' +next3};
                                            
        }
        if (q == 3)
        {
            q_list = new List<String> {'Q3 - ' +year,'Q4 - ' +year, 'Q1 - ' +next1, 'Q2 - ' +next1,
                                       'Q3 - ' +next1, 'Q4 - ' +next1, 'Q1 - ' +next2, 'Q2 - ' +next2,  
                                       //'Q3 - ' +next2 , 'Q4 - ' +next2};
                                       'Q3 - ' +next2 , 'Q4 - ' +next2, 'Q1 - ' +next3,'Q2 - ' +next3};
                                                                                                            
        }
        if (q == 4)
        {
            q_list = new List<String> {'Q4 - ' +year,'Q1 - ' +next1, 'Q2 - ' +next1, 'Q3 - ' +next1,
                                       'Q4 - ' +next1, 'Q1 - ' +next2, 'Q2 - ' +next2, 'Q3 - ' +next2,
                                       //'Q4 - ' +next2,'Q1 - ' +next3};
                                       'Q4 - ' +next2,'Q1 - ' +next3, 'Q2 - ' +next3, 'Q3 - ' +next3};
                                                                    
                                                
        }
        return q_list;          
        
    }
	public void init(){
		rows_inserts = new List<MLNX_Price_Book__c>();
		pb_rows = [select id,X9Quarter_out_Price__c,X10Quarter_out_Price__c,X11Quarter_out_Price__c,
						X8Quarter_out_Price__c, X7Quarter_out_Price__c, X6Quarter_out_Price__c, X5Quarter_out_Price__c, 
						X4Quarter_out_Price__c, X3Quarter_out_Price__c, X2Quarter_out_Price__c, X1Quarter_out_Price__c, 
						Product__c, Product_Name__c, Current_Price__c,Selected_row__c 
						from MLNX_Price_Book__c where Pricebook_Name__c = :PricebookSelected order by Name];
		for (integer i = 0; i<10 ; i++) {
			add_MLNX_Price_Book_rows.add(new MLNX_Price_Book__c());
		}
	}
	public void SelectPB() 
    {
		init();
    }
    
    public pagereference save(){
    	//Consider future updates or inserts ?
    	if (pb_rows.size()>0)
    		update pb_rows;
    	if (add_MLNX_Price_Book_rows.size()>0){
    		for (MLNX_Price_Book__c mp : add_MLNX_Price_Book_rows) {
    			if(mp.Product__c != null && (mp.X1Quarter_out_Price__c != null || mp.X2Quarter_out_Price__c != null 
    											|| mp.X3Quarter_out_Price__c != null || mp.X4Quarter_out_Price__c != null
    											|| mp.X5Quarter_out_Price__c != null || mp.X6Quarter_out_Price__c != null
    											|| mp.X7Quarter_out_Price__c != null || mp.X8Quarter_out_Price__c != null ) ) {
    				mp.Pricebook_Name__c = PricebookSelected;
    				rows_inserts.add(mp);
    				insert rows_inserts;
    			}
    		}
    	}
    	//Re-initialize after save
    	add_MLNX_Price_Book_rows.clear();
    	init();
    	return null;
    }
}