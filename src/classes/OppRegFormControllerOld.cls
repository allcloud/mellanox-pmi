public class OppRegFormControllerOld
{
/*

    public Distributor_Oppy_Registrations__c odisioppreg {get;set;}
    public boolean bSupportPortal{get;set;}
    public List<SelectOption> DistributerState {get;set;}
    public String ErrMsg {get;set;}
    
  public OppRegFormControllerOld()   {      
  odisioppreg=new Distributor_Oppy_Registrations__c();      
  bSupportPortal=IsSupportPortal();    }
  public OppRegFormControllerOld(ApexPages.StandardController controller) 
    {
        odisioppreg=new Distributor_Oppy_Registrations__c();
        bSupportPortal=IsSupportPortal();
    }
    public List<selectOption> getDistStates()
    {
        return retAllStates(odisioppreg.Disty_Country__c);
    }  
    public List<selectOption> getPartnerStates()
    {
        return retAllStates(odisioppreg.Partner_Country__c);
    }  
    public List<selectOption> getEndUserStates()
    {
        return retAllStates(odisioppreg.End_User_Country__c);
    }  
    private List<selectOption> retAllStates(String selectedCountry)
    {
        List<selectoption> soptions = new List<selectOption>();
        soptions.Add(new selectoption('','- None -'));
        if(selectedCountry!=null && selectedCountry!='')
        {
            List<US_Territory_Map__c> stateOptions = [Select u.Id, u.Name, u.Country__c From US_Territory_Map__c u where IsDeleted=false and u.Country__c=:selectedCountry Order By Name];
            set<string> stateset = new set<string>();
            List <String> lst_state = new List <String>();
            for(US_Territory_Map__c c: stateOptions)
            {
                if(!stateset.contains(c.Name))
                {
                    stateset.Add(c.Name);
                    lst_state.Add(c.Name);
                }
            }
            
            for(String s : lst_state )
            {
                soptions.Add(new selectoption(s,s));
            }
        }
        return soptions;
    }
    public List<selectOption> getAllCountries()
    {
        List<US_Territory_Map__c> countryOptions = [Select u.Id, u.Country__c From US_Territory_Map__c u where IsDeleted=false Order By Country__c];
        system.debug('BLAT countryOptions: ' + countryOptions);
        set<string> countryset = new set<string>();
        List <String> lst_country = new List <String>();
        for(US_Territory_Map__c c: countryOptions)
        {
            if(!countryset.contains(c.Country__c)&& c.Country__c != NULL)
            {
                countryset.Add(c.Country__c);
                lst_country.Add(c.Country__c);
            }
        }
        system.debug('BLAT countryset: ' + countryset);
        List<selectoption> coptions = new List<selectOption>();
        coptions.Add(new selectoption('','- None -'));
        
        for(String s : lst_country)
        {
            system.debug('BLAT s: ' + s);
            coptions.Add(new selectoption(s,s));
        }
        
        system.debug('BLAT coptions: ' + coptions);
        return coptions;
        
    }
    private boolean IsSupportPortal()
    {
        return true;
    }
    
    public PageReference Reset()
    {
        PageReference cPage = ApexPages.CurrentPage();
        cPage.SetRedirect(true);
        return cPage;
    }
     public List<SelectOption> GetDistiList()
    {
        List<SelectOption> listOptions=new list<SelectOption>();    
        listOptions.add(new SelectOption('','- None -'));
            
        for(Account oAccount:[select Name from Account where Is_Distributor__c=true]) 
        {
            listOptions.add(new SelectOption(oAccount.Name,oAccount.Name));     
        }       
        return listOptions;    
    }
    
    public PageReference Save()
    {
        try
        {
                if(!ValidateRequierFields())
                {
                    system.debug('Saving');
                    SetDefaults();          
                    Distributor_Oppy_Registrations__c odisioppregtemp=new Distributor_Oppy_Registrations__c();
                    odisioppregtemp=odisioppreg;
                    insert odisioppregtemp;         
                    odisioppreg=odisioppregtemp.clone(false,true);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Registration has been submitted'));
                }
                return null;
        }
        catch(exception e)
        {
        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage()));
            return null;
        }
    }
    private Boolean ValidateRequierFields()
    {
        Boolean IsErrorOccured=false;
        
                //Requestor Section
                if(odisioppreg.Requestor_Name__c==null ||  odisioppreg.Requestor_Name__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Requestor Name: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Requestor_Email__c==null ||  odisioppreg.Requestor_Email__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Requestor Email: You must enter a value'));
                        IsErrorOccured=true;
                }
                //Distributer Section
                if(odisioppreg.Distributor_Name__c==null ||  odisioppreg.Distributor_Name__c=='')
        {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Distributor Name: You must enter a value'));
                IsErrorOccured=true;
        }
                if(odisioppreg.Disty_Contact_Name__c==null ||  odisioppreg.Disty_Contact_Name__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Distributor Contact Name: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Disty_State__c==null ||  odisioppreg.Disty_State__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Distributor State/Province: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Disty_Contact_Email__c==null ||  odisioppreg.Disty_Contact_Email__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Distributor Email: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Disty_Country__c==null ||  odisioppreg.Disty_Country__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Distributor Country: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Disty_Phone__c==null ||  odisioppreg.Disty_Phone__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Distributor Phone: You must enter a value'));
                        IsErrorOccured=true;
                }               
                                
                //Partner Section
                if(odisioppreg.Partner_Reseller_Name__c==null ||  odisioppreg.Partner_Reseller_Name__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Partner/Reseller Name: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Partner_Contact_Name__c==null ||  odisioppreg.Partner_Contact_Name__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Partner/Reseller Contact Name: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Partner_State__c==null ||  odisioppreg.Partner_State__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Partner/Reseller State/Province: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Partner_Contact_Email__c==null ||  odisioppreg.Partner_Contact_Email__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Partner/Reseller Email: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Partner_Country__c==null ||  odisioppreg.Partner_Country__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Partner/Reseller Country: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Partner_Contact_Phone__c==null ||  odisioppreg.Partner_Contact_Phone__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Partner/Reseller Phone: You must enter a value'));
                        IsErrorOccured=true;
                }
                
                //EndUser Section
                if(odisioppreg.End_User__c==null ||  odisioppreg.End_User__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'End User Name: You must enter a value'));
                        IsErrorOccured=true;    

                }
                if(odisioppreg.End_User_Contact_Name__c==null ||  odisioppreg.End_User_Contact_Name__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'End User Contact Name: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.End_User_State__c==null ||  odisioppreg.End_User_State__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'End User State/Province: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.End_User_Contact_Email__c==null ||  odisioppreg.End_User_Contact_Email__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'End User Email: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.End_User_Country__c==null ||  odisioppreg.End_User_Country__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'End User Country: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.End_User_Contact_Phone__c==null ||  odisioppreg.End_User_Contact_Phone__c=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'End User Phone: You must enter a value'));
                        IsErrorOccured=true;
                }
                
                //Opportunity Information Section
                if(odisioppreg.Name==null ||  odisioppreg.Name=='')
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Opportunity Name: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Opportunity_Close_Date__c==null)
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Opportunity Expected Close Date: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.Opportunity_Ship_Date__c==null)
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Opportunity Expected Ship Date: You must enter a value'));
                        IsErrorOccured=true;
                }
                
                //Mellanox Product Section
                if(odisioppreg.Mellanox_OPN_1__c==null)
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Product OPN 1: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.OPN_1_Quantity__c==null )
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Product OPN 1 Quantity: You must enter a value'));
                        IsErrorOccured=true;
                }
                if(odisioppreg.OPN_1_Requested_Discount__c==null)
                {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Product OPN 1 Requested % Discount: You must enter a value'));
                        IsErrorOccured=true;
                }
              
                return IsErrorOccured;
    }

*/

}