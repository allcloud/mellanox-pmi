@isTest
private class test_QuoteLineItem_afterInUpDe {

    static testMethod void myUnitTest() 
    {
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        insert acc;
        
        Opportunity op = obj.createOpportunity(acc);
        system.debug(' my op: ' + op);
        insert op;
        
        List<Product2> lst_Prods = New List<Product2>();
        
        Product2 prod = obj.createProduct();
        prod.Name ='ADD-ON - LARGE CLUSTER';
        prod.Type__c = 'HW Support';
        prod.ProductCode = 'SUP-000812';        
        prod.Product_Type1__c = 'CABLES';
        prod.Product_Cost__c = 90;
        prod.OEM_PL__c = 10;
        prod.Support_Percent_Amount__c = 20;
       
        
      
        //insert prod;
        lst_Prods.add(prod);
        
        Product2 prod2 = obj.createProduct();
        prod2.Name ='Test-GP';
        prod2.Type__c = 'SW Support';
        prod2.ProductCode = 'SUP-00014';
        prod2.Product_Type1__c = 'CABLES';
        prod2.Inventory_Item_id__c = '3332';
        prod2.partNumber__c = '21213';
        prod2.Support_Percent_Amount__c = 60;
        //insert prod2;
        lst_Prods.add(prod2);
      
    
        Product2 prod3 = obj.createProduct();
        prod3.Name ='GOLD_SUPPORT-GP';
        prod3.Type__c = 'HW Support';
        prod3.ProductCode = 'SUP-00017';
        prod3.Product_Type1__c = 'OPTIONS';    
        prod3.Inventory_Item_id__c = '12121212tx';   
        prod3.partNumber__c = '2121tx';
        //prod3.isProductSupport__c=1;
        
        lst_Prods.add(prod3);
        
        Product2 prod4 = obj.createProduct();
        prod4.Name ='HARDWARE';
        prod4.Type__c = 'HW';
        prod4.ProductCode = 'FGTTFSS';
        prod4.Product_Type1__c = 'CABLES';
        prod4.Inventory_Item_id__c = '12121112';
        prod4.partNumber__c = 'dhfgeryte';
        prod4.OEM_PL__C=10000;
     
        lst_Prods.add(prod4);
        
                
        insert lst_Prods;
        
        List<Pricebook2> lst_pbk2 = new List<Pricebook2>();
        
        Pricebook2 pbk2 = obj.CreatePriceBook();
        //insert pbk2;
        lst_pbk2.add(pbk2);
       
        insert lst_pbk2;
        
        List<PricebookEntry>  lst_pbkEntry = new List<PricebookEntry>();
        
        PricebookEntry pbkEntry = obj.CreatePriceBook( pbk2.Id,prod.id);
        //insert pbkEntry;
        
        lst_pbkEntry.add(pbkEntry);
        
        PricebookEntry pbkEntry2 = obj.CreatePriceBook( pbk2.Id,prod2.id);
        //insert pbkEntry2;
        lst_pbkEntry.add(pbkEntry2);
   
 
        PricebookEntry pbkEntry3 = obj.CreatePriceBook( pbk2.Id,prod3.id);
        //insert pbkEntry2;
        lst_pbkEntry.add(pbkEntry3);
         
        PricebookEntry pbkEntry4 = obj.CreatePriceBook( pbk2.Id,prod4.id);
        //insert pbkEntry2;
        lst_pbkEntry.add(pbkEntry4);
                       
        insert lst_pbkEntry;
        
        List<Quote> lst_quotes = new List<Quote>();
        
        Quote q = obj.createQuote(op,pbk2.Id);
        q.Big_Cluster_Opportunity__c = true;
        q.Exclude_Cables_From_Support__c = true;
        
        //insert q;
        lst_quotes.add(q);
        
        Quote q1 = obj.createQuote(op,pbk2.Id);
        q1.Big_Cluster_Opportunity__c = false;
        q1.Exclude_Cables_From_Support__c = false;
        //insert q1;
        lst_quotes.add(q1);
        
        Quote q2 = obj.createQuote(op,pbk2.Id);
        q2.Big_Cluster_Opportunity__c = true;
        q2.Exclude_Cables_From_Support__c = false;
        q2.New_Price_Book__c= '01s500000006J7F';
        q2.Support_Renew_Quote__c=true;
        //insert q1;
        lst_quotes.add(q2);
        
        insert lst_quotes;
        
        
        
        List<QuoteLineItem> lst_quotesLineItems = new List<QuoteLineItem>();
        
        
        QuoteLineItem qli = obj.createQuoteLineItem(q,pbkEntry.id);
        //insert qli;
        
        lst_quotesLineItems.add(qli);
        
        QuoteLineItem qli2 = obj.createQuoteLineItem(q,pbkEntry2.Id);
        
        //insert qli2;
        lst_quotesLineItems.add(qli2);
        
        QuoteLineItem qli3 = obj.createQuoteLineItem(q1,pbkEntry3.Id);
        lst_quotesLineItems.add(qli3);
        QuoteLineItem qli4 = obj.createQuoteLineItem(q2,pbkEntry3.Id);
        lst_quotesLineItems.add(qli4);
        QuoteLineItem qli5 = obj.createQuoteLineItem(q1,pbkEntry4.Id);
        lst_quotesLineItems.add(qli5);
       
        insert lst_quotesLineItems ;
        
        system.debug('List of QLItems: '+ lst_quotesLineItems);
        
        List<QuoteLineItem> testDat = [Select  q.New_Price__c, q.TotalPrice, q.product_type1__c, q.Product__c, q.Product_Type__c, q.Id From QuoteLineItem q where q.QuoteId =: q.Id];
        system.debug('testDat: ' +testDat);
        
        List<Quote> lst_Quote = [ Select q.Price_List__c,q.Price_Book__c,  q.Id From Quote q WHERE q.Id =: q.id];
        system.debug('lst_Quote: ' +lst_Quote);
         /*
        System.debug('TESTTTTTTTT');
        qli5.UnitPrice=400;
        qli5.Quantity=20;
        update qli5;
        System.debug('qli5.');
        
        
        q1.New_Price_Book__c= '01s500000006J7F';
        update q1;
        q2.New_Price_Book__c= '01s500000006J7F';
         q.New_Price_Book__c= '01s500000006J7F';
         update q;
        q2.Exclude_Cables_From_Support__c =true;
        update q2;
         List<QuoteLineItem> lst_quotesLineItems1 = new List<QuoteLineItem>();
        QuoteLineItem qli7 = obj.createQuoteLineItem(q2,pbkEntry.Id);
        lst_quotesLineItems1.add(qli7);
        QuoteLineItem qli8 = obj.createQuoteLineItem(q2,pbkEntry3.Id);
        lst_quotesLineItems1.add(qli8);
        QuoteLineItem qli9 = obj.createQuoteLineItem(q2,pbkEntry4.Id);
        lst_quotesLineItems1.add(qli9);
        
       
   
        insert lst_quotesLineItems1 ;
       
       
        
        
     
        
        
        
        prod.Type__c = 'HW';
        prod.Product_Type1__c = 'CABLES';
        prod.ProductCode = 'SUP-00099';
        update prod;
        
        prod.Type__c = 'SW Support';
        prod.Product_Type1__c = NULL;
        prod.ProductCode = 'SUP-00012';
        update prod;
      
        prod.Type__c = 'HW';
        prod.Product_Type1__c = 'OPTIONS' ;
        prod.ProductCode = 'SUP-00013';
        update prod;

            
        */
   
    }
}