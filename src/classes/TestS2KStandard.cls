/**
 * This class tests the S2KStandard and the Batch Job Invoker trigger.
 *
 */
@isTest
public class TestS2KStandard {	
    // Test Batch Apex Job and Batch Job Invoker Trigger
    public static testMethod void test1(){       
        try{
            Test.startTest();          
            
            //Create two Solutions
            List<Solution> mySolutions = new List<Solution>();
            Solution s1 = new Solution(SolutionNote='Hello World Solution Note -1', SolutionName='Hello World Solution Name -1');  mySolutions.add(s1); 
            Solution s2 = new Solution(SolutionNote='Hello World Solution Note -2', SolutionName='Hello World Solution Name -2');  mySolutions.add(s2);
            insert mySolutions;            
            
            CategoryNode cnParent = [Select MasterLabel from CategoryNode where MasterLabel = 'Level1' LIMIT 1];
            CategoryNode cnChild = [Select MasterLabel from CategoryNode where MasterLabel = 'Level1.1' LIMIT 1];
            CategoryData cd1 = new CategoryData(RelatedSObjectId=mySolutions[0].Id, CategoryNodeId=cnChild.Id); insert cd1;
            CategoryData cd2 = new CategoryData(RelatedSObjectId=mySolutions[1].Id, CategoryNodeId=cnParent.Id); insert cd2;
            
            //Add 2 Attachments to Solution 1
            Attachment a1 = new Attachment(Name='MyFile-1', ContentType='application/pdf', Body=Blob.valueOf('BLAHBLAH'), ParentId = mySolutions[0].Id);
            Attachment a2 = new Attachment(Name='MyFile-2', ContentType='application/pdf', Body=Blob.valueOf('BLAHBLAH'), ParentId = mySolutions[0].Id);
            insert new List<Attachment>{a1, a2};
            
            //Submit Batch Job            
            Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(JobType__c='S2K', Status__c='Custom Fields Creation Completed', 
				//Article_Fields__c = 'Resolution__c, Title',
				//Solution_Fields__c = 'SolutionNote, SolutionName',
				Article_Type__c = 'S2K1235__kav'
				//Attachment_Fields__c = 'Attachment_1__c,Attachment_2__c',
				//WHERE_Clause__c = '', //'SolutionName LIKE \'Hello World%\'',
				//LIMIT_Clause__c = '150',
				//Publish_after_Import__c = true
				);
			insert bjic;
			
			//TODO: Read Articles created & ASSERT
								                                  
            Test.stopTest();
        }
        catch (Exception e){
            System.debug(e.getTypeName() + '>>' + e.getMessage());
        }
    } 
    
    // Test Batch Apex Job and Batch Job Invoker Trigger
    public static testMethod void test2(){       
        try{
            Test.startTest();          
            
            //Create two Solutions
            List<Solution> mySolutions = new List<Solution>();
            Solution s1 = new Solution(SolutionNote='Hello World Solution Note -1', SolutionName='Hello World Solution Name -1', isMigrated__c=false);  mySolutions.add(s1); 
            Solution s2 = new Solution(SolutionNote='Hello World Solution Note -2', SolutionName='Hello World Solution Name -2', isMigrated__c=false);  mySolutions.add(s2);
            insert mySolutions;            
            
            CategoryNode cnParent = [Select MasterLabel from CategoryNode where MasterLabel = 'Level1' LIMIT 1];
            CategoryNode cnChild = [Select MasterLabel from CategoryNode where MasterLabel = 'Level1.1' LIMIT 1];
            CategoryData cd1 = new CategoryData(RelatedSObjectId=mySolutions[0].Id, CategoryNodeId=cnChild.Id); insert cd1;
            CategoryData cd2 = new CategoryData(RelatedSObjectId=mySolutions[1].Id, CategoryNodeId=cnParent.Id); insert cd2;
            
            //Add 2 Attachments to Solution 1
            Attachment a1 = new Attachment(Name='MyFile-1', ContentType='application/pdf', Body=Blob.valueOf('BLAHBLAH'), ParentId = mySolutions[0].Id);
            Attachment a2 = new Attachment(Name='MyFile-2', ContentType='application/pdf', Body=Blob.valueOf('BLAHBLAH'), ParentId = mySolutions[0].Id);
            insert new List<Attachment>{a1, a2};
            
            //Submit Batch Job            
            Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(JobType__c='S2K', Status__c='Duplicate - Rejected', 
				//Article_Fields__c = 'Resolution__c, Title',
				//Solution_Fields__c = 'SolutionNote, SolutionName',
				Article_Type__c = 'S2K1235__kav'
				//Attachment_Fields__c = 'Attachment_1__c,Attachment_2__c',
				//WHERE_Clause__c = '', //'SolutionName LIKE \'Hello World%\'',
				//LIMIT_Clause__c = '150',
				//Publish_after_Import__c = true
				);
			insert bjic;
			
			//TODO: Read Articles created & ASSERT
								                                  
            Test.stopTest();
        }
        catch (Exception e){
            System.debug(e.getTypeName() + '>>' + e.getMessage());
        }
    }   
    
    // Test Batch Apex Job and Batch Job Invoker Trigger
    public static testMethod void test3(){       
        try{
            Test.startTest();          
            
            //Create two Solutions
            List<Solution> mySolutions = new List<Solution>();
            Solution s1 = new Solution(SolutionNote='Hello World Solution Note -1', SolutionName='Hello World Solution Name -1', isMigrated__c=false);  mySolutions.add(s1); 
            Solution s2 = new Solution(SolutionNote='Hello World Solution Note -2', SolutionName='Hello World Solution Name -2', isMigrated__c=false);  mySolutions.add(s2);
            insert mySolutions;            
            
            CategoryNode cnParent = [Select MasterLabel from CategoryNode where MasterLabel = 'Level1' LIMIT 1];
            CategoryNode cnChild = [Select MasterLabel from CategoryNode where MasterLabel = 'Level1.1' LIMIT 1];
            CategoryData cd1 = new CategoryData(RelatedSObjectId=mySolutions[0].Id, CategoryNodeId=cnChild.Id); insert cd1;
            CategoryData cd2 = new CategoryData(RelatedSObjectId=mySolutions[1].Id, CategoryNodeId=cnParent.Id); insert cd2;
            
            //Add 2 Attachments to Solution 1
            Attachment a1 = new Attachment(Name='MyFile-1', ContentType='application/pdf', Body=Blob.valueOf('BLAHBLAH'), ParentId = mySolutions[0].Id);
            Attachment a2 = new Attachment(Name='MyFile-2', ContentType='application/pdf', Body=Blob.valueOf('BLAHBLAH'), ParentId = mySolutions[0].Id);
            insert new List<Attachment>{a1, a2};
            
            //Submit Batch Job            
            Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(JobType__c='S2K', Status__c='Rejected - Use Advanced Option', 
				//Article_Fields__c = 'Resolution__c, Title',
				//Solution_Fields__c = 'SolutionNote, SolutionName',
				Article_Type__c = 'S2K1235__kav'
				//Attachment_Fields__c = 'Attachment_1__c,Attachment_2__c',
				//WHERE_Clause__c = '', //'SolutionName LIKE \'Hello World%\'',
				//LIMIT_Clause__c = '150',
				//Publish_after_Import__c = true
				);
			insert bjic;
			
			//TODO: Read Articles created & ASSERT
								                                  
            Test.stopTest();
        }
        catch (Exception e){
            System.debug(e.getTypeName() + '>>' + e.getMessage());
        }
    } 
    
     // Test Batch Apex Job and Batch Job Invoker Trigger
     //Test Nulls - i.e. No Solutions
    public static testMethod void test4(){       
        try{
            Test.startTest();           
            
            //Submit Batch Job            
            Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(JobType__c='S2K', Status__c='Custom Fields Creation Completed', 
				//Article_Fields__c = 'Resolution__c, Title',
				//Solution_Fields__c = 'SolutionNote, SolutionName',
				Article_Type__c = 'S2K1235__kav'
				//Attachment_Fields__c = 'Attachment_1__c,Attachment_2__c',
				//WHERE_Clause__c = '', //'SolutionName LIKE \'Hello World%\'',
				//LIMIT_Clause__c = '150',
				//Publish_after_Import__c = true
				);
			insert bjic;
			
			//TODO: Read Articles created & ASSERT
								                                  
            Test.stopTest();
        }
        catch (Exception e){
            System.debug(e.getTypeName() + '>>' + e.getMessage());
        }
    }                
}