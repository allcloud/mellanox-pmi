public class vfRMAForm
{

    public boolean isInternal {get;set;}
    public RMA__c MainRMA     {get;set;}
    public RMA__c RMAAfterInsert {get;set;}
    public static RMA__c staticMainRMA {get;set;}
    public Id MainRMAId       {get;set;}
    public List<SerialNumber> lstSerialNumber {get;set;}
    public Boolean IsSitesUser {get;set;}
    public Boolean EndOfPage   {get;set;}
    public Boolean serialsIsOk   {get;set;}
    public String ErrMsg       {get;set;}
    public Integer UrlStatus   {get;set;}
    public String retUrl;
    public Boolean showExtraSection {get;set;}
    public Boolean consentSubmitted{get;set;}
    private boolean validated {get;set;}
    private My_Mellanox_Setting__c settings = My_Mellanox_Setting__c.getInstance();
    private List<Serial_Number__c> insSerials = new List<Serial_Number__c>();
    
    
    /*************  INNER CLASS ****************/
    public class SerialNumber
    {
        public Serial_Number__c serial {get;set;}
        public Integer ind {get;set;}
        
        public SerialNumber(Serial_Number__c serial,Integer ind )
        {
            this.serial = serial;
            this.ind = ind;
        }
    }
    /*************  INNER CLASS ****************/
    
    
    public vfRMAForm(ApexPages.StandardController controller) {
        validated = true;
        consentSubmitted = false;
        showExtraSection = false;
        serialsIsOk = false;
        ErrMsg = '';
        system.debug('inside 38');
        String i = Apexpages.currentPage().getParameters().get('type');
        if (i == '1') isInternal = true;
        else isInternal = false;
        retUrl = system.currentPageReference().getParameters().get('retUrl');
        resetForm();
        FillFormFromCase();
    }

    public void FillFormFromCase() {

        ID currentCaseId = system.currentPageReference().getParameters().get(ENV.CaseFieldInRMA);
        
         if (currentCaseId != null) {

            List < Case > CaseList = [Select c.Id, c.Account.ShippingPostalCode, c.Account.ShippingCity, c.Account.ShippingStreet, c.Account.BillingCity, c.Account.BillingStreet, c.Contact.Email, c.Contact.Phone, c.Contact.Fax, c.Contact.Name, c.Contact.Id, c.ContactId, c.Account.Phone, c.Account.BillingPostalCode, c.Account.Name, c.Account.Id, c.AccountId, c.caseNumber From Case c where c.Id = : currentCaseId];
            if (CaseList.size() > 0) {

                Case CurrCase = CaseList[0];
                MainRMA.Case_Parent__c = CurrCase.Id;
                MainRMA.Case_Number__c = CurrCase.caseNumber;
                MainRMA.Name__c = CurrCase.Contact.Name;
                MainRMA.E_Mail__c = CurrCase.Contact.Email;
                MainRMA.Contact_Phone__c = CurrCase.Contact.Phone;
                MainRMA.Fax__c = CurrCase.Contact.Fax;
                MainRMA.Contact__c = CurrCase.Contact.Id;
                MainRMA.Account_Name__c = CurrCase.AccountId;
                MainRMA.Account_Name_text__c = CurrCase.Account.Name;
                MainRMA.Bill_Company__c = CurrCase.Account.Name;
                MainRMA.Bill_Contact__c = CurrCase.Contact.Name;
                MainRMA.Bill_Phone__c = CurrCase.Account.Phone;
                MainRMA.Bill_Zip__c = CurrCase.Account.BillingPostalCode;
                MainRMA.Bill_City_State__c = CurrCase.Account.BillingCity;
                MainRMA.Bill_Company_Address_1__c = CurrCase.Account.BillingStreet;
                MainRMA.Bill_Zip__c = CurrCase.Account.BillingPostalCode;
                MainRMA.Ship_Company__c = CurrCase.Account.Name;
                MainRMA.Ship_Contact__c = CurrCase.Contact.Name;
                MainRMA.Ship_Phone__c = CurrCase.Account.Phone;
                MainRMA.Ship_Zip__c = CurrCase.Account.ShippingPostalCode;
                MainRMA.Ship_City_State__c = CurrCase.Account.ShippingCity;
                MainRMA.Ship_Company_Address_1__c = CurrCase.Account.ShippingStreet;



            }
        }

        String uRoleId = UserInfo.getUserRoleId();

        if (uRoleId != null && uRoleId.contains('00E50000000loto')) {

            MainRMA.Bill_Company_Address_1__c = '.C/O EXEL LOGISTICS 4501';
            MainRMA.Bill_Company_Address_2__c = 'BLALOCK';
            MainRMA.Bill_Country__c = 'USA';
            MainRMA.Bill_City_State__c = 'HOUSTON, Texas';
            MainRMA.Bill_Zip__c = '77041';
            MainRMA.Ship_Company__c = 'HEWLETT-PACKARD';
            MainRMA.Ship_Zip__c = MainRMA.Bill_Zip__c;
            MainRMA.Ship_City_State__c = MainRMA.Bill_City_State__c;
            MainRMA.Ship_Company_Address_1__c = MainRMA.Bill_Company_Address_1__c;
            MainRMA.Ship_Country__c = MainRMA.Bill_Country__c;
            MainRMA.Ship_Company_Address_2__c = MainRMA.Bill_Company_Address_2__c;
            MainRMA.Ship_Contact__c = MainRMA.Bill_Contact__c;
            MainRMA.Ship_Phone__c = MainRMA.Bill_Phone__c;
        }
    }

    public Pagereference SaveFormAndAddAttachmentWhenConsent() {

        consentSubmitted = true;

        PAgeReference page = SaveFormAndAddAttachment();

        return page;
    }

    // This method is called upon hiting the "Submit and Add Attachment" Button
    public Pagereference SaveFormAndAddAttachment() {

        set < String > partNumbers_Set = new set < String > ();
        if (!lstSerialNumber.isEmpty()) {

            String partNumOnlyLetters_Str;
            for (SerialNumber s: lstSerialNumber) {

                if ((s.serial.Part_Number__c != null && s.serial.Part_Number__c != '') &&
                    (s.serial.Serial__c != null && s.serial.Serial__c != '') &&
                    ((s.serial.Problem_Description__c != null && s.serial.Problem_Description__c != '') && (s.serial.DOA_new__c != null && s.serial.DOA_new__c != ''))) {

                    partNumOnlyLetters_Str = s.serial.Part_Number__c.replaceAll('[^a-zA-Z]', '');
                    partNumbers_Set.add(partNumOnlyLetters_Str);
                }
            }
        }


        if (!consentSubmitted) {

            try {

                list < Product2 > productsWithPartNumbers_List = DAL_Product.getProductsByPartNumbers();
                system.debug('productsWithPartNumbers_List : ' + productsWithPartNumbers_List);
                if (!productsWithPartNumbers_List.isEmpty()) {
                    system.debug('inside 120');
                    for (Product2 product: productsWithPartNumbers_List) {

                        String productNameOnlyLettersStr = product.Name.replaceAll('[^a-zA-Z]', '');

                        if (partNumbers_Set.contains(productNameOnlyLettersStr)) {
                            system.debug('inside 126');
                            showExtraSection = true;
                            return null;
                        }
                    }
                }
            }

            catch (Exception e) {

                system.debug('inside 382');
            }
        }

        system.debug('==>MainRMA 79' + mainRMA);
        Save();
        system.debug('==>RMAAfterInsert 82' + RMAAfterInsert);
        system.debug('==>ErrMsg ' + ErrMsg);
        system.debug('==>MainRMA.Id 84' + mainRMA.Id);
        if (ErrMsg == '' && RMAAfterInsert.Id != null) {
            system.debug('upload the attachment ');
            Pagereference p = new Pagereference('/apex/AddAttachmentToRMA?RMAId=' + RMAAfterInsert.ID);
            return p;
        }
        return null;
    }

    public void save() {

        system.debug('==>MainRMA 96 ' + MainRMA);
        system.debug('==>lstSerialNumber ' + lstSerialNumber);
        ErrMsg = '';
        List < US_Territory_Map__c > territoryMap = new List < US_Territory_Map__c > ();
        set < String > partNumbers_Set = new set < String > ();
        set < String > partNumbersDG_set = new set < String > ();

        if (MainRMA.ShipTo_same_as_BillTo__c) {

            MainRMA.Ship_Company__c = MainRMA.Bill_Company__c;
            MainRMA.Ship_Company_Address_1__c = MainRMA.Bill_Company_Address_1__c;
            MainRMA.Ship_Contact__c = MainRMA.Bill_Contact__c;
            MainRMA.Ship_Company_Address_2__c = MainRMA.Bill_Company_Address_2__c;
            MainRMA.Ship_Phone__c = MainRMA.Bill_Phone__c;
            MainRMA.Ship_City_State__c = MainRMA.Bill_City_State__c;
            MainRMA.Ship_Country__c = MainRMA.Bill_Country__c;
            MainRMA.Ship_Zip__c = MainRMA.Bill_Zip__c;
        }
       
       if(MainRMA.RMA_state__c== 'Pending Sales Approval') { MainRMA.RMA_Status__c = 'Open';}  
       if(MainRMA.RMA_state__c== 'Approved') { MainRMA.RMA_Status__c= 'Approved';}  
       if(MainRMA.RMA_state__c== 'Support Group Approval') { MainRMA.RMA_Status__c= 'Assigned';}  
       if(MainRMA.RMA_state__c== 'Execution in process') { MainRMA.RMA_Status__c= 'Approved';}  
       if(MainRMA.RMA_state__c== 'Rejected') { MainRMA.RMA_Status__c= 'Rejected';}   
   
        Boolean AllSerialsFilled = true;
        for (SerialNumber s: lstSerialNumber) {

            system.debug('SerialNumber 127 :' + s);
            if (((s.serial.Part_Number__c != null && s.serial.Part_Number__c != '') ||
                    (s.serial.Serial__c != null && s.serial.Serial__c != '') ||
                    ((s.serial.Problem_Description__c != null && s.serial.Problem_Description__c != '') && (s.serial.DOA_new__c != null && s.serial.DOA_new__c != ''))) &&
                !((s.serial.Part_Number__c != null && s.serial.Part_Number__c != '') &&
                    (s.serial.Serial__c != null && s.serial.Serial__c != '') &&
                    ((s.serial.Problem_Description__c != null && s.serial.Problem_Description__c != '') && (s.serial.DOA_new__c != null && s.serial.DOA_new__c != ''))))

            {
                AllSerialsFilled = false;
            }
        }

        if (AllSerialsFilled) {
            system.debug('lstSerialNumber :' + lstSerialNumber);
            for (SerialNumber s: lstSerialNumber) {
                if ((s.serial.Part_Number__c != null && s.serial.Part_Number__c != '') &&
                    (s.serial.Serial__c != null && s.serial.Serial__c != '') &&
                    ((s.serial.Problem_Description__c != null && s.serial.Problem_Description__c != '') && (s.serial.DOA_new__c != null && s.serial.DOA_new__c != '')))

                {
                    s.serial.Serial__c = s.serial.Serial__c.deleteWhitespace().toUpperCase();
                    partNumbers_Set.add(s.serial.Part_Number__c);
                    insSerials.Add(s.serial);
                }
            }

            if (!partNumbers_Set.isEmpty()) {

                list < Product2 > productsWithPartNumbers_List = DAL_Product.getProductsByPartNumbers();

                if (!productsWithPartNumbers_List.isEmpty()) {

                    for (Product2 product: productsWithPartNumbers_List) {

                        String partNumOnlyLetters = product.Part_Number__c.replaceAll('[^a-zA-Z]', '');
                        partNumbersDG_set.add(partNumOnlyLetters);
                    }
                }
            }

            system.debug('insSerials :' + insSerials);

            if (insSerials.size() > 0) {

                validated = validateShipToExport(MainRMA);
                if (!validated) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select fill in whether you would like to Ship to Export To Processing Zone'));
                }

                else {

                    if (MainRMA != null) {

                        insert MainRMA;
                        system.debug('==>MainRMA 153 ' + MainRMA);
                        RMAAfterInsert = new RMA__c();
                        RMAAfterInsert = [SELECT Id, Name FROM RMA__c WHERE Id = : MainRMA.Id];
                        system.debug('==>MainRMA ID 155 ' + MainRMA.Id);
                    }

                    for (Serial_Number__c s: insSerials) {


                        if (!partNumbersDG_set.isEmpty()) {

                            String partNumOnlyLetters_Str = s.Part_Number__c.replaceAll('[^a-zA-Z]', '');

                            if (partNumbersDG_set.contains(partNumOnlyLetters_Str)) {

                                s.Dangerous_goods__c = true;
                            }
                        }
                        s.RMA__c = MainRMA.Id;
                    }

                    if (lstSerialNumber.size() > 0) {

                        linkAsset2SerialNumber();

                        insert insSerials;
                        system.debug('==>insSerials ' + insSerials);
                    }


                    validated = validateShipToExport(MainRMA);
                    resetForm();
                    EndOfPage = true;

                    //sandbox
                    ID guestUserID = ENV.SiteUser;

                    User currentUser = [SELECT Id, UserType, ProfileId FROM User WHERE Id = : guestUserID];

                    String MellanoxWebSite = Site.getCurrentSiteUrl();

                    system.debug('currentUser.UserType 281 : ' + currentUser.UserType);
                    system.debug('currentUser.ProfileId 282 : ' + currentUser.ProfileId);

                    if (MellanoxWebSite != null && (MellanoxWebSite.containsIgnoreCase('mellanox.com') || MellanoxWebSite.containsIgnoreCase('mellanox.secure'))) {

                        UrlStatus = 1;
                    }

                    else if (currentUser.UserType.EqualsIgnoreCase('PowerPartner')) {


                        if (currentUser.ProfileId == settings.MyMellanox_Distributor_User__c || currentUser.ProfileId == settings.MyMellanox_Sales_User__c) {

                            UrlStatus = 4;
                        }

                        else {
                            UrlStatus = 2;
                        }

                    }

                    else {

                        system.debug('==>guestUser ');
                        // Pagereference p = new Pagereference(ENV.refrenceToRMA);
                        UrlStatus = 0;
                    }
                }
            }

            else {
                ErrMsg = 'You Must Fill At Least One Serial';
                UrlStatus = 2;
            }
        }

        else {
            ErrMsg = 'All serials must have part number, serial number, description and DOA ';
            UrlStatus = 2;
        }

        if (!validated) {

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'When the Shipping Country is China, you must select Export To Proccesing Zone'));
            return;
        }
    }
    private void linkAsset2SerialNumber() {

        set < String > serialNumbers_Set = new set < String > ();
        set < String > partNumbers_Set = new set < String > ();
        map < string, Asset2__c > assets_Map = new map < string, Asset2__c > ();
		
        for (Serial_Number__c s: insSerials) {

            serialNumbers_Set.add(s.Serial__c);
            partNumbers_Set.add(s.Part_Number__c);
            
        }

        if (!serialNumbers_Set.isEmpty() && !partNumbers_Set.isEmpty()) {

            list < Asset2__c > assets_List = [select Name, Part_Number__c, Product__r.Product_Type1__c, Asset_Status__c from Asset2__c where Name in : serialNumbers_Set and Part_Number__c in : partNumbers_Set];
			string serialNumberANDpartNumber;
			
            for (Asset2__c asset: assets_List) {

                serialNumberANDpartNumber = asset.Name + ' ' + asset.Part_Number__c;
                serialNumberANDpartNumber = serialNumberANDpartNumber.toUpperCase();
                assets_Map.put(serialNumberANDpartNumber, asset);
            }

            for (Serial_Number__c s: insSerials) {

                serialNumberANDpartNumber = s.Serial__c + ' ' + s.Part_Number__c;
                serialNumberANDpartNumber = serialNumberANDpartNumber.toUpperCase();
                
                if (assets_Map.containsKey(serialNumberANDpartNumber)) {
                    s.Asset__c = assets_Map.get(serialNumberANDpartNumber).id;
                    if(assets_Map.get(serialNumberANDpartNumber).Product__r != null){
                    	s.RMA_Product_Family__c = assets_Map.get(serialNumberANDpartNumber).Product__r.Product_Type1__c;
                    }
                }
            }
        }
    }

    public void submit() {
        save();
        system.debug('==>ErrMsg 216' + ErrMsg);
        system.debug('==>MainRMA.Id 217 ' + MainRMA.Id);
        system.debug('==>MainRMA 217 ' + MainRMA);

        if (ErrMsg == '' && MainRMA.Id != null) {
            system.debug('==>submit ');
            resetForm();
            EndOfPage = true;

            //sandbox
            ID guestUserID = ENV.SiteUser;
            if (Userinfo.getUserId() != guestUserID) {
                system.debug('==>guestUser ');
                Pagereference p = new Pagereference(ENV.refrenceToRMA);
                UrlStatus = 0;
            }
            else {
                system.debug('==>rmahome ');
                Pagereference p = new Pagereference('http://www.mellanox.com/content/pages.php?pg=rma&action=success');
                UrlStatus = 1;
            }
        }
    }

    // This method will be initiated once the user has agreed to the terms & conditions and pressed the
    // 'Submit consent' button
    public void submitConsent() {

        consentSubmitted = true;
        SaveForm();
    }

    public void SaveForm() {
        ErrMsg = '';
        List < US_Territory_Map__c > territoryMap = new List < US_Territory_Map__c > ();
        set < String > partNumbersDG_set = new set < String > ();

        String emailAddress = MainRMA.E_Mail__c;
        system.debug('emailAddress : ' + emailAddress);
        // List of contacts retrieved from the DB if exist by Email

        Contact contactFound = new Contact();

        try {

            contactFound = [SELECT Id, AccountId FROM Contact WHERE Email = : emailAddress limit 1];
        }

        catch (Exception e) {

            system.debug('No contact found : ' + e.getMessage());
        }
        // a set of strings that will hold all the Part numbers filled up by the user
        // to retrieve the relevant products associated(Part Number is the Product Name)
        set < String > partNumbers_Set = new set < String > ();

        if (contactFound.Id != null) {

            User user = new User();

            try {

                user = [SELECT Id, IsActive FROM User WHERE ContactId = : contactFound.Id AND IsActive = true];
            }

            catch (Exception e) {

                e.getMessage();
            }

            if (user.Id != null && user.IsActive) {

                MainRMA.OwnerId = user.Id;
            }
            MainRMA.Contact__c = contactFound.Id;
            MainRMA.Account_Name__c = contactFound.AccountId;
        }

        else {

            cls_Contact_Trigger handler = new cls_Contact_Trigger();
            Account relatedAccount;

            try {
                relatedAccount = handler.getAccountByEmailDomain(emailAddress, null);
            }

            catch (Exception ex) {

                system.debug('Could not find Account : ' + ex.getMessage());
            }

            if (relatedAccount != null && relatedAccount.Id != null) {
                contactFound.AccountId = relatedAccount.Id;
                MainRMA.Account_Name__c = relatedAccount.Id;
            }

            else {
                relatedAccount.Id = ENV.UnknownAccount;
                contactFound.AccountId = relatedAccount.Id;
                MainRMA.Account_Name__c = relatedAccount.Id;
            }


            if (contactFound.Id == null) {

                contactFound.Email = emailAddress;
                contactFound.Phone = MainRMA.Contact_Phone__c;
                contactFound.MailingCity = MainRMA.Bill_City_State__c;
                contactFound.MailingCountry = MainRMA.Bill_Country__c;
                contactFound.MailingPostalCode = MainRMA.Bill_Zip__c;
                contactFound.MailingStreet = MainRMA.Bill_Company_Address_1__c;

                String contactFullName;
                if (MainRMA.Name__c != null) {

                    contactFullName = MainRMA.Name__c;
                }

                list < String > namesParts_List = contactFullName.split(' ');

                String firstName = namesParts_List[0];
                String lastName;

                if (namesParts_List.size() == 1) {

                    lastName = firstName;
                }

                else if (namesParts_List.size() > 2) {
                    lastName = namesParts_List[2];
                }

                else {

                    lastName = namesParts_List[1];
                }

                contactFound.FirstName = firstName;
                contactFound.LastName = lastName;
                // Will be filed up with First Name
                // Will be filed up with Last Name


                system.debug('contactFound.AccountId : ' + contactFound.AccountId);
                system.debug('contactFound : ' + contactFound);

                insert contactFound;
                MainRMA.Contact__c = contactFound.Id;
            }
        }

        if (MainRMA.ShipTo_same_as_BillTo__c) {
            MainRMA.Ship_Company__c = MainRMA.Bill_Company__c;
            MainRMA.Ship_Company_Address_1__c = MainRMA.Bill_Company_Address_1__c;
            MainRMA.Ship_Contact__c = MainRMA.Bill_Contact__c;
            MainRMA.Ship_Company_Address_2__c = MainRMA.Bill_Company_Address_2__c;
            MainRMA.Ship_Phone__c = MainRMA.Bill_Phone__c;
            MainRMA.Ship_City_State__c = MainRMA.Bill_City_State__c;
            MainRMA.Ship_Country__c = MainRMA.Bill_Country__c;
            MainRMA.Ship_Zip__c = MainRMA.Bill_Zip__c;
        }
        
       if(MainRMA.RMA_state__c== 'Pending Sales Approval') { MainRMA.RMA_Status__c = 'Open';}  
       if(MainRMA.RMA_state__c== 'Approved') { MainRMA.RMA_Status__c= 'Approved';}  
       if(MainRMA.RMA_state__c== 'Support Group Approval') { MainRMA.RMA_Status__c= 'Assigned';}  
       if(MainRMA.RMA_state__c== 'Execution in process') { MainRMA.RMA_Status__c= 'Approved';}  
       if(MainRMA.RMA_state__c== 'Rejected') { MainRMA.RMA_Status__c= 'Rejected';}                  
   
        Boolean AllSerialsFilled = true;
        for (SerialNumber s: lstSerialNumber) {
            if (((s.serial.Part_Number__c != null && s.serial.Part_Number__c != '') ||
                    (s.serial.Serial__c != null && s.serial.Serial__c != '') ||
                    ((s.serial.Problem_Description__c != null && s.serial.Problem_Description__c != '') && (s.serial.DOA_new__c != null && s.serial.DOA_new__c != ''))) &&
                !((s.serial.Part_Number__c != null && s.serial.Part_Number__c != '') &&
                    (s.serial.Serial__c != null && s.serial.Serial__c != '') &&
                    ((s.serial.Problem_Description__c != null && s.serial.Problem_Description__c != '') && (s.serial.DOA_new__c != null && s.serial.DOA_new__c != '')))) {
                AllSerialsFilled = false;
            }
        }

        if (AllSerialsFilled) {

            for (SerialNumber s: lstSerialNumber) {
                if ((s.serial.Part_Number__c != null && s.serial.Part_Number__c != '') &&
                    (s.serial.Serial__c != null && s.serial.Serial__c != '') &&
                    ((s.serial.Problem_Description__c != null && s.serial.Problem_Description__c != '') && (s.serial.DOA_new__c != null && s.serial.DOA_new__c != ''))) {
                    s.serial.Serial__c = s.serial.Serial__c.deleteWhitespace().toUpperCase();
                    s.serial.Part_Number__c = s.serial.Part_Number__c.trim().toUpperCase();

                    partNumbers_Set.add(s.serial.Part_Number__c);
                    system.debug('s.serial.Part_Number__c ' + s.serial.Part_Number__c);
                    insSerials.Add(s.serial);
                }
            }

            list < Product2 > productsWithPartNumbers_List = new list < Product2 > ();
            if (!partNumbers_Set.isEmpty()) {

                productsWithPartNumbers_List = DAL_Product.getProductsByPartNumbers();
            }

            if (!consentSubmitted) {

                try {

                    if (!productsWithPartNumbers_List.isEmpty()) {

                        for (Product2 product: productsWithPartNumbers_List) {

                            String productNumOnlyLetters = product.Name; //product.Name.replaceAll('[^a-zA-Z]', '');

                            if (partNumbers_Set.contains(productNumOnlyLetters)) {
                                showExtraSection = true;
                                return;
                            }
                        }
                    }
                }

                catch (Exception e) {

                    system.debug('inside 504');
                }
            }

            if (!productsWithPartNumbers_List.isEmpty()) {

                for (Product2 product: productsWithPartNumbers_List) {
                    system.debug('inside 511');
                    partNumbersDG_set.add(product.Part_Number__c);
                }
            }

            if (insSerials.size() > 0) {

                validated = validateShipToExport(MainRMA);
                if (!validated) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'When the Shipping Country is China, you must select Export To Proccesing Zone'));

                }

                else {

                    if (MainRMA != null) {
                        staticMainRMA = MainRMA;
                        insert MainRMA;
                    }

                    for (Serial_Number__c s: insSerials) {
                        if (partNumbersDG_set.contains(s.Part_Number__c)) {
                            system.debug('inside 529');
                            s.Dangerous_goods__c = true;
                        }
                        s.RMA__c = MainRMA.Id; 
                    }

                    if (lstSerialNumber.size() > 0) {
                        linkAsset2SerialNumber();
				
                        insert insSerials;
                        
                        if(insSerials[0].Source__c == 'NPU' ){
		            		MainRMA.Mellanox_Site__c = ENV.Yokneam_Mellanox_Site;
		            	}
		            	else if(insSerials[0].Source__c == 'MC' ){
		            		MainRMA.Mellanox_Site__c = ENV.US_Mellanox_Site;
		            	}
                        
                        try {
				            update MainRMA;
				        }
				        catch (Exception e) {
				            system.debug('Can not update Mellanox Site on RMA: ' + e.getMessage());
				        }
                    }

					
                    resetForm();
                    EndOfPage = true;

                    //sandbox
                    ID guestUserID = ENV.SiteUser;
                    Id currentUserId = system.UserInfo.getUserId();

                    User currentUser = [SELECT Id, UserType, ProfileId FROM User WHERE Id = : currentUserId];

                    system.debug('currentUser UserType : ' + currentUser.UserType);

                    String MellanoxWebSite = Site.getCurrentSiteUrl();

                    system.debug('MellanoxWebSite : ' + MellanoxWebSite);
                    system.debug('currentUser.UserType : ' + currentUser.UserType);
                    system.debug('currentUser.ProfileId : ' + currentUser.ProfileId);

                    if (MellanoxWebSite != null && (MellanoxWebSite.containsIgnoreCase('mellanox.com') || MellanoxWebSite.containsIgnoreCase('mellanox.secure'))) {

                        UrlStatus = 1;
                    }

                    else if (currentUser.UserType.EqualsIgnoreCase('PowerPartner')) {

                        if (currentUser.ProfileId == settings.MyMellanox_Distributor_User__c || currentUser.ProfileId == settings.MyMellanox_Sales_User__c) {

                            UrlStatus = 4;
                        }

                        else {
                            UrlStatus = 2;
                        }
                    }

                    else {

                        system.debug('==>guestUser ');
                        // Pagereference p = new Pagereference(ENV.refrenceToRMA);
                        UrlStatus = 0;
                    }
                }
            }
            else {
                ErrMsg = 'You Must Fill At Least One Serial';
                UrlStatus = 2;
            }
        }
        else {
            ErrMsg = 'All serials must have part number, serial number, description and DOA ';
            UrlStatus = 2;
        }
    }

    @future
    public static void insertRMAAndSerials() {

        insert staticMainRMA;
    }


    public Pagereference Cancel() {
        Pagereference p = new Pagereference(retUrl);
        return p;
    }
    public void resetForm() {
        IsSitesUser = true;
        EndOfPage = false;
        ErrMsg = '';
        UrlStatus = 2;
        MainRMA = new RMA__c(Date_Submitted__c = system.today());
        lstSerialNumber = new List < SerialNumber > ();
        MainRMA.ShipTo_same_as_BillTo__c = false;

        if (Userinfo.getUserId() != ENV.SiteUser) {
            MainRMA.OwnerId = Userinfo.getUserId();
        }
        else {
            MainRMA.OwnerId = ENV.QueueMap.get('RMA');
        }

        if (isInternal == false) {
            for (Integer i = 1; i <= 1; i++) {
                lstSerialNumber.Add(new SerialNumber(new Serial_Number__c(), i));
            }
        }

        if (isInternal == true) {
            for (Integer i = 1; i <= 1; i++) {
                lstSerialNumber.Add(new SerialNumber(new Serial_Number__c(), i));
            }
        }

        ID conID = system.currentPageReference().getParameters().get(ENV.CaseFieldInRMA);
                system.debug('conID : ' + conID);
        
        
        List < Contact > conList = [select Account.BillingPostalCode, Account.Name, Account.BillingState, Account.BillingStreet, Account.BillingCountry, Account.BillingCity, Account.Phone, Name, AccountId, Email, Phone, Fax, Id from Contact where Id = : conID];
        User usr = [select Id, Contact.Account.BillingPostalCode, Contact.Account.Name, Contact.Account.BillingState, Contact.Account.BillingStreet, Contact.Account.BillingCountry, Contact.Account.BillingCity, Contact.Account.Phone, Contact.Name, Contact.AccountId, Contact.Email, Contact.Phone, Contact.Fax, ContactId from User where Id = : Userinfo.getUserId()];
        Contact con;
        if (usr.ContactId != null) {
            con = usr.Contact;
        }
        else if (conList.size() > 0) {
            con = conList[0];
        }
        if (con != null) {
            //get info from the account and add it to the RMA
            IsSitesUser = false;
            MainRMA.Name__c = con.Name;
            MainRMA.E_Mail__c = con.Email;
            MainRMA.Contact_Phone__c = con.Phone;
            MainRMA.Fax__c = con.Fax;
            MainRMA.Contact__c = con.Id;
            MainRMA.Account_Name__c = con.AccountId;
            MainRMA.Account_Name_text__c = con.Account.Name;
            MainRMA.Bill_Company__c = con.Account.Name;
            MainRMA.Bill_Contact__c = con.Name;
            MainRMA.Bill_Phone__c = con.Account.Phone;
            MainRMA.Bill_Country__c = con.Account.BillingCountry;
            MainRMA.Bill_Zip__c = con.Account.BillingPostalCode;
        }
    }


    public List < selectOption > getFailureAnalysis() {
        List < selectoption > FAoptions = new List < selectOption > ();

        FAoptions.Add(new selectoption('No', 'No'));
        FAoptions.Add(new selectoption('Yes', 'Yes'));

        return FAoptions;
    }

    public List < selectOption > getAllCountries() {
        List < Regions__c > countryOptions = [Select Id, Name From Regions__c where IsDeleted = false Order By Name];

        set < string > countryset = new set < string > ();
        List < String > lst_country = new List < String > ();
        for (Regions__c c: countryOptions) {
            if (!countryset.contains(c.Name) && c.Name != NULL) {
                countryset.Add(c.Name);
                lst_country.Add(c.Name);
            }
        }
        List < selectoption > coptions = new List < selectOption > ();
        coptions.Add(new selectoption('', '- None -'));

        for (String s: lst_country) {

            coptions.Add(new selectoption(s, s));
        }


        return coptions;
    }

    public void AddSerial() {
        Serial_Number__c[] lst = new Serial_Number__c[]{};
        for (SerialNumber Item : lstSerialNumber) 
        	lst.add(Item.serial);
        	
        if (cls_trg_RMA_Serial_Numbers.findAndAssignAsset(lst) == true) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'EZChip products (NPU) and Tillera products (MC) cannot be processed together as a single RMA,  please submit separate RMA’s'));
            serialsIsOk = false;
            return;
        }
        serialsIsOk = true;
        if (lstSerialNumber.size() < 40) {
            lstSerialNumber.Add(new SerialNumber(new Serial_Number__c(), lstSerialNumber.size() + 1));
        }
        //need check entered serials
        
        
    }
    public void RemoveSerial() {
        if (lstSerialNumber.size() > 1) {
            lstSerialNumber.remove(lstSerialNumber.size() - 1);
        }
    }
    public void ShipToSameAsBillTo() {
        MainRMA.ShipTo_same_as_BillTo__c = !MainRMA.ShipTo_same_as_BillTo__c;
    }

    private boolean validateShipToExport(RMA__c MainRMA) {

        system.debug('MainRMA :' + MainRMA);
        system.debug('MainRMA.Ship_Country__c :' + MainRMA.Ship_Country__c);
        system.debug('MainRMA.Bill_Country__c :' + MainRMA.Bill_Country__c);
        boolean validated = false;

        if (MainRMA != null) {

            if (MainRMA.Ship_Country__c != null && MainRMA.Ship_Country__c.EqualsIgnoreCase('China')) {

                if (MainRMA.Ship_to_Export_Processing_Zone__c == null) {

                    validated = false;
                }

                else {

                    validated = true;
                }
            }

            else {

                validated = true;
            }
        }

        return validated;
    }

}