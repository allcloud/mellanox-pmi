public with sharing class mlnxMobileAssetViewController {
	
 	public String selectedDocType {get;set;}
 	public list<SelectOption> docTypes_list {get;set;}
 	public Id asstId {get;set;}
 	public list<ContentVersion> popularVersion_List {get;set;}
 	public string currentUserMail {get;set;}
 	public boolean displayAssetNotFound {get;set;}
 	public Asset2__c currentAsset {get;set;}
	
	public mlnxMobileAssetViewController(ApexPages.StandardController controller) {
		
		asstId = controller.getId();
		map<Integer, String> counter2DocTypes_Map = new map<Integer, String>();
		
		if (asstId == null) {
			
			displayAssetNotFound = true;
			return;
		}
		
		popularVersion_List = new list<ContentVersion>();
		currentUserMail = UserInfo.getUserEmail();
		list<Asset2__c> relatedAssets_List = new list<Asset2__c>();
		currentAsset  = new Asset2__c();
		 
	    relatedAssets_List = [SELECT EOL_Check__c,
						      a.price_number__c, a.notify_expire_date__c, a.expore_date_backup__c, a.expected_ship_date__c,
						      a.days_to_notification__c, a.contract_start_date__c, a.batch__c, a.Zip_Code__c,
						      a.Write_Off__c, a.Warranty_months__c, a.Warranty_Expire_N__c, a.Vertical__c,
						      a.Under_Contract__c, a.Total_Price__c, a.To_delete__c, a.SystemModstamp, 
						      a.Support_Asset_Term__c, a.Support_Asset_Term2__c, a.Support_Asset_Term1__c,
						      a.Street_Address__c, a.Status__c, a.StatusDescription__c, a.State__c, 
						      a.Site__c, a.Ship_to_Customer_Account__c, a.Ship_To_Address__c, a.Product__r.Name,
						      a.Serial_Number__c, a.SUN__c, a.SN_lower__c, a.SN_Warranty_Expire__c,
						      a.Qty_Factory_Units__c, a.Project__c, a.Product__c, a.Product_Model_Number__c, 
						      a.Product_Image__c, a.Product_Family__c, a.Price__c, a.Phone_Number__c, a.Pending_Return_Date__c,
						      a.Payment_Terms__c, a.Part_Number__c, a.Part_Num_Quantity__c, a.Part_Description__c, 
						      a.Notification_QT__c, a.Name, a.MyMellanox_asset__c, a.MyMellanox_Contract__c, 
						      a.Moved_from_contract__c, a.Momo_asset__c, a.MobileAPP_Asset__c, Product__r.Image_URL__c,
						      a.Mellanox_Department__c, a.Mellanox_Contact__c, a.MLNX_ASSET__c, Product__r.Product_Detail_Name__c,
						      a.Location__c, a.Loaner_Sample_Type__c, a.Ln__c, a.Is_OEM__c, a.Id, a.CreatedDate,a.Asset_Family__c From Asset2__c a WHERE a.Id =: asstId];
		system.debug('relatedAssets_List : ' + relatedAssets_List);
		
		if (!relatedAssets_List.isEmpty()) {
			
			currentAsset = relatedAssets_List[0];
		}
		
		else {
			
			system.debug('inside 41');
			displayAssetNotFound = true;
			 
		}
		
		counter2DocTypes_Map.put(0,'User Manual');
		counter2DocTypes_Map.put(1,'Release Notes');
		counter2DocTypes_Map.put(3,'Installation Guide');
		counter2DocTypes_Map.put(2,'Quick Start Guide');
		
		//counter2DocTypes_Map.put(3,'BSDL');
		
		if (currentAsset.Product__c != null) {
			
			for (Integer i=0 ; i < 4 ; i ++) {
				
				try {
					 
					String query = 'SELECT  Title, Size__c, Id, Doc_SW_Revision__c, Document_Type__c, Description, Product_Family__c, FileType,' + 
									  ' ContentUrl, Product__c , Category__c FROM ContentVersion WHERE' +
									  ' ( Product__c includes ' +  '(' + '\'' + currentAsset.Product__r.Product_Detail_Name__c + '\'' + ')' + 
									  ' OR Moblie_OPN__c includes ' +  '(' + '\'' + currentAsset.Product__r.Product_Detail_Name__c + '\'' + ')' + 
									  ' OR OEM_Product__c  includes ' +  '(' + '\'' + currentAsset.Product__r.Product_Detail_Name__c  + '\'' + '))' +
									  ' and Document_Type__c = \'' + counter2DocTypes_Map.get(i) + '\'' +
									  ' order by Doc_SW_Revision__c desc limit 1';
					system.debug('query : ' + query);					 
					popularVersion_List.add(database.Query(query));
					
					/*
					popularVersion_List.add([SELECT  Title, Size__c, Id, Doc_SW_Revision__c, Document_Type__c, Description, Product_Family__c, FileType,
											 ContentUrl, Product__c , Category__c FROM ContentVersion 
											 WHERE (Product__c =: relatedProduct.Product_Detail_Name__c
											 OR OEM_Product__c includes relatedProduct.Product_Detail_Name__c) and Document_Type__c =: counter2DocTypes_Map.get(i)
											 order by Doc_SW_Revision__c desc limit 1]);
					 
					 			
					// ************************FOR TESTING  **********************			 
					/* popularVersion_List.add([SELECT  Title, Size__c, Id, Doc_SW_Revision__c, Document_Type__c, Description, Product_Family__c, FileType,
											 ContentUrl, Product__c , Category__c FROM ContentVersion 
											 WHERE Document_Type__c =: counter2DocTypes_Map.get(i) 
											 order by Doc_SW_Revision__c desc limit 1]);
					*/						 
					  						 						 
				}
				
				catch(Exception e) {
					
					
				}
			}
		}
	}
	
	  
	public pageReference moveToContectTablePage() {
		
		 String openerUrl = '/mlnxMobileContentTable?Id=' + asstId + '&selectedDocType=' + selectedDocType ;
         PageReference nextPage = new PageReference(openerUrl);
        
        nextPage.setRedirect(true);
        return nextPage;
	}
	
	

}