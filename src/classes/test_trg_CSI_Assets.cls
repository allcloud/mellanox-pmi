@isTest
public class test_trg_CSI_Assets {
	
	private CSI_Asset__c asset;
	
	static testMethod void myUnitTest(){
		test_trg_CSI_Assets testTrg = new test_trg_CSI_Assets();
		testTrg.dataPreperation();
		test.startTest();
    	testTrg.checkAssignmentAsset();
    	testTrg.checkReleaseAsset();
    	test.stopTest();
	}
	
	private void dataPreperation(){
		Case  c = new Case();
		c.Subject = 'Test';
		c.Type = 'Demo';
        insert c;
		
		asset = new CSI_Asset__c();
		asset.Hostname__c = 'Test';
		asset.CSI_Ticket_Number__c = c.id;
	}
	
	private void checkAssignmentAsset(){
		insert asset;
		CSI_assets_log__c assetsLog = [Select Case_assignment_date__c, Case__c, CSI_asset__c From CSI_assets_log__c Where CSI_asset__c =:asset.id Limit 1];
		Date assignmentDate = assetsLog.Case_assignment_date__c.date();
		System.assertEquals(assignmentDate,Date.today()); 
	}
	
	private void checkReleaseAsset(){
		asset.CSI_Ticket_Number__c = null;
		update asset;
		CSI_assets_log__c assetsLog = [Select Case_release_date__c, Case__c, CSI_asset__c From CSI_assets_log__c Where CSI_asset__c =:asset.id Limit 1];
		Date releaseDate = assetsLog.Case_release_date__c.date();
		System.assertEquals(releaseDate,Date.today()); 
	}
}