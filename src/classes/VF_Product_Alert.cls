global class VF_Product_Alert {
	
	public String ProductAlert_Str {get;set;}
	
	public VF_Product_Alert() {
		
		String currentProductSetailsId_Str = ApexPages.CurrentPage().getParameters().get('Id');   
		
	    ProductAlert_Str = [SELECT Id, Product_Alert__c FROM Product_Alert__c WHERE Product__c =: currentProductSetailsId_Str 
												  order by lastModifiedDate desc limit 1].Product_Alert__c;
												  
	}

}