public without sharing class VF_Accout_Related_Assets_Table {
	
	public Account theAcc {get;set;}
	private  map<String, map<String, decimal>> assets_map;
	public list<AssetBean> AssetBean_List {get;set;}
	private map<String, String> assetFamily2ProductNumbers_Map;
	private map<String, String> productNames2ProductFamily_Map;
	private list<Asset2__c> relatedAssets_List;
	
	// Inner class to hold Part numbers and list of statuses and quantities
	public class AssetBean {
	 	
	 	public String partNumber {get;set;}
	 	public String productFamily {get;set;}
	 	public list<AssetBeanSpecificationHolder> beanHolder_List{get;set;}
	 	public decimal Quantity {get;set;}	 	 
	 } 
	 
	 // Inner class to hold statuses and quantities
	 public class AssetBeanSpecificationHolder {
	 	
	 	public String contractType {get;set;}
	 	public decimal Quantity {get;set;}
	 }
	 
	
	public VF_Accout_Related_Assets_Table(ApexPages.StandardController controller) {
		
		String TableType_Str = Apexpages.currentPage().getParameters().get('TableType');
		system.debug('TableType_Str : ' + TableType_Str);
		Id theAccId = controller.getId();
		assetFamily2ProductNumbers_Map = new map<String, String>();
		set<String> ProductNames_Set = new set<String>();
	    productNames2ProductFamily_Map = new map<String, String>();
		
		list<Contract2__c> contractsAndRelatedAssets_List;
		
		if (TableType_Str != null && TableType_Str.EqualsIgnoreCase('Expired')) { 	  
			
			contractsAndRelatedAssets_List = [SELECT Id, (SELECT Id, Contract_status__c, Part_Number__c, Contract2_Type__c, Asset_Family__c
											  FROM Aessts21__r order by Part_Number__c asc) FROM Contract2__c WHERE Account__c =: theAccId and Status__c = 'Expired'];
		}
		
		else {
			
			contractsAndRelatedAssets_List = [SELECT Id, (SELECT Id, Contract_status__c, Part_Number__c, Contract2_Type__c, Asset_Family__c
											  FROM Aessts21__r order by Part_Number__c asc) FROM Contract2__c WHERE Account__c =: theAccId and Status__c != 'Expired'];
		}
		
														 
		relatedAssets_List = new list<Asset2__c>();
		
		for (Contract2__c currentContract : contractsAndRelatedAssets_List)	{
			
			for (Asset2__c asset : currentContract.Aessts21__r) {
				
				ProductNames_Set.add(asset.Part_Number__c);
				relatedAssets_List.add(asset);
			}
		}												 		  
		 	
	    
	    for (Product2 product : [SELECT Id, Family, Name FROM Product2 WHERE Name IN : ProductNames_Set]) {
	    	
	    	productNames2ProductFamily_Map.put(product.Name, product.Family);
	    }  
	    
				  
		assets_map = populateAssetsMap();
		AssetBean_List = new list<AssetBean>();
		list<String> toBSorted_List = new list<String>();
		for (String partNum : assets_map.keyset()) {
			
			AssetBean masterBean = new AssetBean();
			masterBean.partNumber = partNum;
			 
			masterBean.Quantity = 0;
			 
			if (assetFamily2ProductNumbers_Map.containsKey(partNum)) {
					
				masterBean.productFamily = assetFamily2ProductNumbers_Map.get(partNum);
				toBSorted_List.add(masterBean.productFamily);
			}
			masterBean.beanHolder_List = new list<AssetBeanSpecificationHolder>();
 			
			for (String  theContractType : assets_map.get(partNum).keySet()) {
								
				AssetBeanSpecificationHolder beanHolder = new AssetBeanSpecificationHolder();
				beanHolder.contractType = theContractType;
				beanHolder.Quantity = assets_map.get(partNum).get(theContractType);
				masterBean.Quantity += beanHolder.Quantity;
				masterBean.beanHolder_List.add(beanHolder);
			}
			
			AssetBean_List.add(masterBean);
		}	
		
		toBSorted_List.sort();
		
		set<String> sortedSet = new set<String>();
		sortedSet.addAll(toBSorted_List);
		list<AssetBean> sortedBean_List = new list<AssetBean>();
		
		for (String prodFam_Str : sortedSet) {   
			
			for (AssetBean masterBean : AssetBean_List) {
				
				if (prodFam_Str == masterBean.productFamily) {
					
					sortedBean_List.add(masterBean);
				}
			}
		}
		
		AssetBean_List.clear();
		AssetBean_List.addAll(sortedBean_List); 
	}
	
	
	
	 
	private map<String, map<String, decimal>> populateAssetsMap() {
		
		assets_map = new map<String, map<String, decimal>>();
				  
		for (Asset2__c currentAsset : relatedAssets_List) {
			
			if (  (currentAsset.Part_Number__c != null && !currentAsset.Part_Number__c.StartsWith('SA') 
			    && !currentAsset.Part_Number__c.StartsWith('SFG') 
			    && !currentAsset.Part_Number__c.StartsWith('NA')) || currentAsset.Part_Number__c == null ) {
				if (assets_map.containsKey(currentAsset.Part_Number__c)) {
					
					map <String, decimal> days2Exp2Quantity_Map = assets_map.get(currentAsset.Part_Number__c);    
					
					if (days2Exp2Quantity_Map.containsKey(currentAsset.Contract2_Type__c)) {
						
						decimal theQuantity = days2Exp2Quantity_Map.get(currentAsset.Contract2_Type__c);
						theQuantity ++;
						days2Exp2Quantity_Map.put(currentAsset.Contract2_Type__c,theQuantity);
					}
					
					else {
						
						days2Exp2Quantity_Map.put(currentAsset.Contract2_Type__c , 1);
					}
				}
				
				else {
						 map<String, decimal> newdays2Exp2Quantity_Map = new  map<String, decimal>();
						 newdays2Exp2Quantity_Map.put(currentAsset.Contract2_Type__c, 1);
						 assets_map.put(currentAsset.Part_Number__c, newdays2Exp2Quantity_Map);
					 
				}
				
				assetFamily2ProductNumbers_Map.put(currentAsset.Part_Number__c, productNames2ProductFamily_Map.get(currentAsset.Part_Number__c));
			}
		}
			
		return  assets_map;	
	}
}