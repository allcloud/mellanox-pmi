public class SupportForgotPasswordController{
	public String username {get; set;}   
       
    public SupportForgotPasswordController() {
    	ApexPages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
    }
	
  	public PageReference forgotPassword() {
  		boolean success = Site.forgotPassword(username);
  		PageReference pr = Page.SupportForgotPasswordConfirm;
  		pr.setRedirect(true);
  		
  		if (success) {  			
  			return pr;
  		}
  		return null;
  	}
}