@isTest
private class Test_Opportunity {

    static testMethod void Opportunity_BeforeInsertUpdate () {
        
        List<US_Territory_Map__c> lst_usTeretoryMap = New List <US_Territory_Map__c>();
        
        US_Territory_Map__c tm = new US_Territory_Map__c( SD_Region__c = 'EMEA', Region__c = '0000'); 
        tm.Sales_Director__c = '005500000011cs7';
        //insert tm;
        lst_usTeretoryMap.add(tm);
  
        US_Territory_Map__c tm1 = new US_Territory_Map__c(SD_Region__c = 'EMEA', Region__c = '0000');  
        tm1.Sales_Director__c = '005500000011vex';
        //insert tm1;
        lst_usTeretoryMap.add(tm1);
        
        insert lst_usTeretoryMap;
        
        
        Account acc = new Account(name = 'test1',support_center__c = 'Global', is_oracle_parent__c = 'Yes', type = 'OEM');
        insert acc;
        
        List<Opportunity> lst_ops = New List<Opportunity>();
        
        
        Opportunity Op0 = new Opportunity(AccountId=acc.id, Name = 'test opp', RecordTypeID=ENV.map_opportunityRecordTypeTOID.get('End_User_Opportunity'), StageName = 'Forecast', type = 'Direct', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
        op0.ownerid = '005500000011cs7';          
        op0.OpportunityCode__c = '122212';  
        lst_ops.add(op0);
        
        
        Opportunity Op = new Opportunity(AccountId=acc.id, Name = 'test opp', RecordTypeID='01250000000DOn2' , StageName = 'Forecast', type = 'Direct', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
        op.ownerid = '005500000011cs7';          
        op.OpportunityCode__c = '123345';  
        //insert op;
        lst_ops.add(op);
        
        insert lst_ops; 

        //OpportunityLineItem OpLI = [select id, quantity from OpportunityLineItem limit 1];
        OpportunityLineItem OpLI =new OpportunityLineItem( OpportunityId = op.id,PricebookEntryId = '01u50000001pnrb',Quantity=15,UnitPrice=55);
        insert opLI;
        OpportunityLineItemSchedule  OpLIS = new OpportunityLineItemSchedule  (OpportunityLineItemId =opLI.id, Type= 'Both', Quantity = 15, revenue = 55, ScheduleDate = Date.Today()+20);
        insert opLIS;
          OpLIS.Quantity = 15; 
        update opLI;

        op.ownerid = '005500000011vex';
        update op;   
        US_Territory_Map__c Tmap = new US_Territory_Map__c();
        Tmap.Sales_Director__c = '005500000011vex';
                
        // TO DO: implement unit test
    }
}