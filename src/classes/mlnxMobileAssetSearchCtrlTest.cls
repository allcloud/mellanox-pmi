/******************************************************************************
 * Tests against the mlnxMobileAssetSearchCtrl & various VF Pages
 *
 * @Author: 	Abdul Sattar (Magnet 360)
 * @Date: 		09.09.2015
 * 
 * @Updates:
 *
 */
@isTest
private class mlnxMobileAssetSearchCtrlTest {
	
	private static Integer NUM_OF_RECORDS = 5;							// No. of test records
	private static Account testAct;										// Test Account
	private static Contract2__c testCont;								// Test Contract2__c
	private static List<Asset2__c> testAssets = new List<Asset2__c>();	// List of new test assets.

    // Tests against mlnxMobileAssetSearchCtrl.searchAsset()
    private static testMethod void searchAssetTest() {
    	// Create set up data for test
    	createSetupData();

    	// Create a page reference.
    	PageReference searchPage = Page.mlnxMobileAssetSearch;
	    Test.setCurrentPage(searchPage); 

	    mlnxMobileAssetSearchCtrl controller = new mlnxMobileAssetSearchCtrl();


		// Run tests  & perform asserts
		Test.startTest();
	    	
	    	// No record found scenario
	    	controller.barCode = 'N/A';
			controller.searchAsset();
			System.assertEquals(true, controller.isNoRecordFound, 'Failed - 1: No record found scenario.');
			System.assertEquals(NULL, controller.recordId, 'Failed - 2: No record found scenario.');
			
			// One match found scenario
	    	controller.barCode = '01';
			controller.searchAsset();
			//System.assertEquals(true, controller.isDisplayOne, 'Failed - 1: One match found scenario.');
			//System.assertNotEquals(NULL, controller.recordId, 'Failed - 2: One match found scenario.');

			// Multiple match found scenario
	    	controller.barCode = '00';
			controller.searchAsset();
			//System.assertEquals(true, controller.isDisplayAll, 'Failed - 1: Multiple match found scenario');
			//System.assertEquals(NULL, controller.recordId, 'Failed - 2: Multiple match found scenario');
			//System.assert(controller.assetsToDisplay.size() > 0, 'Failed - 3: Multiple match found scenario');

		Test.stopTest();

	}

	/** HELPER METHODS */

	// Create test data
	private static void createSetupData() {
		// Create test Account
    	testAct = MyMellanoxTestUtility.getAccount();

    	// Create a test contract
    	testCont = MyMellanoxTestUtility.getContracts2(1, true, testAct)[0];

		// Create test Assets
		Asset2__c a2 = new Asset2__c(Name ='Test Asset'
									,Asset_Type__c = 'Support'
									,part_number__c = 'weryu5SP'
									,Serial_Number__c = '00'
									,Contract2__c = testCont.Id );
		testAssets.add(a2);

		testAssets.addAll(MyMellanoxTestUtility.getAsset2(NUM_OF_RECORDS, false, testAct, testCont));

		INSERT testAssets;
		//System.assert(testAssets[0].Id != NULL, 'Unable to create test assets.');
	}
}