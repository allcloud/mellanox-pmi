public with sharing class VF_Reseller_Dell_Partner_Banner {
	
	public boolean isDellSerialCase {get;set;}
		
	
	public VF_Reseller_Dell_Partner_Banner(ApexPages.StandardController controller) {
		
		isDellSerialCase = false;
		Id TheCaseId = controller.getId();
		
		Case theCAse = [SELECT Id, Reseller_Dell_Partner__c FROM Case WHERE Id =: TheCaseId];
		if (theCAse.Reseller_Dell_Partner__c != null ) {
			
			isDellSerialCase = true;
		}
	}

}