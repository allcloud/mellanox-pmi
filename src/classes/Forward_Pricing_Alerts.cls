global class Forward_Pricing_Alerts implements Schedulable{
	global void execute(SchedulableContext SC) {
		//Logics to run on the 15th day of Mar, June, Sep, Dec
		//15 days before End Of Q
		List<MLNX_Price_Book__c> lst_forward_pricing = new List<MLNX_Price_Book__c>([select id, X1Quarter_out_Price__c,Pricebook_Name__c,Product_Name__c
																						from MLNX_Price_Book__c where X1Quarter_out_Price__c != null]);
		if(lst_forward_pricing == null || lst_forward_pricing.size() == 0)
			return;
			
		Set<String> set_PBnames = new Set<String>();
		Set<String> set_prodnames = new Set<String>();
		
		Map<String,Map<String,MLNX_Price_Book__c>> map_PB_Prod_CurrQ_Price = new Map<String,Map<String,MLNX_Price_Book__c>>();
		Map<String,MLNX_Price_Book__c> tmp_map_prod_currQ = new Map<String,MLNX_Price_Book__c>();
		
		for (MLNX_Price_Book__c m : lst_forward_pricing) {
			set_PBnames.add(m.Pricebook_Name__c);
			set_prodnames.add(m.Product_Name__c);
			
			if(map_PB_Prod_CurrQ_Price.get(m.Pricebook_Name__c) == null ) {
				tmp_map_prod_currQ = new Map<String,MLNX_Price_Book__c>();
				tmp_map_prod_currQ.put(m.Product_Name__c,m);
				map_PB_Prod_CurrQ_Price.put(m.Pricebook_Name__c,tmp_map_prod_currQ);
			} 	
			else {
				tmp_map_prod_currQ = map_PB_Prod_CurrQ_Price.get(m.Pricebook_Name__c);
				tmp_map_prod_currQ.put(m.Product_Name__c,m);
				map_PB_Prod_CurrQ_Price.put(m.Pricebook_Name__c,tmp_map_prod_currQ);
			}
		}//end building map
		//Delete old Forward_Pricing reference on Quote Lines first
		List<QuoteLineItem> qline_delete_old_forwardPB = new List<QuoteLineItem>([select id from QuoteLineItem where Forward_Pricing__c != null]);
		if(qline_delete_old_forwardPB.size() > 0) {
			for (QuoteLineItem q : qline_delete_old_forwardPB) {
				q.Forward_Pricing__c = null;
			}
			Database.update(qline_delete_old_forwardPB,false);
		}
		//End delete
		
		List<QuoteLineItem> lst_quotes = new List<QuoteLineItem>([select id,New_Price__c,UnitPrice,Product__c,Quote.New_Price_Book__c,Forward_Pricing__c
																	from QuoteLineItem where Product__c in :set_prodnames 
																	and Quote.New_Price_Book__c in :set_PBnames
																	and (Quote.Stage__c = 'Pipeline' or Quote.Stage__c = 'Best Case' or Quote.Stage__c = 'Commit')
																	and (Quote.Status = 'Draft' or (Quote.Status='Approved' and Quote.ExpirationDate >= :System.today())) ]);
		List<QuoteLineItem> quotelines_updates = new List<QuoteLineItem>();
		for(QuoteLineItem qline : lst_quotes) {
			//New_Price__c is the quote list price
			if (qline.Quote.New_Price_Book__c != null 
					&& map_PB_Prod_CurrQ_Price.get(qline.Quote.New_Price_Book__c) != null 
					&& map_PB_Prod_CurrQ_Price.get(qline.Quote.New_Price_Book__c).get(qline.Product__c) != null 
					&& map_PB_Prod_CurrQ_Price.get(qline.Quote.New_Price_Book__c).get(qline.Product__c).X1Quarter_out_Price__c != null
					&& map_PB_Prod_CurrQ_Price.get(qline.Quote.New_Price_Book__c).get(qline.Product__c).X1Quarter_out_Price__c != qline.New_Price__c) {
				
				qline.Forward_Pricing__c = map_PB_Prod_CurrQ_Price.get(qline.Quote.New_Price_Book__c).get(qline.Product__c).ID; 
				quotelines_updates.add(qline);								
			}
		}
		if(quotelines_updates.size() > 0) {
			Database.update(quotelines_updates,false);
		}																			 		 																							
	}
}