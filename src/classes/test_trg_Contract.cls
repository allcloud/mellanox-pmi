@isTest(seeAllData = true)
public with sharing class test_trg_Contract {

	static testMethod void test_deleteRealtedOppsAndQuotesWhenPendingPOUnChecked () {
		
		CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        insert acc;
        
        Contract2__c c3 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
        
        insert c3;
        
   		Opportunity Opp = new Opportunity(AccountId=acc.id, Name = 'test opp', RecordTypeID=ENV.map_opportunityRecordTypeTOID.get('End_User_Opportunity'), StageName = 'Forecast', type = 'Direct', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
   		Opp.Old_Contract__c = c3.Id;
   		insert opp;
   		
   		c3.Reset_Contract_ERI__c = true;
   		
   		test.StartTest();
   		
   		update c3;
   		
   		test.StopTest();       
	}
	
	static testMethod void test_createContactRelatedToContract() {
		
		//Contact cont1 = [select Id, Email from Contact limit 1];
		Contact con1 = new Contact(LastName = 'testLast', email = 'testEmail321123321@testMail.com');
		insert con1;
		
 	    Account Acc1 = new Account(name = 'test1',support_center__c = 'Global');
        insert Acc1;

        Account Acc2 = new Account(name = 'test2222', support_center__c = 'Global',parentid = Acc1.id);
        insert Acc2;

        Contract2__c contr1 = new Contract2__c(Account__c = Acc2.id, contract_type__c = 'Gold support',status__c = 'activated', Contract_Term_months__c = 5);
        contr1.End_Contact_Email__c = con1.Email;
        insert contr1;
        
        Contract2__c contr2 = new Contract2__c(Account__c = Acc2.id, contract_type__c = 'Gold support',status__c = 'activated', Contract_Term_months__c = 5);
        contr2.End_Contact_Email__c = 'testMail69696868@testMail2.com';
        contr2.End_Contact_Name__c = 'Joseph Cohen';
        insert contr2;
	}
	
	static testMethod void test_updateERIContractWhenApproved() {
		
		Account Acc = new Account(name = 'test1',support_center__c = 'Global');
        insert Acc;
        
		Contract2__c cont = new Contract2__c(Account__c = Acc.id, contract_type__c = 'Gold support',status__c = 'activated', Contract_Term_months__c = 5);
        cont.End_Contact_Email__c = 'testMail69696868@testMail2.com';
        cont.End_Contact_Name__c = 'Joseph Cohen';
        cont.EndDate__c = date.today().addYears(1);
        cont.contract_start_date__c =  date.today();
        insert cont;
		
		SFDC_Projects__c gpsProj = new SFDC_Projects__c();
		gpsProj.Contract__c = cont.Id;
		gpsProj.Project_Description__c = 'Test desc';
		insert gpsProj;
		
		Opportunity Op0 = new Opportunity(AccountId=acc.id, Name = 'test opp', RecordTypeID=ENV.map_opportunityRecordTypeTOID.get('End_User_Opportunity'), StageName = 'Forecast', type = 'Direct', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
        op0.ownerid = '005500000011cs7';          
        op0.OpportunityCode__c = '122212'; 
        op0.Old_Contract__c = cont.Id; 
        op0.ERI_Opportunity__c = true;
        insert op0;
		
		Contact con2 = new Contact(LastName = 'testLast', email = 'testEmail321123321@testMail.com');
		insert con2;
		OpportunityContactRole ocr = new OpportunityContactRole(Role='Business User',OpportunityId=op0.id,ContactId=con2.id,IsPrimary=True);
		insert ocr;
		
		cont.Payment_Status__c = null;
		update cont;
		
		cont.Payment_Status__c = 'Approved';
		cont.Transaction_Id__c = 'PPL123Test';
		
		update cont;
	}
}