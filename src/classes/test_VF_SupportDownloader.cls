@isTest(seeAllData=true)
public with sharing class test_VF_SupportDownloader {
	
	static testMethod void test_SupportDownloader() {
		
		 Product_Category__c productCategory = [SELECT Id, Product_Family__r.Id FROM Product_Category__c WHERE Product_Family__r.Id != null limit 1];
		 ProductDetails__c productDetail = [SELECT Id, Product_Category__r.Id FROM ProductDetails__c WHERE Product_Category__r.Id != null limit 1];
		 ContentVersion  contentVersion = [SELECT Id FROM ContentVersion limit 1];
		 
		 VF_SupportDownloader vf = new VF_SupportDownloader();
		 
		 vf.getTheproductFamily();
		 vf.chosenFamilyId = productCategory.Product_Family__r.Id;
		 vf.getTheproductCategory();
		 vf.chosenCatId = productDetail.Product_Category__r.Id;
		 vf.getTheproductDetail();
		 vf.getTheRevision();
		 vf.getTheContentFile();
		 vf.getTheContentFileManual();
		 vf.getTheRetrieveDetailsResults();
		 vf.fillUpProductCategory();
		 vf.fillUpProductDetail();
		 vf.fillUpRevision();
		 vf.fillUpContentFile();
		 vf.chosenContentFileId = contentVersion.Id;
		 vf.downloadTheDoc();
		 vf.fillUpGeneralFilesRelatedToDetails();
		 vf.getSearchProductsResults();
		 
		 
	}
}