global class PCN_Weekly_ECR_Released_or_Declined implements Schedulable {
	global void execute(SchedulableContext SC) {
		Set<String> set_ECRs = new Set<String>();
		Set<String> set_ECRs_need_display_reports = new Set<String>();
		
		List<PCN__c> lst_PCNs = new List<PCN__c>([select id, ECR_X_Ref__c,Status__c
													from PCN__c where (Type__c='Approval Required' or Type__c='FYI')
													and Status__c='Pending' ]);
		for (PCN__c p : lst_PCNs)
			set_ECRs.add(p.ECR_X_Ref__c);
			
		List<PCN__c> lst_PCNs_updates = new List<PCN__c>([select id, ECR_X_Ref__c,Status__c,Check_ECR_Released_or_Declined__c,ECR_Released_or_Declined__c
													from PCN__c where (Type__c='Approval Required' or Type__c='FYI')
													and (Status__c = 'Release' or Status__c='Customer Declined')
													and (Release_Date__c >= :System.today().adddays(-30) or Customer_Declined_Date__c >= :System.today().adddays(-30))
													and ECR_X_Ref__c not in :set_ECRs ]);	
		for (PCN__c p : lst_PCNs_updates) {
			set_ECRs_need_display_reports.add(p.ECR_X_Ref__c);
		}							
		List<PCN__c> lst_PCNs_updates_2 = new List<PCN__c>([select id,ECR_Released_or_Declined__c,Check_ECR_Released_or_Declined__c
																from PCN__c where ECR_X_Ref__c in :set_ECRs_need_display_reports]);
																 							
		for (PCN__c p : lst_PCNs_updates_2) {
			p.ECR_Released_or_Declined__c = true;
			p.Check_ECR_Released_or_Declined__c = System.today();
		}
		if(lst_PCNs_updates_2 != null && lst_PCNs_updates_2.size() > 0)
			update lst_PCNs_updates_2;																				
	}
}