public class Loaner_ShippingPDF {
	public Loaner_Request__c loaner {get;set;}
	public List<Loaner_Product__c> loaner_prods {get; set;}
	
	public Loaner_ShippingPDF(ApexPages.StandardController controller) {
		loaner = [select Ship_Company__c,Ship_Country__c,ShipState__c,Ship_City__c,Ship_to_contact_First_Name__c,Ship_to_contact__c,
							Ship_Street_Address__c,Ship_Zip__c,Ship_Email__c,Ship_Phone__c,
							End_Cust_Company_Name__c,Country__c,EndCustState__c,
							Est_Freight_Cost_By_Mellanox__c, name
					from Loaner_Request__c where id = :controller.getRecord().Id];
					
		loaner_prods = new List<Loaner_Product__c>([select Product__r.Name,Product__r.Description,OPN_quantity__c,Freight_Cost__c
													from Loaner_Product__c where Loaner_Request__c = :loaner.ID]);
																				
	}
	
}