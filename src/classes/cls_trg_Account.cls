public with sharing class cls_trg_Account {
	
	public void updateRWDomainMultiPicklist(list<Account> newAccounts_List, map<Id, Account> oldAccounts_Map) {
		
		for (Account newAcc : newAccounts_List) {
							
			if (oldAccounts_Map != null) {
				
				if (newAcc.RW_Domain__c != oldAccounts_Map.get(newAcc.Id).RW_Domain__c) {
					
					if (newAcc.RW_Domain__c != null) {	
						newAcc.RW_Domain_multi__c = newAcc.RW_Domain__c.ReplaceAll(',' , ';');
					}
					
					else {
						
						newAcc.RW_Domain_multi__c = null;
					}
				}
			}
			
			else {
				
				if (newAcc.RW_Domain__c != null) {
					newAcc.RW_Domain_multi__c = newAcc.RW_Domain__c.ReplaceAll(',' , ';');
				}
				
				else {
					
					newAcc.RW_Domain_multi__c = null;
				}
			}
		}
	}

}