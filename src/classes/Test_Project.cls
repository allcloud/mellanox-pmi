@isTest(SeeAllData=true)

private class Test_Project {

    static testMethod void Test() 
    {
    CLS_ObjectCreator cls = new CLS_ObjectCreator();
    
    User users1 = [SELECT Id, contactid, contact.accountid FROM User WHERE userType!= 'Standard' and FirstName = 'Customer' and LastName = '2'and IsActive = true LIMIT 1];
    
   /* Account acc = cls.createAccount();
    acc.name = 'ProjAccount';
    insert(acc);*/
    
    
    Milestone_Map__c mmp = cls.CreateMolestone_Map();
    insert mmp;
    
    Milestone1_Project__c Proj = cls.CreateMPproject(Users1.contact.accountId );
    list<recordtype> RecTypes = [select id from recordtype where name = :mmp.project_type__c and SobjectType = 'Milestone1_Project__c'];
    Proj.recordtypeId = RecTypes[0].id;
    Proj.Salesforce_ID__c = '123123';
    insert(Proj); 
    
    Milestone1_Milestone__c ms = cls.CreateMilestone(Proj.id);
    insert (ms);
    
    task ts = new task();
    ts.whatId = ms.id;
    ts.projectId__c = proj.id;
    insert(ts);
    
    List<User> users = [SELECT Id FROM User WHERE LastName = 'Goldin' and IsActive = true LIMIT 1];
    
      Id User1Id = users[0].Id;
      
     
      
      
      Id User2Id = users1.Id;
      
      
          
      
      Project_Member__c pmem = cls.createProjectMember(proj.id, User1Id);    
      insert pmem; 
      delete pmem; 
      
       Project_Member__c pmem1 = cls.createProjectMember(proj.id, User2Id);    
      insert pmem1; 
      
      Proj.Total_Duration_Hours__c = 50;
      Proj.Business_Justification__c = 'test';
      update Proj;
      delete pmem1; 
    
    }
    
    static testMethod void Test_calculatAllRealtedMilestonesCumulativeDone() {
    	
    	CLS_ObjectCreator cls = new CLS_ObjectCreator();
    
 	    User users1 = [SELECT Id, contactid, contact.accountid FROM User WHERE userType!= 'Standard' and FirstName = 'Customer' and LastName = '2'and IsActive = true LIMIT 1];
    
    	
    	Milestone_Map__c mmp = cls.CreateMolestone_Map();
	    insert mmp;
	    
	    Milestone1_Project__c Proj = cls.CreateMPproject(Users1.contact.accountId );
	    list<recordtype> RecTypes = [select id from recordtype where name = :mmp.project_type__c and SobjectType = 'Milestone1_Project__c'];
	    Proj.recordtypeId = RecTypes[0].id;
	    Proj.Salesforce_ID__c = '123123';
	    insert(Proj); 
	    
	    Milestone1_Milestone__c ms = cls.CreateMilestone(Proj.id);
	    ms.Deadline__c = date.today().addDays(7);
	    ms.Done__c = 20;
	    insert (ms);
	    
	    Milestone1_Milestone__c ms1 = cls.CreateMilestone(Proj.id);
	    ms1.Done__c = 70;
	    insert (ms1);
	    
	    ms1.Done__c = 60;
	    update ms1;
	    
	    delete ms1;
    }
 }