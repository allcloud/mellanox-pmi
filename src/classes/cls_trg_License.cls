global class cls_trg_License {
    
    private Id MyMellanoxLicenseUser = MyMellanoxSettings.MyMellanoxLicenseUser;
    private Id MyMellanoxOEMSystemSupport = MyMellanoxSettings.SystemOEMId;
    private Id MyMellanoxSystemSupport = MyMellanoxSettings.SystemSupportUserProfileId;
    private list<String> newUsersAsStr2Create_List = new list<String>();
    private set<String> contactsEmails_Set = new set<String>();
    
    public void assignUsersPerLicense(list<License__c> newLicense_List) {   
        
        map<String, Contact> emails2Contacts_Map = fillUpLicenseseEmail2ContactsMap(newLicense_List);
        map<Id, User> contactIds2Users_Map = fillUpContactIds2UsersMap(emails2Contacts_Map);
        list<SObject> ContactsAndUsers2Update = getContactsAndUsersList2Update(newLicense_List, emails2Contacts_Map, contactIds2Users_Map);
        list<License__c> licenses2Update_List = fillUpLicensesWithRelatedContacts(newLicense_List, emails2Contacts_Map);
        
            
        if (contactIds2Users_Map != null && !contactIds2Users_Map.values().isEmpty()) {
            
            set<SObject> contacts2Update_Set = new set<SObject>();
            contacts2Update_Set.addAll(ContactsAndUsers2Update);
            ContactsAndUsers2Update.clear();
            ContactsAndUsers2Update.addAll(contacts2Update_Set);
            
            if (!ContactsAndUsers2Update.isEmpty()) {
                
                update ContactsAndUsers2Update;
            }
        }
            
        system.debug('newUsersAsStr2Create_List 29 : ' + newUsersAsStr2Create_List);
        if (!newUsersAsStr2Create_List.isEmpty()) {
            
            for (String theUserAs_Str : newUsersAsStr2Create_List) {
                
                cls_Create_User.insertUserAsync(theUserAs_Str, contactsEmails_Set, true, true);
            }
        }
        
        
        if (!licenses2Update_List.isEmpty()) {
        	
        	update licenses2Update_List;
        }
    }
    
    private list<License__c> fillUpLicensesWithRelatedContacts(list<License__c> newLicense_List, map<String, Contact> emails2Contacts_Map) {
    	
    	list<License__c> licenses2Update_List = new list<License__c>();
    	
    	if (emails2Contacts_Map != null && !emails2Contacts_Map.values().isEmpty()) {
    		
    		for(License__c newLicense : newLicense_List) {
    			
    			if (newLicense.Customer_Email__c != null) {
    				
    				if (emails2Contacts_Map.containsKey(newLicense.Customer_Email__c)) {
    					
    					License__c license2Update = new License__c(Id = newLicense.Id);
    					license2Update.Contact__c = emails2Contacts_Map.get(newLicense.Customer_Email__c).Id;
    					licenses2Update_List.add(license2Update);
    				}
    			}
    		}
    	}
    	
    	return licenses2Update_List;
    }
    
    
    private list<SObject> getContactsAndUsersList2Update(list<License__c> newLicense_List, map<String, Contact> emails2Contacts_Map, map<Id, User> contactIds2Users_Map) {
        
        list<SObject> contactsAndUsers2Update_List = new list<SObject>();
        
        for (License__c currentLicense : newLicense_List) {
                                
            contactsEmails_Set.add(currentLicense.Customer_Email__c);
                                
            if ( currentLicense.Customer_Email__c != null ) {
                
                if (emails2Contacts_Map.containsKey(currentLicense.Customer_Email__c)) {
                   
                    Contact relatedContact = emails2Contacts_Map.get(currentLicense.Customer_Email__c);
                    
                    if (contactIds2Users_Map.containsKey(relatedContact.Id)) {
                        
                        User relatedUser = contactIds2Users_Map.get(relatedContact.Id);
                        
                        if (!relatedUser.isActive) {
                            
                            relatedUser.isActive = true;
                            contactsAndUsers2Update_List.add(relatedUser);
                        }
                        
                        relatedContact.License_Send_New_Notification__c = true;  
                        contactsAndUsers2Update_List.add(relatedContact);
                        
                        cls_Create_User.assignUsersAsLicenseOwner(contactsEmails_Set);
                    }
                    
                    else {
                        
                        User newUser = getUser(relatedContact);
                        
                        String userAs_Str = JSON.Serialize(newUser);
                        newUsersAsStr2Create_List.add(userAs_Str);
                        system.debug('newUsersAsStr2Create_List 104 : ' + newUsersAsStr2Create_List);
                    }
                }
                
                else {
                    
                    cls_Create_User handler = new cls_Create_User();
                    
                    String lastName = (currentLicense.Customer_Name__c != null)? currentLicense.Customer_Name__c.replaceAll('[\\W]|_', ' ') : 'Unknown Name';
                    
                    String Email = currentLicense.Customer_Email__c;
                    
                    Contact newContact;
                    
                    Id currentAccountId;
                    
                     try {
                     	
                     	currentAccountId = [SELECT Id FROM Account WHERE Id =: currentLicense.AccountId__c].Id;
                     }	
                     
                     catch( Exception e) {
                     	
                     	currentAccountId = [SELECT Id FROM Account WHERE NAME =: 'GENERAL LICENSE ACCOUNT'].Id;
                     }
                    
                    newContact = handler.createContact(null, lastName, Email, null, null, currentAccountId);
                    
                    Contact  newContactCreated = [SELECT Id, Email, LastName, FirstName, Portal_Approval__c, Account.Name, AccountId, Account.Type FROM Contact WHERE Id =: newContact.Id];
                    
                    User newUser = getUser(newContactCreated);
                    
                    String userAs_Str = JSON.Serialize(newUser);
                    newUsersAsStr2Create_List.add(userAs_Str);
                    system.debug('newUsersAsStr2Create_List 137 : ' + newUsersAsStr2Create_List);
                }
            }        
        }
        
        return contactsAndUsers2Update_List;
    }
    
    
    // This method fills up a map if Contact Ids and their related Users
    private map<Id, User> fillUpContactIds2UsersMap(map<String, Contact> emails2Contacts_Map) {
        
        map<Id, User> contactIds2Users_Map = new map<Id, User>();
        
        set<Id> contactIds_Set = new set<Id>();
        
        if (emails2Contacts_Map != null && !emails2Contacts_Map.values().isEmpty()) {
            
            for (Contact con : emails2Contacts_Map.values()) {
                
                contactIds_Set.add(con.Id);
            }
            
            list<User> relatedUsers_List = [SELECT Id, Email, isActive, contactId, ProfileId FROM User WHERE contactId IN : contactIds_Set];
            
            if (!relatedUsers_List.isEmpty()) {
                
                for (User currentUser : relatedUsers_List) {
                    
                    contactIds2Users_Map.put(currentUser.contactId, currentUser);
                }
            }
        }
        
        return contactIds2Users_Map;
    }
    
    // This method update the Lead Number of Related Licenses with the number of Licenses exist with the same email as the Lead Email
    public void updRelatedLeadsWithNumberOfRelatedeLicensesByEmail(list<License__c> newLicense_List) {
        
        map<String, list<License__c>> licensesEmail2LicensesList_Map = getLicensesByEmail(newLicense_List);
        
        map<String, Lead> emails2Leads_Map = getLeadsByEmails(newLicense_List);
        
        for (License__c newLicense : newLicense_List) {
            
            if (licensesEmail2LicensesList_Map.containsKey(newLicense.Customer_Email__c)) {
                
                if (emails2Leads_Map.containsKey(newLicense.Customer_Email__c)) {
                    
                    Lead lead2Update = emails2Leads_Map.get(newLicense.Customer_Email__c);
                    
                    lead2Update.Number_Of_Evaluation_Licenses__c = licensesEmail2LicensesList_Map.get(newLicense.Customer_Email__c).size();
                }
            }
        }
        
        if (!emails2Leads_Map.values().isEmpty()) {
            
            update emails2Leads_Map.values();
        }
    }
    
    // This method returns a map of Leads with their emails
    private map<String, Lead> getLeadsByEmails(list<License__c> newLicense_List) {
        
        map<String, Lead> emails2Leads_Map = new map<String, Lead>();
        
        set<String> newLicensesEmails_Set = new set<String>();
        
        for (License__c newLicense : newLicense_List) {
            
            if (newLicense.Customer_Email__c != null && newLicense.RecordTypeId == ENV.recordTypeLicenseEvaluation) {
                
                newLicensesEmails_Set.add(newLicense.Customer_Email__c);
            }
        }
        
        if (!newLicensesEmails_Set.isEmpty()) {
            
            list<Lead> relatedLeads_List = [SELECT Id, Email, Number_Of_Evaluation_Licenses__c FROM Lead WHERE Email IN : newLicensesEmails_Set];
            
            for (Lead relatedLead : relatedLeads_List) {
                
                emails2Leads_Map.put(relatedLead.Email, relatedLead);
            }
        }
        
        return emails2Leads_Map;
    }
    
    // This method returns a map of lists of Licenses as values and their related emails as keys
    private map<String, list<License__c>> getLicensesByEmail(list<License__c> newLicense_List) {
        
        map<String, list<License__c>> licensesEmail2LicensesList_Map = new map<String, list<License__c>>();
        
        set<String> newLicensesEmails_Set = new set<String>();
        
        for (License__c newLicense : newLicense_List) {
            
            if (newLicense.Customer_Email__c != null) {
                
                newLicensesEmails_Set.add(newLicense.Customer_Email__c);
            }
        }
        
        if (!newLicensesEmails_Set.isEmpty()) {
        
            list<License__c> relatedLicenses_List = [SELECT Id, Customer_Email__c FROM License__c WHERE Customer_Email__c IN : newLicensesEmails_Set
                                                     and RecordTypeId =: ENV.recordTypeLicenseEvaluation];
            
            for (License__c relatedLicense : relatedLicenses_List) {
                
                if(licensesEmail2LicensesList_Map.containsKey(relatedLicense.Customer_Email__c)) {
                    
                    licensesEmail2LicensesList_Map.get(relatedLicense.Customer_Email__c).add(relatedLicense);
                }
                
                else {
                    
                    licensesEmail2LicensesList_Map.put(relatedLicense.Customer_Email__c, new list<License__c>{relatedLicense});
                }
            }
        }
        
        return licensesEmail2LicensesList_Map;
    }
    
    
    
    
    // This method fills up a map if Contact Emails and their related Contacts
    private map<String, Contact> fillUpLicenseseEmail2ContactsMap(list<License__c> newLicense_List) {
        
        map<String, Contact> emails2Contacts_Map = new map<String, Contact>();
        
        
        set<String> LicenseEmails_Set = new set<String>();
        
        for (License__c newLicense : newLicense_List) {
            
            if (newLicense.RecordTypeId != ENV.recordTypeLicenseEvaluation) {
            
                LicenseEmails_Set.add(newLicense.Customer_Email__c);
            }
        }
        
        list<Contact> relatedContacts_List = [SELECT Id, Email, LastName, FirstName, Portal_Approval__c, Account.Name, AccountId, Account.Type FROM Contact WHERE Email IN : LicenseEmails_Set];
        
        if (!relatedContacts_List.isEmpty()) {
            
            for (Contact con : relatedContacts_List) {
                
                emails2Contacts_Map.put(con.Email, con);
            }
        }
        
        return emails2Contacts_Map;
    }
    
    //This method returns a new user to be created 
    private User getUser(Contact relatedContact) {
        
        User newUser = new User();
        newUser.UserName = relatedContact.Email;
        newUser.FirstName = relatedContact.FirstName;
        newUser.Email = relatedContact.Email;
        
        if (relatedContact.LastName.length() > 2) {
            
            newUser.LastName = relatedContact.LastName;
        }
        
        else {
            
            newUser.LastName = 'Mr.' + relatedContact.LastName;
        }
        
        newUser.ContactId = relatedContact.Id;
        
        if (newUser.LastName.length() > 6) {
            
            newUser.alias = newUser.LastName.substring(0, 6);
        }
        
        else {  
            
            newUser.alias = newUser.LastName.substring(0,newUser.LastName.length() -1);
        }
        newUser.CommunityNickname = newUser.LastName + '1';
        newUser.TimeZoneSidKey = 'Europe/Dublin';
        
        if (relatedContact.Portal_Approval__c != null) {
            
            if (relatedContact.Portal_Approval__c.EqualsIgnoreCase('No')) {
                    
                newUser.profileId = MyMellanoxLicenseUser;
            }
                
            if (relatedContact.Portal_Approval__c.EqualsIgnoreCase('Yes')) {
                 
                if (relatedContact.Account.Type.EqualsIgnoreCase('OEM')) {
                    
                    newUser.profileId = MyMellanoxOEMSystemSupport;
                }
                
                else {
                    
                    newUser.profileId = MyMellanoxSystemSupport;
                }
            }
        }
        
        else {
            
            newUser.profileId = MyMellanoxOEMSystemSupport;
        }
        
        return newUser;
    }
    
    // This method will split the parent License when created to child Licenses, when the Parent License is marked with Auto Split (Auto_Split__c = true)
    public void splitLicenses(list<License__c> newLicenses_List) {
        
        list<License__c> splittedLicensesToUpsert_List = new list<License__c>();
        
        for (License__c newLicense : newLicenses_List) {
            
            if (newLicense.Parent_License__c && newLicense.Auto_Split__c) {
                
                if (newLicense.Quantity__c != null && newLicense.Quantity__c > 1) {
                    
                    License__c newLicense2Update = new License__c(Id = newLicense.Id);
                    newLicense2Update.Quantity__c = 1;
                    splittedLicensesToUpsert_List.add(newLicense2Update);
                    for (Integer i = 0 ; i < newLicense.Quantity__c -1 ; i ++) {
                        
                        integer splittedNameCounter = i + 1;
                        License__c splittedLicense = new License__c();
                        splittedLicense = newLicense.clone(true,true);
                        splittedLicense.Id = null;
                        splittedLicense.Quantity__c = 1;
                        splittedLicense.Name = newLicense.Name + '_' + String.valueOf(splittedNameCounter);
                        splittedLicense.Master_Serial__c = newLicense.Name;
                        splittedLicense.Split_Counter__c = 0;
                        splittedLicense.Parent_License__c = false;
                        splittedLicensesToUpsert_List.add(splittedLicense);
                    }
                }
            }
        }
        
        if (!splittedLicensesToUpsert_List.isEmpty()) {
            
            upsert splittedLicensesToUpsert_List;
        }
    }
    
    public void createFeedItemsPerNewLicense(list<License__c> newLicenses_List) {  
        
        list<License_Product__c> relatedLicenseProducts_List = getRelatedLicenseProducts(newLicenses_List);
        list<ContentVersion> relevantContentCersions_list = new list<ContentVersion>();
        list<FeedItem> feedItems2Create_List = new list<FeedItem>();
        list<License__c> newLicenses2Update_list = new list<License__c>();
        
        if (relatedLicenseProducts_List != null && !relatedLicenseProducts_List.isEmpty()) {
            
            Id LicenseContentRecType = [SELECT Id FROM RecordType WHERE Name =: 'License Content' and SobjectType =: 'ContentVersion'].Id;
            
            set<String> revisions_Set = new set<String>();
            
            for (License_Product__c lProduct : relatedLicenseProducts_List) {
                
                revisions_Set.add(lProduct.Revision__c);
            }
            
            
            list<ContentVersion> relatedContentVersion_List = [SELECT Id, Doc_SW_Revision__c, Product__c FROM ContentVersion WHERE RecordTypeId =: LicenseContentRecType
                                                               and Doc_SW_Revision__c IN : revisions_Set and Product__c != null];
                                                               
            if (!relatedContentVersion_List.isEmpty()) {
                
                for (License_Product__c lProduct : relatedLicenseProducts_List) {
                    
                    for (ContentVersion version : relatedContentVersion_List) {
                        
                        if (lProduct.Revision__c.equalsIgnoreCase(version.Doc_SW_Revision__c) && version.Product__c.Contains(lProduct.Name) ) {
                            
                            relevantContentCersions_list.add(version);
                        }
                    }
                }
                
                if (!relevantContentCersions_list.isEmpty()) {
                    
                    for (License__c newLicense : newLicenses_List) {
                        
                        for (ContentVersion version : relevantContentCersions_list) {
                            
                             FeedItem newFeeditem = new FeedItem();
                             newFeeditem.parentId = newLicense.Id;
                             newFeeditem.RelatedRecordId = version.Id;
                             newFeeditem.Visibility = 'AllUsers';
                             newFeeditem.Type = 'ContentPost';
                             newFeeditem.Body = 'Post with related document body';
                             feedItems2Create_List.add(newFeeditem);
                        }
                    }
                    
                    
                    for (License__c newLicense : newLicenses_List) {
                        
                        for (License_Product__c lProduct : relatedLicenseProducts_List) {
                            
                            if (newLicense.Product_Type__c.EqualsIgnoreCase(lProduct.Name)) {
                                
                                License__c license2Update = new License__c(Id = newLicense.Id);
                                license2Update.Revision__c = lProduct.Revision__c;
                                newLicenses2Update_list.add(license2Update);
                            }
                        }
                    }
                    
                    if (!newLicenses2Update_list.isEmpty()) {
                        
                        try {
                            
                            update newLicenses2Update_list;
                        }
                        
                        catch(Exception e) {
                            
                            system.debug('ERROR In Updating the License : ' + e.getMessage());
                        }
                    }
                    
                    if (!feedItems2Create_List.isEmpty()) {
                        
                        try {
                            
                            insert feedItems2Create_List;
                        }
                        
                        catch(Exception se) {
                            
                            system.debug('ERROR In creating the feed Items : ' + se.getMessage());
                        }
                    }
                }
            }                                                  
        }
    }
    
    
    private list<License_Product__c> getRelatedLicenseProducts(list<License__c> newLicenses_List) {
        
        //map<Id, License_Product__c> licenseProductsNames2LicneseProducts_Map = new map<Id, License_Product__c>();
        list<License_Product__c> relatedLicenseProducts_List;
        
        set<String> LicenseProductTypes_Set = new set<String>();
         
        for (License__c newLicense : newLicenses_List) {  
            
            if (newLicense.Parent_License__c) {  
                
                LicenseProductTypes_Set.add(newLicense.Product_Type__c);
            }
        }
        
        if (!LicenseProductTypes_Set.isEmpty()) {
            
            relatedLicenseProducts_List = [SELECT Id, Name, Revision__c FROM License_Product__c WHERE Name IN : LicenseProductTypes_Set and Revision__c != null];
        }
        
        return relatedLicenseProducts_List;
    }

    public void sendEmailWhenLicenseGenerated(list<License__c> newLicenses_List, map<Id, License__c> oldLicenses_Map) {
        
        
        map<Id, License__c> relatedLicensesWithAtt_Map = getRelatedLicensesWithAtt(newLicenses_List, oldLicenses_Map);
        
    }
    
    private map<Id, License__c> getRelatedLicensesWithAtt(list<License__c> newLicenses_List, map<Id, License__c> oldLicenses_Map) {
        
        set<Id> relatedLicensesIds_Set = new set<Id>();
        map<Id, License__c> relatedLicensesWithAtt_Map;
         
        for (License__c newLicense : newLicenses_List) {
            
            if (newLicense.Status__c != null) {
                
                if (newLicense.Status__c.EqualsIgnoreCase('Activated') && newLicense.Status__c != oldLicenses_Map.get(newLicense.Id).Status__c) {
                    
                    relatedLicensesIds_Set.add(newLicense.Id);
                }
            }
        }
        
        if (!relatedLicensesIds_Set.isEmpty()) {
            
            relatedLicensesWithAtt_Map = new map<Id, License__c>([SELECT Id, Product_Type__c, Related_Lead__c, (SELECT Id FROM Attachments order by CreatedDate desc limit 1)
                                          FROM License__c WHERE Id IN : relatedLicensesIds_Set]);
        }
        
        return relatedLicensesWithAtt_Map;
    }
    
    
    public void sendHttpReqToGenerateLicense(list<License__c> newLicenses_List) {
        
        list<String> licensesAsStr_List = new list<String>();
        Id EvaluationRecordTypeId = [SELECT Id FROM RecordType WHERE Name =: 'Evaluation' and SobjectType = 'License__c'].Id;
        
        for (License__c newLicense : newLicenses_List) {
            
            if (newLicense.RecordTypeId == EvaluationRecordTypeId) {
                
                String licensesAsStr = JSON.Serialize(newLicense);
                licensesAsStr_List.add(licensesAsStr);
            }
        }
        
        try {   
            sendFutureHttpReqToGenerateLicense(licensesAsStr_List);
        }
        
        catch(Exception e) {
            
        }
        
    }
    
    // This method is executed upon user action and handles 1 License record at a time
    webservice static String sendFutureHttpReqToGenerateLicenseOnUserAction(String LicenseId) {
        
        License__c newLicense = [SELECT  l.Switch_Serial__c, l.Split_Enabled__c, l.Split_Counter__c, 
                                 l.Send_me_email__c, l.Sales_Order__c, l.Revision__c, l.Related_Lead__c, l.RecordTypeId, 
                                 l.Quantity__c, l.Product__c, l.Product_Type__c, l.Parent_License__c, l.OwnerId, l.Name, 
                                 l.Mellanox_PN__c, l.Master_Serial__c, l.Managed_Nodes__c, l.License_Files__c, l.License_Chosen__c,
                                 l.Item__c,
                                 l.Id, l.HA_MAC_Address__c, l.Customer_PO__c, l.MAC_address__c,  
                                 l.Customer_Name__c, l.Customer_Email__c, l.Country__c, 
                                 l.Contract_End_Date__c, l.ContentDelivery_link__c, l.Contact__c, l.Changes_Permitted__c, 
                                 l.Auto_Split__c, l.AccountId__c From License__c l WHERE Id =: LicenseId];
                        
            
        map<String, License_Product__c> licenseProductName2LicenseProduct_Map = new map<String, License_Product__c>();
        
        for (License_Product__c lProduct : [SELECT Id, Name, UFM_Permanent_Dynamic_Param__c, UFM_Permanent_Static_Param__c FROM License_Product__c WHERE Name =: newLicense.Product_Type__c]) {
            
            licenseProductName2LicenseProduct_Map.put(lProduct.Name, lProduct);
        }
        
        try {
            
            handleHttpRequestForLicense(newLicense, licenseProductName2LicenseProduct_Map);
        }
        
        catch(Exception e) {
            
            return 'Could not process the request  :' + e.getMessage();
        }       
        
        return 'Your request has been succesfuly been processed';                            
    }
    
    
    
    @future(callout=true)
    public static void sendFutureHttpReqToGenerateLicense(list<String> licensesAsStr_List) {
        
        map<String, License_Product__c> licenseProductName2LicenseProduct_Map = new map<String, License_Product__c>();
        
        for (License_Product__c lProduct : [SELECT Id, Name, UFM_Permanent_Dynamic_Param__c, UFM_Permanent_Static_Param__c FROM License_Product__c]) {
            
            licenseProductName2LicenseProduct_Map.put(lProduct.Name, lProduct);
        }
        
        list<License__c> newLicenses_List = new list<License__c>();
        
        for (String licenseAsStr : licensesAsStr_List) {
            
            License__c License2Insert = new License__c();
            License2Insert = (License__c)JSON.deserialize(licenseAsStr, License__c.class);
            newLicenses_List.add(License2Insert);
        }
        
        
        for (License__c newLicense : newLicenses_List) {
            
            handleHttpRequestForLicense(newLicense, licenseProductName2LicenseProduct_Map);

        }
    }
    
    public static void handleHttpRequestForLicense(License__c newLicense, map<String, License_Product__c> licenseProductName2LicenseProduct_Map) {
                
        map<String, String> fieldNames2ValuesMap = getStaticFieldNamesAndValuesMap(newLicense, licenseProductName2LicenseProduct_Map);
        map<String, String> fieldNames2ValuesMapDynamic = getDynamicFieldNamesAndValuesMap(newLicense, licenseProductName2LicenseProduct_Map, fieldNames2ValuesMap);
        
        system.debug('fieldNames2ValuesMapDynamic : ' + fieldNames2ValuesMapDynamic);
        
        fieldNames2ValuesMap.putAll(fieldNames2ValuesMapDynamic); 
        
        system.debug('fieldNames2ValuesMap : ' + fieldNames2ValuesMap);
            
        
        XmlStreamWriter writer = new XmlStreamWriter();
        writer.writeStartDocument(null,'1.0');
        writer.writeStartElement(null,'license',null);
        
        writer.writeStartElement(null,'username',null);
        writer.writeCharacters('salesforce');
        writer.writeEndElement();
        writer.writeStartElement(null,'password',null);
        writer.writeCharacters('z,xc#ok^Ujsd982w3');
        writer.writeEndElement();
        writer.writeStartElement(null,'LicenseId',null);
        writer.writeCharacters(newLicense.Id);
        writer.writeEndElement();
        
        for (String key : fieldNames2ValuesMap.keySet()) {
            
          writer.writeStartElement(null,key.trim(),null);
          if (fieldNames2ValuesMap.get(key) != null) {
            
             writer.writeCharacters( String.valueOf(fieldNames2ValuesMap.get(key)).trim());
          }
          writer.writeEndElement();
        }
        
        writer.writeEndElement();
        writer.writeEndDocument();
        string content = writer.getXmlString();
        writer.close();
               
        system.debug('content : ' + content);
        Http m_http = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint('https://swlic01.mellanox.com/');
        req.setHeader('Content-Type','application/json');        
        req.setMethod('POST');
        content = content.replaceAll('\\s+',' ');
        req.setBody(content);
        
        try {   
            httpResponse response = m_http.send(req);
            
            //get the list of header names (keys)
            string[] headerkeys = response.getHeaderKeys();
            string responseBody = response.getBody();
            system.debug('responseBody: ' + responseBody);
            
        }
        
        catch(Exception e) {
            
            
        }
                
    }
    
    public static map<String, String> getStaticFieldNamesAndValuesMap(License__c newLicense, map<String, License_Product__c> licenseProductName2LicenseProduct_Map) {
        
        map<String, String> fieldNames2ValuesStaticMap = new map<String, String>();
        
        License_Product__c relatdLProduct = licenseProductName2LicenseProduct_Map.get(newLicense.Product_Type__c);
        
        if (relatdLProduct != null && relatdLProduct.UFM_Permanent_Static_Param__c != null) {
            list<String> LPFieldsAsStr_List = relatdLProduct.UFM_Permanent_Static_Param__c.split(';');
            
            list<String> fieldsToNameAndValue_List = new list<String>();
            for (String field : LPFieldsAsStr_List) {
                
                fieldsToNameAndValue_List = field.split(',');
                
                fieldNames2ValuesStaticMap.put(fieldsToNameAndValue_List[0], fieldsToNameAndValue_List[1]);
                fieldsToNameAndValue_List.clear();
            }
        }
        
        return fieldNames2ValuesStaticMap;
    }
    
    
    public static map<String, String> getDynamicFieldNamesAndValuesMap(License__c newLicense, map<String, License_Product__c> licenseProductName2LicenseProduct_Map, map<String, String> fieldNames2ValuesMap) {
                
        License_Product__c relatdLProduct = licenseProductName2LicenseProduct_Map.get(newLicense.Product_Type__c);
        
        map<String, String> fieldNames2ValuesMapDynamic = new map<String, String>();
        
        //system.debug('newLicense.Product_Type__c : ' +  newLicense.Product_Type__c);
        //system.debug('licenseProductName2LicenseProduct_Map Key set : ' +  licenseProductName2LicenseProduct_Map.keySet());
        //system.debug('relatdLProduct : ' +  relatdLProduct);
        //system.debug('relatdLProduct.UFM_Permanent_Dynamic_Param__c : ' +  relatdLProduct.UFM_Permanent_Dynamic_Param__c); 
        
        
        list<String> LPFieldsAsStr_List = new list<String>();
        
        if (relatdLProduct != null && relatdLProduct.UFM_Permanent_Dynamic_Param__c != null) {
            
            LPFieldsAsStr_List = relatdLProduct.UFM_Permanent_Dynamic_Param__c.split(';');
            system.debug('LPFieldsAsStr_List : ' +  LPFieldsAsStr_List);
            
            list<String> fieldsToNameAndValue_List = new list<String>();
            for (String field : LPFieldsAsStr_List) {
                
                fieldsToNameAndValue_List = field.split(',');
                
                Object o = newLicense.get(fieldsToNameAndValue_List[1]);
                String returnValue = String.valueOf(o);
                
                system.debug('fieldsToNameAndValue_List[0] : ' +  fieldsToNameAndValue_List[0]);
                system.debug('returnValue : ' +  returnValue);
                
                fieldNames2ValuesMapDynamic.put(fieldsToNameAndValue_List[0], returnValue);
                fieldsToNameAndValue_List.clear();
            }
        }
        
        system.debug('fieldNames2ValuesMapDynamic 582 : ' +  fieldNames2ValuesMapDynamic);  
        
        return fieldNames2ValuesMapDynamic;
    }
    
}