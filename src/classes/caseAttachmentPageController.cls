/**
 * @Author- Persistent
 * @Description-This class had methods that use for getting Recoord for Attachment.   
 */

public class caseAttachmentPageController {
    public List<Community_Case_Closure_Comments__c> commentList{get;set;}
    public String caseID;
    Case cse;
    
    
    public caseAttachmentPageController(ApexPages.StandardController controller) 
    {
        this.cse = (Case)controller.getRecord();
        caseID = cse.id;
        commentList = [select parentId__c,AttachmentID__c,AttachmentName__c,Thread_Title__c,Postback_Comment__c,createddate from Community_Case_Closure_Comments__c where Case__c=:caseID and AttachmentName__c!=null];
        
       
    }
    
  
}