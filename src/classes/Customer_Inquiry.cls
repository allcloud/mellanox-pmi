public class Customer_Inquiry {
	public Customer_Order_Inquiry__c inq {get; set;}
	public boolean is_submit {get; set;}
	public String inq_num {get; set;}
	private List<Customer_Order_Inquiry__c> lst_inq;
	public List<Customer_Inquiry_Line_Item__c> lines {get; set;}
	public List<Customer_Inquiry_Line_Item__c> lines_inserts {get; set;}
	boolean is_error;
	boolean has_line;
	
	public Customer_Inquiry() {
		is_error = false;
		has_line=false;
		inq = new Customer_Order_Inquiry__c();
		lines = new List<Customer_Inquiry_Line_Item__c>();
		lines_inserts = new List<Customer_Inquiry_Line_Item__c>();
		
		inq_num = '';
		is_submit = false;
		for (integer i=0; i<5; i++) {
			lines.add(new Customer_Inquiry_Line_Item__c(Warranty_Start_Date__c=System.today()) );
		}
	}
	public pagereference save() {
		if( !String.isBlank(inq.Reseller_name__c) && (String.isBlank(inq.Reseller_Contact_Name__c) || inq.Reseller_Contact_email__c == null)) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill the Reseller Contact Name and Email.'));
			is_error = true;
		}
		for (Customer_Inquiry_Line_Item__c c : lines) {
			if(!String.isBlank(c.Covered_Item__c))
				has_line = true;
			if( !String.isBlank(c.Covered_Item__c) && (String.isBlank(c.Serial_Numbers__c) && String.isBlank(c.Cust_PO_Line_Number__c))) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill either serial number or Customer PO Line Number.'));
				is_error = true;
			}
		}
		
		if (!has_line) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill at least 1 covered item.'));
			is_error = true;
		}
		if(!is_error) {
			lines_inserts.clear();
			insert inq;
			for (Customer_Inquiry_Line_Item__c c : lines) {
				if(!String.isBlank(c.Covered_Item__c)) {
					c.Customer_Order_Inquiry__c = inq.ID;
					lines_inserts.add(c);
				} 
			}
			if(lines_inserts.size() > 0)
				insert lines_inserts;
				
			is_submit = true;
			lst_inq = [select Name from Customer_Order_Inquiry__c where id = :inq.ID];
			if(lst_inq != null && lst_inq.size() > 0)
				inq_num = lst_inq[0].Name;
		}
		return null;
	}
	public pagereference cancel() {
		//Pagereference pg = page.My_orders;
		PageReference pg = new PageReference('/apex/My_orders');
        pg.setRedirect(true);
        return pg;
	}
	//test customer inquiry form
    static testMethod void myUnitTest3() {
 		PageReference pageRef = Page.Customer_Order_Inquiry;
	    Test.setCurrentPage(pageRef);
	    
	    Customer_Inquiry inquiry_ctl = new Customer_Inquiry();
	    Customer_Order_Inquiry__c inq = new Customer_Order_Inquiry__c(Customer_PO_Number__c='test inquiry');
	    //insert inq;
	    
	    inquiry_ctl.inq = inq;
	    inquiry_ctl.save();
	    inquiry_ctl.cancel();   	
    } //end test 3
}