public with sharing class VF_MDF_ListView {
    
    public list<SFDC_MDF__c> relatedMDF_list {get;set;}
    public list<SFDC_MDF_Claim__c> relatedMDFClaims_list {get;set;}
    
    public VF_MDF_ListView() {
        
        Id currentUserId = UserInfo.getUserId();
        	
    	relatedMDF_list = [SELECT s.Total_Activity_Cost__c, s.Target_Audience__c, s.TAM__c,  
                        s.Status__c, s.Select_Budget_Warning__c, s.Request_ID__c,  
                        s.Purpose_of_Activity__c,  s.Other_Vendors_Participating__c, s.Other_Activity_Type__c,
                        s.Name, s.Mellanox_attendees__c, s.Mellanox_Attendee_Requested__c, s.Marketing_Dollars_Requested__c,
                        s.Level_of_Sponsorship__c, s.Fund_Request_Type__c,  Count_of_Claims__c,
                        s.Claim_Deadline_Date__c, s.CAM__c, s.Budget__c, s.Approved__c, PO_Number__c,
                        s.Approved_Date__c, s.Amount__c, s.Age_Days__c, s.Additional_Notes__c, s.Activity_Vertical_Market_Focus__c,
                        s.Activity_Support__c, s.Activity_Start_Date__c, s.Activity_Product_Focus__c, 
                        s.Activity_End_Date__c, s.Activity_Description__c, s.Activity_Country__c, s.Activity_City__c,
                        s.Account__c, (SELECT Id FROM R00N30000002DjDcEAK__r)  From SFDC_MDF__c s order by lastModifiedDate desc]; 
                            
        relatedMDFClaims_list = [Select s.TAM__c, s.SystemModstamp, s.Status__c, 
                                 s.Fund_Request__r.Name, s.Fund_Request__r.Id,
                                 s.Payment_Method__c,  s.PO_Number__c,  s.Name, PONumber__c, 
                                 s.Link_To_Invoice_URL__c, s.Invoice_Number_to_Mellanox__c, s.Invoice_ID__c, s.Invoice_Date__c, s.Id, s.Fund_Request__c,
                                  s.Claim_Paid_Date__c, s.Claim_ID__c,  s.Budget__c, s.Approved__c,
                                 s.Approved_Date__c, s.Amount__c, s.Age_Days__c, s.Activity_Result__c, s.Activity_Result_Description__c, s.Account__c
                                 From SFDC_MDF_Claim__c s order by lastModifiedDate desc];
        
    }
}