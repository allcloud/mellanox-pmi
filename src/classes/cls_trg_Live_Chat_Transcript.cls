public class cls_trg_Live_Chat_Transcript {
	
	
	// This method will update the related Case's Description with the Live Chat Transcript body when the record is created
	public void updateLiveChatTranscriptRelatedCase(list<LiveChatTranscript> newTranscripts_List) {
		
		set<Id> relatedCasesIds_Set= new set<Id>();
		
		
		for (LiveChatTranscript script : newTranscripts_List) {
			
			relatedCasesIds_Set.add(script.CaseId);
		}
		
		if ( !relatedCasesIds_Set.isEmpty() ) {
		
			map<Id, Case> relatedCasesIds2Cases_map = new map<Id, Case> ([SELECT Id, State__c, Live_Agent_Description__c FROM Case WHERE ID IN : relatedCasesIds_Set]);
			
			if ( !relatedCasesIds2Cases_map.values().isEmpty() ) {
				
				list<Case> casesToUpdate_List = new list<Case>();
				
				for (LiveChatTranscript script : newTranscripts_List) {
					
					case currentCase = relatedCasesIds2Cases_map.get(script.CaseId);
					
					if (currentCase != null && currentCase.Id != null) {
						
						currentCase.Live_Agent_Description__c = script.Body;

						try {
							String liveAgentDescription = script.Body;
					        String startIndex = liveAgentDescription.substring(liveAgentDescription.indexOf(')') +1);
					        String callComments = startIndex.substring(startIndex.indexOf('('));
							List<String> callComments_List = callComments.split('<br>');
							Integer lineNumber = 0;
							currentCase.Live_Agent_Comments__c = '';
							currentCase.Main_Live_Agent_Comments__c = '';
							for (String line : callComments_List) {
								lineNumber++;
	                            if(lineNumber > 1 && lineNumber < 11){
	                                line = line.mid(line.indexOf(')') +1, line.length());
	                                currentCase.Live_Agent_Comments__c += line + '<br>';
	                                if(lineNumber < 4){   
	                                    currentCase.Main_Live_Agent_Comments__c += line + '<br>';
	                                }
	                        	}
							}
						}
						catch(Exception e) {
							system.debug('ERROR Updating Case Live Agent Comments: ' + e.getMessage());
						}
						
						currentCase.State__c = 'Closed';
						casesToUpdate_List.add(currentCase);
					}
				}
				
				if (!casesToUpdate_List.isEmpty()) {
					
					try {
					
						update casesToUpdate_List;
					}
					
					catch(Exception e) {
						
						system.debug('ERROR Updating the Case : ' + e.getMessage());
					}
				}
			}
		}
	}
}