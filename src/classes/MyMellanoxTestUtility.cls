@isTest
public class MyMellanoxTestUtility {

    public static My_Mellanox_Setting__c createMyMellanoxSettings() {

        My_Mellanox_Setting__c settings;
        System.runAs(new User(Id = UserInfo.getUserId())) {

            settings = My_Mellanox_Setting__c.getInstance();
            System.debug('settings: ' + settings);
            if ( settings == null ) {
                settings = new My_Mellanox_Setting__c();
            }

            /*
            String testTemplateId = createEmailTemplate().Id;
            testTemplateId = testTemplateId.substring(0, 15);
            settings.Dealer_Portal_Interest_Lead_RecordType__c = [select name, Id 
                                                                  from RecordType 
                                                                  where developername = 'Dealer_Portal_Interest_Lead' 
                                                                  and SObjectType='Lead'].Id;
            String testVFTemplateId = createEmailTemplate('Visualforce').Id;
            String testVFCustLeadNotifTemplateId = createEmailTemplate('Visualforce', 'User', 'TCC_Customer_Lead__c').Id;
            testVFTemplateId = testVFTemplateId.substring(0, 15);
            Network comm = [Select Id from Network limit 1];
            CollaborationGroup grp = new CollaborationGroup(Name = 'Test Group', collaborationType = 'Public', networkId = comm.Id);
            insert grp;
			*/

            for ( Profile p : [SELECT Name FROM Profile WHERE Name like 'MyMellanox%'] ) {
                if ( p.Name == 'MyMellanox Design In Community User' ) {
                    settings.Design_In_Profile_ID__c = String.valueOf(p.Id).substring(0, 15);
                } else if ( p.Name == 'MyMellanox System Support Community User' ) {
                    settings.System_Support_Profile_ID__c = String.valueOf(p.Id).substring(0, 15);
                }
            }

            for ( Group g : [select DeveloperName from Group where Name like '%(Partner Users)%'] ) {
                if ( g.DeveloperName == 'Design_Support_Partner_Users' ) {
                    settings.Design_In_Users_Group_ID__c = String.valueOf(g.Id).substring(0, 15);
                } else if ( g.DeveloperName == 'System_Support_Partner_Users' ) {
                    settings.System_Support_Users_Group_ID__c = String.valueOf(g.Id).substring(0, 15);
                }
            }
            upsert settings;
        }
        return settings;
    }

    public static Account getAccount() {
        Account acc = new Account(Name = math.random()+ 'TESTAccount'+math.random());
        insert acc;
        return acc;
    }

    public static Contact getContact() {
        return getContact(getAccount().Id);
    }

    public static Contact getContact(ID accountId) {
        Contact contact = new Contact(
            FirstName = 'Test', Lastname = 'McTesty', MailingCountry = 'TestCountry', MailingCity = 'test',
            MailingState ='test', MailingPostalCode='test', MailingStreet = 'test', AccountId = accountId,
            MobilePhone = '123456', Email = System.now().millisecond() + 'testcontact@mellanox.example.com');
        insert contact;
        return contact;
    }

    public static User getPortalUser(ID profileId) {
        return getPortalUser(profileId, getContact().Id);
    }

    public static User getPortalUser(ID profileId, ID contactId) {

        Long rand = Decimal.valueOf(Math.random() * 10000000).longValue();
        User u = new User(
            LastName = 'testuz', FirstName = 'TestGuy', Alias = 'a' + rand, Email = rand + 'testuser@mellanox.example.com',
            isActive = true, Username = rand + 'testuser@mellanox.example.com',  CommunityNickname = 'c' + rand,
            ProfileId = profileId, TimeZoneSidKey='America/Chicago', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US',  ContactId = contactId);
        insert u;
        return u;
    }

    public static User getDesignInCommunityUser() {
        return getPortalUser(MyMellanoxSettings.DesignInUserProfileId);
    }
    public static User getDesignInCommunityUser(ID contactId) {
        return getPortalUser(MyMellanoxSettings.DesignInUserProfileId, contactId);
    }

    public static User getSystemSupportCommunityUser() {
        return getPortalUser(MyMellanoxSettings.SystemSupportUserProfileId);
    }
    public static User getSystemSupportCommunityUser(ID contactId) {
        return getPortalUser(MyMellanoxSettings.SystemSupportUserProfileId, contactId);
    }

    private static testMethod void selfCoverageAndTest() {
        // Cover getAccount, getContracts2
        getAsset2(1, false, null, null);
        getAsset2(1, true, null, null);
        getCases(1, false);
        getCases(1, true);

        List<RecordType> caseRts = [SELECT Id FROM RecordType WHERE DeveloperName = 'TechnicalSupport' LIMIT 1];
        if(caseRts.size() > 0) {
            getCases(1, false, caseRts[0]);
            getCases(1, true, caseRts[0]);
        } 
    }

    /**
     * Initialize or create test cases
     * @param   numToInsert  No. of records to initialize / create
     * @param   doInsert     TRUE/FALSE flag to control creation / initialization on of records. 
     * @return  List<Case>   Reference to list of records.
     *
     * @author  Abdul Sattar (Magnet 360)
     * @Date    09.10.2015
     */
    public static List<Case> getCases( Integer numToInsert, Boolean doInsert ){
        
        List<Case> cases = new List<Case>();
        
        for(Integer i = 0; i < numToInsert; i++){
            Case c = new Case(Subject='Test Case - ' + i, Description = 'Test Case Description - ' + i, Priority = 'Low', Status = 'Open');
            cases.add(c);
        }
        
        if(doInsert) {
            INSERT cases;
            System.assert(cases[0].Id != NULL, 'Unable to create test cases.');
        }

        return cases;
    }

    /**
     * Initialize or create test cases for a specific record type.
     * @param   numToInsert  No. of records to initialize / create
     * @param   doInsert     TRUE/FALSE flag to control creation / initialization on of records.
     * @param   rt           Case record type.
     * @return  List<Case>   Reference to list of records.
     *
     * @author  Abdul Sattar (Magnet 360)
     * @Date    09.10.2015
     */
    public static List<Case> getCases( Integer numToInsert, Boolean doInsert, RecordType rt ){
        
        List<Case> cases = new List<Case>();
        
        for(Integer i = 0; i < numToInsert; i++){
            Case c = new Case(RecordTypeId = rt.Id, Subject='Test Case - ' + i, Description = 'Test Case Description - ' + i, Priority = 'Low', Status = 'Open');
            cases.add(c);
        }
        
        if(doInsert) {
            INSERT cases;
            System.assert(cases[0].Id != NULL, 'Unable to create test cases.');
        }

        return cases;
    }

    /**
     * Initialize or create test contracts (Contract2__c)
     * @param   numToInsert          No. of records to initialize / create
     * @param   doInsert             TRUE/FALSE flag to control creation / initialization on of records. 
     * @return  List<Contract2__c>   Reference to list of records.
     *
     * @author  Abdul Sattar (Magnet 360)
     * @Date    09.10.2015
     */
    public static List<Contract2__c> getContracts2( Integer numToInsert, Boolean doInsert, Account act ){
        
        List<Contract2__c> contracts2 = new List<Contract2__c>();
        
        if(act == NULL)
            act = getAccount();

        Contract2__c tempCont = new Contract2__c( Account__c = act.Id
                                                 ,Contract_Start_Date__c = System.today()
                                                 ,Contract_Term_months__c = 12);

        for(Integer i = 0; i < numToInsert; i++){
            contracts2.add(tempCont.clone());
        }
        
        if(doInsert) {
            INSERT contracts2;
            System.assert(contracts2[0].Id != NULL, 'Unable to create test contracts (Contract2__c).');
        }

        return contracts2;
    }

    /**
     * Initialize or create test assets (Asset2__c)
     * @param   numToInsert       No. of records to initialize / create
     * @param   doInsert          TRUE/FALSE flag to control creation / initialization on of records. 
     * @return  List<Asset2__c>   Reference to list of records.
     *
     * @author  Abdul Sattar (Magnet 360)
     * @Date    09.10.2015
     */
    public static List<Asset2__c> getAsset2( Integer numToInsert, Boolean doInsert, Account act, Contract2__c cont ){
        
        List<Asset2__c> assets2 = new List<Asset2__c>();
        
        if(act == NULL)
            act = getAccount();

        if(cont == NULL)
            cont = getContracts2(1, true, act)[0];

        Asset2__c a2 = new Asset2__c(Name ='Test Asset'
                                    ,Asset_Type__c = 'Support'
                                    ,part_number__c = 'weryu5SP'
                                    ,Serial_Number__c = '00'
                                    ,Contract2__c = cont.Id );

        for(Integer i = 0; i < numToInsert; i++){
            Asset2__c tempA2 = a2.Clone();
                tempA2.Name = tempA2.Name + ' - ' + i;
                tempA2.Serial_Number__c = '0' + i;
            assets2.add(tempA2);
        }
        
        if(doInsert) {
            INSERT assets2;
            System.assert(assets2[0].Id != NULL, 'Unable to create test assets (Asset2__c).');
        }

        return assets2;
    }
}