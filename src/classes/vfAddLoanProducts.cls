public with sharing class vfAddLoanProducts {

    private final Loan__c loan;             // input Loan__c object
    public String byKeyword {get; set;}     // Search input string
    public Boolean ResultExist {get; set;}  //flag for result list//    
    public List<ProductSearchResult> lst_Products {get; set;}   // Search result
    
    public vfAddLoanProducts(ApexPages.StandardController controller) 
    {
        this.loan = (Loan__c)controller.getRecord();
        ResultExist = false;
    }
    
    // searchProducts - looks for all products by Name or by Description with the input Keyword which are not related to the input Loan.
    public void searchProducts()
    {
        ResultExist = false;
        lst_Products = new List <ProductSearchResult>();
        
        if (byKeyword != null && byKeyword !='')
        {
            List <Product2> LstProduct = [Select Id, Name, Description, ProductCode, MSRP__c, Family, Product_Type1__c
                                          From Product2
                                          Where (Name like : '%' + byKeyword + '%'
                                          or Description like : '%' + byKeyword + '%')
                                          And Id not in (Select Product__c 
                                                         From Loan_Product__c 
                                                         Where Loan__c =: loan.Id)];
                                                 
            if (LstProduct.size() > 0)
            {
                ResultExist = true;             

                for(Product2 prd : LstProduct)
                {
                    ProductSearchResult prodRes = new ProductSearchResult(prd);
                    lst_Products.add(prodRes);
                }
                
            }
        }       
    }
    
    // This function is called by the 'Add & New Search' button and calls the general function addProducts
    public PageReference addAndSearch()
    {
        Boolean result = addProducts();
        
        if (result)
        {
            lst_Products = new List<ProductSearchResult>();
            byKeyword = '';
        }
        
        return null;    
    }
    
     // This function is called by the 'Add' button and calls the general function addProducts
    public PageReference add()
    {
        Boolean result = addProducts();

        if (result)
        {
            PageReference p =  new PageReference('/'+ loan.Id);
            return p;       
        } else
        {
            return null;
        }
    }

    // The general addProducts function is looking for selected products. If the Quantity field for each selected product is valid, adds the product to the loan.
    // returns true if the products were added successfully or false if an error occured.
    private Boolean addProducts()
    {       
        List <Loan_Product__c> LstNewLoanProds = new List <Loan_Product__c>();
        Boolean ErrorOccured = false;
        
        if (lst_Products == null)
            return false;
        
        // a loop on the search result objects  
        for (ProductSearchResult result : lst_Products) 
        {
            // if the product is selected, validate the Quantity field and adds it to the loan if valid
            if (result.selected)
            {
                if (result.Qty == null || result.Qty == '')
                {
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Quantity for selected product ' + result.prd.Name));
                    ErrorOccured = true;
                } else
                {
                    Loan_Product__c loan_Product = new Loan_Product__c(Product__c = result.prd.Id, Loan__c=loan.Id);
                    try
                    {
                        loan_Product.Quantity__c = Integer.valueOf(result.Qty);
                        LstNewLoanProds.add(loan_Product);
                    } catch (Exception ex)
                    {
                        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a non-decimal numeric value on the Quantity field for selected product ' + result.prd.Name));
                        ErrorOccured = true;                        
                    }
                    
                }
            }
        }
        
        if (ErrorOccured)
            return false;
    
        if (LstNewLoanProds.size() > 0)
        {   
            insert LstNewLoanProds;
        }       
        
        return true;
    }
    
    //go back to opportunity page
    public PageReference cancel()
    {
        PageReference p =  new PageReference('/'+ loan.Id);
        return p;
    }  


    //output search object result// 
    public class ProductSearchResult
    {
        public Product2 prd {get; set;}
        public Boolean selected {get; set;}
        public String Qty {get; set;}
        public String Comment {get; set;}
                
        public ProductSearchResult(Product2 prd)
        {
                this.prd = prd;
                this.selected = false;
        }
    }    
}