public class PublicMilestonesList {
    private List<Milestone1_Milestone__c> Msts;
    private Milestone1_project__c project; 
    public PublicMilestonesList(ApexPages.StandardController controller) {
        this.project= (Milestone1_project__c)controller.getRecord();
    }
    public List<Milestone1_Milestone__c> getMsts()
    {
        Msts = [Select id,name, Kickoff__c, Deadline__c, Complete__c, Parent_Milestone__c, Customer_Due_Date__c 
                 FROM Milestone1_Milestone__c where project__c = :project.id and IsPublic__c = true];
        
        return Msts;
    }
}