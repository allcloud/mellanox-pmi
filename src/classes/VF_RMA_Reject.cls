public with sharing class VF_RMA_Reject
{   
      
    public ID rmaId;
    
    public VF_RMA_Reject (ApexPages.StandardController controller) 
         {    
             //id = Apexpages.currentPage().getParameters().get('Id');    
         }

    
    
    
    public PageReference Reject_RMA() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         rmaId = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         system.debug('id is : ' + rmaId);
         //get the related RMA id from the case
         RMA__c rm = [Select RMA_state__c, SN_NotApproved_Count__c,Num_of_SNs__c  From RMA__c WHERE id =: rmaId];


         if(rm.SN_NotApproved_Count__c != rm.Num_of_SNs__c )
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'All SNs need to be \'Not Approved\' in order to Reject RMA'));
         return Apexpages.currentPage();

         }

                  
         rm.RMA_state__c = 'Rejected';
         update rm;
         return new Pagereference('/'+RMAId);
     }




static testMethod void Test_VF_RMA_Reject() 
    {
        //list of serial numbers to be related to the RMA and to avoid many inserts
                List<Serial_Number__c> lst_snOfRMA = new List<Serial_Number__c>();
                List<Product2> lst_prods = New List<Product2>();
                //instance of object creator
                CLS_ObjectCreator obj = new CLS_ObjectCreator();

                Account acc = obj.createAccount();
                insert acc;
        
               Contact con = obj.CreateContact(acc);
               con.email = 'hgft56@ibm.com';
               insert con;
                
                MellanoxSite__c  site  = obj.createMellanoxSite();
                insert site;
                 
                RMA__c rma = obj.CreateRMA();
                rma.e_mail__c = 'dsfds@ibm.com';
                rma.RMA_state__c= 'Pending Sales Approval';
                rma.contact__c = con.id;
                insert rma;
                
                rma.RMA_state__c = 'Pending Sales Approval';
                update rma;
                
                Product2 p1 =  obj.createProduct();
                p1.Inventory_Item_id__c = '90989';
                lst_prods.add(p1);
               
                                  
                Serial_Number__c sn1 = obj.createSerialNumber(rma,p1);
                sn1.Approval__c = 'Not Approved';
                sn1.reject_reason__c = 'not found';
                lst_snOfRMA.add(sn1);
                
                                
                
                insert lst_prods;
                insert lst_snOfRMA; 
                
                Test.setCurrentPageReference(new PageReference('Page.VF_OEM_RMA_Cancel')); 
                System.currentPageReference().getParameters().put('id',rma.Id );       
                System.currentPageReference().getParameters().put('isTest','Testing' );  
              
               Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rma);
                VF_RMA_reject vf = new VF_RMA_reject(teststandard);
                vf.reject_rma();
    
    }

}