public class cls_Create_User {
	
	private static final String TYPE_DESIGN_IN = 'Mellanox Silicon Design-In customer';
	private static final String TYPE_SYSTEM = 'Mellanox System Customer';
	private static Id MyMellanoxLicenseUser = MyMellanoxSettings.MyMellanoxLicenseUser;
	private static Id MyMellanoxOEMSystemSupport = MyMellanoxSettings.SystemOEMId;
	private static Id MyMellanoxSystemSupport = MyMellanoxSettings.SystemSupportUserProfileId;
	
	// This method will create a user asynchronically and will handle duplication of User Names in other Salesforce Orgs
	  
    public static User insertUser(String User_Str, set<String> contactsEmails_Set, boolean DontSendLoginEmail, boolean createdFromLicense) {
    	
    	User user2Insert = new User();
	    user2Insert = (User)JSON.deserialize(User_Str, User.class);
	    user2Insert.LocaleSidKey = 'en_US';
    	user2Insert.EmailEncodingKey = 'ISO-8859-1';
    	user2Insert.LanguageLocaleKey = 'en_US';
    	user2Insert.FederationIdentifier = user2Insert.userName;
    	
    	if ( DontSendLoginEmail && (user2Insert.ProfileId == MyMellanoxOEMSystemSupport || 
    		user2Insert.ProfileId == MyMellanoxSystemSupport) ) {
    			
    			Database.DMLOptions dmlo = new Database.DMLOptions();
                dmlo.EmailHeader.triggerUserEmail = false; 
    		}
    		
    	if (createdFromLicense) {
    		
    		user2Insert.License_send_portal_email__c = true;
    	}	
	    
	    system.debug('user2Insert.ContactId :' + user2Insert.ContactId);
	    
	    system.debug('user2Insert :' + user2Insert);
	    
	    try {	
	   		insert user2Insert;
	   	}
	   	
	   	catch(Exception e) {
	   		
	   		if ( e.getMessage().contains('DUPLICATE_USERNAME') ) {
	   			
	   			user2Insert.userName = '_' + user2Insert.userName;
	   		}
	   		
	   		else if (e.getMessage().contains('DUPLICATE_COMM_NICKNAME') ) {
	   			
	   			user2Insert.CommunityNickname = user2Insert.CommunityNickname + 'Mel';
	   		}
	   			
   			try {	
   				
   				system.debug('user2Insert.userName : ' + user2Insert.userName);
   				insert user2Insert;
   			}
   			
   			catch(Exception me) {
   				
   				if ( me.getMessage().contains('DUPLICATE_USERNAME') ) {
   			
   					user2Insert.userName = 'm' + user2Insert.userName;
   				}
   				
   				else if (e.getMessage().contains('DUPLICATE_COMM_NICKNAME') ) {
	   			
	   				user2Insert.CommunityNickname = user2Insert.CommunityNickname + 'Mel';
	   			}
	   			
	   			insert user2Insert;
   			}
	   		
	   		system.debug('error message : ' + e.getMessage() + ', cause : ' + e.getCause());
	   		system.debug('user2Insert.userName : ' + user2Insert.userName);
	   	}
	   	
	   	if (contactsEmails_Set != null && !contactsEmails_Set.isEmpty()) {
	   		
	   		assignUsersAsLicenseOwner(contactsEmails_Set);
	   	}
	   	
	   	return user2Insert;
    }
    
    @future
    public static void insertUserAsync(String User_Str, set<String> contactsEmails_Set, boolean DontSendLoginEmail, boolean createdFromLicense) {
    	
    	insertUser(User_Str, contactsEmails_Set, DontSendLoginEmail, createdFromLicense);
    }
    
     
	public Contact getContactByEmail(String email) {
		
		Contact contactFromDB = [SELECT Id, FirstName, LastName, Email, Phone, Company_Name_from_Web__c, MailingStreet,Mellanox_Support_contact__c, 
							     Mellanox_silicon_device__c,Mellanox_silicon_description__c, purchase_Mellanox_equipment_from__c , Serial_Number__c,  MailingCity, 
							     MailingState, MailingPostalCode, access_required__c, MailingCountry, Contact_Type__c, Portal_Approval__c, AccountId,
							     need_access_to_silicon_datasheets__c,  Name FROM Contact WHERE Email =: email];
							     
		return contactFromDB;				     
	}
	
	public Contact createContact(String firstName, String lastName, String Email, String contactType, String region, Id AccountId) {
		
		Contact contactFromDB;
		
		try {	
		    contactFromDB = getContactByEmail(email);
			return contactFromDB;
		}
		
		catch (Exception e){
			
			system.debug('Could not find Contact');
		}
		
		Account accountByDomain;
		
		cls_Contact_Trigger handler = new cls_Contact_Trigger();
		
		if (AccountId != null) {
			
			list<Account> acc_List = [SELECT id  FROM Account WHERE Id =: AccountId];
			
			if (acc_List.isEmpty()) {
				
				accountByDomain = handler.getAccountByEmailDomain(email, null);  
			}
			
			else {
				
				accountByDomain = acc_List[0];
			}
		}	
		
		else {
			
			accountByDomain = handler.getAccountByEmailDomain(email, null);
		}
		contactFromDB = new Contact();
		contactFromDB.firstName = firstName;
		contactFromDB.lastName = lastName;
		contactFromDB.email = email;
		contactFromDB.Contact_Type__c = (contactType != null) ? contactType : TYPE_SYSTEM;
			
		if (accountByDomain != null && accountByDomain.Id != null) {
			
			contactFromDB.AccountId = accountByDomain.Id;
			
			try {
				
				insert contactFromDB;
			}
			
			catch(Exception me) {
				
				system.debug('Error in creating the Contact : ' + me.getMessage());
			}
		}
		
		else {
			
			if (String.isNotBlank(region) ) {
				
				Id accByRegionId;
				
				if (region.EqualsIgnoreCase('US')) {
					
					accByRegionId = ENV.UnknownUSAccount;
				}
				
				else if (region.EqualsIgnoreCase('EMEA')) {
					
					accByRegionId = ENV.UnknownEMEAAccount;
				}
				
				if (accByRegionId != null ) {
					
					contactFromDB.AccountId = accByRegionId;
					
					try {
				
						insert contactFromDB;
					}
					
					catch(Exception eme) {
						
						system.debug('Error in creating the Contact : ' + eme.getMessage());
					}
				}
			}
			
			else {
				
				contactFromDB.AccountId = ENV.UnknownAccount;
				
				try {
				
						insert contactFromDB;
					}
					
					catch(Exception emex) {
						
						system.debug('Error in creating the Contact : ' + emex.getMessage());
					}
			}
		}
		
		return contactFromDB;
	}
	
	public Account getAccountByName(String accName) {
		
		Account accFromDB = [SELECT Id FROM Account WHERE Name =: accName];
		
		return accFromDB;
	}
	
	
	public static void assignUsersAsLicenseOwner(set<String> contactsEmails_Set) {
	
	/*	
		list<License__c> licenses2Update_List = new list<License__c>();
		list<User> relatedUsersByContactsEmails_List = [SELECT Id, ProfileId, Contact.Email FROM User WHERE Contact.Email IN : contactsEmails_Set];
		
		map< String, list<License__c> > emails2Licenses_Map = new map<String, list<License__c> >();
		
		for (License__c currentLicense : [SELECT Id, Customer_Email__c FROM License__c WHERE Customer_Email__c IN : contactsEmails_Set]) {
			
			if (emails2Licenses_Map.containsKey(currentLicense.Customer_Email__c)) {
				
				emails2Licenses_Map.get(currentLicense.Customer_Email__c).add(currentLicense);
			}
			
			else {
				
				emails2Licenses_Map.put(currentLicense.Customer_Email__c, new list<License__c> {currentLicense});
			}
		}
		
		for (User currentUser : relatedUsersByContactsEmails_List) {
			
			if (emails2Licenses_Map.containsKey(currentUser.Contact.Email)) {
				
				for (License__c relatedLicense : emails2Licenses_Map.get(currentUser.Contact.Email)) {
					
					if (currentUser.ProfileId == myMellanoxLicenseUser)	{
						relatedLicense.ownerId = currentUser.Id;
						licenses2Update_List.add(relatedLicense);
					}
				}
			}
		}
		
		if (!licenses2Update_List.isEmpty()) {
			
			try {
				
				update licenses2Update_List;
			}
			
			catch(Exception e) {
				
				system.debug('ERROR In updating the Licenses : ' + e.getMessage());
			}
		}
		*/
	}

}