@istest
public with sharing class test_Batch_Update_ERI_Approve {
	
	static testmethod void test_scheduleERIApprove() {
		
	    CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        insert acc;
        
        Contract2__c contr = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
        insert contr;
        
        test.startTest();
        Schedule_Update_ERI_Approve sch = new Schedule_Update_ERI_Approve();
        
        String jobId = System.schedule('ScheduleCases',
        '0 0 0 8 9 ? 2020', sch);
        
        test.StopTest();
        
	}
}