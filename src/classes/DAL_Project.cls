public class DAL_Project {
    
    public static list<Milestone1_Project__c> getProjectsByIds(set<Id> projectsIds_Set)
    {
        return [Select m.Next_Milestone__c, m.Id, (Select Id, Name, Complete__c, Project__c, Deadline__c From Project_Milestones__r WHERE Complete__c != true ORDER BY Deadline__c ASC)
         From Milestone1_Project__c m WHERE m.Id IN : projectsIds_Set];
    }
    
    public static list<Milestone1_Project__c> getProjectsRelatedRecordsBySalesforceIds(set<String> projectsSalesforceIds_Set)
    {
        return [Select m.Id,(Select Id, OwnerId, IsDeleted, Name, RecordTypeId, CreatedDate, CreatedById, 
                LastModifiedDate, LastModifiedById,Project_Member__c, SystemModstamp, Project__c, Member_UserType__c,
                Contact_Phone__c From Customer_Group_Member__r) From Milestone1_Project__c m
                WHERE Id IN : projectsSalesforceIds_Set];
    }

}