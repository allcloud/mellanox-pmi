@isTest(seeAllData=true)
public with sharing class test_VF_Renewal_Contract_Form {
	
	static testMethod void test_theController() {
		
		User communityUser = [SELECT Id FROM User Where ContactId != null and isActive = true limit 1];
		
 	        CLS_ObjectCreator obj = new CLS_ObjectCreator();    
	        Account acc = obj.createAccount();
	        acc.Name = 'test Name';
	        acc.billingStreet = 'test Street';
	        insert acc;
	        
	        Contract2__c theContract2 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
	        theContract2.End_Contact_Name__c = 'test';
	        theContract2.End_Contact_Email__c = 'test@test.com';
	        theContract2.End_Customer_Phone__c = '12323123';
	        
	        theContract2.CTF__c = 'Extended Warranty';
	        theContract2.Renewal_Cost__c = 10000;
	               
	        insert  theContract2; 
	        
	        Product2 p = new Product2();
	        p.Name = 'MBX5020-2SFR';
	        p.Inventory_Item_id__c = '333';
	        p.Product_Type1__c = 'HERMON';
	        p.partNumber__c = '2323';
	        p.Support_Percent_Amount__c = 30;
	        p.DB_Price__c = 1000;
	        insert p;
	        
	        
	        Quote_Support_OPN__c opnBronze = new Quote_Support_OPN__c();
	        opnBronze.Product_OPN__c =  'MBX5020-2SFR';
	        opnBronze.Quantity__c = 4;
	        opnBronze.Support_Level__c = 'Bronze';
	        opnBronze.Support_OPN__c = p.Id;
	       
	        insert opnBronze;
	        
	        Quote_Support_OPN__c opnSilver = new Quote_Support_OPN__c();
	        opnSilver.Product_OPN__c =  'MBX5020-2SFR';
	        opnSilver.Quantity__c = 4;
	        opnSilver.Support_Level__c = 'Silver';
	        opnSilver.Support_OPN__c = p.Id;
	        insert opnSilver;
	        
	        Quote_Support_OPN__c opnGold = new Quote_Support_OPN__c();
	        opnGold.Product_OPN__c =  'MBX5020-2SFR';
	        opnGold.Quantity__c = 4;
	        opnGold.Support_Level__c = 'Gold';
	        opnGold.Support_OPN__c = p.Id;
	        insert opnGold;
	        
	        Quote_Support_OPN__c opnSilver2 = new Quote_Support_OPN__c();
	        opnSilver2.Product_OPN__c = 'erN4H';
	        opnSilver2.Support_Level__c = 'Silver';
	        opnSilver2.Quantity__c = 4;
	        opnSilver2.Support_OPN__c = p.Id;
	        insert opnSilver2;
	        
	        Quote_Support_OPN__c opnGold2 = new Quote_Support_OPN__c();
	        opnGold2.Product_OPN__c = 'erNBD';
	        opnGold2.Support_Level__c = 'Gold';
	        opnGold2.Quantity__c = 4;
	        opnGold2.Support_OPN__c = p.Id;
	        insert opnGold2;
	        
	        
	       
	        Asset2__c assetHca1 = new Asset2__c();
	        assetHca1.name = 'test name';
	        assetHca1.EOS_Date__c  = date.today().addyears(1);
	        assetHca1.Part_Number__c = 'MBX5020-1SFR';
	        assetHca1.Part_Description__c = 'BridgeX FCoIB/EoIB bridge system, 4 QSFP uplink ports and 16 SFP+ downlink ports with CPU, 1 Power S';
	        assetHca1.Asset_Family__c = 'HCA';
	        assetHca1.Serial_Number__c = '321321';
	        assetHca1.EOS_Date__c = date.today().addYears(4);
	        assetHca1.Renewed_Asset__c = true;
	        assetHca1.Contract2__c =  theContract2.Id;
	        
	        Asset2__c assetCables = new Asset2__c();
	        assetCables.name = 'test name';
	        assetCables.EOS_Date__c  = date.today().addyears(1);
	        assetCables.Part_Number__c = 'MBX5020-1SFR';
	        assetCables.Part_Description__c = 'BridgeX FCoIB/EoIB bridge system, 4 QSFP uplink ports and 16 SFP+ downlink ports with CPU, 1 Power S';
	        assetCables.Asset_Family__c = 'CABLES';
	        assetCables.Serial_Number__c = '321321';
	        assetCables.EOS_Date__c = date.today().addYears(4);
	        assetCables.Contract2__c =  theContract2.Id;
	        assetCables.Renewed_Asset__c = true;
	       
	        Asset2__c assetNoHcaNonCables1 = new Asset2__c();
	        assetNoHcaNonCables1.Contract2__c =  theContract2.Id;
	        assetNoHcaNonCables1.name = 'test name';
	        assetNoHcaNonCables1.Serial_Number__c = '321321';
	        assetNoHcaNonCables1.EOS_Date__c  = date.today().addyears(1);
	        assetNoHcaNonCables1.Part_Number__c = 'MBX5020-1SFR';
	        assetNoHcaNonCables1.Part_Description__c = 'BridgeX FCoIB/EoIB bridge system, 4 QSFP uplink ports and 16 SFP+ downlink ports with CPU, 1 Power S';
	        assetNoHcaNonCables1.Asset_Family__c = 'SWITCH4036';
	        assetNoHcaNonCables1.EOS_Date__c = date.today().addYears(4);
	        assetNoHcaNonCables1.Renewed_Asset__c = true;
	        
	        Asset2__c assetHca2 = new Asset2__c();
	        assetHca2.name = 'test name';
	        assetHca2.EOS_Date__c  = date.today().addyears(1);
	        assetHca2.Part_Number__c = 'erN4H';
	        assetHca2.Part_Description__c = 'BridgeX FCoIB/EoIB bridge system, 4 QSFP uplink ports and 16 SFP+ downlink ports with CPU, 1 Power S';
	        assetHca2.Asset_Family__c = 'HCA';
	        assetHca2.Serial_Number__c = '321321';
	        assetHca2.Contract2__c =  theContract2.Id;
	        assetHca2.EOS_Date__c = date.today().addYears(4);
	        assetHca2.Renewed_Asset__c = true;
	        
	        Asset2__c assetCables2 = new Asset2__c();
	        assetCables2.name = 'test name';
	        assetCables2.EOS_Date__c  = date.today().addyears(1);
	        assetCables2.Part_Number__c = 'erN4H';
	        assetCables2.Part_Description__c = 'BridgeX FCoIB/EoIB bridge system, 4 QSFP uplink ports and 16 SFP+ downlink ports with CPU, 1 Power S';
	        assetCables2.Asset_Family__c = 'CABLES';
	        assetCables2.Serial_Number__c = '321321';
	        assetCables2.Contract2__c =  theContract2.Id;
	        assetCables2.EOS_Date__c = date.today().addYears(4);
	        assetCables2.Renewed_Asset__c = true;
	       
	        Asset2__c assetNoHcaNonCables2 = new Asset2__c();
	        assetNoHcaNonCables2.Contract2__c =  theContract2.Id;
	        assetNoHcaNonCables2.name = 'test name';
	        assetNoHcaNonCables2.Serial_Number__c = '321321';
	        assetNoHcaNonCables2.EOS_Date__c  = date.today().addyears(1);
	        assetNoHcaNonCables2.Part_Number__c = 'erNBD';
	        assetNoHcaNonCables2.Part_Description__c = 'BridgeX FCoIB/EoIB bridge system, 4 QSFP uplink ports and 16 SFP+ downlink ports with CPU, 1 Power S';
	        assetNoHcaNonCables2.Asset_Family__c = 'SWITCH4036';
	        assetNoHcaNonCables2.EOS_Date__c = date.today().addYears(4);
	        assetNoHcaNonCables2.Renewed_Asset__c = true;
	        
	        insert assetNoHcaNonCables1;
	        insert  assetHca1;
	        
	        insert assetNoHcaNonCables2;
	        insert  assetHca2;
	        insert assetCables2; 
	        
	         Database.SaveResult  sr = Database.insert(assetCables, false);
	         
	        if(!sr.isSuccess())
	        {
	            list<Database.Error> error = sr.getErrors();
	            String errorMessage = error[0].getMessage();
	           system.debug('The operation could not be performed because of: '+ errorMessage);
	        }
	        
	        system.debug('assetHca1 ERI Approved : ' + assetHca1.ERI_Approve__c);
	        system.debug('assetNoHcaNonCables1 ERI Approved : ' + assetNoHcaNonCables1.ERI_Approve__c);       
	       
	        
	        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(theContract2);
	        Apexpages.currentPage().getParameters().put('Id',theContract2.Id);
	        VF_Renewal_Contract_Form contoller = new VF_Renewal_Contract_Form(teststandard);
	        contoller.chosenContractype = 'Bronze';
	        contoller.getCustomerCounties();
	        contoller.selectedCountry = 'Israel';
	        contoller.getNumSupportYears();
	        contoller.moveToSecondSection();
	        contoller.backToFirstSection();
	        contoller.chosenContractype = 'Silver Partner Assist Support';
	        contoller.getNumSupportYears();
	        contoller.checkAllCB();
	        contoller.next();
	        contoller.previousPage();
	        contoller.getprev();
	        contoller.continueToPrepareOppAndQuote();
	        contoller.getnxt();
	        contoller.payOnline();
	        contoller.getCustomerCounties();
	        
            system.runAs(communityUser) {
      			contoller.isCurrentUserNotPartner();
            }
            
            contoller.displayEmailAddressInput = true;
            contoller.displayOrHideEmailAdressInput();
	}
}