@isTest
private class Test_Loaners
 { 
   static testMethod void Test()  
   {  
	User User1 = [SELECT Id FROM User Where isActive = true and ManagerID != null and Manager.IsActive=true and ProfileID='00e50000000pdwL' limit 1]; 
  CLS_ObjectCreator obj = new CLS_ObjectCreator();    
  Loaner_Request__c LR1 = obj.createLoaner();  
  LR1.Status__c = 'Draft';
 
  Loaner_Request__c LR2 = obj.createLoaner();
  LR2.Status__c = 'Draft';
 
  Loaner_Request__c LR3 = obj.createLoaner();
  LR3.Status__c = 'Draft';
  
  LR1.Customer_Type__c = 'Distributor' ;
  LR2.Customer_Type__c = 'End User';
  LR2.region__c = 'USE';
 // LR3.Customer_Type__c = 'OEM';
  if (User1 != null) {
  	LR1.Sales_owner__c = User1.ID;
  	LR2.Sales_owner__c = User1.ID;
  	LR3.Sales_owner__c = User1.ID;
  }
  insert LR1;
  insert LR2;
		
		Product2 prod4_test1 = obj.createProduct();
        prod4_test1.Name ='98Y6352';
        prod4_test1.Type__c = 'HW';
        prod4_test1.Product_Type1__c = 'HCA';
        prod4_test1.ProductCode = 'FGTTFSS';
        prod4_test1.Product_Type1__c = 'CABLES';
        prod4_test1.Inventory_Item_id__c = '12121112';
        prod4_test1.partNumber__c = 'dhfgeryte';
        prod4_test1.OEM_PL__C=10000;
        insert prod4_test1;
        
        Product2 prodEMC_test2 = obj.createProduct();
        prodEMC_test2.Name ='071-000-588';
        prodEMC_test2.Type__c = 'HW';
        prodEMC_test2.Product_Type1__c = 'CABLES';
        prodEMC_test2.ProductCode = 'FGTTFSS22';
        prodEMC_test2.Product_Type1__c = 'CABLES';
        prodEMC_test2.Inventory_Item_id__c = '1212111212';
        prodEMC_test2.partNumber__c = 'dhfgeryte22';
        prodEMC_test2.OEM_PL__C=10000;
        insert prodEMC_test2;
        
        Product2 prod_LENOVO_test3 = obj.createProduct();
        prod_LENOVO_test3.Name ='VLT-30040';
        prod_LENOVO_test3.Type__c = 'HW';
        prod_LENOVO_test3.Product_Type1__c = 'SWITCH';
        prod_LENOVO_test3.ProductCode = 'FGTTFSS333';
        prod_LENOVO_test3.Product_Type1__c = 'CABLES';
        prod_LENOVO_test3.Inventory_Item_id__c = '12121112333';
        prod_LENOVO_test3.partNumber__c = 'dhfgeryte333';
        prod_LENOVO_test3.OEM_PL__C=10000;
        insert prod_LENOVO_test3;
        		
	Loaner_Product__c OPN_1 = new Loaner_Product__c(Loaner_Request__c=LR2.id,Product__c=prod4_test1.id,OPN_quantity__c=5); 
	Loaner_Product__c OPN_2 = new Loaner_Product__c(Loaner_Request__c=LR2.id,Product__c=prodEMC_test2.id,OPN_quantity__c=5);
	Loaner_Product__c OPN_3 = new Loaner_Product__c(Loaner_Request__c=LR2.id,Product__c=prod_LENOVO_test3.id,OPN_quantity__c=5);
	
	insert OPN_1;
	insert OPN_2;
	insert OPN_3;
	
	Loaner_Comment__c comm = new Loaner_Comment__c(Loaner__c=LR1.id,Comments__c='test 23',Send_Comments_To__c='Requester');
	insert comm;
	  
   }
 }