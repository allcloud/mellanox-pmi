// This batch class is scheduled to run and if the 'ERI_Approve1__c' is Approved it will set the 'ERI_Approved__c' to true
// otherwise it will set it false
global class Batch_Update_ERI_Approve implements Database.Batchable<Contract2__c>{
	
	global Iterable<Contract2__c> start(Database.BatchableContext BC) {
    
    	List<Contract2__c> contractsToBeUpdated_List = [SELECT Id, ERI_Approve1__c, ERI_Approved__c FROM Contract2__c
    	 									  ];
		return 	contractsToBeUpdated_List;							       
    }
     
    global void execute(Database.BatchableContext BC, List<Contract2__c> contractsToBeUpdated_List) {
     										       
		 if (!contractsToBeUpdated_List.isEmpty()) {
		 	
		 	for (Contract2__c currentContract : contractsToBeUpdated_List) {
		 		
		 		if (currentContract.ERI_Approve1__c != null && currentContract.ERI_Approve1__c.EqualsIgnoreCase('Approved')) {
		 			
		 			currentContract.ERI_Approved__c = true;
		 		}
		 		
		 		else {
		 			
		 			currentContract.ERI_Approved__c = false;
		 		}
		 	}
		 	
		 	Database.update(contractsToBeUpdated_List, false);
		 }									       
     }
     
     global void finish(Database.BatchableContext BC) {
     	   
     }
}