@isTest
public class test_VF_DealScoreRelatedListPerUser {
	
	static testMethod void test_theVF() {
		
		Deal_Flow__c flow = new Deal_Flow__c();
		flow.Name = 'test flow';
		
		insert flow;
		
		Deal_Round__c round = new Deal_Round__c();
		round.Deal_Flow__c = flow.Id;
		round.Name = 'test round';
		insert round;
		
		Deal_Score__c score =  new Deal_Score__c();
		score.Deal_Rounds__c = round.Id;
		score.Name = 'test Score';
		score.Rated_by__c = UserInfo.GetUserId();
		score.Score__c = 2;
		insert score;
		
		test.startTest();
		
		ApexPages.standardController thePage = new  ApexPages.standardController(round);
        VF_DealScoreRelatedListPerUser controller = new VF_DealScoreRelatedListPerUser(thePage);
        
        System.currentPageReference().getParameters().put('dealId', score.Id);
        
        controller.deleteDealScore();
        test.stopTest();
		
	}
}