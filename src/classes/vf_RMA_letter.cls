public class vf_RMA_letter 
{
        //has the title of the RMA Letter
    public String letterTitle         {get;set;}
    
    //has the ID of the current RMA
    public ID rmaId;
    
    public Boolean RejectedSNs  {get;set;}
    
    //a list of Serial numbers related to RMA
    public List<Serial_Number__c> lst_rmaSNs          {get;set;}
    
    //my current RMA used a list in order to loop in data with the currrent record and display in VF page
    public List<RMA__c> myRMA                         {get;set;}
    
    //
    public RMA__c anRMA                         {get;set;}
    
    //make a map of production to description, so can be used on the Serial number table of the RMA Letter
    public Map<String, String> map_prodNbrToDesc      {get;set;}
    
    //has the set of ids related to product
    public Set<String> set_prodIds                    {get;set;}
    
    //has the list of products related to the Serial Number of the given RMA
    public List<Product2> lst_Products                {get;set;}
        
        //The current date and time
    public DateTime nowDay                                {get;set;}
    
    //set the date and time as string
    public String stringDate                                {get;set;}
    
    // an inner class created to have all related information regarding the serial number of a given RMA
    public List<sn_info> lst_SNs          {get;set;}
    
    //this list uses to eliminate duplicates in the serial numbers and helps to display quantity of serial namber in the serial number table
    public List<sn_info> lst_SNs_Unique          {get;set;}
    
   //Rejected SNs
    public List<sn_info> lst_SNs_Rejected          {get;set;}
    
    //a new object added by Inna has the Mellanox site used for address
    public MellanoxSite__c MellanoxSite         {get;set;}
    
    //tmporary instance on an inner class that has all the serial number data of a given RMA
    public sn_info tmpSn;
    
    //maps product number to the same products has same prod nbr
    public map<String,List<sn_info>> map_PartNbrToListOfSN ;
     public map<String,List<sn_info>> map_PartNbrToListOfSNRejected ;
    //the following list used to add to the above map and the unique list
    public List<sn_info> lst_tmpSnInfo;
    public List<sn_info> lst_tmpSnInfoRejected;
    //my inner class that has all the required info regarding serial number of a given RMA
    //public List<string> lst_SnNumApp {get;set;}
    //public List<string> lst_SnNumRejected {get;set;}
    
    public class sn_info
    {
            
            //has the Serial Number ID    
            public String snID                      {get;set;}
            
            //has the description of the product
            public String snDescrip         {get;set;}
            
            //has the description of problem
            public String snProbemDescrip         {get;set;}
            
            
            //has the part number
            public String Part_Number       {get;set;}
            
            
            //has the part number
            public String Sup_Part_Number       {get;set;}
            
            //has the part number
            public String Is_Approved       {get;set;}
            
            public String Rejection_Reason       {get;set;}
            
            
            //will have the customer reference number 
            public String CustomerRefPartNubr       {get;set;}
            
            //refers to number of existing Serial numbers in same RMA
            public Integer Quantity         {get;set;}
            
            //refers to number of existing Serial numbers in same RMA
            public String snRMATypeFinal        {get;set;}
            
            // hold the serial
            public String Serial                      {get;set;}
            
            //constructor initialize the inner class vars
            public List<string> lst_SnNumApp {get;set;}
                    public List<string> lst_SnNumRejected {get;set;}
        public sn_info()
        {
                Quantity = 0;
                lst_SnNumApp = new list<string>();
                lst_SnNumRejected = new list<string>();
                //nothing to initialize
        }
        // a method that will set the description
        public void setSNDesc(String descrip)
        {
                snDescrip = descrip;
        }
        public void set_snProbemDescrip(String descrip)
        {
                snProbemDescrip = descrip;
        }
    }
    
    //list of objects of inner class
    public List<sn_info> lst_SNInf;
    
    //the controller constructor first to be called as we get into the VF_RM_Letter page
    public vf_RMA_letter (ApexPages.StandardController controller)
    {
        
        map_PartNbrToListOfSN = new map<String,List<sn_info>>();
        map_PartNbrToListOfSNRejected = new map<String,List<sn_info>>();
        //set the RMA Letter title
        letterTitle = 'RMA Request Acknowledgement';
                
        System.debug('I am the controller');
        
        //get the current date and time
        nowDay = system.now();
        
        //convert the returned date and time into string
        stringDate = String.valueOf(nowDay.date());
        
        //get the current RMA id
        //will be used to extract data from the RMA__c and the Serial_Number__c 
        rmaId = Apexpages.currentPage().getParameters().get('Id');
        
        //initialize my list that has the inner class
        lst_SNInf = new List<sn_info>();
        
        //initialize the list the will hold no duplicated Serial Numbers
        lst_SNs_Unique = new List<sn_info>(); //used to have uniqe only
        lst_SNs_Rejected = new List<sn_info>(); 
        
        //initialize my RMA so i can get data to it
        myRMA = New List<RMA__c>();
        
        //initialize the anRMA object
        anRMA = new RMA__c();
        
        //create a set that will have the products ids
        set_prodIds = New set<String>();
        
        //initialize the a nother list of Serial Numbers
        lst_SNs = new List<sn_info>();
        
        //initialize the MellanoxSite object
        MellanoxSite = New MellanoxSite__c();
        
        //debug
        System.debug('the ID: ' + rmaId);


                //if we have an id received by the controller
        if(rmaId != null)
        {
        
            //make a querey to retrive my required data
            myRMA = [Select r.Retrun_Euip_Msg__c, r.Mellanox_Site__c, r.Ship_Zip__c, r.Ship_Phone__c, r.Ship_Country__c, r.Ship_Contact__c, r.Ship_Company__c,r.RMA_Number__c, 
                                            r.Ship_Company_Address_2__c, r.Ship_Company_Address_1__c, r.Ship_City_State__c, r.PO_Number__c,
                                            r.ShipTo_same_as_BillTo__c, r.Name, r.Id, r.Fax__c, r.E_Mail__c, r.Date_Submitted__c,  Mellanox_Site__r.Name,
                                            r.Contact__c, r.Contact_TechOwner__c, r.Contact_TechOwner_Email__c, r.Contact_Phone__c, r.Bill_Zip__c, 
                                            r.Bill_Phone__c, r.Bill_Country__c, r.Bill_Contact__c, r.Bill_Company__c, r.Bill_Company_Address_2__c, 
                                            r.Bill_Company_Address_1__c, r.Bill_City_State__c , r.Failure_Analysis_Request__c, r.DOA_Count__c,r.diagnosed_by__c
                             From RMA__c r
                             WHERE id =: rmaId];
            
            //the RMA object will have the first RMA, as i have said the list of myRMA will have always one value
            //declared as a list for data display using repeat in VF_RMA_letter page                
            anRMA = myRMA[0];
            
            //get the related Serial numbers of a give RMA id
            lst_rmaSNs = New List<Serial_Number__c>([Select s.Serial__c,s.RMA_Type_final__c,s.Problem_Description__c ,s.Mellanox_PN__c,s.Approval__c, s.product_description__c,s.Reject_Reason__c, s.RMA__c, s.Id, s.Part_Number__c  
                                                                                             From Serial_Number__c s
                                                                                             where RMA__c =: rmaId ]); // and Approval__c = 'Approved']);
            
            //get the related mellanox site that is set in the current RMA 
            if(myRMA[0].Mellanox_Site__c != null)
            {                                                                                
                MellanoxSite = [Select m.Zip__c, m.SystemModstamp, m.Phone__c, m.OwnerId, m.Name, m.Id, m.Country__c, m.City__c, m.Address__c 
                                    From MellanoxSite__c m
                                    WHERE m.Id =: myRMA[0].Mellanox_Site__c];
                                    
            }
        }

        System.debug('myRMA.size(): ' + myRMA.size());
        System.debug('lst_rmaSNs.size: ' + lst_rmaSNs.size());
        System.debug('MellanoxSite.adress: ' + MellanoxSite.Address__c);

                //if there are serial numbers
    /*    if(lst_rmaSNs != null && lst_rmaSNs.size() >0)
        {
                //loop on the list in order to get the product ids
            for(Serial_Number__c sn :lst_rmaSNs)
            {
                    if(!set_prodIds.contains(sn.Part_Number__c))
                    {
                            set_prodIds.add(sn.Part_Number__c);
                    }
            }
        }*/
                
                //using the set above we extract the products from the system and the required fields
      //  lst_Products = New List<Product2> ([Select p.id, p.Name, p.Description From Product2 p where Name IN: set_prodIds]);
        
       // System.debug('lst_Products.size: ' + lst_Products.size());
        
        //initialize the map that will have a product number to Description
        map_prodNbrToDesc = New Map<String, String>();
                
                //initialize a temporary inner class that will be used to add to the list of the inner classes
        sn_info tmp_sn_info;


                //if there are serial numbers for the given RMA
        if(lst_rmaSNs!= null && lst_rmaSNs.size() > 0)
        {
                //loop on the serial numbers
            for(Serial_Number__c snbr:lst_rmaSNs)
            {
                 sn_info tmpSn                  = new sn_info();
                 tmpSn.snID                     = snbr.Id; 
                 tmpSn.Rejection_Reason = snbr.Reject_reason__c; 
                 tmpSn.Serial = snbr.Serial__c ;                  
                 if(snbr.Mellanox_PN__c != NULL) {tmpSn.Part_Number =  snbr.Mellanox_PN__c;}
                  else{ tmpSn.Part_Number   =  snbr.Part_Number__c;}
                 tmpSn.Sup_Part_Number =  snbr.Part_Number__c; 
                 tmpSn.snProbemDescrip  = snbr.Problem_Description__c;
                 tmpSn.Is_Approved = snbr.Approval__c;
                if(snbr.RMA_Type_final__c =='Item not found') {tmpSn.snRMATypeFinal = 'Factory Repair';} 
                 else           
                      { tmpSn.snRMATypeFinal = snbr.RMA_Type_final__c;} 
                 //Products from Asset
                 tmpSn.snDescrip = snbr.Product_Description__c;

                                          
                //if there are products
            /*    if(lst_Products!= null && lst_Products.size() > 0)
                {
                        
                        //loop on the products and get the description of the given Serial number
                     for(Product2 p2: lst_Products)
                    {
                            if(p2.Name == snbr.Part_Number__c )
                            {
                                    //initialize a new instance of the inner class and assign required data to it
                                    //sn_info tmpSn = new sn_info();
                                    //tmpSn.snID = snbr.Id;
                                    tmpSn.snDescrip = p2.Description;
                                    System.debug('tmpSn.snDescrip: ' + tmpSn.snDescrip);
                                    //tmpSn.Part_Number =  snbr.Part_Number__c;
                                    //tmpSn.snProbemDescrip = snbr.Problem_Description__c;
                                    tmpSn.Quantity = 1;
                                    system.debug('set quality by one when create a serial number');
                                    //add the new inner class instance to the list
                                    //lst_SNs.add(tmpSn);
                            }
                    }
                }*/
                 lst_SNs.add(tmpSn);
            }   
        }
        
        if(lst_SNs != null && lst_SNs.size() > 0)
        {
                //if the first list of the inner class has data
                for(sn_info anInf: lst_SNs)
                {
                        //fill the product nbr map to same products
                        
                     if( anInf.Is_Approved == 'Approved')  
                     {
                        if(map_PartNbrToListOfSN.containskey(anInf.Part_Number))
                        {
                                map_PartNbrToListOfSN.get(anInf.Part_Number).add(anInf);
                        }
                        else
                        {
                                lst_tmpSnInfo = new List<sn_info>();
                                lst_tmpSnInfo.add(anInf);
                                map_PartNbrToListOfSN.put(anInf.Part_Number,lst_tmpSnInfo);
                        }  
                      }  
                      //Rejected SNs 
                      
                     
                    if( anInf.Is_Approved == 'Not Approved')
                     { 
                      if(map_PartNbrToListOfSNREjected.containskey(anInf.Part_Number))
                        {
                                map_PartNbrToListOfSNRejected.get(anInf.Part_Number).add(anInf);
                        }
                        else
                        {
                                lst_tmpSnInfo = new List<sn_info>();
                                lst_tmpSnInfo.add(anInf);
                                map_PartNbrToListOfSNREjected.put(anInf.Part_Number,lst_tmpSnInfo);
                        }  
                    } //if(not approved)    
                        
                                                     
                }
        }
        
        //verify that there are values in the map
        if(map_PartNbrToListOfSN != null && map_PartNbrToListOfSN.size() > 0)
        {
                // build the unique list of products and get the quantity
                for(String prodNbr :map_PartNbrToListOfSN.keyset())
                {
                        lst_tmpSnInfo =  new List<sn_info>();
                        lst_tmpSnInfo =  map_PartNbrToListOfSN.get(prodNbr);
                        for(sn_info sn:map_PartNbrToListOfSN.get(prodNbr))
                        {
                                lst_tmpSnInfo[0].lst_SnNumApp.add(sn.Serial);
                        }
                        lst_tmpSnInfo[0].Quantity = lst_tmpSnInfo.size();
                        lst_SNs_Unique.add(lst_tmpSnInfo[0]);
                }
                
                
                for(String prodNbr :map_PartNbrToListOfSNRejected.keyset())
                {
                        lst_tmpSnInfo =  new List<sn_info>();
                        lst_tmpSnInfo =  map_PartNbrToListOfSNRejected.get(prodNbr);
                        for(sn_info sn:map_PartNbrToListOfSNRejected.get(prodNbr))
                        {
                                lst_tmpSnInfo[0].lst_SnNumRejected.add(sn.Serial);
                        }
                        lst_tmpSnInfo[0].Quantity = lst_tmpSnInfo.size();
                        lst_SNs_Rejected.add(lst_tmpSnInfo[0]); 
                }
         if(lst_SNs_Rejected.size()>0) 
         { 
            RejectedSNs = true;
              
         }                                                                       
         else  RejectedSNs = false;                                     
        }
        system.debug(' New lst_SNs_Unique size() ' + lst_SNs_Unique.size());
         system.debug(' New lst_SNs_Unique ' + lst_SNs_Unique);
        
        //get the map product number with prod description
        if(lst_Products != null && lst_Products.size() > 0)
        {
                //loop on the produt list
                for(Product2 p : lst_Products)
                {
                        System.debug('p.Name : ' + p.Name + '     p.Description: '+ p.Description);
                        //create new instance of the serial number info
                        tmp_sn_info = new sn_info();
                        tmp_sn_info.snID = p.Name;
                        tmp_sn_info.snDescrip =  p.Description;
                        
                        //add the product and its related description
                        map_prodNbrToDesc.put(p.Name , p.Description);
                }
        }
        
        System.debug('map_prodNbrToDesc.size(): ' + map_prodNbrToDesc.size());
        
       
        
    }
    
    // add the test in here
  /*  
   static testMethod void Test_VF_RMA_Letter() 
   {
                //list of serial numbers to be related to the RMA and to avoid many inserts
                List<Serial_Number__c> lst_snOfRMA = new List<Serial_Number__c>();
                
                List<Product2> lst_prods = New List<Product2>();
                //instance of object creator
                CLS_ObjectCreator obj = new CLS_ObjectCreator();
                
                MellanoxSite__c  site  = obj.createMellanoxSite();
                insert site;
                 
                RMA__c rma = obj.CreateRMA();
                rma.Mellanox_Site__c = site.id;
                insert rma;
                
        
                Product2 p1 =  obj.createProduct();
                p1.Inventory_Item_id__c = '90989';
                lst_prods.add(p1);
                
                Product2 p2 =  obj.createProduct();
                p2.Name = 'Test Product 2';
                p2.Inventory_Item_id__c = '90980';
                lst_prods.add(p2);
                
                Serial_Number__c sn1 = obj.createSerialNumber(rma,p1);
                lst_snOfRMA.add(sn1);
                
                Serial_Number__c sn2 = obj.createSerialNumber(rma,p2);
                lst_snOfRMA.add(sn2);
                
                insert lst_prods;
                insert lst_snOfRMA; 
                
                Test.setCurrentPageReference(new PageReference('Page.vf_RMA_letter')); 
                System.currentPageReference().getParameters().put('id',rma.Id );            
                Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rma);
                vf_RMA_letter vf = new vf_RMA_letter(teststandard);
                
     }*/

}