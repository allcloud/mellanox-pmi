@isTest
private class Test_vf_TrainingRequest {

    static testMethod void myUnitTest() 
    { 
     
        string email = string.valueOf(system.currentTimeMillis()) + '@test.com';        
         
        Account Ac = new Account(name = 'testAcc1');
        Insert Ac;
             
        Contact c = new Contact(LastName='testos');
        //c.AccountId=ENV.AccountId;     
        c.AccountId=Ac.Id;                                                          
        c.Email= email;
        insert c;
        
        ctl_vf_TrainingRequest tr = new ctl_vf_TrainingRequest();
        
        tr.reset();
        
        tr.newTR.SuppliedName__c='test';
        tr.newTR.Supplied_Company__c='test';
        tr.newTR.Contact_Phone__c='43543534534';
        tr.newTR.Contact_Email__c='test@test.com';
        
        tr.submit();
    }
    static testMethod void myUnitTest2()
    {
        ctl_vf_TrainingRequest tr = new ctl_vf_TrainingRequest();
        tr.newTR.SuppliedName__c='test';
        tr.newTR.Supplied_Company__c='test';
        tr.newTR.Contact_Phone__c='43543534534';
        tr.newTR.Contact_Email__c='test2@test2.com';
        
        
        tr.submit();
        tr.cancel();
    }
}