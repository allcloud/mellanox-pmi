/*
** created date : 17/12/13
** created by   : Rima Rohana
** called  by   : Trigger  cls_Contact_Trigger 
** Added Methods : Elad Kaplan 12/03/14
*/
global class cls_Contact_Trigger 
{
  
  public static final String TYPE_SYSTEM = 'Mellanox System Customer';
    @future
    public static void ManageGroupMember(set<Id> set_contactId ,map<Id,string> map_ContactIdToNewGroupId,map<Id,string> map_ContactIdToOldGroupId)
    {
    	map<Id,User>      map_User ;
    	map<string,Id>    map_GroupToUser           = new map<string,Id>();
    	map<Id,string>    map_UserToGroupInsert     = new map<Id,string>();
    	map<Id,string>    map_UserToGroupDelete     = new map<Id,string>();
    	map<string,Group> map_UserGroup             = new map<string,Group>() ;
    	list<GroupMember> lst_GroupMemberTodelete     = new list<GroupMember>() ;
		list<GroupMember> lst_GroupMemberToInsert     = new list<GroupMember>() ;
		set<string> set_CRM_Content_Permissions_Names = new set<string>();
		
		//get the contact available groups 
    	Schema.DescribeFieldResult fieldResult = Contact.CRM_Content_Permissions__c.getDescribe();
    	List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    	
    	for(Schema.PicklistEntry f : ple)
	    {
	   		set_CRM_Content_Permissions_Names.add(f.getValue());
	    }
	    
	    //get the groups Ids
	    for(Group gr : [select id, name,(Select Id, GroupId, UserOrGroupId From GroupMembers) from Group where name IN: set_CRM_Content_Permissions_Names])
		{
			map_UserGroup.put(gr.name, gr);
		}
		system.debug('==>map_UserGroup: '+map_UserGroup);
		
		//get the contacts' users 
		map_User = new map<Id,User>([Select Id, ContactId From User where ContactId IN: set_contactId]);
		
		for(User u:map_User.values())
		{
			if(map_ContactIdToNewGroupId.get(u.ContactId) != null)
			{
				map_UserToGroupInsert.put(u.Id,map_ContactIdToNewGroupId.get(u.ContactId));
			}
			if(map_ContactIdToOldGroupId.get(u.ContactId) != null)
			{
				map_UserToGroupDelete.put(u.Id,map_ContactIdToOldGroupId.get(u.ContactId));
			}
		}
		system.debug('==>map_UserToGroupDelete '+map_UserToGroupDelete);
		
		//get all the geoup member that should be deleted 
		string str_query;
		if(!map_UserToGroupDelete.IsEmpty())
		{
			str_query = 'Select UserOrGroupId, Id, GroupId, Group.Name From GroupMember where';
		
			for(Id duId: map_UserToGroupDelete.keyset())
			{
				str_query += ' (UserOrGroupId = \'' + duId + '\' and Group.Name = \''+map_UserToGroupDelete.get(duId)+'\') or';
			}
			str_query = str_query.substring(0, str_query.length()-2);
			system.debug('==>str_query: '+str_query);
			system.debug('==> : '+Database.query(str_query));
			
			for(GroupMember gm: Database.query(str_query))
			{
				system.debug('==>gm.Group.name: '+gm.Group.name);
				lst_GroupMemberTodelete.add(new GroupMember(Id=gm.Id) );
			}
		}
		for(Id uId:map_UserToGroupInsert.keyset())
		{
			lst_GroupMemberToInsert.add(new GroupMember(UserOrGroupId = uId, GroupId = map_UserGroup.get( map_UserToGroupInsert.get(uId)).Id ) );
		}
		
		//update the members group
		if(!lst_GroupMemberTodelete.IsEmpty())
		{
			delete lst_GroupMemberTodelete;
			system.debug('==>lst_GroupMemberTodelete: '+lst_GroupMemberTodelete);
		}
		if(!lst_GroupMemberToInsert.IsEmpty())
		{
			insert lst_GroupMemberToInsert;
			system.debug('==>lst_GroupMemberToInsert: '+lst_GroupMemberToInsert);
		}
    }
    
    /*
    public void assignAccountToContactByDomain(list<Contact> newContacts_List) {
    	
    	String currentUserName = system.UserInfo.getUserName();
    	
    	Account defaultAccount;
    	try {	
    		
    		 defaultAccount = [SELECT Id FROM Account WHERE Name =: 'UNKNOWN COMMUNITY'];
    	}
    	
    	catch (Exception e) {
    		
    		system.debug('defaultAccount was not found');
    	}
    	
    	if (currentUserName.EqualsIgnoreCase('tatianase@mellanox.com.test')) {
    		
    		boolean AccountFoundOrEmailIsGmail = false;
    		
    		for (Contact con : newContacts_List) {
    			    			
    			if(defaultAccount != null && con.Email.ContainsIgnoreCase('gmail')) {
    				
    				con.AccountId = defaultAccount.Id;
    			}
    			
	    		else {	
	    			
	    			Account theAccount = new Account();
	    			theAccount = getAccountByEmailDomain(con.Email, null);
	    			
	    			if (theAccount.Id != null) {
	    				
	    				con.AccountId = theAccount.Id;
	    			}
	    			
	    			else {
	    				
	    				con.AccountId = defaultAccount.Id;
	    			}
	    		}
    		}
    	}
    }
    
    */
    
    // This method will get the existing Account if founded, where one of its fields (domain__c, Domain1__c or Domain2__c) values
	// matches the domain name as entered in the request form
	public Account getAccountByEmailDomain(String email, String contactTypeStr)
	{
		Account accountFoundByEmail;
		if (email != null && email != '')
		{
			
			String domainName = ENV.getDomainFromEmail(email);
			 
			Id parentAccountRecordTypeId = ENV.GetRecordTypeIdByName('Parent Account');
			list<Account> acc_List = new list<Account>();
			accountFoundByEmail = new Account();
		   
		    String soql = 'Select RecordTypeId, Name, Id, Customer_Type__c, Domain2__c, Actual_Trial_Contracts__c, Maximum_Permitted_Trial_Contracts__c From Account' +
			             ' WHERE RecordTypeId = \'' + parentAccountRecordTypeId  +'\'' +  ' and RW_Domain_multi__c includes '+  '(' + '\'' + domainName  + '\'' + ')';
			             
		   
		   if (contactTypeStr != null && !contactTypeStr.equalsIgnoreCase(TYPE_SYSTEM)) {
		   	
		   		soql += ' and Technical_Owner__c != null';
		   }
		   
		   soql += ' order by Highest_Active_contract_rank__c desc NULLS LAST';
        	
           system.debug('soql : ' + soql);	
        	
		    acc_List = database.query(soql);
			 
				
			if (!acc_List.isEmpty()) {
					
				for(Integer i = 0; i < acc_List.size(); i++) {
					
					if (acc_List[i].Name.EqualsIgnoreCase('IBM - XIV')) {
						
						acc_List.remove(i);
						break;
					}
				}	
					
				accountFoundByEmail = acc_List[0];
			}
		}
		
		return accountFoundByEmail;
	}
	
	// This method will get the existing Account if founded, where one of its fields (domain__c, Domain1__c or Domain2__c) values
	// matches the domain name as entered in the request form
	public list<Account> getAccountByEmailDomainAndCountry(String email, String theBillingCountry, Id theAccountId) {
	    
	    list<Account> acc_List = new list<Account>();
		Account accountFoundByEmail;
		if (email != null && email != '') {
 			
			String domainName = ENV.getDomainFromEmail(email);
			 
			Id parentAccountRecordTypeId = ENV.GetRecordTypeIdByName('Parent Account');
			 
			accountFoundByEmail = new Account();
		   
		   acc_List = [select Id, Name, BillingCountry, Partner_Type__c, Customer_Type__c, BillingCity, BillingState, BillingStreet, RW_Domain__c, Actual_Trial_Contracts__c, Maximum_Permitted_Trial_Contracts__c, RW_Domain1__c FROM Account
 		   WHERE RW_Domain_multi__c != null and RecordTypeId =: parentAccountRecordTypeId and BillingCountry =: theBillingCountry];
		   
		}
		
		return acc_List;
	}
	
	
	
	
	public void UpdateAccountDomainFromContact(list<Contact> newContacts_List, map<Id, Contact> oldContacts_Map) {
		
 		list<Account> DistributorAccounts_List = [SELECT Id, RW_Domain__c, RW_Domain1__c FROM Account WHERE Is_Distributor__c = true];
		set<Id> AccountsIds_Set = populateDomainsSet(newContacts_List, oldContacts_Map);
		list<Account> accounts2Update_List = new list<Account>();
		list<Contact> contacts2Update_List = new list<Contact>();
		
		if (!AccountsIds_Set.isEmpty()) {
			
			map<Id , Account> accountsIds2Accounts_Map = new map<Id, Account>([SELECT Id, Is_Distributor__c, RW_Domain__c, RW_Domain1__c FROM Account WHERE Id IN : AccountsIds_Set]);
			
			for (Contact newContact : newContacts_List) {
				
				Account relatedAccount = accountsIds2Accounts_Map.get(newContact.AccountId);
				
				if (relatedAccount != null && !relatedAccount.Is_Distributor__c) {
					
					boolean foundDomain = false;
					for (Account acc : DistributorAccounts_List) {
						
						if (acc.RW_Domain__c != null && acc.RW_Domain__c.containsIgnoreCase(newContact.domain__c)) {
							
							foundDomain = true;
							break;
						}
						
						else if (acc.RW_Domain1__c != null && acc.RW_Domain1__c.containsIgnoreCase(newContact.domain__c)) {
							
							foundDomain = true;
							break;
						}
						
						if (!foundDomain) {
							
							contacts2Update_List.add(newContact);
						}
					}
				}
			}
			
			
			if (!accountsIds2Accounts_Map.values().isEmpty()) {
				
				for (Contact newContact : contacts2Update_List) {
					
					Account relatedAccount = accountsIds2Accounts_Map.get(newContact.AccountId);
					
					if (relatedAccount.RW_Domain__c != null) {
						
						if (!relatedAccount.RW_Domain__c.contains(newContact.domain__c)) {
							
							if (relatedAccount.RW_Domain1__c != null) {
								
								if (!relatedAccount.RW_Domain1__c.contains(newContact.domain__c)) {
									
									String aggragatedDomains_Str = relatedAccount.RW_Domain1__c + ',' + newContact.domain__c;
									relatedAccount.RW_Domain1__c = aggragatedDomains_Str;
									accounts2Update_List.add(relatedAccount);
								}
							}
							
							else {
								
								String aggragatedDomains_Str = relatedAccount.RW_Domain__c + ',' + newContact.domain__c;
							
								if (aggragatedDomains_Str.length() <= 254) {
									
									 relatedAccount.RW_Domain__c = aggragatedDomains_Str;
									 accounts2Update_List.add(relatedAccount);
								}
								
								else {
									
 									relatedAccount.RW_Domain1__c = newContact.domain__c;
									accounts2Update_List.add(relatedAccount);
								} 
							}
						}
					}
					
					else {
						
						String aggragatedDomains_Str =  newContact.domain__c;
						relatedAccount.RW_Domain__c = aggragatedDomains_Str;
						accounts2Update_List.add(relatedAccount);
					}					
				}
			}
		}
		
		if (!accounts2Update_List.isEmpty()) {
			
			try {	
				update accounts2Update_List;
			}
			
			catch(Exception e) {
				
				system.debug('Error 251 : ' + e.getMessage());
			}
		}
	}
	
	  
	private set<Id> populateDomainsSet(list<Contact> newContacts_List, map<Id, Contact> oldContacts_Map) {
		
		set<Id> AccountsIds_Set = new set<Id>();
		Public_Email_Domains__c emailDomains_Settings = Public_Email_Domains__c.getInstance('Global Emails');
		String publicDomains =  emailDomains_Settings.Value__c;
		 
		
		for (Contact newContact : newContacts_List) {
			
			if (oldContacts_Map != null) {
				
				if (newContact.AccountId != oldContacts_Map.get(newContact.Id).AccountId && newContact.domain__c != null) {
					
					if (!publicDomains.containsignorecase(newContact.domain__c) && !newContact.domain__c.containsignorecase('Mellanox')) {
						
						AccountsIds_Set.add(newContact.AccountId);
					}
				}
			}
			
			else {
				
				if (publicDomains != null && newContact.domain__c != null && !publicDomains.containsignorecase(newContact.domain__c) && !newContact.domain__c.containsignorecase('Mellanox')) {
					
					AccountsIds_Set.add(newContact.AccountId);
				}
			}
		}
		return AccountsIds_Set;
	}
	
	// This method will assign COntacts created/updated from PRM to the relevant Account
	public void updateContactAccountsFromPRM(list<Contact> newContacts_List, map<Id, Contact> oldContacts_Map) {
		
		map<String, set<Id>> accountTypes2AccountIds_Map = fillUpAccountsMap(newContacts_List, oldContacts_Map);
		
		list<Contact> contacts2Update_List = getContacts2UpdateList(newContacts_List, oldContacts_Map);
		list<Account> accounts2Delete_List = new list<Account>();
		list<Account> accounts2Update_List = new list<Account>();
		 
		map<Id, Account> mainAccountsIds2Accounts_Map = new map<Id, Account>([SELECT Id, Name, Partner_Type__c, Customer_Type__c, BillingCity, BillingState, BillingStreet, BillingCountry, General_Account__c FROM Account WHERE Id IN : accountTypes2AccountIds_Map.get('Main Account')]);
		map<Id, Account> partnerAccountsIds2Accounts_Map = new map<Id, Account>([SELECT Id, Name, Partner_Type__c, Customer_Type__c, BillingCity, BillingState, BillingStreet, BillingCountry, General_Account__c FROM Account WHERE Id IN : accountTypes2AccountIds_Map.get('Partner Account')]);
		map<String, String> countries2Regions_Map = getCountries2RegionsMap(partnerAccountsIds2Accounts_Map);
		
		for (Contact newContact : contacts2Update_List) { 
			
			Account MainAccount = mainAccountsIds2Accounts_Map.get(newContact.AccountId);
			Account PartnerAccount = partnerAccountsIds2Accounts_Map.get(newContact.Partner_Account__c);
				
			if (mainAccountsIds2Accounts_Map != null && mainAccountsIds2Accounts_Map.containsKey(newContact.AccountId)) {
				
				if (mainAccountsIds2Accounts_Map.get(newContact.AccountId).General_Account__c 
				|| (mainAccountsIds2Accounts_Map.get(newContact.AccountId).BillingCountry != partnerAccountsIds2Accounts_Map.get(newContact.Partner_Account__c).BillingCountry) ) {
					
					newContact.AccountId = newContact.Partner_Account__c;
					newContact.Partner_Account__c = null;
					setUpAccountSupportType(PartnerAccount, PartnerAccount);
					accounts2Update_List.add(PartnerAccount);
	 			}
			}
			
			else {
				
				if (PartnerAccount != null && MainAccount != null) { 
					
					MainAccount.BillingStreet = (PartnerAccount.BillingStreet != null)  ? PartnerAccount.BillingStreet : null;
					MainAccount.BillingCity = PartnerAccount.BillingCity;
					MainAccount.BillingState = PartnerAccount.BillingState;
					MainAccount.BillingCountry = PartnerAccount.BillingCountry;
					MainAccount.Support_Region__c = countries2Regions_Map.get(PartnerAccount.BillingCountry);
					newContact.Partner_Account__c = null;
					setUpAccountSupportType(MainAccount, PartnerAccount);
					MainAccount.Type = PartnerAccount.Partner_Type__c;
					accounts2Delete_List.add(PartnerAccount);
					accounts2Update_List.add(MainAccount);
				}
			}
		}
		
		if (!accounts2Update_List.isEmpty()) {
			
			update accounts2Update_List;
		}
		
		if (!accounts2Delete_List.isEmpty()) {
			
			delete accounts2Delete_List;
		}
	}
	
	// This method updates the main Account Customer Type   
	private Account setUpAccountSupportType(Account mainAcc, Account seconderyAcc) {
		
		if (seconderyAcc.Partner_Type__c !=  null) {
					
			system.debug('seconderyAcc.Partner_Type__c : ' + seconderyAcc.Partner_Type__c);		
			if (seconderyAcc.Partner_Type__c.EqualsIgnoreCase('Distributor')) {
				
				mainAcc.Customer_Type__c = 'Business Partner';
			}
			
			else if (seconderyAcc.Partner_Type__c.EqualsIgnoreCase('Reseller')) {
				
				mainAcc.Customer_Type__c = 'Reseller';
			}
		}
		return mainAcc;
	}
	
	// This method fill up the accounts map
	private map<String, set<Id>> fillUpAccountsMap(list<Contact> newContacts_List, map<Id, Contact> oldContacts_Map) {
		
		map<String, set<Id>> accountTypes2AccountIds_Map = new map<String, set<Id>>();
		 
		for (Contact newContact : newContacts_List) {
			
			if (newContact.Partner_Account__c != null && oldContacts_Map.get(newContact.Id).Partner_Account__c == null) {
				
				if (accountTypes2AccountIds_Map.containsKey('Partner Account')) {
					
					accountTypes2AccountIds_Map.get('Partner Account').add(newContact.Partner_Account__c);
				}
				
				else {
					
					set<Id> partnerAccountIds_Set = new set<Id>{newContact.Partner_Account__c};
					accountTypes2AccountIds_Map.put('Partner Account', partnerAccountIds_Set);
				}
				
				if (accountTypes2AccountIds_Map.containsKey('Main Account')) {
					
					accountTypes2AccountIds_Map.get('Main Account').add(newContact.AccountId);
				}
				
				else {
					
					set<Id> accountIds_Set = new set<Id>{newContact.AccountId};
					accountTypes2AccountIds_Map.put('Main Account', accountIds_Set);
				} 
			}
		}
		
		return accountTypes2AccountIds_Map;
	}
	
	// This method returns a list of contacts that need to be updated
	private list<Contact> getContacts2UpdateList(list<Contact> newContacts_List, map<Id, Contact> oldContacts_Map) {
		
		list<Contact> contacts2Update_List = new list<Contact>();
		for (Contact newContact : newContacts_List) {
			
			if (newContact.Partner_Account__c != null && oldContacts_Map.get(newContact.Id).Partner_Account__c == null) {
				
				contacts2Update_List.add(newContact);
			}
		}
		
		return contacts2Update_List;
	}
	
	// This method returns a map of regions country names as key and regions as values
	private map<String, String> getCountries2RegionsMap(map<Id, Account> partnerAccountsIds2Accounts_Map) {
		
		map<String, String> countries2Regions_Map = new map<String, String>();
		set<String> countries_Set = new set<String>();
		
		
		for (Account acc : partnerAccountsIds2Accounts_Map.values()) {
			
			countries_Set.add(acc.BillingCountry);
		}
		
		if (!countries_Set.isEmpty()) {	
		
			list<Regions__c> regions_List = [SELECT Id, Region__c, Name FROM Regions__c WHERE Name IN : countries_Set];
			
			for (Regions__c region : regions_List) {
				
				countries2Regions_Map.put(region.Name, region.Region__c);
			}
		}
		
		return countries2Regions_Map;
	}
	
	
	public void updateContactAccountsFromPRMOnContactCreate(list<Contact> newContacts_List) {
		
		set<Id> accountIds_Set = new set<Id>();
		
		for (Contact newContact : newContacts_List) {
			
			system.debug('newContact.Account_Partner_Type__c : ' + newContact.Account_Partner_Type__c);
			if (newContact.Account_Partner_Type__c != null && newContact.Type__c != null && newContact.Type__c.EqualsIgnoreCase('Partner')) {
				
				accountIds_Set.add(newContact.AccountId);
			}
		}
		
		map<Id, Account> mainAccountsIds2Accounts_Map = new map<Id, Account>([SELECT Id, Name, Partner_Type__c, Customer_Type__c, BillingCity, BillingCountryCode, BillingState, BillingStreet, BillingCountry, General_Account__c FROM Account WHERE Id IN : accountIds_Set]);
		map<String, list<Account>> domains2AccountsList_Map = getMapOfDomainsToAccountsList(newContacts_List);
		map<String, String> countries2Regions_Map = getCountries2RegionsMap(mainAccountsIds2Accounts_Map);
		system.debug('domains2AccountsList_Map : ' + domains2AccountsList_Map);
		 
		list<Account> accounts2Update_List = new list<Account>();
		list<Account> accounts2Delete_List = new list<Account>();
		list<Contact> contacts2Update_List = new list<Contact>(); 
		
		for (Contact newContact : newContacts_List) {
			
			if (newContact.Account_Partner_Type__c != null && newContact.Type__c != null && newContact.Type__c.EqualsIgnoreCase('Partner')) {
				
				String theDomain = ENV.getDomainFromEmail(newContact.email);
				Account PartnerAccount = mainAccountsIds2Accounts_Map.get(newContact.AccountId);
				
				if (domains2AccountsList_Map.containsKey(newContact.Email)) {
					
					Account MainAccount;
					for (Account acc : domains2AccountsList_Map.get(newContact.Email)) {
						
						system.debug('acc.BillingCountry : ' + acc.BillingCountry);
						system.debug('PartnerAccount.BillingCountry : ' + PartnerAccount.BillingCountry);
						system.debug('acc.BillingCountry : ' + acc.BillingCountry);
						
						if (acc.BillingCountry == PartnerAccount.BillingCountry && acc.Id != PartnerAccount.Id &&
						( (acc.RW_Domain__c != null && acc.RW_Domain__c.containsIgnoreCase(theDomain)))) {
							
							MainAccount = acc;
							break;
						}
					}	
						
					if (MainAccount != null && MainAccount.Id != null) {
						
						system.debug('MainAccount : ' + MainAccount);
						system.debug('PartnerAccount : ' + PartnerAccount);
						MainAccount.BillingStreet = PartnerAccount.BillingStreet;
						MainAccount.BillingCity = PartnerAccount.BillingCity;
						MainAccount.BillingState = PartnerAccount.BillingState;
						MainAccount.BillingCountry = PartnerAccount.BillingCountry;
						MainAccount.Support_Region__c = countries2Regions_Map.get(PartnerAccount.BillingCountry);
						setUpAccountSupportType(MainAccount, PartnerAccount);
						Contact con = new Contact(Id = newContact.Id);
						con.AccountId = MainAccount.Id;
						contacts2Update_List.add(con);
						accounts2Update_List.add(MainAccount);
						accounts2Delete_List.add(PartnerAccount);
					}
				}
			}
		}
		
		if (!contacts2Update_List.isEmpty()) {
			
			update contacts2Update_List;
		}
		
		if (!accounts2Update_List.isEmpty()) {
			
			update accounts2Update_List;
		}
		
		if (!accounts2Delete_List.isEmpty()) {
			
			delete accounts2Delete_List;
		}
	} 
	
	
	
	// This method returns a map of email domains as keys and lists of Accounts as values
	private map<String, list<Account>> getMapOfDomainsToAccountsList(list<Contact> newContacts_List) {
		
	
		map<String, list<Account>> emails2AccountsList_Map = new map<String, list<Account>>();
		
		for (Contact newContact : newContacts_List) {
			
			if (newContact.Account_Partner_Type__c != null && newContact.Type__c != null && newContact.Type__c.EqualsIgnoreCase('Partner')) {
				
				list<Account> acc_List = getAccountByEmailDomainAndCountry(newContact.Email, newContact.Account_Billing_Country__c, newContact.AccountId);
				system.debug('acc_List : '  + acc_List);
				
					if (emails2AccountsList_Map.containsKey(newContact.Email)) {
						
						emails2AccountsList_Map.get(newContact.Email).addAll(acc_List);
					}
					
					else {
						
						emails2AccountsList_Map.put(newContact.Email, acc_List);
					}
			}
		}
		
		return emails2AccountsList_Map;
	}
}