global class cls_WebService {
	
	
	static webservice String activateUser(String contactId) {
			
		User relatedUser;	
		try {
			
			 relatedUser = [SELECT Id, isActive FROM User WHERE ContactId =: contactId];
		}
		
		catch(Exception e) {
			
			return 'No User';
		}
		
		if (relatedUser.Id == null) {
			
			return 'No User';
		}
		
		if (relatedUser.isActive) {
			
			return 'User Active';
		}
		
		else {
			
			relatedUser.isActive = true;
			update relatedUser;
		}
		return '';
	}
	
	static webservice String deActivateUser(String contactId) {
			
		User relatedUser;	
		try {
			
			 relatedUser = [SELECT Id, isActive FROM User WHERE ContactId =: contactId];
		}
		
		catch(Exception e) {
			
			return 'No User';
		}
		
		if (relatedUser.Id == null) {
			
			return 'No User';
		}
		
		if (!relatedUser.isActive) {
			
			return 'User Not Active';
		}
		
		else {
			
			relatedUser.isActive = false;
			update relatedUser;
		}
		return '';
	}
	
	

}