public class vf_oppLineItemSchedule {

    public List<String> seperatedURL = new List<String>();
    public List<ID> oppLineItemIds = new List<ID>();
    public List<OpportunityLineItemSchedule> oList ;
    public OpportunityLineItem oppLineItem;
    public List<wrapper> productSchedule  {get; set;}
    public String oppLineItemId;
    
    public class wrapper 
    {
    	public OpportunityLineItemSchedule lineitem {get; set;}
    	public Double price {get; set;}
    	public String quarter {get; set;}
    	
    	public wrapper (OpportunityLineItemSchedule o)
    	{
    		lineitem = o;
    		if (o.Quantity != 0)
    		{
    			price = o.Revenue/o.Quantity;
    		}
    		else
    		{
    			price = 0;
    		}
    		convertToQuarters();
    	}
    	public void convertToQuarters ()
    	{
    		String tmpYear = String.valueOf(lineitem.ScheduleDate.year());
    		Integer tmpMonth = Integer.valueOf(lineitem.ScheduleDate.month());
    		
    		if (tmpMonth < 4)
    			quarter = 'Q1 - '+tmpYear;
    		else
	    		if (tmpMonth < 7)
	    			quarter = 'Q2 - '+tmpYear;
	    		else
	    			if (tmpMonth < 10)
	    				quarter = 'Q3 - '+tmpYear;
	    			else
	    				quarter = 'Q4 - '+tmpYear;
    	}
    }
    
    public vf_oppLineItemSchedule (ApexPages.StandardController controller)
    {	
    	productSchedule = new List<wrapper>();
	    
	     oppLineItem = [Select  o.Id, (Select OpportunityLineItemId, Id, Revenue, Quantity, ScheduleDate, Description  
 										From OpportunityLineItemSchedules 
 										order by ScheduleDate asc) 
                        From OpportunityLineItem o 
                        where o.Id = : controller.getId()
                        limit 1];
	    
	    for (OpportunityLineItemSchedule o : oppLineItem.OpportunityLineItemSchedules)
	    {
	    	productSchedule.add(new wrapper(o));
	    }
	    
    }
    
    
}