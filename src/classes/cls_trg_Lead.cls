global class cls_trg_Lead {
        
      // This method will look for the related Assets (Asset2__c) by the new Leads Serial Numbers using a Batch class ('Batch_AssetToLicenses'), as
    // Mellanox DB contains millions od Assets records and if found, it will assign the right asset on the 'UFM_Asset__c' field on the Lead
    public void assignAssetsToLeadsBySN(list<Lead> newLeads_List) {
        
        set<String> leadsSNs_Set = new set<String>();
        list<String> leadsSNs_list = new list<String>();
        list<Id> SNIds = new list<Id>();
    //    set<String> licensesEmail2LicensesList_Set = getRelatedLicensesByEmail(newLeads_List).keySet();
        list<Lead> leads2Update_List = new list<Lead>();
        
        
 /*      for (Lead newLead : newLeads_List) {
            
            if (newLead.LeadSource != null && newLead.LeadSource.EqualsIgnoreCase('Web - UFM Evaluation Registration') && licensesEmail2LicensesList_Set.contains(newLead.Email)) {
                
                leadsSNs_Set.add(newLead.SN__c);
                SNIds.add(newLead.Id);
            }
        }
        
        leadsSNs_list.addAll(leadsSNs_Set);
        
       if (!leadsSNs_list.isEmpty()) {
            Batch_AssetToLicenses reassign = new Batch_AssetToLicenses();
            reassign.query = 'select id, Serial_Number__c, Asset_Status__c from Asset2__c where  Serial_Number__c in'+ ENV.getStringsForDynamicSoql(leadsSNs_list);
            reassign.NewSNs1 = SNIds;   
            ID batchprocessid = Database.executeBatch(reassign);
        } */
    }
    
  /*  public void countNumberOfLicensesperLeadByEmail(list<Lead> newLeads_List) {
        
        map<String, list<License__c>> licensesEmail2LicensesList_Map = getRelatedLicensesByEmail(newLeads_List);
        
        for (Lead newLead : newLeads_List) {
            
            if(newLead.Email != null) {
                
                if (licensesEmail2LicensesList_Map.containsKey(newLead.Email)) {
                    
                    newLead.Number_Of_Evaluation_Licenses__c = licensesEmail2LicensesList_Map.get(newLead.Email).size();
                }
            }
        }
    }
    
    // This method returns a map of licensesEmails as keys and Licenses as values
    public map<String, list<License__c>> getRelatedLicensesByEmail(list<Lead> newLeads_List) {
        
        set<String> leadEmails_Set = new set<String>();
        map<String, list<License__c>> licensesEmail2LicensesList_Map = new map<String, list<License__c>>();
        
        for (Lead newLead : newLeads_List) {
            
            leadEmails_Set.add(newLead.Email);
        }
        
        if (!leadEmails_Set.isEmpty()) {
        
            list<License__c> relatedLicenses_List = [SELECT Id, Customer_Email__c FROM License__c WHERE Customer_Email__c IN : leadEmails_Set
                                                     and RecordTypeId =: ENV.recordTypeLicenseEvaluation];
            
            for (License__c relatedLicense : relatedLicenses_List) {
                
                if(licensesEmail2LicensesList_Map.containsKey(relatedLicense.Customer_Email__c)) {
                    
                    licensesEmail2LicensesList_Map.get(relatedLicense.Customer_Email__c).add(relatedLicense);
                }
                
                else {
                    
                    licensesEmail2LicensesList_Map.put(relatedLicense.Customer_Email__c, new list<License__c>{relatedLicense});
                }
            }
        }
        
        return licensesEmail2LicensesList_Map;
    }
    
    
    // This method will create a new License upon User Action - clicking the 'Create License' button
    webservice static String createLicenseUponUserAction(Id leadId, String LeadSerialNumber, String leadSource) {
        
        String result = 'failure';
        
        if (leadSource != null && leadSource.EqualsIgnoreCase('Web - UFM Evaluation Registration')) {   
            
            Lead relatedLead = [SELECT Id, FirstName, LastName, country, email, Number_of_Nodes__c FROM Lead WHERE Id =: leadId];
            
            License__c newLicense = createLicenseFromLead(relatedLead);
            
            try {
                insert newLicense;
            }
            
            catch(Exception e) {
                
                result =  'Could not process the request';
            }   
            
            result = newLicense.id;
        }   
        
        return result;
    }
    
    // This method executes the 'returnLicenseFromLead' method 
    public static License__c createLicenseFromLead(Lead relatedLead) {
        
        cls_trg_Lead handler = new cls_trg_Lead();
        License__c newLicense = handler.returnLicenseFromLead(relatedLead);
        
        return newLicense;
    }
    
    // This method returns a new License record with the given Lead info
    public License__c returnLicenseFromLead(Lead relatedLead) {
        
        License__c newLicense = new License__c();
        newLicense.RecordTypeId = ENV.recordTypeLicenseEvaluation;
        newLicense.Product_Type__c  ='UFM';  
        newLicense.Customer_Name__c = relatedLead.FirstName + ' ' + relatedLead.LastName;
        newLicense.Country__c = relatedLead.country;
        newLicense.Customer_Email__c = relatedLead.email;
        newLicense.Managed_Nodes__c = relatedLead.Number_of_Nodes__c;
        newLicense.Related_Lead__c = relatedLead.Id;
        
        return newLicense;
    }*/
    
     //This method will update the Lead CountryCode    
     public void updateAddressCodes(list<Lead> newLeads_List, map<Id, Lead> oldLeads_Map) {
        
        
        for (Lead newLead : newLeads_List)   {
            
            if (oldLeads_Map != null) {
                
                if (newLead.DSCORGPKG__DiscoverOrg_FullCountryName__c != null && newLead.DSCORGPKG__DiscoverOrg_FullCountryName__c != oldLeads_Map.get(newLead.Id).DSCORGPKG__DiscoverOrg_FullCountryName__c) {
                    
                     //newLead.CountryCode = newLead.Country;
                     newLead.Country = newLead.DSCORGPKG__DiscoverOrg_FullCountryName__c;
                }
                
                if (newLead.DSCORGPKG__DiscoverOrg_State_Full_Name__c != null && newLead.DSCORGPKG__DiscoverOrg_State_Full_Name__c != oldLeads_Map.get(newLead.Id).DSCORGPKG__DiscoverOrg_State_Full_Name__c) {
                    
                    //newLead.StateCode = newLead.State;
                    newLead.State = newLead.DSCORGPKG__DiscoverOrg_State_Full_Name__c;
                }
            }
            
            else {
                
               // newLead.CountryCode = newLead.Country;
               // newLead.StateCode = newLead.State;
                newLead.Country = (newLead.DSCORGPKG__DiscoverOrg_FullCountryName__c != null) ? newLead.DSCORGPKG__DiscoverOrg_FullCountryName__c : newLead.Country;
                newLead.State = (newLead.DSCORGPKG__DiscoverOrg_State_Full_Name__c != null) ? newLead.DSCORGPKG__DiscoverOrg_State_Full_Name__c : newLead.State;
           		 
            }
            
            system.debug('newLead : ' + newLead);
            system.debug('newLead CountryCode : ' + newLead.CountryCode);
            system.debug('newLead StateCode : ' + newLead.StateCode);
            system.debug('newLead Country : ' + newLead.Country);
                 
        }           
    }
    
    // This metohd will update the lead with the related Lead Owner according to the Lead's Country Or State
    // mapped in the Regions and Region States records
    public void updateLeadOwner(list<Lead> newLeads_List, map<Id, Lead> oldLeads_Map) {
    	
    	map<String ,Id> leadCountriesOrStates2LeadSources_Map = getMapOfRegionsStatesAndCountriesToLeadOwners(newLeads_List, oldLeads_Map);
    	list<Lead> leads2Update_List = new list<Lead>();
    	
    	for (Lead newLead : newLeads_List) {
    		
    		Lead lead2Update = new Lead(Id = newLead.Id);
    		if (newLead.StateCode != null) {
    			
    			if (leadCountriesOrStates2LeadSources_Map.containsKey(newLead.StateCode)
    			    && leadCountriesOrStates2LeadSources_Map.get(newLead.StateCode) != null) {
    				
    				lead2Update.OwnerId = leadCountriesOrStates2LeadSources_Map.get(newLead.StateCode);
    				leads2Update_List.add(lead2Update);
    			}
    		}
    		
    		else if (leadCountriesOrStates2LeadSources_Map.containsKey(newLead.Country)
    		         && leadCountriesOrStates2LeadSources_Map.get(newLead.Country) != null) {
    			 
				lead2Update.OwnerId = leadCountriesOrStates2LeadSources_Map.get(newLead.Country);
    			system.debug('lead2Update.OwnerId : ' + lead2Update.OwnerId);
    			leads2Update_List.add(lead2Update);
    		}
    	}
    	
    	if (!leads2Update_List.isEmpty()) {
    		
    		update leads2Update_List;
    	}
    	
    }
    
    
    
    // This method returns a map of Countries and states associated with the given leads where the map
    // key is the State/Contry and it's value is the Lead Owner
    private map<String ,Id> getMapOfRegionsStatesAndCountriesToLeadOwners(list<Lead> newLeads_List, map<Id, Lead> oldLeads_Map) {
    	
    	
    	set<String> leadsStateOrCountriesNames_Set = new set<String>();
    	map<String ,Id> leadCountriesOrStates2LeadSources_Map = new map<String ,Id>();
    	
    	for (Lead newLead : newLeads_List) {
    		
    		if (oldLeads_Map != null) {
    			
    			if ( (newLead.Country != null && newLead.Country != oldLeads_Map.get(newLead.Id).Country) 
						|| (newLead.StateCode != null && newLead.StateCode != oldLeads_Map.get(newLead.Id).StateCode) ) {
		 
    			    if (newLead.StateCode != null) {
    			     	system.debug('newLead.StateCode : ' + newLead.StateCode);
    			     	leadsStateOrCountriesNames_Set.add(newLead.StateCode);
    			    }	
    			     
    			    else {
    			     	system.debug('newLead.Country : ' + newLead.Country);
    			     	leadsStateOrCountriesNames_Set.add(newLead.Country);
    			    }
						
    			}
    		}
	    			
	    		
    		else {
    			
    			if (newLead.StateCode != null) {
    			    system.debug('newLead.StateCode : ' + newLead.StateCode); 	
			     	leadsStateOrCountriesNames_Set.add(newLead.StateCode);
			    }	
			     
			    else {
			     	system.debug('newLead.Country : ' + newLead.Country);
			     	leadsStateOrCountriesNames_Set.add(newLead.Country);
			    }
    		}
    	}
    	
    	if (!leadsStateOrCountriesNames_Set.isEmpty()) {
    		
    		for (Regions__c region : [SELECT Id, Lead_Owner__c, Name FROM Regions__c WHERE Name IN : leadsStateOrCountriesNames_Set]) {
    			
    			leadCountriesOrStates2LeadSources_Map.put(region.Name, region.Lead_Owner__c);
    		}
    		
    		for (Region_State__c theState : [SELECT Id, Lead_Owner__c, StateCode__c FROM Region_State__c WHERE StateCode__c IN : leadsStateOrCountriesNames_Set]) {
    			
    			leadCountriesOrStates2LeadSources_Map.put(theState.StateCode__c, theState.Lead_Owner__c);
    		}
    	}
    	system.debug('leadCountriesOrStates2LeadSources_Map : ' + leadCountriesOrStates2LeadSources_Map);
    	return leadCountriesOrStates2LeadSources_Map;
    }
    
    // This method will update the Lead Account (Related_Account__c) with the relevant Account
    public void updateLeadAccount(list<Lead> newLeads_List) {
    	
    	Id parentAccountRecordTypeId = ENV.GetRecordTypeIdByName('Parent Account');
    	list<Account> allAccountsWithSalesRep = [SELECT Id, Sales_Rep__c, RW_Domain_multi__c FROM Account WHERE Sales_Rep__c != null
    											 and RW_Domain_multi__c != null and RecordTypeId =: parentAccountRecordTypeId];
    	
    	list<Account> allAccountsWithNOSalesRep = [SELECT Id, Sales_Rep__c, RW_Domain_multi__c FROM Account WHERE Sales_Rep__c = null
    											 and RW_Domain_multi__c != null and RecordTypeId =: parentAccountRecordTypeId];
    	
    	
    	for (Lead newLead : newLeads_List) {
    		
    		for (Account relatedAccount : allAccountsWithSalesRep) {
    			
    			String domainName = ENV.getDomainFromEmail(newLead.Email);
    			
    			if (relatedAccount.RW_Domain_multi__c.contains(domainName)) {
    				
    				newLead.Related_Account__c = relatedAccount.Id;
    				return;
    			}
    		}
    	}
    	
    	for (Lead newLead : newLeads_List) {
    		
    		for (Account relatedAccount : allAccountsWithNOSalesRep) {
    			
    			String domainName = ENV.getDomainFromEmail(newLead.Email);
    			
    			if (newLead.Related_Account__c == null && relatedAccount.RW_Domain_multi__c.contains(domainName)) {
    				
    				newLead.Related_Account__c = relatedAccount.Id;
    				return;
    			}
    		}
    	}
    }
    
    // This method will update the fields 'Management_Level__c' and 'Job_Function__c' with the values from Custom settings
    // mapped to related values from Discover Org fields
    public void updateRelatedDiscoverOrgFields(list<Lead> newLeads_List, map<Id, Lead> oldLeads_Map) {
    	
    	for (Lead newLead : newLeads_List) {
    		
    		if (oldLeads_Map != null) {
    			
    			if (newLead.DSCORGPKG__Job_Function__c != null) {
	    			
	    			if (newLead.DSCORGPKG__Job_Function__c != oldLeads_Map.get(newLead.Id).DSCORGPKG__Job_Function__c) {
	    			    	
		    			list<DiscoverOrgLeadJobFunction__c> leadJobFunction_List = 	DiscoverOrgLeadJobFunction__c.getall().values();
		    			   
	    			    for (DiscoverOrgLeadJobFunction__c jobFunc : leadJobFunction_List) {
	    			   	
	    			   		if (jobFunc.DiscoveOrg_Name__c != null && jobFunc.DiscoveOrg_Name__c.EqualsIgnoreCase(newLead.DSCORGPKG__Job_Function__c)) {
	    			   			   
	    			   			newLead.Job_Function__c = jobFunc.Value__c;
	    			   			break;
	    			   		}
	    			    }
	 			    }
    			}
    			
    			else {
    				
    				newLead.Job_Function__c = null;	
    			}
 			    
	 			if (newLead.DSCORGPKG__Management_Level__c != null) {
	 			   
	 			    if (newLead.DSCORGPKG__Management_Level__c != oldLeads_Map.get(newLead.Id).DSCORGPKG__Management_Level__c) {
	 			    	
	 			    	list<DiscoverOrgLeadManagementLevel__c> managementLevel_List = 	DiscoverOrgLeadManagementLevel__c.getall().values();
		    			   
	    			    for (DiscoverOrgLeadManagementLevel__c ml : managementLevel_List) {
	    			   	
	    			   		if (ml.DiscoveOrg_Name__c != null && ml.DiscoveOrg_Name__c.EqualsIgnoreCase(newLead.DSCORGPKG__Management_Level__c)) {
	    			   			   
	    			   			newLead.Management_Level__c = ml.Value__c;
	    			   			break;
	    			   		}
	    			    }
	 			    }
	 			}
	 			
	 			else {
    				
    				newLead.Management_Level__c = null;	
    			}
    		}
    		
    		else {
    			
				list<DiscoverOrgLeadJobFunction__c> leadJobFunction_List = 	DiscoverOrgLeadJobFunction__c.getall().values();
		    			   
			    for (DiscoverOrgLeadJobFunction__c jobFunc : leadJobFunction_List) {
			   	
			   		if (jobFunc.DiscoveOrg_Name__c != null && jobFunc.DiscoveOrg_Name__c.EqualsIgnoreCase(newLead.DSCORGPKG__Job_Function__c)) {
			   			   
			   			newLead.Job_Function__c = jobFunc.Value__c;
			   			break;
			   		}
			    }
	 			    
	 			    
	 			list<DiscoverOrgLeadManagementLevel__c> managementLevel_List = 	DiscoverOrgLeadManagementLevel__c.getall().values();
			    			   
			    for (DiscoverOrgLeadManagementLevel__c ml : managementLevel_List) {
		    			   	
			   		if (ml.DiscoveOrg_Name__c != null && ml.DiscoveOrg_Name__c.EqualsIgnoreCase(newLead.DSCORGPKG__Management_Level__c)) {
			   			   
			   			newLead.Management_Level__c = ml.Value__c;
			   			break;
			   		}
			    }
    		}
    	}
    }
    
    // This method will update the Lead's Campaign on Lead create if the Lead source is 'Discoverorg'
    public void updateLeadCampaign(list<Lead> newLeads_List) {
    	
    	set<Id> leadIds_Set = new set<Id>();
     	
    	for (Lead newLead : newLeads_List) {
    		
    		if (newLead.LeadSource != null && newLead.LeadSource.EqualsIgnoreCase('DiscoverOrg')) {
    			system.debug('inside 397');
    			leadIds_Set.add(newLead.Id);
    		}
    	}
    	
    	if (!leadIds_Set.isEmpty()) {
    		
    		system.debug('inside 404');
			
    		updateLeadsCampaignAsync(leadIds_Set);           
    	}
    }
    
    // This method will update the Lead's Campaign by creating Campaign members when a new 'DiscoverOrg' Lead is created
    @future
    public static void updateLeadsCampaignAsync(set<Id> leadIds_Set) {
    	
    	map<Id, Lead> leadsIds2Leads_Map = new map<Id, Lead>([SELECT Id, (SELECT Id FROM CampaignMembers) FROM Lead WHERE Id IN : leadIds_Set]);
		list<Campaign> campaigns2Assign_List = [SELECT Id FROM Campaign WHERE Name like 'DiscoverOrg%' and isActive =  true order by EndDate desc limit 1];
		list<CampaignMember> members2Create_List = new list<CampaignMember>();
		
		system.debug('inside 416');
		
		
		if (!campaigns2Assign_List.isEmpty()) {
			
			for (Id newLeadId : leadIds_Set) {
				
				if (leadsIds2Leads_Map.containsKey(newLeadId)) {
					
					if (leadsIds2Leads_Map.get(newLeadId).CampaignMembers.isEmpty()) {
						
						CampaignMember newMember = new CampaignMember();
						newMember.CampaignId = campaigns2Assign_List[0].Id;
						newMember.LeadId = newLeadId;
						members2Create_List.add(newMember);
					}
				}
			}
			
			if (!members2Create_List.isEmpty()) {
    			try {
    				
    				insert members2Create_List;
    			}
    			
    			catch(Exception e) {
    				
    				system.debug('Error while updating Leads : ' + e.getMessage());
    			}
			}
		}
    }
}