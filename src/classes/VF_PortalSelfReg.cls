// This Class is a controller class for a form that once filled up by the community users,
// if all the validations passed, it will create new Contact or update the existing one with all the customer info
public class VF_PortalSelfReg {
    
    Contact con{get;set;}
   
    // picklist of the contact types
    public List<selectOption> contactTypes_List {get;set;}
    // picklist stating if the contact needs Access
    public List<selectOption> NeedAccess_List {get;set;}
    // String representing the chosen Password 
    public String password{get;set;}
    // String representing the chosen contact type
    public String contactTypeStr{get;set;}
    // String representing the chosen answer for needed access
    public String needAccessStr{get;set;}
    // Constants for re use in this class
    public static final String TYPE_DESIGN_IN = 'Mellanox Silicon Design-In customer';
    public static final String TYPE_SYSTEM = 'Mellanox System Customer';
    public static final String TYPE_SYSTEM_OEM = 'Mellanox System OEM customer';
    public static final String TYPE_EZCHIP_DESIGN_IN = 'Ezchip Design-In customer';
    public Pagereference thePage{get;set;}
    private Regions__c regionForAccount{get;set;}
    private Contact contactFound{get;set;}
    private list<User> user_List{get;set;}
    private boolean sendLoginDetails{get;set;}
    public list<Contact> contact_List {get;set;}
    private cls_Contact_Trigger handler;
    
    // Controller where we initiate the page
    public VF_PortalSelfReg(ApexPages.StandardController controller)
    {
        con = (Contact)controller.getRecord();
        sendLoginDetails = false;
        contact_List = new list<Contact>();
        handler = new cls_Contact_Trigger();
    }
            
    // filling the countries picklist with values from the custom object 'Region' records,
    // each record name represents a country
    
    // filling the contact type pickilst with values
    public List<SelectOption> getTypes() {
        
        contactTypes_List = new List<selectOption>();

        contactTypes_List.add(new SelectOption(TYPE_SYSTEM, TYPE_SYSTEM));
        contactTypes_List.add(new SelectOption(TYPE_DESIGN_IN, TYPE_DESIGN_IN));
        contactTypes_List.add(new SelectOption(TYPE_EZCHIP_DESIGN_IN, TYPE_EZCHIP_DESIGN_IN));
        
        return contactTypes_List;
    }
    
    // filling the contact types picklist with values
    public List<SelectOption> getNeedAccess() {
        
        NeedAccess_List = new List<selectOption>();
        
        NeedAccess_List.add(new SelectOption('Yes', 'Yes'));
        NeedAccess_List.add(new SelectOption('No', 'No'));
        
        return NeedAccess_List;
    }

    
    // This metohd will update/create Contact based on different requirements
    public pageReference registerUser()
    {
        
        system.debug(' contact Email 102 : ' + con.Email);
        user_List = new list<User>();
        user_List = [SELECT u.Contact.Contact_Type__c, u.ContactId,  u.Email, u.Name FROM User u
                    WHERE u.Email =: con.Email];  // and u.Contact.Contact_Type__c = 'standard'
        system.debug('MailingCountryCode: ' + con.MailingCountryCode);
        try{  
            regionForAccount = [SELECT Id, Support_Region__c, OwnerId FROM Regions__c WHERE version1__c =: con.MailingCountryCode];
       }
            
        catch(Exception e){
            system.debug('No region For Account has been found');
        }
        
        system.debug('user_List 105: ' + user_List);
        
        // Capitilazing the First and last name's first letter in case the user forgot to do so
        con.LastName = con.LastName.substring(0,1).toUpperCase() + con.LastName.substring(1);
        con.FirstName = con.FirstName.substring(0,1).toUpperCase() + con.FirstName.substring(1);
        
        // If a related user has found, we update the contact with the form's details 
        if (!user_List.isEmpty())
        {
            User user = user_List[0];  
            system.debug('inside 116');
            userFound(user);
            system.debug('inside 118');
        }
        
        // If no user with the same email as the Email on the requested form has been found
        else
        {
            userNotFound();
        }
        
        PageReference page = System.Page.Community_self_reg_success_page;
        page.setRedirect(true);
        return page;
        
         
    }
        
        // This method will create a new user associated with the founded contact
        private void createUser()
        {
            
            User user = new User();
           
            system.debug('inside 159'); 

            if (regionForAccount.Support_Region__c.EqualsIgnoreCase('EMEA')){
            
                user.TimeZoneSidKey = 'Europe/Dublin';
            }
            
            else if(regionForAccount.Support_Region__c.EqualsIgnoreCase('Americas')){
                
                user.TimeZoneSidKey = 'America/New_York';
            }
            else if(regionForAccount.Support_Region__c.EqualsIgnoreCase('India,ANZ,SEA')){
                
                user.TimeZoneSidKey = 'Asia/Colombo';
            }
            else if(regionForAccount.Support_Region__c.EqualsIgnoreCase('China')){
                
                user.TimeZoneSidKey = 'Asia/Shanghai';
            }
            else if(regionForAccount.Support_Region__c.EqualsIgnoreCase('Japan-Korea')){
                
                user.TimeZoneSidKey = 'Asia/Tokyo';
            }
            else if(regionForAccount.Support_Region__c.EqualsIgnoreCase('Taiwan')){
                
                user.TimeZoneSidKey = 'Asia/Taipei';
            }
            
            else {
                
                 user.TimeZoneSidKey = 'America/New_York';
            }
            
            user.ContactId = contactFound.Id;
            user.FirstName = con.FirstName;
            user.LastName = con.LastName;
            user.Email = contactFound.Email;
            user.userName = contactFound.Email;
            user.EmailEncodingKey = 'ISO-8859-1';
            String alias_Str = con.FirstName.subString(0,2) + con.LastName;
            user.Alias = alias_Str.substring(0, 4);
            user.LocaleSidKey = 'en_US';
            user.CommunityNickname = con.firstName + con.lastName ;
        
            if(regionForAccount.Support_Region__c.EqualsIgnoreCase('China')){
                
                user.LanguageLocaleKey = 'zh_CN';
            }
            
            else {
                
                user.LanguageLocaleKey = 'en_US';
            }
            
            Account userRelatedAccount = handler.getAccountByEmailDomain(con.Email, contactTypeStr);
            
            Profile systemSupportProfile = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox System Support Community User'];
            Profile designInProfile = [SELECT Id FROM Profile WHERE Name =: 'MyMellanox Design In Community User'];
            Profile systemOEMProfile =  [SELECT Id, Name FROM Profile WHERE Name =: 'MyMellanox System OEM Community User'];
            Profile ezchipDesignInProfile =  [SELECT Id, Name FROM Profile WHERE Name =: 'MyMellanox Ezchip Design In Community User'];
            
            /**********************   REMOVED BY ELAD ON 28/10/2014 - WILL HAVE TO BE ADDED WHEN THE PORTAL IS UP AND RUNNING   ********************/
            
        /*  UserRole systemUserRole = new UserRole();
            UserRole designInUserRole = new UserRole();
            
            String roleSystem_Str = contactFound.Account.Name + ' Partner User';
            String roleDesignIn_Str = contactFound.Account.Name + ' Partner Manager';
            
            systemUserRole = [SELECT Id FROM UserRole WHERE Name =: roleSystem_Str];
            designInUserRole = [SELECT Id FROM UserRole WHERE Name =: roleDesignIn_Str];
            
        */  
        
           /*************************************************************************************************************************************/
           
            if (contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN)) {
                
                user.ProfileId = designInProfile.Id;
                
            //  if (designInUserRole.Id != null) {
                    
            //      user.UserRoleId = designInUserRole.Id;
            //  }
            } 
            
            else if(contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN)) {
                
                user.ProfileId = ezchipDesignInProfile.Id;
                
            }
            
            else if (userRelatedAccount != null && userRelatedAccount.Customer_Type__c != null && userRelatedAccount.Customer_Type__c.EqualsIgnoreCase('OEM')) {
                
                user.ProfileId = systemOEMProfile.Id;
            }
            
            else {
                
                user.ProfileId = systemSupportProfile.Id;
                
            //  if (systemUserRole.Id != null) {
                    
            //      user.UserRoleId = systemUserRole.Id;
            //  }
            }
            
            String user2Insert_Str = JSON.serialize(user);
            cls_Create_User.insertUserAsync(user2Insert_Str, null, false, false);  
            //insertRecord(user);
        }
        
        
        // this method will update the related contact if found with the requested form info, OR
        // will create a new contact and associate it to the relevant account if no contact has been found
        private void userNotFound()
        {
            list<Contact> contactFounded_List = [SELECT Id, Portal_Access__c, MailingCountryCode, MailingStateCode, Case_to_techowner__c ,No_contract_access__c, CRM_Content_Permissions__c, Portal_Approval__c,
                                                Email, createdDate, AccountId, Account.Name, Contact_Type__c, Name FROM Contact WHERE Email =: con.Email order by CreatedDate asc];

            dateTime earliestCreatedDate = con.CreatedDate;
            
            if (contactFounded_List != null && !contactFounded_List.isEmpty())
            {                                                                         
                contactFound = contactFounded_List[0];
                
                if (contactFound.Id != null)
                {      
                    
                    system.debug('contactFound :' + contactFound);
                    
                    if(contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) ) {
                    
                        contactFound.access_required__c = TYPE_DESIGN_IN;
                    }
                    
                    else if(contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN) ) {
 
                        contactFound.access_required__c = TYPE_EZCHIP_DESIGN_IN;
                    }
                    
                    else {
                        
                        if (contactFound.Portal_Approval__c != null && contactFound.Portal_Approval__c.EqualsIgnoreCase('Yes')) {
                        
                            contactFound.CRM_Content_Permissions__c = 'System NDA';
                            contactFound.Portal_Access__c = 'Mellanox System Customer';
                            createUser();
                        }
                            
                        else {     
                        
                            if ( contactFound.AccountId ==  ENV.UnknownUSAccount
                              || contactFound.AccountId ==  ENV.UnknownAccount
                              || contactFound.AccountId ==  ENV.UnknownEMEAAccount ) {
                                
                                contactFound.access_required__c = TYPE_SYSTEM;
                              }
                              
                              else {
                            
                                  contactFound.Access_denied__c = true;
                                  contactFound.Reject_Reason__c = 'No Contract';
                              }
                        }
                    }
                    
                    upodateContactWithSameContactType(true, false, true);
                }
            }
            
            else
            {
                
                Account foundAccountByEmail = new Account();
                
                try{
                    
                    foundAccountByEmail = handler.getAccountByEmailDomain(con.Email, contactTypeStr);
                    system.debug('foundAccountByEmail : ' + foundAccountByEmail );
                }
                
                catch(Exception e){ 
                    
                    system.debug('No Account has been found');
                }
                
                contactFound = new Contact();

                if (foundAccountByEmail != null && foundAccountByEmail.Id != null){
                    system.debug('inside 241');
                    
                    if(contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) || contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN)){
                        
                        if (contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) ) {
                    
                            contactFound.access_required__c = TYPE_DESIGN_IN;
                        }
                        
                        else if(contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN) ) {
     
                            contactFound.access_required__c = TYPE_EZCHIP_DESIGN_IN;
                        }
                        
                        system.debug('inside 230');
                        contactFound.AccountId = foundAccountByEmail.Id;
                          
                        upodateContactWithSameContactType(false, false, true);
                    }
                     
                    
                    else{
                        
                         contactFound.AccountId = foundAccountByEmail.Id;
                         
                    /*     if (foundAccountByEmail.Customer_Type__c.EqualsIgnoreCase('OEM')) {
                            
                            contactFound.Portal_Access__c = 'Mellanox System OEM Customer';
                         }
                         
                         else {
                            
                            contactFound.Portal_Access__c = 'Mellanox System Customer';
                         }
                         
                         contactFound.CRM_Content_Permissions__c = 'System NDA';
                        
                      */   
                         
                         system.debug('contactFound.AccountId' + contactFound.AccountId);
                         upodateContactWithSameContactType(false, false, true);
                    }
                }
                
                else {
                   
                    if(contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) || contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN)){
                        
                       
                       if (contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) ) {
                    
                            contactFound.access_required__c = TYPE_DESIGN_IN;
                        }
                        
                        else if(contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN) ) {
     
                            contactFound.access_required__c = TYPE_EZCHIP_DESIGN_IN;
                        }
                        
                    }
                   
                    Id unknownAccountByRegionId; 
            
                    // if the country is in the US Region
                    if (regionForAccount.Support_Region__c.EqualsIgnoreCase('Americas')){
                        
                        unknownAccountByRegionId = ENV.UnknownUSAccount;  
                    }
                    
                    // if the country is in the EMEA or APAC Region
                    else {
                        
                        unknownAccountByRegionId = ENV.UnknownEMEAAccount;   
                    }
                    
                    contactFound.AccountId = unknownAccountByRegionId;
                    
                    if ( contactTypeStr.EqualsIgnoreCase(TYPE_SYSTEM) ) {
                        
                        contactFound.access_required__c = TYPE_SYSTEM;
                        //contactFound.CRM_Content_Permissions__c = 'System NDA';
                    }
                    
                    
                    upodateContactWithSameContactType(false, false, true);
                }
                
            }
        }
        
        //This method will update the contact info from the requested form, if a user
        // associated to the contact has been found
        private void userFound(User user)
        {
            
            try{
                
                contactFound = [SELECT Id, FirstName, LastName, Email, Phone, Company_Name_from_Web__c, MailingCountryCode, MailingStreet,Mellanox_Support_contact__c, Portal_Approval__c,
                Mellanox_silicon_device__c,Mellanox_silicon_description__c,  purchase_Mellanox_equipment_from__c ,Serial_Number__c,  MailingCity, Account.Name,
                MailingStateCode, MailingPostalCode, access_required__c, MailingCountry, Portal_Access__c, Contact_Type__c,
                need_access_to_silicon_datasheets__c, send_login_details__c, Name FROM Contact WHERE Id =: user.ContactId];
            }
            
            catch (Exception e){
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The operation could not be performed because the user is a system user '));
            }
            
            if (contactFound != null && contactFound.Contact_Type__c != null)
            {
                // If the related Contact has the same contact type as on the request form
                if (contactFound.Contact_Type__c.EqualsIgnoreCase(contactTypeStr))
                {
                    upodateContactWithSameContactType(true, true, true);
                    system.resetPassword(user.Id, true);
                }
                
                // If the related Contact has a different contact type than the type on the request form
                else if (!contactFound.Contact_Type__c.EqualsIgnoreCase(contactTypeStr))
                {
                    // If the Contact has  contact type 'System'  and requests 'Design In'
                    if(contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) || contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN))
                    {
                       
                       if (contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) ) {
                    
                            contactFound.access_required__c = TYPE_DESIGN_IN;
                        }
                        
                        else if(contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN) ) {
     
                            contactFound.access_required__c = TYPE_EZCHIP_DESIGN_IN;
                        }
                        
                        contactFound.send_login_details__c = false;
                        upodateContactWithSameContactType(true, false, false);
                    }
                    
                    // If the Contact has  contact type 'Design In' and requests 'System'
                    else {
                    
                         system.resetPassword(user.Id, true);
                         upodateContactWithSameContactType(true, true, false);
                    }
                }
            }
        }
        
        
        // This method will get the existing Account if founded, where one of its fields (domain__c, Domain1__c or Domain2__c) values
        // matches the domain name as entered in the request form
        private Account getAccountByEmailDomain()
        {
            Account accountFoundByEmail;
            if (con.Email != null && con.Email != '')
            {
                
                String domainName = con.Email.substring(con.Email.indexOf('@') + 1);
                String domainNameNoDot = '';
                String domainNameOneDot = '';
                set<String> domainNames_Set = new set<String>();
                
                String[] parts = domainName.split('[.]');
                domainNameNoDot = parts[0];
                domainNames_Set.add(domainNameNoDot);
                
                if (parts.size() > 2)
                {
                    String secondtSection = parts[1];
                    domainNameOneDot = domainNameNoDot + '.' + secondtSection;
                    domainNames_Set.add(domainNameOneDot);
                }
                
                Id parentAccountRecordTypeId = ENV.GetRecordTypeIdByName('Parent Account');
                list<Account> acc_List = new list<Account>();
                accountFoundByEmail = new Account();
             
                if (contactTypeStr.equalsIgnoreCase(TYPE_SYSTEM)) {
               
                    acc_List = [SELECT Id, Name, Customer_Type__c FROM Account WHERE (domain__c IN : domainNames_Set OR 
                    Domain1__c IN : domainNames_Set OR Domain2__c IN : domainNames_Set) AND RecordTypeId =: parentAccountRecordTypeId 
                    order by Highest_Active_contract_rank__c desc NULLS LAST];
                    
                }
                
                else {
                    
                    acc_List = [SELECT Id, Name, Customer_Type__c FROM Account WHERE (domain__c IN : domainNames_Set OR 
                    Domain1__c IN : domainNames_Set OR Domain2__c IN : domainNames_Set) AND RecordTypeId =: parentAccountRecordTypeId
                    and Technical_Owner__c != null 
                    order by Highest_Active_contract_rank__c desc NULLS LAST];
                }
                
                if (!acc_List.isEmpty()) {
                    
                for(Integer i = 0; i < acc_List.size(); i++) {
                    
                    if (acc_List[i].Name.EqualsIgnoreCase('IBM - XIV')) {
                        
                        acc_List.remove(i);
                        break;
                    }
                }   
                    
                accountFoundByEmail = acc_List[0];
            }
                
            }
            
            return accountFoundByEmail;
        }
        
        // This method will fill up the Contact with the relevant values
        private Contact assignContactFieldsValues()
        {
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType ContactSchema = schemaMap.get('Contact');
            Map<String, Schema.SObjectField> fieldMap = ContactSchema.getDescribe().fields.getMap();
                    
            for (String field :fieldMap.keySet())
            {    
                
                system.debug('field :' + field);
                
                try         
                {
                    if(!(fieldMap.get(field).getDescribe().getType()==Schema.DisplayType.BOOLEAN) && field != 'AccountId' && field != 'Id')  
                    { system.debug('inside if field:' + field);
                        
                        if(con.get(field) != null){
                        
                            contactFound.put(field, con.get(field));
                        }
                    }
                }  
        
                catch(Exception e)
                {
                    system.debug('Non Editable field :' + field);
                } 
            } 
            
            system.debug('contactFound iD 453 :' + contactFound.Id);
            return contactFound;     
        }
        
        
        
        
        //This method will update the contact retrieved from the database or create a new contact with the requested form fields values
        private void upodateContactWithSameContactType(Boolean isUpdate, boolean sendLoginDetails, boolean setContactType)
        {
            system.debug('contactFound 463 :' + contactFound);
            system.debug('contactFound 463 contactTypeStr:' + contactTypeStr);
            contactFound = assignContactFieldsValues(); 
            //contactFound.MailingCountry = con.MailingCountryCode;
            
            if (setContactType) {
                
                contactFound.Contact_Type__c = contactTypeStr;
            }
            
            if(regionForAccount != null){
                contactFound.Region__c = regionForAccount.Support_Region__c;
            }
           // contactFound.send_login_details__c = sendLoginDetails;
                        
            Regions__c contactOwnerRegion;
            
            try{
                
                // getting the user that serves as the region support owner and assigning him as the Contact owner
                contactOwnerRegion = [SELECT Support_Owner__c FROM Regions__c Where version1__c =: con.MailingCountryCode];
            }
            
            catch(Exception e){
                
                system.debug('No contact owner has been found');
            }
            
            if(contactOwnerRegion.Support_Owner__c != null)
            {
                contactFound.OwnerId = contactOwnerRegion.Support_Owner__c;
            }
                
            if(needAccessStr != null)
            {
                if (needAccessStr.equalsIgnoreCase('Yes'))
                {
                    contactFound.need_access_to_silicon_datasheets__c = true;
                }
                
                else
                {
                    contactFound.need_access_to_silicon_datasheets__c = false;
                }
            }
            
            
            if (isUpdate)
            {
                // If a user exists and the type required is Type System and Portal Approval = Yes, we send login details and access required becomes null
                if (contactFound.Contact_Type__c.EqualsIgnoreCase(contactTypeStr) && contactTypeStr.EqualsIgnoreCase(TYPE_SYSTEM)) {
                    
                    if (contactFound.Portal_Approval__c.EqualsIgnoreCase('Yes')) {
                        
                       // contactFound.send_login_details__c = true;
                       // contactFound.access_required__c = null;
                    }
                }
                UpdateRecord(contactFound);
                return;
            }
        
            else
            {   
                
                if(contactOwnerRegion.Support_Owner__c != null)
                {
                    contactFound.OwnerId = contactOwnerRegion.Support_Owner__c;
                }
                
                else
                {
                    User contactOwner = [SELECT Id FROM User Where Name like '%Goldin%' limit 1];
                    contactFound.OwnerId = contactOwner.Id;
                }
                system.debug('contactFound.AccountId in insert :' + contactFound.AccountId);
                insertRecord(contactFound);
                
                Contact contactFromDB = [SELECT Id, FirstName, LastName, Email, Phone, Company_Name_from_Web__c, MailingStreet,Mellanox_Support_contact__c, 
                Mellanox_silicon_device__c,Mellanox_silicon_description__c,  purchase_Mellanox_equipment_from__c , Serial_Number__c,  MailingCity, Account.Name,
                MailingState, MailingPostalCode, access_required__c, MailingCountry, Contact_Type__c, Portal_Approval__c, AccountId, Account.Customer_Type__c,  
                CRM_Content_Permissions__c, need_access_to_silicon_datasheets__c,  Name FROM Contact WHERE Id =: contactFound.Id];
                
                contactFound = contactFromDB;
                system.debug('contactFound.Contact_Type__c :'+ contactFound.Contact_Type__c);
                system.debug('contactFound.Portal_Approval__c :'+ contactFound.Portal_Approval__c);
                system.debug('contactFromDB.Contact_Type__c :'+ contactFromDB.Contact_Type__c);
                system.debug('contactFromDB.Portal_Approval__c :'+ contactFromDB.Portal_Approval__c);
                
                if (contactFromDB.Contact_Type__c.EqualsIgnoreCase(TYPE_SYSTEM) && contactFromDB.Portal_Approval__c.EqualsIgnoreCase('Yes')){
                    
                    createUser();
                    
                    if ( contactFromDB.Account.Customer_Type__c.EqualsIgnoreCase('OEM') ) {
                        
                        contactFound.Portal_Access__c = 'Mellanox System OEM Customer';
                    }
                    
                    else {
                    
                        contactFound.Portal_Access__c = 'Mellanox System Customer';
                    }
                    
                    contactFound.CRM_Content_Permissions__c = 'System NDA';
                    UpdateRecord(contactFound);
                }
                
                else if (contactFromDB.Contact_Type__c.EqualsIgnoreCase(TYPE_SYSTEM) 
                         && contactFromDB.Portal_Approval__c.EqualsIgnoreCase('No')
                         && contactFound.AccountId !=  ENV.UnknownUSAccount
                         && contactFound.AccountId !=  ENV.UnknownAccount
                         && contactFound.AccountId !=  ENV.UnknownEMEAAccount ){
                    
                    contactFound.Access_denied__c = true;
                    contactFound.Reject_Reason__c = 'No Contract';
                    UpdateRecord(contactFound);
                }
                return;
            }
        }
    
     //This method will insert the given Object
    public pageReference insertRecord(SObject obj)
    {
         Database.SaveResult  sr = Database.insert(obj, false);
        
         system.debug('obj on insert :' + obj);
         
         if(sr.isSuccess() == false)
         {
             system.debug('==>error : '+sr.getErrors());
           
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The operation could not be performed because of: '+ sr.getErrors()));
             return null;
         }
        
    
         String hostname = ApexPages.currentPage().getHeaders().get('Host');
         String absolutePath = 'https://' + hostname + '/apex/VF_CommunitySelfReg_success_page';
         thePage = new Pagereference(absolutePath );
         thePage.setRedirect(true);
         return thePage;
         
    }
    
    
    //This method will update the given Object
    public pageReference UpdateRecord(SObject obj)
    {
        
        contactFound = (Contact)obj;
        
        system.debug('contactFound: ' + contactFound);
        system.debug('contactFound.Id: ' + contactFound.Id);
        system.debug('contactFound.Contact_Type__c: ' + contactFound.Contact_Type__c);
        system.debug('con.Contact_Type__c: ' + contactTypeStr);
        system.debug('user_List 355: ' + user_List); 
        
        if (contactFound.Id != null && contactFound.Contact_Type__c.EqualsIgnoreCase(TYPE_SYSTEM)
            && (contactTypeStr.EqualsIgnoreCase(TYPE_DESIGN_IN) || contactTypeStr.EqualsIgnoreCase(TYPE_EZCHIP_DESIGN_IN)) ){
                
              // do nothing 
            }
            
        else if (contactFound.Id != null){
                system.debug('User list : 599 ' + user_List);
                if(!user_List.isEmpty()){
                    
                    User user = user_List[0];
                    system.debug('user id :' + user.Id);
                    //system.resetPassword(user.Id, true);
                }
        }
        
        system.debug('obj on update :' + obj);
        system.debug('contactFound serial before update :' + contactFound.Serial_Number__c);
        system.debug('contactFound Id before update :' + contactFound.Id);
        system.debug('contact name in form :' + con.LastName);
        system.debug('contactFound Name before update : :' + contactFound.LastName);
        
         Database.SaveResult  sr = Database.update(obj, false);
         
         
         
         if(!sr.isSuccess())
        {
            system.debug('==>error : '+sr.getErrors());
           
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The operation could not be performed because of: '+ sr.getErrors()));
        }
       
         system.debug('update success');
         String hostname = ApexPages.currentPage().getHeaders().get('Host');
         String absolutePath = 'https://' + hostname + '/apex/VF_CommunitySelfReg_success_page';
         thePage = new Pagereference(absolutePath );
         thePage.setRedirect(true);
         return thePage;
    }
    
    public PageReference reset() {
        
        PageReference newpage = new PageReference(System.currentPageReference().getURL());
        newpage.getParameters().clear();
        newpage.setRedirect(true);
        return newpage;
    }
    
    

}