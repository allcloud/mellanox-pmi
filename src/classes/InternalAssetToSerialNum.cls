global class InternalAssetToSerialNum implements Database.Batchable<sObject>
{ 
	global string query;
	global list<string> UpdSNs;
	global List<id> NewSNs1;
	global Map<string,Serial_Number__c> Serials ;
	
	global database.querylocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		Serials= new Map<string,Serial_Number__c> (); 
		for( Serial_Number__c SN:[select id,Serial_Upper__c,Mellanox_Part_Number__c,Order_Number__c,OShip_Date__c,Warranty_Status__c,Approval__c,Reject_reason__c  FROM Serial_Number__c WHERE Id in :NewSNs1 ])
		{
			Serials.put(SN.Serial_upper__c,SN);
		} 
		for(sObject s : scope)
		{ 
			Asset_Internal__c internalAsset = (Asset_Internal__c)s;
			if(Serials.get(internalAsset.Serial_Number__c) != null)
			{
				Serials.get(internalAsset.Serial_Number__c).Mellanox_Part_Number__c = internalAsset.Ordered_Item__c;
				Serials.get(internalAsset.Serial_Number__c).Order_Number__c         = internalAsset.Order_Number__c ;
				Serials.get(internalAsset.Serial_Number__c).OShip_Date__c           = internalAsset.Ship_Date__c ;
				Serials.get(internalAsset.Serial_Number__c).Warranty_Status__c      = 'Under warranty';
				
				if(internalAsset.Ship_Date__c!=null && internalAsset.Ship_Date__c.addYears(1)<= system.today())
				{
					Serials.get(internalAsset.Serial_Number__c).Warranty_Status__c = 'Not under warranty';
					Serials.get(internalAsset.Serial_Number__c).Approval__c = 'Not Approved';
					Serials.get(internalAsset.Serial_Number__c).Reject_reason__c = 'Serial number is not under warranty';
				}
			}
		}
		system.debug('==> Serials: '+Serials);
		update Serials.values();
	}
	
	global void finish(Database.BatchableContext BC)
	{
		
	}
}