/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testUpd_Opportunity_on_Note {

    static testMethod void myUnitTest() {
    
    US_Territory_Map__c tm = new US_Territory_Map__c( SD_Region__c = 'EMEA', Region__c = '0000'); 
        tm.Sales_Director__c = '00550000000wuVp';
        insert tm;
   Account acc1 = new Account(name = 'test22',support_center__c = 'Global',is_oracle_parent__c = 'Yes',  type = 'OEM', RecordTypeID = '01250000000DP1n');
        insert acc1;

    Opportunity Op = new Opportunity(ownerid ='00550000000wuVp', primary_partner_N__c=acc1.id, Name = 'test opp', StageName = 'Discovery', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
    
    insert Op; 
    
     Note nt = new Note(Title = 'wwww');
     nt.ParentId = Op.id;
     insert nt;
    
    
    }
}