@isTest(seeAllData=true)
public with sharing class test_Web_Service_Handler {
	
	static testMethod void test_webService() {
		
        Contract2__c theContract2 = [SELECT Id, Name FROM Contract2__c order by createdDate desc limit 1];
                
        Web_Service_Handler.ContractRequest req = new Web_Service_Handler.ContractRequest();

        req.ContractId = theContract2.Name;
        
        system.debug('theContract2.Name : ' + theContract2.Name);
        system.debug('req.ContractId  in Test : ' + req.ContractId);
        req.paymentStatus = 'Approved';
        req.transactionId = 'EC123123TEST';
        
        Web_Service_Handler.updateContractUponRenewal(req);
	}

}