/******************************************************************************
 * Tests against the mlnxMobileHomeCtrl class
 * 
 * @Author: 	Abdul Sattar (Magnet 360)
 * @Date: 		09.09.2015
 * 
 * @Updates:
 *
 */
@isTest
private class mlnxMobileHomeCtrlTest {
    
    // Tests against mlnxMobileHomeCtrl.getRecordFromBarcode()
    private static testMethod void getRecordFromBarcodeTest() {
    	// Create test Account
    	Account act = MyMellanoxTestUtility.getAccount();

    	// Create a test contact
    	Contract2__c con = new Contract2__c( Account__c = act.Id
		    								,Contract_Start_Date__c = System.today()
		    								,Contract_Term_months__c = 12);
    	INSERT con;
		System.assert(con.Id != NULL, 'Unable to create test contract.');

		// Create test Asset
		Asset2__c a2 = new Asset2__c(Name ='Test Asset'
									,Asset_Type__c = 'Support'
									,part_number__c = 'weryu5SP'
									,Serial_Number__c = '123456876'
									,Contract2__c = con.Id );
      	INSERT a2;
		System.assert(a2.Id != NULL, 'Unable to create test asset.');

		// Run test
		Test.startTest();
			String serialFound = mlnxMobileHomeCtrl.getRecordFromBarcode(a2.Serial_Number__c);
			String serialNotFound = mlnxMobileHomeCtrl.getRecordFromBarcode('');
		Test.stopTest();

		// Perform asserts
		System.assertEquals(a2.Id, serialFound, 'Unable to search existing asset.');
		System.assert(serialNotFound.contains('Error: no record matching '), 'Unable to search existing serial. = ' + serialNotFound);
		
		// Code coverage for empty constructor.
		mlnxMobileHomeCtrl mmhc = new mlnxMobileHomeCtrl();
	}
}