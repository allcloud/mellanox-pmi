public with sharing class VF_Case_Approve_no_contract
{   
      
    public ID caseId;
    
    public VF_Case_Approve_no_contract (ApexPages.StandardController controller) 
         {    
             //id = Apexpages.currentPage().getParameters().get('Id');    
         }

    
    
    
    public PageReference Approve_case() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         CaseId = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         system.debug('id is : ' + caseId);
         //get the related RMA id from the case
         case cs = [Select ownerId, accountId,recordTypeId, Approved_By_Admin__c From case WHERE id =: caseId];
  
  
         if( cs.ownerId != ENV.AdminQueue)  
         {
           Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'The case can be approved only when in AdminQueue'));
           return  Apexpages.currentPage();
  
         } 

         if(cs.accountId == ENV.UnknownAccount ||cs.accountId == ENV.UnknownEMEAAccount || cs.accountId == ENV.UnknownUSAccount)

            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ' Please assign the contact to relevant Account (not Unknown)'));
              return  Apexpages.currentPage();
  
            } 

      
      
         database.DMLOptions dmo = new database.DMLOptions();    
         //dmo.AssignmentRuleHeader.AssignmentRuleId = '01Q50000000Ag0N'; 
         dmo.AssignmentRuleHeader.useDefaultRule = true;
         cs.Approved_By_Admin__c = TRUE;
         cs.recordTypeId = '01250000000DP5a';
         cs.setOptions(dmo);
                       
         update cs;

         return new Pagereference('/'+CaseId);
     }




static testMethod void Test_VF_RMA_Reject() 
    {
                //instance of object creator
                CLS_ObjectCreator obj = new CLS_ObjectCreator();

                Account acc = obj.createAccount();
                insert acc;
        
               Contact con = obj.CreateContact(acc);
               con.email = 'hgft56@ibm.com';
               insert con;
                
               Case cs = obj.Create_case(acc, con);
               insert cs;
                  
               cs.accountId = ENV.UnknownAccount;            
               update cs;                                
                                         
                                
                Test.setCurrentPageReference(new PageReference('Page.VF_Case_Approve_no_contract')); 
                System.currentPageReference().getParameters().put('id',cs.Id );       
                System.currentPageReference().getParameters().put('isTest','Testing' );  
              
               Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(cs);
                VF_Case_Approve_no_contract vf = new VF_Case_Approve_no_contract(teststandard);
                vf.approve_case();
    
    }

}