public with sharing class cls_trg_DuplicateRecordItem {
    
    
    // This method will update the field 'Account_Source__c' if the Record is associated to an Account and the Account 'Source'
    // has a value
    public void updateDupRecordItemsAccSource(list<DuplicateRecordItem> dupeRecItem_List) {
        
        map<Id, Account> accountIds2Accounts_Map = getRelatedAccountsMap(dupeRecItem_List);
         
        for (DuplicateRecordItem recDupe : dupeRecItem_List) {
         	
         	if (recDupe.RecordId != null  && String.valueOf(recDupe.RecordId).StartsWith('001')) {
         		
         		if (accountIds2Accounts_Map != null && accountIds2Accounts_Map.containsKey(recDupe.RecordId)) {
         			
         			recDupe.Account_Source__c = accountIds2Accounts_Map.get(recDupe.RecordId).Source__c;
         		}
         	}
        }
    }
    
    //This method returns a map of Related Account IDs and their Accounts
    private map<Id, Account> getRelatedAccountsMap(list<DuplicateRecordItem> dupeRecItem_List) {
        
        set<Id> relatedAccountsIds_Set = new set<Id>();
        map<Id, Account> accountIds2Accounts_Map;
        
        
        for (DuplicateRecordItem recDupe : dupeRecItem_List) {
            
            if (recDupe.RecordId != null  && String.valueOf(recDupe.RecordId).StartsWith('001')) {
                
                relatedAccountsIds_Set.add(recDupe.RecordId);
            }
        }
        
        if (!relatedAccountsIds_Set.isEmpty()) {
        	
        	accountIds2Accounts_Map = new map<Id, Account>([SELECT Id, Source__c FROM Account WHERE Source__c != null and Id IN : relatedAccountsIds_Set]);
        }
        
        return accountIds2Accounts_Map;
    }

}