public without sharing class cls_trg_RMA_Serial_Numbers {
	
	// This method will update the parent RMA's field 'RMA_Support_Project__c' when a new Serial Number record is created/updated and has an Asset related to it
	// It will take the value from the Asset's 'Project' field value to assign to the RMA
	public void updateRelatedRMAINSAProject(list<Serial_Number__c> newSerialNumbers_List,  map<Id, Serial_Number__c> oldSerialNumbers_Map) {
		//Added by Dmitry Rivlin for correct assign asset to seria_number__c
		map<String, Id> sites = getMellanoxSites();
		if (oldSerialNumbers_Map == null) findAndAssignAsset(newSerialNumbers_List);
		 //Mellanox-Yokneam 
//If RMA source- is MC=> update mellanox site= Mellanox US
		map<Id, RMA__c> RMAsIds2RMAs_Map = getMapOfRMAs(newSerialNumbers_List, oldSerialNumbers_Map);
		
		map<Id, Asset2__c> AssetsIds2Assets_Map = getMapOfRelatedAssets(newSerialNumbers_List, oldSerialNumbers_Map, RMAsIds2RMAs_Map);
		
		list<SObject> RMAs2Update_List = new list<SObject>();
		set<id> alreadyProcessed = new set<id>();
		
		for (Serial_Number__c newSerial : newSerialNumbers_List) {
			
			if (RMAsIds2RMAs_Map != null) {
				
				if (AssetsIds2Assets_Map != null) {
					system.debug(LoggingLevel.ERROR,'Asset ' + newSerial.Asset__c);
					if (AssetsIds2Assets_Map.containsKey(newSerial.Asset__c)) {
						
						system.debug(LoggingLevel.ERROR,'FOUND ASSET');

						newSerial.Project__c = AssetsIds2Assets_Map.get(newSerial.Asset__c).Project__c;
						RMA__c rma2Update = RMAsIds2RMAs_Map.get(newSerial.RMA__c);
						rma2Update.Mellanox_Site__c = sites.get(newSerial.Source__c);
						if (rma2Update.RMA_Support_Project__c == null &&  newSerial.Project__c != null) {
							rma2Update.RMA_Support_Project__c = newSerial.Project__c;
							if (!alreadyProcessed.contains(rma2Update.id)) RMAs2Update_List.add(rma2Update);
							else alreadyProcessed.add(rma2Update.id);
						}
						if (rma2Update.Case_Parent__c != null) {
							system.debug('inside 27');

							Case case2Update  = new Case(Id = rma2Update.Case_Parent__c);
							if (newSerial.Project__c != null)	case2Update.RMA_Support_Project__c = newSerial.Project__c;
							case2Update.RMA_Case_Source__c = newSerial.Source__c;

							if (!alreadyProcessed.contains(case2Update.id)) RMAs2Update_List.add(case2Update);
							else alreadyProcessed.add(case2Update.id);

							system.debug(LoggingLevel.ERROR,'case2Update ' + case2Update);
						}
					}
				}
			}
		}
		
		if (!RMAs2Update_List.isEmpty()) {
			
			update RMAs2Update_List;
		}
	}
	
	// This method returns a map of RMA Ids and their RMAs relating to the serial Numbers, Only where the RMA field 'RMA_Support_Project__c'
	// Has NO value
	private map<Id, RMA__c> getMapOfRMAs(list<Serial_Number__c> newSerialNumbers_List,  map<Id, Serial_Number__c> oldSerialNumbers_Map) {
		
		map<Id, RMA__c> RMAsIds2RMAs_Map;
		
		set<Id> RMAsIds_Set = new set<Id>();
		
		for (Serial_Number__c newSerial : newSerialNumbers_List) {
			
			if (oldSerialNumbers_Map != null) {
				
				if (newSerial.Asset__c != null) {
					
					if (newSerial.Asset__c != oldSerialNumbers_Map.get(newSerial.Id).Asset__c) {
						
						RMAsIds_Set.add(newSerial.RMA__c);
					}
				}
			}
			
			else {
				
				RMAsIds_Set.add(newSerial.RMA__c);
			}
		}
		
		if (!RMAsIds_Set.isEmpty()) {
			
			RMAsIds2RMAs_Map = new map<Id, RMA__c>([SELECT Id, Case_Parent__c, RMA_Support_Project__c FROM RMA__c WHERE Id IN : RMAsIds_Set]);
			system.debug('RMAsIds2RMAs_Map : ' + RMAsIds2RMAs_Map);
		} 
		
		return RMAsIds2RMAs_Map;
	}
	
	// This method returns a map of Assets Ids and their Assets relating to the newly created/updated Serial Numbers, only where the Asset's field
	// 'Project__c' has a value
	private map<Id, Asset2__c> getMapOfRelatedAssets(list<Serial_Number__c> newSerialNumbers_List,  map<Id, Serial_Number__c> oldSerialNumbers_Map,
													 map<Id, RMA__c> RMAsIds2RMAs_Map) {
		
		map<Id, Asset2__c> AssetsIds2Assets_Map;
		
		set<Id> assetsIds_Set = new set<Id>();
		
		for (Serial_Number__c newSerial : newSerialNumbers_List) {
			
			if (RMAsIds2RMAs_Map!= null && RMAsIds2RMAs_Map.containsKey(newSerial.RMA__c)) {
				
				assetsIds_Set.add(newSerial.Asset__c);
			}
		}
		
		if (!assetsIds_Set.isEmpty()) {
			
			AssetsIds2Assets_Map = new map<Id, Asset2__c>([SELECT Id, Project__c, Source__c FROM Asset2__c WHERE Id IN : assetsIds_Set]);
			system.debug('AssetsIds2Assets_Map : ' + AssetsIds2Assets_Map);
		}
		
		return AssetsIds2Assets_Map;
	}
	
	public static boolean findAndAssignAsset(list<Serial_Number__c> newSerialNumbers_List) {

		set <String> serialNumbers_Set = new set <String> ();
		//set <String> partNumbers_Set = new set <String> ();
		for (Serial_Number__c s : newSerialNumbers_List) {
			system.debug(LoggingLevel.ERROR,'Serial ' + s);
			serialNumbers_Set.add(s.Serial__c);
			//partNumbers_Set.add(s.Mellanox_Part_Number__c);
		}
		system.debug(LoggingLevel.ERROR,'Assets Names ' + serialNumbers_Set);
		list < Asset2__c > assets_List = [select Name, Part_Number__c, Source__c, Asset_Status__c from Asset2__c where Name in : serialNumbers_Set];
		String firstSource = null;
		boolean result;
		for (Serial_Number__c s : newSerialNumbers_List) {
			for (Asset2__c Item : assets_List) {
				if (Item.Name == s.Serial__c) {
					if (firstSource == null) firstSource = Item.Source__c;
					if (firstSource != Item.Source__c && result == null) result = true;
					s.Asset__c = Item.Id;
					s.Source__c = Item.Source__c;
					s.Part_Number__c = Item.Part_Number__c;
					s.Asset_Status_Text_Field__c = Item.Asset_Status__c;
					system.debug(LoggingLevel.ERROR,'FOUND Asset ' + s);
					break;
				}
			}
		}
		return result;
	}

	public static map<String, Id> getMellanoxSites(){
		map<String, Id> result = new map<String, Id>();
		for(MellanoxSite__c Item : [SELECT Id, Name from MellanoxSite__c LIMIT 10000]) {
			if (Item.Name == 'Mellanox-Yokneam') result.put('NPU', Item.Id); 
			if (Item.Name == 'Mellanox US') result.put('MC', Item.Id); 
		}
		return result;
	}

}