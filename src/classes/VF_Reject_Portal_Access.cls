public with sharing class VF_Reject_Portal_Access {
	
	// List of select Options to populate the 'Reject Reason' picklist with it's picklist values
	public list<selectOption> rejectRseason_Options_List{get;set;}
	public boolean displayWholePage {get;set;}
	
	public Contact theContact{get;set;}
	
	// Controller
	public VF_Reject_Portal_Access(apexPages.StandardController controller) {
		
		Contact con = (Contact)controller.getRecord();
		
		theContact = [SELECT Id, access_required__c, Reject_Reason__c FROM Contact WHERE Id =: con.Id];
		
		 list<User> user_List = [SELECT Id, Username FROM user WHERE ContactId =: TheContact.Id];
        
        if(!user_List.isEmpty()) {   
        	
        	ApexPages.Message PMsg = new ApexPages.Message(ApexPages.severity.error,'This Contact already has a potal user ');
            ApexPages.addMessage(PMsg);
            displayWholePage = false;
            return ;
        }
        
        else {
        	
        	displayWholePage = true;
        }
	}
	
	
	// This method returns a list of all the Contact's 'Reject_Reason__c'(picklist) values
    // To display on the VF
    public List<SelectOption> getrejectRseasons(){
        
        Schema.DescribeFieldResult rejectRseasonDescription = Contact.Reject_Reason__c.getDescribe();
        rejectRseason_Options_List = new list<SelectOption>();
        
        for (Schema.Picklistentry picklistEntry : rejectRseasonDescription.getPicklistValues())
        {
            rejectRseason_Options_List.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        
        return rejectRseason_Options_List;
    }  
    
    
    //This method will save the form and update the Contact
	public void saveForm() {
		
		theContact.access_required__c = null;
		
		Database.SaveResult  sr = Database.update(theContact, false);
         
        if(!sr.isSuccess()) {
         
    		list<Database.Error> error = sr.getErrors();
            String errorMessage = error[0].getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The operation could not be performed because of: '+ errorMessage));
        	return;
        }
		
		ApexPages.Message PMsg = new ApexPages.Message(ApexPages.severity.INFO,'This Contact has been updated ');
        ApexPages.addMessage(PMsg);
        return ;
	}
	

}