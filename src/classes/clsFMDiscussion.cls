public with sharing class clsFMDiscussion 
{
    public static String updateLatestFMDiscussiont(List <ID> lst_lastModifiedBy, Case c, List <FM_Discussion__c> lst_FMDiscussions)
    {
        String str_LastModifiedBy;
        String str_createdBy= 'Created by: ';
        Map <ID, User> map_idToUser = new Map <ID, User>();
        Map <ID, SelfServiceUser> map_idToSelfServiseUser = new Map <ID, SelfServiceUser>();
        String isPublic ='';
        String isExposedToFM ='';
        /*for(SelfServiceUser ssu:[select Id,name from SelfServiceUser where Id in :lst_lastModifiedBy])
        {
            map_idToSelfServiseUser.put(ssu.id,ssu);
        }*/
      
        /*for(User user:[Select Id, Name FROM User Where id in :lst_lastModifiedBy])
        {
            map_idToUser.put(user.id,user);
        }*/
        Integer i = 0;
        String allCommentsCurr = '';
        String allCommentsPrev = allCommentsCurr; 
        for(FM_Discussion__c fm: lst_FMDiscussions)
        {
            i++;
            if(i==11)
            {
                break;
            }
                       
            if(fm.Public__c)
                isPublic = 'Public';
            else
                isPublic = 'Private';
            if(fm.Exposed_To_FM__c)
                isExposedToFM = ' - Exposed To FM';
            else
                isExposedToFM = '';
            
            allCommentsPrev = allCommentsCurr;
            if(fm.LastModifiedBy.Name=='Evgeny Kagan' ||fm.LastModifiedBy.Name=='INNA GOLDIN')
            {
                allCommentsCurr = allCommentsCurr+'\n' + '-------------------------------------------------' + '\n '+ isPublic + ' Comment '+ isExposedToFM +' ' + 
                fm.CreatedDate + '\n' + fm.Discussion__c + '\n' ;
                if(allCommentsCurr.length()>32000)
                {
                    return allCommentsPrev;
                } 
            }
            else
            {
                allCommentsCurr = allCommentsCurr+'\n' + '-------------------------------------------------' + '\n '+ isPublic + ' Comment '+ isExposedToFM +' ' + 
                + ' ' + str_createdBy + fm.CreatedBy__c+' '+fm.CreatedDate+ '\n'+ fm.Discussion__c + '\n' ;
                if(allCommentsCurr.length()>32000)
                {
                    return allCommentsPrev;
                } 
            }
            if(allCommentsCurr.length()>32000)
            {
                return allCommentsPrev;
            } 
        }
        return allCommentsCurr;
    }
}