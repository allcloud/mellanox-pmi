@isTest
private class test_Task{

static testMethod void myTaskTest() {

CLS_ObjectCreator obj = new CLS_ObjectCreator();

 account acc = obj.createAccount();
 insert acc;
 
 Milestone1_Project__c proj = obj.CreateMPproject(acc.id); 
 insert proj;
 
 Milestone1_Milestone__c mlst = obj.CreateMilestone(Proj.id);
 insert mlst;
    
  Task ts1 = obj.CreateTask();
  ts1.whatId = mlst.id;
  ts1.projectId__c = string.ValueOf(proj.id).left(15);
  insert ts1;
  
  ts1.activityDate = system.today() + 6;
  ts1.status = 'Completed';
  update ts1;

}

}