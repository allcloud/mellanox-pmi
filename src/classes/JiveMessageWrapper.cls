public class JiveMessageWrapper {

        public String contentID;
        public Boolean isAnswer;
        public String replyComments;
        public String jiveURL;
        public String parentId;
        public attachment attachment;
        public String userEmail;
        public String userName;
   
    public JiveMessageWrapper(String jiveUrl,Boolean optionReply,String contentId,String caseComment,String parentId,String attId,String attName,String userEmail,String userName){
  
        this.contentID = contentId;
        this.isAnswer= optionReply;
        this.replyComments= caseComment;
        this.jiveURL = jiveUrl;
        this.parentId= parentId;
        this.attachment = new attachment(attId,attName);
        this.userEmail = userEmail;
        this.userName = userName;
    }
    
    public class attachment
    {
        public string attachmentID;
        public string fileName;
        
        public attachment(String attID,String fname)
        {
            this.attachmentID = attID;
            this.fileName = fname;
        }
    }
        
}