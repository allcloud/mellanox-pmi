public with sharing class VF_Upload_PO_To_Fund_Request {
		
	public Attachment invoiceAttachment {get;set;}
	public SFDC_MDF__c MDFRequest{get;set;}
	public SFDC_MDF_Claim__c MDFClaim{get;set;}
	public boolean displayPOField {get;set;}
	private Id MFDRequestId; 
	public boolean closePage {get;set;}  
	
	public VF_Upload_PO_To_Fund_Request(Apexpages.StandardController controller) {
		
		invoiceAttachment = new Attachment();
		
		MFDRequestId = Apexpages.currentPage().getParameters().get('Id');
		
		if (String.valueOf(MFDRequestId).startsWith('a4k')) {
			
			MDFRequest = new SFDC_MDF__c(Id = MFDRequestId);
			displayPOField = true;
		}
		
		else {
			
			MDFClaim = new SFDC_MDF_Claim__c(Id = MFDRequestId);
		}
	}
		
	 public Pagereference SaveInvoiceAttachment() {
       
	            invoiceAttachment.ParentId = MFDRequestId;
	            insert invoiceAttachment;
	            closePage = true;
	            
	            if (MDFRequest != null) {
		            MDFRequest.PO_Attachment_Id__c = invoiceAttachment.Id;
		            MDFRequest.PO_Attachment_Id__c = MDFRequest.PO_Attachment_Id__c.Substring(0,15);
		            MDFRequest.Status__c = 'Approved';
		            update MDFRequest;
	            }
	            
	            else {
	            	MDFClaim.Invoice_ID__c = invoiceAttachment.Id;
	            	MDFClaim.Invoice_ID__c = MDFClaim.Invoice_ID__c.Substring(0,15);
	           	 	update MDFClaim;
	            }
	            
	            String openerUrl = '/apex/VF_Contract_Renewal_PO_success_page';
	            PageReference nextPage = new PageReference(openerUrl);
	            nextPage.setRedirect(true);
	            return nextPage;
    }
}