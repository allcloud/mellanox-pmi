global class Loaner_Equipment_Exchange_Reminders implements Schedulable {
	global void execute(SchedulableContext SC) {
		List<Loaner_Request__c> loaner_updates = new List<Loaner_Request__c>();
		List<Loaner_Request__c> loaners = new List<Loaner_Request__c>([select id, Est_Delivery_Date_Equipment_Exchange__c,
																		Equipment_Exchange_Reminder_Date__c
																		from Loaner_Request__c 
																		where Loan_Type__c = 'Equipment Exchange' 
																		and Est_Delivery_Date_Equipment_Exchange__c != null
																		and Est_Delivery_Date_Equipment_Exchange__c <= :System.today()
																		and Recipe_Status__c != 'Closed - Received' and Recipe_Status__c != 'Closed - Not Received']);
		if (loaners == null || loaners.size() == 0)
			return;
		for (Loaner_Request__c s : loaners){
			if( Math.mod(System.today().daysBetween(s.Est_Delivery_Date_Equipment_Exchange__c),2) == 0) {
				s.Equipment_Exchange_Reminder_Date__c = System.today();
				loaner_updates.add(s);
			}
		}								
		if(loaner_updates.size() > 0)
			update loaner_updates;									
	}
}