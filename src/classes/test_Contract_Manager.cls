@isTest
private class test_Contract_Manager{

    static testMethod void myUnitTest() {
         
       CLS_ObjectCreator obj = new CLS_ObjectCreator();    
       Account acc = obj.createAccount();
       insert acc;
        
       Contract2__c c3 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
        
   
       insert c3;   
       
       Contract_manager__c cm = new Contract_manager__c(order__c = '123456', contract__c = c3.id);   
       insert cm;
       }  
   }