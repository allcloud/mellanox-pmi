@isTest (SeeAllData=true)
private 
class 
MPCAdvertisementControllerTest 
{

/*******************************************************************************
 * Test method for a successfull call to the MPCAdvertisementComponent component
 *
 */
    static 
    testMethod 
    void 
    testMPCAdvertisementController() 
    {
        System.debug( 'START: MPCAdvertisementControllerTest.testMPCAdvertisementController()' );
        
        MyMellanox_Advertisement__c acontent    = new MyMellanox_Advertisement__c();
        acontent.Image__c                 = 'Test Content';
        acontent.Display_Order__c              = '4 - Top PP';
        acontent.Active__c = true;
        acontent.Start_Date__c = date.today().addDays(-1);
        acontent.End_Date__c = date.today().addDays(1);
        insert acontent;
        
        MPCAdvertisementController ac = new MPCAdvertisementController();
        ac.display_order = acontent.Display_Order__c;
        System.assert(acontent.Active__c == true);
        System.assert(acontent.Start_Date__c <= date.today());
        System.assert(acontent.End_Date__c >= date.today());
        
        System.debug( 'END: MPCAdvertisementControllerTest.testMPCAdvertisementController()' );
    }

/*******************************************************************************
 * Test method for an invalid call the MPCAdvertisementComponent component
 *
 */ 
    static 
    testMethod 
    void 
    testMPCAdvertisementController_InvalidIdentifier() 
    {
        System.debug( 'START: MPCAdvertisementControllerTest.testMPCAdvertisementController_InvalidIdentifier()' );
        
        MyMellanox_Advertisement__c acontent    = new MyMellanox_Advertisement__c();
        acontent.Image__c                 = 'Test Content';
        acontent.Display_Order__c              = '4 - Top PP';
        acontent.Active__c = false;
        acontent.Start_Date__c = date.today().addDays(-1);
        acontent.End_Date__c = date.today().addDays(-1);
        insert acontent;
        
        MPCAdvertisementController ac = new MPCAdvertisementController();
        
                                                 // Try retrieving content with 
                                                 // an invalid Identifier
        ac.display_order = 'Invalid_Content';
        System.assert(acontent.Active__c == false);
        System.assert(acontent.Start_Date__c <= date.today());
        System.assert(acontent.End_Date__c <= date.today());
        
        System.debug( 'END: MPCAdvertisementControllerTest.testMPCAdvertisementController_InvalidIdentifier()' );
    }
    
}// /END Class