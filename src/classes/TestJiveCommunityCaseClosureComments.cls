@isTest
public class TestJiveCommunityCaseClosureComments {
    private static JiveCommunitySettings__c jiveSetting;
public static testMethod void testReplyToMain()
{
    initCustomSettings();
    PageReference pageRef = Page.jiveCommunityReply;
    Test.setCurrentPage(pageRef);
    Case c = new Case(Subject='testCaseSubject1',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName1',Jive_Author_Email__c='jiveAuthor1@cs.com',JiveURL__c='https://demojive-community1.jiveon.com/thread/1',Description='caseSummary1',Content_ID__c ='1190');
    insert c;
    ApexPages.currentPage().getParameters().put('id', c.id);
    ApexPages.currentPage().getParameters().put('msgID','1190');
    ApexPages.currentPage().getParameters().put('contenID', '1190');
    ApexPages.currentPage().getParameters().put('isReplyToMainThread','True');
    ApexPages.StandardController stdC = new ApexPages.StandardController(c);
    JiveCommunityCaseCloseComment jcc = new JiveCommunityCaseCloseComment(stdC);
    jcc.SetCommunityData();
    jcc.saveObject();
    
    Blob b = Blob.valueOf('Test Data');  
    Attachment attachment = new Attachment();  
    attachment.ParentId = c.id;  
    attachment.Name = 'Test Attachment for Parent';  
    attachment.Body = b;  
    insert(attachment);  
    
    
    jcc.deleteAttachment(attachment);
    
    
}
    public static testMethod void testReplyToChield()
{
    initCustomSettings();
    PageReference pageRef = Page.jiveCommunityReply;
    Test.setCurrentPage(pageRef);
    Case c = new Case(Subject='testCaseSubject1',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName1',Jive_Author_Email__c='jiveAuthor1@cs.com',JiveURL__c='https://demojive-community1.jiveon.com/thread/1',Description='caseSummary1',Content_ID__c ='1190');
    insert c;
    ApexPages.currentPage().getParameters().put('id', c.id);
    ApexPages.currentPage().getParameters().put('msgID','1190');
    ApexPages.currentPage().getParameters().put('contenID', '1190');
    ApexPages.currentPage().getParameters().put('isReplyToMainThread','False');
    ApexPages.StandardController stdC = new ApexPages.StandardController(c);
    JiveCommunityCaseCloseComment jcc = new JiveCommunityCaseCloseComment(stdC);
    jcc.SetCommunityData();
    jcc.saveObject();
    Blob b = Blob.valueOf('Test Data');  
    Attachment attachment = new Attachment();  
    attachment.ParentId = c.id;  
    attachment.Name = 'Test Attachment for Parent';  
    attachment.Body = b;  
    jcc.attachmentHelper = attachment;
    jcc.upload();
    jcc.deleteAttachment(attachment);
    
    
}
    public static void initCustomSettings() {
     
      // make sure custom setting record exists
      jiveSetting = JiveCommunitySettings__c.getValues('DefaultSetting');
        
        //put dummy values in the custom setting if jiveSetting == NULL
        if(jiveSetting == NULL) {
            jiveSetting = new JiveCommunitySettings__c();
            jiveSetting.Name = 'DefaultSetting';
            jiveSetting.Community_Base_URL__c ='http://cs-jive-sfdc-connector.com:5000';
            jiveSetting.Create_Contact__c =true;
            jiveSetting.Jive_URL__c='http://jivedemo-cs.jivecustom.com';
            insert jiveSetting;
       
        }
        else{
              // to cover user synchronization feature
             jiveSetting.Create_Contact__c =true;
             update jiveSetting;
        
        }
    }
}