public class cls_trg_Milestone {
    
    // This method will update the 'Next Milestone' on the project when a milestone 'Due Date' OR 'Complete' fields are changed
    // OR, when a new Milestone is created with the related Project's associated Milstone that has the earilest 'Due Date'
    public void updateTheProjectNextMilestoneOnMilestone(list<Milestone1_Milestone__c> newMilestone_List, Map<Id, Milestone1_Milestone__c> oldMilestone_Map, Boolean isUpdate)
    {
        // Set of related Projects IDs
        set<Id> projectsIds_Set = new set<Id>();
        // Map of projects Ids and Projects with theiir related Milstones
        map<id, Milestone1_Project__c> milestones2Projects_Map;
        // List of projects related milestones
        list<Milestone1_Milestone__c> projectsRelatedMilestones_List;
        
        for(Milestone1_Milestone__c milestone : newMilestone_List) {
            
            projectsIds_Set.add(milestone.Project__c);
        }
           
        if  (!projectsIds_Set.isEmpty() ) {
                         
            milestones2Projects_Map = new map<Id,Milestone1_Project__c>(DAL_Project.getProjectsByIds(projectsIds_Set));
            
            if ( !milestones2Projects_Map.isEmpty() ) {
                 
                for ( Milestone1_Milestone__c newMilestone : newMilestone_List ) {
                        
                    Boolean fieldsHaveChanged = false;
                    
                    if ( newMilestone.Deadline__c != null ) {
                                                  
                        if ( isUpdate )
                        {
                            Milestone1_Milestone__c oldMilestone = oldMilestone_Map.get(newMilestone.Id);
                                
                            if ( oldMilestone != null &&
                                 ( newMilestone.Deadline__c != oldMilestone.Deadline__c ) ||
                                 ( newMilestone.Complete__c != oldMilestone.Complete__c) ) {
                                        
                                fieldsHaveChanged = true;    
                            }
                        }
                        else
                        {
                            fieldsHaveChanged = true;
                        }
                        
                        if ( fieldsHaveChanged )
                        {     
                            Milestone1_Project__c theProject = milestones2Projects_Map.get(newMilestone.Project__c);
                                                        
                            map<Id, Milestone1_Milestone__c> map_Id2Milestone1Milestone = new map<Id, Milestone1_Milestone__c>(theProject.Project_Milestones__r);
                        
                            if ( !theProject.Project_Milestones__r.isEmpty() && !map_Id2Milestone1Milestone.isEmpty() ) {
                                
                                // if the current Milestone is Completed, we assign the Milestone that has the earliest Due Date and is not completed from the DataBase
                                if ( newMilestone.Complete__c ) {
                                    
                                    theProject.Next_Milestone__c = theProject.Project_Milestones__r[0].Id;
                                }
                                // if the current Milestone isn't Completed and it's the 'Next Milestone', we assign the Milestone 
                                // that has the earliest Due Date and is not completed from the DataBase ( which includes the the current Milestone as well)
                                
                                system.debug('newMilestone.Complete__c : '+ newMilestone.Complete__c);
                                system.debug('theProject.Next_Milestone__c : '+ theProject.Next_Milestone__c);
                                system.debug('newMilestone.Id : '+ newMilestone.Id);
                                //system.debug('map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c).Id : '+ map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c).Id);
                                
                                
                                
                                 if ( !newMilestone.Complete__c && map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c) != null &&  
                                 map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c).Id != null &&
                                          null != theProject.Next_Milestone__c &&
                                          newMilestone.Id == map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c).Id ) {
                                
                                    theProject.Next_Milestone__c = theProject.Project_Milestones__r[0].Id;                                          
                                }
                                // if the current Milestone isn't Completed and the 'Next Milestone' isn't null
                                // and is different than the current Milestone, we check if the current Milestone
                                // Due date is eariler than the 'Next Milestone' Due Date
                                else if ( !newMilestone.Complete__c &&
                                          null != theProject.Next_Milestone__c && newMilestone.Id != null && map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c) != null &&
                                          newMilestone.Id != map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c).Id &&
                                          newMilestone.Deadline__c < map_Id2Milestone1Milestone.get(theProject.Next_Milestone__c).Deadline__c ) {
                                    
                                    theProject.Next_Milestone__c = newMilestone.Id;
                                }
                                // if the current Milestone isn't Completed and the 'Next Milestone' is null
                                // We assign the Milestone that has the earliest Due Date and is not completed 
                                // from the DataBase ( which includes the the current Milestone as well)
                                else if ( !newMilestone.Complete__c &&
                                          null == theProject.Next_Milestone__c ) {
                                    
                                    theProject.Next_Milestone__c = theProject.Project_Milestones__r[0].Id;      
                                }
                            }
                            // if all Milestones are completed we turn the 'Next Milestone' to Null
                            else {
                                
                                theProject.Next_Milestone__c = null;
                            }
                        }
                    }
                }
                
                update milestones2Projects_Map.values();
            }
        }
    }
    
    // This method will caculate the created/updated/deleted Milesotne Parent Project 'Done%'
    // it will go over all of the Project's related Milestones and will calculate an everage penidng on their 'Cumulative_Done__c' and the number of those records
    public void calculatAllRealtedMilestonesCumulativeDone(list<Milestone1_Milestone__c> newMilestone_List, list<Milestone1_Milestone__c> oldMilestove_List) {
    	
    	map<Id, Milestone1_Project__c> parentProjects_Map = fillUpParentProjectsMap(newMilestone_List, oldMilestove_List);
    	
    	if (!parentProjects_Map.values().isEmpty()) {
    		
    		for (Milestone1_Project__c currentProject : parentProjects_Map.values()) {
    			
    			if (!currentProject.Project_Milestones__r.isEmpty()) {
    				
    				decimal totalCumulativeDone = 0;
    				decimal totalCumulativeWeight = 0;
    				
    				for (Milestone1_Milestone__c currentMilestone : currentProject.Project_Milestones__r) {
    					
    					if (currentMilestone.Cumulative_Done__c != null) {
    						totalCumulativeDone += currentMilestone.Cumulative_Done__c;
    					}
    					
    					else {
    						
    					}
    					
    					if (currentMilestone.Weight__c != null) {
    						totalCumulativeWeight += decimal.valueOf(currentMilestone.Weight__c);
    					}
    					
    					else {
    						
    						currentMilestone.Weight__c = '1';
    						totalCumulativeWeight += decimal.valueOf(currentMilestone.Weight__c);
    					}
    				}
    				
    				currentProject.Done__c = totalCumulativeDone / totalCumulativeWeight;
    			}
    		}
    		
    		update parentProjects_Map.values();
    	}
    }
    
    // This method will return a map of the created/updated milestones parent Projects with all of their related Milestones
     private map<Id, Milestone1_Project__c> fillUpParentProjectsMap(list<Milestone1_Milestone__c> newMilestone_List, list<Milestone1_Milestone__c> oldMilestove_List) {
    	
    	set<Id> parentProjectIds_Set = new set<Id>();
    	map<Id, Milestone1_Project__c> parentProjects_Map;
    	if (newMilestone_List != null) {
    		
    		for (Milestone1_Milestone__c newNilestone : newMilestone_List) {
    			
    			parentProjectIds_Set.add(newNilestone.Project__c);
    		}
    	}
    	
    	else {
    		
    		for (Milestone1_Milestone__c oldNilestone : oldMilestove_List) {
    			
    			parentProjectIds_Set.add(oldNilestone.Project__c);
    		}
    	}
    	
    	if (!parentProjectIds_Set.isEmpty()) {	
    		
    		parentProjects_Map = new map<Id, Milestone1_Project__c>([SELECT Id, Done__c, (SELECT Id, Weight__c, Cumulative_Done__c FROM Project_Milestones__r) 
    																 FROM  Milestone1_Project__c WHERE Id IN : parentProjectIds_Set]);
    	}
    	return parentProjects_Map;
    }
}