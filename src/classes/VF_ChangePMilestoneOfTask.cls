public with sharing class VF_ChangePMilestoneOfTask 
{
    public Id theTaskId {get;set;}
    public Id theProjectId {get;set;}
    public Id theSelectedMilesoneId {get;set;}
    public list<Milestone1_Milestone__c> lst_milestone {get;set;} 
    
    public VF_ChangePMilestoneOfTask(ApexPages.StandardController stdController)
    {
        theTaskId = ApexPages.currentPage().getParameters().get('TaskId');
        theProjectId = ApexPages.currentPage().getParameters().get('ProId');
        
        system.debug('==>theTaskId '+theTaskId);
        system.debug('==>theProjectId '+theProjectId);
        
        
        if(theProjectId != null )
        {
            lst_milestone = new list<Milestone1_Milestone__c>();
            lst_milestone = [Select m.Project__c, m.Name, m.Id From Milestone1_Milestone__c m where Project__c =: theProjectId ];
        }
            
        system.debug('==>the milestone list '+lst_milestone);
        
    } 
    
    public void changeMileston()
    {
        system.debug('==>theSelectedMilesoneId '+theSelectedMilesoneId);
        if(theSelectedMilesoneId != null)
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'The Milestone Was Selected Successfully'));
    }
    
    public void SaveRelatedTo()
    {
        if(theTaskId != null && theSelectedMilesoneId != null)
        {
            try
            {
                system.debug('==>theTaskId: '+theTaskId);
                system.debug('==>theSelectedMilesoneId: '+theSelectedMilesoneId);
                update new task(id=theTaskId ,whatId=theSelectedMilesoneId);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'The Task Related Milestone Was Changed Successfully'));
            }
            catch(exception e)
            {
                system.debug('==>e: '+e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'An Error Has Occured : '+e));
            }
        }
        
    }
}