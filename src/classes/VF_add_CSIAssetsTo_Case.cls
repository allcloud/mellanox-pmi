public class VF_add_CSIAssetsTo_Case {
	
	public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    private Id caseId {get;set;}
    public List<csiAssetBean> csiList {get;set;}
    public List<csiAssetBean> csiListReplicated {get;set;}
    public string chosenOEM {get;set;}
    public CSI_Asset__c csiAsset {get;set;}
    public list<selectOption> oem_List {get;set;}
    public string OEM {get;set;}
    public string PCIe_gen {get;set;}
    public string Model {get;set;}
    public string location {get;set;}
    public string BIOS {get;set;}
    public string OS_1 {get;set;}
    public string HCA_1 {get;set;}
    public string Kernel_1 {get;set;}
    public String HostName {get;set;}
    public String CPU {get;set;}
    public String TYPE {get;set;}
    private ApexPages.StandardController controller;
    
    public boolean filterpressed {get;set;}
    private boolean processRanOnce {get;set;}
    
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
    
    
    // INNER CLASS
    
    public class csiAssetBean {
    	
    	public CSI_Asset__c theAsset {get;set;}
    	public boolean csiChosen {get;set;}
    }
    
    public VF_add_CSIAssetsTo_Case (ApexPages.StandardController controller) {
    	
    	this.controller = controller;
    	caseId = controller.getId();
    	csiListReplicated = new list<csiAssetBean>();
    	csiAsset = new CSI_Asset__c();
    	filterpressed = false;
    	processRanOnce = false;
    	
    }
    
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = 10;
				setTypeDefaultValue();
				
                string queryString = 'Select Name, Owner.Name, CPU__c, BIOS__c, Asset_origin__c, Comments__c, CSI_Ticket_Number__c, Hostname__c, ILO_IP__c, ILO_MAC__c, Kernel_1__c, PCIe_gen__c,' +  
                 ' HCA_PN_1__c, HCA_PN_2__c, HCA_PN_3__c,' + 
                 + ' Kernel_2__c, Location__c, Model__c, Net_IP__c, Net_MAC__c, OEM__c, OS_1__c, OS_2__c, OS_3__c, Serial_Number__c,' + 
                 + ' Site__c, Type__c from CSI_Asset__c WHERE CSI_Ticket_Number__c = null and Assign_Pool__c = true and Type__c LIKE \'%' + TYPE + '%\'' + 
                 + ' order by createdDate';
                 
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();                 
            }
            return setCon;
        }set;
    }
    
    public void setTypeDefaultValue (){
		Schema.DescribeFieldResult fieldResult = CSI_Asset__c.Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple) {
		    if(f.getLabel() == 'Server'){
		        TYPE = 'Server';
		    }
		}
    }
    
    public List<csiAssetBean> getCSIAssets() {   
       
        csiList = new List<csiAssetBean>();  
       
        for(CSI_Asset__c csiAsset : (List<CSI_Asset__c>)setCon.getRecords()) {
        
        	csiAssetBean theBean = new csiAssetBean();
        	theBean.theAsset = csiAsset;
        	theBean.csiChosen = false;
        	
            csiList.add(theBean); 
        }
        
       	if (filterpressed) {	
       		
       		doFilter();
       	}
        
        	
        if (!processRanOnce) {	
        	csiListReplicated = csiList.Clone();
        	processRanOnce = true;
        }
        return csiList;
    }
    
    public pageReference refresh() {
        
        pagereference thePage = new PageReference('/apex/VF_add_CSIAssetsTo_Case?Id=' + caseId);
        thePage.setRedirect(true);
        return thePage;
    }
    
    public PageReference assignAssets() {
    	
    	list<CSI_Asset__c> csiAssets2Update_List = new list<CSI_Asset__c>();
    	
    	for(csiAssetBean theBean : csiList) {
    	
    		system.debug('theBean csiChosen : ' + theBean.csiChosen ) ;
    		if (theBean.csiChosen) {
    			
    			CSI_Asset__c newCSIAsset = new CSI_Asset__c(Id = theBean.theAsset.Id);
    			newCSIAsset.CSI_Ticket_Number__c = caseId;
    			csiAssets2Update_List.add(newCSIAsset);
    		}
    	}
    	
    	if (!csiAssets2Update_List.isEmpty()) {
    		
    		update csiAssets2Update_List;
	        return controller.cancel();
    		
    	}
    	
    	else {
    		
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No CSI Assets have been selected'));
    	}
    	return null;
    }
    
    public list<selectOption> getTheOem_List() {
    	
    	list<SelectOption> oem_List = new list<SelectOption>();
    	Schema.DescribeFieldResult fieldResult = CSI_Asset__c.OEM__c.getDescribe();
 	    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        oem_List.add(new SelectOption('-- None --', '-- None --'));
	    for( Schema.PicklistEntry f : ple) {
	   
	        oem_List.add(new SelectOption(f.getLabel(), f.getValue()));
	    } 
	    
	    return oem_List;  
    }
    
    
     public list<selectOption> getTheType_List() {
    	
    	list<SelectOption> type_List = new list<SelectOption>();
    	Schema.DescribeFieldResult fieldResult = CSI_Asset__c.Type__c.getDescribe();
 	    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        type_List.add(new SelectOption('-- None --', '-- None --'));
	    for( Schema.PicklistEntry f : ple) {
	   
	        type_List.add(new SelectOption(f.getLabel(), f.getValue()));
	    } 
	    
	    return type_List;  
    }
    
    
    
    public list<selectOption> getThePCIe_gen_List() {
    	
    	list<SelectOption> PCIe_gen_List = new list<SelectOption>();
    	Schema.DescribeFieldResult fieldResult = CSI_Asset__c.PCIe_gen__c.getDescribe();
 	    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        PCIe_gen_List.add(new SelectOption('-- None --', '-- None --'));
	    for( Schema.PicklistEntry f : ple) {
	   
	        PCIe_gen_List.add(new SelectOption(f.getLabel(), f.getValue()));
	    } 
	    
	    return PCIe_gen_List;  
    }
    
    public void doFilter() {
    	
       filterpressed = true;
       csiList.clear();
       system.debug('csiList after clear : ' + csiList);
       system.debug('OEM : ' + OEM);
       system.debug('Model : ' + Model);
       system.debug('location : ' + location);
       system.debug('BIOS : ' + BIOS);
       system.debug('OS_1 : ' + OS_1);
       system.debug('PCIe_gen : ' + PCIe_gen);
       system.debug('Kernel_1 : ' + Kernel_1);
       
       OEM = (OEM == null || OEM.EqualsIgnoreCase('')) ? '' : OEM;
       PCIe_gen = (PCIe_gen == null || PCIe_gen.EqualsIgnoreCase('')) ? '' : PCIe_gen;
       Model = (Model == null || Model.EqualsIgnoreCase('')) ? '' : Model;
       location = (location == null || location.EqualsIgnoreCase('')) ? '' : location;
       BIOS = (BIOS == null || BIOS.EqualsIgnoreCase('')) ? '' : BIOS;
       OS_1 = (OS_1 == null || OS_1.EqualsIgnoreCase('')) ? '' : OS_1;
       HCA_1 = (HCA_1 == null || HCA_1.EqualsIgnoreCase('')) ? '' : HCA_1;
       Kernel_1 = (Kernel_1 == null || Kernel_1.EqualsIgnoreCase('')) ? '' : Kernel_1;
       HostName = (HostName == null || HostName.EqualsIgnoreCase('')) ? '' : HostName;
       CPU = (CPU == null || CPU.EqualsIgnoreCase('')) ? '' : CPU;
       TYPE = (TYPE == null || TYPE.EqualsIgnoreCase('')) ? '' : TYPE;
        
       
       system.debug('  ' + OEM);
       system.debug('Model after : ' + Model);
       system.debug('location after : ' + location);
       system.debug('BIOS after : ' + BIOS);
       system.debug('OS_1 after : ' + OS_1);
       system.debug('PCIe_gen after : ' + PCIe_gen);
       system.debug('Kernel_1 after : ' + Kernel_1);
       
        String queryString = 'Select Name, Owner.Name, BIOS__c, Comments__c, CPU__c, CSI_Ticket_Number__c, Hostname__c, ILO_IP__c, ILO_MAC__c, Kernel_1__c, Asset_origin__c, PCIe_gen__c, ' +
    	          'HCA_PN_1__c, HCA_PN_2__c, HCA_PN_3__c, Kernel_2__c, Location__c, Model__c, Net_IP__c, Net_MAC__c, OEM__c, OS_1__c, OS_2__c, OS_3__c, Serial_Number__c, Site__c, Type__c from CSI_Asset__c ' +
    	          'WHERE CSI_Ticket_Number__c = null and Assign_Pool__c = true';
    	          
       
       if (OEM != null && !OEM.contains('None') ) {
	       String OEMval = '\'%' + OEM + '%\'';
	       queryString +=  ' and OEM__c LIKE ' + OEMval;
       }
	       
	   if (Model != null && !Model.EqualsIgnoreCase('')) { 
	       String Modelval = '\'%' + Model + '%\'';
	       queryString +=  ' and Model__c LIKE ' + Modelval;
	   }
	       
	   if (location != null && !location.EqualsIgnoreCase('')) {    
	       String locationval = '\'%' + location + '%\'';
	       queryString +=  ' and Location__c LIKE ' + locationval;
	   }
	       
	   if (BIOS != null && !BIOS.EqualsIgnoreCase('')) {
	       String BIOSval = '\'%' + BIOS + '%\'';
	       queryString +=  ' and BIOS__c LIKE ' + BIOSval;
	   }
	   
	   if (PCIe_gen != null && !PCIe_gen.contains('None')) {    
	       String PCIe_genval = '\'%' + PCIe_gen + '%\'';
	       queryString +=  ' and PCIe_gen__c LIKE ' + PCIe_genval;
	   }
	   
	   if (OS_1 != null && !OS_1.EqualsIgnoreCase('')) {
	       String OS_1val = '\'%' + OS_1 + '%\'';
	       queryString +=  ' and (OS_1__c LIKE ' + OS_1val;
	       queryString +=  ' or OS_2__c LIKE ' + OS_1val;
	       queryString +=  ' or OS_3__c LIKE ' + OS_1val + ')';
	   }
	   
	   if (Kernel_1 != null && !Kernel_1.EqualsIgnoreCase('')) {    
	       String Kernel_1val = '\'%' + Kernel_1 + '%\'';
	       queryString +=  ' and (Kernel_1__c LIKE ' + Kernel_1val;
	       queryString +=  ' or Kernel_2__c LIKE ' + Kernel_1val + ')';
	   }
	   
	   if (HCA_1 != null && !HCA_1.EqualsIgnoreCase('')) {
	       String HCA_1val = '\'%' + HCA_1 + '%\'';
	       queryString +=  ' and (HCA_PN_1__c LIKE ' + HCA_1val;
	       queryString +=  ' or HCA_PN_2__c LIKE ' + HCA_1val;
	       queryString +=  ' or HCA_PN_3__c LIKE ' + HCA_1val + ')';
	   }
	   
	   if (HostName != null && !HostName.EqualsIgnoreCase('')) { 
	       String HostNameval = '\'%' + HostName + '%\'';
	       queryString +=  ' and Hostname__c LIKE ' + HostNameval;
	   }
	   
	   if (CPU != null && !CPU.EqualsIgnoreCase('')) { 
	       String CPUNameval = '\'%' + CPU + '%\'';
	       queryString +=  ' and CPU__c LIKE ' + CPUNameval;
	   }
	   
	   if ( TYPE != null && !TYPE.contains('None') ) { 
	       String TYPENameval = '\'%' + TYPE + '%\'';
	       queryString +=  ' and Type__c LIKE ' + TYPENameval;
	   }
       
       system.debug('queryString : ' + queryString);
      list<CSI_Asset__c> filteredCsiAssets_List = database.Query(queryString);
    
    	
      /* 
    	list<CSI_Asset__c> filteredCsiAssets_List = [Select Name, Owner.Name, BIOS__c, Comments__c, CSI_Ticket_Number__c, Hostname__c, ILO_IP__c, ILO_MAC__c, Kernel_1__c, Asset_origin__c, PCIe_gen__c,
    	          HCA_PN_1__c, HCA_PN_2__c, HCA_PN_3__c,  
                  Kernel_2__c, Location__c, Model__c, Net_IP__c, Net_MAC__c, OEM__c, OS_1__c, OS_2__c, OS_3__c, Serial_Number__c, Site__c, Type__c from CSI_Asset__c WHERE CSI_Ticket_Number__c = null
                  and (OEM__c like : '%' + OEM + '%'  and Model__c like : '%' +Model + '%' and Location__c like : '%' + location + '%' and BIOS__c like : '%' +BIOS + '%' and PCIe_gen__c =: PCIe_gen 
                  and OS_1__c like : '%' +OS_1 + '%' and OS_2__c like : '%' + OS_1 + '%'  and OS_3__c like : '%' + OS_1 + '%' and Kernel_1__c like : '%' +Kernel_1 + '%' and Kernel_2__c like : '%' +Kernel_1 + '%' 
                  and HCA_PN_1__c like : '%' + HCA_1 + '%' and HCA_PN_2__c like : '%' + HCA_1 + '%' and HCA_PN_3__c like : '%' + HCA_1 + '%')];
    	
      */	
    	
    	noOfRecords = filteredCsiAssets_List.size();
    	for (CSI_Asset__c csiAsset : filteredCsiAssets_List) {
    		
    		csiAssetBean theBean = new csiAssetBean();
        	theBean.theAsset = csiAsset;
        	theBean.csiChosen = false;
            csiList.add(theBean); 
    	}
    	
       OEM = (OEM == null || OEM.EqualsIgnoreCase('-+-+')) ? '' : OEM;
       PCIe_gen = (PCIe_gen == null || PCIe_gen.EqualsIgnoreCase('-+-+')) ? '' : PCIe_gen;
       Model = (Model == null || Model.EqualsIgnoreCase('-+-+')) ? '' : Model;
       location = (location == null || location.EqualsIgnoreCase('-+-+')) ? '' : location;
       BIOS = (BIOS == null || BIOS.EqualsIgnoreCase('-+-+')) ? '' : BIOS;
       OS_1 = (OS_1 == null || OS_1.EqualsIgnoreCase('-+-+')) ? '' : OS_1;
       HCA_1 = (HCA_1 == null || HCA_1.EqualsIgnoreCase('-+-+')) ? '' : HCA_1;
       Kernel_1 = (Kernel_1 == null || Kernel_1.EqualsIgnoreCase('-+-+')) ? '' : Kernel_1;
       HostName = (HostName == null || HostName.EqualsIgnoreCase('-')) ? '' : HostName;
       CPU = (CPU == null || CPU.EqualsIgnoreCase('-')) ? '' : CPU;
       TYPE = (TYPE == null || TYPE.EqualsIgnoreCase('-')) ? '' : TYPE;
       
    }

}