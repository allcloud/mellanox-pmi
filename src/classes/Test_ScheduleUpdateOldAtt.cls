@isTest
private class Test_ScheduleUpdateOldAtt {

    static testMethod void myUnitTest() 
    {
    	Case theCase = new Case();
    	insert theCase;
    	Case theCase2 = new Case(); 
    	//theCase2.Next_Attachment_Integration_Number__c =8 ;
    	insert theCase2;
    	Attachment att = new Attachment();
    	att.ParentId = theCase.Id;
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	att.Body = bodyBlob;
    	att.Name = 'tttt';
    	insert att;
    	AttachmentIntegration__c attIn = new AttachmentIntegration__c();
    	attIn.Case_Id__c = theCase2.Id;
    	attIn.Attachment_Id__c = att.Id;
    	insert attIn;
    	
        // TO DO: implement unit test
        Test.startTest();
        ScheduleUpdateOldAtt old = new ScheduleUpdateOldAtt();
        ID batchprocessid = Database.executeBatch(old);
        String jobId = System.schedule('testScheduledApex','0 0 0 3 9 ? 2022',new ScheduleUpdateOldAtt());       
        Test.stopTest();
    }
}