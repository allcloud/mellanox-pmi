public with sharing class SupportNewsTickerController {
	public List<Customer_News__c> custNews {get; set;}
	
	public SupportNewsTickerController() {
		custNews = 
			[select Id, Name, Label__c, Link__c from Customer_News__c where Active__c = true and Product_Detail__c = null order by CreatedDate desc];
	}
}