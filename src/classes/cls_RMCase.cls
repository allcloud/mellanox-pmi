public with sharing class cls_RMCase {
    
    public void createFMDiscussionOnRmCaseIsClosed(list<RmCase__c> newRMCases_List, map<Id, RmCase__c> oldRMCases_Map) {
        
        list<FM_Discussion__c> FMDiscussions2Create_List = new list<FM_Discussion__c>();
        
        for (RmCase__c newRMCase : newRMCases_List) {
               
            if (newRMCase.Is_Closed__c && newRMCase.Is_Closed__c != oldRMCases_Map.get(newRMCase.Id).Is_Closed__c) {
                
                FM_Discussion__c newDiscussion = new FM_Discussion__c();
                newDiscussion.Case__c = newRMCase.sfcase__c;
                newDiscussion.Discussion__c = 'The related RM case #' + newRMCase.Name + ' was closed/rejected';
                FMDiscussions2Create_List.add(newDiscussion);
            }
        }
        
        if (!FMDiscussions2Create_List.isEmpty()) {
            
            try {
                insert FMDiscussions2Create_List;
            }
            
            catch(Exception e) {
                
                newRMCases_List[0].addError('Could Not create new FM Discussion : ' + e.getCause() + ', ' + e.getMessage()); 
            }
        }
    }

}