@isTest
public with sharing class test_VF_add_CSIAssetsTo_Case {
	
	static testMethod void test_add_CSIAssetsTo_Case() {
		
		Case c = new Case();
            
        insert c;
        
        CSI_Asset__c newCSIAsset = new CSI_Asset__c();
  	    newCSIAsset.Type__c = 'Server';
  	    newCSIAsset.Serial_Number__c = '123';
        newCSIAsset.Hostname__c = 'Test Host';
        
		insert newCSIAsset;
		
		Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(c);
		VF_add_CSIAssetsTo_Case controller = new VF_add_CSIAssetsTo_Case(teststandard);
		
		controller.csiList = new list<VF_add_CSIAssetsTo_Case.csiAssetBean>();
		controller.getCSIAssets();
		controller.refresh();
		controller.doFilter();
		controller.getCSIAssets();
		controller.assignAssets();
		controller.getTheOem_List();
	}
}