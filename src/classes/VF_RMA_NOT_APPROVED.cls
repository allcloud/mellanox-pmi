global class VF_RMA_NOT_APPROVED {
    
    public String caseId {get;set;}
    public Case theCase{get;set;}
    public list<SerialBean> serialBeans_List {get;set;}
     
    public class SerialBean {
        
        public String partNumber {get;set;}
        public decimal Quantity {get;set;}
    }
    
    public void getPopulateList() {
        
        serialBeans_List = new list<SerialBean>();
        theCase = [SELECT Id, RMA_Request__c, ContactId, Contact.Name, AccountId, Account.Name, Contact.Phone, RMA_Request__r.Equipment_Location__c, CaseNumber FROM Case
                   WHERE Id =: caseId];
                   
        list<Serial_Number__c> serials_list = [SELECT Id, Part_Number__c FROM Serial_Number__c WHERE RMA__c =: theCase.RMA_Request__c];
        
        map<String, decimal> pertnumbers2Quantities_Map = new map<String, decimal>();
        
        
        for (Serial_Number__c serial : serials_list) {
            
            if (pertnumbers2Quantities_Map.containsKey(serial.Part_Number__c)) {
                
                decimal theQuantity = pertnumbers2Quantities_Map.get(serial.Part_Number__c);
                theQuantity ++;
                pertnumbers2Quantities_Map.put(serial.Part_Number__c, theQuantity);
            }
            
            else {
                
                pertnumbers2Quantities_Map.put(serial.Part_Number__c, 1);
            }
        }
        
        
        if (!pertnumbers2Quantities_Map.values().isEmpty()) {
          
            for (String thePartNumber : pertnumbers2Quantities_Map.keyset()) {
                
                SerialBean serailBean = new SerialBean();
                serailBean.partNumber = thePartNumber;
                serailBean.Quantity = pertnumbers2Quantities_Map.get(thePartNumber);
                serialBeans_List.add(serailBean);
            }
        }      
    }

}