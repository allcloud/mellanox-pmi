/**
    @Author Elad Kaplan
    @date 11.12.2014
    
    @Description This is a controller Class for the Visual Force Component 'VF_CaseStatusChangedEmail'
 */
global class VF_CaseStatusChangedEmailTemplate {    
	
  public String caseId {get;set;}
  public Case theCase{get;set;}
  public boolean displayStateclosed {get;set;}
  public boolean displayStateAssignedAE {get;set;}
  public boolean displayStateAssigned {get;set;}
  public boolean displayStateOpen {get;set;}
  public boolean displayStateOnHold {get;set;}
  public boolean displayStateWaitCustApproval {get;set;}
  public boolean displayStateWaitImplementation {get;set;}
  public boolean displayStateWaitForRelease {get;set;}
  public boolean displayStateWaitForCustInfo {get;set;}
  public boolean displayPriorityHigh {get;set;}
  public boolean displayPriorityLow {get;set;}
  public boolean displayPriorityFatal {get;set;}
  public boolean displayPriorityMedium {get;set;}
  public boolean displayProject {get;set;}
  public Comment lastComment {get;set;}
  public String CaseUrl {get;set;}
  public String aeEngineerMellGroup {get;set;}
  public String assigneeMellGroup {get;set;}
  public boolean statusPushedToPM {get;set;}
  public boolean statusAssignedToFAE {get;set;}
  public boolean statusChangedToWaitCustomerInfo {get;set;}
  public boolean statusChangedToWaitForRelease {get;set;}
  public boolean statusChangedToOnHold {get;set;}
  public boolean statusChangedToWaitForImplementation {get;set;}
  
  public list<Comment> comment_List {get;set;}
  
  
  public class Comment {
    
    public FM_Discussion__c theComment {get;set;}
    public Case theCaseComment {get;set;}
    public String isPrivate {get;set;}
    public boolean displayPrivateComment{get;set;}
    public String creatdBy {get;set;}
    public String createdDate {get;set;}
    public String indexNumber {get;set;}
    public Decimal commentNumber {get;set;}
    public String theDiscussion {get;set;}
  }
  
  public void getAllBooleansSet() {
    
    aeEngineerMellGroup = '';
    assigneeMellGroup   = '';
    
    theCase = [SELECT Id, Subject, AccountName__c, ContactName__c, Project__r.Name, Status, Assignee__r.Name, Project__c, Assignee__r.Mellanox_Group__c, 
          Related_Project__r.Name, Related_Project__c, CaseNumber, Contact.Name, Description, AE_Engineer__r.Mellanox_Group__c,    
          AE_Engineer__r.Name, Owner.Name, State__c, priority FROM Case WHERE Id =: caseId]; 
           
    displayStateclosed                   = false;
    displayStateAssignedAE               = false;
    displayStateAssigned                 = false;
    displayStateWaitForRelease           = false;
    displayStateWaitForCustInfo          = false;
    displayPriorityHigh                  = false;
    displayPriorityLow                   = false;
    displayPriorityMedium                = false;
    displayPriorityFatal                 = false;
    displayProject                       = false;
    displayStateOpen                     = false;
    displayStateOnHold                   = false;
    displayStateWaitCustApproval         = false;
    displayStateWaitImplementation       = false;
    statusPushedToPM                     = false;
    statusAssignedToFAE                  = false;
    statusChangedToWaitCustomerInfo      = false;
    statusChangedToWaitForRelease        = false;
    statusChangedToOnHold                = false;
    statusChangedToWaitForImplementation = false;
    
    if (theCase.Id != null) {
        
        
        CaseUrl = 'https://mellanox.my.salesforce.com/' + theCase.Id; 
        
        if (theCase.Assignee__r.Mellanox_Group__c != null) {
          
          assigneeMellGroup = ' (' + theCase.Assignee__r.Mellanox_Group__c + ')';
        }
        
        if (theCase.AE_Engineer__r.Mellanox_Group__c != null) {
          
          aeEngineerMellGroup = ' (' + theCase.AE_Engineer__r.Mellanox_Group__c + ')';
        }  
        
        list<FM_Discussion__c> caseComments_List = [SELECT Id, Index_Number__c, Public__c, CreatedBy__c, Discussion__c,
                 CreatedDate FROM FM_Discussion__c WHERE Case__c =: theCase.Id order by createdDate desc];
                 
        
        if (!caseComments_List.isEmpty()) {
          
          
          
          comment_List = new list<Comment>();
          Boolean isFirstComment = true;
          for (FM_Discussion__c caseComment : caseComments_List) {
          	
          	system.debug('caseComment By : ' + caseComment.CreatedBy__c);
          	system.debug('caseComment At : ' + caseComment.CreatedDate);
          	system.debug('caseComment Discussion__c : ' + caseComment.Discussion__c);
          	
            
            if (isFirstComment) {
              
                lastComment = new Comment();
                lastComment.theCaseComment = new Case();
                lastComment.theCaseComment.Email_template_comment__c = caseComment.Discussion__c;
                lastComment.theDiscussion = caseComment.Discussion__c;
                
                if (caseComment.Discussion__c.contains('?')) {
              
              	    list<String> commentBody_List =  caseComment.Discussion__c.split('?');
              	    caseComment.Discussion__c = '';
              	    for (String thePart_Str : commentBody_List) {
              	  	
              	  	    caseComment.Discussion__c += ' ? ' + thePart_Str + '\n';
              	  	    lastComment.theDiscussion = caseComment.Discussion__c;
              	    }
                }
              
              lastComment.theComment = caseComment;
              lastComment.creatdBy = ' | By: ' + caseComment.CreatedBy__c;
              lastComment.createdDate = ' | At: ' + String.valueOf(caseComment.CreatedDate);
              lastComment.indexNumber = String.valueOf(caseComment.Index_Number__c) + '.';
              
              if (caseComment.Public__c) {
                
                lastComment.displayPrivateComment = false;
                lastComment.isPrivate             = 'Public Comment';
              }
              
              else {
                
                lastComment.displayPrivateComment = true;
                lastComment.isPrivate             = 'Private Comment';
              }
              isFirstComment = false;
            }
              
            else {
            
              Comment comment = new Comment();
              comment.theCaseComment = new Case();
              comment.theCaseComment.Email_template_comment__c = caseComment.Discussion__c;
              Comment.theDiscussion = caseComment.Discussion__c;
              
              if (caseComment.Discussion__c.contains('?')) {
              
              	  list<String> commentBody_List =  caseComment.Discussion__c.split('?');
              	  caseComment.Discussion__c = '';
              	  for (String thePart_Str : commentBody_List) {
              	  	
              	  	  caseComment.Discussion__c += ' ? ' + thePart_Str + '\n';
              	  	  Comment.theDiscussion = caseComment.Discussion__c;
              	  }
              }
              
              comment.theComment = caseComment;
              
              if (caseComment.Public__c) {
                
                comment.displayPrivateComment    = false; 
                comment.isPrivate                = 'Public Comment';
              }
              
              else {
                
                comment.displayPrivateComment    = true;
                comment.isPrivate                = 'Private Comment';
              }
              
              comment.creatdBy = ' | By: ' + caseComment.CreatedBy__c;
              comment.createdDate = ' | At: ' + String.valueOf(caseComment.CreatedDate);
              comment.indexNumber = String.valueOf(caseComment.Index_Number__c) + '.';
              comment_List.add(comment);
            }
          }
        }
      }
     
    if (theCase.State__c != null) {
      
      if (theCase.State__c.EqualsIgnoreCase('Assigned AE')) {
        
        displayStateAssignedAE = true;
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('Closed')) {
      
        displayStateclosed = true;  
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('Assigned')) {
      
        displayStateAssigned = true;  
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('Wait for Release')) {
      
        displayStateWaitForRelease    = true; 
        statusChangedToWaitForRelease = true; 
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('Waiting for Customer Info')) {
      
        displayStateWaitForCustInfo     = true; 
        statusChangedToWaitCustomerInfo = true; 
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('Open')) {
      
        displayStateOpen = true;  
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('On Hold')) {
      
        displayStateOnHold    = true;
        statusChangedToOnHold = true;  
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('Waiting for Customer Approval')) {
      
        displayStateWaitCustApproval = true;  
      }
      
      else if (theCase.State__c.EqualsIgnoreCase('Wait for Implementation')) {
      
        displayStateWaitImplementation       = true; 
        statusChangedToWaitForImplementation = true; 
      }
      
      
    }
      
    if (theCase.priority != null) {  
      
       if (theCase.priority.EqualsIgnoreCase('High')) {
        
        displayPriorityHigh = true;
      }
      
       else if (theCase.priority.EqualsIgnoreCase('Low')) {
      
        displayPriorityLow = true;  
      }
      
       else if (theCase.priority.EqualsIgnoreCase('Medium')) {
      
        displayPriorityMedium = true;  
      }
      
       else if (theCase.priority.EqualsIgnoreCase('Fatal')) {
      
        displayPriorityFatal = true;  
      }
    }
    
    if (theCase.Related_Project__c != null) {
       system.debug('project exists');
      displayProject = true;
    }            
  }
	

}