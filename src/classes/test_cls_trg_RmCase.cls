@isTest
public with sharing class test_cls_trg_RmCase {

	static testMethod void test_UPdateParentRmCaseWhenChildRmCaseDeleted() {
		
		CLS_ObjectCreator obj = new CLS_ObjectCreator();
                Account acc = obj.createAccount();
                insert acc;
                
                Contact con = obj.CreateContact(acc);
                insert con;
                
                Case cs = obj.Create_case(acc, con);
                insert cs;
                
                RMProject__c rmp = obj.createRMProject(cs);
                insert rmp;
                
                RMProject__c rmp1 = obj.createRMProject(cs); 
                rmp1.RM_Id__c = '76';                                                   
                insert rmp1;
                
                RmAssignee__c  rmAss = obj.createRMAssigne();
                rmAss.RM_Project_Id__c = rmp1.RM_Id__c;
                rmAss.RM_Id__c ='5';
                rmAss.name ='aaabb';                                                                   
                insert rmAss; 
                
                RMUsers__c rmUsr = obj.createRMUser();
                insert rmUsr;
                
                RMPriority__c pr = obj.createPriority();
                insert pr;
               
                RMTracker__c trkr = obj.createRMTracker();
                trkr.RM_Project_Id__c =  rmp1.RM_Id__c;
                trkr.RM_Id__c = '1';                     
                insert trkr;
                
                Id RelatedRMCaseRecTypeId = ENV.RecordTypeIdRMCase_RM_Related_Case;
               
                RMCase__c newRMCase = new RMCase__c();
             
                newRMCase.sfcase__c = cs.Id;
                newRMCase.RMAssignee__c = rmAss.RM_Id__c;
                newRMCase.RMProject__c = rmp1.RM_Id__c;
                newRMCase.RMTracker__c = trkr.RM_Id__c;
                insert newRMCase;
                
                RMCase__c childRMCase = new RMCase__c();
             
                childRMCase.sfcase__c = cs.Id;
                childRMCase.RMAssignee__c = rmAss.RM_Id__c;
                childRMCase.RMProject__c = rmp1.RM_Id__c;
                childRMCase.RMTracker__c = trkr.RM_Id__c;
                childRMCase.Parent_RMCase__c = newRMCase.Id;
                childRMCase.RM_Case__c = 'www.Mellanox.com';
                childRMCase.RecordTypeId = RelatedRMCaseRecTypeId;
                insert childRMCase;
                
                Test.StartTest();
                
                delete childRMCase;
                
                Test.StopTest();
	}


}