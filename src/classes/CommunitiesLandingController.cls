/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        return Network.communitiesLanding();
    }
    
    //custom login/landing page method
    public PageReference forwardToCustomAuthPage() {
        if (Site.getPrefix() == null) {
			return new PageReference('/SupportLogin');
        } else {
            return new PageReference(Site.getPrefix() + '/SupportLogin');
        }
	}
    
    public CommunitiesLandingController() {}
}