public class ENV
{
    public static list<boolean> triggerShouldRun_List = new list<boolean>{false,false,false, false};
    public static boolean createCRMGroup = true;
    public static boolean renerTheComponentNav = true;
    public static ID CaseRecordType_TechnicalSupport ='01250000000DP5a';
    public static ID CaseRecordType_Customer_Infrastructure ='01250000000E2m2';
    public static ID AcademyGeneralAccount = '0015000000oqjxl';
    public static boolean triggerShouldRun = true;
    public static boolean triggerShouldRunCase = true;
    public static boolean Upd_child_AccountTriggerShouldRun = true;
    public static boolean cls_trg_ContractTriggerShouldRun = true;
    public static Map<String,ID> map_opportunityRecordTypeTOID = new Map<String,ID>{'End_User_Opportunity'=>'01250000000Dtdw'};
    
    public static Map<String,ID> PriceBookMap = new Map<String,ID>{'Certified Reseller Price Book'=>'01s500000006J7K', 'DB1 Price Book'=> '01s500000006J7P',
        'Distributor Price Book'=>'01s500000006J09', 'OEM Price Book'=>'01s500000006J7F', 'Standard Price Book'=>'01s5000000062WY',
    'Bull Price List'=>'01s500000006MEi','Hewlett-Packard'=>'01s500000006MF2','IBM'=>'01s500000006MIQ','Dell Pricebook'=>'01s500000006MJd'};
    public static String OrgPrefix = 'mellanox.my.salesforce.com'; // Production
    //public static String OrgPrefix = 'mellanox--test.cs13.my.salesforce.com'; // Test
    
    public static boolean createGroupMember = true;
    public static ID emailFolderID ='00l50000001JqeO';
    public static id ST1_shut_downID='a2YW00000009Qvh'; //test env
    public static boolean isQuoteUpdate=false;
    public static boolean cameFromQuote=false;
    //public static boolean updateQuote_1st=false;
    public static ID AccountId='0015000000RfThX';
    public static Id HPAccountId = '001R000000sjEx0';
    // Production Values
    //public static ID OFFHRqueue='00GW0000000GtoR'; //test
    public static ID OFFHRqueue='00G50000001CsIN'; //Production
    public static boolean caseTriggerRanOnce = false;
    public static boolean triggerRanOnce = false;
    //public static ID Jivequeue='00GR0000001AF8H'; //test
    public static ID Jivequeue='00G500000022RkR'; //Production
    //public static ID RecordTypeIdRMCase_RM_Feature = '012W00000008jqF'; //test
    //public static ID RecordTypeIdRMCase_RM_Case = '012W00000008jx1'; //test
    //public static ID RecordTypeIdRMCase_RM_Related_Case = '012W00000008kXd'; //test
    
    
    public static ID RecordTypeIdRMCase_RM_Feature = '01250000000E5dg'; //Production
    public static ID RecordTypeIdRMCase_RM_Case = '01250000000E5db'; //Production
    public static ID RecordTypeIdRMCase_RM_Related_Case = '01250000000E5dl'; //Production
    public static String orgAttFilesPrefix = 'https://mellanox--c.na3.content.force.com/servlet/servlet.FileDownload?file='; // Production
    
    public static Map<String,ID> QueueMap = new Map<String,ID>{'RMA'=>'00G50000001AudV'};
    // public static Map<String,ID> teamRoleMap = new Map<String,ID>{'PM Owner' => '0B7W000000002Qw','AE Owner' => '0B7W000000002QS', 'Assignee'=>'0B75000000000ap', 'AE Engineer' =>'0B75000000000e8', 'AE Manager' => '0B75000000000eD','Follow Up' => '0B75000000000mH','Account Owner' => '0B75000000000mM','SME' => '0B75000000000pf', 'FAE' => '0B7500000000035'}; //test
    public static Map<String,ID> teamRoleMap = new Map<String,ID>{'PM Owner' => '0B75000000000sZ', 'AE Owner' => '0B75000000000sU', 'Assignee'=>'0B75000000000ap', 'AE Engineer' =>'0B75000000000e8', 'AE Manager' => '0B75000000000eD','Follow Up (1)' => '0B75000000000mH','Follow Up (2)' => '0B7W000000002Sn','Follow Up (3)' => '0B7W000000002Ss','Account Owner' => '0B75000000000mM','SME' => '0B75000000000pf','FAE' =>'0B7500000000035', 'Developer'=>'0B750000000003A', 'Sales Director'=>'0B7500000000036' }; // sandbox
    public static Map<ID,String> teamRoleMapIdToName = new Map<Id,String>{ '0B75000000000sZ' => 'PM Owner', '0B75000000000sU' => 'AE Owner' , '0B75000000000ap' => 'Assignee', '0B75000000000e8' => 'AE Engineer' , '0B75000000000eD' => 'AE Manager' , '0B75000000000mH' => 'Follow Up (1)' , '0B7W000000002Sn' => 'Follow Up (2)' , '0B7W000000002Ss' => 'Follow Up (3)' , '0B75000000000mM' => 'Account Owner' , '0B75000000000pf' => 'SME' ,'0B7500000000035' => 'FAE' , '0B75000000000v9' => 'Portal Customer', '0B7500000000036' => 'Sales Director' }; // sandbox
    //public static Map<String,ID> teamRoleMap = new Map<String,ID>{'PM Owner' => '0B75000000000sZ', 'AE Owner' => '0B75000000000sU', 'Assignee'=>'0B75000000000ap', 'AE Engineer' =>'0B75000000000e8', 'AE Manager' => '0B75000000000eD','Follow Up (1)' => '0B75000000000up','Follow Up (2)' => '0B75000000000uu','Follow Up (3)' => '0B75000000000uz','Account Owner' => '0B75000000000mM','SME' => '0B75000000000pf','FAE' =>'0B7500000000035', 'Developer'=>'0B750000000003A', 'Sales Director'=>'0B7500000000036' }; // production
    //public static Map<ID,String> teamRoleMapIdToName = new Map<Id,String>{ '0B75000000000sZ' => 'PM Owner', '0B75000000000sU' => 'AE Owner' , '0B75000000000ap' => 'Assignee', '0B75000000000e8' => 'AE Engineer' , '0B75000000000eD' => 'AE Manager' , '0B75000000000up' => 'Follow Up (1)' , '0B75000000000uu' => 'Follow Up (2)' , '0B75000000000uz' => 'Follow Up (3)' , '0B75000000000mM' => 'Account Owner' , '0B75000000000pf' => 'SME' ,'0B7500000000035' => 'FAE' , '0B750000000003A' => 'Developer', '0B7500000000036' => 'Sales Director' }; // production
    
    
    public static ID CaseTEamFollower1 = '0B75000000000up';
    public static ID CaseTEamFollower2 = '0B7W000000002Sn';
    public static ID CaseTEamFollower3 = '0B7W000000002Ss';
    
    public static string Projectprepend= 'a2q';//value correct to SANDBOX, need to check in prod anv what is the correct val.
    public static ID CommunityId = '0DB500000008OQOGA2';
    // public static ID CommunityId = '0DBW0000000001JOAQ'; //Test
    //public static Map<String,ID> ProjectTypeMap = new Map<string,ID>{'Cluster Design'=>'012W00000000NYR', 'Service Delivery Project'=>'012W00000000QHJIA2'};//test
    public static Map<String,ID> ProjectTypeMap = new Map<string,ID>{'Cluster Design'=>'012W00000000NYR', 'Service Delivery Project'=>'01250000000DzNhAAK'};//production
    public static ID SiteUser = '0055000000145ot';
    public static String refrenceToRMA = '/a0E/o';
    public static String CaseFieldInRMA = 'CF00N50000001vo7t_lkid';
    public static string refreshOpenerCloseMeSControlId = '01N500000009QB3';
    public static ID ContactForEmail = '0035000000dn29S';
    //public static ID UnknownAccount = '0013B000005ZRTo';    // PMI
    public static ID UnknownAccount = '0015000000ZxRg9';    // PRODUCTION 
    public static ID UnknownEMEAAccount = '0015000000ZKQFw';
    public static ID UnknownUSAccount = '0015000000ZKQFv';
    public static ID MELLANOX_ACADEMY_USERS_GENERAL_Account = '0015000000oqjxl';
    public static ID HPAccount = '0015000000ZKPIP';
    public static ID IBMAccount = '0015000000JIq8Z';
    public static ID SUNAccount = '0015000000YdJQ1';
    public static ID DELLAccount = '0015000000JIq8z';
    public static ID TemplateInternalCaseComment = '00X500000016r0n';
    public static ID TemplatePublicCaseComment = '00X500000016r0q';
    public static ID AE_Manager = '005500000013kUq'; //Yael Shenhav
    public static ID MarketingSalesQueue= '00G50000001BWzZ';
    public static ID SystemQueue = '00G50000001AvPB';
    public static ID RMASupportQueue = '00G50000001BeJ3';
    public static ID RMAQueue = '00G50000001AudV';
    public static ID ST1 = '00G500000022CAz'; //production
    public static ID ST1USers = '00G500000022LkXEAU'; //production
    public static Id LiveAgentCaseRecordType = '01250000000E50j';
    
    public static String User_ID = '005';
    public static String Group_ID = '00G';
    public static ID AdminQueue = '00G50000001Blnf';
    public static ID UnassignedUS = '00G50000001BVMh';
    public static ID UnassignedEMEA = '00G50000001BVMc';
    public static ID CallTypeMarketing = '01250000000DtG3';
    public static ID CallTypeSupport = '01250000000DtG8';
    public static ID AEQueue = '0055000000146q2';
    public static ID AEQueueCP ='00550000001qpoQ'; // contact AE queue
    //public static ID AEQueueCP ='00550000001qYZc'; // contact AE queue  PMI  DO NOT DEPLOY TO PROD
    
    public static ID Temp_PublicCommentContact_Admin = '00X500000017fhQ';
    public static ID Temp_PublicCommentContact = '00X500000016r0k'; //SUPPORT: Customer Portal user New Comment Notification
    public static ID Temp_PublicCommentContact_NoAccess = '00X500000016udU'; //SUPPORT: Customer Portal user New Comment Notification - No Access
    public static ID Temp_PublicCommentCc = '00X500000016r0j'; //SUPPORT: Customer Portal Cc New Comment Notification
    public static ID Temp_PublicComment_Email_to_Case = '00X5000000172RR'; // SUPPORT: Customer Portal user New Comment Notification - Email to Case
    //Test
    /* public static ID TemplatePubAttachmentContact ='00XP0000000QOSX';
    public static ID TemplatePubAttachmentSF ='00XP0000000QOSc';
    public static ID TemplatePriveAttachmentSF ='00XP0000000QOSh';
    public static ID TemplatePriveAttachmentRM ='00XP0000000QOSm'; */
    public static ID TemplateNewAttachment = '00XP0000000QOSX';
    //Production
    public static ID TemplatePubAttachmentContact ='00X500000017sRU';
    public static ID TemplatePubAttachmentSF ='00X500000017sRT';
    public static ID TemplatePriveAttachmentSF ='00X500000017sRS';
    public static ID TemplatePriveAttachmentRM ='00X500000017sRR';
    
    public static ID TemplateNewCaseOpened_Design_In ='00X50000001742O';
    public static ID ProfileDesignSupportCus = '00e50000000pQX9';
    public static ID ProfileSystemSupportCus = '00e50000000pQX4';
    public static ID ProfileSystemSupportOEMCus = '00e50000000pQX4';
    public static ID ProfileAE = '00e50000000pQWz';
    public static ID SupportAdminAddress ='0D25000000000Aa';
    public static ID RMASupportAddress ='0D25000000000Cq';
    public static string AEManagerEmail = 'yaeli@mellanox.co.il';
    public static ID webCustomer ='005500000015ZVv';
    public static ID emailToCaseUser ='005500000013ipV';
    public static ID US_Mellanox_Site = 'a1V500000007G8C'; //Production
    public static ID Yokneam_Mellanox_Site = 'a1V5000000071T7'; //Production
    public static ID EMEA_Mellanox_Site = 'a1V5000000071T7';
    public static ID HongKong_Mellanox_Site = 'a1V500000007HA8';
    public static ID HELLMANN_Mellanox_Site = 'a1V50000002gwzL' ; //test
    public static ID CaseTypeRMA = '01250000000Dtey';
    public static ID CaseTypeAdmin = '01250000000Dury'; // Production
    //public static ID CaseTypeAdmin = '012R00000004mW9'; // test
    public static ID CaseTypeSupport = '01250000000DP5a';
    public static ID UnassignedST1 = '00G50000001BWZO';
    public static ID SystemAdmin = '00550000000x9gE';
    //public static ID EscalationOwnersGroup = '00GR0000000wSSp'; //Test
    public static ID EscalationOwnersGroup = '00G50000001Btjc'; //Production
    public static ID IntegrationUser = '00550000001reiG'; //Production
    public static ID OEMRMA_RecordType = '012P00000000NnG'; //Test
    public static ID Internal_RMA_RecordType = '01250000000DyQF'; // intRMA development env
    
    // public static ID OEMRMA_RecordType = '01250000000DwqY'; //Production
    public static ID DefaultContract ='a0M500000025sPL';
    public static ID MellanoxSiteHomgKong ='a1V500000007HA8';
    public static ID POCqueueOwner1 ='005500000015XCi'; //Sagi Schlanger
    public static ID POCqueueOwner2 ='00550000001ruWm'; //Marina Varshaver
    public static ID Eyal_Pitchadze = '005500000014Kwv';
    public static ID CSI_Case = '01250000000E2m2';
    public static ID recordTypeLicenseEvaluation = '012R000000053eo';  // Sandbox
    
    public static Map<String, id> AE_Engineers = new Map<String, id>{'Yael Shenhav' => '005500000013kUq', 'Dan Waxman' => '005500000013kV4','Amit Krig' => '005500000014SVE',
        'Yotam Lahav' => '005500000014SVJ', 'Itay Gazit' => '005500000014SVT','Oren Sela' =>'00550000001sY6G', 'Oren Kladnitsky'=> '005500000014SVd',
        'Peleg Abergel' => '005500000014SVd', 'Doron Fael' => '005500000014GtY', 'Boris Shpolyansky' => '005500000011h1D' , 'Amit Krig' =>'005500000014SVE',
        'Eli Laufer'=> '005500000011tX6', 'Avi Alkobi' => '005500000015XuL','Ari Cohen' =>'00550000001qS4i','Matt Finlay' => '005500000011h0P',
        'Sagi Schlanger' => '005500000015XCi', 'Aweas Rammal' =>'00550000001qiiZ', 'Elad Wind'=>'00550000001qxWn', 'Vadim Balakhovski'=>'00550000001r8Fx', 'Avi Telyas' => '005500000013jD3',
        'Yoram Zer' => '00550000001rHg0', 'Maksim Kunin' => '00550000001rR6Q', 'Adir Lev'=>'00550000001s1rI','Eduard Lazebnik' => '00550000001s3xN',
        'Eyal Tokman' => '00550000001s92r', 'Amir Luckman' => '00550000001rUk2', 'Omer Gazit' => '00550000001s3xS', 'Erez Cohen' => '005500000011h0j',
        'Assaf Azrilevich' => '00550000001sJPD', 'Roi Aibester' => '00550000001sDMR','Asaf Shenhav' => '00550000001sRTh','Raz Baussi' => '00550000001sS52',
    'Marina Varshaver' => '00550000001ruWm', 'Yaniv Aviv' =>'00550000001siX7'};
    
    
    public static Map<String, id> Sales_Directors = new Map<String,Id>{'Chuck Tybur' => '005500000013P66', 'Robert Parda' =>'005500000011csC', 'Don Fiegel' => '005500000011vex',
        'Wayne Augsburger' => '005500000011csI', 'Yossi Avni' =>'0055000000152sn','Joe Cherng' => '005500000011csM', 'Myles Yu' => '005500000014Jd9',
        'Chris Tsumura' => '005500000014dfZ', 'Colin Bridger' => '0055000000144St', 'Vishal Bharat' => '00550000000x1ye','Jon Green' => '005500000014rt1',
        'Steve Williams' =>'005500000013FIz', 'Ken Behampis' => '0055000000144Lx', 'James Lonergan' => '0055000000148y8', 'Pat Kelley' => '005500000014ddi',
        'Christian Olsson' =>'0055000000144PG','Chris Shea' => '005500000013nrY', 'Tony Rea' => '00550000000x9VB', 'Brandon Hathaway' => '005500000012yUo',
        'Michael Marden'=> '0035000000ct4o7', 'Bill Downer' => '005500000014ply', 'Ron Aghazarian' => '00550000000wuVp','Matt Randazzo' => '005500000011tXL',
        'Bill Lee'=> '0055000000148xZ', 'David Brazilai'=> '005500000015ZVw', 'Darrin Chen' => '005500000011h1E', 'Amir Prescher' =>'005500000015ZVp',
        'Sergio Gemo' => '005500000015ZVa', 'Irfan Modak' => '00550000001qXXa','Scott McAuliffe'=>'00550000001qmAq','Gilad Shainer' =>'00550000001r3oE',
        'Garth Fruge' =>'00550000001qfuh','Duane Dial'=> '005500000011eOn', 'Gerry Hunt' => '005500000015ZVb','Ozzie Gomez' => '00550000001qWLs',
    'Juergen Windler' => '005500000015YCA','Marc Sultzbaugh' => '005500000011cs7'};
    
    public static Map<String, id> Eyal_P_Team = new Map<String, id>{'Eddie Shklaer' => '005500000015ZVd' , 'Gadi Godanian'=>'00550000001r3oC' , 'Rian Twizer'=>'00550000001sZdV',
    'Eyal Pitchadze'=>'005500000014Kwv'}; // test + Production values
    
    
    
    public static string getStringIdsForDynamicSoql(list<id> IdsToConcatenate_List)
    {
        string strReturnedIds = '(';
        for(id idItem : IdsToConcatenate_List)
        {
            if(strReturnedIds.length() > 1) // If strReturnedIds is not empty, adds a comma as separator before adding new values.
            strReturnedIds += ',';
            strReturnedIds += '\'' + idItem + '\'';
        }
        strReturnedIds += ')';
        return strReturnedIds;
        
    }
    
    public static string getStringsForDynamicSoql(list<string> StringsToConcatenate_List)
    { //string strReturnedIds ;
        string strReturnedIds = '(';
        for(string idItem : StringsToConcatenate_List)
        {
            if( strReturnedIds.length()>1) // If strReturnedIds is not empty, adds a comma as separator before adding new values.
            strReturnedIds += ',';
            strReturnedIds += '\'' + idItem + '\'';
        }
        strReturnedIds += ')';
        return strReturnedIds;
        
    }
    
    
    /* public static boolean IsInQueue(id UserId, string GroupName )
    {
    if(GroupName == 'AE Queue')
    { if(UserId == '005500000013kV4'||UserId== '005500000013kUq'||UserId== '005500000014GtY'||UserId== '005500000014SVO' ||
    UserId=='005500000014SVT' ||UserId== '005500000013hxs' ||UserId== '005500000014SVJ' ||UserId== '005500000014SVE' || UserId=='005500000014SVd' ||
    UserId=='005500000011h1D' || UserId == '005500000014SVE' || UserId == '005500000011tX6' || UserId == '005500000015XuL' ||
    UserId == '00550000001qS4i'|| UserId == '005500000011h0P' || UserId == '005500000015XCi'|| UserId == '00550000001qiiZ'||
    UserId == '00550000001qxWn' || UserId == '00550000001r8Fx'||UserId == '005500000013jD3' || UserId == '00550000001rHg0' ||
    UserId == '00550000001rR6Q' || UserId == '00550000001rszw' || UserId == '00550000001s1rI'|| UserId =='00550000001s3xN' ||
    UserId =='00550000001s92r' || UserId == '00550000001rUk2' || UserId == '00550000001s3xS' || UserId == '005500000011h0j' ||
    UserId == '00550000001sJPD' || UserId == '00550000001sDMR' || UserId == '00550000001sRTh' || UserId =='00550000001sS52' ||
    UserId =='00550000001ruWm' || UserId =='00550000001sY6G' || UserId == '00550000001siX7' )
    {return true;}
    }
    return false;
    } */
    
    public static boolean IsInQueue(id UserId, string GroupName )
    {
        
        if(UserId == NULL)
        return false;
        
        List<GroupMember> AllMembers = new List<GroupMember>();
        //Group g = [select Id from Group Where Name =:GroupName limit 1];
        
        
        
        AllMembers = [select UserOrGroupId from GroupMember g where g.Group.name =:GroupName];
        
        for( GroupMember GM: AllMembers )
        {
            if(GM.UserOrGroupId == UserId)
            {return True;}
        }
        
        
        return false;
    }
    
    
    public static Id GetRecordTypeIdByName (String str)
    {
        return [Select r.Name, r.Id From RecordType r where Name =: str].Id;
    }
    
    
    public static boolean IsSpamEmail(string EmailAddress)
    {
        if(EmailAddress.contains('post_master')||EmailAddress.contains('postmaster'))
        
        {return true;}
        
        return false;
    }
    
    public static void Insert_RM_discussion(FM_discussion__c FM,id RMcase )
    {
        rmdiscussion__c RM = new rmdiscussion__c();
        RM.RM_Discussion__c = FM.discussion__c;
        RM.SF_RM_Case__c = RMcase;
        RM.Sync_Flag__c ='1';
        RM.Parent_FM_Dis__c = FM.id;
        RM.Created_by__c = FM.CreatedBy__c;
        RM.created_date__c = FM.CreatedDate;
        insert RM;
    }
    
    public static String getDomainFromEmail(String email) {
        
        String domainName = email.substring(email.indexOf('@') + 1);
        return domainName;
    }
    
    // This method gets a list of SObject and a field API name and returns a set of Ids of that field
    public static set<id> createSetOfIds(sObject[] sList,String field) {
   
        set<id> Result = new set<id>();
        
        if (sList == null) {
            
            return Result;
        }
        
        for (sObject item : sList) {
         
            field = (field == null) ? 'id' : field;
            Result.add((id) item.get(field));
        }
        return Result;
    }
}