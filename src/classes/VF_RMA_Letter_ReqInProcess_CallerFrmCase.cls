/*
Johny 14-09-2011
Biref description about this Controller:
- the controller is activated from Case where the user press the Approve button in order to approve the RMA
- using case id we extract the RMA ID and call the RMA_Letter_RequestInProcess VF and put the id of the RMA.
- the result of this VF page is a pdf that will be returned as a blob and attached to an email
- then it sends an email to the RMA group as well as the customer

*/

public with sharing class VF_RMA_Letter_ReqInProcess_CallerFrmCase 
{   
      
    public ID caseId;
    public boolean err;
    
    
    public VF_RMA_Letter_ReqInProcess_CallerFrmCase(ApexPages.StandardController controller) 
    {
        //id = Apexpages.currentPage().getParameters().get('Id');
        //getDeliverAsPDF();
    }

    public PageReference getDeliverAsPDF_InProcessFrmCase() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         caseId = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         err = false;
         
         system.debug('id is : ' + caseId);
         //get the related RMA id from the case
         Case c = [Select c.RMA_Request__c, c.RMA_Request__r.Name, c.RMA_Request__r.RMA_OPS_ALERT__c, c.RMA_Request__r.Advanced__c, c.RMA_Request__r.Num_of_SNs__c,   
                       c.RMA_Request__r.DOA_Count__c, c.RMA_Request__r.SN_NotApproved_Count__c, c.Contact.Email, c.Id, c.State__c , c.RMA_Support_Project__c,
                       c.RMA_Approved__c,c.diagnosed_by__c, c.Failure_Analysis_Request__c,Japan_Asset_Replaced__c,Japan_Asset_Replacment_SN__c,c.RMA_Request__r.Total_Japan_Asset__c
                       From Case c WHERE c.id =: caseId];
                       
                       
         if(c.state__c ==  'Closed')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'The case was already closed'));
         return Apexpages.currentPage();
         }
         
                                
         if(c.state__c == 'Open')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'RMA case can be moved to Approved only when it is Assigned'));
         err = true;
         }              
        
                           
         if(c.diagnosed_by__c == NULL)
         {  
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please fill in the \'Diagnosed by\' field in order to Approve RMA'));
         err = true;
                
         }
         
         
         
        if(c.RMA_approved__c == True)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'The RMA was already Approved, email notification was sent to customer'));
             err = true;   
            }
        if(c.RMA_Request__r.Num_of_SNs__c  ==c.RMA_Request__r.SN_NotApproved_Count__c)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'You can not Approve RMA when all SNs are \'Not Approved\' '));
              err = true;  
            }
            
        if(c.RMA_Support_Project__c != Null)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'You can not Approve this RMA - This RMA Case Contains a Support Project Asset '));
              err = true;  
            }

      if(c.Failure_Analysis_Request__c ==NULL)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please fill in the \'Failure Analysis Request\' field in order to Approve RMA'));
             err = true;   
            }

      if(c.RMA_Request__r.Total_Japan_Asset__c > 0 && c.Japan_Asset_Replaced__c ==NULL)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'This is Japan RMA, please fill in the \'Japan Asset Replaced\' field in order to Approve RMA'));
             err = true;   
            }
            
      if(c.RMA_Request__r.Total_Japan_Asset__c > 0 && c.Japan_Asset_Replaced__c == 'Yes' && c.Japan_Asset_Replacment_SN__c == NULL)
            {  
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'This is Japan RMA with replacement, please fill in the \'Japan Replacement SN\' field in order to Approve RMA'));
              err = true;  
            }      


          
         PageReference pdf =  Page.VF_RMA_Letter_RequestInProcess;
         system.debug('RMA_Request__c :' + c.RMA_Request__c + ' State__c: ' + c.State__c);
        //PageReference pdf = new PageReference('Page.vf_RMA_letter');
         if(c.RMA_Request__c != null && c.State__c == 'Assigned' && err == false)
         {
            c.State__c = 'RMA Approved';
            c.RMA_Approved__c = true;
                        
            pdf.getParameters().put('id',c.RMA_Request__c);
            system.debug('pdf.getParameters().get id:  ' + pdf.getParameters().get('id'));
         
            //pdf.setRedirect(true);
    
            // Grab it!
            //system.debug('pdf.getContent() : ' + pdf.getContent().size());
             
            //String b = pdf.getContent().toString();
            //---------------------Email to Customer------------------------------------------------
            Messaging.SingleEmailMessage cust_email = new Messaging.SingleEmailMessage();
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

    
            cust_email.setSubject('Mellanox RMA #' + c.RMA_Request__r.Name +' is in Process');
    
            String [] toCustAddresses = new String[] {c.Contact.Email}; 
              
            cust_email.setToAddresses(toCustAddresses);
        
            cust_email.setPlainTextBody('Dear Valued Customer,\n' + '\nYour RMA request is verified and qualified. \n'+
                                    'You will receive a confirmation by email containing your RMA number. Note you MUST obtain an RMA number before returning your product.\n'+
                                    'Please see the attached information on your RMA status.\n' +
                                    '\nTo learn more about Mellanox RMA:\n' +
                                    'http://www.mellanox.com/pdf/user_manuals/Mellanox_Support_and_Services_User_Guide.pdf \n\n' +
                                    'Thank you\n' +
                                   'Mellanox Support\n');

        
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
             system.debug('isTest: ' + isTest);
            if(isTest == 'Testing')
            {
            }else
            {
                blob b = pdf.getContent();
                 efa.setFileName('RMA_Request_In_Process.pdf'); // neat - set name of PDF
        
                 efa.setBody(b); //attach the PDF
        
                 email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                 cust_email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

            }   
             // send it, ignoring any errors (bad!)
    
             //ek Messaging.SendEmailResult [] cust_r =
        
               //ek      Messaging.sendEmail(new Messaging.SingleEmailMessage[] {cust_email});
             
                          
                          
                      
             




                
            //******************************************************************
            
            // Create an email
    
               
            email.setSubject('Mellanox RMA #' + c.RMA_Request__r.Name +' is in Process');
    
             String [] toAddresses = new String[] {}; //{'jabuhana@gmail.com','innag@mellanox.co.il'};
                     
             //here i add the list of emails of the RMA group by getting the RMA Group ID
             //then all members and then using the members i get the users and then looping in the users adding the emails for the toAddresses
             Group rmaGroup = [Select g.Name, g.Id, g.Email From Group g WHERE Name ='RMA GROUP'];
             system.debug('rmaGroup: ' +rmaGroup );
             
             //List<GroupMember> lst_grpMembers = new List<GroupMember >([Select g.UserOrGroupId, g.Id, g.GroupId From GroupMember g where g.GroupId  ='00G50000001AucS' ]);
             List<GroupMember> lst_grpMembers = new List<GroupMember >([Select g.UserOrGroupId, g.Id, g.GroupId From GroupMember g where g.GroupId  =: rmaGroup.Id ]);
             system.debug('lst_grpMembers siae : ' +lst_grpMembers.size() );
             
             Set<ID> set_UsrIds = new Set<ID>();
             
             if(lst_grpMembers != null && lst_grpMembers.size() > 0)
             {
                for(GroupMember grpm :lst_grpMembers)
                {
                    set_UsrIds.add(grpm.UserOrGroupId);
                }           
             }
         
             system.debug('set_UsrIds Size : ' + set_UsrIds.size());
             if(c.assignee__c != NULL)
             {set_UsrIds.add(c.assignee__c);}
             
             List<User> lst_users = new List<User>([Select u.Id, u.Email From User u WHERE u.id IN: set_UsrIds]);
              system.debug('lst_users Size : ' + lst_users.size());
             
             set<String> set_usrEmails = new set<String>();
             if( lst_users != null && lst_users.size() > 0)
             {
                for(User usr :lst_users)
                {
                    if(usr.Email != null)
                    {
                        set_usrEmails.add(usr.Email);
                    }
                }
             }
             system.debug('set_usrEmails Size : ' + set_usrEmails.size());
             if(set_usrEmails != null && set_usrEmails.size() > 0)
             {
                for(String usrEmail :set_usrEmails)
                {
                    toAddresses.add(usrEmail);
                }
             }
             //till here the adds of the group members to the toAddresses
             system.debug('toAddresses: ' + toAddresses);
             
             email.setToAddresses(toAddresses);
        
             email.setPlainTextBody( '**** RMA REQUEST APPROVED BY SUPPORT**** \n\n' +
                                     'RMA request #' + c.RMA_Request__r.Name + 'was approved by Support group. \n'+
                                     'See the attached information on  RMA \n' +
                                     'Please take care by placing an order in Oracle. \n' +
                                     'Once you receive RMA number from Oracle please enter it to Salesforce and change RMA state to Execution in Process.\n'+
                                     'OPS team please see special notes: \n' +    
                                     'Advanced Replacement: ' + c.RMA_Request__r.Advanced__c + '\n'+ 
                                     'Number of DOAs: ' + c.RMA_Request__r.DOA_Count__c + '\n' +
                                     'Other notes: ' + c.RMA_Request__r.RMA_OPS_ALERT__c);
                
           //ek  Messaging.SendEmailResult [] r =
        
               //ek      Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
             
             update c;
         
            return new Pagereference('/'+caseId);
        }else
        {
            return null;
        }
    }
   
//************************* TEST CLASS ************************   

    static testMethod void Test_VF_RMA_Letter_ReqInProcess_CallerFrmCase() 
    {
        //list of serial numbers to be related to the RMA and to avoid many inserts
        List<Serial_Number__c> lst_snOfRMA = new List<Serial_Number__c>();
        
        List<Product2> lst_prods = New List<Product2>();
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        con.email = 'hgft56@er.com';
        insert con;
        
       // RMA__c rma = obj.CreateRMA();
        //insert rma;
        
        MellanoxSite__c  site  = obj.createMellanoxSite();
        insert site;
         
        RMA__c rma = obj.CreateRMA();
        rma.Mellanox_Site__c = site.id;
        insert rma;
        
    
        Product2 p1 =  obj.createProduct();
        p1.Inventory_Item_id__c = '90989';
        lst_prods.add(p1);
        
        Product2 p2 =  obj.createProduct();
        p2.Name = 'Test Product 2';
        p2.Inventory_Item_id__c = '90980';
        lst_prods.add(p2);
        
        Serial_Number__c sn1 = obj.createSerialNumber(rma,p1);
        lst_snOfRMA.add(sn1);
        
        Serial_Number__c sn2 = obj.createSerialNumber(rma,p2);
        lst_snOfRMA.add(sn2);
        
        insert lst_prods;
        insert lst_snOfRMA; 
        
        Case rmaCase = obj.CreateRMA_case(acc, con, rma);
        
       // rmaCase.State__c = 'Assigned';
        insert rmaCase;
        
        Case rmaCaseSelected = [Select id,State__c,c.RMA_Request__c, c.RMA_Request__r.Name, c.Contact.Email from Case c Where Id =: rmaCase.Id ];
        rmaCaseSelected.OwnerId = '005500000013ipV';
        rmaCaseSelected.Assignee__c = '005500000013ipV';
        rmaCaseSelected.State__c = 'Assigned';
        Test.startTest();
        	
        try { 	
        	update rmaCaseSelected;
        }
        
        catch(Exception e) {
        	
        	
        }
        
        system.debug('Babcom rmaCase display' + rmaCase);
        
       // PageReference pdf =  Page.VF_RMA_Letter_ReqInProcess_CallerFrmCase;
        
       // pdf.getParameters().put('id',rmaCase.id);
              
        
        
        Test.setCurrentPageReference(new PageReference('Page.VF_RMA_Letter_ReqInProcess_CallerFrmCase')); 
        System.currentPageReference().getParameters().put('id',rmaCaseSelected.Id );
        System.currentPageReference().getParameters().put('isTest','Testing' );  
                   
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rmaCaseSelected);
        VF_RMA_Letter_ReqInProcess_CallerFrmCase vf = new VF_RMA_Letter_ReqInProcess_CallerFrmCase(teststandard);
        vf.getDeliverAsPDF_InProcessFrmCase();
        
        Test.stopTest();
    }

}