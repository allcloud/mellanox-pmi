public class VFEditAllQuoteLineItem 
{
    public Quote currQuote {get;set;}
    public QuoteLineItem currQuoteLineItem {get;set;}
    public List<QuoteLineItem> lst_QuoteLineItems {get;set;}
    
    public VFEditAllQuoteLineItem(ApexPages.StandardController stdController) 
    {
        Quote quote = (Quote)stdController.getRecord();
        lst_QuoteLineItems = [Select q.List_Price_Holder__c,q.New_Price__c,q.UnitPrice, q.TotalPrice, q.Subtotal, q.QuoteId, q.Quantity, q.Product__c, q.Order__c, q.ListPrice, q.Customer_Ref_Part_Number__c 
                              From QuoteLineItem q 
                              Where QuoteId=: quote.Id];
        currQuote = [Select q.Id, q.Name From Quote q Where Id=: quote.Id][0];
        
    }
    
    public Pagereference Save()
    {
        List<QuoteLineItem> tmplist = lst_QuoteLineItems;
        
        update tmplist;
        return new Pagereference ('/'+ currQuote.Id);
    }
    
    public Pagereference Cancel()
    {
        return new Pagereference ('/'+ currQuote.Id); 
    }
}