/**********************************************************************
controller of visual force page : vf_TrainingRequest
created by : Dan Kacen 
date created : 8/2/11
**********************************************************************/

public class ctl_vf_TrainingRequest {

    public Training_Request__c newTR {get; set;} //the new form object.
    
    public ctl_vf_TrainingRequest()
    {
            newTR= new Training_Request__c();
    }
        
    public PageReference cancel()//the cancel functionality
    {
        Pagereference pr = new Pagereference('http://mellanox.com/content/pages.php?pg=support_index'); //URL to return back after canceling the form
        
        return pr;
    }


    public PageReference submit()//submit functionality 
    {
        detailsFromEmail(); //call the function to retrieve the contact detail and account details
        if (newTR!=null && newTR.Contact_Email__c!=null)
        {
            insert newTR;
        }
        Pagereference pr = new Pagereference('http://support.mellanox.com/SupportWeb/service_center/Thank_You'); //URL to return back after submiting the form
        return pr;
    }
    
    public PageReference reset() //clear the fields in the form
    {
        Pagereference pr = ApexPages.currentPage();//the URL for the page, inorder to reload it with clear fields
        pr.setRedirect(true);
        return pr;
    }
    
    public void detailsFromEmail() {
        //the function that pull the contact information and relate the new object (newTR) with the right details.
        
        List<Contact> c = new List<Contact>();
        if (newTR.Contact_Email__c!=null && newTR.Contact_Email__c!='')
        {
            c = [Select c.Phone, c.Name, c.Id, c.Email, c.AccountId //the query to retrieve the contact details
                 From Contact c
                 Where c.Email=:newTR.Contact_Email__c
                 limit 1];
            
            if (c.size()>0)//in case the query is not empty
            {
                for (Contact con : c)//put in the new Request the details of the contact retrieve in the query
                {
                    newTR.Account_Name__c=con.AccountId;
                    newTR.Contact_Name__c=con.Id;
                }
            }
            else//show an error msg in case the query was empty
            {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.severity.ERROR, 'The contact with the specified email does not exists in the system'));
            }
        }
    }
}