global class ScheduleUpdateOldAtt implements Database.Batchable<sObject>, Schedulable
{   public set<Id> set_Case ;                   
    public ScheduleUpdateOldAtt()
    {                  
    }
       
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //set<Id> set_CaseIds = new set<Id>{'5005000000Q3Qjz','5005000000Q3Qzc','5005000000Q3RS1','5005000000Q3RXF','5005000000Q3S6F','5005000000Q3SU7','5005000000Q3SVK','5005000000NCFrr','5005000000NCkki','5005000000PLgLG','5005000000PLyro','5005000000Q2GeG','5005000000Q3JdD','5005000000Q3Ji8','5005000000Q3Jmp','5005000000Q3JsT','5005000000Q3L4J','5005000000Q3Lqb','5005000000Q3Ndg','5005000000Q3Q4B','5005000000NtwyB'}; 
        //return Database.getQueryLocator('Select c.isUpdated__c, c.CaseNumber,Assignee__r.Username,Assignee__c,Account.RecordTypeId , (Select IsDeleted, Name, CreatedDate, Attachment_Id__c, Case_Id__c, Source__c, Attachment_Number__c From Attachments_Integration__r where IsDeleted = false and Attachment_Id__c != NULL order by CreatedDate), (Select Id, IsDeleted, ParentId, Name From Attachments where IsDeleted = false) From Case c where  (Assignee__c != NULL or State__c != \'Closed\') and ( Assignee__c != NULL or State__c != \'Assigned\') and Account.RecordTypeId = \'01250000000DP1n\' and c.isUpdated__c=false order by CaseNumber');
        set_Case = new set<Id>();
        for(AttachmentIntegration__c atin: [Select Case_Id__c FROM AttachmentIntegration__c where isCaseUpdated__c=true and CaseStatus__c != 'Closed' and Case_Id__c != NULL and Attachment_Id__c !=NULL and Attachment_Number__c = NULL order by CreatedDate])
        {
        	set_Case.add(atin.Case_Id__c);
        }
        system.debug('==> set_Case '+set_Case);
        system.debug('==> set_Case size '+set_Case.size());
        //set<Id> set_CaseIds = new set<Id>{'5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000OZhBOAA1','5005000000Q2nITAAZ','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000PJx1TAAT','5005000000PK1IGAA1','5005000000PK1IGAA1','5005000000PK1IGAA1','5005000000ObdpUAAR','5005000000ObdpUAAR','5005000000PIRR5AAP','5005000000Q3EwJAAV','5005000000NvimcAAB','5005000000NvimcAAB','5005000000PLtcgAAD','5005000000ObdpUAAR','5005000000ObdpUAAR','5005000000Q2nITAAZ','5005000000ObU7RAAV','5005000000OaCyTAAV','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000ObU7RAAV','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000OZhBOAA1','5005000000PJF9jAAH','5005000000Nt2cQAAR','5005000000ObU7RAAV','5005000000OZhBOAA1','5005000000PJx1TAAT','5005000000ObdpUAAR','5005000000ObdpUAAR','5005000000ObU7RAAV','5005000000Nvz28AAB','5005000000Nt2cQAAR','5005000000OZhBOAA1','5005000000Nvz28AAB','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000ObdpUAAR','5005000000ObdpUAAR','5005000000ObdpUAAR','5005000000ObU7RAAV','5005000000NtM5yAAF','5005000000Q35sAAAR','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000OYx6wAAD','5005000000Q3S6FAAV','5005000000Q2neoAAB','5005000000Q3EwJAAV','5005000000OYifnAAD','5005000000LUj7uAAD','5005000000Q3872AAB','5005000000Q3872AAB','5005000000Q3EwJAAV','5005000000Q3EwJAAV','5005000000Q3EwJAAV','5005000000Q3S6FAAV','5005000000PLPBOAA5','5005000000PKV79AAH','5005000000PI5ayAAD','5005000000PIR7yAAH','5005000000MHh90AAD','5005000000PJoeJAAT','5005000000Q3QjzAAF','5005000000Q3QjzAAF','5005000000Q3QjzAAF','5005000000Q3QjzAAF','5005000000Q3QjzAAF','5005000000Q3QjzAAF','5005000000Q3QjzAAF','5005000000Q3QjzAAF','5005000000Q2rwQAAR','5005000000Q2rwQAAR','5005000000Q2rwQAAR','5005000000PM4o9AAD','5005000000PI39RAAT','5005000000Q3QjzAAF','5005000000NvBcLAAV','5005000000Q3QjzAAF','5005000000Oaep6AAB','5005000000LR1k8AAD','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000PIaiFAAT','5005000000PMCrMAAX','5005000000PMCrMAAX','5005000000PMCrMAAX','5005000000Q2wqMAAR','5005000000PKXLsAAP','5005000000PKXLsAAP','5005000000PL8HaAAL','5005000000ObU7RAAV','5005000000Q2rwQAAR','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Nt2cQAAR','5005000000Nt2cQAAR','5005000000PLPHHAA5','5005000000PLPHHAA5','5005000000PLPHHAA5','5005000000PLPHHAA5','5005000000PLPHHAA5','5005000000Nt2cQAAR','5005000000MHs88AAD','5005000000MHs88AAD','5005000000MHs88AAD','5005000000ObdpUAAR','5005000000ObdpUAAR','5005000000PIaiFAAT','5005000000PM4o9AAD','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000Oaep6AAB','5005000000Oaep6AAB','5005000000Oaep6AAB','5005000000Oaep6AAB','5005000000Oaep6AAB','5005000000Oaep6AAB','5005000000Oaep6AAB','5005000000PKKe5AAH','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000NDTIUAA5','5005000000PLfGtAAL','5005000000PLfGtAAL','5005000000PK5qcAAD','5005000000PK5qcAAD','5005000000Q3L4JAAV','5005000000NDTIUAA5','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000PMCrMAAX','5005000000Oc1ZiAAJ','5005000000Q2nITAAZ','5005000000Q2nITAAZ','5005000000Q2nITAAZ','5005000000Q2nITAAZ','5005000000Q2nITAAZ','5005000000Q2nITAAZ','5005000000Q37hAAAR','5005000000Q2TrvAAF','5005000000Q38olAAB','5005000000Q38olAAB','5005000000ObgbIAAR','5005000000Q2nYHAAZ','5005000000Q35sAAAR','5005000000PLkqcAAD','5005000000Q35sAAAR','5005000000Q2D8GAAV','5005000000PJ9fhAAD','5005000000PJ9fhAAD','5005000000PLQonAAH','5005000000PL6CdAAL','5005000000Oaep6AAB','5005000000LTjr5AAD','5005000000Q333rAAB','5005000000PIR7yAAH','5005000000Q2D8GAAV','5005000000PKV79AAH','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000PKV79AAH','5005000000Q3S6FAAV','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000PLhvaAAD','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000PIV4HAAX','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000ObAsCAAV','5005000000PLiynAAD','5005000000PJTTyAAP','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PLkavAAD','5005000000Q32WeAAJ','5005000000Q32WeAAJ','5005000000PLkqcAAD','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000PJKXQAA5','5005000000Q3872AAB','5005000000NuUUKAA3','5005000000PIV4HAAX','5005000000PLkqcAAD','5005000000PLkqcAAD','5005000000PKV79AAH','5005000000PKV79AAH','5005000000PKV79AAH','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000HmjZZAAZ','5005000000HmjZZAAZ','5005000000Q3EwJAAV','5005000000OZhBOAA1','5005000000PItyYAAT','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q38olAAB','5005000000Q38olAAB','5005000000PLkavAAD','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000OZhBOAA1','5005000000Q2nITAAZ','5005000000PLkavAAD','5005000000PLkavAAD','5005000000NuQ1FAAV','5005000000NuQ1FAAV','5005000000NuQ1FAAV','5005000000NuQ1FAAV','5005000000NuQ1FAAV','5005000000Q38olAAB','5005000000Q38olAAB','5005000000OZhBOAA1','5005000000Q39GBAAZ','5005000000Q39GBAAZ','5005000000Q37hAAAR','5005000000Q37hAAAR','5005000000Q37hAAAR','5005000000Q37hAAAR','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000Q2jskAAB','5005000000ObdpUAAR','5005000000OYn8PAAT','5005000000PKvjPAAT','5005000000Q37hAAAR','5005000000Q37hAAAR','5005000000Q2nVNAAZ','5005000000Q333rAAB','5005000000Oc1ZiAAJ','5005000000NFFkHAAX','5005000000Q333rAAB','5005000000Q3EwJAAV','5005000000PLv7KAAT','5005000000Q3QjzAAF','5005000000ObfpTAAR','5005000000ObdpUAAR','5005000000PJDpsAAH','5005000000PLiDbAAL','5005000000OZFA4AAP','5005000000Q2jHZAAZ','5005000000PL4K8AAL','5005000000PLPHHAA5','5005000000Q2neoAAB','5005000000NG7uXAAT','5005000000Nt2cQAAR','5005000000PLWbIAAX','5005000000Q2jHZAAZ','5005000000Nua8hAAB','5005000000Nua8hAAB','5005000000PJTTyAAP','5005000000PLPHHAA5','5005000000ObJzfAAF','5005000000Q3LqbAAF','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000Q333rAAB','5005000000Q333rAAB','5005000000Q333rAAB','5005000000Q2D8GAAV','5005000000Q2K6RAAV','5005000000PKvjPAAT','5005000000PKvjPAAT','5005000000PIaiFAAT','5005000000PIaiFAAT','5005000000PIaiFAAT','5005000000PIaiFAAT','5005000000PIaiFAAT','5005000000NFFkHAAX','5005000000Q333rAAB','5005000000PIQpCAAX','5005000000PIQpCAAX','5005000000PIQpCAAX','5005000000PJi9IAAT','5005000000PJi9IAAT','5005000000Q37hAAAR','5005000000Q2neoAAB','5005000000Q2nYHAAZ','5005000000Q3EwJAAV','5005000000Q2neoAAB','5005000000PLkDxAAL','5005000000Q2nYHAAZ','5005000000PLiynAAD','5005000000PLiynAAD','5005000000PLiynAAD','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000Q333rAAB','5005000000Q333rAAB','5005000000Q37hAAAR','5005000000Q2neoAAB','5005000000PKvjPAAT','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nITAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000LUj7uAAD','5005000000PKvjPAAT','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000Q37hAAAR','5005000000OYfRsAAL','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000OZhBOAA1','5005000000Q2nITAAZ','5005000000Q2nITAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q2nYHAAZ','5005000000Q38oDAAR','5005000000Q38olAAB','5005000000OYfRsAAL','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000Q2dtoAAB','5005000000Q2dtoAAB','5005000000Q2t6sAAB','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000KkqcFAAR','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000NuUUKAA3','5005000000Q37hAAAR','5005000000OZhBOAA1','5005000000Q35sAAAR','5005000000PLv7KAAT','5005000000PLv7KAAT','5005000000PLv7KAAT','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000Q35sAAAR','5005000000PKV79AAH','5005000000PIMlrAAH','5005000000Q35sAAAR'};
        return Database.getQueryLocator('Select c.Next_Attachment_Integration_Number__c,c.isUpdated__c, c.CaseNumber,Assignee__r.Username,Assignee__c,Account.RecordTypeId , (Select IsDeleted, Name, CreatedDate, Attachment_Id__c, Case_Id__c, Source__c, Attachment_Number__c From Attachments_Integration__r where IsDeleted = false and Attachment_Id__c != NULL order by CreatedDate), (Select Id, IsDeleted, ParentId, Name From Attachments where IsDeleted = false) From Case c where  (Assignee__c != NULL or State__c != \'Closed\') and ( Assignee__c != NULL or State__c != \'Assigned\') and Account.RecordTypeId = \'01250000000DP1n\' and c.isUpdated__c=true and c.Id IN:set_Case order by CaseNumber');
        
    } 
      
    global void execute(Database.BatchableContext BC, list<Case> scope)
    {
        list<AttachmentIntegration__c>   lst_attInToUpdate      = new list<AttachmentIntegration__c> ();
        list<AttachmentIntegration__c>   lst_attInCopyToRedM    = new list<AttachmentIntegration__c> ();
        map<Id,AttachmentIntegration__c> map_attachmentToattachmentIntegration = new map<Id,AttachmentIntegration__c>();
        system.debug('==>scope size: '+scope.size());
           
        for(Case ca:scope)
        { 
            if(!ca.Attachments_Integration__r.isEmpty() && ca.Attachments_Integration__r.size() >0)
            {
                system.debug('==>ca.Attachments_Integration__r size: '+ca.Attachments_Integration__r.size());   
                
                integer counter=0;
                for( AttachmentIntegration__c ati: ca.Attachments_Integration__r)
                {
                    if(ati.Source__c == 'Copy to Redmine')
                    {
                        if(!ca.Attachments.isEmpty())
                        {
                            for(Attachment att:ca.Attachments)
                            {
                                if(ati.Attachment_Id__c == att.id )
                                {
                                    if( att.Name.length() > 80)
                                        ati.name = att.Name.substring(0,79) ;
                                    else ati.name = att.Name;
                                    
                                    system.debug('==> ati '+ati);
                                }
                            }
                        }
                        lst_attInToUpdate.add(ati);
                        lst_attInCopyToRedM.add(ati);
                    }
                    else
                    {
                        
                        if(map_attachmentToAttachmentIntegration.get(ati.Attachment_Id__c)==null)
                            map_attachmentToAttachmentIntegration.put(ati.Attachment_Id__c,ati);
                        
                        ati.Attachment_Number__c = counter ;
                        counter = counter+1 ;
                        lst_attInToUpdate.add(ati);
                        
                        system.debug('==> ati '+ati);
                        system.debug('==> counter '+counter);
                    }
                }
                ca.Next_Attachment_Integration_Number__c = counter ;
                //ca.isUpdated__c = true ;
                
            }
        }
                
        update scope; 
        if(!lst_attInToUpdate.isEmpty())
            update lst_attInToUpdate;
            
        system.debug('==>lst_attInToUpdate: '+lst_attInToUpdate);   
        system.debug('==>lst_attInToUpdate size: '+lst_attInToUpdate.size());   
        
        lst_attInToUpdate = new list<AttachmentIntegration__c> ();
        
        if(!lst_attInCopyToRedM.IsEmpty() && lst_attInCopyToRedM.size()>0)
        {
            for(AttachmentIntegration__c aic :lst_attInCopyToRedM)
            {
                if(map_attachmentToattachmentIntegration.get(aic.Attachment_Id__c) != null)
                {
                    aic.Attachment_Number__c = map_attachmentToattachmentIntegration.get(aic.Attachment_Id__c).Attachment_Number__c ;
                    aic.name = map_attachmentToattachmentIntegration.get(aic.Attachment_Id__c).name ;
                    lst_attInToUpdate.add(aic);
                }
            }
        }
        if(!lst_attInToUpdate.isEmpty())
            update lst_attInToUpdate;
        system.debug('==>lst_attInToUpdate: '+lst_attInToUpdate);   /*
        for(Case ca:scope)
        { 
            if(!ca.Attachments_Integration__r.isEmpty() && ca.Attachments_Integration__r.size() >0)
            {
                system.debug('==>ca.Attachments_Integration__r size: '+ca.Attachments_Integration__r.size());   
                
                Decimal counter=ca.Next_Attachment_Integration_Number__c;
                for( AttachmentIntegration__c ati: ca.Attachments_Integration__r)
                {
                	system.debug('==> ati.Attachment_Number__c: '+ati.Attachment_Number__c);
                    if(ati.Source__c == 'Copy to Redmine')
                    {
                        if(!ca.Attachments.isEmpty())
                        {
                            for(Attachment att:ca.Attachments)
                            {
                                if(ati.Attachment_Id__c == att.id )
                                {
                                    if( att.Name.length() > 80)
                                        ati.name = att.Name.substring(0,79) ;
                                    else ati.name = att.Name;
                                    system.debug('==> ati.name '+ati.name);
                                }
                            }
                        }
                        lst_attInToUpdate.add(ati);
                        lst_attInCopyToRedM.add(ati);
                        system.debug('==> lst_attInToUpdate '+lst_attInToUpdate);
                        system.debug('==> lst_attInCopyToRedM '+lst_attInCopyToRedM);
                    }
                    else
                    {
                        
                        if(map_attachmentToAttachmentIntegration.get(ati.Attachment_Id__c)==null)
                            map_attachmentToAttachmentIntegration.put(ati.Attachment_Id__c,ati);
                        
                        ati.Attachment_Number__c = counter ;
                        counter++;
                        lst_attInToUpdate.add(ati);
                        
                        system.debug('==>ati.Attachment_Number__c '+ati.Attachment_Number__c);
                    }
                }
                //ca.Next_Attachment_Integration_Number__c = counter ;
                //ca.isUpdated__c=true;
            } 
        } 
                 
        update scope; 
        if(!lst_attInToUpdate.isEmpty())
            update lst_attInToUpdate;
            
        system.debug('==>lst_attInToUpdate: '+lst_attInToUpdate);   
        system.debug('==>lst_attInToUpdate size: '+lst_attInToUpdate.size());   
        
        lst_attInToUpdate = new list<AttachmentIntegration__c> ();
        
        if(!lst_attInCopyToRedM.IsEmpty() && lst_attInCopyToRedM.size()>0)
        {
            for(AttachmentIntegration__c aic :lst_attInCopyToRedM)
            {
                if(map_attachmentToattachmentIntegration.get(aic.Attachment_Id__c) != null)
                {
                    aic.Attachment_Number__c = map_attachmentToattachmentIntegration.get(aic.Attachment_Id__c).Attachment_Number__c ;
                    aic.name = map_attachmentToattachmentIntegration.get(aic.Attachment_Id__c).name ;
                    lst_attInToUpdate.add(aic);
                    system.debug('==>aic.Attachment_Number__c '+aic.Attachment_Number__c);
                    system.debug('==>aic.name '+aic.name); 
                }
            }
        }
        if(!lst_attInToUpdate.isEmpty())
            update lst_attInToUpdate;
        system.debug('==>lst_attInToUpdate: '+lst_attInToUpdate);   */
    }
    
    global void finish(Database.BatchableContext BC)
    {
    }
    global void execute(SchedulableContext ctx)
    {
        ScheduleUpdateOldAtt batch = new ScheduleUpdateOldAtt();
        Database.executeBatch(batch,5);            
    }
}