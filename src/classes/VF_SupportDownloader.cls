public class VF_SupportDownloader {
	
	public List<selectOption> productFamily {get; set;}
    public List<selectOption> productCategory {get; set;}
    public List<selectOption> productDetailsList {get; set;}
    public List<selectOption> revisionList {get; set;}
    public List<selectOption> contentFilesList {get; set;}
    public List<selectOption> contentFilesListManuals {get; set;}
    public List<selectOption> searchedDetailsList {get; set;}
    public list<selectOption> allVerions_List {get;set;}
    public list<ContentVersion> generalFilesFromDetails_List {get;set;}
    public boolean displayGeneralFilesSection {get;set;}
    public boolean resetContents {get;set;}
     public boolean resetVersions {get;set;}
    
    
 	public Id chosenFamilyId {get;set;}
 	public Id chosenCatId {get;set;}
 	public String chosenDetId {get;set;}
 	public String chosenRevisionId {get;set;}
 	public Id chosenContentFileId  {get;set;}
 	public String choenSearchedProductDetail {get;set;}
 	public String choendProductFromResults {get;set;}
 	

	public VF_SupportDownloader() {
		
	}
	
	public List<selectOption> getTheproductFamily() {
		
		list<ProductFamily__c> productFamily_List = [SELECT Id, Name FROM ProductFamily__c];
		productFamily = new List<SelectOption>();
		system.debug('productFamily_List : ' + productFamily_List);
		
		for (ProductFamily__c fam : productFamily_List) {
			
			 productFamily.add( new SelectOption(fam.Id, fam.Name) );
		}
		
		return productFamily;
	}
	
	
	public List<selectOption> getTheproductCategory() {
		
		List<Product_Category__c> productCategory_List = [SELECT Id, Name FROM Product_Category__c WHERE Product_Family__r.Id =: chosenFamilyId];
		productCategory = new List<SelectOption>();
		
		if (!productCategory_List.isEmpty()) {
			for (Product_Category__c cat : productCategory_List) {
				
				 productCategory.add( new SelectOption(cat.Id, cat.Name) );  
			}
		}
		
		else {
			
			productCategory.add( new SelectOption('Please choose a Family', 'Please choose a Family') ); 
		}
		
		return productCategory;
	}
	
	public List<selectOption> getTheproductDetail() {
		
		List<ProductDetails__c> productDetails_List = [SELECT Id, Name FROM ProductDetails__c WHERE Product_Category__r.Id =: chosenCatId];
		productDetailsList = new List<SelectOption>();
		
		system.debug('chosenCatId : ' + chosenCatId);
		system.debug('productDetails_List : ' + productDetails_List);
		
		if (!productDetails_List.isEmpty()) {
			for (ProductDetails__c det : productDetails_List) {
				
				 productDetailsList.add( new SelectOption(det.Name, det.Name) );  
			}
		}
		
		else {
			
			productDetailsList.add( new SelectOption('Please choose a Category', 'Please choose a Category') ); 
		}
		
		return productDetailsList;
	}
	
	public List<selectOption> getTheRevision() {
		
		List<ContentVersion> version_List = [SELECT Id, Doc_SW_Revision__c FROM ContentVersion WHERE Product__c != null AND Product__c =: chosenDetId];
		
		system.debug('chosenDetId : ' + chosenDetId);
		system.debug('version_List : ' + version_List);
		
		revisionList = new List<SelectOption>();
		
		if (!version_List.isEmpty() && !resetVersions) {
			for (ContentVersion ver : version_List) {
				
				 if (ver.Doc_SW_Revision__c != null) {	
				 	
				 	revisionList.add( new SelectOption(ver.Doc_SW_Revision__c, ver.Doc_SW_Revision__c) ); 
				 } 
			}
		}
		
		else {
			
			revisionList.add( new SelectOption('Please choose a Product', 'Please choose a Product') ); 
		}
		
		return revisionList;
	}
	
	public List<selectOption> getTheContentFile() {
		
		List<ContentVersion> contentFiles_List = [SELECT Id, Title FROM ContentVersion 
												  WHERE Doc_SW_Revision__c != null AND Doc_SW_Revision__c =: chosenRevisionId and Display_type__c =: 'Downloads'];
		
		system.debug('contentFiles_List : ' + contentFiles_List);
		 
		contentFilesList = new List<SelectOption>();
		
		if (!contentFiles_List.isEmpty() && !resetContents) {
			
			for (ContentVersion ver : contentFiles_List) {
				
				 if (ver.Title != null) {	
				 	
			 		contentFilesList.add( new SelectOption('', '') );
			 		contentFilesList.add( new SelectOption(ver.Id, ver.Title) );
				 } 
			}
		}
		
		else {
			
			contentFilesList.add( new SelectOption('', '') ); 
		}
		
		return contentFilesList;
	}
	
	public List<selectOption> getTheContentFileManual() {
		
		List<ContentVersion> contentFiles_List = [SELECT Id, Title FROM ContentVersion 
												  WHERE Doc_SW_Revision__c != null AND Doc_SW_Revision__c =: chosenRevisionId and Display_type__c =: 'Manuals'];
		
		system.debug('contentFiles_List : ' + contentFiles_List);
		 
		contentFilesListManuals = new List<SelectOption>();
		
		if (!contentFiles_List.isEmpty() && !resetContents) {
			
			for (ContentVersion ver : contentFiles_List) {
				
				 if (ver.Title != null) {	
				 	
			 		contentFilesListManuals.add( new SelectOption('', '') );
			 		contentFilesListManuals.add( new SelectOption(ver.Id, ver.Title) );
				 } 
			}
		}
		
		else {
			
			contentFilesListManuals.add( new SelectOption('', '') ); 
		}
		
		return contentFilesListManuals;
	}
	
	
	public list<SelectOption> getTheRetrieveDetailsResults() {
		
		allVerions_List = new list<selectOption>();
		
		list<ProductDetails__c> details_List = new list<ProductDetails__c>();
		
		if (choenSearchedProductDetail != null && choenSearchedProductDetail.length() > 0) {
			
			details_List = [SELECT Id, Name FROM ProductDetails__c WHERE Name like : '%' + choenSearchedProductDetail +'%'];
			
			system.debug('details_List : ' + details_List);
			
			for (ProductDetails__c  det :  details_List) {
				
				allVerions_List.add( new SelectOption(det.Name, det.Name) );
			}
		}
		
		return allVerions_List;
	}
	
	
	public void fillUpProductCategory() {
		
		resetVersions = true;
		resetContents = true;
		List<Product_Category__c> productCategory_List = [SELECT Id, Name FROM Product_Category__c WHERE Product_Family__r.Id =: chosenFamilyId];
		
		productCategory = new List<SelectOption>();
				
		for (Product_Category__c cat : productCategory_List) {
			
			 productCategory.add( new SelectOption(cat.Id, cat.Name) );
		}
	}
	
	public void fillUpProductDetail() {
		
		resetVersions = true;
		resetContents = true;
		List<ProductDetails__c> productDetails_List = [SELECT Id, Name FROM ProductDetails__c WHERE Product_Category__r.Id =: chosenCatId];
		productDetailsList = new List<SelectOption>();
	
		for (ProductDetails__c det : productDetails_List) {
			
			 productDetailsList.add( new SelectOption(det.Name, det.Name) );  
		}		
	}
	
	public void fillUpRevision() {
		
		resetVersions = false;
		List<ContentVersion> version_List = [SELECT Id, Doc_SW_Revision__c FROM ContentVersion WHERE Product__c =: chosenDetId];
		
		system.debug('chosenDetId : ' + chosenDetId);
		system.debug('version_List : ' + version_List);
		
		revisionList = new List<SelectOption>();
	
		for (ContentVersion ver : version_List) {
			
			if (ver.Doc_SW_Revision__c != null) {	
			    
			    revisionList.add( new SelectOption(ver.Doc_SW_Revision__c, ver.Doc_SW_Revision__c) ); 
			} 
		}
		
		resetContents = true;
		
	}
	
	public void fillUpContentFile() {
		
		resetContents = false;
		List<ContentVersion> contentFiles_List = [SELECT Id, Title, Display_type__c FROM ContentVersion 
												  WHERE Doc_SW_Revision__c != null AND Doc_SW_Revision__c =: chosenRevisionId];
		
		contentFilesList = new List<SelectOption>();
		contentFilesListManuals = new List<SelectOption>();
		
		if (!contentFiles_List.isEmpty()) {
			for (ContentVersion ver : contentFiles_List) {
				
				 if (ver.Title != null && ver.Display_type__c != null) {	
				 	
				 	if (ver.Display_type__c.EqualsIgnoreCase('Downloads')) {	
				 		
				 		contentFilesList.add( new SelectOption(ver.Id, ver.Title) ); 
				 	}
				 	
				 	else {
				 		
				 		contentFilesListManuals.add( new SelectOption(ver.Id, ver.Title) );
				 	}
				 } 
			}
		}
		
		else {
			
			contentFilesList.add( new SelectOption('', '') );
			contentFilesListManuals.add( new SelectOption('', '') ); 
		}
	}
		
	public pageReference downloadTheDoc() {
		
		if (chosenContentFileId != null) {	
			String theUrl = '/sfc/servlet.shepherd/version/download/' + chosenContentFileId;
			PageReference thePage = new Pagereference(theUrl );
	        thePage.setRedirect(true);
	        return thePage;
		}
		return null;
	}	
	
	public void getSearchProductsResults() {
		
		allVerions_List = new list<selectOption>();
		
		list<ProductDetails__c> details_List = new list<ProductDetails__c>();
		
		
		if (choenSearchedProductDetail != null && choenSearchedProductDetail.length() > 0) {
			
			details_List = [SELECT Id, Name FROM ProductDetails__c WHERE Name like : '%' + choenSearchedProductDetail +'%'];
			
			system.debug('details_List 287 : ' + details_List);
			
			for (ProductDetails__c  det :  details_List) {
				
				allVerions_List.add( new SelectOption(det.Name, det.Name) );
			}
		}
	}
	
	public void fillUpGeneralFilesRelatedToDetails() {
		
		generalFilesFromDetails_List = [SELECT Id, Title FROM  ContentVersion WHERE Product__c != null AND Product__c =: choendProductFromResults];
		
		system.debug('choendProductFromResults : ' + choendProductFromResults);
		system.debug('generalFilesFromDetails_List : ' + generalFilesFromDetails_List);
		
		
		displayGeneralFilesSection = (!generalFilesFromDetails_List.isEmpty()) ? true : false;
	}
}