public class VF_RMA_Domain_foxconn {
	
	public Case theCase {get;set;}
    public boolean is_foxconn {get;set;}

    public VF_RMA_Domain_foxconn(ApexPages.standardController controller) 
    { 
        is_foxconn = false;
        Id caseId  = controller.getId();
        
        theCase = [SELECT Id, RMA_Request__c FROM Case WHERE Id =: caseId];
        
	    if (theCase.RMA_Request__c != null) { 
	       
	        RMA__c theRMA = [SELECT id, Domain__c, (SELECT Id, Asset_End_Customer_Account_Name__c FROM Serial_Numbers__r WHERE Asset_End_Customer_Account_Name__c =: 'EMC') FROM RMA__c WHERE Id =: theCase.RMA_Request__c];
	        
	        if ( theRMA.Domain__c != null && theRMA.Domain__c.Contains('foxconn'))  { 
	            
	           if (!theRMA.Serial_Numbers__r.isEmpty()) {
	            	
	            	is_foxconn = true;
	            }	
	        }
	    }
    }
}