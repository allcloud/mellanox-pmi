public with sharing class VF_RMA_Case_canceled 
{   
      
    public ID caseId;
    
    public VF_RMA_Case_canceled (ApexPages.StandardController controller) 
         {    
             //id = Apexpages.currentPage().getParameters().get('Id');    
         }

    
    
    
    public PageReference Reject_RMA() 
    //public void getDeliverAsPDF_InProcess()
    {
    
        // Reference the page, pass in a parameter to force PDF
         caseId = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         system.debug('id is : ' + caseId);
         //get the related RMA id from the case
         Case c = [Select c.state__c, c.RMA_Reject_Reason__c  From Case c WHERE c.id =: caseId];
         if(c.state__c ==  'Closed')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'The case was already closed'));
         return Apexpages.currentPage();
         }
         
         if(c.RMA_Reject_Reason__c== NULL)
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please fill in the \'RMA Cancelation Reason\' field'));
         return Apexpages.currentPage();
         }

  
                           
         c.state__c = 'RMA Canceled';
         update c;
          return new Pagereference('/'+caseId);
     }

/* EK
 static testMethod void Test_VF_RMA_Letter_ReqInProcess_CallerFrmCase() 
    {
        //list of serial numbers to be related to the RMA and to avoid many inserts
        List<Serial_Number__c> lst_snOfRMA = new List<Serial_Number__c>();
        
        List<Product2> lst_prods = New List<Product2>();
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Account acc = obj.createAccount();
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        con.email = 'hgft56@er.com';
        insert con;
        
       // RMA__c rma = obj.CreateRMA();
        //insert rma;
        
        MellanoxSite__c  site  = obj.createMellanoxSite();
        insert site;
         
        RMA__c rma = obj.CreateRMA();
        rma.Mellanox_Site__c = site.id;
        insert rma;
        
    
        Product2 p1 =  obj.createProduct();
        p1.Inventory_Item_id__c = '90989';
        lst_prods.add(p1);
        
                
        Serial_Number__c sn1 = obj.createSerialNumber(rma,p1);
        lst_snOfRMA.add(sn1);
        
                
        insert lst_prods;
        insert lst_snOfRMA; 
        
        Case rmaCase = obj.CreateRMA_case(acc, con, rma);
        
       // rmaCase.State__c = 'Assigned';
        insert rmaCase;
        
        Case rmaCaseSelected = [Select id,State__c,c.RMA_Request__c, c.RMA_Request__r.Name,RMA_reject_reason__c  from Case c Where Id =: rmaCase.Id ];
        rmaCaseSelected.OwnerId = '005500000013ipV';
        rmaCaseSelected.Assignee__c = '005500000013ipV';
        rmaCaseSelected.RMA_reject_reason__c = 'No Reason';
        rmaCaseSelected.state__c ='Assigned';
        update rmaCaseSelected;
        
        system.debug('Babcom rmaCase display' + rmaCase);
        
       // PageReference pdf =  Page.VF_RMA_Letter_ReqInProcess_CallerFrmCase;
        
       // pdf.getParameters().put('id',rmaCase.id);
              
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.VF_OEM_RMA_Cancel')); 
        System.currentPageReference().getParameters().put('id',rmaCaseSelected.Id );
        System.currentPageReference().getParameters().put('isTest','Testing' );  
                   
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rmaCaseSelected);
        VF_RMA_Case_canceled vf = new VF_RMA_Case_canceled (teststandard);
        vf.Reject_RMA();
        
        Test.stopTest();
    } */



     
     }