global class Batch_Merge_Contacts implements  Database.Batchable<Contact>{
	
	global Iterable<Contact> start(Database.BatchableContext BC) {
    
    	list<Contact> theContact_List = [SELECT Id,FirstName, LastName, Email, Phone, MobilePhone, Company_Name_from_Web__c, Product_Purchase_Date__c, Email_for_merge__c,
    									 purchase_Mellanox_equipment_from__c, Contact_Need_to_be_merged__c, Serial_Number__c, Comment__c,
    									 (SELECT Id, ContactId FROM Cases order by createdDate) FROM Contact WHERE 
    									 Contact_Need_to_be_merged__c = true and Email_for_merge__c != null];

											       											      
		return 	theContact_List;							       
     }
     
     global void execute(Database.BatchableContext BC, List<Contact> scope) {
     	
     	map<Id,SObject> obj2Update_Map = new map<Id, SObject>();
      	list<Contact> contacts2Delete_List = new list<Contact>();
     	
     	set<String> contactsEmails_Set = new set<String>();
     	set<Id> contactsIds_Set = new set<Id>();
    	
    	for (Contact newContact : scope) {
    		
    		contactsEmails_Set.add(newContact.Email_for_merge__c);
    		contactsIds_Set.add(newContact.Id);
    	}
    	
    	if(!contactsEmails_Set.isEmpty()) {
    		
    		map<String, Contact> contactsEmail2Contacts_Map = new map<String, Contact>();
    		list<Contact> relatedContactsByEmail_List = [SELECT Id, FirstName, LastName, Email, Phone, MobilePhone, Company_Name_from_Web__c,
    													 Product_Purchase_Date__c, Email_for_merge__c,
    													 purchase_Mellanox_equipment_from__c, Contact_Need_to_be_merged__c, Serial_Number__c, 
    													 Comment__c FROM Contact WHERE Email IN : contactsEmails_Set];
    													 
    		    												 
    		
    		for (Contact con : relatedContactsByEmail_List) {
    			
    			contactsEmail2Contacts_Map.put(con.Email, con);
    		}
    		
    		for (Contact newContact : scope) {
    			
    			if (contactsEmail2Contacts_Map.containsKey(newContact.Email_for_merge__c)) {
    				
    				Contact masterContact = contactsEmail2Contacts_Map.get(newContact.Email_for_merge__c);
    				masterContact.FirstName = newContact.FirstName;
    				masterContact.LastName = newContact.LastName;
    				masterContact.Phone = newContact.Phone;
    				masterContact.MobilePhone = newContact.MobilePhone;
    				masterContact.Company_Name_from_Web__c = newContact.Company_Name_from_Web__c;
    				masterContact.Product_Purchase_Date__c = newContact.Product_Purchase_Date__c;
    				masterContact.purchase_Mellanox_equipment_from__c = newContact.purchase_Mellanox_equipment_from__c;
    				masterContact.Serial_Number__c = newContact.Serial_Number__c;
    				masterContact.Comment__c = newContact.Comment__c;
    				masterContact.Contact_Need_to_be_merged__c = false;
    				obj2Update_Map.put(masterContact.Id, masterContact);
    				contacts2Delete_List.add(newContact);
    				
    				if (!newContact.Cases.isEmpty()) {  	
    					  
    					for (Case relatedCase : newContact.Cases) {
	    					
		    				relatedCase.ContactId = masterContact.Id;
		    				obj2Update_Map.put(masterContact.Id, relatedCase);  
    					}
    				}
    			}  
    		}   
			   
			
			if (!obj2Update_Map.values().isEmpty()) {
    				
				update obj2Update_Map.values();
			}
			
			if (!contacts2Delete_List.isEmpty()) {
				
				Database.DeleteResult[] lst_SR = Database.delete(contacts2Delete_List, false);
					
					
				for(Database.DeleteResult sr: lst_SR) {
			     
			       if(!sr.isSuccess()) {
			         
			       	   system.debug('==>delete sr.getMessage(): ' + sr.getErrors() + 'Id is :' + sr.getId());
			       }
			    }
			}
    	}
     }
     
     global void finish(Database.BatchableContext BC) {
     	   
     }
	

}