/**
 */
@isTest
private class Test_Custom_PB_trigger {

    static testMethod void myUnitTest() {
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Product2 prod4 = obj.createProduct();
        prod4.Name ='HARDWARE-1';
        prod4.Type__c = 'HW';
        prod4.ProductCode = 'FGTTFSS';
        prod4.Product_Type1__c = 'CABLES';
        prod4.Inventory_Item_id__c = '12121112';
        prod4.partNumber__c = 'dhfgeryte';
        prod4.OEM_PL__C=10000;
        prod4.IsActive = true;
        insert prod4;
        
        Pricebook2 pbk2 = obj.CreatePriceBook();
        pbk2.Name='OEM Price Book';
        insert pbk2;
        
        Pricebook2 pbk3 = obj.CreatePriceBook();
        pbk2.Name='Standard Price Book';
        insert pbk3;
        
        Custom_Price_Book__c custom_pb = new Custom_Price_Book__c(Period__c = 'Q4-2014',Product__c=prod4.Name,Unit_Price__c=10.99,PriceBook_Name__c='OEM Price Book');
        insert custom_pb;
        
        //Custom_Price_Book__c custom_pb2 = new Custom_Price_Book__c(Period__c = 'Q4-2014',Product__c=prod4.Name,Unit_Price__c=10.99,PriceBook_Name__c='Standard Price Book');
        //insert custom_pb2;
    }
}