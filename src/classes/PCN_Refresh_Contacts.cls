public class PCN_Refresh_Contacts {
	public PCN__c current_PCN;
	String old_PCN_type;
	List<PCN_Affected_Products__c> old_parts = new List<PCN_Affected_Products__c>();
	List<PCN_Affected_Products__c> new_parts = new List<PCN_Affected_Products__c>();
	public PCN_Refresh_Contacts (ApexPages.StandardController controller){
		current_PCN = [select id,PCN_Contacts_Refresh_Date__c,Type__c from PCN__c where id = :controller.getRecord().id];
		old_PCN_type = current_PCN.Type__c; 
	}
	public pagereference init(){
		old_parts = [select Product__c,Replacement_OPN__c,Current_Version__c,New_Version__c from PCN_Affected_Products__c where PCN__c = :current_PCN.id];
		for (PCN_Affected_Products__c p : old_parts) {
			new_parts.add(new PCN_Affected_Products__c (PCN__c=current_PCN.id,Product__c=p.Product__c,Replacement_OPN__c=p.Replacement_OPN__c,
														Current_Version__c = p.Current_Version__c,New_Version__c = p.New_Version__c));
		}
		
		if(old_parts != null && old_parts.size() > 0)
			Database.delete (old_parts, false);
		if(new_parts.size() > 0) {
			//Need to bring back to Private to comply w/ a validation on PCN Products
			current_PCN.Type__c = 'Private';
			update current_PCN;
			
			insert new_parts;
		}
		
		current_PCN.PCN_Contacts_Refresh_Date__c = System.today();
		current_PCN.Type__c = old_PCN_type;
		update current_PCN;
		
		PageReference pg = new PageReference('/'  + current_PCN.ID);
        pg.setRedirect(true);
        return pg;
	}
}