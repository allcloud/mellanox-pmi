global class PCN_Reminder_Emails_DAILY implements Schedulable{
	global void execute(SchedulableContext SC) {
		Date d = System.today().adddays(-14);
		Set<ID> PCN_IDs_haveComments_or_ApprovalRejection = new Set<ID>();
		List<PCN__c> pcn_updates = new List<PCN__c>();
				
		Map<ID,PCN__c> map_PCNs = new Map<ID,PCN__c>([select id,Send_Reminders_14_Days_Approval_Require__c,Send_Reminder_Date__c 
															from PCN__c 
															where Type__c='Approval Required' and Status__c='Pending'
															and Send_Email_Date__c != null and Send_Email_Date__c = :d]);
		if(map_PCNs == null || map_PCNs.size() == 0)
			return;
		
		List<PCN_Contacts__c> pcn_contacts = new List<PCN_Contacts__c>([select id,PCN__c,send_reminder_email__c,Contact_Email__c,
																				Contact_Name__c,PCN_Name__c,PCN__r.Send_Email_Date__c 
																			from PCN_Contacts__c
																			where PCN__c in :map_PCNs.keyset() and Contact__c != null
																			and (Approved__c = true or Rejected__c = true) 
																			]);
																			//and ( (Contact_Approver__c = 'Yes' and PCN_Approver_by_OPN_flag__c = false) OR is_Approver_by_OPN__c = true )]);
		List<PCN_Comment__c> pcn_comments = new List<PCN_Comment__c>([select id, Contact_Email__c,PCN__c from PCN_Comment__c
																		where PCN__c in :map_PCNs.keyset() ]);
																		//and Comments_from_Portal__c = true
																		//and (Contact_Approver__c = 'Yes' OR PCN_Contacts__r.is_Approver_by_OPN__c = true)]);
		if(map_PCNs.size()==0)
			return;
		for (PCN_Contacts__c p : pcn_contacts)
			PCN_IDs_haveComments_or_ApprovalRejection.add(p.PCN__c);	
		for (PCN_Comment__c p : pcn_comments)
			PCN_IDs_haveComments_or_ApprovalRejection.add(p.PCN__c);
		
		for (PCN__c p : map_PCNs.values()) {
			if( !PCN_IDs_haveComments_or_ApprovalRejection.contains(p.ID) ){
				p.Send_Reminders_14_Days_Approval_Require__c = true;
				p.Send_Reminder_Date__c = System.today();
				pcn_updates.add(p);
			}	
		}							
		
		if(pcn_updates.size()>0)
			update pcn_updates;
		
	}
}