/**************************************************************************************
* Description:
*   This is a batch class that creates Knowledge Articles using content from Solutions.
*
***************************************************************************************/
global class S2KStandard implements Database.batchable<sObject>, Database.Stateful{
        
    global final Batch_Job_Invoker__c bji;
    global final Integer MAX_FILE_CUSTOM_FIELDS = 5;
    global String solutionQuery;
    global String[] solutionFieldsArray = new String[]{};
    global String message = 'SUCCESS!';
    global Integer solutionsProcessed = 0;
    global Integer articlesCreated = 0;
            
    global S2KStandard(Batch_Job_Invoker__c bji){
        this.bji = bji;                
		
		//Get all fields of Solution
		Map<String, Schema.SObjectField> solutionFieldsMap = Schema.SObjectType.Solution.fields.getMap();
		String allSolutionFields = '';
		Integer index = 0;
		
		for (String s : solutionFieldsMap.keySet()){
			System.debug('**** DISPLAYING...solutionFieldsMap.get(\'' + s + '\') = ' + solutionFieldsMap.get(s).getDescribe().getName());			
			String fieldName = solutionFieldsMap.get(s).getDescribe().getName();
			if (!(fieldName.equals('IsOutOfDate') || fieldName.equals('ParentId'))){
				allSolutionFields += (index == 0) ? fieldName : ',' + fieldName;
				solutionFieldsArray.add(fieldName);			
				index++;
			}
		}
        
        solutionQuery = 'SELECT ' + allSolutionFields + ' FROM Solution WHERE isMigrated__c = false';  
        
        System.debug('SOLUTION QUERY: ' + solutionQuery);  
                    
        Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job In Progress');
        if (Database.getQueryLocator(solutionQuery) == null)
        	bjic =  new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job Completed');
        	
        update bjic;
    }
        
    //START
    global Database.queryLocator start(Database.BatchableContext ctx){           
        return Database.getQueryLocator(solutionQuery);
    }
    
    //EXECUTE
    global void execute(Database.BatchableContext ctx, List<Solution> solutionList){    	
    	Savepoint sp = null;
        try{
        	sp = Database.setSavepoint();
            String articleType = bji.Article_Type__c;
            Boolean publishAfterImport = true;           
            
            //TODO: Solution Fields will map 1:1 to Article Fields
            String[] solutionFieldTokens = solutionFieldsArray;           
             
            //We can have at most 15 File type fields on Article Type.
            String[] attachmentFieldTokens = new String[]{};
            String[] attachmentFieldTokensWithCSuffix = new String[]{};
             
            for (Integer i=0; i<MAX_FILE_CUSTOM_FIELDS; i++){
            	attachmentFieldTokens.add('Attachment' + (i+1));
            	attachmentFieldTokensWithCSuffix.add('Attachment' + (i+1) + '__c');
            }             
            
            //Collect all Solution IDs
            Set<ID> solutionIDs = new Set<ID>();
            for (Solution s : solutionList){
                solutionIDs.add(s.Id);
            }            
                        
            //Fetch Attachments for all Solutions
            Map<ID, Attachment> attachmentMap = new Map<ID, Attachment>([SELECT Body, Name, ContentType, ParentId from Attachment where ParentId in :solutionIDs]);
            List<Attachment> attachmentList = attachmentMap.values();
            Map<ID, String> solution2AttachmentMapping = new Map<ID, String>();
            
            for (Attachment a : attachmentList){
                Boolean solutionHasEntry = (solution2AttachmentMapping.get(a.ParentId) != null);
                String newValue = '';
                if (solutionHasEntry)
                    newValue = solution2AttachmentMapping.get(a.ParentId) + ',' + a.Id;
                else
                    newValue = a.Id;
                    
                solution2AttachmentMapping.put(a.ParentId, newValue);
            }  
            
            //TODO-NEW: Create Data Categories on the fly, using Solution Categories
            //Fetch Data Categories for all Solutions
            Map<ID, CategoryData> categoriesMap = new Map<ID, CategoryData>([Select RelatedSObjectId, CategoryNodeId from CategoryData where RelatedSObjectId in :solutionIDs]) ;
            List<CategoryData> categoryList = categoriesMap.values();
            Set<ID> categoryNodeIDs = new Set<ID>();
            Map<ID, String> solution2CategoryMapping = new Map<ID, String>();
            
            for (CategoryData cd : categoryList){
            	categoryNodeIDs.add(cd.CategoryNodeId);
                Boolean solutionHasEntry = (solution2CategoryMapping.get(cd.RelatedSObjectId) != null);
                String newValue = '';
                if (solutionHasEntry)
                    newValue = solution2CategoryMapping.get(cd.RelatedSObjectId) + ',' + cd.CategoryNodeId;
                else
                    newValue = cd.CategoryNodeId;
                    
                solution2CategoryMapping.put(cd.RelatedSObjectId, newValue);
            } 
            
            System.debug('&*&*&* solution2CategoryMapping: ' + solution2CategoryMapping);    
            
            //Fetch MasterLabel for all Data Categories
            List<CategoryNode> categoryNodes = [Select ParentId, MasterLabel from CategoryNode where Id in :categoryNodeIDs];
            Set<ID> parentCategoryNodeIDs = new Set<ID>();
            Map<ID, ID> category2ParentCategory = new Map<ID, ID>();
            Map<ID, String> categoryMasterLabelsMap = new Map<ID, String>();
            
            //Insert Category Node Master Labels into HashMap
            for (CategoryNode cn : categoryNodes){
            	parentCategoryNodeIDs.add(cn.ParentId);
            	category2ParentCategory.put(cn.Id, cn.ParentId);
            	categoryMasterLabelsMap.put(cn.Id, cn.MasterLabel);
            }
            
            //Insert *Parent* Category Node Master Labels into HashMap
            List<CategoryNode> parentCategoryNodes = [Select MasterLabel from CategoryNode where Id in :parentCategoryNodeIDs];
            for (CategoryNode cn : parentCategoryNodes){            	
            	categoryMasterLabelsMap.put(cn.Id, cn.MasterLabel);
            }
                        
            System.debug('&*&*&*&* categoryMasterLabelsMap: ' + categoryMasterLabelsMap);
            
            //Insert Articles
            List<SObject> articles2Insert = new List<Sobject>();
            System.debug('SOLUTION LIST SIZE: ' + solutionList.size());
            for (Solution s : solutionList){                        
                Schema.SObjectType targetType = Schema.getGlobalDescribe().get(articleType);        
                SObject myObj = targetType.newSObject();        
                
                System.debug('SOLUTION FIELD TOKENS: ' + solutionFieldTokens);
                for (Integer i=0; i<solutionFieldTokens.size(); i++){                	
                	String solutionFieldName = solutionFieldTokens[i];
                	String articleFieldName = solutionFieldName.endsWith('__c')?solutionFieldName:solutionFieldName + '__c';
                	
                	//Special handling for fields from installed packages (namespaces) - BEGIN			
					if (solutionFieldName.endsWith('__c')){
						articleFieldName = articleFieldName.replace('__', '00');						
						articleFieldName = articleFieldName.replace('00c', '__c');
						System.debug('solutionFieldName IF SECTION: ' + solutionFieldName);
						System.debug('articleFieldName IF SECTION: ' + articleFieldName);
					}
					else{						
						System.debug('solutionFieldName ELSE SECTION: ' + solutionFieldName);
						System.debug('articleFieldName ELSE SECTION: ' + articleFieldName);
					}            
					//Special handling for fields from installed packages (namespaces) - END   	
                	                	                	               
                    myObj.put(articleFieldName, (''+s.get(solutionFieldName.trim())).trim());
                }               
                                
                //Drive visibility of Article based on 'Published' statuses of Solution                
                myObj.put('IsVisibleInCsp', s.isPublished);
                myObj.put('IsVisibleInPkb', s.isPublishedInPublicKb);
                myObj.put('IsVisibleInPrm', s.isPublished);
                                
                myObj.put('Title', (String)s.get('SolutionName'));
                myObj.put('UrlName', genURLName((String)s.get('SolutionName')));
                
                //Move attachments as well if they exist
                String attachmentIds = solution2AttachmentMapping.get(s.Id);            
                if (attachmentIds != null){             
                    //Get the attachment IDs for this Solution
                    String[] attachIdTokens = attachmentIds.split(',', MAX_FILE_CUSTOM_FIELDS);
                    Integer index = 0;
                    for (String attachId : attachIdTokens){                 
                        myObj.put(attachmentFieldTokens[index]+'__Name__s', attachmentMap.get(attachId).Name);                  
                        myObj.put(attachmentFieldTokens[index]+'__ContentType__s', attachmentMap.get(attachId).ContentType);
                        myObj.put(attachmentFieldTokens[index]+'__Body__s', attachmentMap.get(attachId).Body);
                        index++;
                    }
                }
                
                articles2Insert.add(myObj);
                s.isMigrated__c = true;
            }   
            
            //Insert all Articles
            insert articles2Insert;    
            
            //Update Solutions w/ isMigrated = true.  To ensure that Solutions from successful previous batch(es) are not migrated again.
            update solutionList;
            
            //Solution Id to Article Id Mapping
            Map<ID, ID> solution2Article = new Map<ID, ID>();
            Integer solutionIndex = 0;
            for (Solution s : solutionList){
            	solution2Article.put(s.Id, articles2Insert[solutionIndex].Id);
            	solutionIndex++;
            }
            
            //Assign Data Categories to Articles            
            List<SObject> dataCategorySelections2Insert = new List<Sobject>();
            String articleTypeWithoutKAV = articleType.substring(0, articleType.indexOf('__kav'));
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(articleTypeWithoutKAV + '__DataCategorySelection');
            
            for (Solution s : solutionList){
            	String categoryIds = solution2CategoryMapping.get(s.Id);    
            	System.debug('>>>> Solution ID: ' + s.Id + ' ---- categoryIds: ' + categoryIds);        	
            	if (categoryIds != null){
            		//TODO: Remove magic number '10' below.  This is the max number of Categories a Solution can have
                    String[] categoryIdTokens = categoryIds.split(',', 10);
                                		
            		for (String categoryId : categoryIdTokens){
            			SObject myObj = targetType.newSObject();                 
                        myObj.put('ParentId', solution2Article.get(s.Id));                                                
                        myObj.put('DataCategoryName', genCategorization(categoryMasterLabelsMap.get(categoryId)));                        
                        //myObj.put('DataCategoryGroupName', category2ParentCategory.get(categoryId)==null?articleTypeWithoutKAV:genCategorization(categoryMasterLabelsMap.get(category2ParentCategory.get(categoryId))));
                        myObj.put('DataCategoryGroupName', articleTypeWithoutKAV);                        
                        dataCategorySelections2Insert.add(myObj);                       
                    }               	 
            	}           	
            }           
            
            System.debug('>>>Data Category Selection LIST: ' + dataCategorySelections2Insert);
            
            //TODO: Bulk Testing 
            insert dataCategorySelections2Insert;			
            
            //If publishAfterImport = TRUE, call the publish method.            
            if (publishAfterImport){
                Set<ID> objectIds = new Set<ID>();
                for (SObject article : articles2Insert){                	                	
                    objectIds.add(article.Id);          
                }       
                
                //Query below has been modified to publish only those Articles with Solution status (i.e. Status__c) = Reviewed.
                String draftArticlesQuery = 'SELECT Id, KnowledgeArticleId FROM ' + articleType + ' WHERE PublishStatus=\'Draft\' and Status__c=\'Reviewed\' and Id in :objectIds';
                List<SObject> draftArticles = Database.query(draftArticlesQuery);
                
                for (SObject myDraft : draftArticles){
                    KbManagement.PublishingService.publishArticle(((ID)myDraft.get('KnowledgeArticleId')), true);
                }       
            }
            
            solutionsProcessed += solutionList.size();
            articlesCreated += articles2Insert.size();
        }
        catch (Exception e){
            Database.rollback(sp);
            Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job Completed');
        	update bjic;         
            message = e.getTypeName() + ' ' + e.getMessage() + ' at: ' + e.getStackTraceString() + '.  This batch has been rolled back.';           
        }           
    }    
    
    //FINISH     
    global void finish(Database.BatchableContext ctx){      
        Batch_Job_Invoker__c bjic = new Batch_Job_Invoker__c(Id = bji.Id, Status__c = 'Apex Batch Job Completed');
        update bjic;         
        
        AsyncApexJob a = [SELECT id, ApexClassId,
                        JobItemsProcessed, TotalJobItems,
                        NumberOfErrors, CreatedBy.Email FROM AsyncApexJob WHERE id = :ctx.getJobId()];      
        
        //TODO: Use an email template, don't HARDCODE message body
        String emailMessage = 'Your batch job Solutions to Knowledge (Standard) has completed.'; 
                                
        emailMessage += '<br/><br/>Number of Solutions Processed: ' + solutionsProcessed + '.  Number of Articles created and/or published: ' + articlesCreated + '.';
        
        emailMessage += '<br/><br/>' + message;                     
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {a.createdBy.email};
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSenderDisplayName('KBApps S2K');
        mail.setSubject('Solutions to Knowledge (Standard) - COMPLETED');
        mail.setPlainTextBody(emailMessage);
        mail.setHtmlBody(emailMessage);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ mail });   
    }    

	//Generate URL Name
	private String genURLName(String t) {		
		if (t == null)
			return '';
			
		t = t.replaceAll('[^a-zA-Z0-9]', '-').replaceAll('--', '-').replaceAll('--', '-').replaceAll('--', '-');
		t = t.removeEnd('-');
		t = t.removeStart('-');
		 
		return t;
	}
	
	//Generate Category API name from the Solution Category Master Label
	private String genCategorization(String t) {
		if (t == null)
			return '';
			
        t = t.replaceAll('[ ,/,\\-,(,),;,\\.]', '_').replaceAll('___', '_').replaceAll('__', '_').replaceAll('__', '_');
        if (t.lastIndexOf('_') == (t.length() - 1)) {
            t = t.substring(0, t.length() - 1);
        }

        return t;
    }	
}