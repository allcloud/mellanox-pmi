public with sharing class VF_Create_Opportunity_from_Reg 
{   
      
   public Distributor_Oppy_Registrations__c OppReg {get;set;}
   public Id theOppId {get;set;}    
      
    
        
        public VF_Create_Opportunity_from_Reg (ApexPages.StandardController controller) 
    {
            
           theOppId = Apexpages.currentPage().getParameters().get('id');
        
           OppReg = new Distributor_Oppy_Registrations__c();
            OppReg = [  Select  op.id,op.name, op.State_Full_Name__c, op.End_User_Country_opp_coversion__c,op.Region_for_Opp_Conversion__c, op.End_user_account__c,op.Partner_Reseller_Name__c,op.End_User__c, op.Partner_Account__c, op.Opportunity_Description__c,  op.Registration_Status_Calc__c, op.distributor__c,op.Opportunity_Close_Date__c,op.Opportunity_Ship_Date__c, op.End_User_Country__c       From Distributor_Oppy_Registrations__c op
                            WHERE op.Id =: theOppId];       
                             
        if(OppReg.Registration_Status_Calc__c != 'Approved')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Opportunity can be created only on Approved registration'));
          }
    
    }                    
                
               
     public Pagereference Cancel()
    {           
       return new Pagereference('/'+theOppId);       
    }     
       
    public Pagereference Save()
    {     
        //****************Validations*********************                
        if(OppReg.Registration_Status_Calc__c != 'Approved')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Opportunity can be created only on Approved registration'));
         return  Apexpages.currentPage();
         }
          
                                 
                    
        List<id> newProducts = new  List<id>();     
         Map<id,PricebookEntry> PE_products = new Map<id,PricebookEntry>(); 
         List<Opp_Reg_OPN__c> OppRegProds = [Select id,OPN__c,OPN_quantity__c FROM Opp_Reg_OPN__c  WHERE Opp_Reg__c =: OppReg.id];         
         for( Opp_Reg_OPN__c ORP:OppRegProds)      
          {newProducts.add (ORP.OPN__c);}         
          List<OpportunityLineItem> NewLineItems = new List<OpportunityLineItem>();                                                    
        Opportunity Opp = new Opportunity();
         opp.name = OppReg.name;
         opp.Distribution_Partner__c = OppReg.distributor__c;
         opp.Required_Ship_Date__c = OppReg.Opportunity_Close_Date__c;
         opp.closeDate =  OppReg.Opportunity_Ship_Date__c;
         opp.description = OppReg.Opportunity_Description__c;
         opp.AccountId = OppReg.End_user_account__c;  //OppReg.End_User_Account__c;
         opp.Opportunity_Registration__c = OppReg.id;
         opp.Primary_Partner_N__c = OppReg.Partner_Account__c; //OppReg.End_User_Account__c;
               
                   // { opp.Primary_Partner_N__c = OppReg.Partner_Account__c;}
         Opp.type = 'Reseller';
         Opp.recordTypeId = '01250000000Dtdw';
         Opp.stageName = 'Pipeline';
        // Opp.Pricebook2Id = '01s500000006J7PAAU'; // DB1 pricebook  
           Opp.Pricebook2Id = '01s5000000062WYAAY'; // Standard    
         Opp.Country_Region__c = OppReg.End_User_Country_opp_coversion__c ;  
         Opp.Sub_Channel__c = OppReg.Region_for_Opp_Conversion__c;
         Opp.State__c = OppReg.State_Full_Name__c;                             
         insert opp;       
     //    Opp.Pricebook2Id = '01s500000006J7PAAU';        
     //    update opp;                      
        for(PricebookEntry PE:[select id,Product2Id, UnitPrice from PricebookEntry where Product2Id in:newProducts AND Pricebook2Id = '01s5000000062WYAAY'])       //'01s500000006J7PAAU'])
     
            {       
              PE_products.put(PE.Product2Id,PE);        
                            
            }         
            for(Opp_Reg_OPN__c RegProd:OppRegProds)          
              {          
               OpportunityLineItem LI = new OpportunityLineItem();
               LI.Quantity = RegProd.OPN_Quantity__c;     
               LI.OpportunityId =Opp.id;        
               LI.pricebookentryId = PE_products.get(RegProd.OPN__c).id;            
               LI.totalPrice = PE_products.get(RegProd.OPN__c).UnitPrice * RegProd.OPN_Quantity__c ;          
               NewLineItems.add(LI);                                       
              }          
            insert  NewLineItems;         
            return new Pagereference('/'+Opp.Id);             
    }   

}