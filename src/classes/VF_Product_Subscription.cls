public class VF_Product_Subscription {
	
	public list<Product_Subscriptions__c> subscriptions_List {get;set;}
	public list<Document_Subscriptions__c> docsSubscriptions_List {get;set;}
	public list<DocumentSubscriptionBean> docsSubscriptionBeanList {get;set;}
	public Integer rowIndex {get;set;}
	public Integer rowIndex1 {get;set;}
	public Product_Subscriptions__c  subscriptionToDelete;
	public Integer totalCount {get;set;}
	public Integer totalDocsCount {get;set;}
	private map<Id, Product_Subscriptions__c> subscriptionsToDelete_Map {get;set;}
	private map<Id, Document_Subscriptions__c> docsSubscriptionsToDelete_Map {get;set;}
	private Id userId {get;set;}
	private final ProductDetails__c productDetail;
	
	
	
	/*********   INNER CLASS   **************/
	public class DocumentSubscriptionBean {
		
		public String relatedContentName {get;set;}
		public Document_Subscriptions__c subscription {get;set;}
	}
	/*********   INNER CLASS   **************/
	
	
	public VF_Product_Subscription(apexPages.StandardController controller) {
		
		userId = system.UserInfo.getUserId();
		subscriptionsToDelete_Map     = new map<Id, Product_Subscriptions__c>();
		docsSubscriptionsToDelete_Map = new map<Id, Document_Subscriptions__c>();
		
		// List of all the product subscriptions related to the current user and their Product types to display to the user
	    subscriptions_List = new list<Product_Subscriptions__c>
		([Select p.Id, p.Product__r.StaticResourceImage__c, p.Product__r.Product_Description__c,p.Product__r.Active__c, p.Product__r.Key_Features__c, p.is_subscribed__c,
		p.Product__r.Product_Category__c, p.Product__r.LastReferencedDate, p.Product__r.LastViewedDate, p.Product__r.SystemModstamp, p.Product__r.Display_Order__c, 
		p.Product__r.LastModifiedById, p.Product__r.LastModifiedDate, p.Product__r.CreatedById, p.Product__r.CreatedDate, p.Product__r.Name, Name,
		p.Product__r.IsDeleted, p.Product__r.Id, p.Product__c From Product_Subscriptions__c p WHERE p.Subscriber__r.Id =: userId]);
		 
	    totalCount = subscriptions_List.size();
	    
	    docsSubscriptions_List = [SELECT Id, Name, Document__c, is_subscribed__c FROM Document_Subscriptions__c WHERE User__c =: userId];
	    
	    docsSubscriptionBeanList = new list<DocumentSubscriptionBean>();
	    
	    map<Id, ContentDocument> contentIds2Contents_Map;
	    
	    if(!docsSubscriptions_List.isEmpty()) {
	    	
	    	set<id> contentIds_Set = new set<Id>();
	    	for (Document_Subscriptions__c theDoc : docsSubscriptions_List) {
	    		
	    		contentIds_Set.add(theDoc.Document__c);
	    	}
	    	
	    	if (!contentIds_Set.isEmpty()) {
	    		
	    		contentIds2Contents_Map = new map<Id, ContentDocument> ([SELECT Id, Title FROM ContentDocument WHERE Id IN : contentIds_Set]);
	    	}
	    	
	    	if (!contentIds2Contents_Map.values().isEmpty()) {
	    		
	    		for (Document_Subscriptions__c theDoc : docsSubscriptions_List) {
	    			
	    			ContentDocument currentContent = contentIds2Contents_Map.get(theDoc.Document__c);
	    			
	    			if (currentContent != null) {
	    			
		    			DocumentSubscriptionBean docBean = new DocumentSubscriptionBean();
		    			docBean.subscription = theDoc;
		    			docBean.relatedContentName = currentContent.Title;
		    			docsSubscriptionBeanList.add(docBean);
	    			}	    			
	    		}
	    	}
	    }
	    
	    
	    totalDocsCount = docsSubscriptions_List.size();
	    
	    
		
	}

	 
	 /**
	  @Author Elad Kaplan
	  @date 15.09.2014
	  @return void
	  @Description This method will delete the current row, where the user clicks on the relevent 'Un subscribe' button
	  */
	 public void deleteRow() {
	 
		 rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
		 
		 Product_Subscriptions__c subscriptionToDelete =  subscriptions_List[rowIndex];
		
		 subscriptionToDelete.is_subscribed__c = false;
		 
		 subscriptionsToDelete_Map.put(subscriptionToDelete.Id, subscriptionToDelete);
		 
		 delete subscriptionToDelete;	 
	 }
	 
	 /**
	 @Author Elad Kaplan
	 @date 15.09.2014
	 @return void
	 @Description This method will restore the current row, where the user clicks on the relevent 'subscribe' button
	 */
	 public void restoreRow() {
	 	
	 	rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
	 	
	 	Product_Subscriptions__c subscriptionToRestore =  subscriptions_List[rowIndex];
	 
		subscriptions_List[rowIndex].is_subscribed__c = true;
		
		undelete subscriptionToRestore;
	 }
	 
	 /**
	  @Author Elad Kaplan
	  @date 30.09.2014
	  @return void
	  @Description This method will delete the current row, where the user clicks on the relevent 'Un subscribe' button
	  */
	 public void deleteDocsRow() {
	 
		 rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex1'));
		 
		 Document_Subscriptions__c subscriptionToDelete =  docsSubscriptions_List[rowIndex];
		 
		 system.debug('subscriptionToDelete : '+ subscriptionToDelete);
		
		 subscriptionToDelete.is_subscribed__c = false;
		 
		 docsSubscriptionsToDelete_Map.put(subscriptionToDelete.Id, subscriptionToDelete);
		 
		 delete subscriptionToDelete;	 
	 }
	 
	 /**
	 @Author Elad Kaplan
	 @date 30.09.2014
	 @return void
	 @Description This method will restore the current row, where the user clicks on the relevent 'subscribe' button
	 */
	 public void restoreDocsRow() {
	 	
	 	rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex1'));
	 	
	 	Document_Subscriptions__c subscriptionToRestore =  docsSubscriptions_List[rowIndex];
	 	
	 	system.debug('subscriptionToRestore : '+ subscriptionToRestore);
	 
		subscriptionToRestore.is_subscribed__c = true;
		
		undelete subscriptionToRestore;
		subscriptionToRestore.is_subscribed__c = true;
	 }
	 
	 public void testHelper() {
	 	
	 	Integer  i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 	i = 1;
	 }
}