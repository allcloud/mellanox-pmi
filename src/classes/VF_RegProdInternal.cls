public with sharing class VF_RegProdInternal 
{
    public  boolean isTypeSwitch {get;set;}


         
        //*******
         //Johny Code from 14-11-2011
        public class MellanoxProducts
    {
                
        public Opp_Reg_OPN__c  OppRegOPN {get;set;}
        public Opp_Reg_OPN__c  OppRegOPN_SupportItem {get;set;}
        public Integer ind {get;set;}
        public boolean Required {get;set;}
        public boolean RequiredOPN {get;set;}
        public  boolean isTypeSwitch {get;set;}



           
        public List<Product2> lst_optionsSupportedItems {get;set;}
        public String supItmSelectedFrmList {get;set;}
        
        public List<selectOption> lst_supItemOptions {get;set;} 
        
        public MellanoxProducts(Opp_Reg_OPN__c  opRegOpn,Opp_Reg_OPN__c  opRegOpnSupItmIn,Integer ind)
        {
                 
            this.OppRegOPN = opRegOpn;
            isTypeSwitch =false;
            this.ind = ind;
            Required = true;
            RequiredOPN = false;
            lst_optionsSupportedItems = new List<Product2>();
            lst_supItemOptions = new List<selectOption>();
            OppRegOPN_SupportItem = opRegOpnSupItmIn;
            supItmSelectedFrmList = '';
        }
        public void setSupItmSelectedFrmList(String theItemSelected)
        {
                supItmSelectedFrmList = theItemSelected;
        }
        
        
        
        public void getRelevantSupportedItems()
        {
                String str1  = system.currentPageReference().getParameters().get('opIdFrm');
                system.debug('Sent parameter : ' + system.currentPageReference().getParameters().get('opIdFrm'));
            system.debug('getRelevantSupportedItems OppRegOPN.OPN__c : ' + OppRegOPN.OPN__c);
             system.debug('getRelevantSupportedItems OppRegOPN.Name : ' + OppRegOPN.Name);
             
             system.debug('before  getting the product name ' + OppRegOPN.Name + '  OppRegOPN.OPN__c ' + OppRegOPN.OPN__c);
             
             if ( string.valueOf(OppRegOPN.Name).length() > 2 && string.valueOf(OppRegOPN.OPN__c).length() < 1)
             {
                try{
                        
                
                        Product2 pTmp = [select id, name From Product2 where name =:OppRegOPN.Name.trim()];
                        OppRegOPN.OPN__c = pTmp.Id;
                        system.debug(' PTmp : ' + pTmp);
                        
                        OppRegOPN.Name =pTmp.Name;
                        OppRegOPN_SupportItem.Name =pTmp.Name;
                        
                        
                }catch(Exception ex){}
             }
             system.debug('after getting the product');
             
            if(OppRegOPN.OPN__c != null )
            {
                lst_supItemOptions = new List<selectOption>();
                system.debug('OppRegOPN.OPN__c is : ' + OppRegOPN.OPN__c);
                list<Support_Item__c > lst_opp_regProd = new list<Support_Item__c >();
                 Product2 opRegProd_checkifSwitchTyp;
                if(OppRegOPN.OPN__c != null)
                {   Required = true;
             
             //--------------------NEW --------------                                                             
                  lst_opp_regProd = [Select o.Support_product__c,o.Support_product__r.name,o.Id 
                                                                From Support_Item__c o 
                                                                where o.mlxProduct__c =: OppRegOPN.OPN__c and o.mlxProduct__c != null
                                                                and Support_Product__r.Is_Registerable__c != 'No'
                                                                ];                                                        
                    String str = 'Select o.Support_product__c,o.Support_product__r.name,o.Id From Support_Item__c o where o.mlxProduct__c =: ' + OppRegOPN.OPN__c;
                    system.debug('The sup items are : ' + str);                                                                                                                              
                    system.debug('the id of current selection this.OppRegOPN.Id: ' + this.OppRegOPN.OPN__c);
                   
                    try{
                        
                    
                    opRegProd_checkifSwitchTyp = [  Select  o.Id ,o.num_of_sup__c,Product_Type1__c, Support_Not_Required__c
                                                                    From Product2 o
                                                                 where o.Id =: OppRegOPN.OPN__c];
                    }catch(Exception ex){}
                
                }
               if(opRegProd_checkifSwitchTyp != null )
                {
                    
                    if(opRegProd_checkifSwitchTyp.num_of_sup__c != NULL && opRegProd_checkifSwitchTyp.num_of_sup__c >0 &&opRegProd_checkifSwitchTyp.Product_Type1__c != 'CABLES'   && !opRegProd_checkifSwitchTyp.Support_Not_Required__c)
                     {
                        isTypeSwitch  =true;
                         
                    }else
                    {
                        isTypeSwitch  =false;
                    }
                    
                    
                }
                system.debug(' isTypeSwitch is' + isTypeSwitch);
                if(lst_opp_regProd != null && lst_opp_regProd.size() > 0)
                {
                    //setSupportItemList(lst_opp_regProd);
                    lst_supItemOptions.Add(new selectoption('','- None -'));
                    for(Support_Item__c orp :lst_opp_regProd)
                    {                        
                        lst_supItemOptions.add(new selectoption(orp.Support_Product__c, orp.Support_Product__r.name));                             
                    }
                }
            }
            system.debug('lst_supItemOptions: ' + lst_supItemOptions);
            system.debug('supItmSelectedFrmList : ' + supItmSelectedFrmList);
            if(lst_supItemOptions != null && lst_supItemOptions.size() > 0)
            {
                system.debug('lst_supItemOptions[0].getValue(); : '+ lst_supItemOptions[0].getValue());
                    supItmSelectedFrmList = lst_supItemOptions[0].getValue();
                   supportItemSelected();
            }
            //return null;
        }
        public void supportItemSelected()
        {
            //supItmSelectedFrmList  = system.currentPageReference().getParameters().get('supItem');
            String str  = system.currentPageReference().getParameters().get('supItem');
            system.debug('the value sent is : ' + str);
            system.debug('OppRegOPN.OPN__c ' + OppRegOPN.OPN__c);
            system.debug(' supportItem selected : ' + supItmSelectedFrmList);
            if(supItmSelectedFrmList == null || supItmSelectedFrmList == ''  || supItmSelectedFrmList == '- None -')
            {
                system.debug('The value is inccorrect' + supItmSelectedFrmList );
                OppRegOPN_SupportItem.OPN__c = null;
            }
            else
            {
                
                OppRegOPN_SupportItem.OPN__c = supItmSelectedFrmList;
                system.debug('The value of supp item selected is: ' + supItmSelectedFrmList);
            }
            //return null;
            
        }
    }
        //*******
        public ID oppReg {get;set;}
    public MellanoxProducts newMellanoxProd {get;set;}
    

    public VF_RegProdInternal() 
    {
        oppReg = Apexpages.currentPage().getParameters().get('Id');
        system.debug('oppReg :  ' + oppReg);
            
        newMellanoxProd = new MellanoxProducts(new Opp_Reg_OPN__c(),new Opp_Reg_OPN__c(),1);
        
        
        newMellanoxProd.OppRegOPN.Opp_Reg__c = oppReg;
        newMellanoxProd.OppRegOPN_SupportItem.Opp_Reg__c = oppReg;

    }
    
    public pagereference close()
    {
         return new pagereference('/'+oppReg);
    }
    public PageReference Save()
    {
       
        try
        {
                List<Opp_Reg_OPN__c> lst_oppRegOPNTobeInserted = new List<Opp_Reg_OPN__c>(); 
                //KN added
                //List<Opp_Reg_OPN__c> lst_oppRegOPNTobeUpdated = new List<Opp_Reg_OPN__c>();
                //
                if(!ValidateRequierFields())
                {
                        if(newMellanoxProd.OppRegOPN.OPN__c != null )
                        {
                                lst_oppRegOPNTobeInserted.add(newMellanoxProd.OppRegOPN);
                        }
                        
                        if(newMellanoxProd.OppRegOPN_SupportItem.OPN__c != null )
                        {
                                lst_oppRegOPNTobeInserted.add(newMellanoxProd.OppRegOPN_SupportItem);
                        }
                        newMellanoxProd.OppRegOPN_SupportItem.OPN_Requested_Discount__c =  newMellanoxProd.OppRegOPN.OPN_Requested_Discount__c;

                        if(lst_oppRegOPNTobeInserted != Null && lst_oppRegOPNTobeInserted.size() > 0)
                        {
                                
                                insert lst_oppRegOPNTobeInserted;
                                //update support item IDs
                                newMellanoxProd.OppRegOPN.Supported_Item__c = newMellanoxProd.OppRegOPN_SupportItem.ID;
                                update newMellanoxProd.OppRegOPN;
                                //                              
                                //return new pagereference('https://mellanox.my.salesforce.com/'+oppReg);
                                PageReference pg = new PageReference('/'  + oppReg);
                                pg.setRedirect(true);
                                return pg;
                                
                        }
                        
                }
                
                return null;
                
        }catch(Exception e)
        {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage()));
                return null;
        }
           
        
    }
    
    public boolean ValidateRequierFields()
    {     
        Product2 opRegProd_checkifSwitchTyp;
        boolean anErr = false;        
        opRegProd_checkifSwitchTyp = [  Select  o.Id ,o.num_of_sup__c,Product_Type1__c,Support_Not_Required__c,Is_Registerable__c
                                                                    From Product2 o
                                                                 where o.Id =: newMellanoxProd.OppRegOPN.OPN__c];
        if(opRegProd_checkifSwitchTyp != null )
        {
            
            if(opRegProd_checkifSwitchTyp.num_of_sup__c != NULL && opRegProd_checkifSwitchTyp.num_of_sup__c >0 &&opRegProd_checkifSwitchTyp.Product_Type1__c != 'CABLES' && !opRegProd_checkifSwitchTyp.Support_Not_Required__c)
             {
                newMellanoxProd.isTypeSwitch  =true;
                 
            }else
            {
                newMellanoxProd.isTypeSwitch  =false;
            }
            
            
        }
             
        
        if( newMellanoxProd.OppRegOPN.OPN__c ==null )
        {
                 system.debug('you cannot save without givin mellanox opn ');
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill in Mellanox opn '));
        
                anErr=true;
        }else 
            if(opRegProd_checkifSwitchTyp.Is_Registerable__c == 'No')
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'The selected product is not registerable, type the product name and select from a list'));
        
                anErr=true;
            }
                
                else             
           if(newMellanoxProd.isTypeSwitch &&  newMellanoxProd.OppRegOPN.OPN__c  !=null &&string.valueOf(newMellanoxProd.OppRegOPN.OPN__c).length() >5 && newMellanoxProd.OppRegOPN_SupportItem.OPN__c==NULL)
             {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Mellanox Product should have Support Item'));
              anErr=true;
             }else
            
                               
              
                if(   newMellanoxProd.OppRegOPN.OPN__c !=null && newMellanoxProd.OppRegOPN_SupportItem.OPN__c != null   &&
              newMellanoxProd.OppRegOPN_SupportItem.OPN_Quantity__c != newMellanoxProd.OppRegOPN.OPN_Quantity__c 
          )
                                        
                {
                 system.debug('Quantity should be the same ');
                 anErr=true;
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Mellanox Product Quantity and Support Item Quantity for Mellanox Product  should be the same'));
                
                }else
                
                if(   newMellanoxProd.OppRegOPN.OPN__c !=null && newMellanoxProd.OppRegOPN.OPN_Quantity__c == null )
                {
                 system.debug('Quantity is a must for Mellanox OPN ');
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill in Quantity for Mellanox OPN'));
                
                 anErr=true;
                }else
                
                if(   newMellanoxProd.OppRegOPN.OPN__c !=null && newMellanoxProd.OppRegOPN.OPN_Requested_Discount__c == null )
                {
                 system.debug('the Discount is a must ');
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill in the Discount'));
                
                anErr=true;
                }else
                
                
                {
                    anErr = false;
                }
                
                return anErr;
                
    }
}