@isTest(seeAllData=true)
public with sharing class test_VF_RMA_NOT_APPROVED {
	
	static testMethod void test_TheVF() {
		
		Case theCase = [SELECT Id, RMA_Request__c FROM Case  WHERE RMA_Request__c != null limit 1];
		
		VF_RMA_NOT_APPROVED vf = new VF_RMA_NOT_APPROVED();
		vf.caseId = theCase.Id;
		vf.getPopulateList();
	}
	
	
}