// This class is executed by a scheduler class, It will update all the relavant Cases in batches of 200 records per batch
global class Batch_Cases_Breached_EMC  implements Database.Batchable<Case>{
	
	 global Iterable<Case> start(Database.BatchableContext BC) {
    
    	 List<Case> CasesToBeUpdated_List = [SELECT Id, Breached_EMC_SLA__c FROM Case
    	 									 WHERE Status != 'Closed' and Status != 'RMA Canceled' and Status != 'Soft Closed - Pending Approval'
    	 									 and State__c = 'Open' and Reopened_Case__c != true
    	 									 and ( (Priority = 'Low' and Case_age__c > 2) or 
											       (Priority = 'Medium' and Case_age__c > 1) or
											       (Priority = 'High' and Case_age__c > 0.25) or 
											       (Priority = 'Fatal' and Case_age__c > 0.08333) )];
											       
     
          system.debug('CasesToBeUpdated_List 16 : '+ CasesToBeUpdated_List);
          system.debug('CasesToBeUpdated_List size : '+ CasesToBeUpdated_List.size());  	
          
          										       
											      
		return 	CasesToBeUpdated_List;							       
     }
     
     global void execute(Database.BatchableContext BC, List<Case> scope) {
     	
     	  List<Case> CasesToBeUpdated_List = scope;
     	  
     	  system.debug('CasesToBeUpdated_List : '+ CasesToBeUpdated_List);
											       
		 if (!CasesToBeUpdated_List.isEmpty()) {
		 	
		 	for (Case theCase : CasesToBeUpdated_List) {
		 		
		 		theCase.Breached_EMC_SLA__c = true;
		 	}
		 	
		 	Database.update(CasesToBeUpdated_List, false);
		 }									       
     }
     
     global void finish(Database.BatchableContext BC) {
     	   
     }

}