global class Schedule_Batch_Merge_Contacts implements Schedulable{
	
	// This metohd executes the class 'Batch_Merge_Contacts' with a scheduler
    global void execute(SchedulableContext SC) {
    
        Batch_Merge_Contacts batch = new Batch_Merge_Contacts();
        Database.executeBatch(batch);
   }	
}