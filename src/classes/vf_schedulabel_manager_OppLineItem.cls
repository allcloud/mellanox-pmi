public with sharing class vf_schedulabel_manager_OppLineItem {

    public final Integer QUARTER_NUMBER = 10;
        
    public Integer CellNum                              {get; set;}
    
    Set<ID> oppLineItemSchedulare_Ids = new Set<ID>();
     
    public Map<Integer, Date> cell_To_Date;
    
    public Integer Currentq;
       
    public List<Integer> listquarterNum
    {     
        get{
                
             List<Integer> ans = new List<Integer>();
             for (Integer i = 0; i < QUARTER_NUMBER ; i++)
                   ans.add(i);     
             return ans;                                             
        }
    }
    public Opportunity opp {get; set;}   

    public String showBoth { get; set; }
    
    private ID oppLineItemId;
    
    public schedulare_row row {get; set;}
    
    Map<Integer, Integer> month_To_Quarter = new Map<Integer, Integer> 
        {1=>1, 2=>1, 3=>1, 4=>2, 5=>2, 6=>2, 7=>3, 8=>3, 9=>3, 10=>4, 11=>4, 12=>4};
        
    
    Map<Integer, Date> map_CellNum_To_ScheDate = new Map<Integer, Date>();
        
    public List<String> quarters {
        
        get{
                
            List<String> ans = new List<String>();
            ans = getNext8Quarters(month_To_Quarter.get(system.today().month()), system.today().year());
            return ans;
        
        }
    }
    
    List<OpportunityLineItemSchedule> lst_OppLineItemSchedule_To_Insert = new List<OpportunityLineItemSchedule>();
    List<OpportunityLineItemSchedule> lst_OppLineItemSchedule_To_Update = new List<OpportunityLineItemSchedule>();
    
    Map<Id, OpportunityLineItem> map_opp_line_items = new Map<Id, OpportunityLineItem>();
    
    OpportunityLineItem oppLineItem;
    
    public List<String> getNext8Quarters(integer q, integer year)
    {
        
        List<String> q_list;
        Integer next1 = year + 1;
        Integer next2 = year + 2;
        
        if (q == 1)
        {
            q_list = new List<String> {'Q1 - ' +year,'Q2 - ' +year, 'Q3 - ' +year, 'Q4 - ' +year,
                                       'Q1 - ' +next1, 'Q2 - ' +next1, 'Q3 - ' +next1, 'Q4 - ' +next1};
                                                                                                            
        }
        if (q == 2)
        {
            q_list = new List<String> {'Q2 - ' +year,'Q3 - ' +year, 'Q4 - ' +year, 'Q1 - ' +next1,
                                       'Q2 - ' +next1, 'Q3 - ' +next1, 'Q4 - ' +next1, 'Q1 - ' +next2};
                                            
        }
        if (q == 3)
        {
            q_list = new List<String> {'Q3 - ' +year,'Q4 - ' +year, 'Q1 - ' +next1, 'Q2 - ' +next1,
                                       'Q3 - ' +next1, 'Q4 - ' +next1, 'Q1 - ' +next2, 'Q2 - ' +next2};
                                                                                                            
        }
        if (q == 4)
        {
            q_list = new List<String> {'Q4 - ' +year,'Q1 - ' +next1, 'Q2 - ' +next1, 'Q3 - ' +next1,
                                       'Q4 - ' +next1, 'Q1 - ' +next2, 'Q2 - ' +next2, 'Q3 - ' +next2};
                                                                    
                                                
        }
        return q_list;          
        
    }
      public class price_row
    {
        public OpportunityLineItemSchedule oppLineItemSched {get; set;}
        public boolean isChange                             {get; set;}
        public double price                                 
        {
            get; 
            set
            {
                isChange = true;
                price = value;
            }
        }
        double oldPrice;
        
        public price_row(OpportunityLineItemSchedule oppLineItemSched)
        {
            this.isChange = false;
            this.oppLineItemSched = oppLineItemSched;
            if (oppLineItemSched.revenue != null && oppLineItemSched.revenue > 0 && oppLineItemSched.quantity != null && oppLineItemSched.quantity > 0)
                price = oppLineItemSched.revenue / oppLineItemSched.quantity;
            if (oppLineItemSched.revenue != null && oppLineItemSched.revenue > 0 && oppLineItemSched.quantity != null && oppLineItemSched.quantity > 0)
                this.oldPrice = oppLineItemSched.revenue / oppLineItemSched.quantity;
        } 
    } 
    
    public class Quantity_row
    {
        public OpportunityLineItemSchedule oppLineItemSched {get; set;}
        public boolean isChange                             {get; set;}
        public Integer quantity                                 
        {
            get; 
            set
            {
                isChange = true;
                quantity = value;
            }
        }
       
        public Quantity_row(OpportunityLineItemSchedule oppLineItemSched)
        {
            this.isChange = false;
            this.oppLineItemSched = oppLineItemSched;
            if (oppLineItemSched.quantity != null)
                quantity = oppLineItemSched.quantity.intValue();
        } 
    } 
    
    
    public class schedulare_row
    {
        public OpportunityLineItem line                                 {get; set;}
        public Quantity_row[] OppLineSchedulesQty        {get; set;}
        public price_row[] OppLineSchedulesPrc                          {get; set;}
        public OpportunityLineItemSchedule[] OppLineSchedulesRev        {get; set;}
        
        public Map<Id, Date> map_id_To_Date = new Map<Id, Date>();
        Map<Integer, Date> cell_To_Date = new Map<Integer, Date>();
        Boolean IsInit = false;
        
        public Integer getQuarterByDate(Date currentdate, Date scheduledDate)
        {    
             Integer ans ;
                        
             Integer Pre ; 
                        
            if ( currentdate.Month() >= 1 && currentdate.month() <= 3)
                Pre = 0;
            if ( currentdate.Month() >= 4 && currentdate.month() <= 6)
                Pre = 1;
            if ( currentdate.Month() >= 7 && currentdate.month() <= 9)
                Pre = 2;
            if ( currentdate.Month() >= 10 && currentdate.month() <= 12)
                Pre = 3;
                        
                        
            if ( scheduledDate.Month() >= 1 && scheduledDate.month() <= 3)
                ans = 1;
            if ( scheduledDate.Month() >= 4 && scheduledDate.month() <= 6)
                ans = 2;
            if ( scheduledDate.Month() >= 7 && scheduledDate.month() <= 9)
                ans = 3;
            if ( scheduledDate.Month() >= 10 && scheduledDate.month() <= 12)
                ans = 4;
                        
            return ((scheduledDate.year() - currentdate.year()) * 4 )+ ans - 1 - Pre ; 
        }
        
        public schedulare_row(OpportunityLineItem oli,Integer num)
        {
            if (!IsInit)
            {
                init();
                IsInit = true;
            }
            line = oli;
            
            OppLineSchedulesQty = new Quantity_row[num];
            OppLineSchedulesPrc = new price_row[num];
            OppLineSchedulesRev = new OpportunityLineItemSchedule[num];
            
            for ( OpportunityLineItemSchedule ols : oli.OpportunityLineItemSchedules)
            {
                if (getQuarterByDate(System.today(), ols.ScheduleDate) < 8)
                {
                    OppLineSchedulesQty[getQuarterByDate(System.today(), ols.ScheduleDate)] = new Quantity_row(ols);
                    OppLineSchedulesRev[getQuarterByDate(System.today(), ols.ScheduleDate)] = ols;
                    OppLineSchedulesPrc[getQuarterByDate(System.today(), ols.ScheduleDate)] = new price_row(ols);
                    map_id_To_Date.put(ols.Id, ols.ScheduleDate);    
                }
            }
        
            for (Integer i = 0 ; i < num ; i++ )
            {
                OpportunityLineItemSchedule ols = new OpportunityLineItemSchedule( OpportunityLineItemId = oli.Id, Type = 'Both', ScheduleDate = cell_To_Date.get(i));
                
                if (OppLineSchedulesQty[i] == null)
                {
                    OppLineSchedulesQty[i] = new Quantity_row(ols);
                }
                
                if (OppLineSchedulesRev[i] == null)
                {       
                    OppLineSchedulesRev[i] = ols;
                }
                
                if (OppLineSchedulesPrc[i] == null)
                {
                    OppLineSchedulesPrc[i] = new price_row(ols);  
                    OppLineSchedulesPrc[i].price = oli.UnitPrice; 
                }
            }
        }
        
        public void init()
        {
            Integer Currentq = 0;
                
            if ( System.today().Month() >= 1 && System.today().month() <= 3)
                Currentq = 1;
            if ( System.today().Month() >= 4 && System.today().month() <= 6)
                Currentq = 2;
            if ( System.today().Month() >= 7 && System.today().month() <= 9)
                Currentq = 3;
            if ( System.today().Month() >= 10 && System.today().month() <= 12)
                Currentq = 4;   
                
            if (Currentq == 1)
            {
                cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 1, 1),
                                                       1 => Date.newInstance(System.today().year(), 4, 1),
                                                       2 => Date.newInstance(System.today().year(), 7, 1),
                                                       3 => Date.newInstance(System.today().year(), 10, 1),
                                                       4 => Date.newInstance(System.today().year()+1 , 1, 1),
                                                       5 => Date.newInstance(System.today().year()+1, 4, 1),
                                                       6 => Date.newInstance(System.today().year()+1, 7, 1),
                                                       7 => Date.newInstance(System.today().year()+1, 10, 1)};
 
            }
            else
            {
                if (Currentq == 2)
                {
                        cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 4, 1),
                                                                                        1 => Date.newInstance(System.today().year(), 7, 1),
                                                                                                2 => Date.newInstance(System.today().year(), 10, 1),
                                                                                                3 => Date.newInstance(System.today().year()+1, 1, 1),
                                                                                                4 => Date.newInstance(System.today().year()+1 , 4, 1),
                                                                                                5 => Date.newInstance(System.today().year()+1, 7, 1),
                                                                                                6 => Date.newInstance(System.today().year()+1, 10, 1),
                                                                                                7 => Date.newInstance(System.today().year()+2, 1, 1)};
                }
                else
                {
                        if (Currentq == 3)
                        {
                                cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 7, 1),
                                                                                        1 => Date.newInstance(System.today().year(), 10, 1),
                                                                                                2 => Date.newInstance(System.today().year()+1, 1, 1),
                                                                                                3 => Date.newInstance(System.today().year()+1, 4, 1),
                                                                                                4 => Date.newInstance(System.today().year()+1, 7, 1),
                                                                                                5 => Date.newInstance(System.today().year()+1, 10, 1),
                                                                                                6 => Date.newInstance(System.today().year()+2, 1, 1),
                                                                                                7 => Date.newInstance(System.today().year()+2, 4, 1)};
                        }
                        else
                        {
                                if (Currentq == 4)
                                {
                                        cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 10, 1),
                                                                                                1 => Date.newInstance(System.today().year()+1, 1, 1),
                                                                                                        2 => Date.newInstance(System.today().year()+1, 4, 1),
                                                                                                        3 => Date.newInstance(System.today().year()+1, 7, 1),
                                                                                                        4 => Date.newInstance(System.today().year()+1, 10, 1),
                                                                                                        5 => Date.newInstance(System.today().year()+2, 1, 1),
                                                                                                        6 => Date.newInstance(System.today().year()+2, 4, 1),
                                                                                                        7 => Date.newInstance(System.today().year()+2, 7, 1)};
                                }
                        }
                }
            }
            system.debug('cell_To_Date'+cell_To_Date); 
        }
    }
    public vf_schedulabel_manager_OppLineItem(ApexPages.StandardController controller)
    {
       oppLineItemId = ApexPages.currentPage().getParameters().get('id');
       
       initForQuery();
       
       /*oppLineItem = [Select o.Product_Family__c, o.Opportunity.Amount, o.PricebookEntry.Name, o.Quantity, o.UnitPrice, o.TotalPrice, o.Id, 
                        (Select OpportunityLineItemId, Id, Revenue, Quantity, ScheduleDate From OpportunityLineItemSchedules where ScheduleDate >=: System.today() order by ScheduleDate asc) 
                            From OpportunityLineItem o where o.Id = :oppLineItemId limit 1];*/
        
        oppLineItem = [Select o.Product_Family__c, o.Opportunity.Amount, o.PricebookEntry.Name, o.Quantity, o.UnitPrice, o.TotalPrice, o.Id, 
                        (Select OpportunityLineItemId, Id, Revenue, Quantity, ScheduleDate From OpportunityLineItemSchedules where ScheduleDate >=: cell_To_Date.get(Currentq) order by ScheduleDate asc) 
                            From OpportunityLineItem o where o.Id = :oppLineItemId limit 1];
        
        
        opp = oppLineItem.Opportunity;
            
        row = new schedulare_row(oppLineItem, QUARTER_NUMBER);

        for (OpportunityLineItemSchedule ols :oppLineItem.OpportunityLineItemSchedules)
            oppLineItemSchedulare_Ids.add(ols.Id);
                        
    }
    
    public void initForQuery()
    {
        Currentq = 0;
            
        if ( System.today().Month() >= 1 && System.today().month() <= 3)
            Currentq = 1;
        if ( System.today().Month() >= 4 && System.today().month() <= 6)
            Currentq = 2;
        if ( System.today().Month() >= 7 && System.today().month() <= 9)
            Currentq = 3;
        if ( System.today().Month() >= 10 && System.today().month() <= 12)
            Currentq = 4;   
            
        if (Currentq == 1)
        {
            cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 1, 1),
                                                   1 => Date.newInstance(System.today().year(), 4, 1),
                                                   2 => Date.newInstance(System.today().year(), 7, 1),
                                                   3 => Date.newInstance(System.today().year(), 10, 1),
                                                   4 => Date.newInstance(System.today().year()+1 , 1, 1),
                                                   5 => Date.newInstance(System.today().year()+1, 4, 1),
                                                   6 => Date.newInstance(System.today().year()+1, 7, 1),
                                                   7 => Date.newInstance(System.today().year()+1, 10, 1)};
 
        }
        else
        {
            if (Currentq == 2)
            {
                    cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 4, 1),
                                                                                    1 => Date.newInstance(System.today().year(), 7, 1),
                                                                                            2 => Date.newInstance(System.today().year(), 10, 1),
                                                                                            3 => Date.newInstance(System.today().year()+1, 1, 1),
                                                                                            4 => Date.newInstance(System.today().year()+1 , 4, 1),
                                                                                            5 => Date.newInstance(System.today().year()+1, 7, 1),
                                                                                            6 => Date.newInstance(System.today().year()+1, 10, 1),
                                                                                            7 => Date.newInstance(System.today().year()+2, 1, 1)};
            }
            else
            {
                    if (Currentq == 3)
                    {
                            cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 7, 1),
                                                                                    1 => Date.newInstance(System.today().year(), 10, 1),
                                                                                            2 => Date.newInstance(System.today().year()+1, 1, 1),
                                                                                            3 => Date.newInstance(System.today().year()+1, 4, 1),
                                                                                            4 => Date.newInstance(System.today().year()+1, 7, 1),
                                                                                            5 => Date.newInstance(System.today().year()+1, 10, 1),
                                                                                            6 => Date.newInstance(System.today().year()+2, 1, 1),
                                                                                            7 => Date.newInstance(System.today().year()+2, 4, 1)};
                    }
                    else
                    {
                            if (Currentq == 4)
                            {
                                    cell_To_Date = new Map<Integer, Date> {0 => Date.newInstance(System.today().year(), 10, 1),
                                                                                            1 => Date.newInstance(System.today().year()+1, 1, 1),
                                                                                                    2 => Date.newInstance(System.today().year()+1, 4, 1),
                                                                                                    3 => Date.newInstance(System.today().year()+1, 7, 1),
                                                                                                    4 => Date.newInstance(System.today().year()+1, 10, 1),
                                                                                                    5 => Date.newInstance(System.today().year()+2, 1, 1),
                                                                                                    6 => Date.newInstance(System.today().year()+2, 4, 1),
                                                                                                    7 => Date.newInstance(System.today().year()+2, 7, 1)};
                            }
                    }
            }
        }
        system.debug('cell_To_Date'+cell_To_Date); 
    }
    
    public Pagereference saveChanges()
    {
        
        Pagereference page = new Pagereference('/apex/opportunityLineITem_schedulabe_manager?id='+oppLineItemId);
        
        try
        {
            Set<Id> ids = new Set<Id>();
            Set<Id> OpplineItemIds = new Set<Id>();
            
            ids.AddAll(row.map_id_To_Date.keySet());
             
             
            Map<Id, OpportunityLineItemSchedule> map_OpportunityLineItemSchedule = new Map<Id, OpportunityLineItemSchedule>(
                                                                                    [Select o.ScheduleDate, o.Revenue, o.Quantity, o.OpportunityLineItem.OpportunityId, o.OpportunityLineItemId 
                                                                                     From OpportunityLineItemSchedule o 
                                                                                     where o.Id IN : ids]
            );
             
            Integer q;                                                                                                                                               
            for (Integer i = 0 ; i < QUARTER_NUMBER ; i++)
            {
                
                OpportunityLineItemSchedule olis = row.OppLineSchedulesQty[i].oppLineItemSched;

                if (map_OpportunityLineItemSchedule.containsKey(olis.Id))
                {   
                    
                    q = olis.Quantity.intValue();
                    
                    if ((q != row.OppLineSchedulesQty[i].quantity) || (row.OppLineSchedulesPrc[i].price != row.OppLineSchedulesPrc[i].oldPrice))
                    {
                        system.debug('quantity xxx: '+row.OppLineSchedulesQty[i].quantity);
                        system.debug('price xxx: '+row.OppLineSchedulesPrc[i].price);
                        olis.quantity = row.OppLineSchedulesQty[i].quantity;
                        olis.Revenue = row.OppLineSchedulesQty[i].quantity * row.OppLineSchedulesPrc[i].price;        
                        if (row.OppLineSchedulesQty[i].quantity == null)
                            olis.Quantity = 0;
                        
                        lst_OppLineItemSchedule_To_Update.add(olis);
                    } 
                }
                else
                {
                    if (row.OppLineSchedulesQty[i].quantity > 0  && row.OppLineSchedulesPrc[i].price > 0)
                    {
                        olis.Quantity = row.OppLineSchedulesQty[i].quantity;
                        olis.Revenue = row.OppLineSchedulesQty[i].quantity * row.OppLineSchedulesPrc[i].price;        
                        lst_OppLineItemSchedule_To_Insert.add(olis);    
                    }
                }
            }
               
            system.debug('lst_OppLineItemSchedule_To_Insert:'+lst_OppLineItemSchedule_To_Insert);
            system.debug('lst_OppLineItemSchedule_To_Update: '+lst_OppLineItemSchedule_To_Update);
    
            if (lst_OppLineItemSchedule_To_Insert.size() > 0)
                insert lst_OppLineItemSchedule_To_Insert;
     
            if (lst_OppLineItemSchedule_To_Update.size() > 0)
                update lst_OppLineItemSchedule_To_Update;
            
                
        }
        catch(exception ex){ system.debug(ex.getMessage()); }
        
        page.setRedirect(true); 
        return page;
        
    }
    
    public Pagereference CalculateRevenue()
    {         
           
        for (Integer i = 0 ; i < row.OppLineSchedulesQty.size() ; i++)
        {     
            system.debug('BXD->'+  row.OppLineSchedulesPrc[i].price );
            if (row.OppLineSchedulesQty[i].Quantity != null && row.OppLineSchedulesQty[i].Quantity > 0 && row.OppLineSchedulesPrc[i].price != null && row.OppLineSchedulesPrc[i].price > 0) 
            {
                row.OppLineSchedulesRev[i].revenue = row.OppLineSchedulesQty[i].Quantity * row.OppLineSchedulesPrc[i].price;
            }
            
            if (row.OppLineSchedulesPrc[i].price == 0)
                row.OppLineSchedulesPrc[i].price = null;
            
            if (row.OppLineSchedulesQty[i].quantity == 0)
                row.OppLineSchedulesQty[i].quantity = null;             
        }
       
        return null;
    }
    
    public void doShift()
    {
          
        row.OppLineSchedulesQty[CellNum+1].quantity = row.OppLineSchedulesQty[CellNum].quantity;
        row.OppLineSchedulesPrc[CellNum+1].price = row.OppLineSchedulesPrc[CellNum].price;
        row.OppLineSchedulesRev[CellNum+1].revenue = row.OppLineSchedulesRev[CellNum].revenue;    
    
  
        for (Integer i = 0 ; i < row.OppLineSchedulesPrc.size() ; i++)  
        {
            if ( row.OppLineSchedulesPrc[i].price == 0)
                row.OppLineSchedulesPrc[i].price = null;
                
            if ( row.OppLineSchedulesQty[i].quantity == 0)
                    row.OppLineSchedulesQty[i].quantity = null;     
        }
        
    }

    public Pagereference cancel()
    {
        Pagereference p = new Pagereference('/'+oppLineItemId);
        p.setRedirect(true);
        return p;
        return null;
    }
    
    public void ShiftAll()
    {
               
        for (Integer i = CellNum + 1 ; i < QUARTER_NUMBER ; i++)
        {
            row.OppLineSchedulesQty[i].quantity = row.OppLineSchedulesQty[CellNum].quantity;
            row.OppLineSchedulesPrc[i].price = row.OppLineSchedulesPrc[CellNum].price;
            row.OppLineSchedulesRev[i].revenue = row.OppLineSchedulesRev[CellNum].revenue;
        }
        
   
        for (Integer i = 0 ; i < row.OppLineSchedulesPrc.size() ; i++)  
        {
            if ( row.OppLineSchedulesPrc[i].price == 0)
                row.OppLineSchedulesPrc[i].price = null;
            
            if ( row.OppLineSchedulesQty[i].quantity == 0)
                    row.OppLineSchedulesQty[i].quantity = null;     
        }
    
    }
    
 /*   @isTest
    static void test_vf_schedulabel_manager_OppLineItem()
    {
        
        OpportunityLineItem line = [Select o.PricebookEntry.Product2.Product_Type1__c, o.Product_Family__c, o.Opportunity.Amount, o.PricebookEntry.Name, o.Quantity, o.UnitPrice, o.TotalPrice, o.ListPrice, o.Id, 
                                                         (Select OpportunityLineItemId, Id, Revenue, Quantity, ScheduleDate, OpportunityLineItem.UnitPrice From OpportunityLineItemSchedules where ScheduleDate >=: System.today() order by ScheduleDate asc) 
                                                           From OpportunityLineItem o limit 1];
     
        
        
       
        Apexpages.currentPage().getParameters().put('Id',line.Id);
        ApexPages.StandardController con = new ApexPages.StandardController(line);
        vf_schedulabel_manager_OppLineItem manager = new vf_schedulabel_manager_OppLineItem(con);
        
        manager.row = new schedulare_row(line, 4);
        manager.row.OppLineSchedulesPrc[0].price = 3;
        manager.row.OppLineSchedulesQty[0].quantity = 3;
        manager.opp = line.Opportunity;
        manager.CellNum = 0;
        manager.getNext8Quarters(1, 2011);
        manager.saveChanges();
        manager.CalculateRevenue();
        manager.cancel();
        manager.doShift();
        manager.ShiftAll();
    } */  
}