public with sharing class JiveContentIdWrapper {
 public String jiveURL;
 public String contentID;
   public JiveContentIdWrapper(String jiveURL,String contentID){
    this.jiveURL=jiveURL;
    this.contentID =contentID;
   }
}