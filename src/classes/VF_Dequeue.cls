public with sharing class VF_Dequeue 
{   
      
   public Case MyCase {get;set;}
   public Id theCaseId {get;set;} 
   public boolean errMsg {get;set;} 
   public boolean displayCreatorOrigin {get;set;}  
      
    
        
        public VF_Dequeue (ApexPages.StandardController controller) 
    {
           displayCreatorOrigin = false;
           theCaseId = Apexpages.currentPage().getParameters().get('id');
        
           MyCase = new Case();
           MyCase = [  Select  cs.id, cs.RecordType.DeveloperName, cs.CSI_Creator_Origin__c, cs.Assignee__c,cs.OwnerId, cs.Customer_Environment__c , cs.state__c,cs.What_is_the_Impact_in_the_network__c,cs.Business_impact__c   
                       From Case cs
                       WHERE cs.Id =: theCaseId]; 
                       
           if (MyCase.RecordType.DeveloperName != null && MyCase.RecordType.DeveloperName.EqualsIgnoreCase('Customer_Infrastrcture')) {
           	
           		displayCreatorOrigin = true;
           }             
                             
        if(MyCase.State__c != 'Open')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Only case in Open state can be dequeued'));
         errMsg = true;
          }
    
    }                    
                
               
     public Pagereference Cancel() 
        { 
             return new Pagereference('/'+MyCase.Id);
           }  
      
    public Pagereference Save()
    {     
        //****************Validations*********************                
      /*  if(OppReg.Registration_Status_Calc__c != 'Approved')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Opportunity can be created only on Approved registration'));
         return  Apexpages.currentPage();
         } */
          
                
                                                       
       
         
         MyCase.Assignee__c = UserInfo.getUserId();
         MyCase.OwnerId = UserInfo.getUserId();
         MyCase.state__c = 'Assigned';
     
                 
         update MyCase;         
          Pagereference p =  new Pagereference('/'+MyCase.Id);  
           p.setRedirect(true);
          return p;           
    }   
  
}