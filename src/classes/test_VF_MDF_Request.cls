@isTest
public class test_VF_MDF_Request {
    
    static testMethod void test_TheVF() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        acc.Name = 'test Name';
        acc.billingStreet = 'test Street';
        insert acc; 
        
        SFDC_MDF__c mdf = new SFDC_MDF__c();
        mdf.Account__c = acc.Id;
        mdf.Name = 'Test MDF';
        mdf.Amount__c = 1200;        
        mdf.Activity_Start_Date__c = date.today().addDays(20);
        mdf.Activity_End_Date__c = mdf.Activity_Start_Date__c + 10;
        mdf.Fund_Request_Type__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1000;
        mdf.Activity_Description__c = 'Advertising';
        mdf.Purpose_of_Activity__c = 'Advertising';
        mdf.Activity_Product_Focus__c = 'Advertising';
        mdf.Activity_Vertical_Market_Focus__c = 'Advertising';
        mdf.Marketing_Dollars_Requested__c = 1000;
        mdf.Target_Audience__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1500;
        
        insert mdf;
        
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(mdf);
        VF_MDF_Request thePage = new VF_MDF_Request(teststandard);
        thePage.createAttachmentHolder();
        thePage.submitForApproval();
        thePage.saveAsDraft();
        thePage.turnExitingRecordNonEditable();
        thePage.cancelRequest();
        thePage.ChangeCount();
        
    }
    
    static testMethod void test_TheVF2() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        acc.Name = 'test Name';
        acc.billingStreet = 'test Street';
        insert acc; 
        
        SFDC_MDF__c mdf = new SFDC_MDF__c();
        mdf.Account__c = acc.Id;
        mdf.Name = 'Test MDF';
        mdf.Amount__c = 1200;
       
        mdf.Activity_Start_Date__c = date.today().addDays(20);
        mdf.Activity_End_Date__c = mdf.Activity_Start_Date__c + 10;
        mdf.Fund_Request_Type__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1000;
        mdf.Activity_Description__c = 'Advertising';
        mdf.Purpose_of_Activity__c = 'Advertising';
        mdf.Activity_Product_Focus__c = 'Advertising';
        mdf.Activity_Vertical_Market_Focus__c = 'Advertising';
        mdf.Marketing_Dollars_Requested__c = 1000;
        mdf.Target_Audience__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1500;
        
        insert mdf;
        
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(mdf);
        VF_MDF_Request thePage = new VF_MDF_Request(teststandard);
        thePage.createAttachmentHolder();
        thePage.submitForApproval();
        thePage.saveAsDraft();
        thePage.turnExitingRecordNonEditable();
        thePage.cancelRequest();
        thePage.ChangeCount();
        
    }
}