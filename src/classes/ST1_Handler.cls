public class ST1_Handler {
    
    private Id usersQueueId = ENV.ST1;
  
  // This method will update the new ST1 fields (relating to cases statuses) upon creation of a new ST1 record
  // It will run on a creation of a SINGLE ST1 record (Mass data loading isn't possible in this case)
  public void updateST1ParamsOnInsert(list<ST1_Shift__c> newST1_List) {
  
    list<Case> relatedCases_List = [SELECT Id, State__c, OwnerId FROM Case WHERE OwnerId =: usersQueueId and Status != 'Closed'];
    
    Integer unclosedCases = 0;
    Integer AssignedCases = 0;
    Integer OpenCases = 0;
    Integer WaitingCustomerApprovalCases = 0;
    Integer WaitingCustomerInfoCases = 0;
    Integer RMAApprovedCases = 0;
    
    for (Case c : relatedCases_List) {
      
      if (!c.State__c.EqualsIgnoreCase('Closed')) {
        
        unclosedCases ++;
      }
      
      
        if (c.State__c.EqualsIgnoreCase('Assigned')) {
          
          AssignedCases ++;
        }
        
        else if  (c.State__c.EqualsIgnoreCase('Waiting for Customer Approval')) {
          
          WaitingCustomerApprovalCases ++;
        }
        
        else if  (c.State__c.EqualsIgnoreCase('Open')) {
          
          OpenCases ++;
        }
        
        else if  (c.State__c.EqualsIgnoreCase('Waiting for Customer Info')) {
          
          WaitingCustomerInfoCases ++;
        }
        
        else if  (c.State__c.EqualsIgnoreCase('RMA Approved')) {
          
          RMAApprovedCases ++;
        }
    }
     for (ST1_Shift__c st1 : newST1_List) {
       
       st1.Number_Of_Assigned_Cases__c = AssignedCases;
       st1.Number_Of_Open_Cases__c = OpenCases;
       st1.Number_Of_Unclosed_Cases__c = unclosedCases;
       st1.Number_Of_Waiting_Customer_Info__c = WaitingCustomerInfoCases;
       st1.Number_Waiting_Customer_Approval_Cases__c = WaitingCustomerApprovalCases;
       st1.Number_Of_Approved_Cases__c = RMAApprovedCases;
     }
  }
  
  // This method will update the last Craeted ST1 with the number of cases that were opened/Closed/status changed and comments added 
  // since it was created and will update it's status to 'Closed'
  public void updateLastST1ParamsOnInsert(list<ST1_Shift__c> newST1_List) {
    
    Integer numOfCasesMovedFromAssigned = 0;
    Integer numOfCasesMovedToAssigned   = 0;
    ST1_Shift__c st1 = new ST1_Shift__c();
    
    try {
      
       st1 = [SELECT Id, Cases_escalated_to_GSO__c, Name, CreatedDate, Comments_added__c, OwnerId, Cases_from_Assigned_to_Other__c, Cases_from_Other_to_Assigned__c,
                Cases_Opened_during_shift__c, Cases_Closed_during_shift__c
                FROM ST1_Shift__c WHERE isDeleted != true order by CreatedDate desc limit 1];
    }
    
    catch(Exception e) {
      
      //newST1_List[0].addError('There is no ST1 Shift exists ');
    }
    system.debug('st1 : ' + st1);            
    
    if (st1.Id != null) {
      
      Id st1GroupId = ENV.ST1USers;
      list<GroupMember> str1Users_List = [SELECT UserOrGroupId, GroupId FROM GroupMember WHERE GroupId =: st1GroupId];
      
      set<Id> usersIds_Set = new set<Id>();
      
      for (GroupMember member : str1Users_List) {
        
         usersIds_Set.add(member.UserOrGroupId);
         system.debug('member.UserOrGroupId : ' + member.UserOrGroupId);
         system.debug('member.GroupId : ' + member.GroupId);
      }
      
      list<Case> NewOpenedCases_List = [SELECT Id FROM Case WHERE CreatedDate >: st1.CreatedDate and OwnerId =: usersQueueId];
                          
      list<Case> NewClosedCases_List = [SELECT Id FROM Case WHERE ClosedDate >: st1.CreatedDate and OwnerId =: usersQueueId];  
      
      //list<Case> escalated2GSOCases_List = [SELECT Id FROM Case WHERE Escalated_to_GSO_time__c >: st1.CreatedDate and OwnerId =: usersQueueId];
      list<Case> escalated2GSOCases_List;
      
      if (!usersIds_Set.isEmpty()) {
         
          escalated2GSOCases_List = [SELECT Id FROM Case WHERE Escalated_to_GSO_time__c >=: st1.CreatedDate and Prev_Assignee__c IN : usersIds_Set];
      }
      
      system.debug('st1.CreatedDate : ' + st1.CreatedDate);
      system.debug('usersQueueId : ' + usersQueueId);
      system.debug('escalated2GSOCases_List : ' + escalated2GSOCases_List);
                          
      list<FM_Discussion__c> newDiscussions_List = [Select f.Id From FM_Discussion__c f WHERE CreatedDate >: st1.CreatedDate 
                               and  f.Case__r.OwnerId =: usersQueueId and f.Source__c =: 'Mellanox User'];
                               
      list<Case> casesWithStatusChanged_List = [SELECT Id, Moved_from_Assigned_time__c, Moved_to_Assigned_time__c FROM Case  
                            WHERE (Moved_from_Assigned_time__c >=: st1.CreatedDate OR Moved_to_Assigned_time__c >=: st1.CreatedDate)
                            and OwnerId =: usersQueueId];    
                                   
      for (Case c: casesWithStatusChanged_List) {
        
        if (c.Moved_from_Assigned_time__c > st1.CreatedDate) {
          
          numOfCasesMovedFromAssigned ++;
        }
        
        if (c.Moved_to_Assigned_time__c > st1.CreatedDate) {
          
          numOfCasesMovedToAssigned ++;
        }
      }
      
      st1.Cases_from_Assigned_to_Other__c = numOfCasesMovedFromAssigned;
      st1.Cases_from_Other_to_Assigned__c = numOfCasesMovedToAssigned;
      st1.Cases_Closed_during_shift__c    = NewClosedCases_List.size();
      st1.Cases_Opened_during_shift__c    = NewOpenedCases_List.size();
      st1.Cases_escalated_to_GSO__c       = escalated2GSOCases_List.size();
      
       system.debug('st1.Cases_escalated_to_GSO__c : ' + st1.Cases_escalated_to_GSO__c);
        system.debug('escalated2GSOCases_List.size() : ' + escalated2GSOCases_List.size());
        
      st1.Comments_added__c               = newDiscussions_List.size();
      st1.Status__c                   = 'Closed';
      
       try {
        
            update st1;
       }
                   
       catch(Exception e) {
         
         newST1_List[0].addError('The operation could not be performed because of: '+ e.getMessage());
       }
     }
  }
}