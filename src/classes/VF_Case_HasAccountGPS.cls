/*****       This class is a controller class that will determeine if the message 
                          that is on the related VF will be displayed on the Case Layout    *****/
public with sharing class VF_Case_HasAccountGPS {
    
    public Case theCase {get;set;}
    public Milestone1_Project__c theMilestonePrj {get;set;}
    //Boolean that determines if the output will be displayed on tha page
    public boolean showPage {get;set;}
    public String milestoneProjStr {get;set;}
    
    public VF_Case_HasAccountGPS(ApexPages.StandardController controller)
    {
        theCase = (Case)controller.getRecord();
        
        Case dbCase = [SELECT Id, AccountId FROM Case WHERE Id =: theCase.Id];
        showPage = false;
        
         Id projRecTypeId = [SELECT Id FROM RecordType WHERE developerName =: 'Service_Delivery_Project'
        				    and Id != '01250000000Dys7AAC' and Id != '01250000000Dys7' limit 1].Id;
        
        
       if (projRecTypeId  != null) {
            
        	Account acc = new Account();
        	
            acc = [SELECT Id, (Select Name, RecordTypeId, Status__c, Deadline__c From Projects__r Where
            (Status__c =: 'Completed & Accepted/no Invoice' OR Status__c =: 'Completed & Accepted')
            and Deadline__c <: date.today() and Deadline__c >: date.today().addMonths(-6) and RecordTypeId =: projRecTypeId)
            FROM Account WHERE Id =: dbCase.AccountId];
        
        
            if (acc.Id != null)
            {
                list<Milestone1_Project__c> milestoneProjects_List = acc.Projects__r;
                
                if (!milestoneProjects_List.isEmpty())
                {
                    showPage = true;
                    Date latestDueDate = date.today().addMonths(-7);
                    for (Milestone1_Project__c proj : milestoneProjects_List)
                    {
                        if (proj.Deadline__c > latestDueDate)
                        {
                            latestDueDate = proj.Deadline__c;
                            theMilestonePrj = proj;
                            milestoneProjStr = '/'+ theMilestonePrj.Id;
                        }
                    }
                }
            }
        } 
    }
    
    

}