public with sharing class mlnxMobileContentTableController {
	
	public list<ContentVersion> selectedContentVerions_List {get;set;}
	public boolean displayErrorMessage {get;set;}
	public boolean listHasValues{get;set;}
	public string currentUserMail {get;set;}
	public String selectedDocType {get;set;}
 	public list<SelectOption> docTypes_list {get;set;}
 	private Id assetId;
 	private Asset2__c currentAsset;
 	private map<Integer, String> counter2DocTypes_Map;
 	
	public mlnxMobileContentTableController (ApexPages.StandardController controller) {
		
		assetId = controller.getId();
 		//selectedDocType = Apexpages.currentPage().getParameters().get('selectedDocType');
		
		currentUserMail = UserInfo.getUserEmail();
		
 		system.debug('assetId : ' + assetId);
		system.debug('selectedDocType : ' + selectedDocType);
		
		
	}
	
	public List<SelectOption> getDocTypes() {
	 	
	 	listHasValues = false;
	    docTypes_list = new List<SelectOption>();
		counter2DocTypes_Map = new map<Integer, String>();

	    
	    docTypes_list.add(new selectOption ('None', '--NONE--'));
	    docTypes_list.add(new selectOption ('Application Note', 'Application Note'));
	    docTypes_list.add(new selectOption ('Datasheet (HRM)', 'Datasheet (HRM)'));
	    docTypes_list.add(new selectOption ('PRM (Prog Ref Manual)', 'PRM (Prog Ref Manual)'));
	    docTypes_list.add(new selectOption ('Reference Design', 'Reference Design'));
 		
	    return docTypes_list;
	}
	
	public void moveToContectTablePage() {
		
		  list<ContentVersion> contentVersionsByOPNs_List = new list<ContentVersion>();
		 Asset2__c currentAsset = [SELECT Id, Name, Part_Number__c,  Product__r.Product_Detail_Name__c, Product__r.Name FROM Asset2__c WHERE Id =: assetId];
				
		
		try {	
			
		 /*
			map<Id, ContentVersion> contentVersionIds2ContentVersions_Map = new map<Id, ContentVersion>([SELECT  Title, Size__c, Id, Doc_SW_Revision__c, Document_Type__c, Description, Product_Family__c, FileType,
									ContentUrl, Product__c , Category__c FROM ContentVersion 
									WHERE Product__c =: relatedProduct.Product_Detail_Name__c and Document_Type__c =: selectedDocType
									and IsLatest = true 
									order by Doc_SW_Revision__c desc]);
		*/	 
			//************************FOR TESTING  **********************	
			map<Id, ContentVersion> contentVersionIds2ContentVersions_Map = new map<Id, ContentVersion>();
			
			
			/*
			([SELECT  Title, Size__c, Id, Doc_SW_Revision__c, Document_Type__c, Description, Product_Family__c, FileType,
									ContentUrl, Product__c , Category__c, MyMellanox_direct_link__c, ContentSize FROM ContentVersion 
									WHERE Document_Type__c =: selectedDocType
									and IsLatest = true   and  ContentSize < 40000  
									order by Doc_SW_Revision__c desc]);
			*/ 
											
			if (currentAsset.Product__r.Name != null) {	
				
				
				
				
				try {
					String query = 'SELECT  Title, Size__c, Id, Doc_SW_Revision__c, Document_Type__c, Description, Product_Family__c, FileType, ' +
								   'ContentUrl, Product__c , Category__c FROM ContentVersion WHERE (Product__c ' +
								   'includes ' +  '(' + '\'' + currentAsset.Product__r.Product_Detail_Name__c + '\'' + ')' + 
								   ' OR OEM_Product__c includes ' +  '(' + '\'' + currentAsset.Product__r.Product_Detail_Name__c + '\'' + ')' + 
								   ' OR Moblie_OPN__c includes ' +  '(' + '\'' + currentAsset.Product__r.Product_Detail_Name__c  + '\'' + '))' +
								   ' and Document_Type__c = \'' + selectedDocType + '\'' +
								   ' and IsLatest = true';
								   
								    
				   system.debug('query : ' + query);
				   contentVersionsByOPNs_List.add(database.Query(query));
				}
				
			catch(Exception e) {
					
					
				}
			 
			    for (ContentVersion version : contentVersionsByOPNs_List) {
			   	
			   		if (!contentVersionIds2ContentVersions_Map.containsKey(version.Id)) {
			   			
			   			contentVersionIds2ContentVersions_Map.put(version.Id, version);
			   		}
			    }				   
			}
			
			selectedContentVerions_List = contentVersionIds2ContentVersions_Map.values();
		}
		
		catch(Exception e) {
			
			displayErrorMessage = true;
		}
		
		if (selectedContentVerions_List.isEmpty()) {
			
			
			displayErrorMessage = true;
		}
		
		else {
			
			listHasValues = true;
			displayErrorMessage = false;
		}
	}
}