@isTest
private class test_callCenter {

    static testMethod void callCenter_CustomerCase() 
    {
    
        string email = string.valueOf(system.currentTimeMillis()) + '@test.com';
        string email3 = string.valueOf(system.currentTimeMillis()) + '3@test.com';
        string email4 = string.valueOf(system.currentTimeMillis()) + '4@test.com';
        string email5 = string.valueOf(system.currentTimeMillis()) + '5@test.com';


                
        Contact c = new Contact(LastName='Test', FirstName='Test', AccountId=ENV.UnknownAccount, email=email, Contact_Origin__c='Mellanox');
        insert c;
        
        Contact c1 = new Contact(LastName='Test', FirstName='Test', AccountId=ENV.UnknownAccount, email=email3);
        insert c1;

        
        Call_Center_Customer__c ccc = new Call_Center_Customer__c(Contact_name__c=c.Id, Email__c=email4, Customer_Local_Time__c = '13:00');
        insert ccc;
        
        Call_Center_Customer__c ccc1 = new Call_Center_Customer__c(Contact_name__c=c1.Id, Email__c=email5, Customer_Local_Time__c = '13:00');
        insert ccc1;

        
        ccc.Contact_Name__c = null;
        ccc.Account_Name__c = ENV.UnknownAccount;
        ccc.Email__c = null;
        ccc.First_Name__c = null;
        ccc.Last_Name__c = null;
        ccc.Customer_Source__c = null;
        try
        {
            update ccc;
        }catch(exception e){}
        
        Contract2__c c2 = new Contract2__c(Account__c = ENV.UnknownAccount, Contract_Term_months__c=12 );
        insert c2;
        
        Asset a = new Asset (Name='Test', Serial_Number__c='12345', AccountId=ENV.UnknownAccount);
        insert a;
        
        ccc.Account__c = null;
        ccc.Email__c = 'test@test.com';
        ccc.Serial_Number__c = '12345';
        ccc.First_Name__c = 'Test';
        ccc.Last_Name__c='Test';
        ccc.Customer_Source__c = 'Mellanox';
        update ccc;
    }
    static testMethod void callCenter_CaseDetails() 
    {
              
       string email1 = string.valueOf(system.currentTimeMillis()) + '1@test.com';
       string email2 = string.valueOf(system.currentTimeMillis()) + '2@test.com';

       
        Account Acc = new Account(name = 'geog');
        Contact c = new Contact(LastName='Test', FirstName='Test', AccountId=ENV.UnknownAccount, email=email1, Contact_Origin__c='Mellanox');
        insert c;
        Contact c1 = new Contact(LastName='Test', FirstName='Test', AccountId=ENV.UnknownAccount, email=email2);
        insert c1;

        
        
        List<Call_Center_Customer__c> cccList = new List<Call_Center_Customer__c>();
        Call_Center_Customer__c ccc = new Call_Center_Customer__c(Contact_name__c=c.Id, Email__c='test@test.com', Customer_Local_Time__c = '13:00',geographic__c = 'Global');
        cccList.add(ccc);

        Call_Center_Customer__c ccc1 = new Call_Center_Customer__c(Contact_name__c=c1.Id, Email__c='test@test.com', Customer_Local_Time__c = '13:00',geographic__c = 'US');
        cccList.add(ccc1);
        
        
        Call_Center_Customer__c ccc2 = new Call_Center_Customer__c();
        ccc2.Contact__c = null;
        ccc2.Account__c = null;
        ccc2.Customer_Local_Time__c = '13:00';
        ccc2.Email__c = 'test@test.com';
        ccc2.Serial_Number__c = '12345';
        ccc2.First_Name__c = 'Test';
        ccc2.Last_Name__c='Test';
        ccc2.Customer_Source__c = 'Mellanox';
        cccList.add(ccc2);
        insert cccList;
        
        Call_Center__c cc = new Call_Center__c(Call_Center_Customer__c=ccc.Id, RecordTypeId = ENV.CallTypeSupport, Subject__c = 'Test',  Problem_Description__c = 'Test', Call_Center_Representative__c = 'Test', Severity__c = 'High' );
        cc.impact_net__c = 'Critical - Site Down Or interruption of Production service';
        insert cc;
        
        Call_Center__c cc2 = new Call_Center__c(Call_Center_Customer__c=ccc2.Id, RecordTypeId = ENV.CallTypeMarketing, Subject__c = 'Test2',  Customer_Query__c = 'Test2', Call_Center_Representative__c='Test2');
        cc2.impact_net__c = 'High - Intermittent disruption of service Or jeopardizing production rollout';
          insert cc2;
    }
}