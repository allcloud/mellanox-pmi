public class VFquote_pdf
{
    public Quote currQuote {get;set;}
    public Quote currQuoteService {get;set;}
    //public Quote currQuoteRebate {get;set;}
    public Decimal productSum  {get;set;}
    public Decimal serviceSum  {get;set;}
    //public Decimal rebateSum  {get;set;}
    public integer colspn  {get;set;}
    public String preparedFor {get; set; }
    public list<QuoteLineItemBean> quoteLineItemBean_List {get;set;}
    ApexPages.StandardController controller;
    
    // Elad Kaplan 11/05/2015
    // Added an Inner class to hold params from the QuoteLineItem and display it on the page 'quote_pdf_no_msrp_ERI'
    // since the Community users doesn't have access to any PriceBook, thus can't view any of the QuoteLineItem fields values
    public class QuoteLineItemBean {
    	
    	public String ProductDescriptionBean {get;set;}
    	public decimal quantityBean {get;set;}
    	public decimal listPriceBean {get;set;}
    	public decimal totalPriceBean {get;set;}
    	public String CustomerRefPartNumberBean {get;set;}
    	public decimal discountMSRPBean {get;set;}
    	public decimal MSRPBean {get;set;}
    	public String ProductBean {get;set;}
    	
    }
    
    public VFquote_pdf(ApexPages.StandardController controller)
    {
        ID quoteId;
        quoteLineItemBean_List = new list<QuoteLineItemBean>();
        
        try {
            quoteId = Apexpages.currentPage().getParameters().get('quoteid');
        }
        
        catch (Exception e) {
        }
        
        if (quoteId == null) {
            
            quoteId = controller.getRecord().Id;
        }
        
        currQuote = [Select Print_Customer_Ref_P_N__c,Print_MSRP__c ,Add_Price_List__c,  Distributor_SI__c, Site__c, Status, LastModifiedDate, ExpirationDate, Signature_User_When_Print_Quote__c,
			        Print_MSRP_Discount__c,Configuration_Quote__c, Opportunity.Account.Name, Account_nonmanaged_Quote__c, TotalPrice, 
			        Account_nonmanaged_Quote__r.Name,Opportunity.owner.Title, Opportunity.owner.Email, Opportunity.owner.Phone, Opportunity.owner.MobilePhone,
        			Signature_User_When_Print_Quote__r.Name, Signature_User_When_Print_Quote__r.Title, Signature_User_When_Print_Quote__r.Email, Signature_User_When_Print_Quote__r.Phone,
        			Signature_User_When_Print_Quote__r.MobilePhone, Support_Value__c,  Add_Percents_Vars_Dist_F__c, 
			        Contact.Name, Account__r.Name, QuoteNumber, Opportunity.owner.Name, Distributor_SI__r.Name,Contact__r.Name, 
			        (Select PricebookEntry.Product2.MSRP__c, PricebookEntry.Product2.Description, Product_Decription__c, 
			        PricebookEntry.Product2Id, PricebookEntryId, Quantity,ListPrice, UnitPrice, Product_Description_ERI__c, 
			        Discount_from_PL__c,List_Price_Holder__c,TotalPrice, Product__c,
			        Discount_MSRP__c, Customer_Ref_Part_Number__c, Order__c,MSRP__C
			        From QuoteLineItems
			        where (Product__c = 'REBATE' or product_type__c not in ('HW Support','SW Support','support','GPS','OTHER'))
			        Order By Rank_Product__c,SortOrder)
			        From Quote q Where Id=: quoteId and IsDeleted = false limit 1][0];
			        
			        system.debug('currQuote.Site__c : ' + currQuote.Site__c);
			        system.debug('currQuote.Support_Value__c : ' + currQuote.Support_Value__c);  
			        
			        
			        
		for (QuoteLineItem qLI : currQuote.QuoteLineItems) {
        	
        	QuoteLineItemBean theBean = new QuoteLineItemBean();
        	theBean.ProductDescriptionBean = qLI.Product_Description_ERI__c;
        	theBean.quantityBean = qLI.Quantity;
        	theBean.listPriceBean = qLI.UnitPrice;
        	theBean.totalPriceBean = qLI.TotalPrice;
        	theBean.CustomerRefPartNumberBean = qLI.Customer_Ref_Part_Number__c;
        	theBean.discountMSRPBean = qLI.Discount_MSRP__c;
        	theBean.MSRPBean = qLI.MSRP__C;
        	theBean.ProductBean = qLI.Product__c;
        	quoteLineItemBean_List.add(theBean);
        }	        
			                
        preparedFor = currQuote.Contact.Name != null ? currQuote.Contact.Name : currQuote.Account__r.Name;
        if (currQuote.Add_Price_List__c==TRUE)
        {
            colspn =7;
        }else if (currQuote.Configuration_Quote__c==TRUE) {
            colspn =3;
            
        } else{
            colspn=5;
        }
        if (currQuote.Print_MSRP__c==TRUE)
        {
            
            colspn++;
        }
        if (currQuote.Print_MSRP_Discount__c==TRUE)
        {
            
            colspn++;
        }
        
        if (currQuote.Print_Customer_Ref_P_N__c!=TRUE) {
            colspn--;
        }
        productSum=0;
        for (QuoteLineItem qLI : currQuote.QuoteLineItems){
            
            productSum= productSum + qLI.TotalPrice;
        }
        
        
        currQuoteService = [Select Id, Print_MSRP_Discount__c, Opportunity.Old_Contract__c, Configuration_Quote__c, Signature_User_When_Print_Quote__c,
        					Account_nonmanaged_Quote__r.Name, Opportunity.owner.Title, Opportunity.owner.Email, Opportunity.owner.Phone, Opportunity.owner.MobilePhone,
        					Signature_User_When_Print_Quote__r.Name, Signature_User_When_Print_Quote__r.Title, Signature_User_When_Print_Quote__r.Email, Signature_User_When_Print_Quote__r.Phone,
        					Signature_User_When_Print_Quote__r.MobilePhone, Support_Value__c, Print_Customer_Ref_P_N__c, TotalPrice ,
        				    Add_Price_List__c, Add_Percents_Vars_Dist_F__c, (Select PricebookEntry.Product2.MSRP__c, 
        				    PricebookEntry.Product2.Description, Product_Decription__c,    
					        PricebookEntry.Product2Id, PricebookEntryId, Quantity, ListPrice,UnitPrice,
					        Discount_from_PL__c,TotalPrice,List_Price_Holder__c, Product__c, Product_Description_ERI__c, 
					        Discount_MSRP__c, Customer_Ref_Part_Number__c, Order__c,MSRP__C
					        From QuoteLineItems
					        where product_type__c  in ('HW Support','SW Support','support','GPS','OTHER') and Product__c != 'REBATE'
					        Order By Rank_Support__c,SortOrder)
					        From Quote q Where Id=: quoteId and IsDeleted = false limit 1][0];
					        serviceSum=0;
					        					        
        for (integer x=0;x<currQuoteService.QuoteLineItems.size();x++) {
        	
            serviceSum= serviceSum + currQuoteService.QuoteLineItems[x].TotalPrice;
            
        }
        
        for (QuoteLineItem qLI : currQuoteService.QuoteLineItems) {
        	
        	QuoteLineItemBean theBean = new QuoteLineItemBean();
        	theBean.ProductDescriptionBean = qLI.Product_Description_ERI__c;
        	theBean.quantityBean = qLI.Quantity;
        	theBean.listPriceBean = qLI.UnitPrice;
        	theBean.totalPriceBean = qLI.TotalPrice;
        	theBean.CustomerRefPartNumberBean = qLI.Customer_Ref_Part_Number__c;
        	theBean.discountMSRPBean = qLI.Discount_MSRP__c;
        	theBean.MSRPBean = qLI.MSRP__C;
        	theBean.ProductBean = qLI.Product__c;
        	quoteLineItemBean_List.add(theBean);
        }
        //KN added - Handling REBATE
        /*
        currQuoteRebate = [Select (Select PricebookEntry.Product2.MSRP__c, PricebookEntry.Product2.Description,
        PricebookEntry.Product2Id, PricebookEntryId, Quantity, ListPrice,UnitPrice,
        Discount_from_PL__c,TotalPrice,List_Price_Holder__c, Product__c,
        Discount_MSRP__c, Customer_Ref_Part_Number__c, Order__c,MSRP__C
        From QuoteLineItems
        where Product__c = 'REBATE'
        Order By Rank_Support__c,SortOrder)
        From Quote q Where Id=: quoteId and IsDeleted = false limit 1][0];
        if (currQuoteRebate!=null){
        rebateSum=0;
        for (integer x=0;x<currQuoteRebate.QuoteLineItems.size();x++){
        rebateSum= rebateSum + currQuoteRebate.QuoteLineItems[x].TotalPrice;
        
        }
        }
        */
        //
    }
    
    public void checkSendGetAQuoteCBTrue() {
        
        Contract2__c theContract;
        
        if (currQuoteService.Opportunity.Old_Contract__c != null) {
            
            theContract = new Contract2__c(Id = currQuoteService.Opportunity.Old_Contract__c);
            theContract.Send_get_a_Quote_email__c = true;
            
            try {
                update theContract;
            }
            
            catch(Exception e) {
                
            }
        }
    }
}