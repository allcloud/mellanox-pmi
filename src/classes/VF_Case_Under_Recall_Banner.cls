public class VF_Case_Under_Recall_Banner {
	
	public Case theCase {get;set;}
    public boolean is_Recall {get;set;}

    public VF_Case_Under_Recall_Banner(ApexPages.standardController controller) 
    { 
        is_Recall = false;
        Id caseId = controller.getId();
        
        theCase = [SELECT id, Case_Type__c, Account.Recall__c FROM Case WHERE Id =: caseId];
        
        if ( theCase.Account.Recall__c != null && theCase.Case_Type__c != null && theCase.Case_Type__c.contains('System') )  { 
            
            is_Recall = true;
        }
    }

}