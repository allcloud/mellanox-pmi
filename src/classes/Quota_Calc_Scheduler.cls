global class Quota_Calc_Scheduler implements Schedulable{
	global void execute(SchedulableContext SC) {
		Quota_Calculation.runCalc();
/*
		List<Period> periods = new List<Period>([select StartDate, EndDate from Period where Type='Quarter' and EndDate >= :System.today() order by EndDate limit 1]);
		// Hold map all Sales Guys to their number Opty Actuals
		Map<ID,Double> map_Salesguy_OptyActuals = new Map<ID,Double>();
		// Map Region to number Opty Actuals
		// If Sergio is an opty owner, he will be credit BOTH (opty owner + Region owner)
		Map	<String,Double> map_Region_OptyActuals = new Map <String,Double>();
		//Map Region to Sales Directors
		Map<String,ID> map_region_SalesDirector = new Map<String,ID>();
		map_region_SalesDirector.put('Americas','005500000015ZVaAAM'); //Sergio G
		map_region_SalesDirector.put('JPN','005500000014dfZAAQ'); //Chris Tsumura 
		map_region_SalesDirector.put('CTK','005500000011csMAAQ'); // Joe Cherng
		map_region_SalesDirector.put('SEA','00550000001qXXaAAM'); //= Irfan Modak 
		map_region_SalesDirector.put('EUR','0055000000152snAAA'); // Yossi Avni
		map_region_SalesDirector.put('MFS','00550000001s3jfAAA'); // Dale D’Alessio
		//Date used to run Previous Quarter
		Date prev_quarter_start = Date.newinstance(2013,1,1);
		Date prev_quarter_end = Date.newinstance(2013,3,31);
		
		//Handle count overlap number for China
		//Case: BizDev deals where Westwoord Liu got credit and Jeff Liu is opty owner, end up Myles Yu number is sum of all, causing double counting
		//Map<String,ID> map_China_users = new Map<String,ID>{'Myles Yu' =>'005500000014Jd9','Jeffrey Liu' => '00550000001qJRs','Roger Yang' => '00550000001sFiK'};
		Set<ID> set_China_users = new Set<ID>{'005500000014Jd9','00550000001qJRs','00550000001sFiK'};
		Double optyActuals_count_twice_WestwoodLiu = 0;
		Double optyCommitted_count_twice_WestwoodLiu = 0;
		//				
		Map<String,ID> map_bizdevusers = new Map<String,ID>{'Amir Prescher' =>'005500000015ZVp','Tony Rea' => '00550000000x9VB',
															'Chris Shea' => '005500000013nrY',
															'Danny Gur' => '005500000015ZVu',
															'Westwood Liu' => '00550000001qXXc', 'Carol Ann Hoag' => '00550000001r3oB',
															'Richard Hastie' => '00550000001rNp1',
															'Ezra Yosef' => '00550000001rmJF', 
															'Asaf Wachtel' => '005500000015ZVl',
															'Nimrod Gindi' => '00550000000x1y0'};
		//old logic
		List<Quota__c> lst_quota = new List<Quota__c>([Select id,Quota_Period_1__r.Current_Quarter__c,Opportunity_Actuals_Before_Adjustment__c,
														Opportunity_Committed__c,Opportunity_Actuals__c,Revenue_Actuals__c,
														Revenue_TOGO__c,Revenue_Actuals_Before_Adjustment__c,Revenue_Quota__c,Opportunity_Quota__c,
		 												Employee_Name__c,Opportunity_Adjustment__c,Revenue_Adjustment__c
		 												From Quota__c where Quota_Period_1__r.Current_Quarter__c=true and Employee_Name__c != null ]);
		if(lst_quota==null || lst_quota.size()==0)
			return;
			
		Map<ID,Quota__c> map_userID_quota = new Map<ID,Quota__c>();
		for (Quota__c q : lst_quota){
			map_userID_quota.put(q.Employee_Name__c,q);
		} 	
		for (Quota__c q : map_userID_quota.values()){
			q.Opportunity_Actuals_Before_Adjustment__c = 0;
			q.Revenue_Actuals_Before_Adjustment__c = 0;
			q.Opportunity_Committed__c = 0;	
			q.Revenue_TOGO__c = 0;
			q.Revenue_Adjustment__c = 0;
			q.Opportunity_Adjustment__c = 0;
		}	 
		
		List<Opportunity> lst_optys = new List<Opportunity>([select id,CloseDate,StageName,Sales_Credit_Amount__c,Amount,Sub_Channel__c,
												OwnerID,Owner.Name,BizDev_Opp__c,BizDev_Rep__c from Opportunity
												where 
												//Required_Ship_Date__c >= :periods[0].StartDate and Required_Ship_Date__c <= :periods[0].EndDate
												Required_Ship_Date__c >= :prev_quarter_start and Required_Ship_Date__c <= :prev_quarter_end
												//and Sales_Credit_Amount__c!=null and Sales_Credit_Amount__c>0 
												and (StageName = 'Close Won' OR StageName = 'Closed Won' OR StageName='Commit') ]);
		for (Opportunity o : lst_optys){
			if(o.StageName=='Close Won' || o.StageName=='Closed Won'){
				//Sum based on Owner
				if(map_userID_quota.containsKey(o.OwnerID)){
					if(map_userID_quota.get(o.OwnerID).Opportunity_Quota__c!=null && map_userID_quota.get(o.OwnerID).Opportunity_Quota__c>0){						
						if(map_bizdevusers.containsKey(o.Owner.Name))
							map_userID_quota.get(o.OwnerID).Opportunity_Actuals_Before_Adjustment__c += o.Amount!=null ? o.Amount : 0;
						else
							map_userID_quota.get(o.OwnerID).Opportunity_Actuals_Before_Adjustment__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
						//Handle double counting Westwood Liu
						if(o.OwnerID=='00550000001qXXcAAM' && o.Amount != o.Sales_Credit_Amount__c)
							optyActuals_count_twice_WestwoodLiu += (o.Amount!=null ? o.Amount : 0) - (o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0);
						//							
					}						
				}	
				//Sum Region
				if(map_region_SalesDirector.containsKey(o.Sub_Channel__c)){
					if(map_region_SalesDirector.get(o.Sub_Channel__c)!=null && map_region_SalesDirector.get(o.Sub_Channel__c)!=o.OwnerID && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c))!=null)
					{		
						if(map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c>0)				
							map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Actuals_Before_Adjustment__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
					}						
				}
				//Take care BixDev case ?
				//Chris asks to change to sum up Amount instead of Sales Credit for Bizdev guys this quarter ONLY
				if (o.BizDev_Opp__c==true && o.BizDev_Rep__c!=null && map_bizdevusers.get(o.BizDev_Rep__c)!=null && map_bizdevusers.get(o.BizDev_Rep__c)!=o.OwnerID){
					if(map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c))!=null)
					{
						if(map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Quota__c>0)
							//map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Actuals_Before_Adjustment__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
							map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Actuals_Before_Adjustment__c += o.Amount!=null ? o.Amount : 0;
						//Avoid count twice Westwood Liu number when roll up to Myles Yu
						//KN -added 4-16-13
						if(o.BizDev_Rep__c=='Westwood Liu' && set_China_users.contains(o.OwnerID))
							optyActuals_count_twice_WestwoodLiu += o.Amount!=null ? o.Amount : 0;
																				
						//							
					}
				}
			}
			else {
				if(map_userID_quota.containsKey(o.OwnerID)){
					if(map_userID_quota.get(o.OwnerID).Opportunity_Quota__c!=null && map_userID_quota.get(o.OwnerID).Opportunity_Quota__c>0){						
						if(map_bizdevusers.containsKey(o.Owner.Name))
							map_userID_quota.get(o.OwnerID).Opportunity_Committed__c += o.Amount!=null ? o.Amount : 0;
						else
							map_userID_quota.get(o.OwnerID).Opportunity_Committed__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
						//Handling Westwood Liu double counting case
						if(o.OwnerID=='00550000001qXXcAAM' && o.Amount != o.Sales_Credit_Amount__c)
							optyCommitted_count_twice_WestwoodLiu += (o.Amount!=null ? o.Amount : 0) - (o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0);							
					}						
				}	
				//Sum Region
				if(map_region_SalesDirector.containsKey(o.Sub_Channel__c)){
					if(map_region_SalesDirector.get(o.Sub_Channel__c)!=null && map_region_SalesDirector.get(o.Sub_Channel__c)!=o.OwnerID && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c))!=null)
					{
						if(map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Quota__c>0)
							map_userID_quota.get(map_region_SalesDirector.get(o.Sub_Channel__c)).Opportunity_Committed__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
					}						
				}
				//Take care BixDev case ?
				//For Bizdev, add up Amount instead of Sales_Credit per Chris request
				if (o.BizDev_Opp__c==true && o.BizDev_Rep__c!=null && map_bizdevusers.get(o.BizDev_Rep__c)!=null && map_bizdevusers.get(o.BizDev_Rep__c)!=o.OwnerID){
	
					if(map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c))!=null)
					{
						if(map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Quota__c>0)
							//map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Committed__c += o.Sales_Credit_Amount__c!=null ? o.Sales_Credit_Amount__c : 0;
							map_userID_quota.get(map_bizdevusers.get(o.BizDev_Rep__c)).Opportunity_Committed__c += o.Amount!=null ? o.Amount : 0;
						//Avoid count twice Westwood Liu number when roll up to Myles Yu
						//KN -added 4-16-13
						if(o.BizDev_Rep__c=='Westwood Liu' && set_China_users.contains(o.OwnerID))
							optyCommitted_count_twice_WestwoodLiu += o.Amount!=null ? o.Amount : 0;
						
						//									
					}
				}	
			}				
		}	
		
		//if (map_userID_quota!=null && map_userID_quota.size()>0)
		//	update map_userID_quota.values();
			
		//Update REV Actuals
		Double total_rev_TOGO = 0;
		Double total_bbb = 0;
		//Double total_adj = 0;
		List<Opportunity> lst_optys_TOGO = new List<Opportunity>([select id,Amount,Opportunity_VT_Region__c,VT__c
																from Opportunity
																where RecordTypeid='01250000000Dw1p'
																and 
																//CloseDate >= :periods[0].StartDate and CloseDate <= :periods[0].EndDate
																CloseDate >= :prev_quarter_start and CloseDate <= :prev_quarter_end
																]);
		List<BBB_Orders__c> orders = new List<BBB_Orders__c>([select id,Ordered_Item__c,Selling_Amount__c,S__c,VT__c,VT_Map__c,VT_Map__r.Name,
																VT1_COMISSION__c,VT1_PERCENTAGE__c,VT2_COMISSION__c,VT2_PERCENTAGE__c,Customer_Parent__c 
																from BBB_Orders__c 
																where 
																//BBBSDT__c >= :periods[0].StartDate and BBBSDT__c <= :periods[0].EndDate
																BBBSDT__c >= :prev_quarter_start and BBBSDT__c <= :prev_quarter_end
																and Dist_Order_type__c != 'Deferred' 
																and (S__c='BLS' or S__c='BxLog' or S__c='BxLOG')
																//and VT__c='USE'
																//and Customer_Parent__c = 'DELL'
																order by Customer_Parent__c,ID
																]);
		
		Map<String,ID> map_VT_SalesDirector = new Map<String,ID>();
		Map<String,String> map_VTMap_VT = new Map<String,String>(); 
		List<VT_Map__c> lst_vtmap = new List<VT_Map__c>([select Name,Sales_Director__c,VT__c from VT_Map__c]);
		for (VT_Map__c v : lst_vtmap){
			map_VT_SalesDirector.put(v.Name, v.Sales_Director__c);	
			if (v.Name != v.VT__c){
				map_VTMap_VT.put(v.Name,v.VT__c);
			}
		}															
//System.debug('... KN map_VT_SalesDirector='+map_VT_SalesDirector);
//System.debug('... KN map_VTMap_VT='+map_VTMap_VT);	
		//Calculate TOGO Revenue
		for (Opportunity o : lst_optys_TOGO){
			total_rev_TOGO += o.Amount!=null ? o.Amount : 0;
			if (o.Opportunity_VT_Region__c!=null){
				if(map_VT_SalesDirector.get(o.Opportunity_VT_Region__c)!=null && map_userID_quota.get(map_VT_SalesDirector.get(o.Opportunity_VT_Region__c))!=null)	
				{
					if(map_userID_quota.get(map_VT_SalesDirector.get(o.Opportunity_VT_Region__c)).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(o.Opportunity_VT_Region__c)).Revenue_Quota__c>0)
						map_userID_quota.get(map_VT_SalesDirector.get(o.Opportunity_VT_Region__c)).Revenue_TOGO__c += o.Amount!=null ? o.Amount : 0;		
				}	
			}
			if(o.VT__c!=null){
				if(map_VT_SalesDirector.get(o.VT__c)!=null
					&& map_VT_SalesDirector.get(o.VT__c) != map_VT_SalesDirector.get(o.Opportunity_VT_Region__c) 
					&& map_userID_quota.get(map_VT_SalesDirector.get(o.VT__c))!=null)	
				{
						if(map_userID_quota.get(map_VT_SalesDirector.get(o.VT__c)).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(o.VT__c)).Revenue_Quota__c>0)
							map_userID_quota.get(map_VT_SalesDirector.get(o.VT__c)).Revenue_TOGO__c += o.Amount!=null ? o.Amount : 0;	
				}
			}
		}
		//	
		Double VT1_sales, VT2_sales;
		for (BBB_Orders__c bbb : orders){
//system.debug('... KN Duanne TOP==='+map_userID_quota.get('005500000011eOnAAI').Revenue_Actuals_Before_Adjustment__c+' bbb.ID='+bbb.id);			
			VT1_sales = VT2_sales = 0;
			total_bbb += bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c : 0;
			if(bbb.VT1_PERCENTAGE__c==null && bbb.VT2_PERCENTAGE__c==null){
				map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT__c)).Revenue_Actuals_Before_Adjustment__c += bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c : 0;
				map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT_Map__r.Name)).Revenue_Actuals_Before_Adjustment__c += bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c : 0;	
			}	
			else {
				if(bbb.VT1_COMISSION__c!='' && bbb.VT1_PERCENTAGE__c!=null && bbb.VT1_PERCENTAGE__c>0){
					VT1_sales = bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c * bbb.VT1_PERCENTAGE__c : 0;
					if(map_VT_SalesDirector.get(bbb.VT1_COMISSION__c)!=null && map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT1_COMISSION__c))!=null)
					{	
						if(map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT1_COMISSION__c)).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT1_COMISSION__c)).Revenue_Quota__c>0)							
							map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT1_COMISSION__c)).Revenue_Actuals_Before_Adjustment__c += VT1_sales;
					}						
					//Roll up

					
					if (map_VTMap_VT.get(bbb.VT1_COMISSION__c) != null){
						if(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))!=null 
								&& map_VT_SalesDirector.get(bbb.VT1_COMISSION__c)!=null
								&& map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c)) != map_VT_SalesDirector.get(bbb.VT1_COMISSION__c) 
								&& map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c)))!=null)
						{								
							if(map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))).Revenue_Quota__c>0)								
								map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT1_COMISSION__c))).Revenue_Actuals_Before_Adjustment__c += VT1_sales;
						}							
					}
					
				}
				if(bbb.VT2_COMISSION__c!='' && bbb.VT2_PERCENTAGE__c!=null && bbb.VT2_PERCENTAGE__c>0){
					VT2_sales = bbb.Selling_Amount__c!=null ? bbb.Selling_Amount__c * bbb.VT2_PERCENTAGE__c : 0;
					if(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)!=null && map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c))!=null)
					{
						if(map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)).Revenue_Quota__c>0)
							map_userID_quota.get(map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)).Revenue_Actuals_Before_Adjustment__c += VT2_sales;
					}						
					//Roll Up							
					if (map_VTMap_VT.get(bbb.VT2_COMISSION__c) != null){
						if(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))!=null 
								&& map_VT_SalesDirector.get(bbb.VT2_COMISSION__c)!=null
								&& map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c)) != map_VT_SalesDirector.get(bbb.VT2_COMISSION__c) 
								&& map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c)))!=null)
						{									
							if(map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))).Revenue_Quota__c>0)
								map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(bbb.VT2_COMISSION__c))).Revenue_Actuals_Before_Adjustment__c += VT2_sales;
						}							
					}	
				}		
			}
			//KN debug
			//if(bbb.Customer_Parent__c=='DELL'){
			//	system.debug('... KN Duanne1 bbb.ID==='+bbb.ID+' VT1_COMISSION__c=='+bbb.VT1_COMISSION__c+' VT2_COMISSION__c=='+bbb.VT2_COMISSION__c+' VT1_PERCENTAGE__c='+bbb.VT1_PERCENTAGE__c+' VT2_PERCENTAGE__c='+bbb.VT2_PERCENTAGE__c);
			//	system.debug('... KN Duanne2==='+map_userID_quota.get('005500000011eOnAAI').Revenue_Actuals_Before_Adjustment__c+' bbb.ID='+bbb.ID+' VT1sales='+VT1_sales+' VT2sales='+VT2_sales);	
			//}
		}	
		//Calculate Adjustment
		
		List<Quota_Adjustment__c> lst_adjustments = new List<Quota_Adjustment__c>([select id,Opportunity_Actuals_Adjustment__c,Revenue_Actuals_Adjustment__c,
																						Quota__r.Employee_Name__c,Region__c,VT_Map__c
																						from Quota_Adjustment__c
																						where Quota_Period__r.Current_Quarter__c=true]);
		for (Quota_Adjustment__c adj : lst_adjustments){
			//total_adj += adj.Revenue_Actuals_Adjustment__c!=null ? adj.Revenue_Actuals_Adjustment__c : 0;
			if(adj.Opportunity_Actuals_Adjustment__c!=null){
				map_userID_quota.get(adj.Quota__r.Employee_Name__c).Opportunity_Adjustment__c += adj.Opportunity_Actuals_Adjustment__c;
				//roll up
				if(adj.Region__c!=null && map_region_SalesDirector.get(adj.Region__c)!=null 
						&& map_region_SalesDirector.get(adj.Region__c)!=adj.Quota__r.Employee_Name__c 
						&& map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c))!=null)
				{		
					if(map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c)).Opportunity_Quota__c!=null && map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c)).Opportunity_Quota__c>0)				
						map_userID_quota.get(map_region_SalesDirector.get(adj.Region__c)).Opportunity_Adjustment__c += adj.Opportunity_Actuals_Adjustment__c;
				} 	
			}
			if(adj.Revenue_Actuals_Adjustment__c!=null){
				map_userID_quota.get(adj.Quota__r.Employee_Name__c).Revenue_Adjustment__c += adj.Revenue_Actuals_Adjustment__c;
				//roll up
				if (adj.VT_Map__c!=null && map_VTMap_VT.get(adj.VT_Map__c) != null){
						if(map_VT_SalesDirector.get(map_VTMap_VT.get(adj.VT_Map__c))!=null 
								&& map_VT_SalesDirector.get(adj.VT_Map__c)!=null
								&& map_VT_SalesDirector.get(map_VTMap_VT.get(adj.VT_Map__c)) != map_VT_SalesDirector.get(adj.VT_Map__c) 
								&& map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(adj.VT_Map__c)))!=null)
						{									
							if(map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(adj.VT_Map__c))).Revenue_Quota__c!=null && map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(adj.VT_Map__c))).Revenue_Quota__c>0)
								map_userID_quota.get(map_VT_SalesDirector.get(map_VTMap_VT.get(adj.VT_Map__c))).Revenue_Adjustment__c += adj.Revenue_Actuals_Adjustment__c;
						}							
				}	
			}
		}
		//
		//Handle special cases
		//Set Marc number to grand total - 005500000011cs7AAA
		if(map_userID_quota.get('005500000011cs7AAA')!=null){
			map_userID_quota.get('005500000011cs7AAA').Revenue_Actuals_Before_Adjustment__c = total_bbb;
			map_userID_quota.get('005500000011cs7AAA').Revenue_TOGO__c = total_rev_TOGO;
			map_userID_quota.get('005500000011cs7AAA').Revenue_Adjustment__c = 0;
		}
			
		//Sum up all USE,USW, USC for Chuck T.
		if(map_userID_quota.get('005500000013P66AAE')!=null && map_userID_quota.get('005500000011vexAAA')!=null){
			map_userID_quota.get('005500000013P66AAE').Revenue_Actuals_Before_Adjustment__c += map_userID_quota.get('005500000011vexAAA').Revenue_Actuals_Before_Adjustment__c;
			map_userID_quota.get('005500000013P66AAE').Revenue_TOGO__c += map_userID_quota.get('005500000011vexAAA').Revenue_TOGO__c;
			map_userID_quota.get('005500000013P66AAE').Revenue_Adjustment__c += map_userID_quota.get('005500000011vexAAA').Revenue_Adjustment__c; 
		}
		//Add Carol Ann Hoag to Chris Shea number
		if (map_userID_quota.get('005500000013nrYAAQ')!=null && map_userID_quota.get('00550000001r3oBAAQ')!=null){
			map_userID_quota.get('005500000013nrYAAQ').Opportunity_Actuals_Before_Adjustment__c += map_userID_quota.get('00550000001r3oBAAQ').Opportunity_Actuals_Before_Adjustment__c;
			map_userID_quota.get('005500000013nrYAAQ').Opportunity_Committed__c += map_userID_quota.get('00550000001r3oBAAQ').Opportunity_Committed__c;
			map_userID_quota.get('005500000013nrYAAQ').Opportunity_Adjustment__c += map_userID_quota.get('00550000001r3oBAAQ').Opportunity_Adjustment__c;
		}
		//Add Ezra Yosef to Chris Shea number
		if (map_userID_quota.get('005500000013nrYAAQ')!=null && map_userID_quota.get('00550000001rmJFAAY')!=null){
			map_userID_quota.get('005500000013nrYAAQ').Opportunity_Actuals_Before_Adjustment__c += map_userID_quota.get('00550000001rmJFAAY').Opportunity_Actuals_Before_Adjustment__c;
			map_userID_quota.get('005500000013nrYAAQ').Opportunity_Committed__c += map_userID_quota.get('00550000001rmJFAAY').Opportunity_Committed__c;
			map_userID_quota.get('005500000013nrYAAQ').Opportunity_Adjustment__c += map_userID_quota.get('00550000001rmJFAAY').Opportunity_Adjustment__c;
		}
		//Add China number, Myles Yu (005500000014Jd9AAE) = Westwood (00550000001qXXcAAM) + Jeff Liu (00550000001qJRsAAM) + Roger Yang(00550000001sFiKAAU)
		if (map_userID_quota.get('005500000014Jd9AAE')!=null && map_userID_quota.get('00550000001qXXcAAM')!=null){
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Actuals_Before_Adjustment__c += map_userID_quota.get('00550000001qXXcAAM').Opportunity_Actuals_Before_Adjustment__c;
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Committed__c += map_userID_quota.get('00550000001qXXcAAM').Opportunity_Committed__c;
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Adjustment__c += map_userID_quota.get('00550000001qXXcAAM').Opportunity_Adjustment__c;
			//Handle subtract overlap numbers
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Actuals_Before_Adjustment__c -= optyActuals_count_twice_WestwoodLiu;
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Committed__c -= optyCommitted_count_twice_WestwoodLiu;
			//
		}
		if (map_userID_quota.get('005500000014Jd9AAE')!=null && map_userID_quota.get('00550000001qJRsAAM')!=null){
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Actuals_Before_Adjustment__c += map_userID_quota.get('00550000001qJRsAAM').Opportunity_Actuals_Before_Adjustment__c;
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Committed__c += map_userID_quota.get('00550000001qJRsAAM').Opportunity_Committed__c;
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Adjustment__c += map_userID_quota.get('00550000001qJRsAAM').Opportunity_Adjustment__c;
		}
		if (map_userID_quota.get('005500000014Jd9AAE')!=null && map_userID_quota.get('00550000001sFiKAAU')!=null){
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Actuals_Before_Adjustment__c += map_userID_quota.get('00550000001sFiKAAU').Opportunity_Actuals_Before_Adjustment__c;
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Committed__c += map_userID_quota.get('00550000001sFiKAAU').Opportunity_Committed__c;
			map_userID_quota.get('005500000014Jd9AAE').Opportunity_Adjustment__c += map_userID_quota.get('00550000001sFiKAAU').Opportunity_Adjustment__c;
		}
		//
		//Assign Scott Mcauffe (00550000001qmAqAAI),Mike Montgomery,Jim Lonergan to be the same rev actuals+togo
		if(map_userID_quota.get('00550000001qmAqAAI')!=null && map_userID_quota.get('00550000001r3o9AAA')!=null){
			map_userID_quota.get('00550000001r3o9AAA').Revenue_Actuals_Before_Adjustment__c = map_userID_quota.get('00550000001qmAqAAI').Revenue_Actuals_Before_Adjustment__c;
			map_userID_quota.get('00550000001r3o9AAA').Revenue_TOGO__c = map_userID_quota.get('00550000001qmAqAAI').Revenue_TOGO__c;
			map_userID_quota.get('00550000001r3o9AAA').Revenue_Adjustment__c = map_userID_quota.get('00550000001qmAqAAI').Revenue_Adjustment__c;	
		}
		if(map_userID_quota.get('00550000001qmAqAAI')!=null && map_userID_quota.get('0055000000148y8AAA')!=null){
			map_userID_quota.get('0055000000148y8AAA').Revenue_Actuals_Before_Adjustment__c = map_userID_quota.get('00550000001qmAqAAI').Revenue_Actuals_Before_Adjustment__c;
			map_userID_quota.get('0055000000148y8AAA').Revenue_TOGO__c = map_userID_quota.get('00550000001qmAqAAI').Revenue_TOGO__c;
			map_userID_quota.get('0055000000148y8AAA').Revenue_Adjustment__c = map_userID_quota.get('00550000001qmAqAAI').Revenue_Adjustment__c;	
		}
		//Assign Ronnie Payne and Duanne Dial (005500000011eOnAAI) to be the same
		if(map_userID_quota.get('005500000011eOnAAI')!=null && map_userID_quota.get('00550000001rh2DAAQ')!=null){
			map_userID_quota.get('00550000001rh2DAAQ').Revenue_Actuals_Before_Adjustment__c = map_userID_quota.get('005500000011eOnAAI').Revenue_Actuals_Before_Adjustment__c;
			map_userID_quota.get('00550000001rh2DAAQ').Revenue_TOGO__c = map_userID_quota.get('005500000011eOnAAI').Revenue_TOGO__c;
			map_userID_quota.get('00550000001rh2DAAQ').Revenue_Adjustment__c = map_userID_quota.get('005500000011eOnAAI').Revenue_Adjustment__c;	
		}
		//Assign Ken Bekampis and Garth Fruge to be the same
		if(map_userID_quota.get('00550000001qfuhAAA')!=null && map_userID_quota.get('0055000000144LxAAI')!=null){
			map_userID_quota.get('0055000000144LxAAI').Revenue_Actuals_Before_Adjustment__c = map_userID_quota.get('00550000001qfuhAAA').Revenue_Actuals_Before_Adjustment__c;
			map_userID_quota.get('0055000000144LxAAI').Revenue_TOGO__c = map_userID_quota.get('00550000001qfuhAAA').Revenue_TOGO__c;
			map_userID_quota.get('0055000000144LxAAI').Revenue_Adjustment__c = map_userID_quota.get('00550000001qfuhAAA').Revenue_Adjustment__c;	
		}
		//Assign Glenn Churchn to be same as Garth Fruge
		if(map_userID_quota.get('00550000001qfuhAAA')!=null && map_userID_quota.get('005500000013vjJAAQ')!=null){
			map_userID_quota.get('005500000013vjJAAQ').Revenue_Actuals_Before_Adjustment__c = map_userID_quota.get('00550000001qfuhAAA').Revenue_Actuals_Before_Adjustment__c;
			map_userID_quota.get('005500000013vjJAAQ').Revenue_TOGO__c = map_userID_quota.get('00550000001qfuhAAA').Revenue_TOGO__c;
			map_userID_quota.get('005500000013vjJAAQ').Revenue_Adjustment__c = map_userID_quota.get('00550000001qfuhAAA').Revenue_Adjustment__c;	
		}
		//Calc Actuals by sum up Actuals_before_Adjustment and Adjustment
		for (Quota__c q : map_userID_quota.values()){
			q.Opportunity_Actuals__c = q.Opportunity_Actuals_Before_Adjustment__c + q.Opportunity_Adjustment__c;
			q.Revenue_Actuals__c = q.Revenue_Actuals_Before_Adjustment__c + q.Revenue_Adjustment__c;
		}
		//End Rev Actuals Calc
		if (map_userID_quota!=null && map_userID_quota.size()>0)
			update map_userID_quota.values();	
*/																				
	}
}