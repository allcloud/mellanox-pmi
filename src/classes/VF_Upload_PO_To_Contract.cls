public class VF_Upload_PO_To_Contract {
    
    public Id theContractId {get;set;}
    public Attachment PODoc {get;set;}
    public Contract2__c theContract {get;set;}
    public BLob body_Str {get;set;}
    public String POName {get;set;}
    public boolean displayUploadThePoButton {get;set;}
    public boolean displayWholeSection {get;set;}
    public boolean displayResetProcessBtn {get;set;}
    
    public VF_Upload_PO_To_Contract(ApexPages.StandardController controller) {
        
        PODoc = new Attachment();
        Id theContractId = controller.getId();
        theContract = [SELECT Id, Name, Contract_Owner__c, Pending_PO__c, (SELECT Id, StageName FROM Opportunities2__r) FROM Contract2__c WHERE Id =: theContractId];
        displayUploadThePoButton = true;
        displayWholeSection = true;
        displayResetProcessBtn = true;
        
        system.debug('theContract : ' + theContract);
         if (theContract != null && !theContract.Opportunities2__r.isEmpty()
        	&& (theContract.Opportunities2__r[0].StageName.EqualsIgnoreCase('Closed Won')
    	    ||  theContract.Opportunities2__r[0].StageName.EqualsIgnoreCase('Close Won')) ) {
        		
        		 
        		displayResetProcessBtn = false;
        	}
    }
    
    
    public pageReference uploadPOFile() {
        
        PODoc.parentId = theContract.Id;
        Attachment clonedAttachment = PODoc.clone();
        clonedAttachment.parentId = theContract.Opportunities2__r[0].Id;
        list<Attachment> attachments_List  = new list<Attachment>{PODoc, clonedAttachment};
        
        system.debug('PODoc : ' + PODoc);
        system.debug('body_Str : ' + body_Str); 
            
        if (PODoc.Name != null) {
            
                    
            try {   
                insert attachments_List;
            }
            
            catch (Exception e) {
                
            }
            
            theContract.Pending_PO__c = false;
           
            
            if (PODoc != null && PODoc.Id != null) {
                
                theContract.Link_To_PO__c = ENV.orgAttFilesPrefix + PODoc.Id;   
            }
            update theContract;
            
            String openerUrl = '/apex/VF_Contract_Renewal_PO_success_page';
            PageReference nextPage = new PageReference(openerUrl);        
            
            nextPage.setRedirect(true);
            return nextPage; 
            
        }
            
        else {
            
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You need to browse for a file before uploading'));
            return null;
        }      
    }
    
    public pageReference moveToQuotePDF() {
        
    
    Integer counter = 0;
    
    while (counter < 5000000) { 
        
        counter ++;
    }
    
    
    
    
    Opportunity relatedOpp;         
            
    try {       
           relatedOpp = [SELECT Id, Old_Contract__c, (SELECT Id FROM Quotes) FROM Opportunity WHERE Old_Contract__c =: theContract.Id];
    }
    
    catch(Exception e) {
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please try again'));
        return null;
    }
    
    if (relatedOpp.Quotes.isEmpty()) {
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please try again'));
        return null;
    }
    
    PageReference nextPage;
    
    if (relatedOpp != null && !relatedOpp.Quotes.isEmpty()) {
        
        String openerUrl = '/apex/quote_pdf_no_msrp_ERI?retURL=apex/VF_Upload_PO_To_Contract?Id=' + theContract.Id  + '&quoteid=' + relatedOpp.Quotes[0].Id;
        nextPage = new PageReference(openerUrl);        
        
        nextPage.setRedirect(true);
        return nextPage;
    }
        
        return null;
    }
    
    public void resetTheProccess() {
        
        theContract.Pending_PO__c = false;
        theContract.Reset_Contract_ERI__c = true;
        update theContract;
        
        displayUploadThePoButton = false;
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your Contract Renewal Process has been reset, You can close the window and start the renewal again'));
        displayWholeSection = false;
    }
    
    public pagereference backHome() {
        
        String openerUrl = '/' + theContract.Id;
        PageReference nextPage = new PageReference(openerUrl);        
        
        nextPage.setRedirect(true);
        return nextPage;
    }
    
    public pagereference backToContractsList() {
        
        String openerUrl = '/VF_Resellers_Related_Contracts?sfdc.tabName=01r50000000AXzH' ;
        PageReference nextPage = new PageReference(openerUrl);        
        
        nextPage.setRedirect(true);
        return nextPage;
    }
    
    
    
  
    
    @future
    public static void uploadPOAsync(String PODoc_Str) {
        
        Attachment POAtt = (Attachment)JSON.deserialize(PODoc_Str, Attachment.class);
        insert poAtt;
    }
}