@isTest
private class test_RedmineCase {
     
    static testMethod void Test_VF_FRCase() {
  
         //instance of object creator
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        Account acc = obj.createAccount();
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        Case cs = obj.Create_case(acc, con);
        insert cs;
        
        RMProject__c rmp = obj.createRMProject(cs);
        insert rmp;
        
        RMProject__c rmp1 = obj.createRMProject(cs); 
        rmp1.RM_Id__c = '76'; 
        rmp1.SF_FR_Sync__c = true;                                              
        insert rmp1;
        
        RmAssignee__c  rmAss = obj.createRMAssigne();
        rmAss.RM_Project_Id__c = rmp1.RM_Id__c;
        rmAss.RM_Id__c ='5';
        rmAss.name ='aaabb';                                                                   
        insert rmAss; 
        
        RMUsers__c rmUsr = obj.createRMUser();
        insert rmUsr;
        
        RMPriority__c pr = obj.createPriority();
        insert pr;
       
        RMTracker__c trkr = obj.createRMTracker();
        trkr.RM_Project_Id__c =  rmp1.RM_Id__c;
        trkr.RM_Id__c = '1';                     
        insert trkr;
       
        Test.setCurrentPageReference(new PageReference('Page.VF_FR_Csse')); 
        System.currentPageReference().getParameters().put('caseId',cs.Id ); 
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(cs);
        VF_FR_Csse vf = new VF_FR_Csse(teststandard);
       
        vf.ProjectSelected = '901';
        vf.getProjectsListProcess();
        vf.SubProjectSelected = '909';
        vf.getAssigneBsedOnProjAndSubProj();
        vf.getTracker();
        
        vf.getPriority();
        
        vf.SaveRMCase();
        
        RMCase__c newFRCase = new RMCase__c();
     
        newFRCase.sfcase__c = cs.Id;
        newFRCase.RMAssignee__c = rmAss.RM_Id__c;
        newFRCase.RMProject__c = rmp1.RM_Id__c;
        newFRCase.RMTracker__c = trkr.RM_Id__c;
        newFRCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: 'RM_feature'].Id;
        insert newFRCase;
         
        Test.setCurrentPageReference(new PageReference('Page.VF_FR_Csse')); 
        System.currentPageReference().getParameters().put('Id',newFRCase.Id );
        Apexpages.Standardcontroller teststandard1 = new Apexpages.Standardcontroller(cs);   
        VF_FR_Csse vf1 = new VF_FR_Csse(teststandard);
        
        RmAssignee__c  rmAss1 = obj.createRMAssigne();
        rmAss1.RM_Project_Id__c = rmp1.RM_Id__c;
        rmAss1.RM_Id__c = '123';
        insert rmAss1; 
        
        vf1.assigneSelected();
        vf1.TrackerSelectedFunc();
        vf1.PrioritySelectedFunc();
        vf1.CloseRMCaseUpdate();
        vf1.UnMoveStateToAssigned();
        vf1.SaveRMCaseUpdate();
        vf1.selectclick();   
        vf1.MoveStateToAssigned();
        vf1.unselectclick();
        vf1.getunSelectedValues();
        vf1.getSelectedValues();
        vf1.save();
        
        list<SelectOption> selectOptions_List = new list<SelectOption>();
        selectOptions_List.add(new SelectOption('--None--','--None--'));
        VF_FR_Csse.SortOptionList(selectOptions_List);
        
        selectOptions_List.add(new SelectOption('12','12'));
        selectOptions_List.add(new SelectOption('13','13'));
        VF_FR_Csse.SortOptionList(selectOptions_List);
        RMCase__c rmcUpd = [select Id, RMAssignee__c from RMCase__c where ID =:newFRCase.Id ];
        rmcUpd.RMAssignee__c = rmAss1.RM_Id__c;
        
        update rmcUpd;  
    }
        
        
        static testMethod void Test_VF_RMCase() {
  
             //instance of object creator
            CLS_ObjectCreator obj = new CLS_ObjectCreator();
            Account acc = obj.createAccount();
            insert acc;
            
            Contact con = obj.CreateContact(acc);
            insert con;
            
            Case cs = obj.Create_case(acc, con);
            insert cs;
            
            RMProject__c rmp = obj.createRMProject(cs);
            insert rmp;
            
            RMProject__c rmp1 = obj.createRMProject(cs); 
            rmp1.RM_Id__c = '76'; 
            rmp1.SF_FR_Sync__c = true;                                              
            insert rmp1;
            
            RmAssignee__c  rmAss = obj.createRMAssigne();
            rmAss.RM_Project_Id__c = rmp1.RM_Id__c;
            rmAss.RM_Id__c ='5';
            rmAss.name ='aaabb';                                                                   
            insert rmAss; 
            
            RMUsers__c rmUsr = obj.createRMUser();
            insert rmUsr;
            
            RMPriority__c pr = obj.createPriority();
            insert pr;
           
            RMTracker__c trkr = obj.createRMTracker();
            trkr.RM_Project_Id__c =  rmp1.RM_Id__c;
            trkr.RM_Id__c = '1';                     
            insert trkr;
           
            Test.setCurrentPageReference(new PageReference('Page.VF_RedMineCase')); 
            System.currentPageReference().getParameters().put('caseId',cs.Id ); 
            Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(cs);
            VF_RedMineCase vf = new VF_RedMineCase(teststandard);
           
            vf.ProjectSelected = '901';
            vf.SubProjectSelected = '909';
            vf.getAssigneBsedOnProjAndSubProj();
            vf.getTracker();
            
            vf.getPriority();
            
            vf.SaveRMCase();
            
            RMCase__c newFRCase = new RMCase__c();
         
            newFRCase.sfcase__c = cs.Id;
            newFRCase.RMAssignee__c = rmAss.RM_Id__c;
            newFRCase.RMProject__c = rmp1.RM_Id__c;
            newFRCase.RMTracker__c = trkr.RM_Id__c;
            newFRCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: 'RM_case'].Id;
            insert newFRCase;
            
            Test.setCurrentPageReference(new PageReference('Page.VF_RedMineCase')); 
            System.currentPageReference().getParameters().put('Id',newFRCase.Id );
            Apexpages.Standardcontroller teststandard1 = new Apexpages.Standardcontroller(cs);   
            VF_RedMineCase vf1 = new VF_RedMineCase(teststandard);
            
            RmAssignee__c  rmAss1 = obj.createRMAssigne();
            rmAss1.RM_Project_Id__c = rmp1.RM_Id__c;
            rmAss1.RM_Id__c = '123';
            insert rmAss1; 
            
            vf1.assigneSelected();
            vf1.TrackerSelectedFunc();
            vf1.PrioritySelectedFunc();
            vf1.CloseRMCaseUpdate();
            vf1.UnMoveStateToAssigned();
            vf1.SaveRMCaseUpdate();
            vf1.selectclick();   
            vf1.MoveStateToAssigned();
            vf1.unselectclick();
            vf1.getunSelectedValues();
            vf1.getSelectedValues();
            vf1.save();
            
            list<SelectOption> selectOptions_List = new list<SelectOption>();
            selectOptions_List.add(new SelectOption('--None--','--None--'));
            VF_RedMineCase.SortOptionList(selectOptions_List);
            
            selectOptions_List.add(new SelectOption('12','12'));
            selectOptions_List.add(new SelectOption('13','13'));
            VF_RedMineCase.SortOptionList(selectOptions_List);
            RMCase__c rmcUpd = [select Id, RMAssignee__c from RMCase__c where ID =:newFRCase.Id ];
            rmcUpd.RMAssignee__c = rmAss1.RM_Id__c;
            
            update rmcUpd;  
        }
    }