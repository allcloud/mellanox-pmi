/********************************************************************
 * Controller class for mlnxMobileCaseView VF page on mobile devices
 * 
 * @Author: Abdul Sattar (Magnet 360)
 * @Date:  08.28.2015
 *
 * @Updates:
 * 08.31.2015   Abdul Sattar (Magnet 360)
 *              Added logic required for ordered list of case comments.
 *
 * 09.01.2015   Abdul Sattar (Magnet 360)
 *              Added more fields for Case Comments
 *
 * 09.10.2015   Sophia Murphy (Magnet 360)
 *              Added method for closing case
 * 
 ********************************************************************/
public with sharing class mlnxMobileCaseViewCtrl {
    private ApexPages.StandardController sc;        // Reference to standard controller
    private Boolean isCommunity;                    // In Community flag
    
    public Case currentCase {get; set;}             // Reference to current case
    public CaseComment newComment {get; set;}       // Reference to case comment
    public List<CaseComment> comments {get; set;}   // List of comments on Case
    public boolean hasAttachments {get;set;}
	public Boolean showFullDescription {get;set;}

    // Constructor - Standard Controller
    public mlnxMobileCaseViewCtrl(ApexPages.StandardController sc){
        try {
            // Initialize reference to standard controller
            this.sc = sc;   

            // Initialize current case with current SC record
            currentCase = (Case)sc.getRecord();

            // Get community status
            isCommunity = ( Network.getNetworkId() != null );

            // Initialize list of comments
            comments = selectComments();

            // Initialize a new comment
            newComment = new CaseComment();

        } catch (Exception e) {
            System.debug('General VF Exception: ' + e.getMessage());
        }
        
        hasAttachments = false;
        showFullDescription = false;
        
        list<Attachment> relatedAttList = [SELECT Id FROM Attachment WHERE ParentId =: currentCase.Id];
        
        if (!relatedAttList.isEmpty()) {
          
          hasAttachments = true;
        }
    }

    // Closes case by setting the Status and the State
    public void closeCase() {
        
            PageReference currentPage = ApexPages.currentPage();
            currentCase = (Case) sc.getRecord();    // Get current case from SC record

            currentCase.State__c = 'Closed';
            currentCase.Status = 'Closed';
          try {    
              update currentCase;
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'The case is closed'));
          }
          
            // return currentPage;
         catch (Exception e) {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The case cannot be closed'));
        }
       // return null;
    }
    
    // Saves comment to database
    public PageReference saveComment() {
      try {
            PageReference currentPage = ApexPages.currentPage();
          	currentCase = (Case) sc.getRecord();    // Get current case from SC record

          // Save Case Comment to database, if there is any comment added by user
          if(newComment != null && currentCase.Id != null) {
              newComment.ParentId = currentCase.Id;
              if (!String.isBlank(newComment.CommentBody)) {
                  INSERT newComment;
                  
                  newComment = new CaseComment();    // Re-initialize for next comment.
              }
          }

            // Navigate to Case View or My Cases page
            if (currentPage != null) {
                PageReference targetPage = new PageReference( buildTargetURL() );
                return targetPage;
            }
            return currentPage;
        } catch (Exception e) {
            System.debug('General VF Exception on saveComment: ' + e.getMessage());
        }
        return null;
    }

    // Select comments order by creation date from database
    public List<CaseComment> selectComments() {
        List<CaseComment> ccmnts;
        try {
            currentCase = (Case) sc.getRecord();    // Get current case from SC record

            // Retrieve comments from database.
            if(currentCase.Id != null) {
               ccmnts = [SELECT Id, CommentBody, CreatedDate, CreatedBy.Id, CreatedBy.Name  FROM CaseComment WHERE ParentId = :currentCase.Id ORDER BY CreatedDate DESC];
            }

        } catch (Exception e) {
            System.debug('General VF Exception on saveComment: ' + e.getMessage());
        }

        return ccmnts;
    }

    // Prepare target URL
    private String buildTargetURL () {
        // Construct URL of view page
        String targetURL;

        targetURL = ( isCommunity ? Site.getPathPrefix() + '/' : '/' );

        if(currentCase.Id == null) {
            targetURL = targetURL + 'apex/mlnxMobileCases';
        } else {
            targetURL = targetURL + 'apex/mlnxMobileCaseView?Id=' + currentCase.Id;
        }

        return targetURL;
    }
    public void showDescription() {
    	showFullDescription = true;
    }
   public void hideDescription() {
    	showFullDescription = false;
    }
    
}