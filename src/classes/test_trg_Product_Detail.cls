@isTest(seeAllData=True)
public with sharing class test_trg_Product_Detail {
	
	static testMethod void test_updateRelatedProductsImageURL() {
		
		ProductDetails__c testProductDetail = [SELECT Id FROM ProductDetails__c WHERE isDeleted = false limit 1];
		
		Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
        Pr1.Product_Detail__c = testProductDetail.Id;
        insert Pr1;
         
		Test.StartTest();
		
		testProductDetail.StaticResourceImage__c = 'test';
		update testProductDetail;
		
		Test.StopTest();
	}
}