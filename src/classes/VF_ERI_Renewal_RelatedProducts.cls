global class VF_ERI_Renewal_RelatedProducts {
	
	public String contractId {get;set;}
	public Contract2__c theContract {get;set;}
	public list<QuoteLineItem> relatedProducts_List {get;set;}
	
	public void getAllParams() {
		
		Opportunity relatedOpp = [SELECT Id, (SELECT Id FROM Quotes) FROM Opportunity WHERE Old_Contract__c =: contractId order by createdDate desc limit 1];
		
		Id quoteId = relatedOpp.Quotes[0].Id;
		
		relatedProducts_List = [SELECT Id, Product2.Name, Product2.Part_Number__c,  Quantity, UnitPrice, Covered_Part_Number__c, Serial_Numbers__c, TotalPrice   
								FROM QuoteLineItem WHERE QuoteId =: quoteId];
													
	}
}