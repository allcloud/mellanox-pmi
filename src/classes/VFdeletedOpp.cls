public class VFdeletedOpp
{
    //public List <Opportunity> lst_selectedOpps = new List <Opportunity>();
    private id id_opportunity;
    public Opportunity currOpp;
    private List<ID> lst_OppId = new List <ID>();
    ApexPages.StandardController controller;
    private String param; 
    public VFdeletedOpp() 
    {
        Id OppId = Apexpages.currentPage().getParameters().get('Id');
        currOpp = [Select Id From Opportunity Where Id =: OppId and IsDeleted = false];
    }
    
    public Pagereference AutoRun()
    {
        delete currOpp;
        return new Pagereference('/006');
    }
    
    public static testMethod void Testeo()
    {   
        US_Territory_Map__c tm = new US_Territory_Map__c( SD_Region__c = 'EMEA', Region__c = '0000'); 
        tm.Sales_Director__c = '00550000000wuVp';
        insert tm;

      
        Opportunity Op = new Opportunity(ownerid = '00550000000wuVp', Name = 'test opp', StageName = 'Discovery', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
        insert op;
        Apexpages.currentPage().getParameters().put('Id',op.Id);
        VFdeletedOpp cont = new VFdeletedOpp();
        cont.AutoRun();
    }
}