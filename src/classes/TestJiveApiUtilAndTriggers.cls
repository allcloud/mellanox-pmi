@isTest(OnInstall=false)

private class TestJiveApiUtilAndTriggers {
    
    private static JiveCommunitySettings__c  jcs;

   
    public static testMethod void test() {
        // make sure custom setting record exists
        jcs=JiveCommunitySettings__c.getValues('DefaultSetting');
        if(jcs == NULL) {
            jcs = new JiveCommunitySettings__c();
            jcs.Name = 'DefaultSetting';
            //change this add  line for jive url
            jcs.Community_Base_URL__c='https://ec2-54-201-62-239.us-west-2.compute.amazonaws.com:8090'; 
            jcs.Create_Contact__c=true;
            insert jcs;
            }
        else {
           //change this and add  line for jive url
            jcs.Community_Base_URL__c='https://ec2-54-201-62-239.us-west-2.compute.amazonaws.com:8090'; 
            jcs.Create_Contact__c=true;
            update jcs;
            }
            
       // testTriggers();
        testJiveApiUtil();
        }
        
        public static void testJiveApiUtil(){
        
           String filterText='searchText';
           String filterDate='2014-07-02';
           String filterAuthor='Persistent';
           Boolean grp=true;
           Boolean blog=true;
           Boolean document=true;
           Boolean space=true;
           String typeOfDiss='discussion';
           //change this with more appropriate dummy request body
           String jsonRequestBodyForJiveDocSync='jsonRequestBody';
           String jiveURL='https://cloudsquads-jivedemo.jiveon.com';
           String contentType='space';
           String contentId='12334';
            JiveAPIUtil.getJiveCustomSettings();
            JiveAPIUtil.getLoggerSettings();
            JiveAPIUtil.retrieveCaseThreads(contentId);
            JiveAPIUtil.getContentsByType('space');
            JiveAPIUtil.sendContentFilters(filterText,filterDate,filterAuthor,grp,blog,document,space,typeOfDiss);
            JiveAPIUtil.ContentsByTypeResponseWrapper responseWrapper = new JiveAPIUtil.ContentsByTypeResponseWrapper('id','name','url','contenttype');
            //String id,String name,String url,String contentType
            JiveAPIUtil.ContentsByTypeRequestWrapper requestWrapper = new JiveAPIUtil.ContentsByTypeRequestWrapper(jiveURL,contentType);
            JiveContentFilterWrapper filterwrapper=new JiveContentFilterWrapper(filterText,filterDate,filterAuthor,jiveURL,grp,blog,document,space,typeOfDiss);
            JiveContentIdWrapper contentIdWrapper=new JiveContentIdWrapper(jiveURL,contentId);
            Case c1 = new Case(subject='testCase');
            insert c1;
            Logger__c postbackLogger = new Logger__c(FeatureType__c = 'Community Postback - Postback Failure',IsErrorLog__c=true,Status__c = 'Re-Attempted',RetryCount__c=1,SFObject__c = 'Case',JiveContentType__c = 'Question',SFID__c = c1.id,JiveId__c='3455');
            insert postbackLogger;
            JiveAPIUtil.upsertLoggerForCommunityPostback(postbackLogger, false, true, c1.Id, '2344', 'testPostback', true);
            
            
            String loggerAPIResponse = '{"statusCode":400,"headers":{"server":"Apache-Coyote/1.1","x-jive-request-id":"9d251760-0bd9-11e4-8439-0050568b1114","x-jive-flow-id":"9d251761-0bd9-11e4-8439-0050568b1114","x-frame-options":"SAMEORIGIN","p3p":"CP=\"CAO PSA OUR\"","x-jive-user-id":"2156","expires":"0","pragma":"no-cache","content-type":"application/json","cache-control":"no-store","x-jsl":"D=114641 t=1405398999980737","x-cnection":"close","date":"Tue, 15 Jul 2014 04:36:40 GMT","transfer-encoding":"chunked","connection":"close, Transfer-Encoding","set-cookie":["jive.login.ts=1405399000022; Path=/; Secure;HttpOnly","BIGipServerpool_jivedemo-cloudsquads.jiveon.com=1355850506.20480.0000; path=/"]},"entity":{"error":{"status":400,"message":"Another message is already marked as the answer to this discussion."}}}';
            JiveAPIUtil.sendPostbackMessage('http://www.dummyurl.com', new Map<String,Boolean>{postbackLogger.SFID__c=>true}, new Map<String,String>{postbackLogger.SFID__c=>'12345'}, new Map<String,String>{postbackLogger.SFID__c=>'replyComments'}, new Set<String>{postbackLogger.SFID__c}, postbackLogger.id,null,null,null,UserInfo.getUserEmail(),UserInfo.getName());
          
        }
        
        
        public static void testTriggers(){
        Case c=new Case(Jive_Author_Name__c='Pranjal',Jive_Author_Email__c='pg@cloudsquads.com',Status='New',Origin='Jive',Subject='Escalated case');

       // Insert Case
        insert c;
        Contact cc=[Select Email from Contact where Email='pg@cloudsquads.com' limit 1];
        System.assertEquals('pg@cloudsquads.com', cc.Email);
        Case c1=new Case(Jive_Author_Name__c='Pranjal',Jive_Author_Email__c='pg@cloudsquads.com',Status='New',Origin='Jive',Subject='Escalated case');
        // Insert Case
        insert c1;
    
        //check if new contact is created or already exists, in both cases Contact c should be populated
       
        Contact cc1=[Select Email from Contact where Email='pg@cloudsquads.com' limit 1];
        jcs.Create_Contact__c=false; 
        upsert cc1;
       
        System.assertEquals('pg@cloudsquads.com', cc.Email);
       
        //now check update trigger..
        //this will call the Jive-Api class
       
       
        c1.Status ='Closed';
        update(c1);
        
        Case c1_2 = [SELECT Status FROM Case WHERE Id = :c1.Id];

        System.assertEquals('Closed', c1_2.Status );
        
        //---------------------------------------------------------------------------
        
        jcs.Create_Contact__c=false;
        update jcs;
        jcs=Null;
        Contact cn2=new Contact(LastName='Ratnesh',Email='rk@cloudsquads.com');
        insert cn2;
        Case c2=new Case(Jive_Author_Name__c='Ratnesh',Jive_Author_Email__c='rk@cloudsquads.com',Status='New',Origin='Jive',Subject='To Escalated case');
        insert c2;
        //---------------------------------
        //update case
        //Post case closure comments as correct answer
        //c2.Postback_Options__c='Post case closure comments as correct answer';
        update c2;
        CaseComment newComment = new CaseComment();
        newComment.CommentBody ='some internal comment';
        newComment.ParentId =c2.id;
        insert newComment;
        c2.Status='Closed'; 
        update c2;
        c1.origin='Email';
        update c2;
}
}