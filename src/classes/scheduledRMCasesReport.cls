global class scheduledRMCasesReport implements Schedulable{ 
 global void execute(SchedulableContext SC) {
 
 list<RMcase__c> OpenRMCases = new list<RMcase__c>();
 list<Case_Monitor__c> CM_toInsert = new List<Case_monitor__c>();
 integer CaseCount = 0;
 integer AECaseCount = 0;
 set<string> Buckets = new set<string>();
 map<string,integer> BucketsToCases = new  map<string,integer>();
 map<string,integer> RMAEToCases = new  map<string,integer>();
 
 OpenRMCases = [select id, Case_AE_PM_Escalated__c, Tracker__c ,project_second_bucket__c,RM_State__c 
                from RMCase__c 
                where RM_state__c != 'Closed' ];  

for(RMcase__c Cs:OpenRMCases)
   { 
      if ((Cs.RM_State__c == 'Assigned') && (Cs.Tracker__c.contains('Bug') || Cs.Tracker__c.contains('bug')))
       {  
          if((Cs.project_second_bucket__c != NULL) && (!BucketsToCases.containsKey(Cs.project_second_bucket__c)))
          { BucketsToCases.put(Cs.project_second_bucket__c.trim(),1);}
          else
           {
           if(Cs.project_second_bucket__c != NULL)
            {
             CaseCount = BucketsToCases.get(Cs.project_second_bucket__c.trim());
             BucketsToCases.put(Cs.project_second_bucket__c.trim(),CaseCount+1);
             }
           }
       }
      
      if ((Cs.RM_State__c == 'Assigned') && (CS.Case_AE_PM_Escalated__c == 'AE') && (Cs.Tracker__c.contains('Bug') || Cs.Tracker__c.contains('bug')))
       {  
       
          if((Cs.project_second_bucket__c != NULL) && (!RMAEToCases.containsKey(Cs.project_second_bucket__c)))
          { RMAEToCases.put(Cs.project_second_bucket__c.trim(),1);}
          else
           {
           if(Cs.project_second_bucket__c != NULL)
           {
            AECaseCount = RMAEToCases.get(Cs.project_second_bucket__c.trim());
            RMAEToCases.put(Cs.project_second_bucket__c.trim(),AECaseCount +1);
           }
           }
       }           
  }

 for(string Bkts:BucketsToCases.keySet())
 
 {
  Case_Monitor__c CM = new Case_monitor__c();
  CM.RM_Top_Project__c = Bkts;
  CM.Case_Count__c = BucketsToCases.get(Bkts);
  CM.Date_of_Sample__c = system.Today(); 
  CM_toInsert.add(CM);
  }
  
 for(string Bkts:RMAEToCases.keySet())
 
 {
  Case_Monitor__c CM = new Case_monitor__c();
  CM.RM_Top_Project__c = Bkts;
  CM.AE_Escalated_Cases__c = RMAEToCases.get(Bkts);
  CM.Date_of_Sample__c = system.Today(); 
  CM_toInsert.add(CM);
  }
  
insert CM_toInsert;

  }
}