public with sharing class cls_trg_Task {
    
    // This method will update the newly created Task's related Lead's Status To 'Accepted Working'
    // If the Lead Status equals 'Sales Accepted' 
    public void updateTasksRelatedLeads(list<Task> newTasks_List) {
        
        map<Id, Lead> leadsIds2Leads_Map = getMapOfLeads(newTasks_List);
        list<Lead> leads2Update_List = new list<Lead>();
        
        for (Task newTask : newTasks_List) {
            
            if (newTask.WhoId != null) {
                    
                if (leadsIds2Leads_Map != null && leadsIds2Leads_Map.containsKey(newTask.WhoId)) {
                    
                    Lead relatedLead  = leadsIds2Leads_Map.get(newTask.WhoId);
                    relatedLead.Status = 'Accepted Working';
                    leads2Update_List.add(relatedLead);
                }
            }
        }
        
        if (!leads2Update_List.isEmpty()) {
            
            try {
                
                update leads2Update_List;
            }
            
            catch(Exception e) {
                
                if (newTasks_List.size() == 1) {
                    
                    newTasks_List[0].addError('Cannot update related Lead : ' + e.getMessage());
                }
            }
        }
    }
    
    // This method returns a map of related leads Ids as keys and Leads as their values
    private map<Id, Lead> getMapOfLeads(list<Task> newTasks_List) {
        
        map<Id, Lead> leadsIds2Leads_Map;
        set<Id> leadsIds_Set = new set<Id>();
        
        for (Task newTask : newTasks_List) {
            
            if (newTask.WhoId != null && String.valueOf(newTask.WhoId).StartsWith('00Q')) {
                
                leadsIds_Set.add(newTask.WhoId);
            }
        }
        
        if (!leadsIds_Set.isEmpty()) {
            
            leadsIds2Leads_Map= new Map<Id, Lead> ([SELECT Id, Status FROM Lead WHERE ID IN : leadsIds_Set
                                                    and (Status =: 'Sales Accepted' OR Status =: 'Marketing Qualified') ]);
        }
        
        return leadsIds2Leads_Map;
    }
    
}