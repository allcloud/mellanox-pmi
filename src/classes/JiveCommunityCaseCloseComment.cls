/*****************************************************************************************************************
 ****** 
 ****** Author : Persistent                                                                                                         
 ****** Description : Custom Controller class ,used in CommunityCaseClosedCommentPage   
 ****** Last Modified By : Swapnil Kulkarni 
 *****************************************************************************************************************/

public with sharing class JiveCommunityCaseCloseComment {

    public Community_Case_Closure_Comments__c community{get;set;}
    public Case c;
    public ApexPages.StandardController controller = null;
    String parentID,contentID;
    Boolean isReplyToMainThread,isAllowedToPost;
    public Thread_Details__c td{get;set;}
    Boolean isAttachmentInserted;
    String attachmentID,attachmentName;
    String[] attachedFiles = new String[]{};
    public JiveCommunityCaseCloseComment(ApexPages.StandardController controller) {
        isAllowedToPost =true;
        isReplyToMainThread=false;
        isAttachmentInserted=false;
        community= new Community_Case_Closure_Comments__c();
        parentID = ApexPages.currentPage().getParameters().get('msgID');
        contentID= ApexPages.currentPage().getParameters().get('contenID');
        isReplyToMainThread = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isReplyToMainThread'));
        c = [Select id,Content_ID__c,CaseNumber from Case where Content_ID__c =: contentID];
    }
    
    
     /**
    * @Author- Persistent
    * @Description-This method is used to get data from standard object for postback reply  
    */ 
    
    public void SetCommunityData(){
        //getting data from case
        try{
           
            if(Test.isRunningTest())
            {
                community.Postback_Options__c = 'Post comments as reply';
                community.Postback_Comment__c = 'TEst';
            }
            community.Case__c = c.Id;
            community.Content_Id__c = c.Content_ID__c;
            community.parentId__c =parentID; 
            community.isReplyToMainThread__c = isReplyToMainThread;
            community.AttachmentID__c = attachmentID;
            String temp ='';
            if(community.Postback_Comment__c!=null)
            temp = community.Postback_Comment__c.replaceAll('<[^>]+>',' ');
            if(temp.length()>80)
            community.Thread_Title__c = temp.subString(0,80);
            else
            community.Thread_Title__c = temp;
           
            System.debug('==>Content_ID of Case '+community.Case__c); 
            System.debug('==>Content_ID of Case '+c.Content_ID__c );
            System.debug('Postback Comment '+community.Postback_Comment__c  );
        }catch(Exception e){
            String error = e.getMessage();
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        
    
    }

/**
    * @Author- Persistent
    * @Description- This method is used to save record in custom object in SFDC and Sent reply to Jive
    */
    public PageReference saveObject(){
    PageReference closePage = new PageReference('/apex/closePage');
    closePage.setRedirect(true);
        
              try{
             isAllowedToPost = true;     
             SetCommunityData();
             sendPostbackToJIVE();
            //checking for fields null or not
            if(community.Postback_Options__c == null || community.Postback_Comment__c==null ){
                  System.debug('=====>In if block');
                  return null;
            }else{
                if(isAllowedToPost)
                {
                    
                        community.AttachmentID__c = attachmentID;
                        community.AttachmentName__c = attachmentName;
                                     
                 //inserting data 
                 Database.SaveResult[] results = Database.insert(new Community_Case_Closure_Comments__c [] {
                    community });
                   System.debug('=====>'+community);
                  for (Integer i = 0; i < results.size(); i++) {
                    if (results[i].isSuccess()) {
                    
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Reply Sent To Jive');
                  
                    ApexPages.addMessage(msg);
                    
                    return closePage;
                }else{
                    
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.error,'Could Not Insert Case Closure Comments.');
                     
                    ApexPages.addMessage(msg);
                    
                    return null;
                }
                }

                   community =  new Community_Case_Closure_Comments__c();
               }
                return null;
            }
            
           }catch(Exception e){
                String error = e.getMessage();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
                return null;
            }
            
      
    }
    
    public void showThreadDesc()
    {        
        td = [select id,MessageID__c,Description__c,Thread_Title__c,ThreadAttachmentDetails__c from Thread_Details__c where MessageID__c =:parentID order by createddate desc limit 1];        
    }
    
    public void sendPostbackToJIVE()
    {       
        if(community.Postback_Options__c!=NULL && community.Postback_Comment__c!=NULL)
        {
            if(attachment.Name!=null)
            {
                upload();                    
            }            
            else
            {
               postReply();    
            }           
        }
       
    }
    
    public void postReply()
    {
        Set<String> set_CaseId = new Set<String>();
            Set<String> set_caseComments = new Set<String>();
            Map<String,Boolean>  map_optionReply_caseId= new Map<String,Boolean>();
            Map<String,String>  map_contentId_caseId= new Map<String,String>();
            Map<String,String>  map_caseComment_caseId= new Map<String,String>();
            Boolean isAnswer=false;
            JiveCommunitySettings__c jcs = JiveCommunitySettings__c.getValues('DefaultSetting');
         
            if(jcs==NULL) {
                jcs=new JiveCommunitySettings__c
                (  
                    Name ='DefaultSetting' ,
                    Community_Base_URL__c ='https://ec2.compute.amazonaws.com:8090',
                    Create_Contact__c     = false,
                    Jive_URL__c           = 'https://jive-community.jiveon.com',
                    Email_As_Author__c = false,
                    Both_UserName_And_Email_As_Author__c = false,
                    Name_As_Author__c = false
                ); 
            }
            set_CaseId.add(c.Id);
            if(community.Postback_Options__c=='Post comments as correct answer'){
                isAnswer=true;
            }
            
            map_optionReply_caseId.put(c.Id,isAnswer);
            map_contentId_caseId.put(c.Id,community.Content_ID__c);
            
            String ParentID;
            boolean isReplyToMainThread;
            
            
            System.debug('BEFORE MAP :'+ community.Postback_Comment__c);
            
            
            map_caseComment_caseId.put(c.id,community.Postback_Comment__c);
            
            System.debug('AFTER MAP :'+ map_caseComment_caseId.get(c.id));
            
            ParentID=community.parentId__c ;
            isReplyToMainThread = community.isReplyToMainThread__c; 
            
            String jiveUrl=jcs.Jive_URL__c;
            String UserEmail;
            String UserName;
            Boolean UEmail = jcs.Email_As_Author__c;
            Boolean UName = jcs.Name_As_Author__c;
            Boolean BothNameAndEmail = jcs.Both_UserName_And_Email_As_Author__c;
            
            if(UEmail==true && UName==true || BothNameAndEmail ==true){
                UserEmail = UserInfo.getUserEmail();
                UserName = UserInfo.getName();
            }else if(UEmail==true){
                UserEmail = UserInfo.getUserEmail();
            }else if(UName==true){
                UserName = UserInfo.getName();
            }else{
                UserName = null;
                UserEmail = null;
            }
            transient List<Attachment> att = [SELECT ContentType,Id,Name,ParentId,BodyLength FROM Attachment where id=:attachment.id order by createddate desc limit 1];
            
            if(!isReplyToMainThread)
            {
                if(att.size()==1)
                {        
                    if(att[0].BodyLength <2097152)
                    {
                        JiveAPIUtil.sendPostbackMessage(jiveUrl,map_optionReply_caseId,map_contentId_caseId,map_caseComment_caseId,set_CaseId,ParentID,null,att[0].id,att[0].Name,UserEmail,UserName);
                    }
                    else
                    {
                        isAllowedToPost=false;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Upload Attachment Lesser Than 2MB Size'));
                        deleteAttachment(att[0]);                       
                    }
                }
                else
                    JiveAPIUtil.sendPostbackMessage(jiveUrl,map_optionReply_caseId,map_contentId_caseId,map_caseComment_caseId,set_CaseId,ParentID,null,null,null,UserEmail,UserName);
            }
            else
            {
                if(att.size()==1) 
                {
                    if(att[0].BodyLength <2097152)
                    {
                        JiveAPIUtil.sendPostbackMessage(jiveUrl,map_optionReply_caseId,map_contentId_caseId,map_caseComment_caseId,set_CaseId,null,null,att[0].id,att[0].Name,UserEmail,UserName);    
                    }
                    else
                    {
                        isAllowedToPost=false;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Upload Attachment Lesser Than 2MB Size'));
                        deleteAttachment(att[0]);
                    }
                }
                else
                    JiveAPIUtil.sendPostbackMessage(jiveUrl,map_optionReply_caseId,map_contentId_caseId,map_caseComment_caseId,set_CaseId,null,null,null,null,UserEmail,UserName);    
            }

    }
    
   
   public transient Attachment attachmentHelper;
    
   public Attachment attachment {
   get {
      if (attachmentHelper == null)
        attachmentHelper = new Attachment();
      return attachmentHelper;
    }
   set;
   }
   
    
    public void deleteAttachment(Attachment att)
    {
        try
        {
            delete(att);
        }
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Could Not Upload Attachment.'));
        }
    }
    
   
     public PageReference upload() {

    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = c.id; // the record the file is attached to
    attachment.IsPrivate = true;

    try {
      insert attachment;
      attachmentID =attachment.id;
      attachmentName=attachment.Name ;
      isAttachmentInserted = true; 
      postReply();  
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error on uploading attachment'));
      return null;
    } finally {
      attachment = new Attachment(); 
    }
    if(isAllowedToPost)
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,' Attachment Uploaded'));
    return null;
  }

 
    
    
  
}