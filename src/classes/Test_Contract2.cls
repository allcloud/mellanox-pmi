@isTest
private class Test_Contract2 {

    static testMethod void update_contract_on_rollUp() {
        
        Account Acc1 = new Account(name = 'testAccount',support_center__c = 'Global');
        Insert Acc1; 
        
        Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
        Insert Pr1;
        
        Contact Contact1 = new Contact(lastname = 'aaa', firstname ='bbb',AccountId= Acc1.Id);
        insert Contact1;
               
        Contract2__c Cont1 = new Contract2__c(Account__c = Acc1.id,Contract_Term_months__c =2);
        Cont1.Contract_Renew_RollUp_to__c = NULL; 

        insert Cont1;
        
        Contract2__c Cont2 = new Contract2__c(Account__c = Acc1.id, Contract_Term_months__c =2, EndDate__c=System.today()+1000);
        insert Cont2;

       Asset Ast1 = new Asset(name = 'testAsset', AccountId =Acc1.Id, ContactId = Contact1.Id, product2=Pr1);
       Ast1.Contract2__c = Cont1.id;
       Ast1.Asset_Type__c = 'OFED';
       Insert Ast1;

        
        Cont1.Contract_Renew_RollUp_to__c = Cont2.id;
        cont1.EndDate__c = System.today()-50;
        cont1.contract_start_date__c = System.today()-90;

        update Cont1;
        
  }       
      static testMethod void update_contract_on_rollUp2() {
        
        Account Acc1 = new Account(name = 'testAccount',support_center__c = 'Global');
        Insert Acc1; 
        
        Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
        Insert Pr1;
        
        Contact Contact1 = new Contact(lastname = 'aaa', firstname ='bbb',AccountId= Acc1.Id);
        insert Contact1;
               
        Contract2__c Cont1 = new Contract2__c(Account__c = Acc1.id,Contract_Term_months__c =2);
        Cont1.Contract_Renew_RollUp_to__c = NULL; 

        insert Cont1;
        
        Contract2__c Cont2 = new Contract2__c(Account__c = Acc1.id, Contract_Term_months__c =2, EndDate__c=System.today()+1000, contract_start_date__c = System.today()-90);
        insert Cont2;

       Asset Ast1 = new Asset(name = 'testAsset', AccountId =Acc1.Id, ContactId = Contact1.Id, product2=Pr1);
       Ast1.Contract2__c = Cont1.id;
       Ast1.Asset_Type__c = 'OFED';
       Insert Ast1;

        
        Cont1.Contract_Renew_RollUp_to__c = Cont2.id;
        cont1.contract_start_date__c = System.today()-90;
        cont1.EndDate__c = System.today()+100;
        update Cont1;
        
  }       
      static testMethod void update_contract_on_rollUp3() {
        
        Account Acc1 = new Account(name = 'testAccount',support_center__c = 'Global');
        Insert Acc1; 
        
        Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
        Insert Pr1;
        
        Contact Contact1 = new Contact(lastname = 'aaa', firstname ='bbb',AccountId= Acc1.Id);
        insert Contact1;
               
        Contract2__c Cont1 = new Contract2__c(Account__c = Acc1.id,Contract_Term_months__c =2);
        Cont1.Contract_Renew_RollUp_to__c = NULL; 

        insert Cont1;
        
        Contract2__c Cont2 = new Contract2__c(Account__c = Acc1.id, Contract_Term_months__c =2, EndDate__c=System.today()+1000,contract_start_date__c = System.today()-90, Oracle_Contract_Name__c = 'CON12345');
        insert Cont2;

       Asset Ast1 = new Asset(name = 'testAsset', AccountId =Acc1.Id, ContactId = Contact1.Id, product2=Pr1);
       Ast1.Contract2__c = Cont1.id;
       Ast1.Asset_Type__c = 'OFED';
       Insert Ast1;

        cont1.contract_start_date__c = System.today()-90;
        Cont1.Contract_Renew_RollUp_to__c = Cont2.id;
        cont1.EndDate__c = System.today()-50;
        cont1.Addtional_Extended_Warramty_Years__c=5;
        cont1.Renewed_Contract_text__c = 'CON12345';
        update Cont1;
        
  }       
       
}