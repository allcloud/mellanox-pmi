/*****************************************************************************************************************
 ****** 
 ****** Author : Persistent                                                                                                         
 ****** Description : Controller Class for inline VF page on Case to show replies from JIVE in hirarchy
 ****** Last Modified By : Swapnil Kulkarni
 *****************************************************************************************************************/
public class JiveViewCtrl1
{
    public Case aCase {get; private set;}
    private final Case cse;
    String caseId ;
    public String jsonstr{get;set;}
    public String jsonResponse;
    public String treeJson{get;set;}
    public String descJson{get;set;}
    public Map<String, String> mapJsonObjects;
    public Map<String, String> mapJsonTitle;
    public Map<String,List<attachments>>mapAttachmentDetails;
    public List<Thread_Details__c> tdList;        
    
    //Constructor Start
    public JiveViewCtrl1(ApexPages.StandardController controller) 
    {    
        this.cse = (Case)controller.getRecord();
        caseId = cse.Id;          
        aCase=[SELECT Content_ID__c,Description__c,Id,Subject FROM Case WHERE Id =:caseId];
        tdList = new List<Thread_Details__c>();   
        //Get JSON Response from JIVE. JSON has two parts - ListforPlugin and RTF Desc
        try{
        jsonstr = '['+ getJiveThreads() +']';
        
        System.debug('JSON :'+jsonstr);
        getDetails(jsonstr);
        
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Could Not Fetch Discussion Details from Jive'+ e.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }

    public void getDetails(String jsonstr)
    {
        if((jsonstr.indexOf('Application Error')==-1) &&(jsonstr.indexOf('"listForPlugin":[]')==-1) && (jsonstr.indexOf('Discussion Not Found in Jive')==-1))
        {
             
            //WORKING WITHOUT ROOT NODE treeJson = '{"data":['+jsonstr.subString(jsonstr .indexOf('listForPlugin')+16,jsonstr.indexOf('msgList')-3)+']}';
            //To seperate plugin part from entire JSON response and add root node to plugin JSON 
            String temp1 ='{"data":[{ "data":"'+ jsonstr.subString(jsonstr.indexOf('subject')+13,jsonstr.indexOf('author')-2) +',"metadata":{"msgID":'+ jsonstr.subString(jsonstr.indexOf('contentID')+11,jsonstr.indexOf('children')-3)+',"contentID" :'+jsonstr.subString(jsonstr.indexOf('contentID')+11,jsonstr.indexOf('children')-3)+',"mainThreadReply" :"True"'+'}'+', "children": ['+jsonstr.subString(jsonstr .indexOf('listForPlugin')+16,jsonstr.indexOf('msgList')-3)+']}]}';
            String temp2 = temp1.replaceAll('\"data\":\"\"','\"data\":\"Image Only\"');
            String temp3= temp2.replaceAll('&nbsp;',' ');
            //treeJson = '{"data":[{ "data":"'+ jsonstr.subString(jsonstr.indexOf('subject')+13,jsonstr.indexOf('author')-2) +',"metadata":{"msgID":'+ jsonstr.subString(jsonstr.indexOf('contentID')+11,jsonstr.indexOf('children')-3)+',"contentID" :'+jsonstr.subString(jsonstr.indexOf('contentID')+11,jsonstr.indexOf('children')-3)+',"mainThreadReply" :"True"'+'}'+', "children": ['+jsonstr.subString(jsonstr .indexOf('listForPlugin')+16,jsonstr.indexOf('msgList')-3)+']}]}';
            treeJson = temp3;
            System.debug('TREE JSON :'+treeJson);
            //To get Description part of JSON
            descJson = jsonstr.subString(jsonstr.indexOf('msgList')+9,jsonstr.length()-2);
            
            //deserialize to WRAPPER class
            List<descJsonWrapper> descList = (List<descJsonWrapper>)JSON.deserialize(descJson, List<descJsonWrapper>.class);
            System.debug('DESERIALIZED OBJECT :'+ descList);
            mapJsonObjects = new Map<String,String>();
            mapJsonTitle = new Map<String,String>();
            mapAttachmentDetails = new Map<String,List<attachments>>();
            
            for(descJsonWrapper d: descList )
            {
                mapAttachmentDetails.put(d.msgID,d.attachments);
                mapJsonObjects.put(d.msgID,d.description);
                mapJsonTitle.put(d.msgID,d.data);
            }
               
            
            
            //Data for Main THREAD
            Thread_Details__c tmain = new Thread_Details__c();
            String temp = (aCase.Subject).replaceAll('<[^>]+>',' ');
            String temp4 = temp.replaceAll('&nbsp;',' ');
            if(temp4 .length()>80)
            tmain.Thread_Title__c = temp4 .subString(0,80);
            else
            tmain.Thread_Title__c = temp4 ;    
            tmain.CaseID__c = caseId ;
            tmain.MessageID__c = jsonstr.subString(jsonstr.indexOf('contentID')+12,jsonstr.indexOf('children')-4);
            tmain.Description__c =aCase.Description__c;
            tdList.add(tmain);
            
            for(String s:mapJsonObjects.keyset())
            {
                Thread_Details__c t = new Thread_Details__c();
                t.CaseID__c = caseId ;
                t.MessageID__c = s;
                t.Description__c = mapJsonObjects.get(s);
                t.Thread_Title__c = mapJsonTitle.get(s).subString(0, mapJsonTitle.get(s).length());
                for(attachments a:mapAttachmentDetails.get(s) )
                {
                    if(t.ThreadAttachmentDetails__c !=null)
                    //t.ThreadAttachmentDetails__c+=a.name+'\r\n';
                    t.ThreadAttachmentDetails__c+='<li>'+a.name+'</li>';
                    else
                    t.ThreadAttachmentDetails__c='<ul><li>'+a.name+'</li>';
                }
                t.ThreadAttachmentDetails__c+='</ul>';
                tdList.add(t);
            }
        }
        if(jsonstr.contains('Application Error'))
        {
            System.debug('INSIDE APP ERROR');
            //treeJson = null;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Application Error - Can not pull data from Jive');
            ApexPages.addMessage(myMsg);
        }
        if(jsonstr.contains('Discussion Not Found in Jive'))
        {
            //System.debug('INSIDE APP ERROR');
            //treeJson = null;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Could Not Found Discussion On Jive.');
            ApexPages.addMessage(myMsg);
        }

    
    
    
        if(jsonstr.contains('"listForPlugin":[]'))
        {
            treeJson = '{ "data": [ { "data":"'+ aCase.Subject +'", "metadata": { "msgID": "'+ aCase.Content_ID__c+ '", "contentID": "'+aCase.Content_ID__c+'", "mainThreadReply": "True" } } ] }';
            Thread_Details__c tmain = new Thread_Details__c();
            tmain.CaseID__c = caseId ;
            tmain.MessageID__c = aCase.Content_ID__c;
            tmain.Description__c =aCase.Description__c;
            tmain.Thread_Title__c = (aCase.Subject);
            tdList.add(tmain);
        }
    }
    
    public JiveViewCtrl1()
    {
      
    }
    
    public String getJiveThreads()
    {
        String s;
        try
        {        
         system.debug(aCase);
         jsonResponse='';
         if(!Test.isRunningTest()){
        //Calling Addon REST Api to retrieve all replies of discussion in Jive Community.
        //aCase.Content_ID__c is the Content Id of the discussion from which SFDC Case is created   
         HTTPResponse jiveHttpResponse= JiveAPIUtil.retrieveCaseThreads(aCase.Content_ID__c);
         system.debug('jiveHttpResponse--'+jiveHttpResponse);
         if(jiveHttpResponse != null && String.isNotBlank(jiveHttpResponse.getBody())){
            jsonResponse = jiveHttpResponse.getBody();
            system.debug('jsonResponse-'+jsonResponse);
         }
         if(jiveHttpResponse.getStatusCode()==404)
         {
            jsonResponse = 'Discussion Not Found in Jive';
         }
         if(jiveHttpResponse.getStatusCode()!=200 && jiveHttpResponse.getStatusCode()!=404)
         {
            jsonResponse = 'Application Error';
            
         }
        }
        else
        {
            jsonResponse='{"listForPlugin":[{"data":"mark it as a correct answer","metadata":{"subject":"Re: This is the case to test phase 3","author":"abhijeet raut","msgID":"1373","parentID":"1208","contentID":"1208"},"children":[]}],"msgList":[{"msgID":"1373","description":"mark it as a correct answer","attachments":[{"name":"Koala.jpg","size":780831}],"data":"mark it as a correct answer"}]}'; 
        }
        return jsonResponse;
       }
       catch(Exception ex)
       {
            system.debug('Exception inside JiveViewCtrl1--'+ex.getMessage());
            return null;
       }  
               
    }
    
   
    public void insertDesc()
    {
        try{
            if(tdList.size()>0)
            upsert tdList MessageID__c;
        }
        catch(Exception e)
        {
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
             ApexPages.addMessage(myMsg);
        }
    }
    
    
    public class descJsonWrapper
    {
        String msgID;
        String description;
        List<attachments> attachments;
        String data;
        public descJsonWrapper(String s1,String s2,List<attachments> att,String d)
        {
            msgID = s1;
            description = s2;
            attachments = att;
            data = d;
        }

    }
    
    public class attachments
    {
        String name;
        String size;
        public attachments(String n,String s)
        {
            this.name = n;
            this.size = s;
        }
    }
}