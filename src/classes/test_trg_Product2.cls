@isTest(seeAllData=true)
public with sharing class test_trg_Product2 {
	
	static testMethod void test_assignAssetsToProductByPartNumbers() {
    	
    	CLS_ObjectCreator creator = new CLS_ObjectCreator();
    	
    	Account Acc1 = creator.createAccount();
    	insert acc1;
         
        Contract2__c Contact1 = new Contract2__c(Contract_Term_months__c = 12 ,Account__c = Acc1.Id);
        insert Contact1;
        
        Asset2__c a2 = new Asset2__c(Name ='test1', contract2__c = Contact1.Id, Asset_Type__c = 'Support', part_number__c = '00373');
        insert a2;
        
        Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
         
		Test.StartTest();
		
		insert Pr1;
		
		Test.StopTest();
    }
    
    static testMethod void test_updateProductPhotoUrlOnProductDetailChange() {
    	    	
    	ProductDetails__c testProductDetail = [SELECT Id, StaticResourceImage__c FROM ProductDetails__c WHERE StaticResourceImage__c != null limit 1];
    	 
        Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
        Pr1.Product_Detail__c = testProductDetail.Id;
         
		Test.StartTest();
		
		insert Pr1;
		
		Pr1.Product_Detail__c = null;
		
		update Pr1;
		Pr1.Product_Detail__c = testProductDetail.Id;
		
		update Pr1;
		
		Test.StopTest();
    }
    
     static testMethod void test_assignProductDetailsToNewProducts() {
    	    	
    	ProductDetails__c testProductDetail = [SELECT Id, Name, StaticResourceImage__c FROM ProductDetails__c WHERE StaticResourceImage__c != null limit 1];
    	 
        Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
        Pr1.Product_Detail_Name__c = testProductDetail.Name;
         
		Test.StartTest();
		
		insert Pr1;
		
		Test.StopTest();
    }
    
    
}