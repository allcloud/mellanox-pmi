public with sharing class VF_Loaner_Submit 
{   
      
    public ID LoanId;
    
    public VF_Loaner_Submit(ApexPages.StandardController controller) 
    {
        //id = Apexpages.currentPage().getParameters().get('Id');
        //getDeliverAsPDF();
    }      
                                                                            
  private Boolean ValidateRequierFields(Loaner_request__c odisiloaner)
    {      
     Boolean IsError=false;
     if( odisiloaner.Customer_Type__c == NULL)      
            {         
             IsError=true;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Customer Type\' field'));
            }
     if( odisiloaner.Loan_requested_Delivery_date__c == NULL)      
            {          
             IsError=true;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Loan requested Delivery date\' field'));
            
            }
     if( odisiloaner.Loan_Type__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Loaner Type \' field'));
            IsError=true;
            }
     if( odisiloaner.Justification__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Loan Justification \' field'));
            IsError=true;
            }
    if( odisiloaner.Project_Name__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Project Name \' field'));
            IsError=true;
            }
   if( odisiloaner.Freight__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Freight \' field'));
            IsError=true;
            }
   if( odisiloaner.Service__c== NULL && odisiloaner.Freight__c == 'Paid by Requestor Company' )      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Customer Carrier\' and \'Service \' field'));
            IsError=true;
            }

  if( odisiloaner.End_Customer_Company__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'End Customer  \' field'));
            IsError=true;
            }

  if( odisiloaner.Country__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Country \' field'));
            IsError=true;
            }

  if( odisiloaner.EndCustState__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'State/Province  \' field'));
            IsError=true;
            }
if( odisiloaner.Ship_company__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship to Company\' field'));
            IsError=true;
            }
      
 if( odisiloaner.Ship_Zip__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Zip\' field'));
            IsError=true;
            }

 if( odisiloaner.Ship_Street_Address__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Street Address\' field'));
            IsError=true;
            }

if( odisiloaner.Ship_to_contact__c== NULL || odisiloaner.Ship_to_contact_First_Name__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship To Contact First Name and Last Name\' field'));
            IsError=true;
            }

if( odisiloaner.Ship_City__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship City\' field'));
            IsError=true;
            }

if( odisiloaner.Ship_Email__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Email\' field'));
            IsError=true;
            }

if( odisiloaner.Ship_Country__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Country\' field'));
            IsError=true;
            }
if( odisiloaner.Ship_Phone__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship Phone\' field'));
            IsError=true;
            }

if( odisiloaner.ShipState__c== NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Ship State\' field'));
            IsError=true;
            }
if( odisiloaner.customer_type__c == 'Internal' && (odisiloaner.Internal_request_type__c == NULL || odisiloaner.Internal_request_type__c == ''))      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'For Internal customer please fill the \'Internal Request Type\' field'));
            IsError=true;
            }
                    
if( odisiloaner.Loan_Type__c != 'Sample' && odisiloaner.evaluation_period__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Evaluation period\' field'));
            IsError=true;
            }
if( odisiloaner.Deviation_PCN_related__c == 'Yes' && odisiloaner.Deviation_Hold_Comments__c == NULL )      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please enter Comments when select Deviation-PCN Related field as Yes'));
            IsError=true;
            }
if( odisiloaner.carrier__c == 'Other' && odisiloaner.other_carrier__c == NULL)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please fill the \'Other Carrier\' field'));
            IsError=true;
            }

if( odisiloaner.Num_of_Products__c== 0)      
            {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Loaner should have at least one product'));
            IsError=true;
            }

     return IsError;
                                             
    }               
                             
 public PageReference Submit_loaner() 
    {
    
        // Reference the page, pass in a parameter to force PDF
         LoanId = Apexpages.currentPage().getParameters().get('Id');
         String isTest = Apexpages.currentPage().getParameters().get('isTest');
         
         system.debug('id is : ' + LoanId);
         //get the related RMA id from the case
         Loaner_request__c lr = [Select id,Carrier__c, status__c,Ship_Company__c,Ship_Zip__c,Ship_Street_Address__c,
         							Customer_Type__c,Loan_requested_Delivery_date__c,Loan_Type__c,Justification__c,
         							Project_Name__c,Freight__c,Service__c,Ship_to_contact__c,Ship_to_contact_First_Name__c,
         							Ship_City__c,Ship_Email__c,Deviation_PCN_related__c,Deviation_Hold_Comments__c,        
                                 	Ship_Country__c,Ship_Phone__c,ShipState__c,Evaluation_period__c,Internal_request_type__c ,
                                 	End_Customer_Company__c,Country__c,EndCustState__c,Num_of_Products__c,Other_carrier__c 
                                 From Loaner_Request__c WHERE id =: LoanId];
         if(lr.status__c != 'Draft')
         {   
         Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Loaner was already submitted'));     
         return NULL; 
         }
         else{         
         
              if(ValidateRequierFields(lr))
              	return NULL;    
              else             
              {lr.status__c= 'Submitted';      
               update lr;       
               return new Pagereference('/'+LoanId);
              }
         }// if Draft                     
     }//Submit_loaner
}