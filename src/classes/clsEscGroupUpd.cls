global class clsEscGroupUpd
{
    @future
    public static void UpdateEscGRoup (List<Id> AddEscUser,List<Id> RemoveEscUser)
    {
      List<GroupMember> NewGroupMbrs = New List<GroupMember>();
      map<id,integer> ExistGrpMem = new map<id,integer>();
      map<id,groupMember> GmIdtoGm = new map<id,groupMember>();
      List<GroupMember> RemoveGroupMbrs = new List<GroupMember>();
      List<Escalation_group__c> DeleteEGmember = new List<Escalation_group__c>();
      
        if(!AddEscUser.isempty())
        { 
        
         
         
          for( id NewMr:AddEscUser)
            {
                GroupMember gm = new GroupMember();
                gm.UserOrGroupId =NewMr;
                gm.GroupId = ENV.EscalationOwnersGroup;
                NewGroupMbrs.add(gm);
                
                
             }
             
        System.debug('NewGroupMbrs =  ' + NewGroupMbrs); 
        
        insert(NewGroupMbrs);
               
          
        }
        
        
        
   if(!RemoveEscUser.isempty() )
    {
      Group gr = [Select (Select UserOrGroupId From GroupMembers )id From Group g Where Id = :ENV.EscalationOwnersGroup];
    
     for(GroupMember  gm: gr.GroupMembers)
      {
       GmIdtoGm.put(gm.UserOrGroupId ,gm);
      }
                   
     for(id gu:RemoveEscUser)
      {
        RemoveGroupMbrs.add(GmIdtoGm.get(gu));
      }
     System.debug('RemoveGroupMbrs =  ' + RemoveGroupMbrs);
     
     delete(RemoveGroupMbrs);
     
   } //if
 } // future
}