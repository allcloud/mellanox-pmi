public with sharing class VF_DealScoreRelatedListPerUser {
	
	private Id dealroundId;
	
	public list<Deal_Score__c> dealScores_List {get;set;}
	
	public VF_DealScoreRelatedListPerUser(ApexPages.StandardController controller) {
		
		dealroundId = controller.getId();
		
		system.debug('dealroundId : ' + dealroundId);
		
		
		
		dealScores_List = [SELECT Id, Comments__c, Deal_Rounds__c, Name, Rated_by__c, Score__c, createdDate FROM Deal_Score__c
						   WHERE Deal_Rounds__c =: dealroundId and Rated_by__c =: UserInfo.getUserId()];
	}
	
	public Pagereference deleteDealScore() {
 
	    String  dealId = ApexPages.currentPage().getParameters().get('dealId'); 
	     
	    Deal_Score__c dealToDelete = new Deal_Score__c(Id = dealId);
	     
	     
	    delete dealToDelete;
	      
	    return null;
	}

}