public without sharing class cls_trg_RMA {
	
	// This method will update the Parent Case's State of the RMA to 'Close' when the RMA State is changed to 'Returned not Pending Shipment' 
	public void changeStateOfParentCase(list<RMA__c> newRMAs_List, map<Id, RMA__c> oldRMAs_Map) {
		
		list<Case> parentCases2Update_List = new list<Case>();
		
		for (RMA__c newRMA : newRMAs_List) {
			
			if ( newRMA.RMA_state__c.equalsIgnoreCase('Returned not Pending Shipment') ) {
			    
		    	if ( newRMA.RMA_state__c != oldRMAs_Map.get(newRMA.Id).RMA_state__c ) {
		    		
					Case parentCase = new Case(Id = newRMA.Case_Parent__c);
					parentCase.State__c = 'Closed';
					parentCase.Dont_Close_note__c = true;
					parentCases2Update_List.add(parentCase);
		    	}
			}
		}	
			 
		if (!parentCases2Update_List.isEmpty()) {
			
			try {	
				update parentCases2Update_List;
			}
			
			catch (Exception e) {
				
				if (newRMAs_List.size() == 1) {
					
					newRMAs_List[0].addError('The Case Cannot be updated because of : ' + e.getMessage() );
				}
			}
		}
	}
}