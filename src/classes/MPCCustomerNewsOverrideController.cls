public with sharing class MPCCustomerNewsOverrideController {
	private final Customer_News__c record;
	   
	public MPCCustomerNewsOverrideController(ApexPages.StandardController 
		   controller) {
		   		List <String> fields = new List <String>{'Link__c'};
		   		controller.addFields(fields);
		   		this.record = (Customer_News__c)controller.getRecord();
		   }
	
	public boolean inCommunity() {
		return Network.getNetworkId() != null;
	}
	
	public PageReference redirect() {
		if (inCommunity()) 
		{
			String link;
			if (!record.Link__c.contains('http://')) {
				link = 'http://' + record.Link__c;
			} else {
				link = record.Link__c;
			}
			PageReference customPage =  new PageReference(link);
			customPage.setRedirect(true);
			system.debug('customPage: ' + customPage);
			return customPage;
		} else {
			return null; //otherwise stay on the same page  
		}
	}
}