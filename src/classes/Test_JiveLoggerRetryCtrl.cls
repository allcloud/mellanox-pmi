@isTest(seeAllData=True)
private class Test_JiveLoggerRetryCtrl{
    public static testMethod void testRetryFeature() {  
    Apexpages.standardcontroller sc;
    JiveLoggerRetryCtrl jcntlr;
        Case c1 = new Case();
        c1.subject = 'testCase';
        insert c1;
        
        Logger__c postbackLogger = new Logger__c(FeatureType__c = 'Community Postback - Correct Answer Postback Failure',IsErrorLog__c=true,Status__c = 'Re-Attempted',RetryCount__c=1,SFObject__c = 'Case',JiveContentType__c = 'Question',SFID__c = c1.id,JiveId__c='3455');
        postbackLogger.APIRequestBody__c = '{"replyComments":"correct ans as reply","jiveURL":"https://jivedemo-cloudsquads.jiveon.com","isAnswer":true,"contentID":"3109"}';
        insert postbackLogger;
        sc = new ApexPages.StandardController(postbackLogger);
        jcntlr= new JiveLoggerRetryCtrl(sc); 
        jcntlr.processLogger();
}
}