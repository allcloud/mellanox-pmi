public class TasksList {
    private List<Task> Tasks;
    private Milestone1_project__c project; 
    // Boolean indicating if the first section should be displayed or not
    public boolean displayFirstSection{get;set;}
    // Boolean indicating if the second section should be displayed or not
    public boolean displaySecondSection{get;set;}
    public List<selectOption> DoneOptions_List {get;set;}
   
   
    public TasksList(ApexPages.StandardController controller) {
        this.project= (Milestone1_project__c)controller.getRecord();
        displayFirstSection = true;
        displaySecondSection = false;
    }
    public List<task> getTasks()
    {
            Tasks = [Select id, subject, status, ActivityDate, ownerid, owner.name, priority,IsVisibleInSelfService, InTime__c, What.name, WhatId, projectId__c, Done__c
                 FROM Task where projectId18__c = :project.id AND IsClosed = False order By ActivityDate ASC ];
        
        return Tasks;
    }
    
    public List<SelectOption> getDoneOptions(){
		
		Schema.DescribeFieldResult RequestedDoneDescription = Task.Done__c.getDescribe();
		DoneOptions_List = new list<SelectOption>();
		
		for (Schema.Picklistentry picklistEntry : RequestedDoneDescription.getPicklistValues())
		{
			DoneOptions_List.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
		}
		
		return DoneOptions_List;
	}  
    
    // This method will hide the first section and display the second(edit mode) section
    public void turnToEditMode(){
    	
    	displayFirstSection = false;
        displaySecondSection = true;
    }   
    
    // This method will save the changes made and will revert the related list mode to it's original mode
    public void saveTasks(){     
    	
    	try{
    		
    		update Tasks;
    		displayFirstSection = true;
        	displaySecondSection = false;
    	}
    	
    	catch(Exception e){
    		
    		e.getMessage();
    	}
    }
    
    // This method will Cancel the changes and will revert the related list mode to it's original mode
    public void cancel(){     
    	
		displayFirstSection = true;
    	displaySecondSection = false;
    }
}