/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest
private class Test_OppRegFormOld {

/*
    static testMethod void Test() 
    {
       Test.setCurrentPageReference(Page.OppRegForm);
        OppRegFormControllerOld oform=new OppRegFormControllerOld();
        Pagereference pg;
        pg=oform.Reset();
        System.assertEquals('/apex/oppregform', pg.getUrl());
        oform.GetDistiList();
        oform.odisioppreg.Distributor_Name__c='BELL MICROPRODUCTS';
        oform.odisioppreg.Requestor_Name__c='test';
        oform.odisioppreg.Requestor_Email__c='Test@test.com';
        oform.odisioppreg.Disty_Contact_Email__c='distiTest@test.com';
        oform.odisioppreg.Disty_Contact_Name__c='disiTest';
        oform.odisioppreg.Disty_Phone__c='408544000';
        oform.odisioppreg.Partner_City__c='SanJose';
        oform.odisioppreg.Partner_Contact_Email__c='partnerTest@test.com';
        oform.odisioppreg.Partner_Contact_Name__c='partnerTest';
        oform.odisioppreg.Partner_Contact_Phone__c='4085447000';
        oform.odisioppreg.Partner_Country__c='US';
        oform.odisioppreg.Partner_State__c='CA';
        oform.odisioppreg.Opportunity_Close_Date__c=DateTime.now().date();
        oform.odisioppreg.Name='TestOppTest99929';
        oform.odisioppreg.Opportunity_Ship_Date__c=DateTime.now().date();
        oform.odisioppreg.Mellanox_OPN_1__c=[select id from Oppy_Reg_Products__c limit 1].id;
        oform.odisioppreg.OPN_1_MSRP__c=100;
        oform.odisioppreg.OPN_1_Quantity__c=10;
        oform.odisioppreg.OPN_1_Requested_Cost__c=10;
        oform.odisioppreg.OPN_1_Requested_Discount__c=10;
        oform.Save();
       
        
        
        
        
        
        
        // TO DO: implement unit test 
    } */
}