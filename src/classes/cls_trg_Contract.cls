public class cls_trg_Contract {
	
	// This method will delete the Contract's relatedOpportunity and Quote, when the Contract's field 'Reset_Contract_ERI__c' is checked
	public void deleteRealtedOppsAndQuotesWhenPendingPOUnChecked(list<Contract2__c> newContracts_List, map<id, Contract2__c> oldContracts_Map) {
		
		set<Id> contractsIds_Set = new set<Id>();
		list<Contract2__c> contracts2Update_List = new list<Contract2__c>();
		map<Id, Contract2__c> contractIds2Contracts_Map;
		
		for (Contract2__c newContract : newContracts_List) {
			
			if (newContract.Reset_Contract_ERI__c && !oldContracts_Map.get(newContract.Id).Reset_Contract_ERI__c) {
				
				contractsIds_Set.add(newContract.Id);
			}
		}
		
		if (!contractsIds_Set.isEmpty()) {
			
			contractIds2Contracts_Map = new map<Id, Contract2__c> ([SELECT Id, (SELECT Id, StageName FROM Opportunities2__r), (SELECT Id FROM GPS_Projects__r) FROM Contract2__c WHERE Id IN : contractsIds_Set]);
			 
			for (Contract2__c newContract : newContracts_List) {
				
				if (newContract.Reset_Contract_ERI__c && !oldContracts_Map.get(newContract.Id).Reset_Contract_ERI__c) {
									
					 if(contractIds2Contracts_Map.containsKey(newContract.Id)) {
						
						Contract2__c relatedContract = contractIds2Contracts_Map.get(newContract.Id);
						
						if (!relatedContract.Opportunities2__r.isEmpty()
							&& relatedContract.Opportunities2__r[0].StageName != 'Closed Won'
							&& relatedContract.Opportunities2__r[0].StageName != 'Close Won') {
							
							Contract2__c contract2UPdate = new Contract2__c(Id = newContract.Id);
							contract2UPdate.Reset_Contract_ERI__c = false;
							contract2UPdate.ERI_confirm_PO_received__c = false;
							contract2UPdate.Pending_PO__c = false;
							contract2UPdate.Pending_Paypal_Approval__c = false;
							contract2UPdate.Transaction_Id__c = null;
							contract2UPdate.Payment_Status__c = null;
							contract2UPdate.Link_To_PO__c = null;
							contract2UPdate.ERI_Total_Price__c = null;
							contracts2Update_List.add(contract2UPdate);
						}
					}
				}
			}
		 
			list<Contract2__c> contractswithRelatedObjects_List = contractIds2Contracts_Map.values();
			
			list<SObject> obj2Delete_List = new list<SObject>();
			
			if (!contractswithRelatedObjects_List.isEmpty()) {
				
				for (Contract2__c currentContract : contractswithRelatedObjects_List) {
					
					if (!currentContract.Opportunities2__r.isEmpty()
					    && currentContract.Opportunities2__r[0].StageName != 'Closed Won'
					    && currentContract.Opportunities2__r[0].StageName != 'Close Won') {
						
						for (Opportunity relatedOpp : currentContract.Opportunities2__r) {
							
							obj2Delete_List.add(relatedOpp);
						}
					}
					
					if (!currentContract.GPS_Projects__r.isEmpty()
					    && currentContract.Opportunities2__r[0].StageName != 'Closed Won'
						&& currentContract.Opportunities2__r[0].StageName != 'Close Won') {
						
						for (SFDC_Projects__c relatedGps : currentContract.GPS_Projects__r) {
							
							obj2Delete_List.add(relatedGps);
						}
					}
				}
				
				
				if (!obj2Delete_List.isEmpty()) {
					
					try {
						
						delete obj2Delete_List;
					}
					
					catch(Exception e) {
						
						system.debug('could not delete the Opportunity');
					}
				}
				
				if (!contracts2Update_List.isEmpty()) {
					
					update contracts2Update_List;
				}
			}
		}
	}
	
	// This method will assign an existing Contact to the new Contract Created if such Contact exists and if not,
	// it will create the Contact and assign it to the new Contract
	public void createContactRelatedToContract(list<Contract2__c> newContracts_List) {
		
		map<String, Contact> emails2Contacts_Map = fillUpMapOfEmailsAndContacts(newContracts_List);
		map<String, Contact> contacts2Create_Map = new map<String, Contact>();
		
		for (Contract2__c newContract : newContracts_List) {
			
			if (newContract.End_Contact_Email__c != null) {	
				
				if (emails2Contacts_Map.containsKey(newContract.End_Contact_Email__c)) {
					
					newContract.Contact__c = emails2Contacts_Map.get(newContract.End_Contact_Email__c).Id;
				}
				
				else {
				
					Contact newContact = new Contact();
					newContact.AccountId = newContract.Account__c;
					newContact.Email = newContract.End_Contact_Email__c;
					list<String> names_List = newContract.End_Contact_Name__c.split(' ');
					newContact.LastName = names_List[names_List.size() - 1];
					newContact.FirstName = names_List[0];
					
					contacts2Create_Map.put(newContact.Email, newContact);
				}
			}
		}
		
		if (!contacts2Create_Map.values().isEmpty()) {
			
			insert contacts2Create_Map.values();
		}
		
		for (Contract2__c newContract : newContracts_List) {
			
			if (contacts2Create_Map.containsKey(newContract.End_Contact_Email__c)) {
				
				newContract.Contact__c = contacts2Create_Map.get(newContract.End_Contact_Email__c).Id;
			}
		}
		
	}
	
	
	private map<String, Contact> fillUpMapOfEmailsAndContacts(list<Contract2__c> newContracts_List) {
		
		map<String, Contact> emails2Contacts_Map = new map<String, Contact>();
		set<String> contactsEmails_Set = new set<String>();
		
		for (Contract2__c newContract : newContracts_List) {
			
			if (newContract.End_Contact_Email__c != null) {	
				
					contactsEmails_Set.add(newContract.End_Contact_Email__c);
			}
		}
		
		if(!contactsEmails_Set.isEmpty()) {
			
			list<Contact> contacts_List = [SELECT Id, Email FROM Contact WHERE Email IN : contactsEmails_Set];
			
			for (Contact currentContact : contacts_List) {
				
				emails2Contacts_Map.put(currentContact.Email, currentContact);
			}
		}
		
		return emails2Contacts_Map;
	}
	
	// This method will update the ERI Contract and it's related Opportunity details when it becomes 'Approved'
	public void updateERIContractWhenApproved(list<Contract2__c> newContracts_List, map<Id, Contract2__c> oldContracts_Map) {
		
		set<Id> contractIds_Set = new set<Id>();
		
		for (Contract2__c newContract : newContracts_List) {
			
			if (newContract.Payment_Status__c != null && newContract.Payment_Status__c.EqualsIgnoreCase('Approved')
				&& newContract.Payment_Status__c != oldContracts_Map.get(newContract.Id).Payment_Status__c) {
					
					
				contractIds_Set.add(newContract.Id);	
			}
		}
		
		closeContractOppAndGPS(contractIds_Set, newContracts_List, true);
	}
	
	// This method will close the Contract's related Opportunity and GPS Project as 'Closed Won' when the Contract's 'ERI_confirm_PO_received__c' is checked 'TRUE'
	public void closeContractRelatedOppAndGPSProjects(list<Contract2__c> newContracts_List, map<Id, Contract2__c> oldContracts_Map) {
		
		set<Id> contractIds_Set = new set<Id>();
		
		for (Contract2__c newContract : newContracts_List) {
			
			if (newContract.ERI_confirm_PO_received__c
				&& newContract.ERI_confirm_PO_received__c != oldContracts_Map.get(newContract.Id).ERI_confirm_PO_received__c) {
					
					
				contractIds_Set.add(newContract.Id);	
			}
		}
		
		closeContractOppAndGPS(contractIds_Set, newContracts_List, false);
	}
	
	// This method will fill up the Contract's related Opportunity and GPS Projects fields with values and updated them
	private void closeContractOppAndGPS(set<Id> contractIds_Set, list<Contract2__c> newContracts_List, Boolean fillUpTranaction) {
		
		if (!contractIds_Set.isEmpty()) {
			
			map<Id, Contract2__c> approvedContracts_Map = new  map<Id, Contract2__c> ([SELECT Id, (SELECT Id, Amount FROM Opportunities2__r), 
																					  (SELECT Id FROM GPS_Projects__r) FROM Contract2__c 
			 																		  WHERE Id IN : contractIds_Set]);
			
			list<SObject> objects2Update_List = new list<SObject>();
			 												
			for (Contract2__c newContract : newContracts_List) {
			 	
				if (approvedContracts_Map.containsKey(newContract.Id)) {
			 		
			 		newContract.Stage__c = 'Closed Won';
					newContract.Renewal_Stat__c = 'Renewed';
					newContract.Close_date__c = date.today();
					
					for (Opportunity opp : approvedContracts_Map.get(newContract.Id).Opportunities2__r) {
						
						newContract.Renewal_Cost__c = opp.Amount;
						opp.Sales_Credit_Justification__c = 'Renewal';
						opp.Required_Ship_Date__c = date.today();
						opp.Requested_Ship_Date__c = date.today();
						
						if (fillUpTranaction) {	
							
							opp.ERI_Transaction_ID__c = newContract.Transaction_Id__c;
						}
						
						else {
							
							opp.ERI_Transaction_ID__c = 'PO Received';
						}
						opp.stageName = 'Closed Won';
						opp.Win_Loss_Reason__c = 'SUPPORT CONTRACT';
						objects2Update_List.add(opp);
					}
					
					for (SFDC_Projects__c gpsProj : approvedContracts_Map.get(newContract.Id).GPS_Projects__r) {
						
						gpsProj.Project_Stage__c = 'PO received';
						gpsProj.Amount__c = newContract.Renewal_Cost__c;
						gpsProj.Close_Date__c = date.today();
						objects2Update_List.add(gpsProj);
					}
			 	}
			}
			 
			if (!objects2Update_List.isEmpty()) {
			 	
			 	try {	
			 		update objects2Update_List;
			 	}
			 	
			 	catch(Exception e) {
			 		
			 		newContracts_List[0].addError('Could not update Contract and Opportunity beacuse of : ' +  e.getMessage());
			 	}
			}
		}			
	}

}