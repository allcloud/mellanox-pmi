@isTest(seeAllData=true)
public with sharing class test_trg_ST1 {
	
	static testMethod void test_updateST1ParamsOnInsert() {
		
		Case c = new Case(priority = 'Low');
		insert c;
		
		Id currentUserId = system.UserInfo.getUserId();
		
		ST1_Shift__c st1 = new ST1_Shift__c();
		st1.Shift_Time__c = 'Morning';
		st1.Member_1__c = currentUserId;
		insert st1;
	}
}