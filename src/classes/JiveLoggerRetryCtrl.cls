/**
* @Author- Persistent
* @Description-This class handles the Retry functionality based on feature type when invoked from Retry button on logger object.   
*/
public with sharing class JiveLoggerRetryCtrl {
    
    public Logger__c logger;
    public LoggerSettings__c loggerSettings;
    public Boolean isManualRetry;
    JiveCommunitySettings__c jiveConfigSettings;
    String jiveCommunityUrl;
    String configuredSpaceURL;
    
    //Variable to allow/disallow retry for Community Postback feature
    Boolean allowCommunityPostbackRetry = false;
    //contains error messsage of unhandled exception caught in catch block
    String unHandledException;
    
    //contains error messsage of any exception occured when Jive spaces are retrieved in Jive Knowledge Sync
    public String spaceRetrievalError;
    
    //Stores the api response of Jive Knowledge Sync
    public String loggerAPIResponse;
    
    /**
    * @Author- Persistent
    * @Description-This constructor gets the logger record, checks whether retry is applicable and initiates retry based on the feature type in logger.   
    */
    public JiveLoggerRetryCtrl(ApexPages.StandardController stdController) {
        try{
            this.logger = (Logger__c)stdController.getRecord();
            
            //Logger should already exist
            if(logger.Id != null){
                system.debug('logger record--'+logger);
                logger = [Select Status__c, SFObject__c, SFID__c, Retry__c, RetryCount__c, Name, Logtype__c, LoggedTime__c, LastModifiedDate, JiveId__c, JiveContentType__c, IsErrorLog__c, Id, FeatureType__c, ErrorMessage__c, Description__c, CreatedDate, APIRequestBody__c, APIRequestEndpoint__c, APIRequestMethod__c From Logger__c where id=:logger.id];
                
                //Get default Logger setings
                loggerSettings = JiveAPIUtil.getLoggerSettings();
                system.debug('loggerSettings--'+loggerSettings);
                //Get default Jive custom setings
                jiveConfigSettings = JiveAPIUtil.getJiveCustomSettings();
                if(jiveConfigSettings != null){
                    jiveCommunityUrl = jiveConfigSettings.Jive_URL__c;
                }
                
               
                String jsonRequestString;
                if(logger.IsErrorLog__c && String.isNotBlank(logger.FeatureType__c) && (logger.Status__c == 'Failure' || logger.Status__c == 'Re-Attempted')){
                    system.debug('logger.RetryCount__c--'+logger.RetryCount__c);
                    //Check for valid retry and retry count
                    if((String.isBlank(logger.Retry__c) || logger.Retry__c == 'In-Progress') && (logger.RetryCount__c == null || (logger.RetryCount__c < loggerSettings.MaxRetryCount__c))){
                        if(logger.FeatureType__c == 'Community Postback - Postback Failure' || logger.FeatureType__c == 'Community Postback - Correct Answer Postback Failure'){//Handle retry for Community Postback - Postback Failure and Community Postback - Correct Answer Postback Failure
                            system.debug('inside feature---'+logger.FeatureType__c);
                            allowCommunityPostbackRetry = true;//Since DML is not allowed in constructor, set this variable to true to allow updating logger in pageAction method as part of retry  
                        }
                    }else{//Retry is only allowed if Retry count is less than Max Retry limit configured and Retry is either null or In-progress
                        system.debug('Retry is not applicable for this Logger---');
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Retry is not applicable for this Logger.'));
                    }
                }else{//Retry is only allowed for failed / reattempted loggers
                    system.debug('Retry is not applicable for this Logger---');
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Retry is not applicable for this Logger.'));
                }
            }
        }catch(Exception ex){
            system.debug('Exception inside constructor--'+ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Retry is not successful. Please try again.'));
            unHandledException = ex.getMessage();
        } 
        
    }
    
    /**
    * @Author- Persistent
    * @Description-This method is page action method mainly intended to perform any DML operations on current logger record once the retry is performed which are otherwise not possible from constructor.   
    */
    public void processLogger(){
        system.debug('inside processLogger--');
        try{
            if(logger != null){
                //Update the logger with error details if any uncaught exception is thrown as part of retry
                if(String.isNotBlank(unHandledException)){
                    update JiveAPIUtil.generateLogger(logger,false,true,logger.FeatureType__c,logger.Logtype__c,logger.SFObject__c,logger.JiveContentType__c,logger.SFID__c,logger.JiveId__c,unHandledException);
                    return;
                }
                if(logger.FeatureType__c == 'Community Postback - Postback Failure' || logger.FeatureType__c == 'Community Postback - Correct Answer Postback Failure'){//Handle Retry for 'Community Postback Failure'
                    //Get the API request details of the callout that was originally failed from logger fields
                    if(String.isNotBlank(logger.APIRequestBody__c) && (allowCommunityPostbackRetry != null && allowCommunityPostbackRetry)){
                        //Deserialize into individual params in request body 
                        Map<String, Object> apiRequestBodyMap = (Map<String, Object>)JSON.deserializeUntyped(logger.APIRequestBody__c);
                        system.debug('apiRequestBodyMap--'+apiRequestBodyMap);
                        if(apiRequestBodyMap != null && apiRequestBodyMap.get('isAnswer') != null && apiRequestBodyMap.get('replyComments') != null){
                          
                            String displayMessage = 'Postback request sent to Community. Please check the Logger record later to find its status.';
                            Boolean isAnswer = (Boolean)apiRequestBodyMap.get('isAnswer');
                            String parentId = (String)apiRequestBodyMap.get('parentId');
                            String attID = (String)apiRequestBodyMap.get('attID');
                            String AttName= (String)apiRequestBodyMap.get('AttName');
                            String userEmail = (String)apiRequestBodyMap.get('userEmail');
                            String userName = (String)apiRequestBodyMap.get('userName');
                            if(logger.FeatureType__c == 'Community Postback - Correct Answer Postback Failure'){
                                displayMessage = 'Postback request sent to Community as REPLY, as the Jive question already has a correct answer. Please check the Logger record later to find its status.';
                                isAnswer = false; //Send postback as Reply in retry for Correct answer failure case
                            }
                            
                            //Populate the required parameters and make callout
                            JiveAPIUtil.sendPostbackMessage(jiveCommunityUrl, new Map<String,Boolean>{logger.SFID__c=>isAnswer}, new Map<String,String>{logger.SFID__c=>logger.JiveId__c}, new Map<String,String>{logger.SFID__c=>(String)apiRequestBodyMap.get('replyComments')}, new Set<String>{logger.SFID__c},parentId,logger.id,attID,AttName,userEmail,userName);
                            
                            //Since community postback callout is asyncronous, the result of the callout has to be manually checked on the current logger record later.
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO ,displayMessage));
                        }
                    }
                }
            
            }
        }catch(Exception ex){
            system.debug('Exception inside processLogger--'+ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Retry is not successful.'+ex.getMessage()));
            try{
                update JiveAPIUtil.generateLogger(logger,false,true,logger.FeatureType__c,logger.Logtype__c,logger.SFObject__c,logger.JiveContentType__c,logger.SFID__c,logger.JiveId__c,unHandledException);
            }catch(Exception catchBlockException){
                system.debug('Exception inside catch block while updating the current logger--'+catchBlockException.getMessage());
            }
        }
        system.debug('at the end--');
    }
    
}