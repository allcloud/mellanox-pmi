public class Quote_add_SupportOPN {
	public Quote curr_quote {get;set;}
	public boolean exist_supportOPN {get; set;}
	public boolean is_step1 {get; set;}
	public boolean show_partner_assist {get; set;}
	public boolean show_onsite_dispatch {get; set;}
	
	List<QuoteLineItem> qlines = new List<QuoteLineItem>();
	List<QuoteLineItem> lst_QuoteLineItemToInsert = new List<QuoteLineItem>();
	List<QuoteLineItem> lst_QuoteLineItemToDelete = new List<QuoteLineItem>();
    QuoteLineItem qline;
    Set<ID> set_ProductIDs = new Set<ID>();
    Set<Id> set_PriceBookID = new Set<Id>();
	
	Map<String,Double> map_prodfamily_prod_Qty = new Map<String,Double>();
	//for UFM products, need to take in 1000N_SW_Support and 4000N_SW_Support flags
	// other kind of products, we skip these 2 flags - 
	//05-12-2014
	Map<String,Double> map_UFM_prod_Qty = new Map<String,Double>();

	//KN 10-16-15
	Map<String,Double> map_HCA_directmap_Qty = new Map<String,Double>();
	Double count_HCA_qty_directmap = 0;
	
	//public List<Quote_Support_OPN__c> lst_distinct_supportOPNs {get; set;}
	public List<Quote_Support_OPN__c> lst_supportOPNs {get; set;}
	List<Quote_Support_OPN__c> lst_Cables_supportOPNs = new List<Quote_Support_OPN__c>();
	
	List<Quote_Support_OPN__c> lst_all_HCA = new List<Quote_Support_OPN__c>();
	List<Quote_Support_OPN__c> lst_HCA_supportOPNs = new List<Quote_Support_OPN__c>();
	List<Quote_Support_OPN__c> lst_HCA_directmap_supportOPNs = new List<Quote_Support_OPN__c>();
	
	
	List<Quote_Support_OPN__c> lst_UFM_Only_supportOPNs = new List<Quote_Support_OPN__c>();
		    
    public Quote_add_SupportOPN (ApexPages.StandardController controller){
		curr_quote = [select id,X1000N_SW_Support__c,X4000N_SW_Support__c,Onsite_Dispatch__c,Partner_Assisted__c,Support_Years__c,Support_Level__c,
						New_Price_Book__c,Pricebook2ID,Name,Opportunity_Name__c,Account__r.Name 
						from Quote where id = :controller.getRecord().id];
		set_PriceBookID.add(curr_quote.Pricebook2ID);
		set_PriceBookID.add(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c));
		exist_supportOPN = false;
		is_step1 = false;			
		show_partner_assist = true;
		show_onsite_dispatch = false;				
		lst_supportOPNs = new List<Quote_Support_OPN__c>();
		//lst_distinct_supportOPNs = new List<Quote_Support_OPN__c>();
	}
	public pagereference prev(){
		is_step1 = false;
		exist_supportOPN = false;
		//re-init
		curr_quote.Support_Level__c = '';
		curr_quote.Support_Years__c = '';
		curr_quote.Onsite_Dispatch__c = '';
		curr_quote.Partner_Assisted__c = false;
		curr_quote.X1000N_SW_Support__c = false;
		curr_quote.X4000N_SW_Support__c = false;
		
		show_partner_assist = true;
		show_onsite_dispatch = false;
		curr_quote.Onsite_Dispatch__c = '';
		lst_supportOPNs = new List<Quote_Support_OPN__c>();
		//lst_distinct_supportOPNs = new List<Quote_Support_OPN__c>();
		lst_Cables_supportOPNs = new List<Quote_Support_OPN__c>();
		lst_all_HCA = new List<Quote_Support_OPN__c>();
		lst_HCA_supportOPNs = new List<Quote_Support_OPN__c>();
		lst_HCA_directmap_supportOPNs = new List<Quote_Support_OPN__c>();
		lst_UFM_Only_supportOPNs = new List<Quote_Support_OPN__c>();
		map_prodfamily_prod_Qty.clear();
		map_UFM_prod_Qty.clear();
		map_HCA_directmap_Qty.clear();
		return null;
	}
	public pagereference next(){
		is_step1 = true;
		count_HCA_qty_directmap = 0;
		
		qlines = [select id,supportOPN_by_mapping__c,Product__c,product_type1__c,Quantity
					from QuoteLineItem where quoteid = :curr_quote.id and product_type1__c != 'SERVICE' and supportOPN_by_mapping__c = false];			
		boolean check_only_HCA_Cables = true;
		
		for (QuoteLineItem q : qlines){
			if (q.product_type1__c == 'HCA'){
				if(map_prodfamily_prod_Qty.get('HCA')==null){
					map_prodfamily_prod_Qty.put('HCA',q.Quantity);
				}
				else {
					map_prodfamily_prod_Qty.put('HCA',q.Quantity + map_prodfamily_prod_Qty.get('HCA') );
				}
				//add to HCA direct map
				if(map_HCA_directmap_Qty.get(q.Product__c)==null)
					map_HCA_directmap_Qty.put(q.Product__c,q.Quantity);
				else
					map_HCA_directmap_Qty.put(q.Product__c,q.Quantity + map_HCA_directmap_Qty.get(q.Product__c) );
				//end
			}
			else if (q.product_type1__c == 'CABLES'){
				if(map_prodfamily_prod_Qty.get('CABLES')==null){
					map_prodfamily_prod_Qty.put('CABLES',q.Quantity);
				}
				else {
					map_prodfamily_prod_Qty.put('CABLES',q.Quantity + map_prodfamily_prod_Qty.get('CABLES') );
				}
			}
			else {
				if(q.Product__c == 'SWL-00341' || q.Product__c=='S_W-00133' || q.Product__c=='S_W-00137'){
					if(map_UFM_prod_Qty.get(q.Product__c)==null)
						map_UFM_prod_Qty.put(q.Product__c,q.Quantity);
					else {
						map_UFM_prod_Qty.put(q.Product__c,q.Quantity + map_UFM_prod_Qty.get(q.Product__c) );
					}
				}
				else {
					if(map_prodfamily_prod_Qty.get(q.Product__c)==null)
						map_prodfamily_prod_Qty.put(q.Product__c,q.Quantity);
					else
						map_prodfamily_prod_Qty.put(q.Product__c,q.Quantity + map_prodfamily_prod_Qty.get(q.Product__c) );
				}
				//No display Onsite Dispatch if only HCAs, CABLES
				check_only_HCA_Cables = false;
			}
		}
		if(check_only_HCA_Cables == true)
			show_onsite_dispatch = false;
						
System.debug('...KN map_prodfamily_prod_Qty===' + map_prodfamily_prod_Qty);
		if(map_prodfamily_prod_Qty != null){
			//Query specific OPNs only
			if(curr_quote.Onsite_Dispatch__c == '4H' || curr_quote.Onsite_Dispatch__c == 'Next Business Day'){
				lst_supportOPNs = [select Product_Family__c,Product_OPN__c,Related_Support_OPN__c,Support_OPN__c from Quote_Support_OPN__c
									where Support_Level__c = :curr_quote.Support_Level__c
									and Support_Years__c = :curr_quote.Support_Years__c
									and Onsite_Dispatch__c = :curr_quote.Onsite_Dispatch__c
									and Partner_Assisted__c = :curr_quote.Partner_Assisted__c
									//and X1000N_SW_Support__c = :curr_quote.X1000N_SW_Support__c
									//and X4000N_SW_Support__c = :curr_quote.X4000N_SW_Support__c
									and Used_for_Registration_ONLY__c = false
									and Product_OPN__c in :map_prodfamily_prod_Qty.keyset() ];
				if(map_UFM_prod_Qty!=null && map_UFM_prod_Qty.size()>0){
					lst_UFM_Only_supportOPNs = [select Product_Family__c,Product_OPN__c,Related_Support_OPN__c,Support_OPN__c from Quote_Support_OPN__c
												where Support_Level__c = :curr_quote.Support_Level__c
												and Support_Years__c = :curr_quote.Support_Years__c
												and Onsite_Dispatch__c = :curr_quote.Onsite_Dispatch__c
												and Partner_Assisted__c = :curr_quote.Partner_Assisted__c
												and X1000N_SW_Support__c = :curr_quote.X1000N_SW_Support__c
												and X4000N_SW_Support__c = :curr_quote.X4000N_SW_Support__c
												and Used_for_Registration_ONLY__c = false
												and Product_OPN__c in :map_UFM_prod_Qty.keyset() ];
					if(lst_UFM_Only_supportOPNs != null && lst_UFM_Only_supportOPNs.size()>0)
						lst_supportOPNs.addall(lst_UFM_Only_supportOPNs);
				}
			}
			else {
				lst_supportOPNs = [select Product_Family__c,Product_OPN__c,Related_Support_OPN__c,Support_OPN__c from Quote_Support_OPN__c
									where Support_Level__c = :curr_quote.Support_Level__c
									and Support_Years__c = :curr_quote.Support_Years__c
									and Onsite_Dispatch__c != '4H' and Onsite_Dispatch__c != 'Next Business Day'
									and Partner_Assisted__c = :curr_quote.Partner_Assisted__c
									//and X1000N_SW_Support__c = :curr_quote.X1000N_SW_Support__c
									//and X4000N_SW_Support__c = :curr_quote.X4000N_SW_Support__c
									and Used_for_Registration_ONLY__c = false
									and Product_OPN__c in :map_prodfamily_prod_Qty.keyset() ];
				if(map_UFM_prod_Qty!=null && map_UFM_prod_Qty.size()>0){
					lst_UFM_Only_supportOPNs = [select Product_Family__c,Product_OPN__c,Related_Support_OPN__c,Support_OPN__c from Quote_Support_OPN__c
												where Support_Level__c = :curr_quote.Support_Level__c
												and Support_Years__c = :curr_quote.Support_Years__c
												and Onsite_Dispatch__c = :curr_quote.Onsite_Dispatch__c
												and Partner_Assisted__c = :curr_quote.Partner_Assisted__c
												and X1000N_SW_Support__c = :curr_quote.X1000N_SW_Support__c
												and X4000N_SW_Support__c = :curr_quote.X4000N_SW_Support__c
												and Used_for_Registration_ONLY__c = false
												and Product_OPN__c in :map_UFM_prod_Qty.keyset() ];
					if(lst_UFM_Only_supportOPNs != null && lst_UFM_Only_supportOPNs.size()>0)
						lst_supportOPNs.addall(lst_UFM_Only_supportOPNs);
				}
			}
			//Add match for Cables
			if(map_prodfamily_prod_Qty.containsKey('CABLES')){
					lst_Cables_supportOPNs = [select Product_Family__c,Product_OPN__c,Related_Support_OPN__c,Support_OPN__c from Quote_Support_OPN__c
												where Support_Years__c = :curr_quote.Support_Years__c
												and Support_Level__c = 'Bronze'
												and Product_Family__c = 'CABLES'
												and Used_for_Registration_ONLY__c = false
											];
					if(lst_Cables_supportOPNs != null && lst_Cables_supportOPNs.size()>0)
						lst_supportOPNs.addall(lst_Cables_supportOPNs);	 	
			}
			//Add match for HCA
			if(map_prodfamily_prod_Qty.containsKey('HCA')){
					if(curr_quote.Support_Level__c=='Bronze') {
						lst_all_HCA = [select Product_Family__c,Product_OPN__c,Related_Support_OPN__c,Support_OPN__c from Quote_Support_OPN__c
												where Support_Level__c = :curr_quote.Support_Level__c 
												and Support_Years__c = :curr_quote.Support_Years__c
												and Used_for_Registration_ONLY__c = false
												and (Product_Family__c = 'HCA' or Product_OPN__c in :map_HCA_directmap_Qty.keyset() )];
						if (lst_all_HCA != null) {
							for (Quote_Support_OPN__c q : lst_all_HCA) {
								if (q.Product_OPN__c != null && q.Product_OPN__c != '' ) {
									lst_HCA_directmap_supportOPNs.add(q);
									if(map_HCA_directmap_Qty.get(q.Product_OPN__c) != null)
										count_HCA_qty_directmap += map_HCA_directmap_Qty.get(q.Product_OPN__c);
								}
								else
									lst_HCA_supportOPNs.add(q);	
							} //end for loop
						} //end if
					}
					else {
						lst_all_HCA = [select Product_Family__c,Product_OPN__c,Related_Support_OPN__c,Support_OPN__c from Quote_Support_OPN__c
												where Support_Level__c = :curr_quote.Support_Level__c 
												and Support_Years__c = :curr_quote.Support_Years__c
												and Partner_Assisted__c = :curr_quote.Partner_Assisted__c
												and Used_for_Registration_ONLY__c = false
												and (Product_Family__c = 'HCA' or Product_OPN__c in :map_HCA_directmap_Qty.keyset() )];
						if (lst_all_HCA != null) {
							for (Quote_Support_OPN__c q : lst_all_HCA) {
								if (q.Product_OPN__c != null && q.Product_OPN__c != '' ) {
									lst_HCA_directmap_supportOPNs.add(q);
									if(map_HCA_directmap_Qty.get(q.Product_OPN__c) != null)
										count_HCA_qty_directmap += map_HCA_directmap_Qty.get(q.Product_OPN__c);
								}
								else
									lst_HCA_supportOPNs.add(q);	
							} //end for loop
						} //end if												
					}
					
					if(lst_HCA_directmap_supportOPNs != null && lst_HCA_directmap_supportOPNs.size()>0)
						lst_supportOPNs.addall(lst_HCA_directmap_supportOPNs);
					//only add HCA support if no directmap HCA or not every HCA found a directmap
					if(lst_HCA_directmap_supportOPNs.size() == 0 || lst_HCA_directmap_supportOPNs.size() < map_HCA_directmap_Qty.size()  ) {
						if(lst_HCA_supportOPNs != null && lst_HCA_supportOPNs.size()>0)
							lst_supportOPNs.addall(lst_HCA_supportOPNs);	 	
					}
					//Recount # of Qty needed for HCAs , extract out from direct map
					if (count_HCA_qty_directmap > 0 && map_prodfamily_prod_Qty.get('HCA') != null) {
						map_prodfamily_prod_Qty.put('HCA',map_prodfamily_prod_Qty.get('HCA') - count_HCA_qty_directmap);	
					}
					//
			}
			//
		}
		//Check if there is any matched support OPNs
		if(lst_supportOPNs != null && lst_supportOPNs.size()>0){
			exist_supportOPN = true;
		}
		//Need to consolidate if there are dup support OPNs
		//lst_distinct_supportOPNs = new List<Quote_Support_OPN__c>();
		List<Quote_Support_OPN__c> lst_duplicated_supportOPNs = new List<Quote_Support_OPN__c>();
		//Set<ID> unique_supportOPNs = new Set<ID>();
		Map<ID,Quote_Support_OPN__c> map_distinct_supportOPNs = new Map<ID,Quote_Support_OPN__c>(); 
		for (Quote_Support_OPN__c sup1 : lst_supportOPNs){
			//if(!unique_supportOPNs.contains(sup1.Support_OPN__c)){
			if(!map_distinct_supportOPNs.containsKey(sup1.Support_OPN__c)) {
				map_distinct_supportOPNs.put(sup1.Support_OPN__c,sup1);
			}
			else {
				lst_duplicated_supportOPNs.add(sup1);	
			}
		}
		Double d1, d2;
		for (Quote_Support_OPN__c sup1 : lst_duplicated_supportOPNs){
			Quote_Support_OPN__c tmp_quote_sup = map_distinct_supportOPNs.get(sup1.Support_OPN__c);
			// map_prodfamily_prod_Qty.get(sup.Product_OPN__c)
			if(tmp_quote_sup != null && tmp_quote_sup.Product_OPN__c != null && sup1.Product_OPN__c != null ){
				d1 = map_prodfamily_prod_Qty.get(tmp_quote_sup.Product_OPN__c);
				d2 = map_prodfamily_prod_Qty.get(sup1.Product_OPN__c);
				
				if(d1!=null && d2!=null){
					d1 += d2;
					map_prodfamily_prod_Qty.put(tmp_quote_sup.Product_OPN__c,d1);
				}
				//if can't find from the map, looking into another map
				else {
					d1 = map_HCA_directmap_Qty.get(tmp_quote_sup.Product_OPN__c);
					d2 = map_HCA_directmap_Qty.get(sup1.Product_OPN__c);
					if(d1!=null && d2!=null){
						d1 += d2;
						map_HCA_directmap_Qty.put(tmp_quote_sup.Product_OPN__c,d1);
					}
				}
			}	
		}
		lst_supportOPNs = map_distinct_supportOPNs.values();
		//
		return null;
	} // end init()
	public void setSupportYears(){
		if(curr_quote.Support_Level__c=='Silver' || curr_quote.Support_Level__c=='Gold'){
			if (curr_quote.Support_Years__c == '2' || curr_quote.Support_Years__c == '4' || curr_quote.Support_Years__c == '5'){
				show_onsite_dispatch = false;
				curr_quote.Onsite_Dispatch__c = '';
			}
			else
				show_onsite_dispatch = true;
		}
	}
	public void setBronzeLevel(){
		if(curr_quote.Support_Level__c=='Bronze'){
			show_partner_assist = false;
			show_onsite_dispatch = false;
			curr_quote.Onsite_Dispatch__c = '';	
		}
		else {
			show_partner_assist = true;
			
			if (curr_quote.Support_Years__c == '2' || curr_quote.Support_Years__c == '4' || curr_quote.Support_Years__c == '5') {
				show_onsite_dispatch = false;
				curr_quote.Onsite_Dispatch__c = '';
			}
			else{
				show_onsite_dispatch = true;
				curr_quote.Onsite_Dispatch__c = '';
			}
		}
	}
	public pagereference save(){
		for (Quote_Support_OPN__c sup : lst_supportOPNs){
			set_ProductIDs.add(sup.Support_OPN__c);			
		}
		
		Map<ID,PricebookEntry> map_PricebookEntry = new Map<ID,PricebookEntry>([select id,Pricebook2ID,Product2ID,UnitPrice from PricebookEntry 
                                                                                where Product2Id in :set_ProductIDs and Pricebook2ID in :set_PriceBookID and isActive=true]);
        Map<ID,Map<ID,PricebookEntry>> map_prodID_PricebookID_PBEntryID = new Map<ID,Map<ID,PricebookEntry>>();
        Map<ID,PricebookEntry> map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
        for (PricebookEntry pbentry : map_PricebookEntry.values()){
            if(map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID)==null){
                map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
                map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
                map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
            }
            else {
                map_PricebookID_PBEntryID = map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID);
                map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
                map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
            }
        }
        
        for (Quote_Support_OPN__c sup : lst_supportOPNs){
        	if(sup.Product_Family__c != null && sup.Product_Family__c != '') {
        		if(map_prodID_PricebookID_PBEntryID != null && map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c) != null
        			&& map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(curr_quote.Pricebook2Id) != null
        			&& Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c) != null
        			//&& map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)) != null 
        			&& map_prodfamily_prod_Qty.get(sup.Product_Family__c) != null 
        			&& map_prodfamily_prod_Qty.get(sup.Product_Family__c) > 0)
        		{
        				qline = new QuoteLineItem (QuoteID=curr_quote.ID,supportOPN_by_mapping__c=true, 
        											PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(curr_quote.Pricebook2Id).ID,
                                       				Quantity=map_prodfamily_prod_Qty.get(sup.Product_Family__c),
                                       				UnitPrice=0,
                                       				New_Price__c = 0);
                        if(map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c) != null
                        	&& map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)) != null) {
                        		qline.UnitPrice = map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice;
                        		qline.New_Price__c = map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice;		
                        }
            			lst_QuoteLineItemToInsert.add(qline);                           
        		}                                   
        	}
            else {
				if(map_prodID_PricebookID_PBEntryID != null && map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c) != null
        			&& map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(curr_quote.Pricebook2Id) != null
        			&& Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c) != null
        			//&& map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)) != null 
        			//&& map_prodfamily_prod_Qty.get(sup.Product_OPN__c) != null
        			)
        		{
					qline = new QuoteLineItem (QuoteID=curr_quote.ID, supportOPN_by_mapping__c=true,
												PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(curr_quote.Pricebook2Id).ID,
                                       			Quantity=1,
                                       			UnitPrice=0,
                                       			New_Price__c = 0
                                       			);
					if(map_prodfamily_prod_Qty.get(sup.Product_OPN__c) != null || map_UFM_prod_Qty.get(sup.Product_OPN__c) !=null){
						qline.Quantity = map_prodfamily_prod_Qty.get(sup.Product_OPN__c) != null ? map_prodfamily_prod_Qty.get(sup.Product_OPN__c) : map_UFM_prod_Qty.get(sup.Product_OPN__c);	
					}                    
					if(map_HCA_directmap_Qty.get(sup.Product_OPN__c) != null) {
						qline.Quantity = map_HCA_directmap_Qty.get(sup.Product_OPN__c);
					}
                    if(map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c) != null
                        	&& map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)) != null) {
                        		qline.UnitPrice = map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice;
                        		qline.New_Price__c = map_prodID_PricebookID_PBEntryID.get(sup.Support_OPN__c).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice;		
                    }
        			lst_QuoteLineItemToInsert.add(qline);
        		}                                                   	
            }
            
        }
        
        lst_QuoteLineItemToDelete = [select id from QuoteLineItem where quoteid = :curr_quote.id
        															and (NOT Product__c like 'GPS%')  
        															and (product_type1__c = 'SERVICE' or supportOPN_by_mapping__c = true)]; 

        
        if(lst_QuoteLineItemToInsert.size()>0) {
        	insert lst_QuoteLineItemToInsert;
        	if (lst_QuoteLineItemToDelete != null && lst_QuoteLineItemToDelete.size() > 0)
        		delete lst_QuoteLineItemToDelete;
        }
     	
     	PageReference pg = new PageReference('/'  + curr_quote.ID);
        pg.setRedirect(true);
        return pg;   
	}
}