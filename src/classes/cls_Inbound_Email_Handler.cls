global class cls_Inbound_Email_Handler implements Messaging.inboundEmailHandler{

    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                         Messaging.InboundEnvelope env ) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();                   
        
        if (!email.subject.containsIgnoreCase('a4Y')) {     // The Object prefix in Sandox is 'a1T'
            
            result = handleInboundEmailWithAtt(email, env);
        }  
        
        else {
            
            result = handleInboundEmailUpdateDealScore(email, null);
        }  
        
        return result;          
    }
    
    
    public Messaging.InboundEmailResult handleInboundEmailWithAtt(Messaging.InboundEmail email, 
                         Messaging.InboundEnvelope env ) {
    
        // Create an inboundEmailResult object for returning 
        // the result of the email service.
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
         
        // Create contact and lead lists to hold all the updated records.
        List<Contact> contacts2Update_list = new List <contact>();
        list<Attachment> attachments2Create_List = new list<Attachment>();
        Deal_Flow__c newDealFlow = new Deal_Flow__c ();
        
        
        list<Deal_Flow__c> dealFlows_List = [SELECT Id, Name FROM Deal_Flow__c];
        
        for (Deal_Flow__c flow : dealFlows_List ) {
        
            if (email.subject.containsIgnoreCase(flow.Name)) {
            
                newDealFlow = flow;
            }
        }
        
        if (newDealFlow == null || newDealFlow.Id == null ) {
        
            newDealFlow.Name = email.subject.toLowerCase();
            newDealFlow.Description__c = email.plainTextBody;
            newDealFlow.Deal_Source__c = 'Mail';
        
            insert newDealFlow;
        }
        
         
        // Convert the subject line to lower case so the program can match on lower case.
        
        // The search string used in the subject line.
        
        if (email.binaryAttachments != null) {
           
            for (Messaging.Inboundemail.BinaryAttachment file : email.binaryAttachments) {
            
                Attachment attachment = new Attachment();
                attachment.Name = file .fileName;
                attachment.Body = file .body;
                // some hardcoded or id extracted from email reference
                attachment.ParentId = newDealFlow.Id;
                attachments2Create_List.add(attachment);
            }
        }
         
         if (!attachments2Create_List.isEmpty())  {
            
            insert attachments2Create_List;
         } 
         
        try {
           
        // Look up all contacts with a matching email address.            
            for (Contact con : [SELECT Id, Name, Email, HasOptedOutOfEmail FROM Contact WHERE Email = : env.fromAddress
                              limit 1]) {
                          
                // Add all the matching contacts into the list.   
                con.hasOptedOutOfEmail = true;
                contacts2Update_list.add(con);
            }
            // Update all of the contact records.
         //   update contacts2Update_list;
        }
        catch (System.QueryException e) {
            System.debug('Contact Query Issue: ' + e);
        }   
        
        
        
        System.debug('Found the unsubscribe word in the subject line.');
       
         
        // Return True and exit.
        // True confirms program is complete and no emails 
        // should be sent to the sender of the unsubscribe request. 
        result.success = true;
        return result;
    }   
    
    
    public Messaging.InboundEmailResult handleInboundEmailUpdateDealScore(Messaging.InboundEmail email, 
                         Messaging.InboundEnvelope env ) {
    
        // Create an inboundEmailResult object for returning 
        // the result of the email service.
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
         
        // Create contact and lead lists to hold all the updated records.
        List<Contact> contacts2Update_list = new List <contact>();
         
        
        String dealRoundId = email.Subject.substring(email.Subject.length() - 15 ,email.Subject.length());
        
        Deal_Round__c newDealRound = new Deal_Round__c (Id = dealRoundId); 
        //Deal_Flow__c newDealFlow = new Deal_Flow__c (Id = dealFlowId); 
        system.debug('dealRoundId : ' + dealRoundId);
        
        if (newDealRound != null || newDealRound.Id != null ) {
        
            Deal_Score__c dealScore = new Deal_Score__c();
            dealScore.Deal_Rounds__c = dealRoundId;
            String bodyAsStr = email.plainTextBody.replaceAll( '\\s+', '');
            bodyAsStr = bodyAsStr.substring(0,1);
            system.debug('bodyAsStr : ' + bodyAsStr);
            dealScore.Score__c = decimal.valueOf(bodyAsStr);
             String FromAddressStr = email.fromAddress;
            
            list<User> senderUser_List = [SELECT Id, userName ,Name FROM User WHERE Email =: FromAddressStr];
            
            if (!senderUser_List.isEmpty()) {
                
                system.debug('dealRoundId : ' + dealRoundId);
                
	            list<Deal_Score__c> existingDealScores = [SELECT Id FROM Deal_Score__c WHERE Deal_Rounds__c =: dealRoundId and Rated_by__c =: senderUser_List[0].Id];
	            
	            if (!existingDealScores.isEmpty()) {
	            	
	            	 return null;
	            }
	               
                dealScore.Name = senderUser_List[0].Name;
                dealScore.User_Mail__c = FromAddressStr;
                dealScore.Rated_by__c = senderUser_List[0].Id;
            }
            
            try {
                
            	insert dealScore;
            }
            
            catch(Exception e) {
                
                system.debug('Error :' + e.getMEssage());
            }
        } 
        
        return result;
    }   
}