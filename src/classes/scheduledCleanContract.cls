global class scheduledCleanContract implements Schedulable{  

global void execute(SchedulableContext SC) {


List<contract2__c> Contracts = new List<contract2__c>();
List<contract2__c> UpdContracts = new List<contract2__c>();
Set<contract2__c> Set_UpdContracts = new Set<contract2__c>();
List<String> toAddresses = new List<String>();

Contracts = [select id, clean_contract__c, stage__c, End_Of_Support__c ,Survey_notification__c, Total_Sales_Price_EOS_N__c, Total_Sales_Price_Hardware_N__c, clean_contract_prev__c,Automatic_notification__c from contract2__c]; 



for(contract2__c c:Contracts)
{
 if(c.clean_contract__c == 'NO' && c.Automatic_notification__c == True && c.stage__c == 'Pipeline')        
  {              
  c.Automatic_notification__c = False;
  c.Survey_notification__c = False;
    UpdContracts.add(c);
  }else 
    {    
    if(c.clean_contract__c == 'YES' && c.Automatic_notification__c == False && c.stage__c == 'Pipeline')         
    {             
     c.Automatic_notification__c = True; 
     c.Survey_notification__c = True;
     UpdContracts.add(c);
     }else 
      {                                     
       if(c.clean_contract__c != c.clean_contract_prev__c)         
       {        
         c.clean_contract_prev__c = c.clean_contract__c; 
         UpdContracts.add(c);
       }else            
        { 
         if(c.End_Of_Support__c != NULL && system.today() >  c.End_Of_Support__c )
         {  
          c.Automatic_notification__c = True;
          c.Survey_notification__c = True;
          UpdContracts.add(c); 
         }else
         {
            if(c.Total_Sales_Price_EOS_N__c > 0 && c.End_Of_Support__c != NULL &&
            system.TODAY() > c.End_Of_Support__c &&
            c.Total_Sales_Price_EOS_N__c == c.Total_Sales_Price_Hardware_N__c) 
           { 
            c.stage__c = 'Do not Pursue';
            c.Automatic_notification__c = False;  
            c.Survey_notification__c = False;    
            UpdContracts.add(c);
            }                                    
         }
        }
       }
      }            
 } //for      
 
//UpdContracts.addAll(Set_UpdContracts);
update UpdContracts;
}
}