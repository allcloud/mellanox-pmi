@isTest(seeAllData=true)
private class test_RMA
 {  
    /*
  static testMethod void upd_RMACustomerCase()    
  { 
      List<RMA__c> InsertRMAs = new List<RMA__c>();
      List<Serial_number__c> InsertSNs = new List<Serial_number__c>();
      List<RMA__c> UpdateRMAs = new List<RMA__c>();
      list<SObject> objectsForUpdate_List = new list<SObject>();


        
      CLS_ObjectCreator obj = new CLS_ObjectCreator();
  
   
       Account acc = obj.createAccount();
       acc.Name = 'Elad Test acc';
       insert acc;

 
   
       Contact cont = obj.createContact(acc);
       cont.email = 'rmaemail@sa.com';    
       insert cont;  
  
  
       //User us = obj.createCPUser(cont);
       //insert us;
    
       RMA__c rma = obj.CreateRMA();
       InsertRMAs.add(rma);   
       


       RMA__c rma2 = obj.CreateRMA(); 
       rma2.name__c='';  
       InsertRMAs.add(rma2); 
       
       
       RMA__c rma1 = obj.CreateRMA();  
       rma1.e_mail__c = 'rmaemail@sa.com';     
       InsertRMAs.add(rma1); 
       
       
       RMA__c rma3 = obj.CreateRMA();  
       rma3.e_mail__c = 'rmaemail@sa.com';     
       InsertRMAs.add(rma3); 

       RMA__c rma4 = obj.CreateRMA();  
       rma4.e_mail__c = 'rmaemail@sa.com';     
       InsertRMAs.add(rma4); 
    
       list<RMA__c> rmas_List = [SELECT Id FROM RMA__c limit 4];
       //insert InsertRMAs;

       serial_number__c SN = obj.createSN(rmas_List[0]);  
       sn.doa_new__c='yes';
       sn.RMA_type_by_support__c = 'Advanced Replacement'; 
      // sn.rma__c = rma.id;
       InsertSNs.add(SN);
       
       Serial_number__c sn4 = obj.createSN(rmas_List[1]);
       Sn4.approval__c='Approved';
       Sn4.RMA_type_by_support__c = 'Advanced Replacement';    
       InsertSNs.add(SN4);
       serial_number__c SN8 = obj.createSN(rmas_List[2]);  
       sn8.doa_new__c='yes';
       sn8.RMA_type_by_support__c = 'Advanced Replacement'; 
       Sn8.approval__c='Approved';
       InsertSNs.add(SN8);

        insert InsertSNs;

        SN.advanced_replacement__c = true;
        objectsForUpdate_List.add(SN);
        
       
        rmas_List[2].rma_state__c = 'Approved'; 
        objectsForUpdate_List.add(rmas_List[2]);          
        update  objectsForUpdate_List;
        objectsForUpdate_List = null;
        objectsForUpdate_List = new list<SObject>();  
   
    
        rmas_List[2].rma_state__c = 'Execution in process'; 
        rmas_List[2].rma_number__c ='123123';  
        update rmas_List[2]; 
        
        List<Case> lst_cases = new List<Case>();
           
        Case cs = obj.createRMA_case(acc,cont,rmas_List[0]);
        lst_cases.add(cs);
    
      
      
       Case cs1 = obj.createRMA_case(acc,cont,rmas_List[1]);
       lst_cases.add(cs1);

       Case cs2 = obj.createRMA_case(acc,cont,rmas_List[2]);
       lst_cases.add(cs2);

        system.test.startTest();
       insert lst_cases; 
       
       lst_cases = new list<Case>();
  
       cs1.state__c = 'Assigned';
       cs1.ownerid = '005500000013ipV';
       cs1.assignee__c ='005500000013ipV';
       objectsForUpdate_List.add(cs1);
 
        lst_cases.add(cs1);
       
       cs2.state__c = 'Assigned';
       cs2.ownerid = '005500000013ipV';
       cs2.assignee__c ='005500000013ipV';
       
        lst_cases.add(cs2);
       
       cs.state__c = 'Assigned';
       cs.ownerid = '005500000013ipV';
       cs.assignee__c ='005500000013ipV';
       
       cs2.state__c = 'Move to OEM RMA';
       objectsForUpdate_List.add(cs2);
                          
          
       cs.RMA_type__c = 'Advanced Replacement';
       objectsForUpdate_List.add(cs); 
       update objectsForUpdate_List;
      
       cs.state__c = 'RMA Canceled';    
       cs.RMA_reject_reason__c = 'RMA canceled';                   
       update cs;            
        // closes the case (WF CASE - "Move RMA State cancled to Close") 
        //and thus can't be reopened- validation rule 'Closed_State_Can_not_be_reopened'
 
        rmas_List[1].RMA_state__c = 'Approved';
        updateRmas.add(rmas_List[1]);
    
        updateRmas.add(rmas_List[3]);
            
        update updateRmas; 
        system.test.stopTest();
    }
    
    
    */ 
    static testMethod void testvfRMAForm()
    {
         CLS_ObjectCreator obj = new CLS_ObjectCreator();
         
         Account acc = new Account(name = 'Elad test Account',support_center__c = 'Global', is_oracle_parent__c = 'Yes', type = 'OEM');
     //    insert acc;
        
         Contact con = obj.CreateContact(acc);
     //    insert con;
        
         Case cs = obj.Create_case(acc, con);
     //    insert cs;
         
         RMA__c rma2 = [SELECT Id FROM RMA__c limit 1];
         rma2.name__c='test RMA';
         //insert rma2; 
        
         Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rma2);
         vfRMAForm form = new vfRMAForm(teststandard);
         
         
         Serial_Number__c serial = new Serial_Number__c();
         serial.Problem_Description__c = 'test problem Description';
         serial.Part_Number__c = '12313';
         serial.Serial__c = '321321';
         serial.DOA_new__c = 'Yes';
         serial.RMA__c = rma2.Id;       
         
         Integer ind = 2;
         
         vfRMAForm.SerialNumber s = new vfRMAForm.SerialNumber(serial, ind);
        
         
         list<vfRMAForm.SerialNumber> serialNumners_List = new list<vfRMAForm.SerialNumber>();
         serialNumners_List.add(s);
         
         form.lstSerialNumber = serialNumners_List;
         form.MainRMA.ShipTo_same_as_BillTo__c = true;
        // form.SaveFormAndAddAttachment();
         
         form.AddSerial();
         form.retUrl = '/';
         form.Cancel();
         System.currentPageReference().getParameters().put('CF00N50000001vo7t_lkid',cs.Id);
         form.FillFormFromCase();
         form.getAllCountries();
         form.getFailureAnalysis();
         form.RemoveSerial();
         form.resetForm();
         form.MainRMA.ShipTo_same_as_BillTo__c = true;
         form.SaveForm();
         form.ShipToSameAsBillTo();
         
         
    }
  
  
    static testMethod void testvfRMAForm2()
    {
         CLS_ObjectCreator obj = new CLS_ObjectCreator();  
         
         RecordType parentAccRecType = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: 'ParentAccount'];
         
         Account acc = new Account(name = 'Elad test Account',support_center__c = 'Global', is_oracle_parent__c = 'Yes', type = 'OEM', RecordTypeId = parentAccRecType.Id);
     //    insert acc;
        
         Contact con = obj.CreateContact(acc);
     //    insert con;
        
         Case cs = obj.Create_case(acc, con);
     //    insert cs;
         
         RMA__c rma2 = [SELECT Id FROM RMA__c limit 1]; 
         rma2.name__c='test RMA';
        // insert rma2; 
        
         Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(rma2);
         vfRMAForm form = new vfRMAForm(teststandard);
                  
         Serial_Number__c serial = new Serial_Number__c();
         serial.Problem_Description__c = 'test problem Description';
         serial.Part_Number__c = '12313';
         serial.Serial__c = '321321';
         serial.DOA_new__c = 'Yes';
         serial.RMA__c = rma2.Id;       
         
         Integer ind = 2;
         
         vfRMAForm.SerialNumber s = new vfRMAForm.SerialNumber(serial, ind);
        
         
         list<vfRMAForm.SerialNumber> serialNumners_List = new list<vfRMAForm.SerialNumber>();
         serialNumners_List.add(s);

         form.lstSerialNumber = serialNumners_List;
      
         form.submit();
    }
    
    
    static testMethod void test_VF_AddAttachmentToRMA()
    {
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        Attachment att = new Attachment();
        att.Name = 'Test Att';
        
        RMA__c rma2 = obj.CreateRMA();  
        rma2.name__c='test RMA';
        
        rma2.E_mail__c = 'test@testmail.com';
        insert rma2;
        
        VF_AddAttachmentToRMA addAtt = new VF_AddAttachmentToRMA();
        addAtt.RMAId = rma2.Id;
        
        
        addAtt.upload();
        
        User siteUser = [SELECT Id from User Where Id =: '0055000000145ot' limit 1];
        
        system.runAs(siteUser)
        {
            addAtt.upload();
        }
    }   
    
    static testMethod void test_changeStateOfParentCase () {
    	
    	 CLS_ObjectCreator obj = new CLS_ObjectCreator();
    	 Account acc;
    	 
    	 acc = new Account(name = 'Elad test Account432432',support_center__c = 'Global', is_oracle_parent__c = 'Yes', type = 'OEM');
        	
        try {	
        	 insert acc;
        }
        
        catch(Exception e) {
        	
        	acc = [SELECT Id FROM Account WHERE type =: 'OEM' limit 1];	
        }
        
         Contact con = obj.CreateContact(acc);
         insert con;
        
         Case cs = obj.Create_case(acc, con);
         insert cs;
         
         RMA__c rma =  obj.CreateRMA();
         rma.Case_Parent__c = cs.Id;
         insert rma;
         
         test.startTest();
         
         rma.RMA_state__c = 'Returned not Pending Shipment';
         update rma;
        
         
         test.StopTest();
    }
}