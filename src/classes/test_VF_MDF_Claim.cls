@isTest(seeAllData=false)
public class test_VF_MDF_Claim {
    
    static testMethod void test_TheVF() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        acc.Name = 'test Name';
        acc.billingStreet = 'test Street';
        insert acc; 
        
        SFDC_MDF__c mdf = new SFDC_MDF__c();  
        mdf.Account__c = acc.Id;
        mdf.Name = 'Test MDF';
        mdf.Amount__c = 1200;        
        mdf.Activity_Start_Date__c = date.today().addDays(20);
        mdf.Activity_End_Date__c = mdf.Activity_Start_Date__c + 10;
        mdf.Fund_Request_Type__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1000;
        mdf.Activity_Description__c = 'Advertising';
        mdf.Purpose_of_Activity__c = 'Advertising';
        mdf.Activity_Product_Focus__c = 'Advertising';
        mdf.Activity_Vertical_Market_Focus__c = 'Advertising';
        mdf.Marketing_Dollars_Requested__c = 1000;
        mdf.Target_Audience__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1500;
        
        insert mdf;
        
        SFDC_MDF_Claim__c mdfClaim = new SFDC_MDF_Claim__c();
        mdfClaim.Account__c = acc.Id;
        mdfClaim.Activity_Result__c = 'test';
        mdfClaim.Activity_Result_Description__c = 'test';
        
        //mdfClaim.Name = 'Test MDF';
        mdfClaim.Invoice_Date__c = mdf.Activity_Start_Date__c + 10;
        mdfClaim.Amount__c = 1200;
        mdfClaim.Fund_Request__c = mdf.Id;
        insert mdfClaim;
        
        Attachment_Holder__c holder = new Attachment_Holder__c();
        holder.Name = UserInfo.getUserId();
        holder.Attachment_Uploaded__c = true;
        insert holder;
        
        Attachment testAtt1 = new Attachment();
        testAtt1.parentId = holder.Id;
        testAtt1.Name = 'test Name';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        testAtt1.Body = bodyBlob;
        
        Attachment testAtt = new Attachment();
        testAtt.parentId = holder.Id;
        testAtt.Name = 'test Name';
        Blob bodyBlob2 = Blob.valueOf('Unit Test Attachment Body');
        testAtt.Body = bodyBlob2;
        insert testAtt;
        
        holder.Invoice_ID__c = testAtt.Id;
        
        update testAtt;
        update holder;
        
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(mdfClaim);
        VF_MDF_Claim thePage = new VF_MDF_Claim(teststandard);
        thePage.createAttachmentHolder();
        thePage.submitForApprovalNoattNedded();
        thePage.submitForApproval();
        thePage.allFileList = new list<Attachment>{testAtt1};
        
        Attachment_Holder__c holder2 = new Attachment_Holder__c();
        holder2.Name = UserInfo.getUserId();
        holder2.Attachment_Uploaded__c = true;
        holder2.Invoice_ID__c = testAtt.Id;
       // insert holder2;
        
        thePage.SaveAttachments();
        thePage.saveAsDraft();
        thePage.turnExitingRecordNonEditable();
        thePage.cancelRequest();
        thePage.allFileList = new list<Attachment>();
        thePage.FileCount = '3';
        thePage.ChangeCount();
    }
    
     static testMethod void test_TheVFNew() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        acc.Name = 'test Name';
        acc.billingStreet = 'test Street';
        insert acc; 
        
        SFDC_MDF__c mdf = new SFDC_MDF__c();
        mdf.Account__c = acc.Id;
        mdf.Name = 'Test MDF';
        mdf.Amount__c = 1200;        
        mdf.Activity_Start_Date__c = date.today().addDays(20);
        mdf.Activity_End_Date__c = mdf.Activity_Start_Date__c + 10;
        mdf.Fund_Request_Type__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1000;
        mdf.Activity_Description__c = 'Advertising';
        mdf.Purpose_of_Activity__c = 'Advertising';
        mdf.Activity_Product_Focus__c = 'Advertising';
        mdf.Activity_Vertical_Market_Focus__c = 'Advertising';
        mdf.Marketing_Dollars_Requested__c = 1000;
        mdf.Target_Audience__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1500;
        
        insert mdf;
        
        SFDC_MDF_Claim__c mdfClaim = new SFDC_MDF_Claim__c();
        mdfClaim.Account__c = acc.Id;
        //mdfClaim.Name = 'Test MDF';
        mdfClaim.Amount__c = 1200;
        mdfClaim.Fund_Request__c = mdf.Id;
        
        Attachment_Holder__c holder = new Attachment_Holder__c();
        holder.Name = UserInfo.getUserId();
        holder.Attachment_Uploaded__c = true;
        insert holder;
        
        Attachment testAtt1 = new Attachment();
        testAtt1.parentId = holder.Id;
        testAtt1.Name = 'test Name';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        testAtt1.Body = bodyBlob;
        
        
        Attachment testAtt = new Attachment();
        testAtt.parentId = holder.Id;
        testAtt.Name = 'test Name';
        Blob bodyBlob2 = Blob.valueOf('Unit Test Attachment Body');
        testAtt.Body = bodyBlob2;
        insert testAtt;
        
        holder.Invoice_ID__c = testAtt.Id;
        
        update testAtt;
        
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(mdfClaim);
        VF_MDF_Claim thePage = new VF_MDF_Claim(teststandard);
       
        
    }
}