public with sharing class VF_RMATypeEZChip {

    RMA__c currentRMA {get;set;}
    public boolean displayMsg {get;set;}
    
    public VF_RMATypeEZChip(Apexpages.standardController controller) {
        
        displayMsg = false;
        Id currnetRMAId = controller.getId();
        
        currentRMA = [SELECT Id, Is_OEM__c, (SELECT Id, Source__c FROM Serial_Numbers__r) FROM RMA__c WHERE Id =: currnetRMAId];
        
       // if (currentRMA.Is_OEM__c != null && currentRMA.Is_OEM__c.EqualsIgnoreCase('Yes') ) {
           
            if (!currentRMA.Serial_Numbers__r.isEmpty()) {
            
                for (Serial_Number__c sn : currentRMA.Serial_Numbers__r) {
                
                    if (sn.Source__c != null) {
                    
                        if (sn.Source__c.EqualsIgnoreCase('NPU')) {
                        
                            displayMsg = true; 
                        }
                    }
                }
            }
        
    }
}