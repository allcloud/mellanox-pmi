@isTest(seeAllData=true)
public with sharing class test_Schedule_Cases_Breached_EMC {

	static testMethod void test_Batch_Cases_Breached_EMC() {
	
		CLS_ObjectCreator creator = new CLS_ObjectCreator();
		Account acc = creator.createAccount();
        insert acc;
        
        Contact con = creator.CreateContact(acc);
        insert con;
        
        Case c = [SELECT Id, Case_age__c, Priority, Status, Reopened_Case__c, State__c FROM Case WHERE Case_age__c > 2 limit 1];
        
        c.AccountId = acc.Id;
        c.ContactId = con.Id;
        c.Priority = 'Low';
        c.Status = 'Open';
        c.Reopened_Case__c = false;
        c.State__c = 'Open';
        
        update c;
        
        test.startTest();
                
		Schedule_Cases_Breached_EMC sch = new Schedule_Cases_Breached_EMC();

        String jobId = System.schedule('ScheduleCases',
        '0 0 0 8 9 ? 2020', sch);
        
        test.stopTest();
	}
	
}