public with sharing class DAL_Contact {
	
	public static Contact getContactByEmail(String emailAddress)
	{
		return [SELECT Id, AccountId FROM Contact WHERE Email =: emailAddress limit 1];
	}

}