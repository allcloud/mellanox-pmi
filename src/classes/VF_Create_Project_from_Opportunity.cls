public with sharing class VF_Create_Project_from_Opportunity 
{   
     
   public Opportunity  Opp {get;set;}
   public Id theOppId {get;set;}    
   public String Description {get;set;}   
   public String Name {get;set;} 
    
        
   public VF_Create_Project_from_Opportunity  (ApexPages.StandardController controller) 
    {
            
           theOppId = Apexpages.currentPage().getParameters().get('id');
        
           Opp = new Opportunity();
           Opp = [  Select  op.id,op.ownerId, op.accountId  from  opportunity op
                            WHERE op.Id =: theOppId];       
                             
      //  if(OppReg.Registration_Status_Calc__c != 'Approved')
      //  {   
      // Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Opportunity can be created only on Approved registration'));
      //   }
    }                    
                
               
     public Pagereference Cancel()
     {           
       return new Pagereference('/'+theOppId);       
     }     
       
    public Pagereference Save()
    {     
      //****************Validations*********************                
      //  if(OppReg.Registration_Status_Calc__c != 'Approved')
      //   {   
      //   Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Opportunity can be created only on Approved registration'));
      //   return  Apexpages.currentPage();
      //   }

         Milestone1_Project__c Pr = new Milestone1_Project__c();
         Pr.name = name;
         Pr.description__c = Description;       
         Pr.c_sales_owner__c = Opp.ownerId;    
         Pr.c_Account__c = Opp.AccountId;
         insert Pr;       
         
         Project_Related_Opportunities__c PO = new Project_Related_Opportunities__c();
         PO.Opportunity__c = Opp.id;
         PO.Project__c= Pr.id;
         insert PO;
        
         return new Pagereference('/'+Pr.Id);             
    }   
/*    
    public List<SelectOption> getPriorities() 
    {
        List<SelectOption> Options = new List<SelectOption>();
        Options.add(new SelectOption('--None--','--None--'));

        List<Milestone1_Project__c> Priorities = new List<Milestone1_Project__c>([Select p.c_priority__c FROM Milestone1_Project__c p]);
        for (Milestone1_Project__c Prio : Priorities)
        {
            Options.add(new SelectOption(Prio,Prio));
        }
        return options;
            
            
            
    }

*/
}