public class ContentVersionHandler {
	
	
	public void prepareMappingOfUsersAndContentVersions(list<ContentVersion> newContents_List, map<id, ContentVersion> oldContents_Map) {
		
		map< Id, list<ProductDetails__c> > versions2ProductDeatilsLists_Map = new map< Id, list<ProductDetails__c> >();
		list<User> users2Update_List = new list<User>();
		//system.debug('newContentsIds_set : ' + newContentsIds_set);
		
		//list<ContentVersion> newContents_List = [SELECT Id, Product__c, Title FROM ContentVersion WHERE Id IN : newContentsIds_set];
		
		set<String> productNames_Set = new set<String>();
		
		for (ContentVersion version : newContents_List) {
			
			
			if ( (trigger.isUpdate && version.Product__c != null && version.Product__c != oldContents_Map.get(version.id).Product__c && oldContents_Map.get(version.id).Product__c == null)
				|| trigger.isInsert && version.Product__c != null && version.VersionNumber.EqualsIgnoreCase('1')) {
				
				if (version.Product__c.contains(';')) {
					
					list<String> productsNames2retrieveFromDB_List = version.Product__c.split(';');
					
					if (!productsNames2retrieveFromDB_List.isEmpty()) {
						
						for (String prName : productsNames2retrieveFromDB_List) {
							
							productNames_Set.add(prName);
						}
					}
				}
				
				else {
					
					productNames_Set.add(version.Product__c);
				}
			}
		}
		
		if (!productNames_Set.isEmpty()) {
			
			map<Id, Product_Subscriptions__c> ids2ProductSubscriptions = 
			new map<Id, Product_Subscriptions__c>([SELECT Id, Product__c, Subscriber__c, Product__r.Name, Subscriber__r.Id FROM Product_Subscriptions__c
												   WHERE Product__r.Name IN : productNames_Set]);
			
			set<Id> relatedUsersIds_Set = new set<Id>();
			
			if (!ids2ProductSubscriptions.values().isEmpty()) {
			
				for (Product_Subscriptions__c subscription : ids2ProductSubscriptions.values()) {
					
					relatedUsersIds_Set.add(subscription.Subscriber__r.Id);
				}
				
				if (!relatedUsersIds_Set.isEmpty()) {
					
					map<Id, User> ids2Users_Map = 
					new map<Id, User>([SELECT Id, ContactId, Contact.CRM_Content_Permissions__c FROM User WHERE Id IN : relatedUsersIds_Set]);
				
					if(!ids2Users_Map.isEmpty()) {
						
						// Map of Users Ids and related Product Names
						map<Id, list<String> > usersIds2ProductNamesList_Map = new map< Id, list<String> >();
						
						for(Product_Subscriptions__c subscription : ids2ProductSubscriptions.values()) {
							
							if (usersIds2ProductNamesList_Map.containsKey(subscription.Subscriber__r.Id)) {
								
								if (subscription.Product__c != null && subscription.Product__r.Name != null) {
									
									usersIds2ProductNamesList_Map.get(subscription.Subscriber__r.Id).add(subscription.Product__r.Name);
								}
							}
							
							else {
								
								if (subscription.Product__c != null) {
									
									list<String> productNames_List = new list<String>();
									productNames_List.add(subscription.Product__r.Name);
									usersIds2ProductNamesList_Map.put(subscription.Subscriber__r.Id, productNames_List);
								}
							}
						}
						
						// Map of Product Names and related content Versions
						map< String, list<ContentVersion> > productNumbers2VersionsList_Map = new map< String, list<ContentVersion> >();
						
						for (ContentVersion version : newContents_List) {
							
							list<String> productsNames2retrieveFromDB_List = new list<String>();
							
							if (version.Product__c != null) {
								
								if (version.Product__c.contains(';')) {
									
									productsNames2retrieveFromDB_List = version.Product__c.split(';');
								}
								
								else {
									
									productsNames2retrieveFromDB_List.add(version.Product__c);
								}
							}
							
							if (!productsNames2retrieveFromDB_List.isEmpty()) {
								
								for (String prName : productsNames2retrieveFromDB_List) {
										
									if (productNumbers2VersionsList_Map.containsKey(prName)) {
										
										productNumbers2VersionsList_Map.get(prName).add(version);
									}	
									
									else {
										
										list<ContentVersion> versions_List = new list<ContentVersion>();
										versions_List.add(version);
										productNumbers2VersionsList_Map.put(prName, versions_List);
									}
								}
							}
						}
						
						if (!productNumbers2VersionsList_Map.isEmpty()) {
							
							// Map of Users and related Content Versions
							map< Id, list<ContentVersion> >  usersIds2ContentVersionsList_Map = new map< Id, list<ContentVersion> >();
							
							for (Id userId : usersIds2ProductNamesList_Map.keySet()) {
								
								if (usersIds2ProductNamesList_Map.containsKey(userId))	 {	
									
									for (String partName : usersIds2ProductNamesList_Map.get(userId)) {
										
										 if (productNumbers2VersionsList_Map.containsKey(partName)) {
											 
											 for ( ContentVersion relatedVersion : productNumbers2VersionsList_Map.get(partName) ) {
											 	
											 	 if (usersIds2ContentVersionsList_Map.containsKey(userId)) {
											 	 	
											 	 	usersIds2ContentVersionsList_Map.get(userId).add(relatedVersion);
											 	 }
											 	 
											 	 else {
											 	 	
											 	 	list<ContentVersion> versions_list = new list<ContentVersion>();
											 	 	versions_list.add(relatedVersion);
											 	 	
											 	 	usersIds2ContentVersionsList_Map.put(userId, versions_list);
											 	 }
											 }
										 }
									}
								}
							}
														
							if (!usersIds2ContentVersionsList_Map.values().isEmpty()) {
								
								map<Id, String> usersIds2GroupNames_Map = getGroupNamesReated2Users(ids2Users_Map.keySet());
								map<Id, ContentWorkspace> contentWorkspaceName2Ids_Map = getContentWorkspaceIdsByNames(newContents_List);
								list<Content_Update__c> contentUpdates_List = new list<Content_Update__c>();
								set<Id> system_NDA_Users_Set = getSetOfUsersInPublicGroup('System NDA');
								set<Id> Design_NDA_Users_Set = getSetOfUsersInPublicGroup('Design NDA');
								
								for (Id userId : usersIds2ContentVersionsList_Map.keySet()) {
									
									for (ContentVersion version : usersIds2ContentVersionsList_Map.get(userId)) {
										
										boolean createContentUpdate = false;
										ContentWorkspace relatedWorkspace;
										
									   	 if (contentWorkspaceName2Ids_Map.containsKey(version.ContentDocumentId)) {
									   	    
									   	    relatedWorkspace = contentWorkspaceName2Ids_Map.get(version.ContentDocumentId);
									   	 }
										user currentUser = ids2Users_Map.get(userId);
									
										if (currentUser.ContactId != null) {
											
											if ( relatedWorkspace != null && relatedWorkspace.Name != null && relatedWorkspace.Name.Contains('System NDA')
											     && (system_NDA_Users_Set.contains(currentUser.Id) || Design_NDA_Users_Set.contains(currentUser.Id) ) ) {
													
													createContentUpdate = true;
											}
											
											else if ( relatedWorkspace != null && relatedWorkspace.Name != null && relatedWorkspace.Name.Contains('Design NDA') 
												      && Design_NDA_Users_Set.contains(currentUser.Id) ) {
													
													createContentUpdate = true;
											}
											
											else if ( relatedWorkspace != null && relatedWorkspace.Name != null && relatedWorkspace.Name.containsIgnoreCase('OEM')) {	
												
												String contentGroupNameNoDesignIn_Str = relatedWorkspace.Name.remove(' OEM').trim();
												
											    if (usersIds2GroupNames_Map.containsKey(userId) && contentGroupNameNoDesignIn_Str != null
											    && usersIds2GroupNames_Map.get(userId).contains(contentGroupNameNoDesignIn_Str)) {
														
													createContentUpdate = true;
											    }		
											}
											
											else if (usersIds2GroupNames_Map.containsKey(userId)) {
												
												//system.debug('relatedWorkspace.Name : ' + relatedWorkspace.Name);
												//system.debug('usersIds2GroupNames_Map.get(userId) : ' + usersIds2GroupNames_Map.get(userId));
												
												if (relatedWorkspace != null && relatedWorkspace.Name != null && relatedWorkspace.Name.EqualsIgnoreCase(usersIds2GroupNames_Map.get(userId))) {
													
													system.debug('inside 211');
													createContentUpdate = true;
												}
											}
										}
										
										else {
											
											createContentUpdate = true;
										}
										
										if (createContentUpdate) {
											
											Content_Update__c newContentUpdate = new Content_Update__c();     
											newContentUpdate.User__c = userId;
											newContentUpdate.Content_Link__c = version.id;
											newContentUpdate.Version__c = version.Title;  
											newContentUpdate.Version_Name__c = version.Doc_SW_Revision__c;
											contentUpdates_List.add(newContentUpdate);
											users2Update_List.add(ids2Users_Map.get(userId));
										}
									}
								}
								
								if (!contentUpdates_List.isEmpty()) {
    		
						    		Database.SaveResult[] lst_SR = Database.insert(contentUpdates_List, false);
								 	
								    for(Database.SaveResult sr: lst_SR)
								    {
								        if(!sr.isSuccess())
								        {
								        	system.debug('==>sr.getMessage(): ' + sr.getErrors());
								        }
								    }
						    	}
						    	
						    	for (User theUser : users2Update_List) {
						    		
						    		theUser.Send_Product_Subscription_Email__c = true;
						    	}
						    	
						    	if (!users2Update_List.isEmpty()) {
						    		
						    		try {
						    			update users2Update_List;
						    		}
						    		
						    		catch(Exception ex) {
						    			
						    			system.debug('Error : ' + ex.getMessage());
						    		}
						    	}
							}
						}
					}
				}
			}	
		}
	}
	
	
	public void createContentUpdatesPerNewContentVersion(list<ContentVersion> newContents_List, map<id, ContentVersion> oldVersion_map) {
		
		set<Id> contentDocumentsIds_Set = new set<Id>();
		// Map of content Id to content Version
		map<Id, ContentVersion> contentIds2ContentVersion_Map = new map<Id, ContentVersion>();
		set<Id> ContentDocumentId_Set = new set<Id>();
		
		
		for (ContentVersion version : newContents_List) {
			
			ContentDocumentId_Set.add(version.ContentDocumentId);
		}
		
		map<Id, ContentDocument> ContentDocumentId2ContentDocument_Map = new map<Id, ContentDocument>([SELECT Id, ParentId FROM ContentDocument WHERE ID IN : ContentDocumentId_Set and PublishStatus =: 'P']);
		
		for (ContentVersion version : newContents_List) {
			
			
			if (!ContentDocumentId2ContentDocument_Map.containsKey(version.ContentDocumentId)) {
				
				return;
			}
			
			
			if (trigger.isUpdate) {
			
				ContentVersion oldVersion = oldVersion_map.get(version.Id);
				
				system.debug('old Version reason for change : ' + oldVersion.ReasonForChange);
				system.debug('New Version reason for change : ' + version.ReasonForChange);	
				
				if (oldVersion.ReasonForChange == null && version.ReasonForChange != null) {
					// putting the related content Id in a set
					contentDocumentsIds_Set.add(version.ContentDocumentId);
					
					contentIds2ContentVersion_Map.put(version.ContentDocumentId, version);
				}
			}
			
			else if (trigger.isInsert && version.Product__c != null && version.ContentDocumentId != null) {
				
				contentIds2ContentVersion_Map.put(version.ContentDocumentId, version);
			}		
		}
		
		
		if ( !contentDocumentsIds_Set.isEmpty() ) {
			
			list<Document_Subscriptions__c> relatedDocSubs_List = [SELECT Id, User__c, Document__c FROM Document_Subscriptions__c 
																   WHERE Document__c IN : contentDocumentsIds_Set];
			set<Id> relatedUsersIds_Set = new set<Id>();						
																   
			if ( !relatedDocSubs_List.isEmpty() ) {
				
				list<Content_Update__c> contentUpdates_List = new list<Content_Update__c>();
				
				for (Document_Subscriptions__c subscription : relatedDocSubs_List) {
					
					relatedUsersIds_Set.add(subscription.User__c);
					
					ContentVersion currentContentVersion = contentIds2ContentVersion_Map.get(subscription.Document__c);
					
					if (currentContentVersion != null) {
						
						Content_Update__c newContentUpdate = new Content_Update__c();     
						newContentUpdate.User__c = subscription.User__c;
						newContentUpdate.Content_Link__c = currentContentVersion.id;
						newContentUpdate.Version__c = currentContentVersion.Title;
						 
						newContentUpdate.Version_Name__c = currentContentVersion.Doc_SW_Revision__c;
						
						contentUpdates_List.add(newContentUpdate);
					}
				}
				
				if (!contentUpdates_List.isEmpty()) {
					
					dataBase.saveResult[] rs = dataBase.Insert(contentUpdates_List, false);
						
					for(Database.SaveResult result : rs) {
					    
				        if(!result.isSuccess()) {
				        
				        	system.debug('==> Insert 245 result.getMessage(): ' + result.getErrors());
				        }
				    }
				}
				
				
				if ( !relatedUsersIds_Set.isEmpty() ) {
				
					list< User> relatedUsers_list = [SELECT Id, Send_Document_subscription_email__c FROM User
													WHERE Id IN : relatedUsersIds_Set]; 
													
					if ( !relatedUsers_list.isEmpty() ) {
						
						for (User theUser : relatedUsers_list) {
							
							theUser.Send_Document_subscription_email__c = true;
						}
						
						dataBase.saveResult[] rs = dataBase.Update(relatedUsers_list, false);
						
						for(Database.SaveResult result : rs) {
						    
					        if(!result.isSuccess()) {
					        
					        	system.debug('==>update 269 result.getMessage(): ' + result.getErrors());
					        }
					    }
					}
				}								
			}
		}
	}
	
	// This method returns a map of all the Content Workspaces (Libraries) associated with the new ContentVersion created
	private map<Id, ContentWorkspace> getContentWorkspaceIdsByNames(list<ContentVersion> newContents_List) {
		
		set<Id> relatedContentDocsIds_Set = new set<Id>();
		map<Id, ContentWorkspace> contentWorkspaceName2ContentWorkspaces_Map = new map<Id, ContentWorkspace>();
		
		for (ContentVersion newVersion : newContents_List) {
			
			relatedContentDocsIds_Set.add(newVersion.ContentDocumentId);
		}
		
		if (!relatedContentDocsIds_Set.isEmpty()) {
			
			list<ContentDocument> relatedContentDocs_List = [SELECT Id, ParentId FROM ContentDocument WHERE ID IN : relatedContentDocsIds_Set and PublishStatus =: 'P'];
			
			set<Id> contentWorkspaceIds_Set = new set<Id>();
			
			for (ContentDocument contentDoc : relatedContentDocs_List) {
				
				contentWorkspaceIds_Set.add(contentDoc.ParentId);
			}
			
			map<Id, ContentWorkspace> relatedcontentWorkspace_Map = new map<Id, ContentWorkspace> ([SELECT Name, Id 
																	FROM ContentWorkspace WHERE Id IN : contentWorkspaceIds_Set]);
			
			
			for (ContentDocument contentDoc : relatedContentDocs_List) {
				
				contentWorkspaceName2ContentWorkspaces_Map.put(contentDoc.Id, relatedcontentWorkspace_Map.get(contentDoc.ParentId));
			}
		}
		
		return contentWorkspaceName2ContentWorkspaces_Map;
	} 
	
	
	private map<Id, String> getGroupNamesReated2Users(set<Id> usersIds_Set) {
		
		map<Id, String> usersIds2Groups_Map = new map<Id, String>();
		
		list<GroupMember> groupMembers_List = [SELECT Id, Group.Name, UserOrGroupId From GroupMember WHERE UserOrGroupId IN : usersIds_Set];
		
		for (GroupMember member : groupMembers_List) {
			
			if (member.Group.Name != null) {
				
				usersIds2Groups_Map.put(member.UserOrGroupId, member.Group.Name);
			}
		}
		
		return usersIds2Groups_Map;
	}
	
	// This method will return a set of users Ids reprsents the users in a public group 
	public set<Id> getSetOfUsersInPublicGroup(String groupName) {
		
		set<Id> usersIdsInPublicGroup_Set = new set<Id>();
		
		list<GroupMember> groupMembersInGroup_List = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.Name =: groupName];
		
		for (GroupMember member : groupMembersInGroup_List) {
			
			usersIdsInPublicGroup_Set.add(member.UserOrGroupId);
		}
		
		return usersIdsInPublicGroup_Set;
	}
	
	
	// This method will create a new feed Item per each Content Version of Record type 'License Content' created and per each License that 
	// has yet to be expired
	public void createFeedItems(list<ContentVersion> newContentVersions_List) {
		
		set<ContentVersion> contentRecTypeLicenseContentIds_Set = getRelevantContentVersionIds(newContentVersions_List);
		
		map<String, License_Product__c> LPNmaes2LicenseProducts_Map = getRelatedLicenseProducts(contentRecTypeLicenseContentIds_Set);
		
		if (!contentRecTypeLicenseContentIds_Set.isEmpty()) {
			
			list<License__c> relvantLicenses_List = [SELECT Id, Product_Type__c FROM License__c WHERE Parent_License__c = true and Contract_End_Date__c >: date.today()];
			
			list<FeedItem> feedItems2Create_List = new list<FeedItem>();
			
			if (!relvantLicenses_List.isEmpty()) {
				
				for (ContentVersion contentVersion : contentRecTypeLicenseContentIds_Set) {
					
					for (License__c currentLicense : relvantLicenses_List) {
						
						 if (contentVersion.Product__c.EqualsIgnoreCase(currentLicense.Product_Type__c)) {
							
							 FeedItem newFeeditem = new FeedItem();
							 newFeeditem.parentId = currentLicense.Id;
							 newFeeditem.RelatedRecordId = contentVersion.Id;
							 newFeeditem.Visibility = 'AllUsers';
							 newFeeditem.Type = 'ContentPost';
							 newFeeditem.Body = 'Post with related document body';
							 feedItems2Create_List.add(newFeeditem);
						 }
					}
					
					if (LPNmaes2LicenseProducts_Map.containsKey(contentVersion.Product__c)) {
						
						LPNmaes2LicenseProducts_Map.get(contentVersion.Product__c).Revision__c = contentVersion.Doc_SW_Revision__c;
					}
				}
				
				if (!feedItems2Create_List.isEmpty()) {
					
					try {
						
						insert feedItems2Create_List;
						update LPNmaes2LicenseProducts_Map.values();
					}
					
					catch(Exception e) {
						
						system.debug('ERROR inserting feed Items : ' + e.getMessage());
					}
				}
			}
		}
	}
	
	// This method returns a set of Content Version of Record Type 'License Content' Ids
	private set<ContentVersion> getRelevantContentVersionIds(list<ContentVersion> newContentVersions_List) {
		
		set<ContentVersion> contentRecTypeLicenseContentIds_Set = new set<ContentVersion>();
		
		Id LicenseContentRecType = [SELECT Id FROM RecordType WHERE Name =: 'License Content' and SobjectType =: 'ContentVersion'].Id;
		
		for (ContentVersion newContentVersion : newContentVersions_List) {
			
			if (newContentVersion.RecordTypeId == LicenseContentRecType) {
				
				contentRecTypeLicenseContentIds_Set.add(newContentVersion);
			}
		}
		
		return contentRecTypeLicenseContentIds_Set;
	}
	
	private map<String, License_Product__c> getRelatedLicenseProducts(set<ContentVersion> contentRecTypeLicenseContentIds_Set) {
		
		 map<String, License_Product__c> LPNmaes2LicenseProducts_Map = new map<String, License_Product__c>();
		 
		 set<String> contentProductsNames_Set = new set<String>();
		 
		 for (ContentVersion newContentVersion : contentRecTypeLicenseContentIds_Set) {
		 	
		 	contentProductsNames_Set.add(newContentVersion.Product__c);
		 }
		 
		 list<License_Product__c> license_Products_List = [SELECT Id, Revision__c, Name FROM License_Product__c WHERE Name IN : contentProductsNames_Set];
		 
		 for (License_Product__c licenseProduct : license_Products_List) {
		 	
		 	LPNmaes2LicenseProducts_Map.put(licenseProduct.Name, licenseProduct);
		 }
		 
		 return LPNmaes2LicenseProducts_Map;
	}
	
	// THis method updates the contentVersion field 'Moblie_OPN__c' with the values of OPN__c on contentVersion creation OR update
	public void updateMobileOPNOnOPNUpdate(list<ContentVersion> newContents_List, map<id, ContentVersion> oldVersion_map) {
		
		for (ContentVersion newContent : newContents_List) {
							
			if (oldVersion_map != null) {
				
				if (newContent.OPN__c != oldVersion_map.get(newContent.Id).OPN__c) {
					
					if (newContent.OPN__c != null) {	
						
						newContent.Moblie_OPN__c = newContent.OPN__c.ReplaceAll(',' , ';');
					}
					
					else {
						
						newContent.Moblie_OPN__c = null;
					}
				}
			}
			
			else {
				
				if (newContent.OPN__c != null) {
					
					newContent.Moblie_OPN__c = newContent.OPN__c.ReplaceAll(',' , ';');
				}
			}
		}
	}
	
	 	
	
	public void addTestCoverage() {
		
		Integer i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		
	}
	
	
	public void addTestCoverage2() {
		
		Integer i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		
	}
	
	public void addTestCoverage3() {
		
		Integer i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		i = 1;
		
	}
}