public with sharing class CLS_ObjectCreator {


  


    public FM_Discussion__c createFMDiscussion(Case c)
    {
        FM_Discussion__c fmdiscussion = new FM_Discussion__c();
        fmdiscussion.Case__c = c.Id;
        
        
        return fmdiscussion;
    }
    
    //Create An RMA
    public  RMA__c CreateRMA()
    {
        RMA__c rma = new RMA__c();
        rma.Name__c = 'Test RMA';
        rma.E_Mail__c = 'tst@tst.com';
        rma.Contact_Phone__c = '4536364564';
        
        //billing section
        rma.Bill_Company__c = 'bil Company fld';
        rma.Bill_Contact__c = 'Bil contact fld';
        rma.Bill_Phone__c = '2334131';
        rma.Bill_Company_Address_1__c = 'Test in Test Area';
        rma.Bill_Country__c = 'TST WORLD';
        rma.Bill_City_State__c = 'TST City state';
        rma.Bill_Zip__c = '080980';
        
        
        rma.ShipTo_same_as_BillTo__c = true;
        //ship to will not be filled as the ship to same as bill is true
        
        
        return rma; 
    }  
  
  public Contact CreateContact(Account acc)
  {
    Contact con = new Contact();
    con.firstname= 'first';
    con.lastname = 'last';     
    con.accountid = acc.id;   
    
    return con;                                                
  } 
  
  
   public Task CreateTask()
   {
    Task tsk = new Task();
    tsk.ActivityDate = system.today();
    tsk.status = 'New';
    return tsk;
   }
  
  
  
  public AE_Queue_Manager__c  CreateAEQmanager( )
  {
    AE_Queue_Manager__c QM = new AE_Queue_Manager__c();
    QM.Class__c = 'abcClass';   
    QM.Q_Owner__c = ENV.AE_manager; 
    QM.Sub_Class__c = 'abcSubClass';  
    
    return QM;                                                
  } 
     
     
  public Optional_AE_Owner__c  CreateOptionalAEowner( id AEman ) 
  {
  Optional_AE_Owner__c OptAE = new Optional_AE_Owner__c ();
  OptAE.AE_Queue_Manager__c = AEman;
  OptAE.Owner__c = ENV.AE_manager; 
  
  return OptAE;
  }
  
  
 public Milestone1_Project__c  CreateMPproject( id Acc ) 
  {
  Milestone1_Project__c Proj= new Milestone1_Project__c ();
  Proj.c_Account__c = Acc;
  Proj.name = 'abcProject';
  
  return Proj;
  }
  
   public Milestone1_Milestone__c  CreateMilestone( id Proj ) 
  {
  Milestone1_Milestone__c MS= new Milestone1_Milestone__c ();
  MS.Project__c = proj;
  MS.name = 'abcProjectMilestone';
  
  return MS;
  }
  
  public  Milestone_Map__c  CreateMolestone_Map() 
  {
  Milestone_Map__c mm = new Milestone_Map__c ();
  mm.Task__c = 'test task';
  mm.Task_Order__c= 1;
  mm.Milestone_Order__c = 1;
  mm.Milestone__c = 'Pre-Kickoff';
  mm.project_type__c = 'Cluster Design';
  
  return mm;
  }
  
   public  Project_Member__c  CreateProjectMember(id proj, id user) 
  {
  Project_Member__c pm = new Project_Member__c ();
  pm.project__c = proj;
  pm.project_member__c= user;
   
  return pm;
  }
  
  
  public Lead  CreateLead( )
  {
    Lead ld  = new Lead();
    ld.lastname= 'myLastName';
    ld.firstName = 'myFirstName';   
    ld.email = 'tututu@ytyty.com'; 
    ld.company = 'myCompany';  
    
    return ld;                                                
  } 
  
  public Campaign CreateCampaign()
  {
    Campaign cmp = new Campaign();
    cmp.name= 'first_test_cmp';
    cmp.type = 'Webinar';   
    cmp.score__c = 5;   
    
    return cmp;                                                
  }  
  
  public CampaignMember CreateCampaignMember(Campaign cmp,lead ld )
  {
    CampaignMember cm = new CampaignMember();
    cm.campaignId= cmp.id;
    cm.leadId = ld.id;   
      
    
    return cm;                                                
  } 
  
  
  
  
     
    public Loaner_request__c CreateLoaner()
  {
    Loaner_request__c LR = new Loaner_request__c();
    LR.Project_Name__c = 'first';
    LR.Country__c = 'USA';
    LR.EndCustState__c ='CA';
  
    return LR;                                                
  }
                         
  
    public  Case Create_case(Account acc, Contact con)
    {
       Case c = new Case();
       c.contactid = con.id;
       c.accountid = acc.id;
             
       c.state__c = 'Open';
       
       c.RecordTypeId = '01250000000Dtey';                                                      
       return c;
       }
   
  public  Case CreateRMA_case(Account acc, Contact con, RMA__c rma)
    {
       Case rc = new Case();
       rc.contactid = con.id;
       rc.accountid = acc.id;
       rc.rma_request__c= rma.id;      
       rc.state__c = 'Open';
       
       rc.RecordTypeId = '01250000000Dtey';                                                      
       return rc;
       }
   
     public Portal_Contact__c createPortalContact(Contact con)
     {
        Portal_Contact__c pc = new Portal_Contact__c();
        
        pc.Email__c = Con.Email;
        pc.Send_Note__c = false;
        return pc;
        
     }
     
     public  User CreateUser()
    {
       Profile p = [Select p.Name, p.Id From Profile p Where Name='Standard User' limit 1];
        
       User us = new user();
       
       us.Username = 'qwe@jkg.com';              
       us.LastName = 'testuser';                                                           
       us.Email = 'qwe@jkg.com';   
       us.Alias ='ytst';
       us.CommunityNickname = 'ytstn'; 
       us.TimeZoneSidKey = 'America/New_York';  
       us.LocaleSidKey = 'en_US';
       us.EmailEncodingKey = 'ISO-8859-1';     
       //us.ProfileId = '00e50000000pQX9AAM';
       us.ProfileId = p.Id;
       us.LanguageLocaleKey = 'en_US';
       
       return us;
   }

    public  User CreateCPUser(Contact cont)
    {
               
       User us = new user();
       us.Contactid = cont.id;
       us.Username = 'qwe@jkg.com';              
       us.LastName = 'testuser';                                                           
       us.Email = 'qwe@jkg.com';   
       us.Alias ='ytst';
       us.CommunityNickname = 'ytstn'; 
       us.TimeZoneSidKey = 'America/New_York';  
       us.LocaleSidKey = 'en_US';
       us.EmailEncodingKey = 'ISO-8859-1';     
       us.ProfileId = '00e50000000pQX9AAM';
       us.LanguageLocaleKey = 'en_US';
       us.PortalRole = 'Manager';
       //us.PortalRole = 'test1 Customer user';
       
       return us;
   }


       
         

  public  Asset2__c CreateAsset(Contract2__c con, string name)
    {
       Asset2__c ast = new Asset2__c();
       ast.name = name;
       ast.contract2__c = con.id;
       ast.Asset_Type__c ='Software';
       ast.date__c = system.today();                                   
       return ast;
  }

public  support_item__c CreateSupportItem(id OPN, id SupItem)
    {
       support_item__c si = new support_item__c();
       si.mlxProduct__c = OPN;
       si.support_product__c =supItem;  
          
       return si;
  }

  
  

 
 
    
                                                    
    public  serial_number__c CreateSN( RMA__c rma)
    {
       serial_number__c sn = new serial_number__c();
       sn.name = '1234567';
       sn.rma__c = rma.id;
       sn.serial__c = 'customerSN';
                                                     
       return sn;
    }
                                            
    
    public Product2 createProduct()
    {
        Product2 p = new Product2();
        p.Name = 'Test Product';
        p.Inventory_Item_id__c = '333';
        p.Product_Type1__c = 'HERMON';
        p.partNumber__c = '2323';
        p.Support_Percent_Amount__c = 30;
        
        return p; 
    }
    public Serial_Number__c createSerialNumber(RMA__c rma, Product2 prod)
    {
        Serial_Number__c sn = new Serial_Number__c();
        sn.RMA__c = rma.Id;
        sn.Part_Number__c = prod.Name;
        sn.Problem_Description__c = 'Test problem description field';
        sn.Name = 'testSN';
        sn.serial__c = 'customerSN';
        
        return sn;
    }
    
    public MellanoxSite__c  createMellanoxSite()
    {
        MellanoxSite__c mellanox = new MellanoxSite__c();
        mellanox.Address__c = 'Test Address';
        mellanox.City__c = 'Test City';
        mellanox.Country__c = 'tst country';
        mellanox.Phone__c = '080809';
        mellanox.Zip__c = 'i8i8';
        
        return mellanox;
    }
    public  Account createAccount()
    {
        Account acc = new Account(name = 'test1',support_center__c = 'Global', is_oracle_parent__c = 'Yes', type = 'OEM');
        
        return acc;
    }
    
    public  Opportunity createOpportunity(Account acc)
    {
        system.debug('acc.id: ' + acc.id);
        
        Opportunity Op =  new Opportunity(AccountId=acc.id, Name = 'test opp', RecordTypeID=ENV.map_opportunityRecordTypeTOID.get('End_User_Opportunity'), StageName = 'Forecast', type = 'Direct', CloseDate =Date.Today()+30, Required_Ship_Date__c=Date.today()+20);
        //op.ownerid = '005500000011csI';          
        op.OpportunityCode__c = '122212';  
        
        return op;
    }
     public  Quote createQuote(Opportunity op,ID pbk2id)
    {
        Quote q = new Quote(OpportunityId = op.Id,Name='Test Quate', Pricebook2Id = pbk2id);
        
        
        return q;
    }
    public  QuoteLineItem createQuoteLineItem(Quote q,ID pbkEntryID)
    {
        QuoteLineItem qli = new QuoteLineItem (QuoteId = q.Id, PricebookEntryId = pbkEntryID ,Quantity = 1,UnitPrice = 1500);
        
        return qli;
    }
    public Pricebook2 CreatePriceBook()
    {
        
        Pricebook2 pb = new Pricebook2();
        pb.IsActive = true;
        
        pb.Name = 'TestPB';
        
        return pb;
    }
    public PricebookEntry CreatePriceBook(ID PriceBookId,ID ProductId)
    {
        system.debug('PriceBookId: ' + PriceBookId + ' ProductId : ' +ProductId);
        
        PricebookEntry pb = new PricebookEntry();
        pb.IsActive = true;
        pb.Pricebook2Id = [Select p.IsStandard, p.Id From Pricebook2 p where p.IsStandard=true limit 1].Id;
        
        pb.Product2Id = ProductId;
        pb.UnitPrice = 50;
        pb.UseStandardPrice=false;   
        insert pb;
        
        
        pb = new PricebookEntry();
        pb.IsActive = true;
        pb.Pricebook2Id = PriceBookId;
    
        pb.Product2Id = ProductId;
        pb.UnitPrice = 50;
        pb.UseStandardPrice=false;   
        
        return pb;
     }
          
     
     public Opp_Reg_OPN__c createOppRegOPN(Product2 prod)
     {
        Opp_Reg_OPN__c opRgopn = new Opp_Reg_OPN__c();
        opRgopn.OPN__c = prod.Id;
        opRgopn.OPN_Quantity__c = 10;
        opRgopn.OPN_Requested_Discount__c = 25;
        
        return opRgopn;
     }
     
     
     
     public RMProject__c createRMProject(Case c)
     {
        RMProject__c rmProj = new RMProject__c();
        rmProj.Name = c.Case_Number_callCenter__c;
        rmProj.Project__c ='golan';
        rmProj.RM_Id__c = '909';
        rmProj.Root__c = 'SW';
        
        
        
        return rmProj;
     }
     
     public RmAssignee__c  createRMAssigne()
     {
        RmAssignee__c  rmAssigne = new RmAssignee__c ();
        rmAssigne.Name = 'Test';
        
        
        return rmAssigne;
        
     } 
     
     public RMTracker__c  createRMTracker()
     {
        RMTracker__c  trkr = new RMTracker__c ();
        
        
        
        return trkr;
        
     }
     
     public RMUsers__c  createRMUser()
     {
        RMUsers__c  rmUsr = new RMUsers__c ();
        rmUsr.Name = '909';
        rmUsr.RM_ID__c = '909';
        
        
        return rmUsr;
        
     }
     
      
     public RMPriority__c  createPriority()
     {
        RMPriority__c  pr = new RMPriority__c ();
        pr.SF_Priority__c = 'Fatal,High';
        pr.RM_Id__c = '909';
        pr.RM_Priority__c = 'P1: Critical';
        
        return pr;
        
     }
     
     public Attachment  createAttachment()
     {
        Attachment  att = new Attachment ();
        
        return att;
        
     }
     
     
    
     public AttachmentIntegration__c createAttachIntegration(Case cs)
     {
         AttachmentIntegration__c  AI =  new AttachmentIntegration__c();
         AI.Case_Id__c = cs.id;
        
        return AI;
     }
     
     public Issue_product__c createIssueProduct()
    {
        Issue_product__c prod = new Issue_product__c();
        prod.name = 'MyProduct';
        
        
        return prod;
    }
    
     public Issue_category__c createIssueCategory(Issue_product__c prod)
    {
        Issue_category__c cat = new Issue_category__c();
        cat.product__c = prod.id;
        
        
        return cat;
    }
 
 
  public Issue_profile__c createIssueProfile(Issue_product__c prod)
    {
        Issue_profile__c prof = new Issue_profile__c();
        prof.product__c = prod.id;
        
        
        return prof;
    }
    
  public Release__c createIssueRelease(Issue_product__c prod)
    {
        Release__c rel = new Release__c();
        rel.product__c = prod.id;
        
        
        return rel;
    }  
    
    
    public SFDC_issue__c createIssue(Issue_product__c prod, Issue_category__c cat, Issue_profile__c prof,Release__c Rel)
    {
        SFDC_issue__c issue = new SFDC_issue__c();
        issue.product__c = prod.id;
        issue.issue_category__c = cat.id;
        issue.release__c = rel.name;
        issue.profile_selected__c = prof.id;
        
        return issue;
    }  
 
}