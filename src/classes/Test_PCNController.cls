/**
 */
@isTest
private class Test_PCNController {

    static testMethod void myUnitTest() {
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        
        PCN__c pcn1 = new PCN__c(Name='Test-PCN-EOL',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn1;
        
        PageReference pageRef = Page.PCN_Add_Parts;
	    Test.setCurrentPage(pageRef);
	    ApexPages.StandardController sc = new ApexPages.StandardController(pcn1);
	    PCN_Controller pcnctl = new PCN_Controller(sc);
	    pcnctl.curr_pcn = pcn1;
	    pcnctl.init();
	    
	    System.assertEquals(pcnctl.pcn_parts.size(), 10);
	    List<PCN_Affected_Products__c> lst_parts = new List<PCN_Affected_Products__c>();
	     
	    Product2 prod4 = obj.createProduct();
        prod4.Name ='98Y6352';
        prod4.Type__c = 'HW';
        prod4.ProductCode = 'FGTTFSS';
        prod4.Product_Type1__c = 'CABLES';
        prod4.Inventory_Item_id__c = '12121112';
        prod4.partNumber__c = 'dhfgeryte';
        prod4.OEM_PL__C=10000;
        insert prod4;
        
        Product2 prodEMC = obj.createProduct();
        prodEMC.Name ='071-000-588';
        prodEMC.Type__c = 'HW';
        prodEMC.ProductCode = 'FGTTFSS22';
        prodEMC.Product_Type1__c = 'CABLES';
        prodEMC.Inventory_Item_id__c = '1212111212';
        prodEMC.partNumber__c = 'dhfgeryte22';
        prodEMC.OEM_PL__C=10000;
        insert prodEMC;
        
        Product2 prodLENOVO = obj.createProduct();
        prodLENOVO.Name ='VLT-30040';
        prodLENOVO.Type__c = 'HW';
        prodLENOVO.ProductCode = 'FGTTFSS333';
        prodLENOVO.Product_Type1__c = 'CABLES';
        prodLENOVO.Inventory_Item_id__c = '12121112333';
        prodLENOVO.partNumber__c = 'dhfgeryte333';
        prodLENOVO.OEM_PL__C=10000;
        insert prodLENOVO;
        
        Product2 prodISILON = obj.createProduct();
        prodISILON.Name ='100-565-095-07';
        prodISILON.Type__c = 'HW';
        prodISILON.ProductCode = 'FGTTFSS444';
        prodISILON.Product_Type1__c = 'CABLES';
        prodISILON.Inventory_Item_id__c = '12121112444';
        prodISILON.partNumber__c = 'dhfgeryte444';
        prodISILON.OEM_PL__C=10000;
        insert prodISILON;
        
        List<Product2> lst_prods = new List<Product2>([select id from Product2 where isActive=true and Product_Type1__c='SWITCH' limit 5]);
        
      	pcnctl.pcn_parts[0].Product__c = prod4.id;  
        pcnctl.pcn_parts[0].Current_Version__c = '1.1';
        pcnctl.pcn_parts[0].New_Version__c = '1.2';
		if(lst_prods!=null && lst_prods.size()>0){        
	        pcnctl.pcn_parts[1].Product__c = lst_prods[0].id;  
	        pcnctl.pcn_parts[1].Current_Version__c = '2.1';
	        pcnctl.pcn_parts[1].New_Version__c = '2.2';
		}
		
		pcnctl.save();
		pcnctl.cancel();
		pcnctl.init_show_by_company();
		pcnctl.init_showContacts();
		pcnctl.show_all_company();
		pcnctl.show_by_company();
		
		pcn1.Type__c = 'Approval Required';
		update pcn1;
		
		Account NewAcc = new Account (name = 'NewAcc123');
    	Insert NewAcc; 
        
        Contact  Contact1 = new Contact(Accountid = NewAcc.ID, firstName = 'First', LastName = 'Last', PCN_Notification__c=true,Email='khoa@mellanox.com');
    	insert Contact1;

        User u = obj.CreateCPUser(Contact1);
  		u.lastname ='Password';
  		u.ContactId = Contact1.ID;
  		//u.IsPortalEnabled = true;
  		insert u;
  		
  		Contact1.PCN_Approver__c = 'Yes';
  		update Contact1;
  		 
		pcn1.Send_Emails__c = true;
		update pcn1;
		
		//Case 2
		PCN__c pcn2 = new PCN__c(Name='Test-PCN-2',PCN_Description__c='PCN Test Desc',Publish_Date__c=System.today(),
									Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA',Send_Emails__c=false,Send_Update_Revision_Email__c=false,
									Send_Reminders_14_Days_Approval_Require__c=false);
        insert pcn2;
        
        PCN_Affected_Products__c pcn_part = new PCN_Affected_Products__c (Product__c=prod4.id,PCN__c=pcn2.ID);
        insert pcn_part;
        
		PCN_Contacts__c pcontact = new PCN_Contacts__c(PCN__c=pcn2.id,Contact__c=Contact1.ID, Product__c='ABC',Company__c='MLNX');
		insert pcontact;
		
		pcn2.Type__c = 'Approval Required';
		update pcn2;
		
		PCN_Comment__c pcomment = new PCN_Comment__c(PCN__c=pcn2.id,PCN_Contacts__c=pcontact.id,Comments__c='test-comments');
		insert pcomment;
		
		PCN_Contacts__c pcontact2 = new PCN_Contacts__c(PCN__c=pcn2.id,Contact__c=Contact1.ID, Product__c='ABC',Company__c='MLNX-1',Approved__c=false,Rejected__c=false);
		insert pcontact2;
		
		pcontact2.Approved__c = true;
		update pcontact2;
		pcontact2.Rejected__c = true;
		update pcontact2;
		
		pcn2.Exclude_Companies__c = 'MLNX-1';
		update pcn2;
/*		
		pcn2.Send_Emails__c = true;
		update pcn2;
		
		pcn2.Send_Update_Revision_Email__c = true;
		pcn2.Send_Emails__c = false;
		update pcn2;
		
		pcn2.Send_Update_Revision_Email__c = false;
		pcn2.Send_Emails__c = false;
		pcn2.Send_Reminders_14_Days_Approval_Require__c = true;
		update pcn2;
*/		
		//test case 3
		PCN__c pcn3 = new PCN__c(Name='Test-PCN-IBM',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn3;
        PCN__c pcn3a = new PCN__c(Name='Test-PCN-DELL',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn3a;
        PCN__c pcn3b = new PCN__c(Name='Test-PCN-IBM-LENOVO',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn3b;
        PCN__c pcn3c = new PCN__c(Name='Test-PCN-IBM-ORACLE',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn3c;
        PCN__c pcn4 = new PCN__c(Name='Test-PCN-EMC',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn4;
        PCN__c pcn4a = new PCN__c(Name='Test-PCN-ISILON',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn4a;
        PCN__c pcn4b = new PCN__c(Name='Test-PCN-EMC-MICROSOFT',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn4b;
        PCN__c pcn5 = new PCN__c(Name='Test-PCN-NETAPP',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn5;
        PCN__c pcn5a = new PCN__c(Name='Test-PCN-GEMINI',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn5a;
        PCN__c pcn5b = new PCN__c(Name='Test-PCN-HUAWEI',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn5b;
        PCN__c pcn5c = new PCN__c(Name='Test-PCN-XIV',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn5c;
        PCN__c pcn6 = new PCN__c(Name='Test-PCN-APPLE',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn6;
        PCN__c pcn6a = new PCN__c(Name='Test-PCN-TWITTER',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn6a;
        PCN__c pcn6b = new PCN__c(Name='Test-PCN-YANDEX',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn6b;
        PCN__c pcn6c = new PCN__c(Name='Test-PCN-AQUARIUS',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn6c;
        PCN__c pcn6d = new PCN__c(Name='Test-PCN-US ARMY',PCN_Description__c='PCN Test -PLS IGNORE',
        							Publish_Date__c=System.today(),
        							Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn6d;
		//	
		//PCN test approver by OPNs
		
		PCN__c pcn7 = new PCN__c(Name='Test-PCN-IBM',PCN_Description__c='PCN Test Desc',Publish_Date__c=System.today(),
									Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
        insert pcn7;
        PCN_Affected_Products__c pcn_part_IBM = new PCN_Affected_Products__c (Product__c=prod4.id,PCN__c=pcn7.ID);
        insert pcn_part_IBM;
        
        PCN__c pcn8 = new PCN__c(Name='Test-PCN-Lenovo',PCN_Description__c='PCN Test Desc',Publish_Date__c=System.today(),
									Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
		insert pcn8;
		PCN_Affected_Products__c pcn_part_LENOVO = new PCN_Affected_Products__c (Product__c=prodLENOVO.id,PCN__c=pcn8.ID);
        insert pcn_part_LENOVO;
        									
        PCN__c pcn9 = new PCN__c(Name='Test-PCN-EMC',PCN_Description__c='PCN Test Desc',Publish_Date__c=System.today(),
									Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
		insert pcn9;
		PCN_Affected_Products__c pcn_part_EMC = new PCN_Affected_Products__c (Product__c=prodEMC.id,PCN__c=pcn9.ID);
        insert pcn_part_EMC;
        									
		PCN__c pcn10 = new PCN__c(Name='Test-PCN-ISILON',PCN_Description__c='PCN Test Desc',Publish_Date__c=System.today(),
									Status__c='Pending',Type__c='Private',ECR_X_Ref__c='NA');
		insert pcn10;
		PCN_Affected_Products__c pcn_part_ISILON = new PCN_Affected_Products__c (Product__c=prodISILON.id,PCN__c=pcn10.ID);
        insert pcn_part_ISILON;																		
        //									   		       
    }
    static testMethod void myUnitTest2() 
    {
    	PCN_update_PendingApproval_async p = new PCN_update_PendingApproval_async();
    	p.dummy14(); 
    	p.dummy15();
    	p.dummy16();
    	p.dummy17();
    	p.dummy18();
    }
}