public with sharing class cls_trg_RmCase {
	
	// This method will go over the deleted RM Cases and the Rm Case is of Record Type 'Related RM case'
	// It will update it's parent history ('Related_RM_cases_history__c ') with info about the deleted record
	public void updateParentRMCaseHistoryUponDelteOfRelatedRmCase(list<RmCase__c> oldRmCases_List) {
		
		Id RelatedRMCaseRecTypeId = ENV.RecordTypeIdRMCase_RM_Related_Case;
		
		list<RmCase__c> rmCases2Update_List = new list<RmCase__c>();
		
		for (RmCase__c oldRmCase : oldRmCases_List) {
			
			if (oldRmCase.RecordTypeId == RelatedRMCaseRecTypeId && oldRmCase.Parent_RMCase__c != null) {
				
				RmCase__c parentRmCase = new RmCase__c(Id = oldRmCase.Parent_RMCase__c);
				parentRmCase.Related_RM_cases_history__c  = 'RM case # ' + oldRmCase.Name + ' was removed from current feature request. \n '
				+ 'Link to Redmine Deleted : ' + oldRmCase.RM_Case__c;
				rmCases2Update_List.add(parentRmCase);
			}
		}
		
		if (!rmCases2Update_List.isEmpty()) {
			
			update rmCases2Update_List;    
		}
	}

}