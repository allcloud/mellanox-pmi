public class Quote_Restore_GPS_Products {
	public Quote curr_quote {get;set;}
	List<QuoteLineItem> lst_QuoteLineItemToInsert = new List<QuoteLineItem>();
	List<QuoteLineItem> lst_QuoteLineItemToDelete = new List<QuoteLineItem>();
    QuoteLineItem qline;
    List<Pro_Service_Products__c> proserve_prods = new List<Pro_Service_Products__c>();
	Set<ID> set_ProductIDs = new Set<ID>();
    Set<Id> set_PriceBookID = new Set<Id>();
    
    ID EDR_KICKSTART_ID = '01t50000002uTk3'; //producttion ID
    //ID EDR_KICKSTART_ID = '01tW000000179t2'; //sandbox ID                                                        
	
	public Quote_Restore_GPS_Products (ApexPages.StandardController controller){
		curr_quote = [select id,Switch_Total__c,UFM_Total__c,Node_Count__c,GPS_Required__c,New_Price_Book__c,
						Pricebook2ID,GPS_Got_Deleted__c,isNewGPS__c,EDR_Quote__c 
						from Quote where id = :controller.getRecord().id];
		set_PriceBookID.add(curr_quote.Pricebook2ID);
		set_PriceBookID.add(ENV.PriceBookMap.get(curr_quote.New_Price_Book__c));							
	}
	public pagereference init(){
		if (curr_quote.EDR_Quote__c == true){
			set_ProductIDs.add(EDR_KICKSTART_ID);
		}
		else if(curr_quote.GPS_Required__c == true){
			
			proserve_prods = [Select Min_Nodes__c,Max_Nodes__c,Qty__c,Qty_UFM_Only__c,Products__c,Products_ID__c,
	    							Products_UFM_Only__c,Products_UFM_Only_ID__c,
	    							Min_Amount__c, Max_Amount__c 
	                                From Pro_Service_Products__c p 
	                                where isNewGPS__c = :curr_quote.isNewGPS__c
	                                order by Min_Amount__c];
			for (Pro_Service_Products__c psp : proserve_prods){
	            set_ProductIDs.add(psp.Products_ID__c);
	            set_ProductIDs.add(psp.Products_UFM_Only_ID__c);    
	        }
		}
		
        Map<ID,PricebookEntry> map_PricebookEntry = new Map<ID,PricebookEntry>([select id,Pricebook2ID,Product2ID,UnitPrice from PricebookEntry 
                                                                                where Product2Id in :set_ProductIDs and Pricebook2ID in :set_PriceBookID and isActive=true]);
        Map<ID,Map<ID,PricebookEntry>> map_prodID_PricebookID_PBEntryID = new Map<ID,Map<ID,PricebookEntry>>();
        Map<ID,PricebookEntry> map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
        for (PricebookEntry pbentry : map_PricebookEntry.values()){
            if(map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID)==null){
                map_PricebookID_PBEntryID = new Map<ID,PricebookEntry>();
                map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
                map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
            }
            else {
                map_PricebookID_PBEntryID = map_prodID_PricebookID_PBEntryID.get(pbentry.Product2ID);
                map_PricebookID_PBEntryID.put(pbentry.Pricebook2ID,pbentry);
                map_prodID_PricebookID_PBEntryID.put(pbentry.Product2ID,map_PricebookID_PBEntryID); 
            }
        }
        // this map is used to check for duplicated GPS already existed
		List<QuoteLineItem> lst_GPS_QuoteLineItem = new List<QuoteLineItem>([select id,PricebookentryID,Pricebookentry.Pricebook2Id,QuoteID 
																				from QuoteLineItem 
																				where QuoteID = :curr_quote.ID 
																					and Pricebookentry.Pricebook2Id = :curr_quote.Pricebook2ID]);
        Map<ID,List<QuoteLineItem>> map_QuoteID_Quotelines_GPS = new Map<ID,List<QuoteLineItem>>();
        List<QuoteLineItem> tmp_qlines = new List<QuoteLineItem>();
        for (QuoteLineItem qline : lst_GPS_QuoteLineItem){
        	if(map_QuoteID_Quotelines_GPS.get(qline.QuoteID)==null){
        		tmp_qlines = new List<QuoteLineItem>();
				tmp_qlines.add(qline);
				map_QuoteID_Quotelines_GPS.put(qline.QuoteID,tmp_qlines);				        			
        	}
        	else{
        		tmp_qlines = map_QuoteID_Quotelines_GPS.get(qline.QuoteID);
        		tmp_qlines.add(qline);
				map_QuoteID_Quotelines_GPS.put(qline.QuoteID,tmp_qlines);
        	}
        } 
        //  
        //KN 01-28-2014
        Boolean Switch_amount_exist;
        
        if (curr_quote.Switch_Total__c != null && curr_quote.Switch_Total__c > 0){
			Switch_amount_exist = true;
        }
        else
        	Switch_amount_exist = false;
	        //
		if(curr_quote.EDR_Quote__c == true){
			if(map_prodID_PricebookID_PBEntryID != null && map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID) != null
				&& map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID).get(curr_quote.Pricebook2Id) != null) {
					qline = new QuoteLineItem (QuoteID=curr_quote.ID, 
												PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID).get(curr_quote.Pricebook2Id).ID,
			                                    Quantity=1,
			                                    UnitPrice=map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID).get(curr_quote.Pricebook2Id).UnitPrice,
			                                    New_Price__c=map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID).get(curr_quote.Pricebook2Id).UnitPrice);
			        
			        if(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c) != null 
			        		&& map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)) != null ) {
						qline.UnitPrice=map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice;			        	
						qline.New_Price__c=map_prodID_PricebookID_PBEntryID.get(EDR_KICKSTART_ID).get(Quote_ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice;
			        }
			        lst_QuoteLineItemToInsert.add(qline);  
			                       	
			        if(map_QuoteID_Quotelines_GPS.get(curr_quote.ID) != null){
						for (QuoteLineItem qli : map_QuoteID_Quotelines_GPS.get(curr_quote.ID)){
					    	if (qli.PricebookEntryId == qline.PricebookEntryId)
					        lst_QuoteLineItemToDelete.add(qli);	
					    }
					}
			}					
		}
		else if(curr_quote.GPS_Required__c == true){	        
			for (Pro_Service_Products__c psp : proserve_prods){
	                if (Switch_amount_exist = true){
	                    if (curr_quote.Switch_Total__c != null && curr_quote.Switch_Total__c > psp.Min_Amount__c && curr_quote.Switch_Total__c <= psp.Max_Amount__c){
	                        qline = new QuoteLineItem (QuoteID=curr_quote.ID, PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(curr_quote.Pricebook2Id).ID,
	                                                    Quantity=psp.Qty__c,UnitPrice=map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice,
	                                                    New_Price__c=map_prodID_PricebookID_PBEntryID.get(psp.Products_ID__c).get(ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice);
	                       	lst_QuoteLineItemToInsert.add(qline);  
	                       	
	                       	if(map_QuoteID_Quotelines_GPS.get(curr_quote.ID) != null){
			                	for (QuoteLineItem qli : map_QuoteID_Quotelines_GPS.get(curr_quote.ID)){
			                    	if (qli.PricebookEntryId == qline.PricebookEntryId)
			                        	lst_QuoteLineItemToDelete.add(qli);	
			                	}
			                }
			                
	                       	if (curr_quote.Switch_Total__c != null && curr_quote.Switch_Total__c <= 600000)
	                        	break;                                             
	                    }
	                    
	                }
	                else {
	                    if (curr_quote.UFM_Total__c != null && curr_quote.UFM_Total__c > psp.Min_Amount__c && curr_quote.UFM_Total__c <= psp.Max_Amount__c){    
	                        qline = new QuoteLineItem (QuoteID=curr_quote.ID, PricebookEntryId=map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(curr_quote.Pricebook2Id).ID,
	                                                    Quantity=psp.Qty_UFM_Only__c,UnitPrice=map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice,
	                                                    New_Price__c=map_prodID_PricebookID_PBEntryID.get(psp.Products_UFM_Only_ID__c).get(ENV.PriceBookMap.get(curr_quote.New_Price_Book__c)).UnitPrice);
	                       	lst_QuoteLineItemToInsert.add(qline);
	                       	
	                       	if(map_QuoteID_Quotelines_GPS.get(curr_quote.ID) != null){
			                	for (QuoteLineItem qli : map_QuoteID_Quotelines_GPS.get(curr_quote.ID)){
			                    	if (qli.PricebookEntryId == qline.PricebookEntryId)
			                        	lst_QuoteLineItemToDelete.add(qli);	
			                	}
			                }
			                
	                       	if (curr_quote.UFM_Total__c != null && curr_quote.UFM_Total__c <= 600000)
	                        	break;
	                    }
	                     
	                }
	        } //end for loop
		} //end if(curr_quote.GPS_Required__c == true){
        if(lst_QuoteLineItemToInsert.size()>0)
            insert lst_QuoteLineItemToInsert;
        if(lst_QuoteLineItemToDelete.size()>0)
            delete lst_QuoteLineItemToDelete;
            
		curr_quote.GPS_Got_Deleted__c = '';
		update curr_quote;
		            
		PageReference pg = new PageReference('/'  + curr_quote.ID);
        pg.setRedirect(true);
        return pg;            
	}
}