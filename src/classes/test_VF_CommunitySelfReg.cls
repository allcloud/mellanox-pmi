@isTest(seeAllData=true)
public with sharing class test_VF_CommunitySelfReg {
	
	/*
	// This test class tests the functionality of 'VF_CommunitySelfReg'
	 static testMethod void test_Page()
	 {
	 
	    Contact c = new Contact();
        c.AccountId = '0015000000K0e8F';
        c.FirstName = 'Elad';
        c.LastName = 'Test';
        c.Title = 'Test';
        c.Email = 'Test@Somewhere.com';
        c.CRM_Content_Permissions__c = 'Other';
        c.Contact_Type__c = 'standard';
        
        insert c;
        
	     User u = [SELECT Id, ProfileId, Email FROM User WHERE ProfileId =: '00e50000000pQX9' and isActive = true limit 1];
	 	 
	     Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(c);
         VF_CommunitySelfReg vf = new VF_CommunitySelfReg(teststandard);
         vf.countryName = 'Israel';
         vf.contactTypeStr = 'Mellanox Silicon Design-In customer';
         vf.needAccessStr = 'Yes';
	     vf.registerUser();
	     u.Email = 'xxx@Somewhere.com';
	     update u;
	     c.Contact_Type__c = 'Mellanox Silicon Design-In customer';
	     c.Deny_Access__c = 'Self Service Portal Access';
	     update c;
	     vf.registerUser();
	     c.Email = 'Test3212@Somewe.com';
	     vf.registerUser();
	     vf.getItems();
	     vf.getTypes();
	     vf.getStates();
	     vf.getNeedAccess();
	     vf.insertRecord(c);
	     vf.countryName = 'United States';
	 	 vf.registerUser();
	 	 vf.countryName = 'China';
	 	 vf.registerUser();
	 	 vf.countryName = 'Japan';
	 	 vf.registerUser();
	 	 vf.countryName = 'India';
	 	 vf.registerUser();
	 	 vf.countryName = 'Taiwan';
	 	 vf.registerUser();
	 	 c.Email = u.Email;
	 	 vf.registerUser();
	 
	 }
     
     */
     
     /*
     static testMethod void test_VF_PortalSelfReg()
	 {
	 
	    Contact c = new Contact();
        c.AccountId = '0015000000K0e8F';
        c.FirstName = 'Elad';
        c.LastName = 'Test';
        c.Title = 'Test';
        c.Email = 'Test@Somewhere.com';
        c.CRM_Content_Permissions__c = 'Other';
        c.Contact_Type__c = 'standard';
        
        insert c;
        
	     User u = [SELECT Id, ProfileId, Email FROM User WHERE ProfileId =: '00e50000001FhsD' and isActive = true limit 1];
	 	 
	     Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(c);
         VF_PortalSelfReg vf = new VF_PortalSelfReg(teststandard);
         vf.countryName = 'Israel';
         vf.contactTypeStr = 'Mellanox Silicon Design-In customer';
         vf.needAccessStr = 'Yes';
	     vf.registerUser();
	     u.Email = 'xxx@Somewhere.com';
	     update u;
	     c.Contact_Type__c = 'Mellanox Silicon Design-In customer';
	     c.Deny_Access__c = 'Self Service Portal Access';
	     update c;
	     vf.registerUser();
	     c.Email = 'Test3212@Somewe.com';
	     vf.registerUser();
	     vf.getItems();
	     vf.getTypes();
	     vf.getStates();
	     vf.getNeedAccess();
	     vf.insertRecord(c);
	     vf.countryName = 'United States';
	 	 vf.registerUser();
	 	 vf.countryName = 'China';
	 	 vf.registerUser();
	 	 vf.countryName = 'Japan';
	 	 vf.registerUser();
	 	 vf.countryName = 'India';
	 	 vf.registerUser();
	 	 vf.countryName = 'Taiwan';
	 	 vf.registerUser();
	 	 c.Email = u.Email;
	 	 vf.registerUser();
	 
	 }
     */

}