public with sharing class cls_trg_Attachment {
	
	// This method will check the related RMA field 'Attachment_Exists_on_RMA__c' true,
	// if an attaccment is attached to an RMA
	
	private  set<Id> parentCasesIds_Set;
    private  String source_Str;
    private  Set<Id> parentCasesIdsPublic_Set;
    private  boolean isCustomerAttachment = false;
    private boolean isAttPublic;
    
	public void checkAttExistsOnRMATrue(list<Attachment> attNew_List)
	{
		list<RMA__c> rmaToUpdate_List = new list<RMA__c>();
 
 		for (Attachment att : attNew_List)
		{   
			String rmaIdStr = String.valueof(att.parentId);
			if (rmaIdStr.startsWith('a0E'))
			{     
				RMA__c rma = new RMA__c(Id = att.ParentId);
				rma.Attachment_Exists_on_RMA__c = true;
				rmaToUpdate_List.add(rma);
			}
		}
		
		if (!rmaToUpdate_List.isEmpty())
		{     
			update rmaToUpdate_List;
		}
	}
	 
	// This method will check the related RMA field 'Attachment_Exists_on_RMA__c' false,
	// if an attaccment is deleted and other attachment exists on the related RMA  
	public void checkAttExistsOnRMAFalse(list<Attachment> attOld_List) 
	{
		list<RMA__c> rmaToUpdate_List = new list<RMA__c>();
		set<id> rmasIds_Set = new set<Id>();
		
		for (Attachment att : attOld_List)
		{
			String rmaIdStr = String.valueof(att.parentId);
			if (rmaIdStr.startsWith('a0E'))
			{
				rmasIds_Set.add(att.parentId);
			}
		}
		
		if (!rmasIds_Set.isEmpty())
		{
			list<RMA__c> relatedRmasWithAtt_list = DAL_RMA.getRMAsAndAttachments(rmasIds_Set);
			
			if (!relatedRmasWithAtt_list.isEmpty())
			{  
				Boolean otherAttexists = false;
				for (RMA__c rma : relatedRmasWithAtt_list)
				{
					list<Attachment> relatedAtt_List = rma.Attachments;
					
					if (!relatedAtt_List.isEmpty() && relatedAtt_List.size() > 1)
					{
						rma.Attachment_Exists_on_RMA__c = true;
					}
					
					else
					{
						rma.Attachment_Exists_on_RMA__c = false;
					}
				}
				
				update relatedRmasWithAtt_list;
			}
		}
	}
	
	
    public void sortAttachmentsPrivatOrPublic(list<Attachment> newAttachments_List, map<Id, Attachment> relatedAttachments_Map) {
        
        set<Id> parentCasesIds_Set = new set<Id>();
        
        system.debug('newAttachments_List : ' + newAttachments_List);
        system.debug('newAttachments_List size : ' + newAttachments_List.size());
        
        for (Attachment newAtt : newAttachments_List) {
        	
        	if (String.valueOf(newAtt.ParentId).StartsWith('500')) {	
        		
        		parentCasesIds_Set.add(newAtt.ParentId);
        	}
        }
        
        list<EmailMessage>  newEmailMessages_List;
        
         list<Attachment> attachments2Create_List = new list<Attachment>();
	     list<Attachment> attachmentsDelete_List = new list<Attachment>();
         list<Case> casesWithAttachments_List;
         integer attachments_ListSize = newAttachments_List.size();
         list<AttachmentIntegration__c> attIntegration2Create_List;
         
         if (!parentCasesIds_Set.isEmpty()) {  

        	newEmailMessages_List =  getRelatedEmailMessages(parentCasesIds_Set);
        
	        casesWithAttachments_List = fillUpListOfCasesWithAttachments(newEmailMessages_List, attachments_ListSize);
	        
	        attIntegration2Create_List = new list<AttachmentIntegration__c>();
	        
	        if (!casesWithAttachments_List.IsEmpty()) {
	            
	            for (Case currentCase : casesWithAttachments_List) {
	                
	                PrivateAttachment__c relatedPrivateAtt;
	                if (currentCase.Private_Attachment__r != null && !currentCase.Private_Attachment__r.isEmpty()) {
	                    
	                    relatedPrivateAtt = currentCase.Private_Attachment__r[0];
	                    
	                    for (Attachment relatedAttach : currentCase.Attachments) {
	                    
		                    Attachment relatedAttWithParams = relatedAttachments_Map.get(relatedAttach.Id);
		                    Attachment newAttachment = new Attachment();
		                    newAttachment.Name = relatedAttWithParams.Name;
		                    newAttachment.Body = relatedAttWithParams.Body;
		                    newAttachment.Description = relatedAttWithParams.Description;
		                    
		                    if (!isAttPublic) {
		                        
		                        newAttachment.ParentId = relatedPrivateAtt.Id;
		                    }
		                    
		                    else {
		                        
		                        newAttachment.ParentId = currentCase.Id;
		                    }
		                    attachmentsDelete_List.add(relatedAttWithParams);
		                    attachments2Create_List.add(newAttachment);
		                }
	                }
	            }
	        }
        }
        
        if (!attachments2Create_List.isEmpty()) {
          
            insert attachments2Create_List;
         
             if (!attachmentsDelete_List.isEmpty() && !isAttPublic) {
            
                list<Attachment> att2DeleteClone = new list<Attachment>();
                
                for (Attachment att2Delete : attachmentsDelete_List) {
                	
                	Attachment att = new Attachment(Id = att2Delete.Id);
                	att2DeleteClone.add(att);
                }	
                	delete att2DeleteClone;
            }
        }
        
        if (casesWithAttachments_List != null && !casesWithAttachments_List.isEmpty())  {   
	        
	        set<Case> cases_Set = new set<Case>();
	        cases_Set.addAll(casesWithAttachments_List);
	        map<Id, Case> relatedCases_Map =  new map<Id, Case> ([SELECT Id, Last_Email_Created_By__c FROM Case WHERE Id IN : cases_Set]);
	            
	        if(attachments2Create_List.isEmpty()) {
	        	
	        	attachments2Create_List.addAll(newAttachments_List);
	        }
	        
	         for (Attachment newAttachment : attachments2Create_List) {
	        
	            AttachmentIntegration__c attIntegration = new AttachmentIntegration__c();
	            attIntegration.Name = newAttachment.Name;
	            attIntegration.Attachment_Id__c = newAttachment.Id;
	            attIntegration.Source__c = source_Str;
	            
	            for (Case currentCase : cases_Set) {
	                
	                if (currentCase.Next_Attachment_Integration_Number__c == null || currentCase.Next_Attachment_Integration_Number__c == 0) {
	                    
	                    attIntegration.Attachment_Number__c = 1;
	                }
	                
	                else {
	                    
	                    attIntegration.Attachment_Number__c = currentCase.Next_Attachment_Integration_Number__c;
	                }
	                
	                if (currentCase.Next_Attachment_Integration_Number__c == null) {
	                	
	                	currentCase.Next_Attachment_Integration_Number__c = 0;
	                }
	                	currentCase.Next_Attachment_Integration_Number__c ++;
	                                   
	                
	                attIntegration.Case_Id__c = currentCase.Id;
	                 attIntegration.Creator_name__c = relatedCases_Map.get(currentCase.Id).Last_Email_Created_By__c;
	                for (PrivateAttachment__c relatedPrivateAtt : currentCase.Private_Attachment__r) {
	                    
	                    if (relatedPrivateAtt.Id == newAttachment.ParentId) {
	                        
	                        attIntegration.Case_Id__c = relatedPrivateAtt.Case__c;
	                        //attIntegration.Attachment_Number__c --; 
	                        //currentCase.Next_Attachment_Integration_Number__c --;
	                    }
	                }
	            }
	            
	            attIntegration2Create_List.add(attIntegration);
	        }
	        
	        if (!attIntegration2Create_List.isEmpty()) {
	            
	            insert attIntegration2Create_List;
	        }
        
	        if (!casesWithAttachments_List.isEmpty()) {
	            
	            update casesWithAttachments_List;
	        }
        }
    }
    
    
    // This method will return a list of Cases with their related Attachments and Private Attachments
    private list<Case> fillUpListOfCasesWithAttachments(list<EmailMessage> newEmailMessages_List, integer attachments_ListSize) {
        
        parentCasesIds_Set = new set<Id>();
        parentCasesIdsPublic_Set = new set<Id>();
        list<Case> casesWithAttachments_List = new list<Case>();
        
        for (EmailMessage email : newEmailMessages_List) {   
            
            String domainName = email.FromAddress.substring(email.FromAddress.indexOf('@') + 1);
            
            if ((email.Subject != null && !email.Subject.containsIgnoreCase('external'))) {
               
                if (email.Incoming && domainName.contains('mellanox')) {
                    
                    parentCasesIds_Set.add(email.ParentId);
                    isAttPublic = false;
                    system.debug('inside 238');
                }
                
                else {
                    
                    parentCasesIdsPublic_Set.add(email.ParentId);
                    isAttPublic = true;
                    system.debug('inside 245');
                }
            }
            
            else {
                    
                    parentCasesIdsPublic_Set.add(email.ParentId);
                    isAttPublic = true;
                    system.debug('inside 253');
                }
            
            if (email.Incoming && domainName.contains('mellanox')) {
                
                source_Str = 'Mellanox Email';
            }
            
            else {
                
                source_Str = 'Customer Email';
                isCustomerAttachment = true;
            }
        }
        
        if (!parentCasesIds_Set.isEmpty()) {
            
             
            casesWithAttachments_List = [SELECT Id, Next_Attachment_Integration_Number__c, (SELECT Id FROM Attachments order by createdDate desc LIMIT : attachments_ListSize), (SELECT Id, Case__c FROM Private_Attachment__r) FROM Case WHERE Id IN : parentCasesIds_Set];
        }
        
        else {
            
            casesWithAttachments_List = [SELECT Id, Next_Attachment_Integration_Number__c, (SELECT Id FROM Attachments order by createdDate desc limit : attachments_ListSize) FROM Case WHERE Id IN : parentCasesIdsPublic_Set];
        }
        
        return casesWithAttachments_List;
    }
    
    
    
    private  map<Id, Attachment> fillUpMapOfrelatedAttachments(list<Case> casesWithAttachments_List) {
        
        map<Id, Attachment> relatedAttachments_Map;
        
        if (!parentCasesIds_Set.isEmpty()) {
            
            relatedAttachments_Map = new map<Id, Attachment>([SELECT Id, ParentId, Name, Body, Description FROM Attachment WHERE ParentId IN : parentCasesIds_Set]);
        }
        
        else {
            
            relatedAttachments_Map = new map<Id, Attachment>([SELECT Id, ParentId, Name, Body, Description FROM Attachment WHERE ParentId IN : parentCasesIdsPublic_Set]);
        }
        
        return relatedAttachments_Map;
    }
    
    
	
	private list<EmailMessage> getRelatedEmailMessages(set<Id> parentCasesIds_Set) {
		
		list<EmailMessage> newEmailMessages = new list<EmailMessage>();
		list<Case> cases_List = [SELECT Id, (SELECT Id, Incoming, Subject, ParentId, FromName, FromAddress FROM EmailMessages 
											 WHERE Incoming = true order by createdDate desc limit 1) FROM Case WHERE Id IN : parentCasesIds_Set];
		
		if (!cases_List.isEMpty()) {
			
			for (Case currentCase : cases_List) {
												 	
				if (!currentCase.EmailMessages.isEmpty()) {	
					
					newEmailMessages.add(currentCase.EmailMessages[0]);	
				}
			}
		}
		
		return 	newEmailMessages;
	}
}