public with sharing class VF_AE_Queue
{
    
    
    public boolean isNew {get;set;} //0 is new, not 0 is edit
    public Id theCaseId {get;set;}
    public Case editCase {get;set;}
    public String EscalationSelected {get;set;}
    public Case currCase {get;set;}
    private User escUser{get;set;}
    private set<id> AEPermissionsGroupUsersIds_Set {get;set;}
    
    public map<ID, AE_Queue_Manager__c> map_ProjIdToProj ;
    //   public map<ID, Milestone1_Project__c> map_ProjIdToPMProj ;
    //   public map<string, Milestone1_Project__c> map_ProjNameToPMProj ;
    public map<ID, AE_Queue_Manager__c> map_ProjSelectedIdToSubProj;
    public map<String, AE_Queue_Manager__c> map_Name_To_Proj ;
    public map<ID, Optional_AE_owner__c> map_OptionalOwners;
    public map<ID, Optional_PM_owner__c> map_OptionalPMOwners;
    
    List<CaseTeamMember> AllTeamMembers = new  List<CaseTeamMember>();
    List<CaseTeamMember> TeamMembersToInsert = new  List<CaseTeamMember>();
    Set<Id> SetAllTeamMemberIds = new  Set<Id>();
    
    public List<SelectOption> projectsAEList {get;set;}
    public List<SelectOption> projectsPMList {get;set;}
    public String ProjectSelected {get;set;}
    public String ClassSelected {get;set;}
    //public List<SelectOption> projectsList {get;set;}
    public List<SelectOption> ClassList {get;set;}
    public Id generalProjectId {get;set;}
    
    public List<SelectOption> SubProjectsList_asPerProjSelected {get;set;}
    public String SubProjectSelected {get;set;}
    public Boolean  AEcase {get;set;}
    public Boolean  PMcase {get;set;}
    public Boolean  POCcase {get;set;}
    public Boolean  BDcase {get;set;}
    
    
    //Validations Flad
    public boolean projFilled {get;set;}
    public boolean SubprojFilled {get;set;}
    public boolean trackerFilled {get;set;}
    public boolean errMsg {get;set;}
    public boolean errMsgAE {get;set;}
    public boolean errMsgAEPermission {get;set;}
    public boolean errMsgPMmissing {get;set;}
    public boolean successSave {get;set;}
    public boolean existCase {get;set;}
    public boolean makeUnMakeAssign {get;set;}
    public boolean showAEOwnerField{get;set;}
    public string escalationOwner{get;set;}
    public boolean showProject{get;set;}
    
    //KN added
    public boolean errMsgPMescalationfrom_Openstate {get;set;}
    public boolean errMsg_Assignee_not_AEQueue {get;set;}
    //
    
    public VF_AE_Queue(ApexPages.StandardController controller)
    {
        //as there is an issue with the IE9 and rerender in visual force this will make the IE 9 react as IE8
        // Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
        
        theCaseId = Apexpages.currentPage().getParameters().get('id');
        system.debug('the case id1 is : ' + theCaseId);
        showAEOwnerField = true;
        showProject = false;
        generalProjectId = [SELECT Id FROM Milestone1_Project__c WHERE Name =: 'General Project'].Id;
        list<GroupMember> AEPermissionsGroupMembers_List = [Select Id, UserOrGroupId, GroupId From GroupMember WHERE Group.Name = 'AE Permissions'];
        AEPermissionsGroupUsersIds_Set = new set<Id>();
        
        for (GroupMember member : AEPermissionsGroupMembers_List) {
        	
        	AEPermissionsGroupUsersIds_Set.add(member.UserOrGroupId);
        }
        
        projFilled  = true;
        SubprojFilled = true;
        errMsg = false;
        errMsgAE = false;
        errMsgAEPermission =false;
        errMsgPMmissing = false;
        successSave = false ;
        errMsgPMescalationfrom_Openstate = false;
        errMsg_Assignee_not_AEQueue = false;
        
        ProjectSelected         =   '--None--';
        ClassSelected         =   '--None--';
        SubProjectSelected      =   '--None--';
        AEcase = false;
        PMcase = false;
        POCcase = false;
        BDcase = false;
        
        
        // getting the case information
        if(theCaseId != NULL)
        {
            editCase = new Case();
            editCase = [Select  c.priority, c.AE_PM_Escalated__c,c.AE_Class__c, c.AE_Sub_Class__c, c.Escalation_Q_owner__c,c.Escalation_Q_Owner2__c,
            c.Requested_Delivery_Date__c, c.state__c, c.assignee__c, c.Optional_AE_owners__c, c.Escalated_AE_Time__c, c.AE_manager_email__c, 
            c.AccountID,c.AE_engineer__c,c.Account.PM_Owner__c, c.PM_owner__c, c.Related_Project__c, c.Related_Project__r.OwnerId, c.Related_Project__r.Name
            From case c WHERE c.Id =: theCaseId];
        }
        
        editCase.Requested_Delivery_Date__c = null;
        if (editCase.Assignee__c != '0055000000146q2')
            errMsg_Assignee_not_AEQueue = true;
        
        classList = getAEprojectsList();
        
        system.debug('the case id is : ' + theCaseId);
        
        if(editCase.AE_PM_Escalated__c != 'PM') {
            
            AEcase = true;
            POCcase = true;
        }
        
        else {
            
            POCcase = false;
            AEcase = false;
            PMcase = true;
            BDcase = false;
            errMsgAEPermission = AEassigneePermissions();
            errMsgPMmissing =PMownerMissing();
        }
        
        SubProjectsList_asPerProjSelected = new List<SelectOption>();
        errMsg = allFieldsFilled();
        
    }
    
    
    
    public List<SelectOption> getEscalationGroup() {
        
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Case.AE_PM_Escalated__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple) {
            
            if(f.getValue() != 'PT') {
                
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }
        return options;
    }
    
    
    
    
    
    //-------------------------------------------------------------
    //get all the classes and sub-classes  in the system
    public List<SelectOption> getAEprojectsList()
    {
        system.debug('START: getprojectsList ');
        map_ProjIdToProj = new map<ID,AE_Queue_Manager__c>([Select  r.Name,r.Class__c From AE_Queue_Manager__c r order by r.class__c ASC ]);
        
        if(map_ProjIdToProj != null && map_ProjIdToProj.size() > 0)
        {
            Set<String> ProjectSet = new Set<String>();
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--None--','--None--'));
            
            for( AE_Queue_Manager__c  proj : map_ProjIdToProj.values())
            {
                if(!ProjectSet.contains(proj.class__c))
                {
                    options.add(new SelectOption(proj.class__c, proj.class__c));
                    ProjectSet.add(proj.class__c);
                }
                
            }
            system.debug('START: getprojectsList End with list full ');
            
            errMsg = allFieldsFilled();
            return options;
            
        }else
        {
            errMsg=allFieldsFilled();
            system.debug('START: getprojectsList End with Null');
            return null;
        }
    }
    
    
    //get the subclass as per the selected class
    public void projectsListProcess()
    {
        system.debug('START: projectsListProcess ');
        system.debug('The selected value is ProjectSelected : ' + ProjectSelected);
        //system.debug('map_ProjIdToProj : ' + map_ProjIdToProj.get(ProjectSelected));
        
        
        //KN changed 6-24-13
        //map_ProjSelectedIdToSubProj = new map<ID,AE_Queue_Manager__c>([Select r.id, r.Name,r.Class__c, r.Sub_Class__c, r.Q_Owner__c  From AE_Queue_Manager__c r
        //                                                        Where r.class__c  =:ClassSelected order by r.class__c ASC]);
        map_ProjSelectedIdToSubProj = new map<ID,AE_Queue_Manager__c>([Select r.id, r.Name,r.Class__c, r.Sub_Class__c, r.Q_Owner__c  From AE_Queue_Manager__c r
        Where r.class__c  =:ClassSelected order by r.Sub_Class__c ASC]);
        
        System.debug('... KN map_ProjSelectedIdToSubProj==' + map_ProjSelectedIdToSubProj);
        if(map_ProjSelectedIdToSubProj != null && map_ProjSelectedIdToSubProj.size() > 0)
        {
            List<SelectOption> SubProjectsList_asPerProjSelectedtmp = new List<SelectOption>();
            SubProjectsList_asPerProjSelected = new List<SelectOption>();
            map_Name_To_Proj = new map<String, AE_Queue_Manager__c>();
            
            
            for(AE_Queue_Manager__c subProj :map_ProjSelectedIdToSubProj.values())
            {
                //the id of the value is the RM Project id and not the record id
                if(subProj.Sub_Class__c != null && subProj.Class__c != null )
                {
                    SubProjectsList_asPerProjSelectedtmp.add(new SelectOption(subProj.Sub_Class__c, subProj.Sub_Class__c));
                    map_Name_To_Proj.put(subProj.Sub_Class__c,subProj);
                }
                
            }
            // Fix Elad bug, select Class = Project 2 does NOT populate sub_class, SortOptionList does NOT work
            //KN changed 6-24-13
            //SubProjectsList_asPerProjSelected =SubProjectsList_asPerProjSelectedtmp;
            SubProjectsList_asPerProjSelected =SortOptionList (SubProjectsList_asPerProjSelectedtmp);
            //KN changed 6-24-13
            //SubProjectsList_asPerProjSelected[0] =new SelectOption('--None--','--None--');
            SubProjectsList_asPerProjSelected.add(0,new SelectOption('--None--','--None--'));
            
        }
        System.debug('... KN SubProjectsList_asPerProjSelected==' + SubProjectsList_asPerProjSelected);
        //SubProjectsList_asPerProjSelected
        errMsg = allFieldsFilled();
        
        system.debug('END: projectsListProcess ');
        
    }
    
    //-----------------------------------------------------------------
    public void SubProjSelectedFunc()
    {
        
        if(SubProjectSelected== '--None--')
        {
            //SubProjectSelected = null;
            escalationOwner = '';
        }
        errMsg = allFieldsFilled();
        
        if (SubProjectSelected != '--None--')
        {
            escUser = [SELECT Name FROM User Where Id =: map_Name_To_Proj.get(SubProjectSelected).Q_owner__c];
            system.debug('escUser Name : ' + escUser.Name);
            escalationOwner = escUser.Name;
        }
    }
    
    //------------------------------------------------------------------
    
    public void EscalationGroupFunc()
    {
        if( EscalationSelected.EqualsIgnoreCase('PM') )
        {
            //KN added per incident INC0026427 - Case cannot moves to 'Assigned AE' from 'Open' unless it has to be 'Assigned' first
            if (editCase.state__c == 'Open')
                errMsgPMescalationfrom_Openstate = true;
            editCase.Requested_Delivery_Date__c = null;
            //
            AEcase = false;
            PMcase = true;
            POCcase = false;
            showAEOwnerField = false;
            showProject = true;
            errMsgAEPermission = AEassigneePermissions();   
            // projectsList = getPMprojectsList();
            
        }
        
       else {
            
            POCcase = true;
            AEcase = true;
            PMcase = false;
            errMsgAEPermission =false;
            errMsgPMmissing = false;
            showAEOwnerField = true;
            showProject = false;
            
            if ( !EscalationSelected.EqualsIgnoreCase('AE') && !EscalationSelected.EqualsIgnoreCase('POC'))
            {
                showAEOwnerField = false;
            }
            
            if ( EscalationSelected.EqualsIgnoreCase('BD') ) {
                
                POCcase = false;
                BDCase = true;
                AEcase = false;
                
            }
        }
        
        errMsg = allFieldsFilled();
        
    }
    
    
    public void ProjectSelectedFunc()
    {
        ProjectSelected = editCase.Related_Project__c;
        errMsg = allFieldsFilled();
        errMsgPMmissing = PMownerMissing();
        
        if (!checkIfProjectOwnerInAEPublicGroup()) {
        	
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Owner of the project is not permitted to be assigned as AE Engineer, please select a different project'));
        }
        
        
    }
    public void PriorityFunc()
    {
        
        errMsg = allFieldsFilled();
        
    }
    
    
    public void ReqDDFunc()
    {
        
        errMsg = allFieldsFilled();
        
    }
    
    public void SaveCaseUpdate()
    {
        
        errMsg = false;
        if(SubProjectSelected == '--None--')
        SubProjectSelected = null;
        
        system.debug('SubProjectSelected : ' + SubProjectSelected);
        
        if(editCase.Account.PM_Owner__c != NULL) {
        	
        	if (editCase.Related_Project__c != null && editCase.Related_Project__c != generalProjectId) {    
        		
        		Id selectProjectOwnerId = [SELECT Id, OwnerId FROM Milestone1_Project__c WHERE Id =: editCase.Related_Project__c].OwnerId;
        		editCase.PM_owner__c = selectProjectOwnerId;
        	}	
        		
        	else {	
        		
        		editCase.PM_owner__c = editCase.Account.PM_Owner__c;
        		
        	}
    	}
    	
         
        if(PMcase)
        {
            
            if( editCase.Account.PM_Owner__c != NULL)
            {
                
                editCase.AE_Class__c = map_Name_To_Proj.get(SubProjectSelected).class__c;
                editCase.AE_Sub_Class__c = map_Name_To_Proj.get(SubProjectSelected).sub_class__c;
                editCase.Escalation_Q_Owner__c = editCase.PM_Owner__c;
                editCase.Escalation_Q_Owner2__c = null;
                editCase.Optional_AE_Owners__c = '';
                editCase.state__c = 'Assigned AE';
                editCase.assignee__c = editCase.PM_owner__c;
                editCase.AE_engineer__c = editCase.PM_owner__c;
                editCase.Escalated_AE_Time__c = system.now();
                
                
                AllTeamMembers = new  List<CaseTeamMember>();
                TeamMembersToInsert = new  List<CaseTeamMember>();
                SetAllTeamMemberIds = new  Set<Id>();
                
                map_OptionalPMOwners = new map<ID,Optional_PM_Owner__c>( [select id,Owner__c, Owner__r.name,Owner__r.IsActive FROM Optional_PM_Owner__c WHERE Account__c = : editCase.AccountId and Owner__r.IsActive=true]);
                AllTeamMembers = [Select MemberId  From CaseTeamMember Where ParentId = :theCaseId];
                
                for(CaseTeamMember CTM: AllTeamMembers)
                { SetAllTeamMemberIds.add(CTM.MemberId );}
                
                
                for( Optional_PM_Owner__c Own:map_OptionalPMOwners.values())   //Checking if the user already exist in CaseTeam
                {
                    if(SetAllTeamMemberIds.isempty() ||  !SetAllTeamMemberIds.contains(Own.Owner__c))
                    {
                        CaseTeamMember ctmNew = new CaseTeamMember();
                        ctmNew.ParentId = theCaseId;
                        ctmNew.MemberId = Own.Owner__c;
                        ctmNew.TeamRoleId = ENV.teamRoleMap.get('PM Owner');
                        TeamMembersToInsert .add(ctmNew);
                        
                        if(editCase.Optional_AE_owners__c == NULL)
                        {editCase.Optional_AE_owners__c  = Own.Owner__r.name;}
                        else { editCase.Optional_AE_owners__c  =editCase.Optional_AE_owners__c +', '+ Own.Owner__r.name;}
                    }
                }
                if(  ! TeamMembersToInsert.isempty())
                { insert(TeamMembersToInsert); }
                
                
            }
        }
        
        else if(AEcase || BDCase || POCcase) {
            
            if( map_name_To_Proj.containskey(SubProjectSelected) ) {
                
                editCase.AE_Class__c = map_Name_To_Proj.get(SubProjectSelected).class__c;
                editCase.AE_Sub_Class__c = map_Name_To_Proj.get(SubProjectSelected).sub_class__c;
                editCase.Escalation_Q_Owner__c = escUser.Id;
                editCase.state__c = 'Open AE';
                editCase.assignee__c = ENV.AEQueueCP;
                editCase.Escalated_AE_Time__c = system.now();
                
                if (BDCase) {
                    
                    //editCase.AE_PM_Escalated__c = 'BD';
                    editCase.State__c = 'Assigned AE';
                    
                    /*********   FOR SANDBOX   **************/
                    //  editCase.Assignee__c = '005500000013kV4';
                    //  editCase.Escalation_Q_Owner__c = '005500000013kV4';
                    //  editCase.AE_Engineer__c = '005500000013kV4';
                    /*********   FOR SANDBOX   **************/
                    
                    /*********   FOR PRODUCTION   **************/
                    editCase.Assignee__c = '005500000033owD';
                    editCase.Escalation_Q_Owner__c = '005500000033owD';
                    editCase.AE_Engineer__c = '005500000033owD';
                    /*********   FOR PRODUCTION   **************/
                }
                
                // adding Optional owner to case team
                List<CaseTeamMember> AllTeamMembers = new  List<CaseTeamMember>();
                List<CaseTeamMember> TeamMembersToInsert = new  List<CaseTeamMember>();
                Set<Id> SetAllTeamMemberIds = new  Set<Id>();
                
                map_OptionalOwners = new map<ID,Optional_AE_Owner__c>( [select id,Owner__c, Owner__r.name,Owner__r.IsActive FROM Optional_AE_Owner__c WHERE AE_Queue_Manager__c = :  map_Name_To_Proj.get(SubProjectSelected).id and Owner__r.IsActive=true]);
                
                for( Optional_AE_Owner__c Own : map_OptionalOwners.values()) {  //Checking if the user already exist in CaseTeam
                    
                    if(editCase.Optional_AE_owners__c == NULL) {
                        
                        editCase.Optional_AE_owners__c  = Own.Owner__r.name;
                    }
                    
                    else {
                        
                        editCase.Optional_AE_owners__c  =editCase.Optional_AE_owners__c +', '+ Own.Owner__r.name;
                    }
                }
            }
        }
        
        
        editCase.AE_PM_Escalated__c = EscalationSelected;
        
        try {
            
            update editCase;
        }
        
        catch(Exception e) {
            
            errMsg = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The case could not be updated : '+ e.getmessage()));
        }
        
        if(!allFieldsFilled()) {
            
            errMsg = false;
        }
        
        else {
            
            
            errMsg = true;
        }
    }
    
    public boolean checkIfProjectOwnerInAEPublicGroup() {
    	
    	if(editCase.Account.PM_Owner__c != NULL) {
        	
        	if (editCase.Related_Project__c != null && editCase.Related_Project__c != generalProjectId) {    
        		
        		Id selectProjectOwnerId = [SELECT Id, OwnerId FROM Milestone1_Project__c WHERE Id =: editCase.Related_Project__c].OwnerId;
        		if (!AEPermissionsGroupUsersIds_Set.contains(selectProjectOwnerId)) {
        			
        			return false;
        		}        	
        	}	        	
    	}
    	
    	return true;
    }
    
    
    
    public Pagereference CloseRMCaseUpdate()
    {
        return new Pagereference('/' + theCaseId);
    }
    
    
    public boolean AEassigneePermissions()
    {
        //return ENV.IsInQueue(editCase.Account.PM_owner__c , 'AE Permissions');
        
        return false;
        
    }
    
    public boolean PMownerMissing()
    {
        if(editCase.Account.PM_owner__c != NULL ) {
        	
        	return false;	
        }
        
        else if (editCase.Related_Project__c != null 
          		 && editCase.Related_Project__c == generalProjectId) {
        	 
        	return true;
        }
        
        return false;
    }
    
    
    
    
    public boolean allFieldsFilled()
    {
        system.debug('ProjectSelected : ' + ProjectSelected);
        system.debug('SubProjectSelected : ' + SubProjectSelected);
        
        if( EscalationSelected != null && EscalationSelected.EqualsIgnoreCase('PM') ) {
            
            //KN added per incident INC0026427 - Case cannot moves to 'Assigned AE' from 'Open' unless it has to be 'Assigned' first
            if (editCase.state__c == 'Open'){
                errMsgPMescalationfrom_Openstate = true;
                //return true;
            }
            
            else {
                
                errMsgPMescalationfrom_Openstate = false;
            }
            //
            if( (SubProjectSelected == '--None--' || SubProjectSelected == null ) ||
            (ClassSelected == '--None--' || ClassSelected == null) || editCase.Related_Project__c == null ) {
                
                system.debug('error');
                return true;
            }
            
            else {
                
                system.debug('Filled');
                return false;
            }
            
        }  // End of if PM
        else{
            if( (SubProjectSelected == '--None--' || SubProjectSelected == null )||
            (ClassSelected == '--None--' || ClassSelected == null) || EscalationSelected == NULL )
            {
                system.debug('error');
                return true;
            }else
            {
                system.debug('Filled');
                return false;
            }
        }
    }
    
    
    
    
    
    
    
    
    public static List<SelectOption> SortOptionList(List<SelectOption> ListToSort)
    {
        if(ListToSort == null || ListToSort.size() <= 1)
        return ListToSort;
        
        List<SelectOption> Less = new List<SelectOption>();
        List<SelectOption> Greater = new List<SelectOption>();
        integer pivot = 0;
        
        // save the pivot and remove it from the list
        SelectOption pivotValue = ListToSort[pivot];
        ListToSort.remove(pivot);
        
        for(SelectOption x : ListToSort)
        {
            system.debug('x.getLabel() : ' + x.getLabel() + ' --- pivotValue.getLabel(): ' +pivotValue.getLabel());
            if(x.getLabel() <= pivotValue.getLabel())
            {
                
                system.debug('****1');
                Less.add(x);
            }
            else
            {
                if(x.getLabel() > pivotValue.getLabel())
                {
                    system.debug('****2');
                    Greater.add(x);
                }
            }
        }
        List<SelectOption> returnList = new List<SelectOption> ();
        returnList.addAll(SortOptionList(Less));
        returnList.add(pivotValue);
        returnList.addAll(SortOptionList(Greater));
        system.debug('returnList : ' + returnList);
        return returnList;
    }
    
    static testMethod void Test_VF_AE_Queue()
    {
        /*      //instance of object creator
        CLS_ObjectCreator obj = new CLS_ObjectCreator();
        Account acc = obj.createAccount();
        insert acc;
        
        Contact con = obj.CreateContact(acc);
        insert con;
        
        Case cs = obj.Create_case(acc, con);
        cs.AE_PM_Escalated__c = 'AE';
        cs.Requested_Delivery_Date__c = system.today();
        cs.priority = 'low';
        insert cs;
        
        AE_Queue_Manager__c AEqm = obj.CreateAEQmanager();
        AEqm.class__c = 'MyClass';
        AEqm.sub_class__c = 'MySubClass';
        insert AEqm;
        
        Optional_AE_Owner__c  OpAE = obj.CreateOptionalAEowner( AEqm.id );
        
        insert OpAE;
        
        
        Milestone1_Project__c Proj = obj.CreateMPproject(acc.id);
        Proj.name = 'MyProj';
        insert Proj;
        
        
        
        
        
        Test.setCurrentPageReference(new PageReference('Page.VF_AE_Queue'));
        System.currentPageReference().getParameters().put('caseId',cs.Id );
        Apexpages.Standardcontroller teststandard = new Apexpages.Standardcontroller(cs);
        VF_AE_Queue vf = new VF_AE_Queue(teststandard);
        vf.classSelected = 'MyClass';
        vf.projectsListProcess();
        vf.subProjectSelected =  'MySubClass';
        //vf.getPMprojectsList();
        
        
        
        vf.SaveCaseUpdate(); */
        
    }
    
    
    /*  Test.setCurrentPageReference(new PageReference('Page.VF_RedMineCase'));
    System.currentPageReference().getParameters().put('id',rmcUpd.Id );
    Apexpages.Standardcontroller teststandard1 = new Apexpages.Standardcontroller(cs);
    VF_RedMineCase vf1 = new VF_RedMineCase(teststandard1);
    
    
    
    vf1.assigneeSelected = rmAss1.Id;
    
    pagereference p1 = vf1.selectclick();
    
    pagereference p11 = vf1.unselectclick();
    
    
    
    vf1.SaveRMCaseUpdate();
    PageReference p = vf1.MoveStateToAssigned();
    vf1.UnMoveStateToAssigned();
    
    
    
    vf1.CloseRMCaseUpdate();
    
    vf1.TrackerSelectedFunc();
    vf1.assigneSelected(); */
    
}