/**
 */
@isTest
private class Bid_Control_Test_Class {

    static testMethod void myUnitTest() {
		Account acct = new Account(name = 'test1',support_center__c = 'Global', type = 'Other');
        insert acct;
        
		Bid_Control__c bcn = new Bid_Control__c();
		bcn.Customer_Parent__c = acct.id;
		bcn.End_Customer__c = acct.id;
		bcn.From_Date__c = System.today();
		bcn.To_Date__c = System.today().adddays(60);
		bcn.status__c = 'Draft';
		bcn.Price_Book__c = '01s5000000062WY';
		
		insert bcn;
		Product2 prod4 = new Product2();
        prod4.Name ='HARDWARE';
        prod4.Type__c = 'HW';
        prod4.ProductCode = 'FGTTFSS';
        prod4.Product_Type1__c = 'CABLES';
        prod4.Inventory_Item_id__c = '12121112';
        prod4.partNumber__c = 'dhfgeryte';
        prod4.OEM_PL__C=10000;
        insert prod4;
        
		Bid_Control_Lines__c bcnlines = new Bid_Control_Lines__c();
		bcnlines.Bid_Control__c = bcn.id;
		bcnlines.Product__c = prod4.id;
		bcnlines.Requested_Price__c = 50;
		bcnlines.Qty__c = 500;
		insert bcnlines;
		
    }
}