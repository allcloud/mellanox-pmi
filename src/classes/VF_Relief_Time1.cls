public with sharing class VF_Relief_Time1 
{   
      
   public case c {get;set;}
   public ID caseId;
   public Account Acc {get;set;}  
      
    
        
        public VF_Relief_Time1 (ApexPages.StandardController controller) 
    {
            
          
          caseId = Apexpages.currentPage().getParameters().get('id');
                 
        //get the related  case
         c = new Case();
         c = [Select cs.Priority, cs.Relief_target_num__c,cs.AccountId, cs.Relief_Statement__c,cs.Relief_time__c,cs.Relief_given_duration__c,cs.State_WaitForCustInfo_Time_Counter__c, cs.State_OpenAdmin_Time_Counter__c, cs.CreatedDate, cs.State_OnHold_Time_Counter__c  From Case cs 
              WHERE cs.id =: caseId];
    
    }                    
                
               
     public Pagereference Cancel() 
        { 
             return new Pagereference('/'+c.Id);
           }  
      
     public PageReference getRelief_Time() 
    {     
      Account Acc = [Select Support_Level__c From Account a WHERE a.id =: c.AccountId];
        c.Relief_time__c = system.now();
         date dt = date.valueOf(c.CreatedDate);
        if(c.State_OnHold_Time_Counter__c==NULL) {c.State_OnHold_Time_Counter__c=0;}
        if(c.State_WaitForCustInfo_Time_Counter__c==NULL){c.State_WaitForCustInfo_Time_Counter__c =0;}
        if(c.State_OpenAdmin_Time_Counter__c == NULL) {c.State_OpenAdmin_Time_Counter__c=0;} 
        c.Relief_given_duration__c = ((dt.daysbetween(system.today()) - c.State_OnHold_Time_Counter__c )- c.State_WaitForCustInfo_Time_Counter__c - c.State_OpenAdmin_Time_Counter__c)*24;
        c.Relief_target_num__c = 9999;
       if(acc.Support_Level__c == 'Silver' ) 
        {
       if(c.Priority == 'Fatal'){c.Relief_target_num__c = 24;} 
       if(c.Priority == 'High'){c.Relief_target_num__c = 48;} 
       if(c.Priority == 'Medium'){c.Relief_target_num__c = 144;} 
        }

       if(acc.Support_Level__c == 'Gold' ) 
        {
       if(c.Priority == 'Fatal'){c.Relief_target_num__c = 4;} 
       if(c.Priority == 'High'){c.Relief_target_num__c = 8;} 
       if(c.Priority == 'Medium'){c.Relief_target_num__c = 24;} 
       if(c.Priority == 'Low'){c.Relief_target_num__c = 144;} 
        }
           
            
        update c;
        return new Pagereference('/'+caseId);  
                                                        
             
    }   
  
}