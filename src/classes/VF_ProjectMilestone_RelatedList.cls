public with sharing class VF_ProjectMilestone_RelatedList {
    
    private List<Milestone1_Milestone__c> milestones_List;
    private Milestone1_project__c project; 
    // Boolean indicating if the first section should be displayed or not
    public boolean displayFirstSection{get;set;}
    // Boolean indicating if the second section should be displayed or not
    public boolean displaySecondSection{get;set;}
     
    
    public VF_ProjectMilestone_RelatedList(ApexPages.StandardController controller){
        
        project = (Milestone1_project__c)controller.getRecord();
        displayFirstSection = true;
        displaySecondSection = false;
    }

     public List<Milestone1_Milestone__c> getPJMilestones()
    {
            milestones_List = [Select m.Total_Num_days__c, m.SystemModstamp, m.Total_HR__c, 
            m.Project__c, m.Parent_Milestone__c, m.Owner.FirstName,
            m.Num_Complete_tasks__c, m.Name, m.LastViewedDate, m.LastReferencedDate,
            m.Labor_Expenses__c, m.Kickoff__c, m.IsPublic__c, m.IsDeleted, 
            m.Id, m.FSE_HR__c, m.FSE2_Origin__c, m.FSE2_HR__c, m.FSE1_Origin__c, 
            m.Expenses__c, m.Description__c, m.Deadline__c, m.Days_Booked__c,
            m.Customer_Due_Date__c, m.Committed_Date__c, m.CreatedDate, m.CreatedById, m.Complete__c,
            m.Complete_Date__c, m.Additional_Onsite_Vist_days__c From Milestone1_Milestone__c m
            WHERE m.Project__c =: project.Id];
        
            return milestones_List;
    }
    
    // This method will hide the first section and display the second(edit mode) section
    public void turnToEditMode(){
        
        displayFirstSection = false;
        displaySecondSection = true;
    }   
    
    // This method will save the changes made and will revert the related list mode to it's original mode
    public void saveMilestones(){   
       
        try{
            
            update milestones_List;
            displayFirstSection = true;
            displaySecondSection = false;
        }
        
        catch(Exception e){
            
            e.getMessage();
        }
    }
    
    // This method will save the changes made and will revert the related list mode to it's original mode
    public void cancel(){   
       
        displayFirstSection = true;
        displaySecondSection = false;
    }
   
	/*
    public pageReference newMilestone(){
    	
    	Milestone1_project__c theProject = [SELECT Name, Id FROM Milestone1_project__c WHERE Id =: project.Id];
    	
    	Pagereference p = new Pagereference('/a2O/e?CF00N50000002URJJ=' +  theProject.Name + '&CF00N50000002URJJ_lkid='+ project.Id +'&retURL=%'+ project.Id);
        p.setRedirect(true);
        return p;
    }
    */
}