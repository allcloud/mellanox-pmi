public class VF_SupportInjection {
	
	public boolean displayGlobalSeach {get;set;}
	
	public VF_SupportInjection() {
		
		displayGlobalSeach = false;
		Id currentUserId = system.UserInfo.getUserId();
		
		User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id =: currentUserId];
		
		if (!currentUser.Profile.Name.EqualsIgnoreCase('MyMellanox Distributor User') && !currentUser.Profile.Name.EqualsIgnoreCase('MyMellanox Sales User') ) {
			
			displayGlobalSeach = true;
		}
	}

}