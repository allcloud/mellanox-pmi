@isTest
public with sharing class test_VF_RejectPortalAccess {
    
    static testMethod void test_TheVF_RejectPortalAccess() {
    
        CLS_ObjectCreator creator = new CLS_ObjectCreator();
        
        Account acc = creator.createAccount();
        insert acc;
        Contact con = creator.CreateContact(acc);
        insert con;
        
        apexPages.Standardcontroller controller = new apexPages.Standardcontroller(con);
        VF_Reject_Portal_Access handler = new VF_Reject_Portal_Access(controller );
        
        handler.saveForm();  
        handler.getrejectRseasons(); 
    }

}