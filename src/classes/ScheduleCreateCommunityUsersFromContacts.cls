global class ScheduleCreateCommunityUsersFromContacts implements Schedulable{
	
	global void execute(SchedulableContext SC) {
    
        BatchCreateCommunityUsersFromContacts batch = new BatchCreateCommunityUsersFromContacts();
        Database.executeBatch(batch);
   }

}