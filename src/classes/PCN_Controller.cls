public class PCN_Controller {
	public PCN__c curr_pcn {get;set;}
	public List<PCN_Affected_Products__c> pcn_parts {get;set;}
	public List<PCN_Affected_Products__c> parts_toInsert {get;set;}
	public Map<String,String> map_contact_lstGroups {get; set;}
	List<PCN_Contacts__c> pcn_contacts;
	//List<PCN_Contacts__c> pcn_null_contacts;
	Set<String> set_company_names_wo_contacts = new Set<String>();
	Map<String,Set<ID>> map_company_setContacts = new Map<String,Set<ID>>();
	Map<String,Set<ID>> map_company_setContacts_pending_approval = new Map<String,Set<ID>>();
	Map<String,Set<ID>> map_company_setContacts_already_approval = new Map<String,Set<ID>>();
	Map<String,Set<String>> map_company_setProds = new Map<String,Set<String>>(); 
	Set<ID> tmp_contacts,tmp_contacts_1;
	Set<ID> tmp_contacts_pending_approved,tmp_contacts_already_approved;
	Set<String> tmp_prods;
	PCN_Affected_Products__c tmp_part;
	//Integer PCN_total_required_Approval = 0;
	
	//Integer PCN_total_pending_Approval = 0;
	public String companyname {get;set;}
	public List<Contact> contacts_by_company {get;set;}
	public Set<String> products_by_company {get;set;}
	public String PCN_ID {get;set;}
	public boolean hasCompanies {get;set;}
	
	public List<PCN_Contacts__c> pcn_contacts_approvers {get; set;}
	//keep track set of Contacts already approved
	//Set<ID> contacts_already_approved = new Set<ID>();
	//Integer total_unique_contacts = 0;
	public class company_rows {
		public String comp_name {get;set;}
		public Integer no_contacts {get;set;}
		public Integer no_contacts_notApproved {get;set;}
		public Integer no_prods {get;set;}
	}
	public class contact_rows {
		public String contact_name {get;set;}
		public String contact_email {get;set;}
		public String PCN_Notification {get;set;}
		public String PCN_Type {get;set;}
		public String status {get;set;}
	}
	public List<company_rows> lst_comp_rows {get;set;}
	public List<contact_rows> lst_contact_rows {get;set;}
	public PCN_Controller (ApexPages.StandardController controller){
		if (controller.getRecord().id!=null){
			curr_pcn = [select id,Type__c, Status__c, Publish_Date__c, Name, PCN_Description__c,
							Total_Pending_Approval__c,Total_Number_Affected_Products__c,Approvers_By_OPN__c
						 	from PCN__c where id = :controller.getRecord().id];
		}
		else if (controller.getRecord().id==null && PCN_ID!=null){
			curr_pcn = [select id,Type__c, Status__c, Publish_Date__c, Name, PCN_Description__c,
							Total_Pending_Approval__c,Total_Number_Affected_Products__c,Approvers_By_OPN__c
						 	from PCN__c where id = :PCN_ID];
		}						 
		pcn_parts = new List<PCN_Affected_Products__c>();
		parts_toInsert = new List<PCN_Affected_Products__c>();
		pcn_contacts_approvers = new List<PCN_Contacts__c>();
		map_contact_lstGroups = new Map<String,String>();
	}
	public void init(){
		for (integer i = 0; i<10; i++){
			tmp_part = new PCN_Affected_Products__c(PCN__c=curr_pcn.ID);
			pcn_parts.add(tmp_part);	
		}
	}
	//KN added 03-17-14
	public void show_Approverlist(){
		map_contact_lstGroups.clear();
		List<PCN_Contacts__c> lst_pcn_contacts = new List<PCN_Contacts__c>();
		
		if(curr_pcn.Approvers_By_OPN__c == false && curr_pcn.Type__c == 'Approval Required'){
			lst_pcn_contacts = [select id,Company__c,Contact__c,Contact_Name__c,Contact_Email__c,
													Contact__r.PCN_Approver__c,Approved__c,Customer_Approval_Date__c,
													Group_ApprovedByOPN__c 
									from PCN_Contacts__c where PCN__c=:curr_pcn.ID and PCNType__c = 'Approval Required'
									and Contact__r.PCN_Notification__c=true and Contact__r.PCN_Approver__c='Yes'
									order by Company__c, Contact_Name__c];	
		}
		else if(curr_pcn.Approvers_By_OPN__c == true && curr_pcn.Type__c == 'Approval Required'){
			lst_pcn_contacts = [select id,Company__c,Contact__c,Contact_Name__c,Contact_Email__c,Group_ApprovedByOPN__c,
													Contact__r.PCN_Approver__c,Approved__c,Customer_Approval_Date__c 
									from PCN_Contacts__c where PCN__c=:curr_pcn.ID and PCNType__c = 'Approval Required'
									and is_Approver_by_OPN__c = true
									order by Company__c, Contact_Name__c];
		}
								
		Set<ID> unique_contacts = new Set<ID>();
		Set<String> unique_groups = new Set<String>();
		Map<String,Set<String>> map_contact_setGroups = new Map<String,Set<String>>();
		
		// pcn_contacts_approvers
		for (PCN_Contacts__c p : lst_pcn_contacts){
			if(!unique_contacts.contains(p.Contact__c)){
				unique_contacts.add(p.Contact__c);
				pcn_contacts_approvers.add(p);
			}
			//build map Contact to list of Groups
			//Apply approved by OPNs logic only
			if(curr_pcn.Approvers_By_OPN__c == true) {			
				if(map_contact_setGroups.get(p.Contact_Name__c) == null){
					unique_groups = new Set<String>();
					unique_groups.add(p.Group_ApprovedByOPN__c);
					map_contact_setGroups.put(p.Contact_Name__c,unique_groups);
				}
				else {
					unique_groups = map_contact_setGroups.get(p.Contact_Name__c);
					unique_groups.add(p.Group_ApprovedByOPN__c);
					map_contact_setGroups.put(p.Contact_Name__c,unique_groups);
				}
			}
			//
		}
		//convert map
		//Apply approved by OPNs logic only
		String tmp_s = '';
		if(curr_pcn.Approvers_By_OPN__c == true) {
			for (String s : map_contact_setGroups.keyset()){
				tmp_s = '';
				for (String s2 : map_contact_setGroups.get(s)){
					if(tmp_s == '')
						tmp_s = s2;
					else
						tmp_s = s2 + ', ' + tmp_s;
				}
				//tmp_s = tmp_s.substring(0,tmp_s.length()-1);
				map_contact_lstGroups.put(s,tmp_s);
			}
		}
		//
	}
	//
	public void init_showContacts(){
		//total_unique_contacts = 0;
		contacts_by_company = new List<Contact>();
		pcn_contacts = [select id,Company__c,Contact__c,Contact__r.PCN_Notification__c,Contact__r.PCN_Approver__c,
							Product__c,Approved__c,PCN__r.Approvers_By_OPN__c,is_Approver_by_OPN__c 
							from PCN_Contacts__c where PCN__c=:curr_pcn.ID];
		//Handle view state limit 135 KB
		if(curr_pcn.Total_Number_Affected_Products__c>100) {
			pcn_contacts = [select id,Company__c,Contact__c,Contact__r.PCN_Notification__c,Contact__r.PCN_Approver__c,
								Product__c,Approved__c,PCN__r.Approvers_By_OPN__c,is_Approver_by_OPN__c 
								from PCN_Contacts__c 
								where PCN__c=:curr_pcn.ID /*and Contact__c != null*/ limit 4000];			
		}
		//Might combine into 1 list in the future if necessary
		//pcn_null_contacts = [select id,Company__c,Contact__c,Product__c from PCN_Contacts__c where PCN__c=:curr_pcn.ID and Contact__c=null]; 
		lst_comp_rows = new List<company_rows>(); 	
		lst_contact_rows = new List<contact_rows>();
		for (PCN_Contacts__c pct : pcn_contacts){
			if(pct.Contact__c==null){
				set_company_names_wo_contacts.add(pct.Company__c);
			}
			//Exist Contact
			else {
				// only build this map if PCN is Approved-Required
				if(curr_pcn.Type__c=='Approval Required'){
					//approvers by OPNs case
					if( pct.PCN__r.Approvers_By_OPN__c && pct.is_Approver_by_OPN__c ){
							if (pct.Approved__c==false){
								if (map_company_setContacts_pending_approval.get(pct.Company__c)==null){
									tmp_contacts = new Set<ID>();
									tmp_contacts.add(pct.Contact__c);
									map_company_setContacts_pending_approval.put(pct.Company__c,tmp_contacts);					
								}
								else {
									tmp_contacts = map_company_setContacts_pending_approval.get(pct.Company__c);
									tmp_contacts.add(pct.Contact__c);
									map_company_setContacts_pending_approval.put(pct.Company__c,tmp_contacts);	
								}					
								//contacts_already_approved.add(pct.Contact__c);
							}
							//when Approved__c=true, add to map
							else {
								if (map_company_setContacts_already_approval.get(pct.Company__c)==null){
									tmp_contacts_1 = new Set<ID>();
									tmp_contacts_1.add(pct.Contact__c);
									map_company_setContacts_already_approval.put(pct.Company__c,tmp_contacts_1);					
								}
								else {
									tmp_contacts_1 = map_company_setContacts_already_approval.get(pct.Company__c);
									tmp_contacts_1.add(pct.Contact__c);
									map_company_setContacts_already_approval.put(pct.Company__c,tmp_contacts_1);	
								}				
							}
					}
					//regular 'Approval-Required' PCN
					if( !pct.PCN__r.Approvers_By_OPN__c  
						 && (pct.Contact__r.PCN_Notification__c==true && pct.Contact__r.PCN_Approver__c=='Yes') ){
							if (pct.Approved__c==false){
								if (map_company_setContacts_pending_approval.get(pct.Company__c)==null){
									tmp_contacts = new Set<ID>();
									tmp_contacts.add(pct.Contact__c);
									map_company_setContacts_pending_approval.put(pct.Company__c,tmp_contacts);					
								}
								else {
									tmp_contacts = map_company_setContacts_pending_approval.get(pct.Company__c);
									tmp_contacts.add(pct.Contact__c);
									map_company_setContacts_pending_approval.put(pct.Company__c,tmp_contacts);	
								}					
								//contacts_already_approved.add(pct.Contact__c);
							}
							//when Approved__c=true, add to map
							else {
								if (map_company_setContacts_already_approval.get(pct.Company__c)==null){
									tmp_contacts_1 = new Set<ID>();
									tmp_contacts_1.add(pct.Contact__c);
									map_company_setContacts_already_approval.put(pct.Company__c,tmp_contacts_1);					
								}
								else {
									tmp_contacts_1 = map_company_setContacts_already_approval.get(pct.Company__c);
									tmp_contacts_1.add(pct.Contact__c);
									map_company_setContacts_already_approval.put(pct.Company__c,tmp_contacts_1);	
								}				
							}
					}
					//end regular approval-required PCN
				}					
				//
				if (map_company_setContacts.get(pct.Company__c)==null){
					tmp_contacts = new Set<ID>();
					tmp_contacts.add(pct.Contact__c);
					map_company_setContacts.put(pct.Company__c,tmp_contacts);					
				}
				else {
					tmp_contacts = map_company_setContacts.get(pct.Company__c);
					tmp_contacts.add(pct.Contact__c);
					map_company_setContacts.put(pct.Company__c,tmp_contacts);	
				}
			}				
			//2nd map
			if (map_company_setProds.get(pct.Company__c)==null){
				tmp_prods = new Set<String>();
				tmp_prods.add(pct.Product__c);
				map_company_setProds.put(pct.Company__c,tmp_prods);					
			}
			else {
				tmp_prods = map_company_setProds.get(pct.Company__c);
				tmp_prods.add(pct.Product__c);
				map_company_setProds.put(pct.Company__c,tmp_prods);
			}
		}
		
		for (String s : map_company_setContacts.keyset()){
			//no_approved = (map_company_setContacts_pending_approval!=null && map_company_setContacts_pending_approval.get(s)!=null) ? map_company_setContacts_pending_approval.get(s).size() : 0;
			company_rows comp = new company_rows();
			comp.comp_name = s;
			comp.no_contacts = map_company_setContacts.get(s).size();
			comp.no_contacts_notApproved = (map_company_setContacts_pending_approval!=null && map_company_setContacts_pending_approval.get(s)!=null) ? map_company_setContacts_pending_approval.get(s).size() : 0; 
			comp.no_prods = map_company_setProds.get(s).size();
			//PCN_total_required_Approval += comp.no_contacts_notApproved +
			//							  ((map_company_setContacts_already_approval!=null && map_company_setContacts_already_approval.get(s)!=null) ? map_company_setContacts_already_approval.get(s).size() : 0);	 
			//Skip this calculation
			//PCN_total_pending_Approval += comp.no_contacts_notApproved;
			
			// Avoid governor limit
			if(lst_comp_rows.size()<300)
				lst_comp_rows.add(comp);
		}
		//For cases where company without contacts
		for (String s : set_company_names_wo_contacts){
			company_rows comp = new company_rows();
			comp.comp_name = s;
			comp.no_contacts = 0;
			comp.no_contacts_notApproved = 0;
			comp.no_prods = map_company_setProds.get(s).size();
			//comp.isFYI = curr_pcn.Type__c=='FYI' ? true : false;
			// Avoid governor limit
			if(lst_comp_rows.size()<300)
				lst_comp_rows.add(comp);
		}
		if (lst_comp_rows!=null && lst_comp_rows.size()>0)
			hasCompanies = true;
		else	
			hasCompanies = false;
		/*	//Already handle this calculation when PCN moves to 'Approval Required'
		if(curr_pcn.Type__c=='Approval Required'){	
			curr_pcn.Total_Pending_Approval__c = PCN_total_pending_Approval;
			update curr_pcn;
		}
		*/			
	}
	public void init_show_by_company(){
		init_showContacts();
		map_contact_lstGroups.clear();
		companyname = ApexPages.CurrentPage().getParameters().get('company');
		PCN_ID = ApexPages.CurrentPage().getParameters().get('ID');
		tmp_contacts = map_company_setContacts.get(companyname);
		tmp_contacts_pending_approved = map_company_setContacts_pending_approval!=null ? map_company_setContacts_pending_approval.get(companyname) : null;
		tmp_contacts_already_approved = map_company_setContacts_already_approval!=null ? map_company_setContacts_already_approval.get(companyname) : null;
		//Adding list og Groups for Contacts - Approval by OPNs only
		List<PCN_Contacts__c> lst_pcn_contacts = new List<PCN_Contacts__c>();
		
		if(curr_pcn.Approvers_By_OPN__c == true){
			lst_pcn_contacts = [select id,Contact_Name__c,Group_ApprovedByOPN__c
									from PCN_Contacts__c where PCN__c=:curr_pcn.ID
									and Company__c = :companyname
									order by Contact_Name__c];
		}
		Set<String> unique_groups = new Set<String>();
		Map<String,Set<String>> map_contact_setGroups = new Map<String,Set<String>>();
		
		// pcn_contacts_approvers
		for (PCN_Contacts__c p : lst_pcn_contacts){
			//build map Contact to list of Groups
			//Apply approved by OPNs logic only
			if(curr_pcn.Approvers_By_OPN__c == true) {			
				if(map_contact_setGroups.get(p.Contact_Name__c) == null){
					unique_groups = new Set<String>();
					unique_groups.add(p.Group_ApprovedByOPN__c);
					map_contact_setGroups.put(p.Contact_Name__c,unique_groups);
				}
				else {
					unique_groups = map_contact_setGroups.get(p.Contact_Name__c);
					unique_groups.add(p.Group_ApprovedByOPN__c);
					map_contact_setGroups.put(p.Contact_Name__c,unique_groups);
				}
			}
			//
		}
		//convert map
		//Apply approved by OPNs logic only
		String tmp_s = '';
		if(curr_pcn.Approvers_By_OPN__c == true) {
			for (String s : map_contact_setGroups.keyset()){
				tmp_s = '';
				for (String s2 : map_contact_setGroups.get(s)){
					if(tmp_s == '')
						tmp_s = s2;
					else
						tmp_s = s2 + ', ' + tmp_s;
				}
				//tmp_s = tmp_s.substring(0,tmp_s.length()-1);
				map_contact_lstGroups.put(s,tmp_s);
			}
		}
		//end KN 1-4-15
		if(tmp_contacts!=null){
			contacts_by_company = [select id,Name,Email,PCN_Notification__c,PCN_Approver__c from Contact where ID in :tmp_contacts order by Name];	
		}	
		for (Contact cc : contacts_by_company){
			contact_rows crow = new contact_rows();
			crow.contact_name = cc.Name;
			crow.contact_email = cc.Email;
			crow.PCN_Notification = cc.PCN_Notification__c==true ? 'Yes' : 'No';
			if(curr_pcn.Approvers_By_OPN__c == false)
				crow.PCN_Type = cc.PCN_Approver__c=='Yes' ? 'Yes' : 'No';
			else {
				if(companyname != null && map_company_setContacts_pending_approval.get(companyname) != null && map_company_setContacts_pending_approval.get(companyname).contains(cc.ID))
					crow.PCN_Type = 'Yes';
				else if(companyname != null && map_company_setContacts_already_approval.get(companyname) != null && map_company_setContacts_already_approval.get(companyname).contains(cc.ID))
					crow.PCN_Type = 'Yes';
				else
					crow.PCN_Type = 'No'; 								
			}
			if(tmp_contacts_pending_approved!=null && tmp_contacts_pending_approved.contains(cc.id))
				crow.Status = 'Pending Approval';
			else if (tmp_contacts_already_approved!=null && tmp_contacts_already_approved.contains(cc.id))
				crow.Status = 'Approved';
			// KN 5-7-13
			/*
			else if (cc.PCN_Notification__c==true && cc.PCN_Approver__c!='Yes')
				crow.Status = 'FYI';
			else
				crow.Status = 'N/A';								 											
			*/				
			lst_contact_rows.add(crow);		
		}
		products_by_company = map_company_setProds.get(companyname);
			
	}
	public PageReference show_by_company(){
		String companyname = ApexPages.CurrentPage().getParameters().get('company');
		PageReference pg;
		pg = new PageReference('/apex/PCN_Display_By_Company');
		pg.getParameters().put('company',companyname);
		pg.getParameters().put('ID',curr_pcn.ID);
      	pg.setRedirect(true);
      	return pg;
	}
	public PageReference show_all_company(){
		PageReference pg;
		pg = new PageReference('/apex/PCN_Show_Contacts');
		pg.getParameters().put('ID',curr_pcn.ID);
      	pg.setRedirect(true);
      	return pg;
	}
	public PageReference save(){
		parts_toInsert.clear();
		
		for (PCN_Affected_Products__c p : pcn_parts){
			if (p.Product__c!=null)
				parts_toInsert.add(p);
		}
		if (parts_toInsert.size()>0) {
			try {
				insert parts_toInsert;
			} catch (DMLException e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'There is duplicated product inserted. Please check!');
				ApexPages.addMessage(myMsg);		
				return null;
			}
		}
		
		PageReference pg = new PageReference('/'  + curr_pcn.ID);
        pg.setRedirect(true);
        return pg;
	}
	public PageReference cancel(){
		PageReference pg = new PageReference('/'  + curr_pcn.ID);
        pg.setRedirect(true);
        return pg;	
	}
}