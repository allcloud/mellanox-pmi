@isTest
public with sharing class test_VF_Case_HasAccountGPS {
    
    static testMethod void test_vf_Case_HasAccountGps()
    {
        CLS_ObjectCreator creator = new CLS_ObjectCreator();
        
        
        Account acc = creator.createAccount();
        insert acc;
        
        Id projRecTypeId = [SELECT Id FROM RecordType WHERE developerName =: 'Service_Delivery_Project'
        				    and Id != '01250000000Dys7AAC' and Id != '01250000000Dys7' limit 1].Id;
        
        Milestone1_Project__c milestonePrj = creator.CreateMPproject(acc.Id);
        milestonePrj.Status__c  = 'Completed & Accepted/no Invoice';
        milestonePrj.Deadline__c = date.today().addDays(-5);
        milestonePrj.RecordTypeId = projRecTypeId;
        insert milestonePrj;
        
        Contact con = creator.CreateContact(acc);
        insert con;
        
        Case c = creator.Create_case(acc, con);
        c.AccountId = acc.Id;
        c.ContactId = con.Id;
        insert c;
        
        Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(c);
        VF_Case_HasAccountGPS handler = new VF_Case_HasAccountGPS(controller1);
    }
 
}