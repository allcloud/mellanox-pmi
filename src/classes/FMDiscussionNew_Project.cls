public with sharing class FMDiscussionNew_Project 
{  
  
    public User    user                 {get;set;}
    public string  errMsg               {get;set;}
    public string  str_msgRefrashPage   {get;set;}
    public string  moveToState          {get;set;}
    public string  FMDiscussion         {get;set;}
    public string  TempFMDiscussion     {get;set;}
    public string  TempDiscussion       {get;set;}
    public boolean IsEditMod            {get;set;}
    public boolean IsChangeState        {get;set;}
    public Id SelectedCommentId         {get;set;}
    public string SelectedTab           {get;set;}
    public Milestone1_Project__c currentProject {get;set;}
    public Milestone1_Project__c updProject {get;set;}
    public Integer  TotalPageSize       {get;set;}
    public list<Integer> lst_Pages      {get;set;}
    public list<pageNumberType> lst_PageNumber {get;set;}
    public Integer selectedPage         {get;set;}
    private ApexPages.StandardSetController con;
    private Integer noOfChosentemplates {get;set;}
    public Boolean disableAEButton{get;set;}
    public Boolean disablePMButton{get;set;}
    public Boolean AEorPMSubmitted{get;set;}
    public String UrlStatus{get;set;}
    
    //This will determine if the "Add" button on the Choose template panel will be disabled or not
    public Boolean isCBDisabled {get;set;}
    
     /**************  INNER CLASS    ******************/
    public class pageNumberType
    {
        public Integer CurrentPageNumger {get;set;}
        public boolean IsPageSelected    {get;Set;}
     /**************  INNER CLASS    ******************/
         
        public pageNumberType(Integer Num)
        {
            CurrentPageNumger = Num;
            IsPageSelected = false ;
        }
    }
    
    
    public list<FMDiscussionWrapper> lst_FMDiscussionWrapper   {get;set;}
    public map<Id,FMDiscussionWrapper> map_FMDiscussionWrapper {get;set;}
    
    public boolean IsHasNext {set;get{return con.getHasNext();}}
    public boolean IsHasPrevious {set;get{return con.getHasPrevious();}}
    public boolean checkbostest {get;set;}
    public list<RmCase__c> lst_RMCases ;
    public RmCase__c RMCase1 {get;set;}
    public RmCase__c RMCase2 {get;set;}
    public RmCase__c RMCase3 {get;set;}
    public RmCase__c RMCase4 {get;set;}
    public RmCase__c RMCase5 {get;set;}
    
    //checkBoxes
    public boolean ExposedToFM   {get;set;}
    public boolean PublicCB      {get;set;}
    public boolean SendMeEmail   {get;set;}
    public boolean LogCall       {get;set;}
    public boolean ManagerEsc    {get;set;}
    public boolean SendToRMCase1 {get;set;}
    public boolean SendToRMCase2 {get;set;}
    public boolean SendToRMCase3 {get;set;}
    public boolean SendToRMCase4 {get;set;}
    public boolean SendToRMCase5 {get;set;}
    
    
     /**************  INNER CLASS    ******************/
    public class FMDiscussionWrapper
    {
        public boolean ISRM1 {get;set;}
        public boolean IsRM2 {get;set;}
        public boolean ISRM3 {get;set;}
        public boolean ISRM4 {get;set;}
        public boolean ISRM5 {get;set;}
        public boolean ISPublic {get;set;}
        public boolean ISPublicRM1 {get;set;}
        public boolean ISPublicRM2 {get;set;}
        public boolean ISPublicRM3 {get;set;}
        public boolean ISPublicRM4 {get;set;}
        public boolean ISPublicRM5 {get;set;}
        public boolean ISPublicPublic {get;set;}
        public boolean ISPublicPrivate {get;set;}
        public Project_FM_Discussions__c FMDiscussion {get;set;}
        public boolean IsEditMode            {get;set;}
        public string CommentText {get;set;}
     /**************  INNER CLASS    ******************/
     
        public FMDiscussionWrapper(Project_FM_Discussions__c FMD)
        {
            FMDiscussion = FMD ;
            
            ISPublic = FMD.Public__c;
            
            ISPublicPublic  = FMD.Public__c;
            ISPublicPrivate = FMD.Public__c;
            CommentText = FMD.Discussion__c;
        }
        
    } 
    /// TEMLAPES
    //public list <Folder> lst_Folder      {get;set;}
    Transient public Integer FolderSize {get;set;}
    Transient public set <Id>      set_FolderId    {get;set;}
    public Id selectedFolder {get;set;}
    public list <Folder> lst_Folders {get;set;}
    public string SelectedFolderId{get;set;}
    
    public map<string,selectedTemplates> map_Templates = new map<string,selectedTemplates>();
    public list <selectedTemplates> lst_EmailTemplate  {get;set;}
    
    public string  selectedTemplate {get;set;}
    public string  theChoosenTemplates {get;set;}
    public boolean IsAddTemplate {get;set;}
    public formulaReplaceTemplate theformula{get;set;}
   
     /**************  INNER CLASS    ******************/
    public class selectedTemplates 
    {
        public boolean TempIsSelected {get;set;}
        public string TempId {get;set;}
        public string TempName {get;set;}
        public string TempBlod {get;set;}
     /**************  INNER CLASS    ******************/    
        
        public selectedTemplates(string tname,string tbody,string tid)
        {
            TempName = tname;
            TempBlod = tbody ;
            TempId   = tid ;
            TempIsSelected = false ;
        }
    }
    
    
    /**************  INNER CLASS  START  ******************/
    public class formulaReplaceTemplate
    {
        public string customerName {get;set;}
        public string OwnerName    {get;set;}
        public string caseNumber   {get;set;}
        public String st1Member    {get;set;}
        public String caseDescription {get;set;}
         public String customerFirstName {get;set;}
        public String customerLastName {get;set;} 
        
    }
    /**************  INNER CLASS  END  ******************/
     
    public FMDiscussionNew_Project(ApexPages.StandardController stdController)
    {
        
        UrlStatus = '0';
        disableAEButton = true;
        disablePMButton = true;
        isCBDisabled = true;
        IsEditMod = false ;
        AEorPMSubmitted = false;
        Id rmCaseRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: 'RM_Related_case'].Id;
        
        
          currentProject = [Select m.c_Sales_Owner__c, m.c_Account__c, m.blank_project__c, m.Weekly_Reporting_Status__c,
          m.Vertical_Market__c, m.VM_Siza__c, m.Username_Password__c, m.Training_Location__c, m.Training_Delivery_Method__c, 
          m.Training_Closest_Lodging__c, m.Training_Classroom_config__c, m.Total_number_of_days__c, m.Total_PDM_HR__c,
          m.Total_Number_Onsite_Person_Days__c, m.Total_Labor_Expenses__c, m.Total_FSE_HR__c, m.Total_FSE2_HR__c, 
          m.Total_Expenses__c, m.Total_Duration__c, m.Total_Duration_Hours__c, m.Technology__c, m.SystemModstamp, 
          m.Support_Provider__c, m.Summary__c, m.Success_Criteria__c, m.Storage_Size__c, m.Status__c, m.Start_Date__c, 
          m.Site_Address__c, m.Shipping_Address__c, m.Salesforce_ID__c, m.Sales_Order__c, m.Related_SE_Project__c, 
          m.Region__c, m.RecordTypeId, m.Quarter__c, m.Project_PNs__c, m.Project_Close_Date__c, m.Project_Category__c, 
          m.Priority__c, m.Planned_Person_Days__c, m.Part_Number__c, m.Parent_Project__c, m.POC_state__c, m.PM_Owner__c, 
          m.Owner_Name__c, m.Owner_Mellanox_Group__c, m.OwnerId, m.Other_Colud_Configuration__c, m.Opportunity_Stage__c, 
          m.Opportunity_Service_Amount__c, m.Opportunity_PO_Number__c, m.Opportunity_Number__c, m.Opportunity_Name__c, 
          m.Opportunity_Amount__c, m.Opp_Requst_delivery_date__c, m.Opertion_System__c, m.Open__c, m.OEM__c, 
          m.Number_of_CPU_s__c, m.Number_Of_VMs__c, m.Next_Milestone__c, m.Name, m.Memory_amount__c, m.Mellanox_Service_Level__c, 
          m.LastReferencedDate, m.LastModifiedDate, m.LastModifiedById, m.LastActivityDate, m.IsDeleted, 
          m.Id, m.Hours_Left__c, m.Geo_Location__c, m.GPS_Project_ID__c, m.GPS_Auto_Number__c, m.GM__c, m.GM_Comments__c, 
          m.Further_Closure_Information__c, m.Free_GPS__c, m.Follow_Up_Date__c, m.Flag_for_Reports__c, m.FAE_Owner__c, m.Expenses__c, 
          m.Escalated_To__c, m.Design_In_Indicator__c, m.Description__c, m.Deadline__c, m.Customer_DD__c, m.CreatedDate, m.CreatedById, 
          m.Course_Time__c, m.Course_Start_Date__c, m.Course_Name__c, m.Course_End_Date__c, m.Course_5th_Meeting_Time__c, 
          m.Course_5th_Meeting_Date__c, m.Course_4th_Meeting_Time__c, m.Course_4th_Meeting_Date__c, m.Course_3rd_Meeting_Time__c, 
          m.Course_3rd_Meeting_Date__c, m.Course_2nd_Meeting_Time__c, m.Course_2nd_Meeting_Date__c, m.Country__c, m.Completion_Status__c, 
          m.Completion_Description__c, m.Complete_Date__c, m.Colud_Application__c, m.Closure_Reason__c, m.Closed__c, 
          m.Certification_Exam_Eligibility__c, m.Business_Value__c, m.Business_Justification__c, m.Book_Date__c, m.BLANK_Design_In_Project__c, 
          m.Assignee__c, m.Assessment_WebEx_Meeting_Invitation__c, m.Assessment_Scheduled_Date__c, m.Assessment_Additional_WebEx_Information__c, 
          m.Amount__c, m.Add_Time__c, m.Account_ID__c From Milestone1_Project__c m
                       Where IsDeleted=false and m.Id =: ApexPages.currentPage().getParameters().get('Id')][0]; 
        
        user = new User ();
        user =[Select u.Id,u.Name, u.Profile.Name, u.ProfileId, u.ContactId From User u Where Id =: UserInfo.getUserId() and IsActive = true limit 1];
        
       
       
        
        
        SelectedTab = 'All';  
        errMsg        = '';
        FMDiscussion  = '';
        initialNewCommentParam();
        searchComments();
        
        searchFolders();
        noOfChosentemplates = 0;
        
        
    }
    
    //////////////////////////////////////////popup
    
   
    public void searchFolders()
    {
        SelectedFolderId ='';
        selectedTemplate ='';
        theChoosenTemplates = '' ;
        lst_Folders = [Select f.Name, f.Id From Folder f where f.Name like '%st1%'];
        system.debug('==>lst_Folders  '+lst_Folders );
    }
    
    public void searchFolderTemplates()
    {
        system.debug('==>SelectedFolderId  '+SelectedFolderId);
        lst_EmailTemplate = new list<selectedTemplates>();
        map_Templates = new map<string,selectedTemplates> ();
        for(EmailTemplate emlt: [Select e.Name, e.Id, e.Folder.Id, e.FolderId, e.Body From EmailTemplate e where e.Folder.Id =: SelectedFolderId ])
        {
            selectedTemplates selectedTemp = new selectedTemplates(emlt.Name,emlt.Body,emlt.Id);
            lst_EmailTemplate.add(selectedTemp);
            map_Templates.put(string.valueof(emlt.Id),selectedTemp);
        }
        system.debug('==>lst_EmailTemplate '+lst_EmailTemplate);
    }
    public void selectedTemplate()
    {
        system.debug('==>lst_EmailTemplate '+lst_EmailTemplate);
        system.debug('==>selectedTemplate '+selectedTemplate );
        system.debug('==>IsAddTemplate '+IsAddTemplate );
        isCBDisabled = false;
        noOfChosentemplates ++;
            
        if (noOfChosentemplates > 1)
        {
            system.debug(' More than 1 chechboxes checked');
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please choose only 1 template');
             ApexPages.addMessage(myMsg);
        }
        
        if(map_Templates.get(selectedTemplate)!=null && map_Templates.get(selectedTemplate).TempBlod !=null )
            theChoosenTemplates = map_Templates.get(selectedTemplate).TempBlod ;
            
        for(selectedTemplates st: map_Templates.values())
        { 
            system.debug('==>st.TempIsSelected'+st.TempIsSelected);
            system.debug('==>st.TempId '+st.TempId );
           // system.debug('==>map_Templates.get(selectedTemplate ).TempIsSelected '+map_Templates.get(selectedTemplate ).TempIsSelected );
            if(st.TempIsSelected == true && st.TempId != map_Templates.get(selectedTemplate ).TempId )
            {
                st.TempIsSelected = false;
            }
           
            system.debug('==>theChoosenTemplates'+theChoosenTemplates);
        }
        system.debug('==>map_Templates'+map_Templates);
        system.debug('==>ls:lst_EmailTemplate'+lst_EmailTemplate);
    
        system.debug('==>theChoosenTemplates '+theChoosenTemplates );
         system.debug('elad isCBDisabled : '+isCBDisabled );
    }
    
    
   
    
    public void searchComments()
    {
        system.debug('==>SelectedTab '+SelectedTab);
        if(SelectedTab == 'All')
        {
            con = new ApexPages.StandardSetController([Select p.SystemModstamp, p.Project__c, p.Project_Comment_ID__c, p.Name, Discussion__c, 
											            p.Public__c, p.IsDeleted, p.Id, p.CreatedDate, p.CreatedBy.Name, p.Comment__c 
											            From Project_FM_Discussions__c p where Project__c =: currentProject.Id order by CreatedDate desc]);
        } 
        else if(SelectedTab == 'Public')
        {
             con = new ApexPages.StandardSetController([Select p.SystemModstamp, p.Project__c, p.Project_Comment_ID__c, p.Name, Discussion__c, 
											            p.Public__c, p.IsDeleted, p.Id, p.CreatedDate, p.CreatedBy.Name, p.Comment__c 
											            From Project_FM_Discussions__c p where Project__c =: currentProject.Id and Public__c  =: false order by CreatedDate desc]);
        } 
        else if(SelectedTab == 'Private')
        {
             con = new ApexPages.StandardSetController([Select p.SystemModstamp, p.Project__c, p.Project_Comment_ID__c, p.Name, Discussion__c, 
											            p.Public__c, p.IsDeleted, p.Id, p.CreatedDate, p.CreatedBy.Name, p.Comment__c 
											            From Project_FM_Discussions__c p where Project__c =: currentProject.Id and Public__c =: true order by CreatedDate desc]);
        } 
       
        else
        {
            lst_FMDiscussionWrapper = new list<FMDiscussionWrapper>();
            return;
        }
        
        con.setPageSize(80);
        
        TotalPageSize = con.getResultSize()/ con.getPageSize() ;
        lst_Pages = new list<Integer>();
        lst_PageNumber = new list<pageNumberType>();
        for(integer i=1; i<= TotalPageSize+1; i++)
        {
            lst_PageNumber.add(new pageNumberType(i));
            lst_Pages.add(i);
        }
        lst_PageNumber[0].IsPageSelected = true ;
        system.debug('===>TotalPageSize '+TotalPageSize);
        
        getFMDW ();
        system.debug('411 ==>lst_FMDiscussionWrapper '+lst_FMDiscussionWrapper);                                     
    }
    
    public list<FMDiscussionWrapper> getFMDW ()
    {
        lst_FMDiscussionWrapper = new list<FMDiscussionWrapper>();
        map_FMDiscussionWrapper = new map<Id,FMDiscussionWrapper>();
       system.debug('418 con : ' + con); 
         system.debug('418 con.getRecords() : ' + con.getRecords()); 
        for(Project_FM_Discussions__c fmd: (list<Project_FM_Discussions__c>)con.getRecords())
        {
            FMDiscussionWrapper tempW = new FMDiscussionWrapper(fmd);
            tempW.IsEditMode = false;
           
            
            lst_FMDiscussionWrapper.add(tempW);
            map_FMDiscussionWrapper.put(fmd.Id,tempW);
        }
       // system.debug('==>lst_FMDiscussionWrapper: '+lst_FMDiscussionWrapper);
       // system.debug('==>map_FMDiscussionWrapper: '+map_FMDiscussionWrapper);
        return lst_FMDiscussionWrapper ;
    }
    
    
    public void UpdatePublicORPrivate()
    {
    	system.debug('inside 485');
       // system.debug('==> ISPublic '+map_FMDiscussionWrapper.get(SelectedCommentId).ISPublic);
       if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId , Public__c = map_FMDiscussionWrapper.get(SelectedCommentId).IsPublic);
            UpdateRecord(tempComment);
            
            if (tempComment.Public__c) {
            	
            	Project_FM_Discussions__c  tempCommentFromDB = [SELECT Id, Discussion__c FROM Project_FM_Discussions__c WHERE Id =: SelectedCommentId];
            	
            	if (tempCommentFromDB.Discussion__c.length() > 3850) {
            		
            		system.debug('inside 497');
            		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'On MyMellanox the comment will be split into multiple Case Comments (due to size limits)'));
            		 
            	}
            }
            system.debug('==> public fm: '+tempComment.Public__c);
        }  
    }
  
    public void UpdatePublicORPrivatePrivate()
    {
        if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId , Public__c=map_FMDiscussionWrapper.get(SelectedCommentId).ISPublicPrivate);
            UpdateRecord(tempComment);
            system.debug('==> public fm: '+tempComment.Public__c);
        }
    }
    public void UpdatePublicORPrivatePublic()
    {
        if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId , Public__c=map_FMDiscussionWrapper.get(SelectedCommentId).ISPublicPublic);
            UpdateRecord(tempComment);
            system.debug('==> public fm: '+tempComment.Public__c);
        }
    }
   
    
    public void UpdatePrivate()
    {
        if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            integer PageNumber = con.getPageNumber();
            Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId , Public__c=false);
            UpdateRecord(tempComment);
            system.debug('==> public fm: '+tempComment.Public__c);
            system.debug('==> getPageNumber: '+PageNumber);
            searchComments();
            con.setPageNumber(PageNumber);
            getFMDW ();  system.debug('512 ==>map_FMDiscussionWrapper: '+map_FMDiscussionWrapper);
        }
    }
    public void UpdatePublic()
    {
        if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            integer PageNumber = con.getPageNumber();
            Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId , Public__c=true);
            UpdateRecord(tempComment);
            system.debug('==> public fm: '+tempComment.Public__c);
            searchComments();
            con.setPageNumber(PageNumber);
            getFMDW ();   system.debug('525 ==>map_FMDiscussionWrapper: '+map_FMDiscussionWrapper);
        }
    }
    
    public void DeleteComment()
    {
        Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId);
        deleteRecord (tempComment);
        searchComments();   
    }
    
    public void EditComment()
    {
        TempDiscussion = map_FMDiscussionWrapper.get(SelectedCommentId).FMDiscussion.Discussion__c;
        map_FMDiscussionWrapper.get(SelectedCommentId).IsEditMode = true;
    }
    
   
    public void SaveChangeCom()
    {
        if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId,Discussion__c=map_FMDiscussionWrapper.get(SelectedCommentId).FMDiscussion.Discussion__c );
            map_FMDiscussionWrapper.get(SelectedCommentId).IsEditMode = false;
            UpdateRecord(tempComment) ;
            
            system.debug('===>map_FMDiscussionWrapper.get(SelectedCommentId) '+map_FMDiscussionWrapper.get(SelectedCommentId).IsPublic);
            system.debug('===>RMCase3: '+RMCase3);
            system.debug('===>RMCase2: '+RMCase2);
            system.debug('===>tempComment: '+tempComment);
        }
    }
    //should delete
    public void SaveChanges()
    {
        if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            Project_FM_Discussions__c  tempComment = new Project_FM_Discussions__c (Id=SelectedCommentId,Discussion__c=map_FMDiscussionWrapper.get(SelectedCommentId).FMDiscussion.Discussion__c);
            map_FMDiscussionWrapper.get(SelectedCommentId).IsEditMode = false;
            UpdateRecord(tempComment) ;
            system.debug('==>save: '+tempComment);
        }
        
    }
    
    public void CancelChanges()
    {
        if(map_FMDiscussionWrapper.get(SelectedCommentId) != null)
        {
            map_FMDiscussionWrapper.get(SelectedCommentId).FMDiscussion.Discussion__c = TempDiscussion;
            map_FMDiscussionWrapper.get(SelectedCommentId).IsEditMode = false;
        }
    }
    
    public void NextPage()
    {
        con.next();
        getFMDW ();    system.debug('==>605 map_FMDiscussionWrapper: '+map_FMDiscussionWrapper);
    }
    public void PreviousPage()
    {
        con.previous();
        getFMDW (); system.debug('==>610 map_FMDiscussionWrapper: '+map_FMDiscussionWrapper);
    } 
    
    public void FirstPage()
    {
        con.first();
        getFMDW ();  system.debug('==>616 map_FMDiscussionWrapper: '+map_FMDiscussionWrapper);
    } 
    
    public void LastPage()
    {
        con.last();
        getFMDW ();
        system.debug('623 ==>map_FMDiscussionWrapper: '+map_FMDiscussionWrapper); 
    } 
    
        
    public void UpdateRecord (sobject sobj)
    {
        system.debug('==> Update Record');
        Database.SaveResult sr = Database.update(sobj, false);
        if(sr.isSuccess() == false)
        {
            system.debug('==>error : '+sr.getErrors());
            str_msgRefrashPage = '';
            errMsg = 'An error has occured : \n'+string.valueof(sr.getErrors()).substringBetween('getMessage=',';');
        }
        return;
    }
        
    public void deleteRecord (sobject sobj)
    {
        Database.DeleteResult  sr = Database.delete(sobj, false);
        if(sr.isSuccess() == false)
        {
            system.debug('==>error : '+sr.getErrors());
            str_msgRefrashPage = '';
            errMsg = 'An error has occured : \n'+string.valueof(sr.getErrors()).substringBetween('getMessage=',';');
        }
        return;
    }
        
    public void insertRecord (sobject sobj)
    {
        system.debug('start insert case ');
        Database.SaveResult  sr = Database.insert(sobj, false);
        system.debug('after save');
        if(sr.isSuccess() == false)
        {
            system.debug('==>error : '+sr.getErrors());
            str_msgRefrashPage = '';
            errMsg = 'An error has occured  : \n'+string.valueof(sr.getErrors()).substringBetween('getMessage=',';');
        }
        system.debug('update success');
        return;
    }
        
    public void initialNewCommentParam()
    {
        ExposedToFM   = false;
        moveToState   = '';
        PublicCB      = false;
        SendMeEmail   = false;
        LogCall       = false;
        ManagerEsc    = false;
        SendToRMCase1 = false;
        SendToRMCase2 = false;
        SendToRMCase3 = false;
        SendToRMCase4 = false;
        SendToRMCase5 = false;
        IsChangeState = false;
        IsAddTemplate = false;
        updProject = new Milestone1_Project__c(Id=currentProject.Id);
    }
    
    public void saveOpenAE()
    {
        moveToState = 'saveOpenAE';
        save();
        system.debug('==>str_msgRefrashPage '+str_msgRefrashPage);
    }
    
    public void save()
    {
        errMsg        = '';
        str_msgRefrashPage ='';
        TempFMDiscussion =FMDiscussion;
        
        
        system.debug('==>errMsg: '+errMsg);
        system.debug('==> TempFMDiscussion : '+ TempFMDiscussion);
          
        
        if(FMDiscussion.length()>32000)
        {
                errMsg='Warning: the comment will not be saved , data value too large';
        }
        else if( errMsg == '' || errMsg == null ) 
        { 
                if( moveToState!='' )
                {
                        //updCase.Send_me_comment__c = SendMeEmail;  
                        updateRecord(updProject);
                }  
                if( FMDiscussion != Null && FMDiscussion != '' && errMsg == '' )
                {
                    Project_FM_Discussions__c newFMDiscussion      = new Project_FM_Discussions__c();
                    newFMDiscussion.Public__c = PublicCB;
                     
                    newFMDiscussion.Project__c = currentProject.Id;
                    newFMDiscussion.Discussion__c = FMDiscussion;
                    insert  newFMDiscussion;
                     
                    if (FMDiscussion.length()> 3850) {
                    	
                    	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'On MyMellanox the comment will be split into multiple Case Comments (due to size limits)'));
                    } 
                }
         }
         
         searchComments();
         FMDiscussion = null; 
          return;
    }
    
    public void test()
    {
        system.debug('==>popup window');
    }
    
   
    public void getPage()
    {
        try{
        system.debug('==>selectedPage '+selectedPage);
        if(selectedPage!=null)
        {
            for(integer j=0 ; j<=TotalPageSize; j++)
            {
                lst_PageNumber[j].IsPageSelected = false;
            }
            system.debug('==>lst_PageNumber '+lst_PageNumber);
            lst_PageNumber[selectedPage-1].IsPageSelected = true;
            system.debug('==>lst_PageNumber[selectedPage-1].IsPageSelected '+lst_PageNumber[selectedPage-1].IsPageSelected);
            con.setPageNumber(selectedPage);
            system.debug('==>con '+con.getPageNumber());
            getFMDW ();
            system.debug('861 ==>map_FMDiscussionWrapper: '+map_FMDiscussionWrapper);
        }
        }
        catch(exception e){system.debug('==>ee'+e);}
    }
     public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
}