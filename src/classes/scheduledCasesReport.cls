global class scheduledCasesReport implements Schedulable{ 
global void execute(SchedulableContext SC) {
 list<case> OpenCases = new list<case>();  
 list<case> ClosedCases = new list<case>(); 
 list<case> lastXDaysClosedCases= new list<case>(); 
 
 list<Case_Monitor__c> CM_toInsert = new List<Case_monitor__c>(); 
 map<string,integer> StateToCases = new  map<string,integer> {'Wait for Release'=>0 ,'Wait for Implementation'=>0 , 'Feature-Request/Doc-Change'=>0 , 'On Hold'=>0 ,'Waiting for Customer Approval'=>0};
 string AEState; 
 
 map<string,integer> PriorityToCaseCount = new  map<string,integer> {'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 map<string,decimal> PriorityToCaseAge = new  map<string,decimal>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 integer StateCaseCount = 0;
 integer PriorityCaseCount = 0;
 decimal PriorityCaseAge = 0;
 
 map<string,integer> AEPriorityToCaseCount = new  map<string,integer>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 map<string,decimal> AEPriorityToCaseAge = new  map<string,decimal>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 integer AEPriorityCaseCount = 0;
 decimal AEPriorityCaseAge = 0;
 
 map<string,integer> RMPriorityToCaseCount = new  map<string,integer>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 map<string,decimal> RMPriorityToCaseAge = new  map<string,decimal>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 integer RMPriorityCaseCount = 0;
 decimal RMPriorityCaseAge = 0;

 map<string,integer> ClosedPriorityToCaseCount = new  map<string,integer>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 map<string,decimal> ClosedPriorityToCaseAge = new  map<string,decimal>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 integer ClosedPriorityCaseCount = 0;
 decimal ClosedPriorityCaseAge = 0;
 
 map<string,integer> ClosedAEPriorityToCaseCount = new  map<string,integer>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 map<string,decimal> ClosedAEPriorityToCaseAge = new  map<string,decimal>{'Low'=>0, 'Medium'=>0, 'High'=>0 , 'Fatal'=>0};
 integer ClosedAEPriorityCaseCount = 0;
 decimal ClosedAEPriorityCaseAge = 0;

// added 23/05/13 - Yael wants to monitor over all states per each priority (need the avg time spent in each state
 map<string,integer> ClosedAEStateToCaseCount = new  map<string,integer>{'Assigned FE/GSO'=>0, 'Assigned AE'=>0, 'Assigned R&D'=>0 , 'Waiting for Customer'=>0, 'Wait for Implemetation/Release'=>0, 'Other'=>0};
 map<string,decimal> ClosedAEStateToCaseAge = new  map<string,decimal>{'Assigned FE/GSO'=>0, 'Assigned AE'=>0, 'Assigned R&D'=>0 , 'Waiting for Customer'=>0, 'Wait for Implemetation/Release'=>0, 'Other'=>0};
 integer ClosedAEStateCaseCount = 0;
 decimal ClosedAEStateCaseAge = 0;
 list<String> Priority = new list<string> {'Low','Medium','High' ,'Fatal'};

 decimal TotalTimeinWaitforRelease = 0;
 integer TotalOpenCasesInWFR=0;

 map<string,integer> CasesPerIBMProject = new  map<string,integer>();
 integer CountCasesPerIBMProject=0;
    
 /* OpenCases = [select id , Account.Name, priority ,RM_cases_count__c, State_Update_Time__c, AE_PM_Escalated__c ,case_source__c, case_age__c, state__c, case_type__c, AE_state__c, FM_state__c, State_WaitForRelease_Time_Counter__c , Account_Project__c
               from Case 
               where state__c != 'Closed' and Status != 'Closed'];  */
 
 // All cases closed in the last 30 days
  /*lastXDaysClosedCases= [ SELECT id, case_age__c,  priority,  AE_PM_Escalated__c,state_open_time_counter__c ,state_assigned_time_counter__c, 
                         State_AssignedAE_Time_Counter__c, State_WaitForCustInfo_Time_Counter__c ,State_Wait_for_Implementation_Time_Count__c,
                         State_WaitForCustApp_Time_Counter__c, State_WaitForRelease_Time_Counter__c, State_AssignedRD_Time_Counter__c
                  FROM Case
                  WHERE state__c = 'Closed' AND Status ='Closed' AND ClosedDate=LAST_N_DAYS:30];*/
  
/*  ClosedCases= [ SELECT id, case_age__c, Time_Esc2Ae_Close__c, RM_cases_count__c,  priority,  AE_PM_Escalated__c
                           FROM Case                 
                           WHERE state__c = 'Closed' AND Status ='Closed' ];*/



  For (String prio:Priority)
  {
      ClosedAEStateToCaseCount.put('Assigned FE/GSO' ,0);
      ClosedAEStateToCaseCount.put('Assigned AE' ,0);
      ClosedAEStateToCaseCount.put('Assigned R&D' ,0);
      ClosedAEStateToCaseCount.put('Waiting for Customer' ,0);
      ClosedAEStateToCaseCount.put('Wait for Implemetation/Release' ,0);
      ClosedAEStateToCaseCount.put('Other' ,0);
      
      ClosedAEStateToCaseAge.put('Assigned FE/GSO' ,0);
      ClosedAEStateToCaseAge.put('Assigned AE' ,0);
      ClosedAEStateToCaseAge.put('Assigned R&D' ,0);
      ClosedAEStateToCaseAge.put('Waiting for Customer' ,0);
      ClosedAEStateToCaseAge.put('Wait for Implemetation/Release' ,0);
      ClosedAEStateToCaseAge.put('Other' ,0);
      ClosedAEStateCaseCount = 0;
      ClosedAEStateCaseAge = 0;
      
      system.debug('Closed Cases avg by prio, current prio'+ prio);
      for(Case CsPrio: [ SELECT id, case_age__c,  priority,  AE_PM_Escalated__c,state_open_time_counter__c ,state_assigned_time_counter__c, 
                         State_AssignedAE_Time_Counter__c, State_WaitForCustInfo_Time_Counter__c ,State_Wait_for_Implementation_Time_Count__c,
                         State_WaitForCustApp_Time_Counter__c, State_WaitForRelease_Time_Counter__c, State_AssignedRD_Time_Counter__c
                  FROM Case
                  WHERE state__c = 'Closed' AND Status ='Closed' AND ClosedDate=LAST_N_DAYS:30])
      {
          if (CsPrio.AE_PM_Escalated__c=='AE' && CsPrio.Priority == Prio)   
          {
              system.debug('Inside the closed cases in last X days loop, current case:'+ CsPrio);
              transient Decimal OtherAge=0;
              OtherAge=CsPrio.case_age__c;
              if (CsPrio.state_assigned_time_counter__c!=NULL)
              {
                  ClosedAEStateCaseCount=ClosedAEStateToCaseCount.get('Assigned FE/GSO');
                  ClosedAEStateCaseAge = ClosedAEStateToCaseAge.get('Assigned FE/GSO');
                  ClosedAEStateToCaseCount.put('Assigned FE/GSO' , ClosedAEStateCaseCount +1);
                  ClosedAEStateToCaseAge.put('Assigned FE/GSO' ,ClosedAEStateCaseAge + CsPrio.state_assigned_time_counter__c); 
                  OtherAge = OtherAge - CsPrio.state_assigned_time_counter__c;
              }

              if (CsPrio.State_AssignedRD_Time_Counter__c!=NULL)
              {
                  ClosedAEStateCaseCount=ClosedAEStateToCaseCount.get('Assigned R&D');
                  ClosedAEStateCaseAge = ClosedAEStateToCaseAge.get('Assigned R&D');
                  ClosedAEStateToCaseCount.put('Assigned R&D' , ClosedAEStateCaseCount +1);
                  ClosedAEStateToCaseAge.put('Assigned R&D' ,ClosedAEStateCaseAge + CsPrio.State_AssignedRD_Time_Counter__c); 
                  OtherAge = OtherAge -CsPrio.State_AssignedRD_Time_Counter__c;
              }
              
              if (CsPrio.State_AssignedAE_Time_Counter__c!=NULL)
              {
                  ClosedAEStateCaseCount=ClosedAEStateToCaseCount.get('Assigned AE');
                  ClosedAEStateCaseAge = ClosedAEStateToCaseAge.get('Assigned AE');
                  ClosedAEStateToCaseCount.put('Assigned AE' , ClosedAEStateCaseCount +1);
                  ClosedAEStateToCaseAge.put('Assigned AE' ,ClosedAEStateCaseAge + CsPrio.State_AssignedAE_Time_Counter__c);
                  OtherAge = OtherAge -CsPrio.State_AssignedAE_Time_Counter__c;
              }
              
              if (CsPrio.State_WaitForCustApp_Time_Counter__c!=NULL && CsPrio.State_WaitForCustInfo_Time_Counter__c!=NULL)
              {
                  ClosedAEStateCaseCount=ClosedAEStateToCaseCount.get('Waiting for Customer');
                  ClosedAEStateCaseAge = ClosedAEStateToCaseAge.get('Waiting for Customer');
                  ClosedAEStateToCaseCount.put('Waiting for Customer' , ClosedAEStateCaseCount +1);
                  ClosedAEStateToCaseAge.put('Assigned AE' ,ClosedAEStateCaseAge + CsPrio.State_WaitForCustApp_Time_Counter__c + CsPrio.State_WaitForCustInfo_Time_Counter__c );
                  OtherAge = OtherAge -CsPrio.State_WaitForCustApp_Time_Counter__c - CsPrio.State_WaitForCustInfo_Time_Counter__c;
              }
              
              if (CsPrio.State_Wait_for_Implementation_Time_Count__c!=NULL && CsPrio.State_WaitForRelease_Time_Counter__c!=NULL)
              {
                  ClosedAEStateCaseCount=ClosedAEStateToCaseCount.get('Wait for Implemetation/Release');
                  ClosedAEStateCaseAge = ClosedAEStateToCaseAge.get('Wait for Implemetation/Release');
                  ClosedAEStateToCaseCount.put('Wait for Implemetation/Release' , ClosedAEStateCaseCount +1);
                  ClosedAEStateToCaseAge.put('Wait for Implemetation/Release' ,ClosedAEStateCaseAge + CsPrio.State_Wait_for_Implementation_Time_Count__c + CsPrio.State_WaitForRelease_Time_Counter__c);
                  OtherAge = OtherAge -CsPrio.State_Wait_for_Implementation_Time_Count__c - CsPrio.State_WaitForRelease_Time_Counter__c;
              }
               

              ClosedAEStateCaseCount=ClosedAEStateToCaseCount.get('Other');
              ClosedAEStateCaseAge = ClosedAEStateToCaseAge.get('Other');
              ClosedAEStateToCaseCount.put('Other' , ClosedAEStateCaseCount +1);
              ClosedAEStateToCaseAge.put('Other' ,ClosedAEStateCaseAge + OtherAge);
          
              }
          }
      if(ClosedAEStateToCaseCount.get('Assigned FE/GSO')!= 0){
          transient Case_Monitor__c CM = new Case_monitor__c();
          CM.Case_state__c = 'Assigned FE/GSO';
          CM.Priority__c = Prio;
          CM.Avg_Age_of_Cases_in_State__c= ClosedAEStateToCaseAge.get('Assigned FE/GSO') / ClosedAEStateToCaseCount.get('Assigned FE/GSO');
          CM.Date_of_Sample__c = system.Today(); 
          CM_toInsert.add(CM);
          system.debug('Case Monitor to insert'+ CM);}
       
      if(ClosedAEStateToCaseCount.get('Assigned AE')!= 0){
          transient Case_Monitor__c CM1 = new Case_monitor__c();
          CM1.Case_state__c = 'Assigned AE';
          CM1.Priority__c = Prio;
          CM1.Avg_Age_of_Cases_in_State__c= ClosedAEStateToCaseAge.get('Assigned AE') / ClosedAEStateToCaseCount.get('Assigned AE');
          CM1.Date_of_Sample__c = system.Today(); 
          CM_toInsert.add(CM1);
          system.debug('Case Monitor to insert'+ CM1);}
      
      if(ClosedAEStateToCaseCount.get('Assigned R&D')!= 0){
          transient Case_Monitor__c CM2 = new Case_monitor__c();
          CM2.Case_state__c = 'Assigned R&D';
          CM2.Priority__c = Prio;
          CM2.Avg_Age_of_Cases_in_State__c= ClosedAEStateToCaseAge.get('Assigned R&D') / ClosedAEStateToCaseCount.get('Assigned R&D');
          CM2.Date_of_Sample__c = system.Today(); 
          CM_toInsert.add(CM2);
          system.debug('Case Monitor to insert'+ CM2);}
      
      if(ClosedAEStateToCaseCount.get('Waiting for Customer')!= 0){
          transient Case_Monitor__c CM3 = new Case_monitor__c();
          CM3.Case_state__c = 'Waiting for Customer';
          CM3.Priority__c = Prio;
          CM3.Avg_Age_of_Cases_in_State__c= ClosedAEStateToCaseAge.get('Waiting for Customer') / ClosedAEStateToCaseCount.get('Waiting for Customer');
          CM3.Date_of_Sample__c = system.Today(); 
          CM_toInsert.add(CM3);
          system.debug('Case Monitor to insert'+ CM3);}
      
      if(ClosedAEStateToCaseCount.get('Wait for Implemetation/Release')!= 0){      
          transient Case_Monitor__c CM4 = new Case_monitor__c();
          CM4.Case_state__c = 'Wait for Implemetation/Release';
          CM4.Priority__c = Prio;
          CM4.Avg_Age_of_Cases_in_State__c= ClosedAEStateToCaseAge.get('Wait for Implemetation/Release') / ClosedAEStateToCaseCount.get('Wait for Implemetation/Release');
          CM4.Date_of_Sample__c = system.Today(); 
          CM_toInsert.add(CM4);
          system.debug('Case Monitor to insert'+ CM4);}
      
      if(ClosedAEStateToCaseCount.get('Other')!= 0){      
          transient Case_Monitor__c CM5 = new Case_monitor__c();
          CM5.Case_state__c = 'Other';
          CM5.Priority__c = Prio;
          CM5.Avg_Age_of_Cases_in_State__c= ClosedAEStateToCaseAge.get('Other') / ClosedAEStateToCaseCount.get('Other');
          CM5.Date_of_Sample__c = system.Today(); 
          CM_toInsert.add(CM5);
          system.debug('Case Monitor to insert'+ CM5);}
 
   }
 
    
    
   
// Monitoring over all the closed cases escalated to AE, saving the total time they were open && their Priority                
  for(Case Cs1: [ SELECT id, case_age__c, Time_Esc2Ae_Close__c, RM_cases_count__c,  priority,  AE_PM_Escalated__c
                           FROM Case                 
                           WHERE state__c = 'Closed' AND Status ='Closed' ])
  { 
 
    if (Cs1.Priority!=NULL && Cs1.Time_Esc2Ae_Close__c != -1 && Cs1.Time_Esc2Ae_Close__c!= NULL){
    if (Cs1.AE_PM_Escalated__c=='AE')   
    {
        if(!ClosedAEPriorityToCaseCount.containsKey(Cs1.Priority.trim()))
            { 
            ClosedAEPriorityToCaseCount.put(Cs1.Priority.trim(),1);
            ClosedAEPriorityToCaseAge.put(Cs1.Priority.trim(),Cs1.Time_Esc2Ae_Close__c);
            }
        Else
        {
            ClosedAEPriorityCaseCount = ClosedAEPriorityToCaseCount.get(Cs1.Priority.trim());
            ClosedAEPriorityCaseAge = ClosedAEPriorityToCaseAge.get(Cs1.Priority.trim());
            ClosedAEPriorityToCaseCount.put(Cs1.Priority.trim(),ClosedAEPriorityCaseCount+1);
            ClosedAEPriorityToCaseAge.put(Cs1.Priority.trim(),ClosedAEPriorityCaseAge + Cs1.Time_Esc2Ae_Close__c);
        }
     }
// Monitoring over all the closed cases, saving the total time they were open && their Priority                    
  
        if(!ClosedPriorityToCaseCount.containsKey(Cs1.Priority.trim())  && Cs1.Priority!= NULL)
            { 
            ClosedPriorityToCaseCount.put(Cs1.Priority.trim(),1);
            ClosedPriorityToCaseAge.put(Cs1.Priority.trim(),Cs1.case_age__c);
            }
        Else if (Cs1.Priority!=NULL)
            {
            ClosedPriorityCaseCount = ClosedPriorityToCaseCount.get(Cs1.Priority.trim());
            ClosedPriorityCaseAge = ClosedPriorityToCaseAge.get(Cs1.Priority.trim());
            ClosedPriorityToCaseCount.put(Cs1.Priority.trim(),ClosedPriorityCaseCount+1);
            ClosedPriorityToCaseAge.put(Cs1.Priority.trim(),ClosedPriorityCaseAge + Cs1.case_age__c);
            }
                
    ClosedAEPriorityToCaseCount.put('High / Fatal' ,(ClosedAEPriorityToCaseCount.get('High')+ ClosedAEPriorityToCaseCount.get('Fatal')));
    ClosedAEPriorityToCaseAge.put('High / Fatal' ,(ClosedAEPriorityToCaseAge.get('High')+ ClosedAEPriorityToCaseAge.get('Fatal')));    
    ClosedAEPriorityToCaseCount.put('Low / Medium' ,(ClosedAEPriorityToCaseCount.get('Low')+ ClosedAEPriorityToCaseCount.get('Medium')));
    ClosedAEPriorityToCaseAge.put('Low / Medium' ,(ClosedAEPriorityToCaseAge.get('Low')+ ClosedAEPriorityToCaseAge.get('Medium')));
    
    ClosedPriorityToCaseCount.put('High / Fatal' ,(ClosedPriorityToCaseCount.get('High')+ ClosedPriorityToCaseCount.get('Fatal')));
    ClosedPriorityToCaseAge.put('High / Fatal' ,(ClosedPriorityToCaseAge.get('High')+ ClosedPriorityToCaseAge.get('Fatal')));             
    ClosedPriorityToCaseCount.put('Low / Medium' ,(ClosedPriorityToCaseCount.get('Low')+ ClosedPriorityToCaseCount.get('Medium')));
    ClosedPriorityToCaseAge.put('Low / Medium' ,(ClosedPriorityToCaseAge.get('Low')+ ClosedPriorityToCaseAge.get('Medium')));
  }}    

    
    
  for(Case Cs1: [select id , Account.Name, priority ,RM_cases_count__c, State_Update_Time__c, AE_PM_Escalated__c ,case_source__c, case_age__c, state__c, case_type__c, AE_state__c, FM_state__c, State_WaitForRelease_Time_Counter__c , Account_Project__c
               from Case 
               where state__c != 'Closed' and Status != 'Closed'])
   {
       if(Cs1.Priority!=NULL) { 
// Monitoring over the Ages of cases which are not ' Waiting for Customer Approval'/'On Hold'  /'Feature-Request/Doc-Change' /'Wait for Release' 
     if (Cs1.Priority!=NULL && Cs1.state__c !='Waiting for Customer Approval' && Cs1.state__c !='On Hold'  && Cs1.state__c !='Feature-Request/Doc-Change' && Cs1.state__c !='Wait for Release' )
     {
             if(!PriorityToCaseCount.containsKey(Cs1.Priority.trim()))
              { PriorityToCaseCount.put(Cs1.Priority.trim(),1);
                PriorityToCaseAge.put(Cs1.Priority.trim(),Cs1.case_age__c);
              }
              Else
               {
                PriorityCaseCount = PriorityToCaseCount .get(Cs1.Priority.trim());
                PriorityCaseAge = PriorityToCaseAge .get(Cs1.Priority.trim());
                PriorityToCaseCount.put(Cs1.Priority.trim(),PriorityCaseCount+1);
                PriorityToCaseAge.put(Cs1.Priority.trim(),PriorityCaseAge+Cs1.case_age__c);
               }
     }
     

// Monitoring over the Ages of cases which are not '  Waiting for Customer Approval'/'On Hold'  /'Feature-Request/Doc-Change' /'Wait for Release' and were escalated to AE     
     if (Cs1.Priority!=NULL && Cs1.AE_PM_Escalated__c=='AE' && Cs1.state__c !='Waiting for Customer Approval' && Cs1.state__c !='On Hold'  && Cs1.state__c !='Feature-Request/Doc-Change' && Cs1.state__c !='Wait for Release')
     {
             if(!AEPriorityToCaseCount.containsKey(Cs1.Priority.trim()))
              { AEPriorityToCaseCount.put(Cs1.Priority.trim(),1);
                AEPriorityToCaseAge.put(Cs1.Priority.trim(),Cs1.case_age__c);
              }
              Else
               {
                AEPriorityCaseCount = AEPriorityToCaseCount.get(Cs1.Priority.trim());
                AEPriorityCaseAge = AEPriorityToCaseAge.get(Cs1.Priority.trim());
                AEPriorityToCaseCount.put(Cs1.Priority.trim(),AEPriorityCaseCount+1);
                AEPriorityToCaseAge.put(Cs1.Priority.trim(),AEPriorityCaseAge+Cs1.case_age__c);
               }
     }
    

// Monitoring over the Ages of cases which are not '  Waiting for Customer Approval'/'On Hold'  /'Feature-Request/Doc-Change' /'Wait for Release' and were escalated to AE which have open redmine cases   
     if ( Cs1.Priority!=NULL && (Cs1.RM_cases_count__c != 0  &&  Cs1.RM_cases_count__c != NULL) && Cs1.AE_PM_Escalated__c=='AE' && CS1.state__c !='Waiting for Customer Approval' && Cs1.state__c !='On Hold'  && Cs1.state__c !='Feature-Request/Doc-Change' && Cs1.state__c !='Wait for Release')
     {
             if(!RMPriorityToCaseCount.containsKey(Cs1.Priority.trim()))
              { RMPriorityToCaseCount.put(Cs1.Priority.trim(),1);
                RMPriorityToCaseAge.put(Cs1.Priority.trim(),Cs1.case_age__c);
              }
              Else
               {
                RMPriorityCaseCount = RMPriorityToCaseCount.get(Cs1.Priority.trim());
                RMPriorityCaseAge = RMPriorityToCaseAge.get(Cs1.Priority.trim());
                RMPriorityToCaseCount.put(Cs1.Priority.trim(),RMPriorityCaseCount+1);
                RMPriorityToCaseAge.put(Cs1.Priority.trim(),RMPriorityCaseAge+Cs1.case_age__c);
               }
     }
    
    PriorityToCaseCount.put('Low / Medium' ,(PriorityToCaseCount.get('Low')+ PriorityToCaseCount.get('Medium')));
    PriorityToCaseAge.put('Low / Medium' ,(PriorityToCaseAge.get('Low')+ PriorityToCaseAge.get('Medium')));
    
    PriorityToCaseCount.put('High / Fatal' ,(PriorityToCaseCount.get('High')+ PriorityToCaseCount.get('Fatal')));
    PriorityToCaseAge.put('High / Fatal' ,(PriorityToCaseAge.get('High')+ PriorityToCaseAge.get('Fatal')));
    
    AEPriorityToCaseCount.put('Low / Medium' ,(AEPriorityToCaseCount.get('Low')+ AEPriorityToCaseCount.get('Medium')));
    AEPriorityToCaseAge.put('Low / Medium' ,(AEPriorityToCaseAge.get('Low')+ AEPriorityToCaseAge.get('Medium')));
    
    AEPriorityToCaseCount.put('High / Fatal' ,(AEPriorityToCaseCount.get('High')+ AEPriorityToCaseCount.get('Fatal')));
    AEPriorityToCaseAge.put('High / Fatal' ,(AEPriorityToCaseAge.get('High')+ AEPriorityToCaseAge.get('Fatal')));     
    
    RMPriorityToCaseCount.put('Low / Medium' ,(RMPriorityToCaseCount.get('Low')+ RMPriorityToCaseCount.get('Medium')));
    RMPriorityToCaseAge.put('Low / Medium' ,(RMPriorityToCaseAge.get('Low')+ RMPriorityToCaseAge.get('Medium')));
    
    RMPriorityToCaseCount.put('High / Fatal' ,(RMPriorityToCaseCount.get('High')+ RMPriorityToCaseCount.get('Fatal')));
    RMPriorityToCaseAge.put('High / Fatal' ,(RMPriorityToCaseAge.get('High')+ RMPriorityToCaseAge.get('Fatal')));    
       }
     
// Monitoring over the States of cases
   if(!StateToCases.containsKey(Cs1.State__c.trim()))
    { StateToCases.put(Cs1.State__c.trim(),1);}
   else
    {
    StateCaseCount = StateToCases.get(Cs1.State__c.trim());
     StateToCases.put(Cs1.State__c.trim(),StateCaseCount+1);
    }
   StateToCases.put('Assigned FE/GSO' ,StateToCases.get('Assigned'));
   StateToCases.put('Wait for Implemetation/Release' , (StateToCases.get('Wait for Implementation')+ StateToCases.get('Wait for Release')));

// Defining that AE Assigned are the cases Assigned AE but NOT to R&D. 
// Defining that R&D Assigned are the cases Assigned R&D but NOT to AE.
   if(Cs1.State__c == 'Assigned AE' )
   { 
         if( Cs1.RM_cases_count__c >0)
          {AEState = 'R&D Assigned'; }    
         else
          {AEState = 'AE Assigned'; }   
           
         if(!StateToCases.containsKey(AEState))
         { StateToCases.put(AEState,1);}
         else
          {
           StateCaseCount = StateToCases.get(AEState);
           StateToCases.put(AEState,StateCaseCount+1);
          }      
    }  

// monitors to calculated the AVG time an opened case stays in 'Wait for Release'     
    if (Cs1.state__c =='Wait for Release' )
    {date dt= date.valueof(Cs1.State_Update_Time__c);
     date ST=system.today();
     integer a = dt.daysBetween(ST);
     TotalTimeinWaitforRelease = TotalTimeinWaitforRelease + Cs1.State_WaitForRelease_Time_Counter__c + a;
     TotalOpenCasesInWFR= TotalOpenCasesInWFR+1;}
  
       
       
       if (Cs1.Account.Name!= null){ 
    if (( Cs1.state__c=='Assigned AE' || Cs1.state__c=='Assigned') && Cs1.Account.Name.toLowerCase().contains('ibm'))
    {
         if(!CasesPerIBMProject.containsKey(Cs1.Account_Project__c))
          { CasesPerIBMProject.put(Cs1.Account_Project__c,1); }
          Else
           {
            CountCasesPerIBMProject = CasesPerIBMProject.get(Cs1.Account_Project__c);
            CasesPerIBMProject.put(Cs1.Account_Project__c,CountCasesPerIBMProject+1);
           }
    }}



       
}


  
 for(string States:StateToCases.keySet())
 {
      Case_Monitor__c CM = new Case_monitor__c();
      CM.Case_state__c = States;
      CM.Case_Count__c = StateToCases.get(States);
      CM.Date_of_Sample__c = system.Today(); 
      CM_toInsert.add(CM);
  }
  
  
  for(string Prio:PriorityToCaseCount.keySet())
  {
      Case_Monitor__c CM = new Case_monitor__c();
      CM.Priority__c = Prio;
      CM.Case_Count__c = PriorityToCaseCount.get(Prio);
      CM.Total_age__c = PriorityToCaseAge.get(Prio);
      CM.Date_of_Sample__c = system.Today(); 
      CM_toInsert.add(CM);
  }
  
  for(string Prio:AEPriorityToCaseCount.keySet())
  {
      Case_Monitor__c CM = new Case_monitor__c();
      CM.Priority__c = Prio;
      CM.AE_Escalated_Cases__c = AEPriorityToCaseCount.get(Prio);
      CM.Total_age__c = AEPriorityToCaseAge.get(Prio);
      CM.Date_of_Sample__c = system.Today(); 
      CM_toInsert.add(CM);
  } 
  
  for(string Prio:RMPriorityToCaseCount.keySet())
  {
      Case_Monitor__c CM = new Case_monitor__c();
      CM.Priority__c = Prio;
      CM.RD_Assigned__c = RMPriorityToCaseCount.get(Prio);
      CM.Total_age__c = RMPriorityToCaseAge.get(Prio);
      CM.Date_of_Sample__c = system.Today(); 
      CM_toInsert.add(CM);
  } 
  for(string Prio:ClosedAEPriorityToCaseCount.keySet())
  {
      Case_Monitor__c CM = new Case_monitor__c();
      CM.Priority__c = Prio;
      CM.Case_Closed_AE_Escalated_Count__c = ClosedAEPriorityToCaseCount.get(Prio);
      CM.Total_age__c = ClosedAEPriorityToCaseAge.get(Prio);
      CM.Date_of_Sample__c = system.Today(); 
      CM_toInsert.add(CM);
  } 
  
  for(string Prio:ClosedPriorityToCaseCount.keySet())
  {
      Case_Monitor__c CM = new Case_monitor__c();
      CM.Priority__c = Prio;
      CM.Case_Closed_Count__c = ClosedPriorityToCaseCount.get(Prio);
      CM.Total_age__c = ClosedPriorityToCaseAge.get(Prio);
      CM.Date_of_Sample__c = system.Today(); 
      CM_toInsert.add(CM);
  } 

  for(string Project:CasesPerIBMProject.keySet())
  {
      Case_Monitor__c CM = new Case_monitor__c();
      CM.Project__c = Project;
      CM.Number_of_Cases_Per_Project__c = CasesPerIBMProject.get(Project);
      CM.Date_of_Sample__c = system.Today(); 
      CM_toInsert.add(CM);
  } 
 
    
    Case_Monitor__c CM1 = new Case_monitor__c(); 
    CM1.AVG_time_in_wait_for_release__c=TotalTimeinWaitforRelease/TotalOpenCasesInWFR;
    CM1.Date_of_Sample__c = system.Today(); 
    if (CM1.AVG_time_in_wait_for_release__c >0)
    {CM_toInsert.add(CM1);}
    insert CM_toInsert;

  }
}