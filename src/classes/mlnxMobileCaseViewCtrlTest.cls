/******************************************************************************
 * Tests against the mlnxMobileCaseViewCtrl class & mlnxMobileCaseView VF Page
 *
 * @Author: 	Abdul Sattar (Magnet 360)
 * @Date: 		09.09.2015
 * 
 * @Updates:
 *
 */
@isTest
private class mlnxMobileCaseViewCtrlTest {

    // Tests against mlnxMobileCaseViewCtrl.saveComment()
    private static testMethod void caesViewSaveCommentTest() {
    	// Record Type for all Mobile Cases must be Technical Support
        List<RecordType> caseRts = [SELECT Id FROM RecordType WHERE DeveloperName = 'TechnicalSupport' LIMIT 1];
        System.assert(caseRts[0].Id != NULL, 'Unable to get Technical Support Case Record Type.');

    	// Create test case
    	Case testCase = MyMellanoxTestUtility.getCases(1, true, caseRts[0])[0];
        
        // Create a page reference and set as test page.
    	PageReference caseViewPage = Page.mlnxMobileCaseView;
	    Test.setCurrentPage(caseViewPage); 

		// Run tests  & perform asserts
		Test.startTest();

	    	ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
	        mlnxMobileCaseViewCtrl controller = new mlnxMobileCaseViewCtrl(sc);
	    	
	    	System.assert(controller.currentCase.Id != NULL, 'Case view not initialized.');
	        System.assert(controller.newComment != NULL, 'New Comment not initialized properly.');
	        controller.newComment.CommentBody = 'Test Comment';

	        String returnURL = controller.saveComment().getURL();
	        System.assert(returnURL.contains('apex/mlnxMobileCaseView?Id='), 'Comment saved but redirection URL not valid.');

		Test.stopTest();	
	}

	// Tests against mlnxMobileCaseViewCtrl.saveComment()
    private static testMethod void closeCaseTest() {
    	// Record Type for all Mobile Cases must be Technical Support
    	List<RecordType> caseRts = [SELECT Id FROM RecordType WHERE DeveloperName = 'TechnicalSupport' LIMIT 1];
        System.assert(caseRts[0].Id != NULL, 'Unable to get Technical Support Case Record Type.');

    	// Create test case
    	Case testCase = MyMellanoxTestUtility.getCases(1, true, caseRts[0])[0];
        
        // Create a page reference and set as test page.
    	PageReference caseViewPage = Page.mlnxMobileCaseView;
	    Test.setCurrentPage(caseViewPage); 

		// Run tests  & perform asserts
		Test.startTest();

	    	ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
	        mlnxMobileCaseViewCtrl controller = new mlnxMobileCaseViewCtrl(sc);
	    	
	    	System.assert(controller.currentCase.Id != NULL, 'Case view not initialized.');

	        controller.closeCase();
	       // System.assert(pr == NULL, 'Unable to close case.');
	        
		Test.stopTest();	
	}
}