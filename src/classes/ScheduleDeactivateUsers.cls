global class  ScheduleDeactivateUsers implements Schedulable 
{
    global void execute(SchedulableContext sc) {
	    date yearBefore = date.today().addYears(-1);
	    system.debug('==>yearBefore: '+yearBefore);  
	    system.debug('==>the users list '+[Select Id From User u where IsActive = true and Do_Not_Deactivate__c = false and UserType='PowerCustomerSuccess' and (LastLoginDate <: yearBefore or LastLoginDate = null)] )  ;
	                                    
	    list<User> lst_Users = new list<User>();
	    for( User u:[Select Id From User u where IsActive = true and Do_Not_Deactivate__c = false and UserType='PowerCustomerSuccess' and (LastLoginDate <: yearBefore or (LastLoginDate = null and CreatedDate <:yearBefore) )] )  
	    {
	    	User tempUser = new User(Id = u.Id , IsActive = false);
	    	lst_Users.add(tempUser);
	    }                           
	    if(!lst_Users.Isempty())
	    {
	    	try{
	    		update lst_Users;
	    	}
	    	catch(exception e)
	    	{
	    		system.debug('===>exception e: '+e);
	    	}
	    }       
    }
   
}