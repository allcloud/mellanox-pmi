public with sharing class MPCProductCategoryOverrideController {
	private final Product_Category__c record;
	   
	public MPCProductCategoryOverrideController(ApexPages.StandardController 
		   controller) {
		   		List <String> fields = new List <String>{'Product_Family__c'};
		   		controller.addFields(fields);
		   		this.record = (Product_Category__c)controller.getRecord();
		   }
	
	public boolean inCommunity() {
		return Network.getNetworkId() != null;
	}
	
	public PageReference redirect() {
		if (inCommunity()) 
		{
			PageReference customPage =  Page.SupportProductFamily;
			customPage.setRedirect(true);
			customPage.getParameters().put('pfid', record.Product_Family__c);
			customPage.getParameters().put('pfcid', record.Id);
			return customPage;
		} else {
			return null; //otherwise stay on the same page  
		}
	}
}