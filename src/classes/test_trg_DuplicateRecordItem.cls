@isTest(seeAllData=true)
public class test_trg_DuplicateRecordItem {
	
	static testMethod void test_updateDupRecordItemsAccSource() {
		
		DuplicateRule dupe = [SELECT Id FROM DuplicateRule limit 1];
		
		DuplicateRecordSet recSet = new DuplicateRecordSet();
		recSet.DuplicateRuleId = dupe.Id;
		insert recSet;
		
		Account theAcc = [SELECT Id, Source__c FROM Account WHERE Source__c != null limit 1];
		
		DuplicateRecordItem dupeItem = new DuplicateRecordItem();
		dupeItem.DuplicateRecordSetId = recSet.Id;
		dupeItem.RecordId = theAcc.Id;
		 
		Test.StartTest();
		
		insert dupeItem;
		
		Test.StopTest();
	}   
}