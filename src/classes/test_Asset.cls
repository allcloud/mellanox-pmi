@isTest(seeAllData=true)
private class test_Asset{

    static testMethod void myUnitTest() {
        
       Account acc = [Select a.Id From Account a where recordtypeId = '01250000000DP1n' order by createdDate desc limit 1];
        
       List<Contract2__c> cList = new List<Contract2__c>();
       map<id,id> ContractToRoll = new map<id,id>();
       Contract2__c c2 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8, Contract_Renew_RollUp_to__c =NULL);
        Contract2__c c3 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
         Contract2__c c4 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
   
   
       
       cList.add(c2);
       cList.add(c3);
       cList.add(c4);
       insert cList;             
       ContracttoRoll.put(c2.id,c3.id);                     
                                                                                                                                                                  
       c2.Contract_Renew_RollUp_to__c = c3.id;
       update(c2);
       List<Asset2__c> aList = new  List<Asset2__c>();
       Asset2__c a = new Asset2__c(Name ='test', contract2__c=c2.Id,Asset_Type__c = 'Support', part_number__c = 'weryu1B');
        Asset2__c a1 = new Asset2__c(Name ='test', contract2__c=c2.Id, Asset_Type__c = 'Support', part_number__c = 'weryu2B');
        aList.add(a1);
       aList.add(a);
       
       Asset2__c a2 = new Asset2__c(Name ='test1', contract2__c=c3.Id,Asset_Type__c = 'Support', part_number__c = 'weryu5SP');
      aList.add(a2);
       
       Asset2__c a3 = new Asset2__c(Name ='test1', contract2__c=c4.Id,Asset_Type__c = 'Support', part_number__c = 'weryu4B');
       aList.add(a3);
       insert aList;
       
       delete aList;           

      
      List <Asset2__c> assts = new List<Asset2__c>();
      for(integer i = 0; i<200; i++){
         Asset2__c ast = new Asset2__c(name='testAsset2'+'i', contract2__c = c2.id ); 
         assts.add(ast);
      }
   
   insert assts;
   
   Test.StartTest();
   AssetReassignment reassign = new AssetReassignment();
   reassign.query='select id,name , ASSET_FAMILY__C,  ASSET_TYPE__C, ASSET_KIND__C, BACKLOG__C, CITY_STATE__C,  CITY__C,COMMENTS_SPECIAL_HANDLING__C,' + 
 'CONTRACT_YEARS__C,  COUNTRY__C, CUST_PART_NUMBER__C,    CUSTOMER_CATEGORY_1__C, CUSTOMER_CATEGORY_3__C, CUSTOMER_CATEGORY_4__C, CUSTOMER_CATEGORY_5__C,'+
 'CUSTOMER_CATEGORY_6__C, CUSTOMER_NAME__C,   CUSTOMER_NO__C, CUSTOMER_S_PURCH_ORD__C,    DATE_LOANED__C, DATE_REQUESTED__C,  DATE__C,    DOC_MONTH__C,'+
 'DOC_QUTER__C,   DOC_YEAR__C,    DOCUMENT__C,    EOL_CHECK__C,   EXTENDED_WARRANTY_YEARS__C, FAX_NUMBER__C,  INTERNAL_STD_COST__C,'+ 
  'LN__C,  LOANER_SAMPLE_TYPE__C,  LOCATION__C,    MELLANOX_CONTACT__C,    MELLANOX_DEPARTMENT__C, NOTIFY_DATA__C, OLD_ID__C,  OPPORTUNITY_NUMBER__C,'+  
 'ORD_MONTH__C,   ORD_QUT__C, ORD_YEAR__C,    ORDER__C,   ORDER_TYPE__C,  PART_DESCRIPTION__C,    PART_NUMBER__C, PAYMENT_TERMS__C,   PENDING_RETURN_DATE__C, PHONE_NUMBER__C,'+      
  'PRICE__C,   PRODUCT_FAMILY__C,  QTY_FACTORY_UNITS__C,   QUANTITY_SHIPPED__C,    QUANTITY_TOTAL__C,  REFERENCE_MELLANOX_SO__C,   REMARKS__C, RENEWAL_CUSTOMER_S_PURCH_ORD__C,'+     
  'RENEWAL_ORDER__C,   RENEWAL_STATUS__C,  SAA14RMACOUNT__C,   SAA3_RMA_PART__C,   SAA_IGNORE__C,  SLS1__C,    SLS2__C,    SLS3__C,    SLS4__C,    SERIAL_NUMBER__C,   SERIAL_NUMBER_S__C,'+  
  'SHIP_TO_ADDRESS__C, SITE__C,    STATE__C,   STATUSDESCRIPTION__C,   STATUS__C,  STREET_ADDRESS__C,  UNDER_CONTRACT__C,  WRITE_OFF__C,   ZIP_CODE__C,    EXPECTED_SHIP_DATE__C,  SHIP_STATUS__C, SHIP_TO_CUSTOMER__C,  MLNX_ASSET__C,  Contract2__c FROM Asset2__c WHERE contract2__c = \''+C2.id + '\'';              
   reassign.NewContract = contractToRoll;
    ID batchprocessid = Database.executeBatch(reassign);
   Test.StopTest();
                                                                      
    }
    
    static testMethod void test_updateAssetsRelatedContractERIApproved() {
    	
    	Account acc = [Select a.Id From Account a where recordtypeId = '01250000000DP1n' order by createdDate desc limit 1];
        
       List<Contract2__c> cList = new List<Contract2__c>();
       map<id,id> ContractToRoll = new map<id,id>();
       Contract2__c c2 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8, Contract_Renew_RollUp_to__c =NULL);
        Contract2__c c3 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
         Contract2__c c4 = new Contract2__c(Account__c=acc.Id, Contract_Term_months__c = 8);
        
   
       Quote_Support_OPN__c relatedOPN = [SELECT Id, Product_OPN__c FROM Quote_Support_OPN__c WHERE Product_OPN__c != null limit 1];
       
       
       cList.add(c2);
       cList.add(c3);
       cList.add(c4);
       insert cList;             
       ContracttoRoll.put(c2.id,c3.id);                     
                                                                                                                                                                  
       c2.Contract_Renew_RollUp_to__c = c3.id;
       update(c2);
       List<Asset2__c> aList = new  List<Asset2__c>();
       Asset2__c a = new Asset2__c(Name ='test', contract2__c=c2.Id,Asset_Type__c = 'Support', part_number__c = 'weryu1B');
        Asset2__c a1 = new Asset2__c(Name ='test', contract2__c=c2.Id, Asset_Type__c = 'Support', part_number__c = relatedOPN.Product_OPN__c);
        aList.add(a1);
       aList.add(a);
       
       Asset2__c a2 = new Asset2__c(Name ='test1', contract2__c=c3.Id,Asset_Type__c = 'Support', part_number__c = 'weryu5SP');
      aList.add(a2);
       
       Asset2__c a3 = new Asset2__c(Name ='test1', contract2__c=c4.Id,Asset_Type__c = 'Support', part_number__c = 'weryu4B', Asset_Family__c = 'SW');
       aList.add(a3);
       
       test.startTest();
       
       insert aList;
       
       delete aList;
       
       test.stopTest();  
    }
    
    static testMethod void test_relateNewAssetsWithProducts() {
    	
    	Account Acc1 = [Select a.Id From Account a where recordtypeId = '01250000000DP1n' order by createdDate desc limit 1];
         
        Contract2__c Contact1 = new Contract2__c(Contract_Term_months__c = 12 ,Account__c = Acc1.Id);
        insert Contact1;
        
        Product2 Pr1 = new Product2(name = '00373', Inventory_Item_id__c = '1234567');
        Insert Pr1;
        
        Asset2__c a2 = new Asset2__c(Name ='test1', contract2__c = Contact1.Id, Asset_Type__c = 'Support', part_number__c = '00373');
        
		Test.StartTest();
		
		insert a2;
		
		Test.StopTest();
    }
}