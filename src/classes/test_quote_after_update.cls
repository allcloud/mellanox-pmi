/**

 */
@isTest
private class test_quote_after_update {


    static testMethod void myUnitTest() {
       Account acc1 = new Account(name = 'test22',support_center__c = 'Global',is_oracle_parent__c = 'Yes',  type = 'OEM', RecordTypeID = '01250000000DP1n');
        insert acc1;

       
        Opportunity op = new Opportunity(ownerid =ENV.Sales_Directors.get('Ron Aghazarian'),Primary_partner_N__c = acc1.id, Required_Ship_Date__c = System.today()+20, Name='Test Opp', StageName='Open',CloseDate=Date.Today()+30);
        insert op;

        Pricebook2 p = new Pricebook2(Name = 'TestPriceBook', IsActive = true);
        insert p;
        
        Pricebook2 pricebookStandard = [Select p.Name, p.Id From Pricebook2 p where p.Name like 'Standard%' limit 1][0];
        
        op.Pricebook2Id = pricebookStandard.Id;
        update op;
        
        Pricebook2 priceb = [Select p.Name, p.Id From Pricebook2 p limit 1][0];
        op.Pricebook2Id = priceb.Id;
        op.type = 'Direct'; 
        op.Primary_partner_N__c =NULL;
        update op;
        
        Product2 product = new Product2(Name = 'Test Product', Inventory_Item_id__c ='123450987', OEM_PL__C=100);
        insert product; 
        
        PricebookEntry pre = new PricebookEntry(Product2Id = product.Id,Pricebook2Id = pricebookStandard.Id,UnitPrice = 100,UseStandardPrice = false,IsActive=true);
        insert pre;
        
        Quote q = new Quote(OpportunityId = op.Id,Name='Test Quate', Pricebook2Id = pricebookStandard.Id);
        insert q; 
        
        QuoteLineItem qli = new QuoteLineItem (QuoteId = q.Id, PricebookEntryId = pre.Id,Quantity = 1,UnitPrice = 1000);
        insert qli; 

        List<Quote> lst_QForTest = [Select Id, New_Price_Book__c, (Select Id, PricebookEntry.Pricebook2Id, PricebookEntry.Product2Id, PricebookEntry.UnitPrice, QuoteId, PricebookEntryId, New_Price__c,Minimum_Price__c From QuoteLineItems)
                                    From Quote q
                                    where Id='00k5000000Az595' limit 1 ];
                                    
        if (lst_QForTest.size() > 0)
        {
            Quote qq = lst_QForTest[0];
            qq.New_Price_Book__c = 'Certified Reseller Price Book';
            qq.Big_Cluster_Opportunity__c = true;
            update qq;
            
                    
            List<QuoteLineItem> lst_qitem = qq.QuoteLineItems;
            
            if (lst_qitem.size() > 0)
            {
                QuoteLineItem qitem = lst_qitem[0];
                QuoteLineItem ql = new QuoteLineItem (QuoteId = qitem.QuoteId, PricebookEntryId = qitem.PricebookEntryId, Quantity = 1,UnitPrice = 1000);
                insert ql; 
            } 
        }
		//KN added
		Product2 product3 = new Product2(Name = 'Test Product 3', Inventory_Item_id__c ='123450988', OEM_PL__C=100,product_type1__c='SWITCH');
        insert product3;
        PricebookEntry pre2 = new PricebookEntry(Product2Id = product3.Id,Pricebook2Id = pricebookStandard.Id,UnitPrice = 100,IsActive=true);
        insert pre2;
        Quote q3 = new Quote(OpportunityId = op.Id,Name='Test Quote 3', Pricebook2Id = pricebookStandard.Id,Account__c=acc1.ID,Node_Count__c=60,Status='Draft');
        insert q3; 
        QuoteLineItem qli3 = new QuoteLineItem (QuoteId = q3.Id, PricebookEntryId = pre2.Id,Quantity = 100,UnitPrice = 99);
        insert qli3;
        
        q3.Node_Count__c = 360;
        q3.GPS_Required__c = true;
        update q3;
        //System.assertEquals([select count() from QuoteLineItem where QuoteID=:q3.ID], 2);
        
        q3.Status = 'Approved';
        update q3;
		System.assertEquals([select Approved_Price__c from QuoteLineItem where ID=:qli3.ID].Approved_Price__c, 99);
		System.assertEquals([select Approved_Qty__c from QuoteLineItem where ID=:qli3.ID].Approved_Qty__c, 100);
		
		qli3.Quantity = 99;
		qli3.UnitPrice = 100;
		update qli3;
		System.assertEquals([select Status from Quote where ID=:qli3.QuoteID].Status, 'Approved');
		
		/*
		qli3.Quantity = 94;
		update qli3;
		System.assertEquals([select Status from Quote where ID=:qli3.QuoteID].Status, 'Draft');		         
		
		q3.Status = 'Approved';
        update q3;
        qli3.UnitPrice = 99;
		update qli3;
		System.assertEquals([select Status from Quote where ID=:qli3.QuoteID].Status, 'Draft');		
		*/        
    }
}