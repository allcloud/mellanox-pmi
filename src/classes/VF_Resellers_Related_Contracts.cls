public class VF_Resellers_Related_Contracts {
	
	public list<Contract2__c> relatedContracts_List {get;set;}
	private list<Contract2__c> tempContract_List;
	public list<SelectOption> optionList {get;set;}
	public String selectedView {get;set;}
	private String query_Str {get;set;}
	private Id AccountId {get;set;}
	public boolean displayLink2Contract {get;set;}
	private User currentUser;
	private boolean isResellerUser;
	private String ordrByStr {get;set;}
	public Date startDate {get;set;}
	public Date endDate {get; set;}
 	private String query_StrRelated;
 	
 	
 	public VF_Resellers_Related_Contracts() {
		
		Id currentUserId = system.UserInfo.getUserId();
		displayLink2Contract = false;
		currentUser = [SELECT Id, Contact.AccountId FROM User WHERE Id =: currentUserId];
		startDate = date.today();
		endDate = date.today();
		
		if (currentUser.Contact.AccountId != null) {
			
			ordrByStr = ' Order by EndDate__c asc ';
			//redirectNonResellersUsersToStandardContractsPage(currentUser);
			
			displayLink2Contract = true;
			AccountId = currentUser.Contact.AccountId;
			
			 				 
			query_Str = 'SELECT Id, Name, Account__r.Name, Account__r.Id, CTF__c, Contract_Start_Date__c, EndDate__c, Renew_Contract__c,' + 
									 'Contract_Term_months__c, Order__c, Status__c FROM Contract2__c WHERE Account__c = \'' + AccountId + '\'' ;
									 
			system.debug('query_Str : ' + query_Str);							 
									 
			relatedContracts_List = Database.query(query_Str + ordrByStr);						 						 
			tempContract_List = relatedContracts_List.clone();						 
		}
	} 
	
	
	public list<SelectOption> getSelectContracts() {
		
		selectedView = 'My Contracts';
		optionList = new list<SelectOption>();
		optionList.add( new SelectOption('My Contracts', 'My Contracts') );
		Account relatedAccount = new Account();
		
		
		try {
		
			 relatedAccount = [SELECT Id, Customer_Type__c FROM Account WHERE Id =: currentUser.Contact.AccountId];
		}
		
		catch(Exception e) {
			
			
		}
			
		if (relatedAccount.Customer_Type__c != null && relatedAccount.Customer_Type__c.EqualsIgnoreCase('Reseller')) {
			
			isResellerUser = true;	
			optionList.add( new SelectOption('My Customers Contracts', 'My Customers Contracts') );
			optionList.add( new SelectOption('My Customers Active Contracts', 'My Customers Active Contracts') );
			optionList.add( new SelectOption('My Customers Expired Contracts', 'My Customers Expired Contracts') );
			optionList.add( new SelectOption('My Customers Draft Contracts', 'My Customers Draft Contracts') );
		}
		
		else {
			
			isResellerUser = false;	
			optionList.add( new SelectOption('My Active Contracts', 'My Active Contracts') );
			optionList.add( new SelectOption('My Expired Contracts', 'My Expired Contracts') );
		}
		
		optionList.add( new SelectOption('My Online Contract Renewals', 'My Online Contract Renewals') );
		 
		optionList.add( new SelectOption('Contracts To Renew Online', 'Contracts To Renew Online') );
		
		return optionList;
	}  
	
	// This method will filter the retreived Contracts according to the User's selection
	public void fillTheRelatedContactsList() {
		
 	    
	    if (isResellerUser) {
	    	query_StrRelated = 'SELECT Id, Name, Account__r.Name, Account__r.Id, CTF__c, Contract_Start_Date__c, EndDate__c, Renew_Contract__c,' + 
					       'Contract_Term_months__c, Order__c, Status__c FROM Contract2__c WHERE Partner_Account__c = \'' + AccountId +  '\'';
	    }
	    
	    else {
	    	
	    	query_StrRelated = 'SELECT Id, Name, Account__r.Name, Account__r.Id, CTF__c, Contract_Start_Date__c, EndDate__c, Renew_Contract__c,' + 
					       'Contract_Term_months__c, Order__c, Status__c FROM Contract2__c WHERE Account__c = \'' + AccountId +  '\'';
	    }
		
		if (selectedView.EqualsIgnoreCase('My Contracts')) {
			
			relatedContracts_List = Database.query(query_Str + ordrByStr);
			displayLink2Contract = true;
		}
		
		else if (selectedView.contains('Active Contracts')) {
			
			query_StrRelated += ' and Status__c = ' + '\'' + 'Activated' + '\'' ;
			
			system.debug('query_Str : ' + query_StrRelated);
			relatedContracts_List = Database.query(query_StrRelated + ordrByStr);
			displayLink2Contract = true;	
		}
		
		else if (selectedView.contains('Expired Contracts')) {
			
			query_StrRelated += ' and Status__c = ' + '\'' + 'Expired' + '\'' ;
			
			system.debug('query_Str : ' + query_StrRelated);
			relatedContracts_List = Database.query(query_StrRelated + ordrByStr);
			displayLink2Contract = true;	
		}
		
		else if (selectedView.EqualsIgnoreCase('My Customers Draft Contracts')) {
			
			query_StrRelated += ' and Status__c = ' + '\'' + 'Draft' + '\'' ;
			
			system.debug('query_Str : ' + query_StrRelated);
			relatedContracts_List = Database.query(query_StrRelated + ordrByStr);
			displayLink2Contract = false;	
		}
		
		else if (selectedView.EqualsIgnoreCase('My Customers Contracts')) {
			
			relatedContracts_List = Database.query(query_StrRelated + ordrByStr);
			displayLink2Contract = false;
		}
		
		else if (selectedView.EqualsIgnoreCase('Contracts To Renew Online')) {
			
			query_StrRelated += ' and Renew_Contract__c != ' + '\'' + null + '\'' ;
			system.debug('query_StrRelated : ' + query_StrRelated);
			relatedContracts_List = Database.query(query_StrRelated + ordrByStr);
				
			if (!isResellerUser) {	
				displayLink2Contract = true;
			}
			
			else {
				
				displayLink2Contract = false;
			}
		}
		
	    if (selectedView.EqualsIgnoreCase('My Customers Active Contracts') || selectedView.EqualsIgnoreCase('My Customers Expired Contracts')) {
			
			displayLink2Contract = false;
		}
		
		else if (selectedView.EqualsIgnoreCase('My Online Contract Renewals')) {
			
			query_StrRelated += ' and ERI_Approve1__c = ' + '\'' + 'Approved' + '\'';
			system.debug('query_StrRelated : ' + query_StrRelated);
			relatedContracts_List = Database.query(query_StrRelated + ordrByStr);
				
				if (!isResellerUser) {	
				displayLink2Contract = true;
			}
			
			else {
				
				displayLink2Contract = false;
			}
		}
		
		tempContract_List = relatedContracts_List.clone();
	}
	
	public pageReference redirectNonResellersUsersToStandardContractsPage() {
		
		Id currentUserId = system.UserInfo.getUserId();
		User currentUser = [SELECT Id, Contact.AccountId FROM User WHERE Id =: currentUserId];
		
		if (currentUser.Contact.AccountId != null) {	
			Account relatedAccount = [SELECT Id, Customer_Type__c FROM Account WHERE Id =: currentUser.Contact.AccountId];
			
			if (relatedAccount.Customer_Type__c != null && !relatedAccount.Customer_Type__c.EqualsIgnoreCase('Reseller')) {
				
			//	Pagereference thePage = new Pagereference('/support/a0M/o');   
			//    thePage.setRedirect(true);
			//    return thePage;
			}
		}
		
		return null;
	}
	
	public void filterByDates() {
		
 		relatedContracts_List = new list<Contract2__c>();
		
		for (Contract2__c contract : tempContract_List) {
			
			if (startDate < endDate && contract.Contract_Start_Date__c >= startDate && contract.EndDate__c <= endDate) {
				
				relatedContracts_List.add(contract);
			}
		}
	}
}