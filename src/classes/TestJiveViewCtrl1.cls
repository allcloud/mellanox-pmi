@isTest
public class TestJiveViewCtrl1 {

    public static testMethod void  test()
    {
        PageReference pageRef = Page.jiveView1;
        Test.setCurrentPage(pageRef);
        Case c = new Case(Subject='testCaseSubject1',Origin='Jive',status='New',Jive_Author_Name__c='JiveAuthorName1',Jive_Author_Email__c='jiveAuthor1@cs.com',JiveURL__c='https://demojive-community1.jiveon.com/thread/1',Description='caseSummary1');
        insert c;
        ApexPages.currentPage().getParameters().put('id', c.id);
        ApexPages.StandardController stdC = new ApexPages.StandardController(c);
        JiveViewCtrl1 jv1 = new JiveViewCtrl1(stdC);
        jv1.getDetails('"listForPlugin":[]');
        jv1.getDetails('Application Error');
        jv1.getDetails('Discussion Not Found in Jive');
        jv1.insertDesc();
        
        JiveViewCtrl1.attachments a = new JiveViewCtrl1.attachments('t1','t2');
        List<JiveViewCtrl1.attachments> aList = new List<JiveViewCtrl1.attachments>();
        aList.add(a);
        
        JiveViewCtrl1.descJsonWrapper d = new JiveViewCtrl1.descJsonWrapper('t1','t2',aList,'t3');        
        
        
    }
}