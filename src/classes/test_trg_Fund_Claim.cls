@isTest
public with sharing class test_trg_Fund_Claim {
    
    static testMethod void  test_sendFundClaim() {
        
        CLS_ObjectCreator obj = new CLS_ObjectCreator();    
        Account acc = obj.createAccount();
        acc.Name = 'test Name';
        acc.billingStreet = 'test Street';
        acc.billingCountryCode = 'US';
        acc.BillingState = 'Texas';
        acc.BillingCity = 'Houston';
        insert acc; 
        
        SFDC_MDF__c mdf = new SFDC_MDF__c();
        mdf.Account__c = acc.Id;
        mdf.Name = 'Test MDF';
        mdf.Amount__c = 1200;       
        mdf.Activity_Start_Date__c = date.today().addDays(20);
        mdf.Activity_End_Date__c = mdf.Activity_Start_Date__c + 10;
        mdf.Fund_Request_Type__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1000;
        mdf.Activity_Description__c = 'Advertising';
        mdf.Purpose_of_Activity__c = 'Advertising';
        mdf.Activity_Product_Focus__c = 'Advertising';
        mdf.Activity_Vertical_Market_Focus__c = 'Advertising';
        mdf.Marketing_Dollars_Requested__c = 1000;
        mdf.Target_Audience__c = 'Advertising';
        mdf.Total_Activity_Cost__c = 1500;
        mdf.Status__c = 'Draft';
        mdf.CAM__c = UserInfo.getUserId();
        
        insert mdf;
        
        SFDC_MDF_Claim__c mdfClaim = new SFDC_MDF_Claim__c();
        mdfClaim.Account__c = acc.Id;
       // mdfClaim.Name = 'Test MDF';
        mdfClaim.Amount__c = 1200;       
        mdfClaim.Fund_Request__c = mdf.Id;
        insert mdfClaim;
                
        Attachment testAtt = new Attachment();
        testAtt.parentId = mdfClaim.Id;
        testAtt.Name = 'test Name';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        testAtt.Body = bodyBlob;
        insert testAtt;
        
        mdfClaim.Invoice_ID__c = testAtt.Id;
        
        update mdfClaim;
        
        test.startTest();
        
        mdfClaim.Status__c = 'Submitted';
        
        update mdfClaim;
        
        mdfClaim.Status__c = 'Approved';
        
        update mdfClaim;
        
        test.stopTest();
    }
}