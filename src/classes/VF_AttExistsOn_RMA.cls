/*****       This class is a controller class that will determeine if the message 
                          that is on the related VF will be displayed on the RMA Layout    *****/

public with sharing class VF_AttExistsOn_RMA {
	
	public RMA__c rma{get;set;}
	//Boolean that determines if the output will be displayed on tha page
	public boolean showPage{get;set;}
	
	public VF_AttExistsOn_RMA(ApexPages.StandardController controller)
	{
		rma = (RMA__c)controller.getRecord();
		RMA__C dbRma = [SELECT Id, Attachment_Exists_on_RMA__c FROM RMA__c Where Id =: rma.Id];
		
		if (dbRma.Attachment_Exists_on_RMA__c == true)
		{
			showPage = true;
		}
		
		else
		{
			showPage = false;
		}
	}
	

}