public without sharing class vfInternalRMAForm 
{
    public boolean isInternal                 {get;set;}     //0 is new not 0 is edit
    public RMA__c MainRMA                     {get;set;}
    public List<SerialNumber> lstSerialNumber {get;set;}
    public Boolean IsSitesUser                {get;set;}
    public Boolean EndOfPage                  {get;set;}
    public String ErrMsg                      {get;set;}
    public Integer UrlStatus                  {get;set;}
    public String retUrl;
    public Id InRecordTypeId ;
    //public Id SerialRecordTypeId;
    
    public class SerialNumber
    {
        public Serial_Number__c serial {get;set;}
        public Integer ind {get;set;}
        
        public SerialNumber(Serial_Number__c serial,Integer ind )
        {
            this.serial = serial;
            this.ind = ind;
        }
    }
    
    public vfInternalRMAForm(ApexPages.StandardController controller) 
    {       
        String i  = Apexpages.currentPage().getParameters().get('type');
        retUrl    = system.currentPageReference().getParameters().get('retUrl');
        
        InRecordTypeId     = [Select r.Name, r.Id From RecordType r where r.Name = 'Internal RMA'].Id ;
        //SerialRecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name = 'Internal Serial'].Id ;
        system.debug('===>InRecordTypeId '+InRecordTypeId);
        //system.debug('===>SerialRecordTypeId '+SerialRecordTypeId);
        resetForm();
        FillFormFromCase();       
    } 
        
    public void FillFormFromCase()
    {
        ID currentCaseId = system.currentPageReference().getParameters().get(ENV.CaseFieldInRMA);
        if(currentCaseId!=null)
        {            
            List<Case> CaseList = [Select c.Id,c.Account.ShippingPostalCode,c.Account.ShippingCity,c.Account.ShippingStreet, c.Account.BillingCity,c.Account.BillingStreet,c.Contact.Email,c.Contact.Phone,c.Contact.Fax,c.Contact.Department, c.Contact.Name, c.Contact.Id, c.ContactId, c.Account.Phone, c.Account.BillingPostalCode, c.Account.Name, c.Account.Id, c.AccountId, c.caseNumber From Case c where c.Id = :currentCaseId];
            if(CaseList.size()>0)
            {
                Case  CurrCase         = CaseList[0];
                MainRMA.Case_Parent__c = CurrCase.Id;
                MainRMA.Case_Number__c = CurrCase.caseNumber;
                MainRMA.Name__c        = CurrCase.Contact.Name;
                MainRMA.E_Mail__c      = CurrCase.Contact.Email;
                MainRMA.Contact_Phone__c = CurrCase.Contact.Phone;
                MainRMA.Contact__c       = CurrCase.Contact.Id;
                MainRMA.Account_Name__c  = CurrCase.AccountId;
                MainRMA.Account_Name_text__c = CurrCase.Account.Name;
                MainRMA.Ship_Company__c           = CurrCase.Account.Name;
                MainRMA.Ship_Contact__c           = CurrCase.Contact.Name;
                MainRMA.Ship_Phone__c             = CurrCase.Account.Phone;
                MainRMA.Ship_Zip__c               = CurrCase.Account.ShippingPostalCode;
                MainRMA.Ship_City_State__c        = CurrCase.Account.ShippingCity;
                MainRMA.Ship_Company_Address_1__c = CurrCase.Account.ShippingStreet;
                MainRMA.Ship_Zip__c               = CurrCase.Account.ShippingPostalCode; 
                MainRMA.Department__c             = CurrCase.Contact.Department ;
            }
        }
        
    }
    
        public void SaveForm()
        {
              system.debug('===>SaveForm');
                ErrMsg='';
                List<Serial_Number__c> insSerials = new List<Serial_Number__c>();
                List<US_Territory_Map__c> territoryMap = new List<US_Territory_Map__c>();
                MainRMA.RecordTypeId = InRecordTypeId ;
                system.debug('===>MainRMA.RecordTypeId '+MainRMA.RecordTypeId);
                territoryMap =[select Ship_Restrict__c from US_Territory_Map__c where country__c = :MainRMA.Ship_Country__c];
                
                if(!territoryMap.isempty())
                { 
                        MainRMA.Ship_Restrictions__c = territoryMap[0].Ship_Restrict__c;
                        if(territoryMap[0].Ship_Restrict__c == 'non IL products')
                        {
                                MainRMA.Mellanox_Site__c = ENV.MellanoxSiteHomgKong;
                        }         
                }
                
                Boolean AllSerialsFilled = true;
                for(SerialNumber s :lstSerialNumber)
                {
                        if(((s.serial.Part_Number__c!=null && s.serial.Part_Number__c!='') || 
                           (s.serial.Serial__c!=null && s.serial.Serial__c!='') ||
                           ((s.serial.Problem_Description__c!=null && s.serial.Problem_Description__c!='')&& (s.serial.DOA_new__c!= null && s.serial.DOA_new__c!= '' ))) && 
                           !((s.serial.Part_Number__c!=null && s.serial.Part_Number__c!='') && 
                           (s.serial.Serial__c!=null && s.serial.Serial__c!='') &&
                           ((s.serial.Problem_Description__c!=null && s.serial.Problem_Description__c!='')&& (s.serial.DOA_new__c!= null && s.serial.DOA_new__c!= '' ))))
                        {
                                AllSerialsFilled = false;
                        }
                }
                system.debug('===>lstSerialNumber '+lstSerialNumber);
                if(AllSerialsFilled)
                {
                        for(SerialNumber s :lstSerialNumber)
                        {
                                if((s.serial.Part_Number__c!=null && s.serial.Part_Number__c!='') && 
                                   (s.serial.Serial__c!=null && s.serial.Serial__c!='') &&
                                   ((s.serial.Problem_Description__c!=null && s.serial.Problem_Description__c!='') && (s.serial.DOA_new__c!= null && s.serial.DOA_new__c!= '' )))
                                {
                                	    s.serial.Serial__c = s.serial.Serial__c.deleteWhitespace().toUpperCase();
                                	    //s.serial.RecordTypeId=SerialRecordTypeId;
                                        insSerials.Add(s.serial);
                                }
                        }
                        if(insSerials.size()>0)
                        {
                                if(MainRMA!=null)
                                {
	                                system.debug('===>MainRMA '+MainRMA);
	                                insert MainRMA;
	                                system.debug('===>MainRMA '+MainRMA);
                                }
                                for(Serial_Number__c s :insSerials)
                                {
                                    s.RMA__c = MainRMA.Id;
                                }
                                if(lstSerialNumber.size()>0)
                                {
                           	 	    	system.debug('===>insSerials '+insSerials);
                                        insert insSerials;
                                }
                                resetForm();
                                EndOfPage = true;
                                
                                //sandbox
                                /*
                                ID guestUserID = ENV.SiteUser;
                                if(Userinfo.getUserId()!= guestUserID)
                                {
                                        Pagereference p = new Pagereference(ENV.refrenceToRMA);
                                        UrlStatus = 0;
                                }
                                else*/
                                {
                                        Pagereference p = new Pagereference('http://www.mellanox.com/content/pages.php?pg=rma&action=success');
                                        UrlStatus = 1;
                                }
                        }
                        else
                        {
                                ErrMsg = 'You Must Fill At Least One Serial';
                                UrlStatus = 2;
                        }
                }
                else
                {
                        ErrMsg = 'All serials must have part number, serial number, description and DOA ';
                        UrlStatus = 2;
                }
                
        }
        
    public Pagereference Cancel()
    {
        Pagereference p = new Pagereference(retUrl);
        return p;
    }
        
    public void resetForm()
    {
        system.debug('==>resetForm ');
        IsSitesUser = true ;
        EndOfPage   = false;
        ErrMsg      = '';
        UrlStatus   = 2 ;
        MainRMA     = new RMA__c(Date_Submitted__c = system.today(),RecordTypeId = InRecordTypeId);
        lstSerialNumber = new List<SerialNumber>();
        MainRMA.ShipTo_same_as_BillTo__c = false;
        
        if(Userinfo.getUserId()!= ENV.SiteUser)
        {
            MainRMA.OwnerId = Userinfo.getUserId();
        }
        else
        {
            MainRMA.OwnerId = ENV.QueueMap.get('RMA');
        }
        system.debug('==>MainRMA.OwnerId '+MainRMA.OwnerId);
       
        for(Integer i=1;i<=1;i++)  
        {
                lstSerialNumber.Add(new SerialNumber(new Serial_Number__c(),i));
        }
        system.debug('==>lstSerialNumber '+lstSerialNumber);
        ID conID = system.currentPageReference().getParameters().get(ENV.CaseFieldInRMA);
        List<Contact> conList = [select Account.BillingPostalCode,Account.Name,Account.BillingState,Account.BillingStreet, Account.BillingCountry,Account.BillingCity,Account.Phone,Name, AccountId,Email,Phone,Fax, Id from Contact where Id = :conID];
        User usr   = [select Id,Contact.Account.BillingPostalCode,Contact.Account.Name,Contact.Account.BillingState,Contact.Account.BillingStreet, Contact.Account.BillingCountry,Contact.Account.BillingCity,Contact.Account.Phone,Contact.Name, Contact.AccountId,Contact.Email,Contact.Phone,Contact.Fax, ContactId from User where Id = :Userinfo.getUserId()];
        Contact con;
        if(usr.ContactId!=null)
        {
            con = usr.Contact;
        }
        else if (conList.size()>0)
        {
            con = conList[0];
        } 
        system.debug('==>con '+con);
        if(con !=null)
        {
          //get info from the account and add it to the RMA
          IsSitesUser       = false;
          MainRMA.Name__c   = con.Name;
          MainRMA.E_Mail__c = con.Email;
          MainRMA.Contact_Phone__c = con.Phone;
          MainRMA.Contact__c       = con.Id;
          MainRMA.Account_Name_text__c = con.Account.Name;
          MainRMA.Account_Name__c  = con.AccountId;
          MainRMA.Department__c    = con.Department ;  
          MainRMA.Site_Location__c = 'MTL';    // the defualt option
          MainRMA.Failure_Analysis_Request__c = 'NO';
          system.debug('==>MainRMA '+MainRMA);
        }
                
  }
    
    public List<selectOption> getFailureAnalysis()
    {
        List<selectoption> FAoptions = new List<selectOption>();
        FAoptions.Add(new selectoption('No','No'));
        FAoptions.Add(new selectoption('Yes','Yes'));
        return FAoptions;
    }
    
    public List<selectOption> getSiteLocation()
    {
        List<selectoption> SiteLocationOptions = new List<selectOption>();
        SiteLocationOptions.Add(new selectoption('MTL','MTL'));
        SiteLocationOptions.Add(new selectoption('MTR','MTR'));
        SiteLocationOptions.Add(new selectoption('MTV','MTV'));
        SiteLocationOptions.Add(new selectoption('MTI','MTI'));
        SiteLocationOptions.Add(new selectoption('MTB','MTB'));
        SiteLocationOptions.Add(new selectoption('Other','Other'));
        return SiteLocationOptions;
    }
        
    public List<selectOption> getAllCountries()
    {
        List<US_Territory_Map__c> countryOptions = [Select u.Id, u.Country__c From US_Territory_Map__c u where IsDeleted=false Order By Country__c];
        system.debug('BLAT countryOptions: ' + countryOptions);
        
        set<string> countryset    = new set<string>();
        List <String> lst_country = new List <String>();
        
        for(US_Territory_Map__c c: countryOptions)
        {
            if(!countryset.contains(c.Country__c)&& c.Country__c != NULL)
            {
                countryset.Add(c.Country__c);
                lst_country.Add(c.Country__c);
            }
        }
        system.debug('BLAT countryset: ' + countryset);
        
        List<selectoption> coptions = new List<selectOption>();
        coptions.Add(new selectoption('','- None -'));
        
        for(String s : lst_country)
        {
            system.debug('BLAT s: ' + s);
            coptions.Add(new selectoption(s,s));
        }
        
        system.debug('BLAT coptions: ' + coptions);
        return coptions;
            
    }
    
    public void AddSerial()
    {
        if(lstSerialNumber.size()<40)
        {
            lstSerialNumber.Add(new SerialNumber(new Serial_Number__c(),lstSerialNumber.size()+1));
        }
    }
    public void RemoveSerial()
    {
        if(lstSerialNumber.size()>1)
        {
            lstSerialNumber.remove(lstSerialNumber.size()-1);
        }
    }
    public Pagereference selectSitLocation()
    {
        if( MainRMA.Name__c == null || MainRMA.Name__c =='' || MainRMA.E_Mail__c == null || MainRMA.E_Mail__c == '' || MainRMA.Contact_Phone__c == null || MainRMA.Contact_Phone__c == '')
        {
                MainRMA.Site_Location__c ='MTL';
        }
        return null;
    }
}