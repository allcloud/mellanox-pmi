public without sharing class MPCAdvertisementController {
	
	public String display_order { set; get; }
    
    public MPCAdvertisementController() {
    	ApexPages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');	
    }
                                                 // The record's content field
    public String content 
    {
        get 
        {
                                                 // Retrieve the Image__c field
                                                 // information based on the display_order
            System.debug( 'START: MPCAdvertisementController.getContent()' );
            System.debug( 'Params: display_order = ' + display_order );
            
            if ( display_order != null && display_order != '' ) 
            {
                if( content == null ) 
                {
                    try 
                    {
                        List<MyMellanox_Advertisement__c> result;
                        
                        String query = 'SELECT Image__c ' +
                        	'FROM MyMellanox_Advertisement__c ' +
                        	'WHERE Display_Order__c = \'' + display_order + '\' ' +
                        	'AND Active__c = true ' +
                        	'AND (Start_Date__c = null OR Start_Date__c <= Today) ' +
                        	'AND (End_Date__c = null OR End_Date__c >= Today) ' +
                        	'Order by Start_Date__c desc ' +
                        	'limit 1';
                           
                        result = Database.query(query);                       
                          
                        if(result.size() > 0) {
                        	content = result[0].Image__c;
                        } else {
                            content = 'Invalid_Content';
                        }
                        
                    }  catch ( Exception ex )  {
                        content = 'Invalid_Content';
                    }
                }
                
            } 
            else  {
                content = 'Invalid_Content';
            }
            
            System.debug( 'Return: content = ' + content );
            System.debug( 'END: MPCAdvertisementController.getContent()' );
            
            return content;
        }
        
        set;
    }
}