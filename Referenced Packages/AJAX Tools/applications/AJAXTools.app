<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>AJAX Tools application for Salesforce.com</description>
    <label>AJAX Tools</label>
    <tab>AJAXTools</tab>
    <tab>Quotes</tab>
    <tab>Contract2__c</tab>
    <tab>Asset2__c</tab>
    <tab>OnSite__c</tab>
    <tab>Training_Request__c</tab>
    <tab>MellanoxSite__c</tab>
    <tab>Opp_Reg_OPN__c</tab>
    <tab>NDD__c</tab>
    <tab>Order_Search__c</tab>
    <tab>AttachmentIntegration__c</tab>
    <tab>RMPriority__c</tab>
    <tab>RmDiscussion__c</tab>
    <tab>BLND_DFDT_Project__c</tab>
    <tab>RmTracker__c</tab>
    <tab>RM_Project_Bucket__c</tab>
    <tab>AE_Queue_Manager__c</tab>
    <tab>Optional_AE_Owner__c</tab>
    <tab>Quota_Period__c</tab>
    <tab>MLNX_Price_Book__c</tab>
    <tab>ProductDetails__c</tab>
    <tab>KPI_Project__c</tab>
    <tab>Custom_Price_Book__c</tab>
    <tab>My_Subscriptions</tab>
    <tab>PCN_Contacts__c</tab>
    <tab>Pure_OPN__c</tab>
    <tab>MARKEITNG</tab>
    <tab>Deal_Round__c</tab>
    <tab>Company_Performance_and_Expectations__c</tab>
</CustomApplication>
